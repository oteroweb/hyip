<?php
/*
	CoinPayments.net API Example
	Copyright 2016 CoinPayments.net. All rights reserved.	
	License: GPLv2 - http://www.gnu.org/licenses/gpl-2.0.txt
*/
	require('./coinpayments.inc.php');
	$cps = new CoinPaymentsAPI();
	$cps->Setup('4c1476fb84083fa53a537e2f0839778a027f5a1433dfc896d8b1e2f15b7ccae6', '4c1476fb84083fa53a537e2f0839778a027f5a1433dfc896d8b1e2f15b7ccae6');

	$result = $cps->CreateTransfer(0.1, 'BTC', 'receiving_merchant_id');
	if ($result['error'] == 'ok') {
		print 'Transfer created with ID: '.$result['result']['id'];
	} else {
		print 'Error: '.$result['error']."\n";
	}
