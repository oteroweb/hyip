<?php
/*
	CoinPayments.net API Example
	Copyright 2016 CoinPayments.net. All rights reserved.	
	License: GPLv2 - http://www.gnu.org/licenses/gpl-2.0.txt
*/
	require('./coinpayments.inc.php');
	$cps = new CoinPaymentsAPI();
	$cps->Setup('6AF6805bB674f67302be7ede859044F6b2f533349746a696beC32B650B8F6d79', '9775f83ac92882612f5f332b1def117d3b742be8b408e0cfed32e8c5a27f04b6');

	$result = $cps->CreateTransactionSimple(10.00, 'USD', 'BTC', '', 'your_buyers_email@email.com');
	if ($result['error'] == 'ok') {
		$le = php_sapi_name() == 'cli' ? "\n" : '<br />';
		print 'Transaction created with ID: '.$result['result']['txn_id'].$le;
		print 'Buyer should send '.sprintf('%.08f', $result['result']['amount']).' BTC'.$le;
		print 'Status URL: '.$result['result']['status_url'].$le;
	} else {
		print 'Error: '.$result['error']."\n";
	}
