var change = 0;
var prechecker = "1";
var bad_format = "";
var url_breaks = "";
var pre_check = "";
var submit_site = "";
var submit_error = "";
function getHTTPObject() {
	var xmlhttp;
  	if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    		try {
      			xmlhttp = new XMLHttpRequest();
    		} catch (e) {
      			xmlhttp = false;
    		}
  	}
  return xmlhttp;
}
function switchDiv(elementId, bolVisible,row) {
	if(row == '') { row=0; }
	if( document.getElementById ) { var theElement = document.getElementById(elementId); } else {
	if( document.all ) { var theElement = document.all[ elementId ]; } else { var theElement = new Object(); } }
	if( !theElement ) { return; }
	if( theElement.style ) { theElement = theElement.style; }
	if( typeof( theElement.display ) == 'undefined' && !( window.ScriptEngine && ScriptEngine().indexOf( 'InScript' ) + 1 ) ) { window.alert( 'Hidden navigation will not work in this browser' ); return; }
	if (bolVisible == true) {
		if (row == 0) {
		theElement.display = 'block';
		}
		if(row == 2)
		{
			if(browser == 'Internet Explorer')
			{
				theElement.display = 'block';
			} else {
				theElement.display = 'table';
			}
		}
		if(row == 1)
		{
			if(browser == 'Internet Explorer')
			{
				theElement.display = 'block';
			} else {
				theElement.display = 'table-row';
			}
		}
	} else {
		theElement.display = 'none';
	}
}


//////////////////////////////////////////////////
function validateURL(url)
{
//URL reg exp
//var exp = /^(http(s?)\:\/\/)[a-zA-Z0-9\-\.]+\.(com|edu|gov|mil|net|org|biz|info|name|museum|us|ca|uk)(\?[a-zA-Z0-9\=\-]+)*$/;
var exp = /^(http(s?)\:\/\/)[a-zA-Z0-9\-\.]+/;
//	var exp = /^(((http(s?))|(ftp))\:\/\/)[a-zA-Z0-9\-\.]+\.(com|edu|gov|mil|net|org|biz|info|name|museum|us|ca|uk)(\?[a-zA-Z0-9\=]+)*(\/($|[a-zA-Z0-9\.\,\;\?\'\\\+&%\$#\=~_\-]+))*$/;
    var regex = new RegExp(exp);
    var result;
     if (url.length > 0)
     {
          result = regex.test(url);
          if (result == false)
          {
               return false;
          }else{
               return true;
          }
     }
}

var http = getHTTPObject();
var request_results;
var broke=0;
var error=0;
var wait = 11;
var WinId;
function checkframebreaker(stage,siteurl,frameclass)
{
	str=$("#framebreakmessage").val();
	var errormessage = str.split("|");
	
	var bad_format = errormessage[0];
	var url_breaks = errormessage[1];
	var pre_check = errormessage[2];
	var submit_site = errormessage[3];
	var submit_error = errormessage[4];
	if(prechecker == 0)
	{
		document.website.submit();
		return true;
	}
	//url = document.website.url.value;
	url = $(".txt"+frameclass).val();
	switch(stage) {
	case 0:
		//Initiate
		resetTest(pre_check);
		$("#check").removeClass().addClass('frambreakmain');
		switchDiv("check", true,0);
		//document["check_frame"].src="images/wait.gif";
//		setTimeout("checkframebreaker(2)",200);
		$("#framebreakimg").html("").removeClass().addClass('framebreakimgwait');
		checkframebreaker(1,siteurl,frameclass);
		break;
	case 1:
		//Check configuration
		if(!validateURL(url))
		{
			error = bad_format;
			//document["check_url"].src="images/fail.gif";
			$("#framebreakimg").html("").removeClass().addClass('framebreakimgfail');
			$("#check").removeClass().addClass('frambreakmainfail');
			setTimeout("checkframebreaker(7,'"+siteurl+"','"+frameclass+"')",100);
			break;
		}else{
			//document["check_url"].src="images/pass.gif";
		}
		setTimeout("checkframebreaker(8,'"+siteurl+"','"+frameclass+"')",1000);
		//document["check_frame"].src="images/wait.gif";
		var WinId = window.open(siteurl+"framechecker.php?url="+url+"&siteurl="+siteurl+"&frameclass="+frameclass,null,"height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
		
		break;
	case 2:
		//Start frambreaker
//		var WinId = window.open("backend/websites/test.php?url="+url,null,"height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");	
		break;
	case 3:
		//Finish Framebreaker
		if(broke == 1)
		{
			error = url_breaks;
			//document["check_frame"].src="images/fail.gif";	
			$("#framebreakimg").html("").removeClass().addClass('framebreakimgfail');
			$("#check").removeClass().addClass('frambreakmainfail');
			setTimeout("checkframebreaker(7,'"+siteurl+"','"+frameclass+"')",100);
			break;
		}else{
			broke = 2;
			$("#framebreakimg").html("").removeClass().addClass('framebreakimgsuccess');
			$("#check").removeClass().addClass('frambreakmainsuccess');
			//document["check_frame"].src="images/pass.gif";	
		}
		setTimeout("checkframebreaker(7,'"+siteurl+"','"+frameclass+"')",200);
		break;
	case 7:
		if(error != 0)
		{
			document.getElementById("check_final").innerHTML=submit_error+error;
		}else{
			document.getElementById("check_final").innerHTML=submit_site;
			//document.website.submit();
			$("."+frameclass).click();
			
		}
		break;
	case 8:
		wait--;
		if(broke == 0)
		{
			if(wait == 0)
			{
                                $("#framechecker").hide();
				//WinId.close();
				broke = 1;
				setTimeout("checkframebreaker(3,'"+siteurl+"','"+frameclass+"')",100);
			}
			else
			{
				document.getElementById("check_final").innerHTML=pre_check + ' - Testing URL: '+wait+' seconds';
				setTimeout("checkframebreaker(8,'"+siteurl+"','"+frameclass+"')",1000);
			}
		}
		break;
	}
	return false;
}
function resetTest(pre_check)
{
	//switchDiv("check", false,0);
	
	broke=0;
	error=0;
	wait=11;
	//document["check_url"].src="images/buffer.gif";
	//document["check_frame"].src="images/buffer.gif";
	$("#framebreakimg").html("").removeClass().addClass('framebreakimgwait');
	document.getElementById("check_final").innerHTML=pre_check;
}