if($(window).width()>590)
{
tinyMCE.init({
	theme : "advanced",
	skin : "o2k7",
	skin_variant : "silver",
	mode : "specific_textareas",
	editor_selector : "advancededitor",
	convert_urls : false,
	theme_advanced_buttons1 : "code,separator,bold,italic,underline,strikethrough,separator,justifyleft,justifycenter,justifyright,justifyfull,fontsizeselect,fontselect,formatselect,insertdate,inserttime,preview,zoom",
	theme_advanced_buttons2 : "bullist,numlist,separator,outdent,indent,separator,sub,sup,separator,link,unlink,separator,hr,removeformat,separator,undo,redo,separator,forecolor,backcolor,separator,emotions,fullscreen,iespell,nonbreaking,spellchecker",
	theme_advanced_buttons3 : "",
	plugins : "emotions,fullscreen,iespell,nonbreaking,spellchecker"
});
}