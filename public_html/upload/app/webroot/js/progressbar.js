(function( $ ){
  $.fn.animateProgress = function(progress, callback, mid, dorh, totsec, compsec) {
    return this.each(function() {
      $(this).animate({
        width: progress+'%'
      }, {
        duration: 0, 
        easing: 'swing',
        step: function( progress ){
          var labelEl = $('.ui-label'+mid),
              valueEl = $('.value', labelEl);
          
          var remainEl = $('.ui-remain'+mid),
              remainvalueEl = $('.value', remainEl);
          if (dorh=='days')
            remainvalueEl.text(Math.round((totsec-compsec)/3600/24));
          else
            remainvalueEl.text(Math.round((totsec-compsec)/3600));
          
          if (progress <= 0)
            labelEl.text('Expire');
          else
            valueEl.text(parseFloat(progress).toFixed(2) + '%');
            
        },
        complete: function(scope, i, elem) {
          if (callback) {
            callback.call(this, i, elem );
          };
        }
      });
    });
  };
})( jQuery );