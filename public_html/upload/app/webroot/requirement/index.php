<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ProXCore Script Installation</title>
<style type="text/css">
@font-face{
	font-family:'Open Sans';
	font-style:normal;
	font-weight:400;
	src:local('Open Sans'), local('OpenSans'), url("images/OpenSans.woff") format('woff');
}
@font-face{
	font-family:'Open Sans';
	font-style:normal;
	font-weight:700;
	src:local('Open Sans Bold'), local('OpenSans-Bold'), url("images/OpenSans-Bold.woff") format('woff');
}
body {margin: 0px;font-family:'Open Sans';background-color: #fff;font-size:12px;}
#header-bg { background-color: #f0f4f7;}
#header { width: 900px;  margin: 0px auto;}
.clearboth { clear: both; }
.logo { width: 255px; height: 65px; float: left; padding-top: 13px; }
.header-top-right { float: right; width: 257px; padding-top: 16px; }
.header-text { font-size: 25px; color: #516c7e; float: left; padding-right: 8px; padding-top: 5px;  }
.step-box { float: left; background: #19b698; height: 42px; width: 47px; text-align: center; font-size: 14px; color: #ffffff; padding-top: 5px; text-transform: uppercase;}
.step-bold { font-weight: 700; font-style: normal; }
#all-content { margin: 0px auto; width: 900px; padding-top: 15px; padding-bottom: 10px; }
.script-req-box { background: #465868; font-size: 17px; font-weight: 700; color: #fff; padding: 15px;}
.last-next-box {background: #fff; border-bottom:solid 1px #ea6153;  height: 51px; text-align: right; padding-top: 12px; padding-right: 11px; }
.next-btn { background: none; border: none; background: #19b698; width: 125px; height: 37px; text-align: center; line-height: 37px; font-size: 15px; color: #ffffff; float: right; cursor: pointer;}
.text-left { width: 360px; line-height: 40px; font-size: 14px; color: #000; font-weight: 400; float: left;}
.text-right { width: 500px; float: left; font-size: 13px; color: #4c4c4c; height: 40px; line-height: 40px; }
/*.grey-line {background: #FAFAFA; height: 40px; padding-left: 15px;  }
.white-line {background: #ffffff; height: 40px; padding-left: 15px;  }*/
#all-content .grey-line, #all-content .white-line { height:40px; vertical-align: middle;   border-bottom: 1px solid #f0f1f4; padding-left: 10px;}
#all-content .grey-line:nth-child(odd) { background:#f7f9fa; }
#all-content .grey-line:nth-child(even) { background:#ffffff; }
#all-content .grey-line:hover { background: none repeat scroll 0  0 #EDEDED ;}
#all-content .white-line:nth-child(odd) { background:#f7f9fa; }
#all-content .white-line:nth-child(even) { background:#ffffff; }
#all-content .white-line:hover { background: none repeat scroll 0  0 #EDEDED ;}

.from-box { background: #ffffff; width: 243px; height: 29px; border: solid 1px #ededed; color: #000; margin-top: 4px;}
.from-right-text { color: #067791; padding-left: 10px; }
.height10{}
.height5{ height: 5px;}
#footer-bg { background: #465868; }
.footer { width: 900px; margin: 0px auto; font-size: 12px; color: #fff; padding-top: 6px; padding-bottom: 6px; text-align: center; }
.footer a { text-decoration: none; color: #fff;}

.error{color:#F00;margin-left:20px;margin-top:5px;}
.reading{color:green;margin-left:20px;margin-top:5px;}

#UpdateMessage.formerror{background-color:#f2dede;background-image:url("images/error.png");background-position:10px 12px;background-repeat:no-repeat;padding:10px 0 0 30px;min-height:26px;font-size:13px;text-align:left;color:#b94a48}
#UpdateMessage.formsuccess{background-color:#dff0d8;background-image:url("images/success.png");background-position:10px 12px;background-repeat:no-repeat;padding:10px 0 0 30px;min-height:26px;font-size:13px;text-align:left;color:#468847}

</style>
<style type="text/css">
.ON,.OK{}
.OFF,.FAIL{color:#FF0000;}
.INFO{color:#666666;}
.TITLE{color:#000000;font-weight:bold;}
</style>
</head>
<body>
<div id="header-bg">
    
	<div id="header">
	    <div class="logo"><a href="https://www.ProxScripts.com" target="_blank"><img src="images/logo.png" /></a></div>
	    <div class="header-top-right">
		<div class="header-text">Script Requirements</div>
		
		 <div class="clearboth"></div>
	    </div>
	    <div class="clearboth"></div>
	</div>
	
</div>

	<div id="all-content">
		
		<div class="script-req-box">Script Requirements</div>
		<div class="grey-line">
			<div class="text-left">Webserver :</div>
			<div class="text-right" style="line-height: normal;"><?php echo getenv("SERVER_SOFTWARE");?></div>
			<div class="clearboth"></div>
		</div>
		<div class="white-line">
			<div class="text-left">PHP Version :</div>
			<?php $php_ver_class = (PHP_VERSION<'5') ? 'FAIL' : 'OK';?>
			<div class="text-right <?php echo $php_ver_class;?>"><?php echo PHP_VERSION;?></div>
			<div class="clearboth"></div>
		</div>
		<div class="grey-line">
			<div class="text-left">MySQL Support :</div>
			<?php $mysql_support = (function_exists( 'mysql' )) ? "ON" : "OFF";?>
			<div class="text-right <?php echo $mysql_support;?>"><?php echo $mysql_support;?></div>
			<div class="clearboth"></div>
		</div>
		<div class="white-line">
			<div class="text-left">PDO Support :</div>
			<?php $php_ext_pdo = (@extension_loaded('pdo')) ? "ON" : "OFF";?>
			<div class="text-right <?php echo $php_ext_pdo;?>"><?php echo $php_ext_pdo;?></div>
			<div class="clearboth"></div>
		</div>
		<div class="grey-line">
			<div class="text-left">PHP cURL Support :</div>
			<?php $php_ext_curl = (@extension_loaded('curl')) ? "ON" : "OFF";?>
			<div class="text-right <?php echo $php_ext_curl;?>"><?php echo $php_ext_curl;?></div>
			<div class="clearboth"></div>
		</div>
		<div class="white-line">
			<div class="text-left">PHP HASH Support :</div>
			<?php $php_ext_mhash = (@extension_loaded('hash')) ? "ON" : "OFF";?>
			<div class="text-right <?php echo $php_ext_mhash;?>"><?php echo $php_ext_mhash;?></div>
			<div class="clearboth"></div>
		</div>
		<div class="grey-line">
			<div class="text-left">PHP GD Support :</div>
			<?php $php_ext_gd = (@extension_loaded('gd')) ? "ON" : "OFF";?>
			<div class="text-right <?php echo $php_ext_gd;?>"><?php echo $php_ext_gd;?></div>
			<div class="clearboth"></div>
		</div>
		<div class="white-line">
			<div class="text-left">GD Version :</div>
			<?php $gd_info = (function_exists('gd_info')) ? @gd_info() : array('GD Version' => 'UNKNOWN');
			$gd_ver = 'GD ' . $gd_info['GD Version'];?>
			<div class="text-right <?php echo $php_ext_gd;?>"><?php echo $gd_ver;?></div>
			<div class="clearboth"></div>
		</div>
		<div class="grey-line">
			<div class="text-left">PHP FreeType library Support :</div>
			<?php $php_ext_freetype = ($gd_info['FreeType Support']) ? "ON" : "OFF";?>
			<div class="text-right <?php echo $php_ext_freetype;?>"><?php echo $php_ext_freetype;?></div>
			<div class="clearboth"></div>
		</div>
		<div class="white-line">
			<div class="text-left">PHP gzip Support :</div>
			<?php $php_ext_gzip = (@function_exists('ob_gzhandler')) ? "ON" : "OFF";?>
			<div class="text-right <?php echo $php_ext_gzip;?>"><?php echo $php_ext_gzip;?></div>
			<div class="clearboth"></div>
		</div>
		<div class="grey-line">
			<div class="text-left">PHP JSON Support :</div>
			<?php $php_ext_json = (@extension_loaded('json')) ? "ON" : "OFF";?>
			<div class="text-right <?php echo $php_ext_json;?>"><?php echo $php_ext_json;?></div>
			<div class="clearboth"></div>
		</div>
		<div class="white-line">
			<div class="text-left">PHP Ioncube Loader Support :</div>
			<?php $php_ext_ioncube_loader = (@extension_loaded('ionCube Loader')) ? "ON" : "OFF";?>
			<div class="text-right <?php echo $php_ext_ioncube_loader;?>"><?php echo $php_ext_ioncube_loader;?></div>
			<div class="clearboth"></div>
		</div>
		<div class="grey-line">
			<div class="text-left">PHP SOAP Support :</div>
			<?php $php_ext_soap = (@extension_loaded('soap')) ? "ON" : "OFF";?>
			<div class="text-right <?php echo $php_ext_soap;?>"><?php echo $php_ext_soap;?></div>
			<div class="clearboth"></div>
		</div>
		<div class="white-line">
			<div class="text-left">PHP mcrypt Support :</div>
			<?php $php_ext_mcrypt = (@extension_loaded('mcrypt')) ? "ON" : "OFF";?>
			<div class="text-right <?php echo $php_ext_mcrypt;?>"><?php echo $php_ext_mcrypt;?></div>
			<div class="clearboth"></div>
		</div>
		<div class="grey-line">
			<div class="text-left">PHP Server Time Zone :</div>
			<div class="text-right ON"><?php echo date_default_timezone_get();?></div>
			<div class="clearboth"></div>
		</div>
	
	</div>
	<div class="clearboth"></div>

<div id="footer-bg">
	<div class="footer">Copyright &copy; 2015 <a href="https://www.ProxScripts.com" target="_blank">ProxScripts.com</a> All Rights Reserved</div>
</div>
</body>
</html>