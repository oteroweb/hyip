<?php
class PayzanotifyController extends AppController 
{
	public function index() 
	{
    	if(isset($_POST['ap_merchant']) && isset($_POST['ap_itemname']) && $_POST['ap_itemname']=='Add Funds')//Payza 2
		{
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'Payza', 'Processor.proc_status' => 1, 'Processor.isaddfund' => 1)
				)
			);
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$_POST['ap_itemcode'],'status' => $_POST['ap_status'], 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['ap_referencenumber'], 'processor' => 'Payza','type'=>'fund','description' => trim($description), 'ipaddress' => $_POST['apc_1'], 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			if(count($processorsdata)==0)
			{exit;}
			//Setting information about the transaction
			$receiveremail = $_POST['ap_merchant'];	
			$transactionstatus = $_POST['ap_status'];
			$testModeStatus = $_POST['ap_test'];	 
			$transaction_no = $_POST['ap_referencenumber'];
			$transactiondate= $_POST['ap_transactiondate'];
			$payeremail= $_POST['ap_custemailaddress'];
			$memberid = $_POST['ap_itemcode'];
			//$paidamount = $_POST['ap_amount'];
			$paidamount = ($_POST['ap_totalamount']-$_POST['ap_discountamount']);
			
			$ip=$_POST['apc_1'];
			$pending_deposit_id=$_POST['apc_2'];
			
			$this->loadModel('Pending_deposit');
			$pendingdepositdata=$this->Pending_deposit->find('first', array(
				'conditions' => array('Pending_deposit.id' => $pending_deposit_id, 'Pending_deposit.member_id' => $memberid, 'round(Pending_deposit.paidamount,2)' => $paidamount, 'Pending_deposit.processorid' => $processorsdata['Processor']['id'], 'Pending_deposit.ip_address' => $ip)
				)
			);
			
			if(count($pendingdepositdata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingdeposit_id-'.$pending_deposit_id.' | p_type-fund', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza','processorid'=>$processorsdata['Processor']['id'], 'ptype' => 'fund','paymentmethod'=>'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype','paymentmethod', 'ip_add'));
				
				$this->Pending_deposit->updateAll(
					array("Pending_deposit.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Payza,ptype-fund\n')"),
					array("Pending_deposit.id" => $pending_deposit_id)
				);
				exit;
			}
			
			if($paidamount<=0)
				$paidamount=0;
			if($pending_reason=='')
				$pending_reason='-';
			if($transaction_no=='')
				$transaction_no='-';
			
			//define("IPN_SECURITY_CODE", $this->dycriptstring($processorsdata['Processor']['api_signature']));
			//define("MY_MERCHANT_EMAIL", $this->dycriptstring($processorsdata['Processor']['proc_accid']));
			$IPN_SECURITY_CODE=$this->dycriptstring($processorsdata['Processor']['api_signature']);
			$MY_MERCHANT_EMAIL=$this->dycriptstring($processorsdata['Processor']['proc_accid']);
			
			$receivedSecurityCode = $_POST['ap_securitycode'];
			$receivedMerchantEmailAddress = $_POST['ap_merchant'];
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no,'Member_fee_payment.transaction_no != ' => '', 'Member_fee_payment.processor' => 'Payza')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund','paymentmethod'=>'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email','paymentmethod', 'processor','processorid', 'fees', 'ptype', 'ip_add'));
			}
			elseif($_POST['ap_currency']!="USD")
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Currency Changed By User', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid','paymentmethod', 'fees', 'ptype', 'ip_add'));
			}
			else
			{
				if ($receivedMerchantEmailAddress != $MY_MERCHANT_EMAIL) 
				{
					// The data was not meant for the business profile under this email address.
					// Take appropriate action 
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Merchant Email is wrong', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail,'paymentmethod'=>'processor', 'processor' => 'Payza','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod','processorid', 'fees', 'ptype', 'ip_add'));
				}
				else 
				{
					//Check if the security code matches
					if ($receivedSecurityCode != $IPN_SECURITY_CODE) 
					{
						// The data is NOT sent by Payza.
						// Take appropriate action.
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Security code does not match', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod','processorid', 'fees', 'ptype', 'ip_add'));
					}
					else 
					{
						if ($transactionstatus == "Success") 
						{
							$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => '-', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
							$this->Member_fee_payment->set($data);
							$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod','processorid', 'fees', 'ptype', 'ip_add'));
							
							
							
							$this->loadModel('Member');
							$memberdata=$this->Member->find('first', array(
								'conditions' => array('Member.member_id' => $memberid), 
								'fields' => array('Member.member_id', 'Member.f_name', 'Member.l_name', 'Member.user_name', 'Member.email', 'Member.unsubscribeemail')
								)
							);
							
							//Add Fund
							if($totaltamount>0)
							{
								if($this->sitesettingconfig[0]["sitesettings"]['balance_type']==1)
								{
									$this->Member->updateAll(
										array("Member.cash_balance" => 'cash_balance+'.round($pendingdepositdata['Pending_deposit']['amount'],2)),
										array("Member.member_id" => $memberid)
									);
								}
								else
								{
									$this->Member->updateAll(
										array("Member.cash_".$processorsdata['Processor']['id'] => 'cash_'.$processorsdata['Processor']['id'].'+'.round($pendingdepositdata['Pending_deposit']['amount'],2)),
										array("Member.member_id" => $memberid)
									);
								}
							}
							
							$this->Pending_deposit->delete($pending_deposit_id);
							
							//mail to member
							$sitetitle=$this->sitesettingconfig[0]["sitesettings"]["sitetitle"];
							if($memberdata['Member']['unsubscribeemail']==0)
							{
								if($this->sitesettingconfig[0]["sitesettings"]["reflinkiduser"]==1)
									$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'</a>';
								else
									$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'</a>';
								$this->template_mail($memberdata['Member']['member_id'],$memberdata['Member']['email'],'Successful Add Fund Notification For The Member',array('[SiteTitle]','[UserName]', '[FirstName]', '[LastName]','[Email]','[MemberID]', '[Amount]', '[Processor]','[RefLink]','[SiteUrl]','[Signature]'),array($sitetitle, $memberdata["Member"]['user_name'], $memberdata["Member"]['f_name'], $memberdata["Member"]['l_name'],$memberdata['Member']['email'],$memberdata['Member']['member_id'], $this->currencydata['Currency']['prefix'].round($paidamount*$this->currencydata['Currency']['rate'],4), 'Payza',$reflink,'<a href="'.$this->SITEURL.'">'.$this->SITEURL.'</a>',$this->sitesettingconfig[0]["sitesettings"]["signature"]));
							}
						}
						else 
						{
							// Transaction was cancelled or an incorrect status was returned.
							// Take appropriate action.
							$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Transaction was cancelled or an incorrect status was returned', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
							$this->Member_fee_payment->set($data);
							$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount','paymentmethod', 'receiver_email', 'processor','processorid', 'fees', 'ptype', 'ip_add'));
						}
					}
				}
			}//duplicate Transaction number
		}//Payza Over
		//Add Fund Code Over
		
		//Purchase Postion Code Start
		if(isset($_POST['ap_merchant']) && isset($_POST['ap_itemname']) && $_POST['ap_itemname']=='Purchase Position')//Payza
		{
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'Payza', 'Processor.proc_status' => 1, 'Processor.receivefund' => 1)
				)
			);
			
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$_POST['ap_itemcode'],'status' => 'Completed', 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['ap_referencenumber'], 'processor' => 'Payza','type'=>'position','description' => trim($description), 'ipaddress' => $_POST['apc_1'], 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			if(count($processorsdata)==0)
			{exit;}
			//Setting information about the transaction
			$receiveremail = $_POST['ap_merchant'];	
			$transactionstatus = $_POST['ap_status'];
			$testModeStatus = $_POST['ap_test'];	 
			$transaction_no = $_POST['ap_referencenumber'];
			$transactiondate= $_POST['ap_transactiondate'];
			$payeremail= $_POST['ap_custemailaddress'];
			$memberid = $_POST['ap_itemcode'];
			//$paidamount = $_POST['ap_amount'];
			$paidamount = ($_POST['ap_totalamount']-$_POST['ap_discountamount']);
			
			$ip=$_POST['apc_1'];
			$pending_position_id=$_POST['apc_2'];
			
			$this->loadModel('Pending_position');
			$pendingpositiondata=$this->Pending_position->find('first', array(
				'conditions' => array('Pending_position.id' => $pending_position_id, 'Pending_position.member_id' => $memberid, 'round(Pending_position.paidamount,2)' => $paidamount, 'Pending_position.processor' => 'Payza', 'Pending_position.ip_address' => $ip)
				)
			);
			$discount=$pendingpositiondata['Pending_position']['discount'];
						
			if(count($pendingpositiondata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingposition_id-'.$pending_position_id.' | p_type-position', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype', 'paymentmethod', 'ip_add'));
				
				$this->Pending_position->updateAll(
					array("Pending_position.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Payza,ptype-position,paymentmethod-processor\n')"),
					array("Pending_position.id" => $pending_position_id)
				);			
				exit;
			}
			
			if($paidamount<=0)
				$paidamount=0;
			if($pending_reason=='')
				$pending_reason='-';
			if($transaction_no=='')
				$transaction_no='-';
			
			//define("IPN_SECURITY_CODE", $this->dycriptstring($processorsdata['Processor']['api_signature']));
			//define("MY_MERCHANT_EMAIL", $this->dycriptstring($processorsdata['Processor']['proc_accid']));
			$IPN_SECURITY_CODE=$this->dycriptstring($processorsdata['Processor']['api_signature']);
			$MY_MERCHANT_EMAIL=$this->dycriptstring($processorsdata['Processor']['proc_accid']);
			
			$receivedSecurityCode = $_POST['ap_securitycode'];
			$receivedMerchantEmailAddress = $_POST['ap_merchant'];
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>0, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id, 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_position']['amount']-$pendingpositiondata['Pending_position']['discount'])));
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'Payza')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
				
			}
			else
			{
				if ($receivedMerchantEmailAddress != $MY_MERCHANT_EMAIL) 
				{
					// The data was not meant for the business profile under this email address.
					// Take appropriate action 
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Merchant Email is wrong', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
				}
				else 
				{	
					//Check if the security code matches
					if ($receivedSecurityCode != $IPN_SECURITY_CODE) 
					{
						// The data is NOT sent by Payza.
						// Take appropriate action.
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Security code does not match', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
					}
					else 
					{
						if ($transactionstatus == "Success") 
						{
							//Member Data Start
							$this->loadModel('Member');
							$MemberData = $this->Member->read(null,$memberid);
							//Member Data Over
							
							//Add Position Start
							$ControllerShort=substr($_POST['apc_3'], 0, strlen($_POST['apc_3'])-1);
							App::import('Controller', $ControllerShort);
							$ControllerName=$ControllerShort."Controller";
							$ControllerObj = new $ControllerName;
							$FunctionName="AddPosition".str_replace($ControllerShort, "", $_POST['apc_3']);
							
							$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields, 0, $this->sitesettingconfig, $this->currencydata);
							
							//Delete Pending Position Record
							$this->Pending_position->delete($pending_position_id);
							//Add Position Over
						}
						else 
						{
							// Transaction was cancelled or an incorrect status was returned.
							// Take appropriate action.
							$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Transaction was cancelled or an incorrect status was returned', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
							$this->Member_fee_payment->set($data);
							$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
						}
					}
				}
			}//duplicate Transaction number
		}//Payza Over
		//Purchase Postion Code Over
		
		//Common Purchase Code Start
		if(isset($_POST['ap_merchant']) && isset($_POST['ap_itemname']) && $_POST['ap_itemname']=='Purchase Payment')//Payza
		{
			$ControllerShort=substr($_POST['apc_3'], 0, strlen($_POST['apc_3'])-1);
			$mem_ppid[0] = $_POST['ap_itemcode'];
			$mem_ppid[1] = $_POST['apc_2'];
			
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'Payza', 'Processor.proc_status' => 1, 'Processor.receivefund' => 1)
				)
			);
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$_POST['ap_itemcode'],'status' => 'Completed', 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['ap_referencenumber'], 'processor' => 'Payza','type'=>$ControllerShort,'description' => trim($description), 'ipaddress' => $_POST['apc_1'], 'pay_dt' => date('Y-m-d H:i:s'));
				
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			
			
			if(count($processorsdata)==0)
			{exit;}
			
			$receiveremail = $_POST['ap_merchant'];	
			$transactionstatus = $_POST['ap_status'];
			$testModeStatus = $_POST['ap_test'];	 
			$transaction_no = $_POST['ap_referencenumber'];
			$transactiondate= $_POST['ap_transactiondate'];
			$payeremail= $_POST['ap_custemailaddress'];
			$memberid = $_POST['ap_itemcode'];
			//$paidamount = $_POST['ap_amount'];
			$paidamount = ($_POST['ap_totalamount']-$_POST['ap_discountamount']);
			
			$ip=$_POST['apc_1'];
			$pending_position_id=$_POST['apc_2'];
			
			$this->loadModel('Pending_plan');
			$pendingpositiondata=$this->Pending_plan->find('first', array(
				'conditions' => array('Pending_plan.id' => $pending_position_id, 'Pending_plan.member_id' => $memberid, 'round(Pending_plan.paidamount,2)' => $paidamount, 'Pending_plan.processor' => 'Payza', 'Pending_plan.ip_address' => $ip)
				)
			);
			$discount=$pendingpositiondata['Pending_plan']['discount'];
						
			if(count($pendingpositiondata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingplan_id-'.$pending_position_id.' | p_type-'.$ControllerShort, 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype', 'ip_add'));
				
				$this->Pending_plan->updateAll(
					array("Pending_plan.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Payza,ptype-".$ControllerShort.",paymentmethod-processor\n')"),
					array("Pending_plan.id" => $pending_position_id)
				);			
				exit;
			}
			
			if($paidamount<=0)
				$paidamount=0;
			if($pending_reason=='')
				$pending_reason='-';
			if($transaction_no=='')
				$transaction_no='-';
				
			//define("IPN_SECURITY_CODE", $this->dycriptstring($processorsdata['Processor']['api_signature']));
			//define("MY_MERCHANT_EMAIL", $this->dycriptstring($processorsdata['Processor']['proc_accid']));
			$IPN_SECURITY_CODE=$this->dycriptstring($processorsdata['Processor']['api_signature']);
			$MY_MERCHANT_EMAIL=$this->dycriptstring($processorsdata['Processor']['proc_accid']);
			
			$receivedSecurityCode = $_POST['ap_securitycode'];
			$receivedMerchantEmailAddress = $_POST['ap_merchant'];	
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>0, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id,'advertisementsetting'=>$pendingpositiondata['Pending_plan']['advertisementsetting'], 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_plan']['amount']-$pendingpositiondata['Pending_plan']['discount'])));
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'Payza')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
			}
			else
			{
				if ($receivedMerchantEmailAddress != $MY_MERCHANT_EMAIL) 
				{
					// The data was not meant for the business profile under this email address.
					// Take appropriate action 
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Merchant Email is wrong', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
				}
				else
				{
					//Check if the security code matches
					if ($receivedSecurityCode != $IPN_SECURITY_CODE) 
					{
						// The data is NOT sent by Payza.
						// Take appropriate action.
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Security code does not match', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
					}
					else
					{
						if ($transactionstatus == "Success") 
						{
							//Member Data Start
							$this->loadModel('Member');
							$MemberData = $this->Member->read(null,$memberid);
							//Member Data Over
							
							//Add Position Start
							$ControllerShort=substr($_POST['apc_3'], 0, strlen($_POST['apc_3'])-1);
							App::import('Controller', $ControllerShort);
							$ControllerName=$ControllerShort."Controller";
							$ControllerObj = new $ControllerName;
							$FunctionName="AddPlan".str_replace($ControllerShort, "", $_POST['apc_3']);
							
							$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields,$this->sitesettingconfig,$this->currencydata);
							
							//Delete Pending Position Record
							$this->Pending_plan->delete($pending_position_id);
							//Add Position Over
						}
						else 
						{
							// Transaction was cancelled or an incorrect status was returned.
							// Take appropriate action.
							$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Transaction was cancelled or an incorrect status was returned', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payza', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
							$this->Member_fee_payment->set($data);
							$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
						}
					}
				}
				
			}//duplicate transection number
		
		}//Okpay Over
		
		//Common Purchase Code Over
		
		//Masspay Code Start
		$with_ids='';
		if($_POST['ap_transactiontype']=="masspay")//Payza Masspay
		{
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'Payza', 'Processor.proc_status' => 1, 'Processor.is_masspay' => 1)
				)
			);
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$myCustomField= explode("|",$_POST['ap_mpcustom']);
				
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$myCustomField[0],'status' => 'Done', 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['ap_referencenumber'], 'processor' => 'Payza','type'=>'masspay','description' => trim($description), 'ipaddress' => '-', 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debug Code Over
			
			
			if(count($processorsdata))
			{
				define("IPN_SECURITY_CODE", $this->dycriptstring($processorsdata['Processor']['api_signature']));
				define("MY_MERCHANT_EMAIL", $this->dycriptstring($processorsdata['Processor']['proc_accid']));
				$receivedMerchantEmailAddress = $_POST['ap_merchant'];
				
				//Setting information about the transaction
				$receivedSecurityCode = $_POST['ap_securitycode'];
				$senderEmailAddress = $_POST['ap_merchant'];
				$receiverEmailAddress = $_POST['ap_receiveremail'];
				$testModeStatus = $_POST['ap_test'];
				$transactionReferenceNumber = $_POST['ap_referencenumber'];
				$currency = $_POST['ap_currency']; 		
				$paymentAmount = $_POST['ap_amount'];
				$transactionType = $_POST['ap_transactiontype'];	
				$transactionDate= $_POST['ap_transactiondate'];
				$myCustomField= explode("|",$_POST['ap_mpcustom']);
				
				//Setting the information about the MassPay from the IPN post variables
				$batchNumber = $_POST['ap_batchnumber'];
				$apiReturnCode = $_POST['ap_returncode'];
				$apiReturnCodeDescription = $_POST['ap_returncodedescription'];
				
				$this->loadModel('Withdrawal_history');
				$whcounter=$this->Withdrawal_history->find('count', array(
					'conditions' => array('Withdrawal_history.refrence_no' => $transactionReferenceNumber, 'Withdrawal_history.processor' => 'Payza')
					)
				);
				if($whcounter>0)
				{
					$data = array('batch_number' => $batchNumber, 'return_code' => $apiReturnCode, 'return_desc' => 'Duplicate Transection Number', 'refrence_no' => $transactionReferenceNumber, 'dt' => date('Y-m-d H:i:s'), 'amount' => $paymentAmount, 'sender_email' => $senderEmailAddress, 'receiver_email' => $receiverEmailAddress, 'receiver_id' => $myCustomField[0], 'with_id' => $myCustomField[1], 'processor' => 'Payza', 'processor_id' => $processorsdata['Processor']['id']);
					$this->Withdrawal_history->set($data);
					$this->Withdrawal_history->save($this->Withdrawal_history->data, false, array('batch_number', 'return_code', 'return_desc', 'refrence_no', 'dt', 'amount', 'sender_email', 'receiver_email', 'receiver_id', 'with_id', 'processor', 'processor_id'));
				}
				else
				{
					if ($receivedMerchantEmailAddress != MY_MERCHANT_EMAIL)
					{
						$data = array('batch_number' => $batchNumber, 'return_code' => $apiReturnCode, 'return_desc' => 'Merchant Email Does Not Match', 'refrence_no' => $transactionReferenceNumber, 'dt' => date('Y-m-d H:i:s'), 'amount' => $paymentAmount, 'sender_email' => $senderEmailAddress, 'receiver_email' => $receiverEmailAddress, 'receiver_id' => $myCustomField[0], 'with_id' => $myCustomField[1], 'processor' => 'Payza', 'processor_id' => $processorsdata['Processor']['id']);
						$this->Withdrawal_history->set($data);
						$this->Withdrawal_history->save($this->Withdrawal_history->data, false, array('batch_number', 'return_code', 'return_desc', 'refrence_no', 'dt', 'amount', 'sender_email', 'receiver_email', 'receiver_id', 'with_id', 'processor', 'processor_id'));
					}
					else
					{	
						if ($receivedSecurityCode != IPN_SECURITY_CODE)
						{
							$data = array('batch_number' => $batchNumber, 'return_code' => $apiReturnCode, 'return_desc' => 'Security Code Does Not Match', 'refrence_no' => $transactionReferenceNumber, 'dt' => date('Y-m-d H:i:s'), 'amount' => $paymentAmount, 'sender_email' => $senderEmailAddress, 'receiver_email' => $receiverEmailAddress, 'receiver_id' => $myCustomField[0], 'with_id' => $myCustomField[1], 'processor' => 'Payza', 'processor_id' => $processorsdata['Processor']['id']);
							$this->Withdrawal_history->set($data);
							$this->Withdrawal_history->save($this->Withdrawal_history->data, false, array('batch_number', 'return_code', 'return_desc', 'refrence_no', 'dt', 'amount', 'sender_email', 'receiver_email', 'receiver_id', 'with_id', 'processor', 'processor_id'));
						}
						else 
						{
							if ($transactionType == "masspay") 
							{
								if ($apiReturnCode == "100") 
								{
									$data = array('batch_number' => $batchNumber, 'return_code' => $apiReturnCode, 'return_desc' => $apiReturnCodeDescription, 'refrence_no' => $transactionReferenceNumber, 'dt' => date('Y-m-d H:i:s'), 'amount' => $paymentAmount, 'sender_email' => $senderEmailAddress, 'receiver_email' => $receiverEmailAddress, 'receiver_id' => $myCustomField[0], 'with_id' => $myCustomField[1], 'processor' => 'Payza', 'processor_id' => $processorsdata['Processor']['id']);
									$this->Withdrawal_history->set($data);
									$this->Withdrawal_history->save($this->Withdrawal_history->data, false, array('batch_number', 'return_code', 'return_desc', 'refrence_no', 'dt', 'amount', 'sender_email', 'receiver_email', 'receiver_id', 'with_id', 'processor', 'processor_id'));
									
									$this->loadModel('Withdraw');
									$this->Withdraw->updateAll(
										array("Withdraw.status" => "'Done'", "Withdraw.complete_dt" => "'".date('Y-m-d H:i:s')."'"),
										array("Withdraw.with_id" => $myCustomField[1])
									);
									
									//Mail Code Start
									$this->loadModel('Member');
									$memberdata=$this->Member->find('first', array(
										'conditions' => array('Member.member_id' => $myCustomField[0]), 
										'fields' => array('Member.member_id', 'Member.f_name', 'Member.l_name', 'Member.user_name', 'Member.email','Member.unsubscribeemail')
										)
									);
									if($memberdata["Member"]['unsubscribeemail']==0)
									{
										if($this->sitesettingconfig[0]["sitesettings"]["reflinkiduser"]==1)
											$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'</a>';
										else
											$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'</a>';
										$this->template_mail($memberdata["Member"]['member_id'],$memberdata["Member"]['email'],'Successful Withdrawal Request Completion Notification',array('[SiteTitle]', '[UserName]', '[FirstName]', '[LastName]', '[Email]', '[MemberID]', '[Amount]', '[RefLink]', '[SiteUrl]', '[Signature]'),array($this->sitesettingconfig[0]["sitesettings"]["sitetitle"], $memberdata['Member']['user_name'], $memberdata['Member']['f_name'], $memberdata['Member']['l_name'],$memberdata["Member"]['email'],$memberdata["Member"]['member_id'], $this->currencydata['Currency']['prefix'].round($paymentAmount*$this->currencydata['Currency']['rate'],4)." ".$this->currencydata['Currency']['suffix'],$reflink,'<a href="'.$this->SITEURL.'">'.$this->SITEURL.'</a>',$this->sitesettingconfig[0]["sitesettings"]["signature"]));
									}
									//Mail Code Over
								}
								else 
								{
									if($processorsdata['Processor']['autowithdrow']==1)
									{
										$with_ids.=",".$myCustomField[1];
									}
								}
							}
							else
							{
								$data = array('batch_number' => $batchNumber, 'return_code' => $apiReturnCode, 'return_desc' => 'Transaction type is not masspay', 'refrence_no' => $transactionReferenceNumber, 'dt' => date('Y-m-d H:i:s'), 'amount' => $paymentAmount, 'sender_email' => $senderEmailAddress, 'receiver_email' => $receiverEmailAddress, 'receiver_id' => $myCustomField[0], 'with_id' => $myCustomField[1], 'processor' => 'Payza', 'processor_id' => $processorsdata['Processor']['id']);
								$this->Withdrawal_history->set($data);
								$this->Withdrawal_history->save($this->Withdrawal_history->data, false, array('batch_number', 'return_code', 'return_desc', 'refrence_no', 'dt', 'amount', 'sender_email', 'receiver_email', 'receiver_id', 'with_id', 'processor', 'processor_id'));
							}
						}
					}
				}//duplicate transection number
			}
		}
		if($with_ids!='')
		{
			$this->loadModel('Auto_withdraw');
			$autowithdata= $this->Auto_withdraw->read(null, 1);
			if($autowithdata['Auto_withdraw']['with_id']!=NULL || $autowithdata['Auto_withdraw']['with_id']!='')
				$with_ids=$autowithdata['Auto_withdraw']['with_id'].$with_ids;
			
			$this->Auto_withdraw->updateAll(
				array("Auto_withdraw.with_id" => "'".trim($with_ids,",")."'"),
				array("Auto_withdraw.id" => 1)
			);
		}
		//Masspay Code Over
    }//notify Over
	
}
?>