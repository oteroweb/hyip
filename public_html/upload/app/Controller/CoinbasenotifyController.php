<?php
class CoinbasenotifyController extends AppController 
{	
	public function index() 
	{
		App::import('Vendor', 'Coinbase/Coinbase');
		$this->loadModel('Processor');
		$processorsdata=$this->Processor->find('first', array(
			'conditions' => array('Processor.proc_name' => 'Coinbase', 'Processor.proc_status' => 1, 'Processor.isaddfund' => 1)
			)
		);
		
		$coinbase=new Coinbase();
		$apiKey = $this->dycriptstring($processorsdata['Processor']['api_name']);
		$apiSecret = $this->dycriptstring($processorsdata['Processor']['api_password']);
		$testmode=$processorsdata['Processor']['testmode'];
		$configuration=$coinbase->callbackauth($apiKey,$apiSecret,$testmode);
		$client = $coinbase->createclient($configuration);
		$account = $client->getPrimaryAccount();
		$raw_body = file_get_contents('php://input');
		if(isset($_SERVER['HTTP_CB_SIGNATURE']) && $_SERVER['HTTP_CB_SIGNATURE']!="")
		{
			$signature = $_SERVER['HTTP_CB_SIGNATURE'];
		}
		else
		{
			$signature="";
		}
		if(isset($raw_body) && isset($signature) && $signature!="")
		{
			
			$responsedata=json_decode($raw_body,true);
			$authenticity = $client->verifyCallback($raw_body, $signature);
			$a=var_dump($authenticity);
			$authenticity=true;
			
			$customdetail=@explode("|",$responsedata['data']['description']);
			$mid=$customdetail[0];
			$callmethod=$customdetail[1];
			$ip=$customdetail[2];
			
			$receiveremail = $responsedata['data']['bitcoin_address'];	
			$transactionstatus = $responsedata['data']['status'];
			$transaction_no=$responsedata['data']['code'];
			$paiddateobj=new DateTime($responsedata['data']['paid_at']);
			$transactiondate=$paiddateobj->format("Y-m-d H:i:s");
			$memberid = $mid;
			$pending_deposit_id=$customdetail[3];
			$pending_position_id=$customdetail[3];
			$paidamount = $responsedata['data']['amount']['amount'];
			$payeremail= "";
			
			//mail("prashant@maksa.in","IPN : Call Back (".$a.")",$raw_body);
			
			if($authenticity && $responsedata['data']['name']=="Add Funds")
			{
				$this->loadModel('Member');
				$memberdata=$this->Member->find('first', array(
					'conditions' => array('Member.member_id' => $memberid), 
					'fields' => array('Member.member_id', 'Member.f_name', 'Member.l_name', 'Member.user_name', 'Member.email', 'Member.unsubscribeemail')
					)
				);
				$payeremail=$memberdata['Member']['email'];
				
				//Processer Debuge Code Start
				if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
				{
					/*$description="";
					foreach($responsedata as $key=>$value)
					{
						$description.=$key.' => '.$value.",\n";
					}*/
					$description=$raw_body;
					$this->loadModel('Processordebug');
					$data = array('memberid'=>$mid,'status' => $transactionstatus, 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $transaction_no, 'processor' => 'Coinbase','type'=>'fund','description' => trim($description), 'ipaddress' => $ip, 'pay_dt' => date('Y-m-d H:i:s'));
					$this->Processordebug->set($data);
					$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
				}
				//Processer Debuge Code Over
				if(count($processorsdata)==0)
				{exit;}
				//Setting information about the transaction
				//$paidamount = $_POST['ap_amount'];
				
				
				$this->loadModel('Pending_deposit');
				$pendingdepositdata=$this->Pending_deposit->find('first', array(
					'conditions' => array('Pending_deposit.id' => $pending_deposit_id, 'Pending_deposit.member_id' => $memberid, 'round(Pending_deposit.paidamount,2)' => $paidamount, 'Pending_deposit.processorid' => $processorsdata['Processor']['id'], 'Pending_deposit.ip_address' => $ip)
					)
				);
				
				if(count($pendingdepositdata)==0)
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingdeposit_id-'.$pending_deposit_id.' | p_type-fund', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Coinbase','processorid'=>$processorsdata['Processor']['id'], 'ptype' => 'fund','paymentmethod'=>'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype','paymentmethod', 'ip_add'));
					
					$this->Pending_deposit->updateAll(
						array("Pending_deposit.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Coinbase,ptype-fund\n')"),
						array("Pending_deposit.id" => $pending_deposit_id)
					);
					exit;
				}
				
				if($paidamount<=0)
					$paidamount=0;
				if($pending_reason=='')
					$pending_reason='-';
				if($transaction_no=='')
					$transaction_no='-';
				
				
				
				$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
				$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
				$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
				
				$this->loadModel('Member_fee_payment');
				$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
					'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no,'Member_fee_payment.transaction_no != ' => '', 'Member_fee_payment.processor' => 'Coinbase')
					)
				);
				if($member_fee_paymentcounter>0)
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Coinbase','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund','paymentmethod'=>'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email','paymentmethod', 'processor','processorid', 'fees', 'ptype', 'ip_add'));
				}
				elseif($responsedata['data']['amount']['currency']!="USD")
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Currency Changed By User', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Coinbase','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid','paymentmethod', 'fees', 'ptype', 'ip_add'));
				}
				else
				{
					if ($transactionstatus == "paid") 
					{
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => '-', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Coinbase','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod','processorid', 'fees', 'ptype', 'ip_add'));
						
						//Add Fund
						if($totaltamount>0)
						{
							if($this->sitesettingconfig[0]["sitesettings"]['balance_type']==1)
							{
								$this->Member->updateAll(
									array("Member.cash_balance" => 'cash_balance+'.round($pendingdepositdata['Pending_deposit']['amount'],2)),
									array("Member.member_id" => $memberid)
								);
							}
							else
							{
								$this->Member->updateAll(
									array("Member.cash_".$processorsdata['Processor']['id'] => 'cash_'.$processorsdata['Processor']['id'].'+'.round($pendingdepositdata['Pending_deposit']['amount'],2)),
									array("Member.member_id" => $memberid)
								);
							}
						}
						
						$this->Pending_deposit->delete($pending_deposit_id);
						
						//mail to member
						$sitetitle=$this->sitesettingconfig[0]["sitesettings"]["sitetitle"];
						if($memberdata['Member']['unsubscribeemail']==0)
						{
							if($this->sitesettingconfig[0]["sitesettings"]["reflinkiduser"]==1)
								$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'</a>';
							else
								$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'</a>';
							$this->template_mail($memberdata['Member']['member_id'],$memberdata['Member']['email'],'Successful Add Fund Notification For The Member',array('[SiteTitle]','[UserName]', '[FirstName]', '[LastName]','[Email]','[MemberID]', '[Amount]', '[Processor]','[RefLink]','[SiteUrl]','[Signature]'),array($sitetitle, $memberdata["Member"]['user_name'], $memberdata["Member"]['f_name'], $memberdata["Member"]['l_name'],$memberdata['Member']['email'],$memberdata['Member']['member_id'], $this->currencydata['Currency']['prefix'].round($paidamount*$this->currencydata['Currency']['rate'],4), 'Coinbase',$reflink,'<a href="'.$this->SITEURL.'">'.$this->SITEURL.'</a>',$this->sitesettingconfig[0]["sitesettings"]["signature"]));
						}
					}
					else 
					{
						// Transaction was cancelled or an incorrect status was returned.
						// Take appropriate action.
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Transaction was cancelled or an incorrect status was returned', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Coinbase','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount','paymentmethod', 'receiver_email', 'processor','processorid', 'fees', 'ptype', 'ip_add'));
					}
				}//duplicate Transaction number
			}//Coinbase Over
			//Add Fund Code Over
			
			if($authenticity && ($responsedata['data']['name']=="Purchase Position" || $responsedata['data']['name']=="Purchase Position Fee"))
			{
				$this->loadModel('Member');
				$memberdata=$this->Member->find('first', array(
					'conditions' => array('Member.member_id' => $memberid), 
					'fields' => array('Member.member_id', 'Member.f_name', 'Member.l_name', 'Member.user_name', 'Member.email', 'Member.unsubscribeemail')
					)
				);
				$payeremail=$memberdata['Member']['email'];
				
				//Processer Debuge Code Start
				if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
				{
					$description=$raw_body;
					$this->loadModel('Processordebug');
					$data = array('memberid'=>$mid,'status' => $transactionstatus, 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $transaction_no, 'processor' => 'Coinbase','type'=>'position','description' => trim($description), 'ipaddress' => $ip, 'pay_dt' => date('Y-m-d H:i:s'));
					$this->Processordebug->set($data);
					$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
				}
				//Processer Debuge Code Over
				
				
				if(count($processorsdata)==0)
				{exit;}
				
				
				$this->loadModel('Pending_position');
				$pendingpositiondata=$this->Pending_position->find('first', array(
					'conditions' => array('Pending_position.id' => $pending_position_id, 'Pending_position.member_id' => $memberid, 'round(Pending_position.paidamount,2)' => $paidamount, 'Pending_position.processor' => 'Coinbase', 'Pending_position.ip_address' => $ip)
					)
				);
				$discount=$pendingpositiondata['Pending_position']['discount'];
							
				if(count($pendingpositiondata)==0)
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingposition_id-'.$pending_position_id.' | p_type-position', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Coinbase', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype', 'paymentmethod', 'ip_add'));
					
					$this->Pending_position->updateAll(
						array("Pending_position.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Coinbase,ptype-position,paymentmethod-processor\n')"),
						array("Pending_position.id" => $pending_position_id)
					);			
					exit;
				}
				
				if($paidamount<=0)
					$paidamount=0;
				if($pending_reason=='')
					$pending_reason='-';
				if($transaction_no=='')
					$transaction_no='-';
				
				
				$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
				$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
				$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
				
				$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Completed', 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Coinbase', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>0, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id, 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_position']['amount']-$pendingpositiondata['Pending_position']['discount'])));
				
				$this->loadModel('Member_fee_payment');
				$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
					'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'Coinbase')
					)
				);
				if($member_fee_paymentcounter>0)
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Coinbase', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
					
				}
				else
				{
					if ($transactionstatus == "paid") 
					{
						//Member Data Start
						//$this->loadModel('Member');
						$MemberData = $this->Member->read(null,$memberid);
						//Member Data Over
						
						//Add Position Start
						$ControllerShort=substr($callmethod, 0, strlen($callmethod)-1);
						App::import('Controller', $ControllerShort);
						$ControllerName=$ControllerShort."Controller";
						$ControllerObj = new $ControllerName;
						$FunctionName="AddPosition".str_replace($ControllerShort, "", $callmethod);
						
						$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields, 0, $this->sitesettingconfig, $this->currencydata);
						
						//Delete Pending Position Record
						$this->Pending_position->delete($pending_position_id);
						//Add Position Over
					}
					else 
					{
						// Transaction was cancelled or an incorrect status was returned.
						// Take appropriate action.
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Transaction was cancelled or an incorrect status was returned', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Coinbase', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
					}
					
				
				}//duplicate Transaction number
			}
			
			//Common Purchase Code Start
			
			if($authenticity && $responsedata['data']['name']=="Purchase Payment")//Coinbase
			{
				$ControllerShort=substr($callmethod, 0, strlen($callmethod)-1);
				$mem_ppid[0] = $memberid;
				$mem_ppid[1] = $pending_deposit_id;
				
				$payeremail=$this->getmemberemail($memberid);
				
				//Processer Debuge Code Start
				if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
				{
					$description=$raw_body;
					$this->loadModel('Processordebug');
					$data = array('memberid'=>$mid,'status' => $transactionstatus, 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $transaction_no, 'processor' => 'Coinbase','type'=>$ControllerShort,'description' => trim($description), 'ipaddress' => $ip, 'pay_dt' => date('Y-m-d H:i:s'));
					
					$this->Processordebug->set($data);
					$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
				}
				//Processer Debuge Code Over
				
				
				if(count($processorsdata)==0)
				{exit;}
				
				$this->loadModel('Pending_plan');
				$pendingpositiondata=$this->Pending_plan->find('first', array(
					'conditions' => array('Pending_plan.id' => $pending_position_id, 'Pending_plan.member_id' => $memberid, 'round(Pending_plan.paidamount,2)' => $paidamount, 'Pending_plan.processor' => 'Coinbase', 'Pending_plan.ip_address' => $ip)
					)
				);
				$discount=$pendingpositiondata['Pending_plan']['discount'];
							
				if(count($pendingpositiondata)==0)
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingplan_id-'.$pending_position_id.' | p_type-'.$ControllerShort, 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Coinbase', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype', 'ip_add'));
					
					$this->Pending_plan->updateAll(
						array("Pending_plan.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Coinbase,ptype-".$ControllerShort.",paymentmethod-processor\n')"),
						array("Pending_plan.id" => $pending_position_id)
					);			
					exit;
				}
				
				if($paidamount<=0)
					$paidamount=0;
				if($pending_reason=='')
					$pending_reason='-';
				if($transaction_no=='')
					$transaction_no='-';
				
				$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
				$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
				$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
				
				$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Coinbase', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>0, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id,'advertisementsetting'=>$pendingpositiondata['Pending_plan']['advertisementsetting'], 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_plan']['amount']-$pendingpositiondata['Pending_plan']['discount'])));
				
				$this->loadModel('Member_fee_payment');
				$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
					'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'Coinbase')
					)
				);
				if($member_fee_paymentcounter>0)
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Coinbase', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
				}
				else
				{
					if ($transactionstatus == "paid") 
					{
						//Member Data Start
						$this->loadModel('Member');
						$MemberData = $this->Member->read(null,$memberid);
						//Member Data Over
						
						//Add Position Start
						$ControllerShort=substr($callmethod, 0, strlen($callmethod)-1);
						App::import('Controller', $ControllerShort);
						$ControllerName=$ControllerShort."Controller";
						$ControllerObj = new $ControllerName;
						$FunctionName="AddPlan".str_replace($ControllerShort, "", $callmethod);
						
						$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields,$this->sitesettingconfig,$this->currencydata);
						
						//Delete Pending Position Record
						$this->Pending_plan->delete($pending_position_id);
						//Add Position Over
					}
					else 
					{
						// Transaction was cancelled or an incorrect status was returned.
						// Take appropriate action.
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Transaction was cancelled or an incorrect status was returned', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Coinbase', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
					}
				}//duplicate transection number
			
			}//Okpay Over
			
			//Common Purchase Code Over
			
		}
		
    }//notify Over
	
	function getmemberemail($memberid)
	{
		$this->loadModel('Member');
		$memberdata=$this->Member->find('first', array(
			'conditions' => array('Member.member_id' => $memberid), 
			'fields' => array('Member.member_id', 'Member.f_name', 'Member.l_name', 'Member.user_name', 'Member.email', 'Member.unsubscribeemail')
			)
		);
		return $memberdata['Member']['email'];
	}
}
?>