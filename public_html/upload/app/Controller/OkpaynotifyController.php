<?php
class OkpaynotifyController extends AppController 
{
	public function index() 
	{
    	if(isset($_POST['ok_txn_status']) && isset($_POST['ok_receiver_wallet']) && isset($_POST['ok_txn_kind']) && $_POST['ok_txn_kind']=="payment_link" && isset($_POST['ok_item_1_custom_2_value']) && $_POST['ok_item_1_custom_2_value']=='Add Funds')//Okpay 4
		{
			$mem_ppid=@explode("-",$_POST['ok_invoice']);
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'Okpay', 'Processor.proc_status' => 1, 'Processor.isaddfund' => 1)
				)
			);
			if(count($processorsdata)==0)
			{exit;}
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$mem_ppid[0],'status' => 'Completed', 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['ok_txn_id'], 'processor' => 'Okpay','type'=>'fund','description' => trim($description), 'ipaddress' => $_POST['ok_item_1_custom_1_value'], 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			
			$receiveremail = $_POST['ok_receiver_email'];	
			$transactionstatus = $_POST['ok_txn_status'];   	//completed,failed,pending,reversed
			$pending_reason=$_POST['ok_txn_pending_reason']; 
			$transaction_no = $_POST['ok_txn_id'];
			$payeremail= $_POST['ok_payer_id'];
			$memberid =$mem_ppid[0];
			$paidamount = $_POST['ok_item_1_price'];
			$netamount=$_POST['ok_item_1_price'];	
			
			$ip=$_POST['ok_item_1_custom_1_value'];
			$pending_deposit_id=$mem_ppid[1];
			
			$this->loadModel('Pending_deposit');
			$pendingdepositdata=$this->Pending_deposit->find('first', array(
				'conditions' => array('Pending_deposit.id' => $pending_deposit_id, 'Pending_deposit.member_id' => $memberid, 'round(Pending_deposit.paidamount,2)' => $paidamount, 'Pending_deposit.processorid' => $processorsdata['Processor']['id'], 'Pending_deposit.ip_address' => $ip)
				)
			);
			
			if(count($pendingdepositdata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingdeposit_id-'.$pending_deposit_id.' | p_type-fund', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay','paymentmethod'=>'processor', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => 'fund', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid','paymentmethod', 'ptype', 'ip_add'));
				
				$this->Pending_deposit->updateAll(
					array("Pending_deposit.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Okpay,ptype-fund\n')"),
					array("Pending_deposit.id" => $pending_deposit_id)
				);
				exit;
			}
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];

			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no,'Member_fee_payment.transaction_no != ' => '', 'Member_fee_payment.processor' => 'Okpay')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay','paymentmethod'=>'processor', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid','paymentmethod', 'fees', 'ptype', 'ip_add'));
			}
			elseif($_POST['ok_txn_currency']!="USD")
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Currency Changed By User', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay','paymentmethod'=>'processor', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid','paymentmethod', 'fees', 'ptype', 'ip_add'));
			}
			else
			{
				// Read the post from OKPAY and add 'ok_verify' 
				$request = 'ok_verify=true'; 
				foreach ($_POST as $key => $value) 
				{
					$value = urlencode(stripslashes($value));
					$request .= "&$key=$value";
				}
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, 'https://www.okpay.com/ipn-verify.html');
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				$result = curl_exec($ch);
				curl_close($ch);
				
				if ($result == 'VERIFIED')
				{
					if($transactionstatus=="completed")
					{ 
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => '-', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay','paymentmethod'=>'processor', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid','paymentmethod', 'fees', 'ptype', 'ip_add'));
						
						$this->loadModel('Member');
						$memberdata=$this->Member->find('first', array(
							'conditions' => array('Member.member_id' => $memberid), 
							'fields' => array('Member.member_id', 'Member.f_name', 'Member.l_name', 'Member.user_name', 'Member.email','Member.unsubscribeemail')
							)
						);
						
						//Add Fund
						if($totaltamount>0)
						{
							if($this->sitesettingconfig[0]["sitesettings"]['balance_type']==1)
							{
								$this->Member->updateAll(
									array("Member.cash_balance" => 'cash_balance+'.$totaltamount),
									array("Member.member_id" => $memberid)
								);
							}
							else
							{
								$this->Member->updateAll(
									array("Member.cash_".$processorsdata['Processor']['id'] => 'cash_'.$processorsdata['Processor']['id'].'+'.$totaltamount),
									array("Member.member_id" => $memberid)
								);
							}
						}
						
						// Status Change In Pending Deposit
						$this->Pending_deposit->delete($pending_deposit_id);
						
						//mail to member
						$sitetitle=$this->sitesettingconfig[0]["sitesettings"]["sitetitle"];
						if($memberdata['Member']['unsubscribeemail']==0)
						{
							if($this->sitesettingconfig[0]["sitesettings"]["reflinkiduser"]==1)
								$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'</a>';
							else
								$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'</a>';
							$this->template_mail($memberdata['Member']['member_id'],$memberdata['Member']['email'],'Successful Add Fund Notification For The Member',array('[SiteTitle]','[UserName]', '[FirstName]', '[LastName]','[Email]','[MemberID]', '[Amount]', '[Processor]','[RefLink]','[SiteUrl]','[Signature]'),array($sitetitle, $memberdata["Member"]['user_name'], $memberdata["Member"]['f_name'], $memberdata["Member"]['l_name'],$memberdata['Member']['email'],$memberdata['Member']['member_id'], $this->currencydata['Currency']['prefix'].round($paidamount*$this->currencydata['Currency']['rate'],4), 'Okpay',$reflink,'<a href="'.$this->SITEURL.'">'.$this->SITEURL.'</a>',$this->sitesettingconfig[0]["sitesettings"]["signature"]));
						}
					}	
					else
					{
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'CANCELED', 'pending_reason' => 'Payment verified but transaction status not completed. its -'.$transactionstatus, 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay','paymentmethod'=>'processor', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid','paymentmethod', 'fees', 'ptype', 'ip_add'));
					}
				}
				else 
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'CANCELED', 'pending_reason' => 'Unverified Payment.', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay','paymentmethod'=>'processor', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'paymentmethod','fees', 'ptype', 'ip_add'));
				}
				
			}//duplicate transection number
		
		}//Okpay Over
		//Add Fund Code Over
		
		//Purchase Postion Code Start
		if((isset($_POST['ok_txn_status']) && isset($_POST['ok_receiver_wallet']) && $_POST['ok_txn_kind']=="payment_link") && ($_POST['ok_item_1_custom_2_value']=='Purchase Position'))//Okpay
		{
			$mem_ppid=@explode("-",$_POST['ok_invoice']);
			
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'Okpay', 'Processor.proc_status' => 1, 'Processor.receivefund' => 1)
				)
			);
			
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$mem_ppid[0],'status' => 'Completed', 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['ok_txn_id'], 'processor' => 'Okpay','type'=>'position','description' => trim($description), 'ipaddress' => $_POST['ok_item_1_custom_1_value'], 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			
			if(count($processorsdata)==0)
			{exit;}
			
			$receiveremail = $_POST['ok_receiver_email'];	
			$transactionstatus = $_POST['ok_txn_status'];   	//completed,failed,pending,reversed
			$pending_reason=$_POST['ok_txn_pending_reason']; 
			$transaction_no = $_POST['ok_txn_id'];
			$payeremail= $_POST['ok_payer_id'];
			$memberid =$mem_ppid[0];
			$paidamount = $_POST['ok_item_1_price'];
			$netamount=$_POST['ok_item_1_price'];
			
			$ip=$_POST['ok_item_1_custom_1_value'];
			$pending_position_id=$mem_ppid[1];
			
			$this->loadModel('Pending_position');
			$pendingpositiondata=$this->Pending_position->find('first', array(
				'conditions' => array('Pending_position.id' => $pending_position_id, 'Pending_position.member_id' => $memberid, 'round(Pending_position.paidamount,2)' => $paidamount, 'Pending_position.processor' => 'Okpay', 'Pending_position.ip_address' => $ip)
				)
			);
			$discount=$pendingpositiondata['Pending_position']['discount'];
						
			if(count($pendingpositiondata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingposition_id-'.$pending_position_id.' | p_type-position', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype', 'ip_add'));
				
				$this->Pending_position->updateAll(
					array("Pending_position.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Okpay,ptype-position,paymentmethod-processor\n')"),
					array("Pending_position.id" => $pending_position_id)
				);			
				exit;
			}
			
			if($paidamount<=0)
				$paidamount=0;
			if($pending_reason=='')
				$pending_reason='-';
			if($transaction_no=='')
				$transaction_no='-';
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>0, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id, 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_position']['amount']-$pendingpositiondata['Pending_position']['discount'])));
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'Okpay')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
			}
			else
			{
				// Read the post from OKPAY and add 'ok_verify' 
				$request = 'ok_verify=true'; 
				foreach ($_POST as $key => $value) 
				{
					$value = urlencode(stripslashes($value));
					$request .= "&$key=$value";
				}
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, 'https://www.okpay.com/ipn-verify.html');
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				$result = curl_exec($ch);
				curl_close($ch);
				
				if ($result == 'VERIFIED')
				{
					if($transactionstatus=="completed")
					{
						//Member Data Start
						$this->loadModel('Member');
						$MemberData = $this->Member->read(null,$memberid);
						//Member Data Over
						
						//Add Position Start
						$ControllerShort=substr($_POST['ok_item_1_custom_1_title'], 0, strlen($_POST['ok_item_1_custom_1_title'])-1);
						App::import('Controller', $ControllerShort);
						$ControllerName=$ControllerShort."Controller";
						$ControllerObj = new $ControllerName;
						$FunctionName="AddPosition".str_replace($ControllerShort, "", $_POST['ok_item_1_custom_1_title']);
						
						$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields, 0, $this->sitesettingconfig, $this->currencydata);
						
						//Delete Pending Position Record
						$this->Pending_position->delete($pending_position_id);
						//Add Position Over
					}	
					else
					{
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'CANCELED', 'pending_reason' => 'Okpay VERIFIED But Status Does Not Match To completed. its -'.$transactionstatus, 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'ip_add'));
					}
				}
				else 
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'CANCELED', 'pending_reason' => 'Okpay VERIFIED Code Does Not Match: the payment has been altered', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'ip_add'));
				}
				
			}//duplicate transection number
		
		}//Okpay Over
		
		//Purchase Postion Code Over
		
		//Common Purchase Code Start
		if((isset($_POST['ok_txn_status']) && isset($_POST['ok_receiver_wallet']) && $_POST['ok_txn_kind']=="payment_link") && ($_POST['ok_item_1_custom_2_value']=='Purchase Payment'))//Okpay
		{
			$ControllerShort=substr($_POST['ok_item_1_custom_1_title'], 0, strlen($_POST['ok_item_1_custom_1_title'])-1);
			$mem_ppid=@explode("-",$_POST['ok_invoice']);
			
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'Okpay', 'Processor.proc_status' => 1, 'Processor.receivefund' => 1)
				)
			);
			
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$mem_ppid[0],'status' => 'Completed', 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['ok_txn_id'], 'processor' => 'Okpay','type'=>$ControllerShort,'description' => trim($description), 'ipaddress' => $_POST['ok_item_1_custom_1_value'], 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			
			if(count($processorsdata)==0)
			{exit;}
			
			$receiveremail = $_POST['ok_receiver_email'];	
			$transactionstatus = $_POST['ok_txn_status'];   	//completed,failed,pending,reversed
			$pending_reason=$_POST['ok_txn_pending_reason']; 
			$transaction_no = $_POST['ok_txn_id'];
			$payeremail= $_POST['ok_payer_id'];
			$memberid =$mem_ppid[0];
			$paidamount = $_POST['ok_item_1_price'];
			$netamount=$_POST['ok_item_1_price'];
			
			$ip=$_POST['ok_item_1_custom_1_value'];
			$pending_position_id=$mem_ppid[1];
			
			$this->loadModel('Pending_plan');
			$pendingpositiondata=$this->Pending_plan->find('first', array(
				'conditions' => array('Pending_plan.id' => $pending_position_id, 'Pending_plan.member_id' => $memberid, 'round(Pending_plan.paidamount,2)' => $paidamount, 'Pending_plan.processor' => 'Okpay', 'Pending_plan.ip_address' => $ip)
				)
			);
			$discount=$pendingpositiondata['Pending_plan']['discount'];
						
			if(count($pendingpositiondata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingplan_id-'.$pending_position_id.' | p_type-'.$ControllerShort, 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype', 'ip_add'));
				
				$this->Pending_plan->updateAll(
					array("Pending_plan.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Okpay,ptype-".$ControllerShort.",paymentmethod-processor\n')"),
					array("Pending_plan.id" => $pending_position_id)
				);			
				exit;
			}
			
			if($paidamount<=0)
				$paidamount=0;
			if($pending_reason=='')
				$pending_reason='-';
			if($transaction_no=='')
				$transaction_no='-';
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>0, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id,'advertisementsetting'=>$pendingpositiondata['Pending_plan']['advertisementsetting'], 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_plan']['amount']-$pendingpositiondata['Pending_plan']['discount'])));
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'Okpay')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
			}
			else
			{
				// Read the post from OKPAY and add 'ok_verify' 
				$request = 'ok_verify=true'; 
				foreach ($_POST as $key => $value) 
				{
					$value = urlencode(stripslashes($value));
					$request .= "&$key=$value";
				}
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, 'https://www.okpay.com/ipn-verify.html');
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				$result = curl_exec($ch);
				curl_close($ch);
				
				if ($result == 'VERIFIED')
				{
					if($transactionstatus=="completed")
					{
						//Member Data Start
						$this->loadModel('Member');
						$MemberData = $this->Member->read(NULL, $memberid);
						//Member Data Over
						
						//Add Position Start
						App::import('Controller', $ControllerShort);
						$ControllerName=$ControllerShort."Controller";
						$ControllerObj = new $ControllerName;
						$FunctionName="AddPlan".str_replace($ControllerShort, "", $_POST['ok_item_1_custom_1_title']);
						
						$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields,$this->sitesettingconfig,$this->currencydata);
						
						//Delete Pending Position Record
						$this->Pending_plan->delete($pending_position_id);
						//Add Position Over
					}	
					else
					{
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'CANCELED', 'pending_reason' => 'Okpay VERIFIED But Status Does Not Match To completed:'.$transactionstatus, 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'ip_add'));
					}
				}
				else 
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'CANCELED', 'pending_reason' => 'Okpay VERIFIED Code Does Not Match: the payment has been altered', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Okpay', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'ip_add'));
				}
				
			}//duplicate transection number
		
		}//Okpay Over
		
		//Common Purchase Code Over
		
    }//notify Over
	
}
?>