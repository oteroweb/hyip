<?php
class PayeernotifyController extends AppController 
{
	public function index() 
	{
    	if (isset($_POST["m_operation_id"]) && isset($_POST["m_sign"]))//Payeer 8
		{
			$receiveremail = $_POST['m_shop'];	
			$transactionstatus = $_POST['m_status'];
			$pending_reason=""; 
			$transaction_no = $_POST['m_operation_id'];
			$transactiondate = date('Y-m-d');
			$payeremail = "-";
			$memberid = $_POST['m_orderid'];
			$paidamount = $_POST['m_amount'];
			$netamount = $_POST['m_amount'];
			
			$dese=explode(',',base64_decode($_POST['m_desc']));
			$ip = $dese[2];
			
			if($dese[0]=="Add Funds")
			{
				$pending_deposit_id = $dese[1];
				$this->loadModel('Processor');
				$processorsdata=$this->Processor->find('first', array(
					'conditions' => array('Processor.proc_name' => 'Payeer', 'Processor.proc_status' => 1, 'Processor.isaddfund' => 1)
					)
				);
				//Processer Debuge Code Start
				if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
				{
					$description="";
					foreach($_POST as $key=>$value)
					{
						$description.=$key.' => '.$value.",\n";
					}
					$this->loadModel('Processordebug');
					$data = array('memberid'=>$_POST['m_orderid'],'status' => $_POST['m_status'], 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['m_operation_id'], 'processor' => 'Payeer','type'=>'fund','description' => trim($description), 'ipaddress' => '-', 'pay_dt' => date('Y-m-d H:i:s'));
					$this->Processordebug->set($data);
					$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
				}
				//Processer Debuge Code Over
				if(count($processorsdata)==0)
				{exit;}
				$this->loadModel('Pending_deposit');
				$pendingdepositdata=$this->Pending_deposit->find('first', array(
					'conditions' => array('Pending_deposit.id' => $pending_deposit_id, 'Pending_deposit.member_id' => $memberid, 'round(Pending_deposit.paidamount,2)' => $paidamount, 'Pending_deposit.processorid' => $processorsdata['Processor']['id'], 'Pending_deposit.ip_address' => $ip)
					)
				);
				
				if(count($pendingdepositdata)==0)
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingdeposit_id-'.$pending_deposit_id.' | p_type-fund', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer','processorid'=>$processorsdata['Processor']['id'], 'ptype' => 'fund', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'ptype', 'ip_add'));
					
					$this->Pending_deposit->updateAll(
						array("Pending_deposit.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Payeer,ptype-fund\n')"),
						array("Pending_deposit.id" => $pending_deposit_id)
					);
					
					echo $_POST['m_orderid']."|error";				
					exit;
				}
				
				$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
				$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
				$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
				
				$this->loadModel('Member_fee_payment');
				$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
					'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no,'Member_fee_payment.transaction_no != ' => '', 'Member_fee_payment.processor' => 'Payeer')
					)
				);
				
				if($member_fee_paymentcounter>0)
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer','paymentmethod'=>'processor', 'processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod', 'processorid', 'fees', 'ptype', 'ip_add'));
					
					echo $_POST['m_orderid']."|error"; exit;
				}
				elseif($_POST['m_curr']!="USD")
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer','paymentmethod'=>'processor', 'processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod', 'processorid', 'fees', 'ptype', 'ip_add'));
					
					echo $_POST['m_orderid']."|error"; exit;
				}
				else
				{
					$m_key = $this->dycriptstring($processorsdata['Processor']['api_password']);
					$arHash = array($_POST['m_operation_id'],
							$_POST['m_operation_ps'],
							$_POST['m_operation_date'],
							$_POST['m_operation_pay_date'],
							$_POST['m_shop'],
							$_POST['m_orderid'],
							$_POST['m_amount'],
							$_POST['m_curr'],
							$_POST['m_desc'],
							$_POST['m_status'],
							$m_key);
					$sign_hash = strtoupper(hash('sha256', implode(":", $arHash)));
					if ($_POST["m_sign"] == $sign_hash && $_POST['m_status'] == "success")
					{
						if($_POST['m_curr']=='USD')
						{
							$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => '-', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer','paymentmethod'=>'processor', 'processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
							$this->Member_fee_payment->set($data);
							$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount','paymentmethod', 'receiver_email', 'processor', 'processorid', 'fees', 'ptype', 'ip_add'));
							
							$this->loadModel('Member');
							$memberdata=$this->Member->find('first', array(
								'conditions' => array('Member.member_id' => $memberid), 
								'fields' => array('Member.member_id', 'Member.f_name', 'Member.l_name', 'Member.user_name', 'Member.email', 'Member.unsubscribeemail')
								)
							);
							
							//Add Fund
							if($totaltamount>0)
							{
								if($this->sitesettingconfig[0]["sitesettings"]['balance_type']==1)
								{
									$this->Member->updateAll(
										array("Member.cash_balance" => 'cash_balance+'.$pendingdepositdata['Pending_deposit']['amount']),
										array("Member.member_id" => $memberid)
									);
								}
								else
								{
									$this->Member->updateAll(
										array("Member.cash_".$processorsdata['Processor']['id'] => 'cash_'.$processorsdata['Processor']['id'].'+'.$pendingdepositdata['Pending_deposit']['amount']),
										array("Member.member_id" => $memberid)
									);
								}
							}
							
							$this->Pending_deposit->delete($pending_deposit_id);
							
							//mail to member
							$sitetitle=$this->sitesettingconfig[0]["sitesettings"]["sitetitle"];
							if($memberdata['Member']['unsubscribeemail']==0)
							{
								if($this->sitesettingconfig[0]["sitesettings"]["reflinkiduser"]==1)
									$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'</a>';
								else
									$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'</a>';
								$this->template_mail($memberdata['Member']['member_id'],$memberdata['Member']['email'],'Successful Add Fund Notification For The Member',array('[SiteTitle]','[UserName]', '[FirstName]', '[LastName]','[Email]','[MemberID]', '[Amount]', '[Processor]','[RefLink]','[SiteUrl]','[Signature]'),array($sitetitle, $memberdata["Member"]['user_name'], $memberdata["Member"]['f_name'], $memberdata["Member"]['l_name'],$memberdata['Member']['email'],$memberdata['Member']['member_id'], $this->currencydata['Currency']['prefix'].round($paidamount*$this->currencydata['Currency']['rate'],4), 'Payeer',$reflink,'<a href="'.$this->SITEURL.'">'.$this->SITEURL.'</a>',$this->sitesettingconfig[0]["sitesettings"]["signature"]));
							}
							
							echo $_POST['m_orderid']."|success"; exit;
						}
						else
						{
							$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Currency Format Does Not Match', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer','paymentmethod'=>'processor', 'processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
							$this->Member_fee_payment->set($data);
							$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod', 'processorid', 'fees', 'ptype', 'ip_add'));
							
							echo $_POST['m_orderid']."|error"; exit;
						}
					}
					else
					{
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Payeer Verification String Does Not Match : The payment has been altered', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer','paymentmethod'=>'processor', 'processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod', 'processorid', 'fees', 'ptype', 'ip_add'));
						
						echo $_POST['m_orderid']."|error"; exit;
					}
					
				}//duplicate Transaction number
			}
			elseif($dese[0]=="Purchase Position")
			{
				$this->loadModel('Processor');
				$processorsdata=$this->Processor->find('first', array(
					'conditions' => array('Processor.proc_name' => 'Payeer', 'Processor.proc_status' => 1, 'Processor.receivefund' => 1)
					)
				);
				
				//Processer Debuge Code Start
				if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
				{
					$description="";
					foreach($_POST as $key=>$value)
					{
						$description.=$key.' => '.$value.",\n";
					}
					$this->loadModel('Processordebug');
					$data = array('memberid'=>$_POST['m_orderid'],'status' => $_POST['m_status'], 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['m_operation_id'], 'processor' => 'Payeer','type'=>'fund','description' => trim($description), 'ipaddress' => '-', 'pay_dt' => date('Y-m-d H:i:s'));
					$this->Processordebug->set($data);
					$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
				}
				//Processer Debuge Code Over
				if(count($processorsdata)==0)
				{exit;}
				$pending_position_id = $dese[1];
				
				$this->loadModel('Pending_position');
				$pendingpositiondata=$this->Pending_position->find('first', array(
					'conditions' => array('Pending_position.id' => $pending_position_id, 'Pending_position.member_id' => $memberid, 'round(Pending_position.paidamount)' => $paidamount, 'Pending_position.processor' => 'Payeer', 'Pending_position.ip_address' => $ip)
					)
				);
				$discount=$pendingpositiondata['Pending_position']['discount'];
				
				if(count($pendingpositiondata)==0)
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingposition_id-'.$pending_position_id.' | p_type-position', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'ptype', 'paymentmethod', 'ip_add'));
					
					$this->Pending_position->updateAll(
						array("Pending_position.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Payeer,ptype-position,paymentmethod-processor\n')"),
						array("Pending_position.id" => $pending_position_id)
					);
					
					echo $_POST['m_orderid']."|error";			
					exit;
				}
				$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
				$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
				$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
				$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>0, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id, 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_position']['amount']-$pendingpositiondata['Pending_position']['discount'])));
				
				$this->loadModel('Member_fee_payment');
				$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
					'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'Payeer')
					)
				);
				if($member_fee_paymentcounter>0)
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount' => $discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
					
					echo $_POST['m_orderid']."|error";
					exit;
				}
				else
				{
					$m_key = $this->dycriptstring($processorsdata['Processor']['api_password']);
					$arHash = array($_POST['m_operation_id'],
							$_POST['m_operation_ps'],
							$_POST['m_operation_date'],
							$_POST['m_operation_pay_date'],
							$_POST['m_shop'],
							$_POST['m_orderid'],
							$_POST['m_amount'],
							$_POST['m_curr'],
							$_POST['m_desc'],
							$_POST['m_status'],
							$m_key);
					$sign_hash = strtoupper(hash('sha256', implode(":", $arHash)));
					if ($_POST["m_sign"] == $sign_hash && $_POST['m_status'] == "success")
					{
						if($_POST['m_curr']=='USD')
						{
							//Member Data Start
							$this->loadModel('Member');
							$MemberData = $this->Member->read(null,$memberid);
							//Member Data Over
							
							//Add Position Start
							$ControllerShort=substr($dese[3], 0, strlen($dese[3])-1);
							App::import('Controller', $ControllerShort);
							$ControllerName=$ControllerShort."Controller";
							$ControllerObj = new $ControllerName;
							$FunctionName="AddPosition".str_replace($ControllerShort, "", $dese[3]);
							
							$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields, 0, $this->sitesettingconfig,$this->currencydata);
							//Delete Pending Position Record
							$this->Pending_position->delete($pending_position_id);
							//Add Position Over
						
							echo $_POST['m_orderid']."|success"; exit;
						}
						else
						{
							$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Currency Format Does Not Match', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
							$this->Member_fee_payment->set($data);
							$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
							
							echo $_POST['m_orderid']."|error";
							exit;
						}
					}
					else
					{
						// Transaction was cancelled or an incorrect status was returned.
						// Take appropriate action.
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Payeer Verification String Does Not Match : The payment has been altered', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
						
						echo $_POST['m_orderid']."|error"; exit;
					}
				}//duplicate Transaction number
			}
			elseif($dese[0]=="Purchase Payment")
			{
				$ControllerShort=substr($dese[3], 0, strlen($dese[3])-1);
				//Processer Debuge Code Start
				$this->loadModel('Processor');
				$processorsdata=$this->Processor->find('first', array(
					'conditions' => array('Processor.proc_name' => 'Payeer', 'Processor.proc_status' => 1, 'Processor.receivefund' => 1)
				));
				if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
				{
					$description="";
					foreach($_POST as $key=>$value)
					{
						$description.=$key.' => '.$value.",\n";
					}
					$this->loadModel('Processordebug');
					$data = array('memberid'=>$_POST['m_orderid'],'status' => $_POST['m_status'], 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['m_operation_id'], 'processor' => 'Payeer', 'type'=>$ControllerShort, 'description' => trim($description), 'ipaddress' => '-', 'pay_dt' => date('Y-m-d H:i:s'));
					$this->Processordebug->set($data);
					$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
				}
				//Processer Debuge Code Over
				if(count($processorsdata)==0)
				{exit;}
				$pending_position_id = $dese[1];
				
				$this->loadModel('Pending_plan');
				$pendingpositiondata=$this->Pending_plan->find('first', array(
					'conditions' => array('Pending_plan.id' => $pending_position_id, 'Pending_plan.member_id' => $memberid, 'round(Pending_plan.paidamount,2)' => $paidamount, 'Pending_plan.processor' => 'Payeer', 'Pending_plan.ip_address' => $ip)
				));
				$discount=$pendingpositiondata['Pending_plan']['discount'];
				
				if(count($pendingpositiondata)==0)
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingplan_id-'.$pending_position_id.' | p_type-'.$ControllerShort, 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'ptype', 'paymentmethod', 'ip_add'));
					
					$this->Pending_plan->updateAll(
						array("Pending_plan.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Payeer,ptype-".$ControllerShort.",paymentmethod-processor\n')"),
						array("Pending_plan.id" => $pending_position_id)
					);
					
					echo $_POST['m_orderid']."|error";			
					exit;
				}
				$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
				$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
				$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
				$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>0, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id,'advertisementsetting'=>$pendingpositiondata['Pending_plan']['advertisementsetting'], 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_plan']['amount']-$pendingpositiondata['Pending_plan']['discount'])));
				
				$this->loadModel('Member_fee_payment');
				$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
					'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'Payeer')
				));
				if($member_fee_paymentcounter>0)
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount' => $discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
					
					echo $_POST['m_orderid']."|error"; exit;
				}
				else
				{
					$m_key = $this->dycriptstring($processorsdata['Processor']['api_password']);
					$arHash = array($_POST['m_operation_id'],
							$_POST['m_operation_ps'],
							$_POST['m_operation_date'],
							$_POST['m_operation_pay_date'],
							$_POST['m_shop'],
							$_POST['m_orderid'],
							$_POST['m_amount'],
							$_POST['m_curr'],
							$_POST['m_desc'],
							$_POST['m_status'],
							$m_key);
					$sign_hash = strtoupper(hash('sha256', implode(":", $arHash)));
					if ($_POST["m_sign"] == $sign_hash && $_POST['m_status'] == "success")
					{
						if($_POST['m_curr']=='USD')
						{
							//Member Data Start
							$this->loadModel('Member');
							$MemberData = $this->Member->read(NULL, $memberid);
							//Member Data Over
							
							//Add Position Start
							App::import('Controller', $ControllerShort);
							$ControllerName=$ControllerShort."Controller";
							$ControllerObj = new $ControllerName;
							$FunctionName="AddPlan".str_replace($ControllerShort, "", $dese[3]);
							
							$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields,$this->sitesettingconfig,$this->currencydata);
							
							//Delete Pending Position Record
							$this->Pending_plan->delete($pending_position_id);
							//Add Position Over
							
							echo $_POST['m_orderid']."|success"; exit;
						}
						else
						{
							$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Currency Format Does Not Match', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
							$this->Member_fee_payment->set($data);
							$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
							
							echo $_POST['m_orderid']."|error"; exit;
						}
					}
					else
					{
						// Transaction was cancelled or an incorrect status was returned.
						// Take appropriate action.
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Payeer Verification String Does Not Match : The payment has been altered', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Payeer', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
						
						echo $_POST['m_orderid']."|error"; exit;
					}
				}//duplicate Transaction number
			}
		}
		//Add Fund Code Over
	}//notify Over
	
}
?>