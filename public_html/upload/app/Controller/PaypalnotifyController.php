<?php
class PaypalnotifyController extends AppController 
{
	public function index() 
	{
    	if(isset($_POST['item_name']) && $_POST['item_name']=='Add Funds')//PayPal 2
		{
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'PayPal', 'Processor.proc_status' => 1, 'Processor.isaddfund' => 1)
				)
			);
			
			$receiveremail = $_POST['receiver_email'];
			$transactionstatus = $_POST['payment_status'];
			$pending_reason=$_POST['pending_reason']; 
			$transaction_no = $_POST['txn_id'];
			$payeremail= $_POST['payer_email'];
			$memberid = $_POST['item_number'];
			$paidamount = $_POST['payment_gross'];
			$customdetail=@explode("|",$_POST['custom']);
			$ip=$customdetail[0];
			$pending_deposit_id=$customdetail[1];
			$callmethod=$customdetail[2];
			
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$_POST['item_number'],'status' => $_POST['payment_status'], 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['txn_id'], 'processor' => 'PayPal','type'=>'fund','description' => trim($description), 'ipaddress' => $ip, 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			if(count($processorsdata)==0)
			{exit;}
			//Setting information about the transaction
			if($pending_reason=='')
				$pending_reason='-';
			if($transaction_no=='')
				$transaction_no='-';
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$this->loadModel('Pending_deposit');
			$pendingdepositdata=$this->Pending_deposit->find('first', array(
				'conditions' => array('Pending_deposit.id' => $pending_deposit_id, 'Pending_deposit.member_id' => $memberid, 'round(Pending_deposit.paidamount,2)' => $paidamount, 'Pending_deposit.processorid' => $processorsdata['Processor']['id'], 'Pending_deposit.ip_address' => $ip)
				)
			);
			
			if(count($pendingdepositdata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingdeposit_id-'.$pending_deposit_id.' | p_type-fund', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'PayPal','processorid'=>$processorsdata['Processor']['id'], 'ptype' => 'fund','paymentmethod'=>'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype','paymentmethod', 'ip_add'));
				
				$this->Pending_deposit->updateAll(
					array("Pending_deposit.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-PayPal,ptype-fund\n')"),
					array("Pending_deposit.id" => $pending_deposit_id)
				);
				exit;
			}
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no,'Member_fee_payment.transaction_no != ' => '', 'Member_fee_payment.processor' => 'PayPal')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'PayPal','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund','paymentmethod'=>'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email','paymentmethod', 'processor','processorid', 'fees', 'ptype', 'ip_add'));
			}
			else
			{
				$sandbox = false;//isset($_POST['test_ipn']) ? true : false;
				$ppHost = '';//$sandbox ? 'sandbox.' : '';
				//verify Code Start
				$raw_post_data = file_get_contents('php://input');
				$raw_post_array = explode('&', $raw_post_data);
				$myPost = array();
				foreach ($raw_post_array as $keyval) {
				  $keyval = explode ('=', $keyval);
				  if (count($keyval) == 2)
					 $myPost[$keyval[0]] = urldecode($keyval[1]);
				}
				// read the post from PayPal system and add 'cmd'
				$req = 'cmd=_notify-validate';
				if(function_exists('get_magic_quotes_gpc')) {
				   $get_magic_quotes_exists = true;
				} 
				foreach ($myPost as $key => $value) {        
				   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
						$value = urlencode(stripslashes($value)); 
				   } else {
						$value = urlencode($value);
				   }
				   $req .= "&$key=$value";
				}
				
				 
				// STEP 2: Post IPN data back to paypal to validate
				
				$ch = curl_init('https://www.'.$ppHost.'paypal.com/cgi-bin/webscr');
				curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
				
				// In wamp like environments that do not come bundled with root authority certificates,
				// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
				// of the certificate as shown below.
				// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
				if( !($res = curl_exec($ch)) ) {
					// error_log("Got " . curl_error($ch) . " when processing IPN data");
					curl_close($ch);
					exit;
				}
				curl_close($ch);
				 
				
				// STEP 3: Inspect IPN validation result and act accordingly
				$tokens = explode("\r\n\r\n", trim($res));
				$res = trim(end($tokens));
				if (strcmp ($res, "VERIFIED") == 0) 
				{
					if($transactionstatus=="Completed")
					{
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => '-', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'PayPal','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod','processorid', 'fees', 'ptype', 'ip_add'));
					
						$this->loadModel('Member');
						$memberdata=$this->Member->find('first', array(
							'conditions' => array('Member.member_id' => $memberid), 
							'fields' => array('Member.member_id', 'Member.f_name', 'Member.l_name', 'Member.user_name', 'Member.email', 'Member.unsubscribeemail')
							)
						);
						
						if($totaltamount>0)
						{
							if($this->sitesettingconfig[0]["sitesettings"]['balance_type']==1)
							{
								$this->Member->updateAll(
									array("Member.cash_balance" => 'cash_balance+'.round($pendingdepositdata['Pending_deposit']['amount'],2)),
									array("Member.member_id" => $memberid)
								);
							}
							else
							{
								$this->Member->updateAll(
									array("Member.cash_".$processorsdata['Processor']['id'] => 'cash_'.$processorsdata['Processor']['id'].'+'.round($pendingdepositdata['Pending_deposit']['amount'],2)),
									array("Member.member_id" => $memberid)
								);
							}
						}
						
						$this->Pending_deposit->delete($pending_deposit_id);
						
						//mail to member
						$sitetitle=$this->sitesettingconfig[0]["sitesettings"]["sitetitle"];
						if($memberdata['Member']['unsubscribeemail']==0)
						{
							if($this->sitesettingconfig[0]["sitesettings"]["reflinkiduser"]==1)
								$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'</a>';
							else
								$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'</a>';
							$this->template_mail($memberdata['Member']['member_id'],$memberdata['Member']['email'],'Successful Add Fund Notification For The Member',array('[SiteTitle]','[UserName]', '[FirstName]', '[LastName]','[Email]','[MemberID]', '[Amount]', '[Processor]','[RefLink]','[SiteUrl]','[Signature]'),array($sitetitle, $memberdata["Member"]['user_name'], $memberdata["Member"]['f_name'], $memberdata["Member"]['l_name'],$memberdata['Member']['email'],$memberdata['Member']['member_id'], $this->currencydata['Currency']['prefix'].round($paidamount*$this->currencydata['Currency']['rate'],4), 'PayPal',$reflink,'<a href="'.$this->SITEURL.'">'.$this->SITEURL.'</a>',$this->sitesettingconfig[0]["sitesettings"]["signature"]));
						}
					}
					//Script Code Over
				}
				else if (strcmp ($res, "INVALID") == 0) 
				{
					// Take appropriate action.
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Transaction was cancelled or an incorrect status was returned', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'PayPal','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount','paymentmethod', 'receiver_email', 'processor','processorid', 'fees', 'ptype', 'ip_add'));
				}
			}
		}//PayPal Over
		//Add Fund Code Over
		
		//Purchase Postion Code Start
		if(isset($_POST['item_name']) && $_POST['item_name']=='Purchase Position')//PayPal
		{
			$receiveremail = $_POST['receiver_email'];
			$transactionstatus = $_POST['payment_status'];
			$pending_reason=$_POST['pending_reason']; 
			$transaction_no = $_POST['txn_id'];
			$payeremail= $_POST['payer_email'];
			$memberid = $_POST['item_number'];
			$paidamount = $_POST['payment_gross'];
			$customdetail=@explode("|",$_POST['custom']);
			$ip=$customdetail[0];
			$pending_position_id=$customdetail[1];
			$callmethod=$customdetail[2];
			//Processer Debuge Code Start
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'PayPal', 'Processor.proc_status' => 1, 'Processor.receivefund' => 1)
				)
			);
			
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$_POST['item_number'],'status' => $_POST['payment_status'], 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['txn_id'], 'processor' => 'PayPal','type'=>'position','description' => trim($description), 'ipaddress' => $ip, 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			
			
			if(count($processorsdata)==0)
			{exit;}
			if($paidamount<=0)
				$paidamount=0;
			if($pending_reason=='')
				$pending_reason='-';
			if($transaction_no=='')
				$transaction_no='-';
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$this->loadModel('Pending_position');
			$pendingpositiondata=$this->Pending_position->find('first', array(
				'conditions' => array('Pending_position.id' => $pending_position_id, 'Pending_position.member_id' => $memberid, 'round(Pending_position.paidamount,2)' => $paidamount, 'Pending_position.processor' => 'PayPal', 'Pending_position.ip_address' => $ip)
				)
			);
			$discount=$pendingpositiondata['Pending_position']['discount'];
						
			if(count($pendingpositiondata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingposition_id-'.$pending_position_id.' | p_type-position', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'PayPal', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype', 'paymentmethod', 'ip_add'));
				
				$this->Pending_position->updateAll(
					array("Pending_position.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-PayPal,ptype-position,paymentmethod-processor\n')"),
					array("Pending_position.id" => $pending_position_id)
				);			
				exit;
			}
			
			$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'PayPal', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>0, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id, 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_position']['amount']-$pendingpositiondata['Pending_position']['discount'])));
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'PayPal')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'PayPal', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
				
			}
			else
			{
				$sandbox = false;//isset($_POST['test_ipn']) ? true : false;
				$ppHost = '';//$sandbox ? 'sandbox.' : '';
				
				//verify Code Start
				$raw_post_data = file_get_contents('php://input');
				$raw_post_array = explode('&', $raw_post_data);
				$myPost = array();
				foreach ($raw_post_array as $keyval) {
				  $keyval = explode ('=', $keyval);
				  if (count($keyval) == 2)
					 $myPost[$keyval[0]] = urldecode($keyval[1]);
				}
				// read the post from PayPal system and add 'cmd'
				$req = 'cmd=_notify-validate';
				if(function_exists('get_magic_quotes_gpc')) {
				   $get_magic_quotes_exists = true;
				} 
				foreach ($myPost as $key => $value) {        
				   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
						$value = urlencode(stripslashes($value)); 
				   } else {
						$value = urlencode($value);
				   }
				   $req .= "&$key=$value";
				}
				
				 
				// STEP 2: Post IPN data back to paypal to validate
				
				$ch = curl_init('https://www.'.$ppHost.'paypal.com/cgi-bin/webscr');
				curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
				
				// In wamp like environments that do not come bundled with root authority certificates,
				// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
				// of the certificate as shown below.
				// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
				if( !($res = curl_exec($ch)) ) {
					// error_log("Got " . curl_error($ch) . " when processing IPN data");
					curl_close($ch);
					exit;
				}
				curl_close($ch);
				 
				
				// STEP 3: Inspect IPN validation result and act accordingly
				$tokens = explode("\r\n\r\n", trim($res));
				$res = trim(end($tokens));
				
				if (strcmp ($res, "VERIFIED") == 0) 
				{
					if($transactionstatus=="Completed")
					{
						//Member Data Start
						$this->loadModel('Member');
						$MemberData = $this->Member->read(null,$memberid);
						//Member Data Over
						
						//Add Position Start
						$ControllerShort=substr($callmethod, 0, strlen($callmethod)-1);
						App::import('Controller', $ControllerShort);
						$ControllerName=$ControllerShort."Controller";
						$ControllerObj = new $ControllerName;
						$FunctionName="AddPosition".str_replace($ControllerShort, "", $callmethod);
						
						$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields, 0, $this->sitesettingconfig, $this->currencydata);
						
						//Delete Pending Position Record
						$this->Pending_position->delete($pending_position_id);
						//Add Position Over
					}
					//Script Code Over
				}
				else if (strcmp ($res, "INVALID") == 0) 
				{
					// Transaction was cancelled or an incorrect status was returned.
					// Take appropriate action.
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Transaction was cancelled or an incorrect status was returned', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'PayPal', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
				}
			}//duplicate Transaction number
		}//PayPal Over
		//Purchase Postion Code Over
		
		//Common Purchase Code Start
		if(isset($_POST['item_name']) && $_POST['item_name']=='Purchase Payment')//PayPal
		{
			$receiveremail = $_POST['receiver_email'];
			$transactionstatus = $_POST['payment_status'];
			$pending_reason=$_POST['pending_reason']; 
			$transaction_no = $_POST['txn_id'];
			$payeremail= $_POST['payer_email'];
			$memberid = $_POST['item_number'];
			$paidamount = $_POST['payment_gross'];
			$customdetail=@explode("|",$_POST['custom']);
			$ip=$customdetail[0];
			$pending_position_id=$customdetail[1];
			$callmethod=$customdetail[2];
			$ControllerShort=substr($callmethod, 0, strlen($callmethod)-1);
			
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'PayPal', 'Processor.proc_status' => 1, 'Processor.receivefund' => 1)
				)
			);
			
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$memberid,'status' => $transactionstatus, 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $transaction_no, 'processor' => 'PayPal','type'=>$ControllerShort,'description' => trim($description), 'ipaddress' => $ip, 'pay_dt' => date('Y-m-d H:i:s'));
				
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			
			if(count($processorsdata)==0)
			{exit;}
			
			$this->loadModel('Pending_plan');
			$pendingpositiondata=$this->Pending_plan->find('first', array(
				'conditions' => array('Pending_plan.id' => $pending_position_id, 'Pending_plan.member_id' => $memberid, 'round(Pending_plan.paidamount,2)' => $paidamount, 'Pending_plan.processor' => 'PayPal', 'Pending_plan.ip_address' => $ip)
				)
			);
			$discount=$pendingpositiondata['Pending_plan']['discount'];
			if(count($pendingpositiondata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingplan_id-'.$pending_position_id.' | p_type-'.$ControllerShort, 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'PayPal', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype', 'ip_add'));
				
				$this->Pending_plan->updateAll(
					array("Pending_plan.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-PayPal,ptype-".$ControllerShort.",paymentmethod-processor\n')"),
					array("Pending_plan.id" => $pending_position_id)
				);			
				exit;
			}
			
			if($paidamount<=0)
				$paidamount=0;
			if($pending_reason=='')
				$pending_reason='-';
			if($transaction_no=='')
				$transaction_no='-';
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'PayPal', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>0, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id,'advertisementsetting'=>$pendingpositiondata['Pending_plan']['advertisementsetting'], 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_plan']['amount']-$pendingpositiondata['Pending_plan']['discount'])));
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'PayPal')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'PayPal', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
			}
			else
			{
				$sandbox = false;//isset($_POST['test_ipn']) ? true : false;
				$ppHost = '';//$sandbox ? 'sandbox.' : '';
				
				//verify Code Start
				$raw_post_data = file_get_contents('php://input');
				$raw_post_array = explode('&', $raw_post_data);
				$myPost = array();
				foreach ($raw_post_array as $keyval) {
				  $keyval = explode ('=', $keyval);
				  if (count($keyval) == 2)
					 $myPost[$keyval[0]] = urldecode($keyval[1]);
				}
				// read the post from PayPal system and add 'cmd'
				$req = 'cmd=_notify-validate';
				if(function_exists('get_magic_quotes_gpc')) {
				   $get_magic_quotes_exists = true;
				} 
				foreach ($myPost as $key => $value) {        
				   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
						$value = urlencode(stripslashes($value)); 
				   } else {
						$value = urlencode($value);
				   }
				   $req .= "&$key=$value";
				}
				
				 
				// STEP 2: Post IPN data back to paypal to validate
				
				$ch = curl_init('https://www.'.$ppHost.'paypal.com/cgi-bin/webscr');
				curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
				
				// In wamp like environments that do not come bundled with root authority certificates,
				// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
				// of the certificate as shown below.
				// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
				if( !($res = curl_exec($ch)) ) {
					// error_log("Got " . curl_error($ch) . " when processing IPN data");
					curl_close($ch);
					exit;
				}
				curl_close($ch);
				 
				
				// STEP 3: Inspect IPN validation result and act accordingly
				$tokens = explode("\r\n\r\n", trim($res));
				$res = trim(end($tokens));
				
				if (strcmp ($res, "VERIFIED") == 0) 
				{
					if($transactionstatus=="Completed")
					{
						//Member Data Start
						$this->loadModel('Member');
						$MemberData = $this->Member->read(null,$memberid);
						//Member Data Over
						
						//Add Position Start
						App::import('Controller', $ControllerShort);
						$ControllerName=$ControllerShort."Controller";
						$ControllerObj = new $ControllerName;
						$FunctionName="AddPlan".str_replace($ControllerShort, "", $callmethod);
						
						$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields,$this->sitesettingconfig,$this->currencydata);
						
						//Delete Pending Position Record
						$this->Pending_plan->delete($pending_position_id);
						//Add Position Over
					}
					//Script Code Over
				}
				else if (strcmp ($res, "INVALID") == 0) 
				{
					// Transaction was cancelled or an incorrect status was returned.
					// Take appropriate action.
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Transaction was cancelled or an incorrect status was returned', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'PayPal', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
				}
			}//duplicate transection number
		
		}//Okpay Over
		//Common Purchase Code Over
		
		//Masspay Code Start
		$with_ids='';
		if($_POST['txn_type']=="masspay" && $_POST['payment_status']=="Completed")//PayPal
		{
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'PayPal', 'Processor.proc_status' => 1, 'Processor.is_masspay' => 1)
				)
			);
			
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$myCustomField= explode("|",$_POST['ap_mpcustom']);
				
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$myCustomField[0],'status' => 'Done', 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['ap_referencenumber'], 'processor' => 'PayPal','type'=>'masspay','description' => trim($description), 'ipaddress' => '-', 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debug Code Over
			
			if(count($processorsdata)==0)
			{exit;}
			
			// Set config fields
			$sandbox = false;//isset($_POST['test_ipn']) ? true : false;
			$ppHost = '';//$sandbox ? 'sandbox.' : '';
			
			//verify Code Start
			$raw_post_data = file_get_contents('php://input');
			$raw_post_array = explode('&', $raw_post_data);
			$myPost = array();
			foreach ($raw_post_array as $keyval) {
			  $keyval = explode ('=', $keyval);
			  if (count($keyval) == 2)
				 $myPost[$keyval[0]] = urldecode($keyval[1]);
			}
			// read the post from PayPal system and add 'cmd'
			$req = 'cmd=_notify-validate';
			if(function_exists('get_magic_quotes_gpc')) {
			   $get_magic_quotes_exists = true;
			} 
			foreach ($myPost as $key => $value) {        
			   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
					$value = urlencode(stripslashes($value)); 
			   } else {
					$value = urlencode($value);
			   }
			   $req .= "&$key=$value";
			}
			
			 
			// STEP 2: Post IPN data back to paypal to validate
			
			$ch = curl_init('https://www.'.$ppHost.'paypal.com/cgi-bin/webscr');
			curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
			
			// In wamp like environments that do not come bundled with root authority certificates,
			// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
			// of the certificate as shown below.
			// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
			if( !($res = curl_exec($ch)) ) {
				// error_log("Got " . curl_error($ch) . " when processing IPN data");
				curl_close($ch);
				exit;
			}
			curl_close($ch);
			
			// STEP 3: Inspect IPN validation result and act accordingly
			
			$tokens = explode("\r\n\r\n", trim($res));
			$res = trim(end($tokens));
			
			if (strcmp ($res, "VERIFIED") == 0) 
			{
				 // Process valid IPN
				$z=1;
				$x=1;
				while($z==1)
				{
					if($_POST['masspay_txn_id_'.$x]!="")
					{
						//Setting information about the transaction
						$batchNumber = $_POST['masspay_txn_id_'.$x];
						$apiReturnCode=0;
						$apiReturnCodeDescription = $_POST['status_'.$x];
						$transactionReferenceNumber="";
						$paymentAmount = $_POST['payment_gross_'.$x];
						$senderEmailAddress = $_POST['payer_email'];
						$receiverEmailAddress = $_POST['receiver_email_'.$x];
						
						$myCustomField=explode("|",$_POST['unique_id_'.$x]);
						
						
						$this->loadModel('Withdrawal_history');
						$whcounter=$this->Withdrawal_history->find('count', array(
							'conditions' => array('Withdrawal_history.refrence_no' => $transactionReferenceNumber, 'Withdrawal_history.processor' => 'PayPal')
							)
						);
						if($whcounter>0)
						{
							$data = array('batch_number' => $batchNumber, 'return_code' => $apiReturnCode, 'return_desc' => 'Duplicate Transection Number', 'refrence_no' => $transactionReferenceNumber, 'dt' => date('Y-m-d H:i:s'), 'amount' => $paymentAmount, 'sender_email' => $senderEmailAddress, 'receiver_email' => $receiverEmailAddress, 'receiver_id' => $myCustomField[0], 'with_id' => $myCustomField[1], 'processor' => 'PayPal', 'processor_id' => $processorsdata['Processor']['id']);
							$this->Withdrawal_history->set($data);
							$this->Withdrawal_history->save($this->Withdrawal_history->data, false, array('batch_number', 'return_code', 'return_desc', 'refrence_no', 'dt', 'amount', 'sender_email', 'receiver_email', 'receiver_id', 'with_id', 'processor', 'processor_id'));
						}
						else
						{
							if($_POST['status_'.$x]=="Completed")
							{	
								$data = array('batch_number' => $batchNumber, 'return_code' => $apiReturnCode, 'return_desc' => $apiReturnCodeDescription, 'refrence_no' => $transactionReferenceNumber, 'dt' => date('Y-m-d H:i:s'), 'amount' => $paymentAmount, 'sender_email' => $senderEmailAddress, 'receiver_email' => $receiverEmailAddress, 'receiver_id' => $myCustomField[0], 'with_id' => $myCustomField[1], 'processor' => 'PayPal', 'processor_id' => $processorsdata['Processor']['id']);
								$this->Withdrawal_history->set($data);
								$this->Withdrawal_history->save($this->Withdrawal_history->data, false, array('batch_number', 'return_code', 'return_desc', 'refrence_no', 'dt', 'amount', 'sender_email', 'receiver_email', 'receiver_id', 'with_id', 'processor', 'processor_id'));
								
								$this->loadModel('Withdraw');
								$this->Withdraw->updateAll(
									array("Withdraw.status" => "'Done'", "Withdraw.complete_dt" => "'".date('Y-m-d H:i:s')."'"),
									array("Withdraw.with_id" => $myCustomField[1])
								);
								
								//Mail Code Start
								$this->loadModel('Member');
								$memberdata=$this->Member->find('first', array(
									'conditions' => array('Member.member_id' => $myCustomField[0]), 
									'fields' => array('Member.member_id', 'Member.f_name', 'Member.l_name', 'Member.user_name', 'Member.email','Member.unsubscribeemail')
								));
								if($memberdata["Member"]['unsubscribeemail']==0)
								{
									if($this->sitesettingconfig[0]["sitesettings"]["reflinkiduser"]==1)
										$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'</a>';
									else
										$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'</a>';
									$this->template_mail($memberdata["Member"]['member_id'],$memberdata["Member"]['email'],'Successful Withdrawal Request Completion Notification',array('[SiteTitle]', '[UserName]', '[FirstName]', '[LastName]', '[Email]', '[MemberID]', '[Amount]', '[RefLink]', '[SiteUrl]', '[Signature]'),array($this->sitesettingconfig[0]["sitesettings"]["sitetitle"], $memberdata['Member']['user_name'], $memberdata['Member']['f_name'], $memberdata['Member']['l_name'],$memberdata["Member"]['email'],$memberdata["Member"]['member_id'], $this->currencydata['Currency']['prefix'].round($paymentAmount*$this->currencydata['Currency']['rate'],4)." ".$this->currencydata['Currency']['suffix'],$reflink,'<a href="'.$this->SITEURL.'">'.$this->SITEURL.'</a>',$this->sitesettingconfig[0]["sitesettings"]["signature"]));
								}
								//Mail Code Over
							}
							else
							{
								if($processorsdata['Processor']['autowithdrow']==1)
								{
									$with_ids.=",".$myCustomField[1];
								}	
							}	
						
						}//duplicate transection number
						
					}
					else
					{
						$z=0;
						break;
					}
					$x++;
				}//while over
			}
			else if (strcmp ($res, "INVALID") == 0)
			{
				 // Process invalid IPN
			}
		}
		if($with_ids!='')
		{
			$this->loadModel('Auto_withdraw');
			$autowithdata= $this->Auto_withdraw->read(null, 1);
			if($autowithdata['Auto_withdraw']['with_id']!=NULL || $autowithdata['Auto_withdraw']['with_id']!='')
				$with_ids=$autowithdata['Auto_withdraw']['with_id'].$with_ids;
			
			$this->Auto_withdraw->updateAll(
				array("Auto_withdraw.with_id" => "'".trim($with_ids,",")."'"),
				array("Auto_withdraw.id" => 1)
			);
		}
		//Masspay Code Over
		
    }//notify Over
	
}
?>