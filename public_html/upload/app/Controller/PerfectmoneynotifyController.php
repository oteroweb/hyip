<?php
class PerfectmoneynotifyController extends AppController 
{
	public function index() 
	{
    	if(isset($_POST['PM_PRODUCT_NAME']) && $_POST['PM_PRODUCT_NAME']=='Add Funds')//Perfect Money 6
		{
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'Perfect Money', 'Processor.proc_status' => 1, 'Processor.isaddfund' => 1)
				)
			);

			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$_POST['MEM_ID'],'status' => 'Completed', 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['PAYMENT_BATCH_NUM'], 'processor' => 'Perfect Money','type'=>'fund','description' => trim($description), 'ipaddress' => $_POST['IP_ADDRESS'], 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			if(count($processorsdata)==0)
			{exit;}
			$receiveremail =$_POST['PAYEE_ACCOUNT'];	
			$transactionstatus = 'Completed';
			$pending_reason=""; 
			$transaction_no =$_POST['PAYMENT_BATCH_NUM'];
			$transactiondate=$_POST['TIMESTAMPGMT'];
			$payeremail=$_POST['PAYER_ACCOUNT'];
			$memberid =$_POST['MEM_ID'];
			$paidamount =$_POST['PAYMENT_AMOUNT'];
			$netamount=$_POST['PAYMENT_AMOUNT'];
			
			$ip=$_POST['IP_ADDRESS'];
			$pending_deposit_id=$_POST['CONFIRM_ID'];
			
			$this->loadModel('Pending_deposit');
			$pendingdepositdata=$this->Pending_deposit->find('first', array(
				'conditions' => array('Pending_deposit.id' => $pending_deposit_id, 'Pending_deposit.member_id' => $memberid, 'round(Pending_deposit.paidamount,2)' => $paidamount, 'Pending_deposit.processorid' => $processorsdata['Processor']['id'], 'Pending_deposit.ip_address' => $ip)
				)
			);
			
			if(count($pendingdepositdata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingdeposit_id-'.$pending_deposit_id.' | p_type-fund', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money','paymentmethod'=>'processor', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => 'fund', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod', 'processorid', 'ptype', 'ip_add'));
				
				$this->Pending_deposit->updateAll(
					array("Pending_deposit.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Perfect Money,ptype-fund\n')"),
					array("Pending_deposit.id" => $pending_deposit_id)
				);
				
				exit;
			}
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no,'Member_fee_payment.transaction_no != ' => '', 'Member_fee_payment.processor' => 'Perfect Money')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money','paymentmethod'=>'processor', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod', 'processorid', 'fees', 'ptype', 'ip_add'));
			}
			elseif($_POST['PAYMENT_UNITS']!='USD')
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Currency Changed By User', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money','paymentmethod'=>'processor', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod', 'processorid', 'fees', 'ptype', 'ip_add'));
			}
			else
			{
				//define('ALTERNATE_PHRASE_HASH',  $this->dycriptstring($processorsdata['Processor']['api_password']));
				$ALTERNATE_PHRASE_HASH=$this->dycriptstring($processorsdata['Processor']['api_password']);
				$UsePassPhase = strtoupper(md5($ALTERNATE_PHRASE_HASH));
				$string=
					  $_POST['PAYMENT_ID'].':'.$_POST['PAYEE_ACCOUNT'].':'.
					  $_POST['PAYMENT_AMOUNT'].':'.$_POST['PAYMENT_UNITS'].':'.
					  $_POST['PAYMENT_BATCH_NUM'].':'.
					  $_POST['PAYER_ACCOUNT'].':'.$UsePassPhase.':'.
					  $_POST['TIMESTAMPGMT'];
				
				$hash=strtoupper(md5($string));
				
				if($hash==$_POST['V2_HASH'])
				{ 
				   if($_POST['PAYEE_ACCOUNT']==$this->dycriptstring($processorsdata['Processor']['proc_accid']) && $_POST['PAYMENT_UNITS']=='USD')
				   {
				   		$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => '-', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money','paymentmethod'=>'processor', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod', 'processorid', 'fees', 'ptype', 'ip_add'));
						
					
						if($transaction_no!="")
						{ 
							$this->loadModel('Member');
							$memberdata=$this->Member->find('first', array(
								'conditions' => array('Member.member_id' => $memberid), 
								'fields' => array('Member.member_id', 'Member.f_name', 'Member.l_name', 'Member.user_name', 'Member.email','Member.unsubscribeemail')
								)
							);
							
							//Add Fund
							if($totaltamount>0)
							{
								if($this->sitesettingconfig[0]["sitesettings"]['balance_type']==1)
								{
									$this->Member->updateAll(
										array("Member.cash_balance" => 'cash_balance+'.$pendingdepositdata['Pending_deposit']['amount']),
										array("Member.member_id" => $memberid)
									);
								}
								else
								{
									$this->Member->updateAll(
										array("Member.cash_".$processorsdata['Processor']['id'] => 'cash_'.$processorsdata['Processor']['id'].'+'.$pendingdepositdata['Pending_deposit']['amount']),
										array("Member.member_id" => $memberid)
									);
								}
							}
							
							// Delete From Pending Deposit
							$this->Pending_deposit->delete($pending_deposit_id);
							
							//mail to member
							$sitetitle=$this->sitesettingconfig[0]["sitesettings"]["sitetitle"];
							if($memberdata['Member']['unsubscribeemail']==0)
							{
								if($this->sitesettingconfig[0]["sitesettings"]["reflinkiduser"]==1)
									$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'</a>';
								else
									$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'</a>';
								$this->template_mail($memberdata['Member']['member_id'],$memberdata['Member']['email'],'Successful Add Fund Notification For The Member',array('[SiteTitle]','[UserName]', '[FirstName]', '[LastName]','[Email]','[MemberID]', '[Amount]', '[Processor]','[RefLink]','[SiteUrl]','[Signature]'),array($sitetitle, $memberdata["Member"]['user_name'], $memberdata["Member"]['f_name'], $memberdata["Member"]['l_name'],$memberdata['Member']['email'],$memberdata['Member']['member_id'], $this->currencydata['Currency']['prefix'].round($paidamount*$this->currencydata['Currency']['rate'],4), 'Perfect Money',$reflink,'<a href="'.$this->SITEURL.'">'.$this->SITEURL.'</a>',$this->sitesettingconfig[0]["sitesettings"]["signature"]));
							}
						}
				   }
				   else
				   { 
				   		$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Fake Data: invalid payments for debug purposes', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money','paymentmethod'=>'processor', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod', 'processorid', 'fees', 'ptype', 'ip_add'));
				   }
				}
				else
				{ 
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'HASH Not Valid: invalid payments for debug purposes', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money', 'processorid' => $processorsdata['Processor']['id'],'paymentmethod'=>'processor', 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod', 'processorid', 'fees', 'ptype', 'ip_add'));
				}
			}//duplicate Transaction number
		}//Perfect Money Over
		//Add Fund Code Over
		
		
		//Purchase Postion Code Start
		if(isset($_POST['PM_PRODUCT_NAME']) && $_POST['PM_PRODUCT_NAME']=='Purchase Position')//Perfect Money
		{
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'Perfect Money', 'Processor.proc_status' => 1, 'Processor.receivefund' => 1)
				)
			);
			
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$_POST['MEM_ID'],'status' => 'Completed', 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['PAYMENT_BATCH_NUM'], 'processor' => 'Perfect Money','type'=>'position','description' => trim($description), 'ipaddress' => $_POST['IP_ADDRESS'], 'pay_dt' => date('Y-m-d H:i:s'));

				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			if(count($processorsdata)==0)
			{exit;}
			$receiveremail =$_POST['PAYEE_ACCOUNT'];	
			$transactionstatus = 'Completed';
			$pending_reason=""; 
			$transaction_no =$_POST['PAYMENT_BATCH_NUM'];
			$transactiondate=$_POST['TIMESTAMPGMT'];
			$payeremail=$_POST['PAYER_ACCOUNT'];
			$memberid =$_POST['MEM_ID'];
			$paidamount =$_POST['PAYMENT_AMOUNT'];
			$netamount=$_POST['PAYMENT_AMOUNT'];
			
			$ip=$_POST['IP_ADDRESS'];
			$pending_position_id=$_POST['CONFIRM_ID'];
			
			
			$this->loadModel('Pending_position');
			$pendingpositiondata=$this->Pending_position->find('first', array(
				'conditions' => array('Pending_position.id' => $pending_position_id, 'Pending_position.member_id' => $memberid, 'round(Pending_position.paidamount,2)' => $paidamount, 'Pending_position.processor' => 'Perfect Money', 'Pending_position.ip_address' => $ip)
				)
			);
			$discount=$pendingpositiondata['Pending_position']['discount'];
			
			if(count($pendingpositiondata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingposition_id-'.$pending_position_id.' | p_type-position', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'ptype', 'paymentmethod', 'ip_add'));
				
				$this->Pending_position->updateAll(
					array("Pending_position.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Perfect Money,ptype-position,paymentmethod-processor\n')"),
					array("Pending_position.id" => $pending_position_id)
				);			
				exit;
			}
			
			if($paidamount<=0)
				$paidamount=0;
			if($pending_reason=='')
				$pending_reason='-';
			if($transaction_no=='')
				$transaction_no='-';
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>0, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id, 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_position']['amount']-$pendingpositiondata['Pending_position']['discount'])));
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'Perfect Money')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
			}
			elseif($_POST['PAYMENT_UNITS']!='USD')
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Currency Changed By User', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money','paymentmethod'=>'processor', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'position', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod', 'processorid', 'fees', 'ptype', 'ip_add'));
			}
			else
			{
				//define('ALTERNATE_PHRASE_HASH',  $this->dycriptstring($processorsdata['Processor']['api_password']));
				$ALTERNATE_PHRASE_HASH=$this->dycriptstring($processorsdata['Processor']['api_password']);
				$UsePassPhase = strtoupper(md5($ALTERNATE_PHRASE_HASH));
				$string=
					  $_POST['PAYMENT_ID'].':'.$_POST['PAYEE_ACCOUNT'].':'.
					  $_POST['PAYMENT_AMOUNT'].':'.$_POST['PAYMENT_UNITS'].':'.
					  $_POST['PAYMENT_BATCH_NUM'].':'.
					  $_POST['PAYER_ACCOUNT'].':'.$UsePassPhase.':'.
					  $_POST['TIMESTAMPGMT'];
				
				$hash=strtoupper(md5($string));
				
				if($hash==$_POST['V2_HASH'])
				{ 
				   if($_POST['PAYEE_ACCOUNT']==$this->dycriptstring($processorsdata['Processor']['proc_accid']) && $_POST['PAYMENT_UNITS']=='USD')
				   {
				   		if($transaction_no!="")
						{ 
							//Member Data Start
							$this->loadModel('Member');
							$MemberData = $this->Member->read(null,$memberid);
							//Member Data Over
							
							//Add Position Start
							$ControllerShort=substr($_POST['CONTROLLERTYPE'], 0, strlen($_POST['CONTROLLERTYPE'])-1);
							App::import('Controller', $ControllerShort);
							$ControllerName=$ControllerShort."Controller";
							$ControllerObj = new $ControllerName;
							$FunctionName="AddPosition".str_replace($ControllerShort, "", $_POST['CONTROLLERTYPE']);
							
							$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields, 0, $this->sitesettingconfig,$this->currencydata);
							
							//Delete Pending Position Record
							$this->Pending_position->delete($pending_position_id);
							//Add Position Over
							
						}
						else 
						{
							// Transaction was cancelled or an incorrect status was returned.
							// Take appropriate action.
							$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Transaction was cancelled or an incorrect status was returned', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
							$this->Member_fee_payment->set($data);
							$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
						}
				   }
				   else
				   { 
				   		$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Fake Data: invalid payments for debug purposes', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
				   }
				}
				else
				{ 
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'HASH Not Valid: invalid payments for debug purposes', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money', 'processorid' => $processorsdata['Processor']['id'],'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
				}
			}//duplicate Transaction number
		}//Perfect Money Over
		//Purchase Postion Code Over
		
		//Common Purchase Code Start
		if(isset($_POST['PM_PRODUCT_NAME']) && $_POST['PM_PRODUCT_NAME']=='Purchase Payment')//Perfect Money
		{
			$ControllerShort=substr($_POST['CONTROLLERTYPE'], 0, strlen($_POST['CONTROLLERTYPE'])-1);
			
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'Perfect Money', 'Processor.proc_status' => 1, 'Processor.receivefund' => 1)
				)
			);
			
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$_POST['MEM_ID'],'status' => 'Completed', 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['PAYMENT_BATCH_NUM'], 'processor' => 'Perfect Money','type'=>$ControllerShort,'description' => trim($description), 'ipaddress' => $_POST['IP_ADDRESS'], 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			if(count($processorsdata)==0)
			{exit;}
			$receiveremail =$_POST['PAYEE_ACCOUNT'];	
			$transactionstatus = 'Completed';
			$pending_reason=""; 
			$transaction_no =$_POST['PAYMENT_BATCH_NUM'];
			$transactiondate=$_POST['TIMESTAMPGMT'];
			$payeremail=$_POST['PAYER_ACCOUNT'];
			$memberid =$_POST['MEM_ID'];
			$paidamount =$_POST['PAYMENT_AMOUNT'];
			$netamount=$_POST['PAYMENT_AMOUNT'];
			
			$ip=$_POST['IP_ADDRESS'];
			$pending_position_id=$_POST['CONFIRM_ID'];
			
			
			$this->loadModel('Pending_position');
			$pendingpositiondata=$this->Pending_position->find('first', array(
				'conditions' => array('Pending_position.id' => $pending_position_id, 'Pending_position.member_id' => $memberid, 'round(Pending_position.paidamount,2)' => $paidamount, 'Pending_position.processor' => 'Perfect Money', 'Pending_position.ip_address' => $ip)
				)
			);
			$discount=$pendingpositiondata['Pending_position']['discount'];
			
			if(count($pendingpositiondata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingplan_id-'.$pending_position_id.' | p_type-'.$ControllerShort, 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'ptype', 'paymentmethod', 'ip_add'));
				
				$this->Pending_position->updateAll(
					array("Pending_position.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-Perfect Money,ptype-".$ControllerShort.",paymentmethod-processor\n')"),
					array("Pending_position.id" => $pending_position_id)
				);			
				exit;
			}
			
			if($paidamount<=0)
				$paidamount=0;
			if($pending_reason=='')
				$pending_reason='-';
			if($transaction_no=='')
				$transaction_no='-';
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>0, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id,'advertisementsetting'=>$pendingpositiondata['Pending_plan']['advertisementsetting'], 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_plan']['amount']-$pendingpositiondata['Pending_plan']['discount'])));
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'Perfect Money')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
			}
			elseif($_POST['PAYMENT_UNITS']!='USD')
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Currency Changed By User', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money','paymentmethod'=>'processor', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => $ControllerShort, 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod', 'processorid', 'fees', 'ptype', 'ip_add'));
			}
			else
			{
				//define('ALTERNATE_PHRASE_HASH',  $this->dycriptstring($processorsdata['Processor']['api_password']));
				$ALTERNATE_PHRASE_HASH=$this->dycriptstring($processorsdata['Processor']['api_password']);
				$UsePassPhase = strtoupper(md5($ALTERNATE_PHRASE_HASH));
				$string=
					  $_POST['PAYMENT_ID'].':'.$_POST['PAYEE_ACCOUNT'].':'.
					  $_POST['PAYMENT_AMOUNT'].':'.$_POST['PAYMENT_UNITS'].':'.
					  $_POST['PAYMENT_BATCH_NUM'].':'.
					  $_POST['PAYER_ACCOUNT'].':'.$UsePassPhase.':'.
					  $_POST['TIMESTAMPGMT'];
				
				$hash=strtoupper(md5($string));
				
				if($hash==$_POST['V2_HASH'])
				{ 
				   if($_POST['PAYEE_ACCOUNT']==$this->dycriptstring($processorsdata['Processor']['proc_accid']) && $_POST['PAYMENT_UNITS']=='USD')
				   {
				   		if($transaction_no!="")
						{ 
							//Member Data Start
							$this->loadModel('Member');
							$MemberData = $this->Member->read(null,$memberid);
							//Member Data Over
							
							//Add Position Start
							$ControllerShort=substr($_POST['CONTROLLERTYPE'], 0, strlen($_POST['CONTROLLERTYPE'])-1);
							App::import('Controller', $ControllerShort);
							$ControllerName=$ControllerShort."Controller";
							$ControllerObj = new $ControllerName;
							$FunctionName="AddPlan".str_replace($ControllerShort, "", $_POST['CONTROLLERTYPE']);
							
							$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields,$this->sitesettingconfig,$this->currencydata);
							
							//Delete Pending Position Record
							$this->Pending_position->delete($pending_position_id);
							//Add Position Over
							
						}
						else 
						{
							// Transaction was cancelled or an incorrect status was returned.
							// Take appropriate action.
							$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Transaction was cancelled or an incorrect status was returned', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
							$this->Member_fee_payment->set($data);
							$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
						}
				   }
				   else
				   { 
				   		$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Fake Data: invalid payments for debug purposes', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
				   }
				}
				else
				{ 
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'HASH Not Valid: invalid payments for debug purposes', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'Perfect Money', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor', 'processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
				}
			}//duplicate Transaction number
		}//Perfect Money Over
		
		//Common Purchase Code Over
		
    }//notify Over
	
}
?>