<?php
class CoinpaymentsnotifyController extends AppController 
{
	public function index() 
	{
    	if(isset($_POST['item_name']) && $_POST['item_name']=="Add Funds" && isset($_POST['ipn_type']) && $_POST['ipn_type']=="button")
		{
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'CoinPayments', 'Processor.proc_status' => 1, 'Processor.isaddfund' => 1)
				)
			);
			
			$receiveremail = $_POST['merchant'];	
			$transactionstatus = $_POST['status'];
			$pending_reason=$_POST['status_text']; 
			$transaction_no = $_POST['txn_id'];
			$transactiondate=date('Y-m-d');
			$payeremail= $_POST['email'];
			$memberid =$_POST['invoice'];
			$paidamount = $_POST['amount1'];
			$netamount=$_POST['amount1'];
			
			$customdetail=@explode("|",$_POST['custom']);
			$ip=$customdetail[1];
			$pending_deposit_id=$customdetail[2];
			$callmethod=$customdetail[0];
			
			if($_POST['status']<0)
			{
				$transactionstatus = "Fail";	
			}
			else if($_POST['status']>0 && $_POST['status']<99)
			{
				$transactionstatus = "Pending for ".$_POST['status_text'];	
			}
			else if($_POST['status']>=100)
			{
				$transactionstatus = "COMPLETE";	
			}
			
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$_POST['invoice'],'status' => $_POST['status'], 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['txn_id'], 'processor' => 'CoinPayments','type'=>'fund','description' => trim($description), 'ipaddress' => $ip, 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			if(count($processorsdata)==0)
			{exit;}
			//Setting information about the transaction
			if($pending_reason=='')
				$pending_reason='-';
			if($transaction_no=='')
				$transaction_no='-';
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$this->loadModel('Pending_deposit');
			$pendingdepositdata=$this->Pending_deposit->find('first', array(
				'conditions' => array('Pending_deposit.id' => $pending_deposit_id, 'Pending_deposit.member_id' => $memberid, 'Pending_deposit.paidamount' => $paidamount, 'Pending_deposit.processorid' => $processorsdata['Processor']['id'], 'Pending_deposit.ip_address' => $ip)
				)
			);
			
			if(count($pendingdepositdata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingdeposit_id-'.$pending_deposit_id.' | p_type-fund', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments','processorid'=>$processorsdata['Processor']['id'], 'ptype' => 'fund','paymentmethod'=>'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype','paymentmethod', 'ip_add'));
				
				$this->Pending_deposit->updateAll(
					array("Pending_deposit.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-CoinPayments,ptype-fund\n')"),
					array("Pending_deposit.id" => $pending_deposit_id)
				);
				exit;
			}
			
			if($_POST['currency1']!="USD")
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'User has changed Curreny', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments','processorid'=>$processorsdata['Processor']['id'], 'ptype' => 'fund','paymentmethod'=>'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype','paymentmethod', 'ip_add'));
				exit;
			}
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no,'Member_fee_payment.transaction_no != ' => '', 'Member_fee_payment.processor' => 'CoinPayments')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund','paymentmethod'=>'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email','paymentmethod', 'processor','processorid', 'fees', 'ptype', 'ip_add'));
				exit;
			}
			else
			{
				$merchantid = $this->dycriptstring($processorsdata['Processor']['proc_accid']);
				$ipnsecret = $this->dycriptstring($processorsdata['Processor']['api_name']);
			
				if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
				{
					if ($_SERVER['PHP_AUTH_USER'] == $merchantid && $_SERVER['PHP_AUTH_PW'] == $ipnsecret)
					{
					
						if($transactionstatus=="COMPLETE")
						{
							$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => '-', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
							$this->Member_fee_payment->set($data);
							$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','paymentmethod','processorid', 'fees', 'ptype', 'ip_add'));
						
							$this->loadModel('Member');
							$memberdata=$this->Member->find('first', array(
								'conditions' => array('Member.member_id' => $memberid), 
								'fields' => array('Member.member_id', 'Member.f_name', 'Member.l_name', 'Member.user_name', 'Member.email', 'Member.unsubscribeemail')
								)
							);
							
							
							//mail to member
							if($totaltamount>0)
							{
								if($this->sitesettingconfig[0]["sitesettings"]['balance_type']==1)
								{
									$this->Member->updateAll(
										array("Member.cash_balance" => 'cash_balance+'.$pendingdepositdata['Pending_deposit']['amount']),
										array("Member.member_id" => $memberid)
									);
								}
								else
								{
									$this->Member->updateAll(
										array("Member.cash_".$processorsdata['Processor']['id'] => 'cash_'.$processorsdata['Processor']['id'].'+'.$pendingdepositdata['Pending_deposit']['amount']),
										array("Member.member_id" => $memberid)
									);
								}
							}
							
							$this->Pending_deposit->delete($pending_deposit_id);
							//mail to member
							$sitetitle=$this->sitesettingconfig[0]["sitesettings"]["sitetitle"];
							if($memberdata['Member']['unsubscribeemail']==0)
							{
								if($this->sitesettingconfig[0]["sitesettings"]["reflinkiduser"]==1)
									$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'</a>';
								else
									$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'</a>';
								$this->template_mail($memberdata['Member']['member_id'],$memberdata['Member']['email'],'Successful Add Fund Notification For The Member',array('[SiteTitle]','[UserName]', '[FirstName]', '[LastName]','[Email]','[MemberID]', '[Amount]', '[Processor]','[RefLink]','[SiteUrl]','[Signature]'),array($sitetitle, $memberdata["Member"]['user_name'], $memberdata["Member"]['f_name'], $memberdata["Member"]['l_name'],$memberdata['Member']['email'],$memberdata['Member']['member_id'], $this->currencydata['Currency']['prefix'].round($paidamount*$this->currencydata['Currency']['rate'],4), 'PayPal',$reflink,'<a href="'.$this->SITEURL.'">'.$this->SITEURL.'</a>',$this->sitesettingconfig[0]["sitesettings"]["signature"]));
							}
							//insert_member($memberid,$referrerid);
							exit;
						}
					}	
					else
					{
						// Take appropriate action.
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Merchant ID and IPN Secret Key Not Matched', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount','paymentmethod', 'receiver_email', 'processor','processorid', 'fees', 'ptype', 'ip_add'));
						exit;
					}
		
				}
				else
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Unauthorised IPN Sent. Merchant And IPN Secret Key Not Found', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'fund', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount','paymentmethod', 'receiver_email', 'processor','processorid', 'fees', 'ptype', 'ip_add'));
					exit;
				}
			}
		}//PayPal Over
		//Add Fund Code Over
		
		//Purchase Postion Code Start
		if(isset($_POST['item_name']) && $_POST['item_name']=="Purchase Position" && isset($_POST['ipn_type']) && $_POST['ipn_type']=="button")
		{
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'CoinPayments', 'Processor.proc_status' => 1, 'Processor.receivefund' => 1)
				)
			);
			
			$receiveremail = $_POST['merchant'];	
			$transactionstatus = $_POST['status'];
			$pending_reason=$_POST['status_text']; 
			$transaction_no = $_POST['txn_id'];
			$transactiondate=date('Y-m-d');
			$payeremail= $_POST['email'];
			$memberid =$_POST['invoice'];
			$paidamount = $_POST['amount1'];
			$netamount=$_POST['amount1'];
			
			$customdetail=@explode("|",$_POST['custom']);
			$ip=$customdetail[1];
			$pending_position_id=$customdetail[2];
			$callmethod=$customdetail[0];
			
			if($_POST['status']<0)
			{
				$transactionstatus = "Fail";	
			}
			else if($_POST['status']>0 && $_POST['status']<99)
			{
				$transactionstatus = "Pending for ".$_POST['status_text'];	
			}
			else if($_POST['status']>=100)
			{
				$transactionstatus = "COMPLETE";	
			}
			
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$_POST['invoice'],'status' => $_POST['status'], 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['txn_id'], 'processor' => 'CoinPayments','type'=>'position','description' => trim($description), 'ipaddress' => $ip, 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			if(count($processorsdata)==0)
			{exit;}
			//Setting information about the transaction
			if($pending_reason=='')
				$pending_reason='-';
			if($transaction_no=='')
				$transaction_no='-';
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$this->loadModel('Pending_position');
			$pendingpositiondata=$this->Pending_position->find('first', array(
				'conditions' => array('Pending_position.id' => $pending_position_id, 'Pending_position.member_id' => $memberid, 'Pending_position.paidamount' => $paidamount, 'Pending_position.processor' => 'CoinPayments', 'Pending_position.ip_address' => $ip)
				)
			);
			$discount=$pendingpositiondata['Pending_position']['discount'];
						
			if(count($pendingpositiondata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingposition_id-'.$pending_position_id.' | p_type-position', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype', 'paymentmethod', 'ip_add'));
				
				$this->Pending_position->updateAll(
					array("Pending_position.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-CoinPayments,ptype-position,paymentmethod-processor\n')"),
					array("Pending_position.id" => $pending_position_id)
				);			
				exit;
			}
			
			$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id, 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_position']['amount']-$pendingpositiondata['Pending_position']['discount'])));
			
			if($_POST['currency1']!="USD")
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'User has changed Curreny', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments','processorid'=>$processorsdata['Processor']['id'], 'ptype' => 'position','paymentmethod'=>'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype','paymentmethod', 'ip_add'));
				exit;
			}
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'CoinPayments')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => 'position', 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
				exit;	
			}
			else
			{
				
				$merchantid = $this->dycriptstring($processorsdata['Processor']['proc_accid']);
				$ipnsecret = $this->dycriptstring($processorsdata['Processor']['api_name']);
			
				if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
				{
					if ($_SERVER['PHP_AUTH_USER'] == $merchantid && $_SERVER['PHP_AUTH_PW'] == $ipnsecret)
					{
					
						if($transactionstatus=="COMPLETE")
						{
							//Member Data Start
							$this->loadModel('Member');
							$MemberData = $this->Member->read(null,$memberid);
							//Member Data Over
							
							//Add Position Start
							$ControllerShort=substr($callmethod, 0, strlen($callmethod)-1);
							App::import('Controller', $ControllerShort);
							$ControllerName=$ControllerShort."Controller";
							$ControllerObj = new $ControllerName;
							$FunctionName="AddPosition".str_replace($ControllerShort, "", $callmethod);
							
							$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields, 0, $this->sitesettingconfig, $this->currencydata);
							
							//Delete Pending Position Record
							$this->Pending_position->delete($pending_position_id);
							//Add Position Over
						}
						exit;
					}	
					else
					{
						// Take appropriate action.
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Merchant ID and IPN Secret Key Not Matched', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'position', 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount','paymentmethod', 'receiver_email', 'processor','processorid', 'fees', 'ptype', 'ip_add'));
						exit;
					}
		
				}
				else
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Unauthorised IPN Sent. Merchant And IPN Secret Key Not Found', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => 'position', 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount','paymentmethod', 'receiver_email', 'processor','processorid', 'fees', 'ptype', 'ip_add'));
					exit;
				}
			}//duplicate Transaction number
		}//PayPal Over
		//Purchase Postion Code Over
		
		//Common Purchase Code Start
		if(isset($_POST['item_name']) && $_POST['item_name']=="Purchase Payment" && isset($_POST['ipn_type']) && $_POST['ipn_type']=="button")
		{
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'CoinPayments', 'Processor.proc_status' => 1, 'Processor.receivefund' => 1)
				)
			);
			
			$receiveremail = $_POST['merchant'];	
			$transactionstatus = $_POST['status'];
			$pending_reason=$_POST['status_text']; 
			$transaction_no = $_POST['txn_id'];
			$transactiondate=date('Y-m-d');
			$payeremail= $_POST['email'];
			$memberid =$_POST['invoice'];
			$paidamount = $_POST['amount1'];
			$netamount=$_POST['amount1'];
			
			$customdetail=@explode("|",$_POST['custom']);
			$ip=$customdetail[1];
			$pending_position_id=$customdetail[2];
			$callmethod=$customdetail[0];
			$ControllerShort=substr($callmethod, 0, strlen($callmethod)-1);
			
			if($_POST['status']<0)
			{
				$transactionstatus = "Fail";	
			}
			else if($_POST['status']>0 && $_POST['status']<99)
			{
				$transactionstatus = "Pending for ".$_POST['status_text'];	
			}
			else if($_POST['status']>=100)
			{
				$transactionstatus = "COMPLETE";	
			}
			
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$_POST['invoice'],'status' => $_POST['status'], 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $_POST['txn_id'], 'processor' => 'CoinPayments','type'=>$ControllerShort,'description' => trim($description), 'ipaddress' => $ip, 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debuge Code Over
			if(count($processorsdata)==0)
			{exit;}
			
			$this->loadModel('Pending_plan');
			$pendingpositiondata=$this->Pending_plan->find('first', array(
				'conditions' => array('Pending_plan.id' => $pending_position_id, 'Pending_plan.member_id' => $memberid, 'Pending_plan.paidamount' => $paidamount, 'Pending_plan.processor' => 'CoinPayments', 'Pending_plan.ip_address' => $ip)
				)
			);
			$discount=$pendingpositiondata['Pending_plan']['discount'];
			if(count($pendingpositiondata)==0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Tampered Payment | pendingplan_id-'.$pending_position_id.' | p_type-'.$ControllerShort, 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments', 'processorid' => $processorsdata['Processor']['id'], 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype', 'ip_add'));
				
				$this->Pending_plan->updateAll(
					array("Pending_plan.notes" => "concat(notes,'Tampered Payment | time-".date('Y-m-d H:i:s').",receiveremail-".$receiveremail.",transactionstatus-".$transactionstatus.",transaction_no-".$transaction_no.",payeremail-".$payeremail.",memberid-".$memberid.",paidamount-".$paidamount.",ip-".$ip.",processor-CoinPayments,ptype-".$ControllerShort.",paymentmethod-processor\n')"),
					array("Pending_plan.id" => $pending_position_id)
				);			
				exit;
			}
			
			if($paidamount<=0)
				$paidamount=0;
			if($pending_reason=='')
				$pending_reason='-';
			if($transaction_no=='')
				$transaction_no='-';
			
			$toptot=100+((100*$processorsdata['Processor']['proc_fee'])/100);
			$totaltamount=(($paidamount-$processorsdata['Processor']['proc_fee_amount'])*100)/$toptot;
			$totalfees=(($totaltamount*$processorsdata['Processor']['proc_fee'])/100)+$processorsdata['Processor']['proc_fee_amount'];
			
			$postedfields = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => $transactionstatus, 'pending_reason' => $pending_reason, 'transaction_no' => $transaction_no, 'paidamount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>0, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip, 'ppid'=>$pending_position_id,'advertisementsetting'=>$pendingpositiondata['Pending_plan']['advertisementsetting'], 'usedbalances'=>array('processor'=>($pendingpositiondata['Pending_plan']['amount']-$pendingpositiondata['Pending_plan']['discount'])));
			
			if($_POST['currency1']!="USD")
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'User has changed Curreny', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments','processorid'=>$processorsdata['Processor']['id'], 'ptype' => $ControllerShort,'paymentmethod'=>'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'ptype','paymentmethod', 'ip_add'));
				exit;
			}
			
			$this->loadModel('Member_fee_payment');
			$member_fee_paymentcounter=$this->Member_fee_payment->find('count', array(
				'conditions' => array('Member_fee_payment.transaction_no' => $transaction_no, 'Member_fee_payment.processor' => 'CoinPayments')
				)
			);
			if($member_fee_paymentcounter>0)
			{
				$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Duplicate Transaction Number', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments', 'processorid' => $processorsdata['Processor']['id'], 'fees' => $totalfees, 'discount'=>$discount, 'ptype' => $ControllerShort, 'paymentmethod' => 'processor', 'ip_add' => $ip);
				$this->Member_fee_payment->set($data);
				$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount', 'receiver_email', 'processor','processorid', 'fees', 'discount', 'ptype', 'paymentmethod', 'ip_add'));
				exit;
			}
			else
			{
				$merchantid = $this->dycriptstring($processorsdata['Processor']['proc_accid']);
				$ipnsecret = $this->dycriptstring($processorsdata['Processor']['api_name']);
			
				if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
				{
					if ($_SERVER['PHP_AUTH_USER'] == $merchantid && $_SERVER['PHP_AUTH_PW'] == $ipnsecret)
					{
					
						if($transactionstatus=="COMPLETE")
						{
							//Member Data Start
							$this->loadModel('Member');
							$MemberData = $this->Member->read(null,$memberid);
							//Member Data Over
							
							//Add Position Start
							App::import('Controller', $ControllerShort);
							$ControllerName=$ControllerShort."Controller";
							$ControllerObj = new $ControllerName;
							$FunctionName="AddPlan".str_replace($ControllerShort, "", $callmethod);
							
							$ControllerObj->$FunctionName($pendingpositiondata, $MemberData, 0, $processorsdata, $postedfields,$this->sitesettingconfig,$this->currencydata);
							
							//Delete Pending Position Record
							$this->Pending_plan->delete($pending_position_id);
							//Add Position Over
						}
						exit;
					}	
					else
					{
						// Take appropriate action.
						$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Merchant ID and IPN Secret Key Not Matched', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => $ControllerShort, 'ip_add' => $ip);
						$this->Member_fee_payment->set($data);
						$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount','paymentmethod', 'receiver_email', 'processor','processorid', 'fees', 'ptype', 'ip_add'));
						exit;
					}
		
				}
				else
				{
					$data = array('member_id' => $memberid, 'pay_dt' => date('Y-m-d H:i:s'), 'payer_email' => $payeremail, 'payment_status' => 'Fail', 'pending_reason' => 'Unauthorised IPN Sent. Merchant And IPN Secret Key Not Found', 'transaction_no' => $transaction_no, 'amount' => $paidamount, 'receiver_email' => $receiveremail, 'processor' => 'CoinPayments','paymentmethod'=>'processor','processorid'=>$processorsdata['Processor']['id'], 'fees' => $totalfees, 'ptype' => $ControllerShort, 'ip_add' => $ip);
					$this->Member_fee_payment->set($data);
					$this->Member_fee_payment->save($this->Member_fee_payment->data, false, array('member_id', 'pay_dt', 'payer_email', 'payment_status', 'pending_reason', 'transaction_no', 'amount','paymentmethod', 'receiver_email', 'processor','processorid', 'fees', 'ptype', 'ip_add'));
					exit;
				}
			}//duplicate transection number
		
		}//Okpay Over
		//Common Purchase Code Over
		
		//Masspay Code Start
		$with_ids='';
		if(isset($_POST['ipn_type']) && $_POST['ipn_type']=="api")//CoinPayments code masspay 
		{
			$senderEmailAddress = "Admin";
			$receiverEmailAddress = $_POST['buyer_name'];
			$transactionReferenceNumber = $_POST['txn_id'];
			$currency1 = $_POST['currency1'];		//from currency
			$currency2 = $_POST['currency2'];		//to currency
			$paymentAmount = $_POST['amount1'];
			$member_id = $_POST['item_name'];
			$with_id = $_POST['item_number'];
			$batchNumber = $_POST['txn_id'];
			$apiReturnCode = $_POST['status'];
			$apiReturnCodeDescription = $_POST['status_text'];
	
			if($_POST['status']<0)
			{
				$transactionstatus = "Fail";	
			}
			else if($_POST['status']>0 && $_POST['status']<99)
			{
				$transactionstatus = "Pending for ".$_POST['status_text'];	
			}
			else if($_POST['status']>=100)
			{
				$transactionstatus = "Complete";	
			}
	
			$this->loadModel('Processor');
			$processorsdata=$this->Processor->find('first', array(
				'conditions' => array('Processor.proc_name' => 'CoinPayments', 'Processor.proc_status' => 1, 'Processor.is_masspay' => 1)
				)
			);
			
			//Processer Debuge Code Start
			if(strpos($this->sitesettingconfig[0]["sitesettings"]["logs"],'ProcessorTraceLogs|1') !== false)
			{
				$description="";
				foreach($_POST as $key=>$value)
				{
					$description.=$key.' => '.$value.",\n";
				}
				
				$this->loadModel('Processordebug');
				$data = array('memberid'=>$member_id,'status' => $transactionstatus, 'processorid' => $processorsdata['Processor']['id'], 'transaction_no' => $transactionReferenceNumber, 'processor' => 'CoinPayments','type'=>'masspay','description' => trim($description), 'ipaddress' => '-', 'pay_dt' => date('Y-m-d H:i:s'));
				$this->Processordebug->set($data);
				$this->Processordebug->save($this->Processordebug->data, false, array('memberid','status', 'processorid', 'transaction_no', 'processor', 'type', 'description', 'ipaddress', 'pay_dt'));
			}
			//Processer Debug Code Over
			
			if(count($processorsdata)==0)
			{exit;}
			
			$this->loadModel('Withdrawal_history');
			$whcounter=$this->Withdrawal_history->find('count', array(
				'conditions' => array('Withdrawal_history.refrence_no' => $transactionReferenceNumber, 'Withdrawal_history.processor' => 'CoinPayments')
				)
			);
			if($whcounter>0)
			{
				$data = array('batch_number' => $batchNumber, 'return_code' => $apiReturnCode, 'return_desc' => 'Duplicate Transection Number', 'refrence_no' => $transactionReferenceNumber, 'dt' => date('Y-m-d H:i:s'), 'amount' => $paymentAmount, 'sender_email' => $senderEmailAddress, 'receiver_email' => $receiverEmailAddress, 'receiver_id' => $member_id, 'with_id' => $with_id, 'processor' => 'CoinPayments', 'processor_id' => $processorsdata['Processor']['id']);
				$this->Withdrawal_history->set($data);
				$this->Withdrawal_history->save($this->Withdrawal_history->data, false, array('batch_number', 'return_code', 'return_desc', 'refrence_no', 'dt', 'amount', 'sender_email', 'receiver_email', 'receiver_id', 'with_id', 'processor', 'processor_id'));
			}
			else
			{
				if($transactionstatus=='Complete')
				{
					$merchantid = $this->dycriptstring($processorsdata['Processor']['proc_accid']);
					$ipnsecret = $this->dycriptstring($processorsdata['Processor']['api_password']);
					
					if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
					{
						if ($_SERVER['PHP_AUTH_USER'] == $merchantid && $_SERVER['PHP_AUTH_PW'] == $ipnsecret)
						{
							$data = array('batch_number' => $batchNumber, 'return_code' => $apiReturnCode, 'return_desc' => $apiReturnCodeDescription, 'refrence_no' => $transactionReferenceNumber, 'dt' => date('Y-m-d H:i:s'), 'amount' => $paymentAmount, 'sender_email' => $senderEmailAddress, 'receiver_email' => $receiverEmailAddress, 'receiver_id' => $member_id, 'with_id' => $with_id, 'processor' => 'CoinPayments', 'processor_id' => $processorsdata['Processor']['id']);
							$this->Withdrawal_history->set($data);
							$this->Withdrawal_history->save($this->Withdrawal_history->data, false, array('batch_number', 'return_code', 'return_desc', 'refrence_no', 'dt', 'amount', 'sender_email', 'receiver_email', 'receiver_id', 'with_id', 'processor', 'processor_id'));
							
							$this->loadModel('Withdraw');
							$this->Withdraw->updateAll(
								array("Withdraw.status" => "'Done'", "Withdraw.complete_dt" => "'".date('Y-m-d H:i:s')."'"),
								array("Withdraw.with_id" => $with_id)
							);
							
							//Mail Code Start
							$this->loadModel('Member');
							$memberdata=$this->Member->find('first', array(
								'conditions' => array('Member.member_id' => $member_id), 
								'fields' => array('Member.member_id', 'Member.f_name', 'Member.l_name', 'Member.user_name', 'Member.email','Member.unsubscribeemail')
							));
							if($memberdata["Member"]['unsubscribeemail']==0)
							{
								if($this->sitesettingconfig[0]["sitesettings"]["reflinkiduser"]==1)
									$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['user_name'].'</a>';
								else
									$reflink='<a href="'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'">'.$this->SITEURL.'ref/'.$memberdata['Member']['member_id'].'</a>';
								$this->template_mail($memberdata["Member"]['member_id'],$memberdata["Member"]['email'],'Successful Withdrawal Request Completion Notification',array('[SiteTitle]', '[UserName]', '[FirstName]', '[LastName]', '[Email]', '[MemberID]', '[Amount]', '[RefLink]', '[SiteUrl]', '[Signature]'),array($this->sitesettingconfig[0]["sitesettings"]["sitetitle"], $memberdata['Member']['user_name'], $memberdata['Member']['f_name'], $memberdata['Member']['l_name'],$memberdata["Member"]['email'],$memberdata["Member"]['member_id'], $this->currencydata['Currency']['prefix'].round($paymentAmount*$this->currencydata['Currency']['rate'],4)." ".$this->currencydata['Currency']['suffix'],$reflink,'<a href="'.$this->SITEURL.'">'.$this->SITEURL.'</a>',$this->sitesettingconfig[0]["sitesettings"]["signature"]));
							}
							//Mail Code Over
						}
						else
						{
							$data = array('batch_number' => $batchNumber, 'return_code' => $apiReturnCode, 'return_desc' => 'Merchant ID and IPN Secret Key Not Matched', 'dt' => date('Y-m-d H:i:s'), 'amount' => $paymentAmount, 'sender_email' => $senderEmailAddress, 'receiver_email' => $receiverEmailAddress, 'receiver_id' => $member_id, 'with_id' => $with_id, 'processor' => 'CoinPayments', 'processor_id' => $processorsdata['Processor']['id']);
							$this->Withdrawal_history->set($data);
							$this->Withdrawal_history->save($this->Withdrawal_history->data, false, array('batch_number', 'return_code', 'return_desc', 'refrence_no', 'dt', 'amount', 'sender_email', 'receiver_email', 'receiver_id', 'with_id', 'processor', 'processor_id'));
							
						}
					}
					else
					{
						$data = array('batch_number' => $batchNumber, 'return_code' => $apiReturnCode, 'return_desc' => 'Unauthorised IPN Sent. Merchant And IPN Secret Key Not Found', 'dt' => date('Y-m-d H:i:s'), 'amount' => $paymentAmount, 'sender_email' => $senderEmailAddress, 'receiver_email' => $receiverEmailAddress, 'receiver_id' => $member_id, 'with_id' => $with_id, 'processor' => 'CoinPayments', 'processor_id' => $processorsdata['Processor']['id']);
						$this->Withdrawal_history->set($data);
						$this->Withdrawal_history->save($this->Withdrawal_history->data, false, array('batch_number', 'return_code', 'return_desc', 'refrence_no', 'dt', 'amount', 'sender_email', 'receiver_email', 'receiver_id', 'with_id', 'processor', 'processor_id'));
						
					}
				}
			}
		}
		//Masspay Code Over
		
    }//notify Over
	
}
?>