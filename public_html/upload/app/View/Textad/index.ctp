<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 15-04-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($themesubmenuaccess){?>
<?php if(!$ajax){ ?>
<div id="textadpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<?php // Top menu code starts here ?>
<div class="comisson-bg mobilecss">
	<div class="commison-menu">
		<ul>
			<?php if($SITECONFIG['enable_textads']==1){?>
			<li>
				<?php
					echo $this->Js->link(__('Text Ads'), array('controller'=>'textad', "action"=>"index"), array(
						'update'=>'#textadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip act',
						'title'=>__('Text Ads')
					));
				?>
			</li>
			<?php }
			if(strpos($SITECONFIG["textadplansetting"],'isenable|1') !== false){ ?>
			<li>
				<?php
					echo $this->Js->link(__('Text Ad Plans'), array('controller'=>'textad', "action"=>"plans"), array(
						'update'=>'#textadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Text Ad Plans')
					));
				?>
			</li>
			<?php } ?>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Top menu code ends here ?>

<?php echo $this->Javascript->link('allpage');?>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#textadpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'textad', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Textadd']['page'];
	?>
	<div class="main-box-eran">
	
	<?php // Text ads table starts here ?>
	<div class="padding-left-serchtabal">
		<?php echo $this->Js->link(__("Add New"), array('controller'=>'textad', "action"=>"add"), array(
			'update'=>'#textadpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'button'
		));?>
	</div>
	<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
	<div class="clear-both"></div>
	<div class="height5"></div>
			<div class="divtable textcenter">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam"><?php echo __("Description");?></div>
						<div class="divth textcenter vam"><?php echo __("CTR");?></div>
						<div class="divth textcenter vam">
							<?php 
							if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
							echo $this->Js->link(__('Clicks'), array('controller'=>'textad', "action"=>"index/0/click/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#textadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Clicks')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__('Used Credits'), array('controller'=>'textad', "action"=>"index/0/disp_counter/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#textadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Used Credits')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Allowed Credits'), array('controller'=>'textad', "action"=>"index/0/credit/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#textadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Allowed Credits')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Status'), array('controller'=>'textad', "action"=>"index/0/status/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#textadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Status')
							));?>
						</div>
						<div class="divth textcenter vam"><?php echo __("Action");?></div>
					</div>
				</div>
				<div class="divtbody">
					<div class="divtr">
						<div class="divtd ovw-padding-tabal"></div>
					</div>
					<?php $i=1;
					foreach ($textads as $textad):
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>

						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam">
								<?php
									echo '<a style="color:'.$SITECONFIG["title_color"].';" href="'.$textad['Textadd']['urllink'].'" target="_blank">'.$textad['Textadd']['title'].'</a>';
									echo '<br><span style="color:'.$SITECONFIG["desc1_color"].';">'.$textad['Textadd']['desc1'].'</span><br><span style="color:'.$SITECONFIG["desc2_color"].';">'.$textad['Textadd']['desc2'].'</span><br>';
								if($textad['Textadd']['showurl']==1)
										echo '<span style="color:'.$SITECONFIG["url_color"].';">'.$textad['Textadd']['urllink']."<span/>";
								?>
							</div>
							<div class="divtd textcenter vam"><?php echo @number_format((($textad['Textadd']['click']*100)/$textad['Textadd']['disp_counter']),2,'.',''); ?>%</div>
							<div class="divtd textcenter vam"><?php echo $textad['Textadd']['click'];?></div>
							<div class="divtd textcenter vam"><?php echo $textad['Textadd']['disp_counter'];?></div>
							<div class="divtd textcenter vam"><?php echo $textad['Textadd']['credit'];?></div>
							<div class="divtd textcenter vam">
								<?php 
								if($textad['Textadd']['status']==0){
									$statusaction='1';
									$statusicon='red-icon.png';
									$statustext='Pending';
								}else{
									$statusaction='0';
									$statusicon='blue-icon.png';
									$statustext='Approved';}
								echo $this->html->image($statusicon, array('alt'=>__($statustext), 'title'=>__($statustext), 'class'=>'vtip'));
								?>
							</div>
							<div class="divtd textcenter vam">
								<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit Text Ad'))), array('controller'=>'textad', "action"=>"add/".$textad['Textadd']['textadd_id']), array(
									'update'=>'#textadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>__('Edit Text Ad')
								));?>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>__('Delete Text Ad'))), array('controller'=>'textad', "action"=>"remove/".$textad['Textadd']['textadd_id']."/".$currentpagenumber), array(
									'update'=>'#textadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>__('Delete Text Ad'),
									'confirm'=>__("Do you really want to delete this Text Ad?")
								));?>
							</div>
						</div>
					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($textads)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // Text ads table starts here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Textadd']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
	<div class="pag-float-left">
		<div class="ul-bg">
			<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Textadd',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'textad','action'=>'index/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#textadpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'textad',
						  'action'=>'index/rpp',
						  'url'   => array('controller' => 'textad', 'action' => 'index/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
			</ul>
		<div class="clear-both"></div>
		</div>
	</div>
	<?php }?>
	<div class="floatright ul-bg">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
		</ul>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>
	
</div>
<?php if(!$ajax){?>
</div>
<?php } ?>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>