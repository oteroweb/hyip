<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Text Ads</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Text Ads", array('controller'=>'textad', "action"=>"index"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("View Credits", array('controller'=>'textad', "action"=>"textcredit"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Text Ad Plans", array('controller'=>'textad', "action"=>"plan"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="textadpage">
<?php }?>
	<div class="tab-innar">
		<ul>
		    <li>
				<?php echo $this->Js->link("Credit Text Ads", array('controller'=>'textad', "action"=>"index"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
		    </li>
		    <li>
				<?php echo $this->Js->link("Plan Text Ads", array('controller'=>'textad', "action"=>"planmember"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'active'
				));?>
			</li>
		</ul>
	</div>		
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Text_Ads#Plan_Text_Ads" target="_blank">Help</a></div>
    <div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Ptextad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'textad','action'=>'planmember')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
					<?php 
					echo $this->Form->input('searchby', array(
						  'type' => 'select',
						  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'member_id'=>'Member Id', 'title'=>'Title',  'display_counter'=>'Displayed', 'click_counter'=>'Clicked', 'running'=>'Running Text Ads', 'expire'=>'Expire Text Ads',  'active'=>'Active Text Ads', 'inactive'=>'Inactive Text Ads'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => '',
						  'onchange'=>'if($(this).val()=="inactive" || $(this).val()=="active" || $(this).val()=="expire" || $(this).val()=="running"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);}'
					));
					?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=="inactive" || $searchby=="active" || $searchby=='expire' || $searchby=='running'){ echo 'style="display:none"';} ?>>
			<span>Search For :</span>
			<span class="searchforfields_s"><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#textadpage',
				'class'=>'searchbtn',
				'controller'=>'textad',
				'action'=>'planmember/'.$planid,
				'url'=> array('controller' => 'textad', 'action' => 'planmember/'.$planid)
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->

	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#textadpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'textad', 'action'=>'planmember/'.$planid)
	));
	$currentpagenumber=$this->params['paging']['Ptextad']['page'];
	?>
	
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Ptextad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'textad','action'=>'planmember')));?>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'textad', "action"=>"planmember/".$planid."/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#textadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('M. Id', array('controller'=>'textad', "action"=>"planmember/".$planid."/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#textadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Purchase Date', array('controller'=>'textad', "action"=>"planmember/".$planid."/0/pruchase_date/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#textadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Purchase Date'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Plan', array('controller'=>'textad', "action"=>"planmember/".$planid."/0/plan_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#textadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Title', array('controller'=>'textad', "action"=>"planmember/".$planid."/0/title/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#textadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Title'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Displayed', array('controller'=>'textad', "action"=>"planmember/".$planid."/0/display_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#textadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Displayed'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Clicked', array('controller'=>'textad', "action"=>"planmember/".$planid."/0/click_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#textadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Clicked'
					));?>
				</div>
				<div><?php echo 'Action';?></div>
			</div>
			<?php foreach ($ptextads as $ptextad): ?>
				<div class="tablegridrow">
					<div><?php echo $ptextad['Ptextad']['id']; ?></div>
					<div>
						<?php 
						echo $this->Js->link($ptextad['Ptextad']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$ptextad['Ptextad']['member_id']."/top/textad/index~top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>
					<?php echo $this->Time->format($SITECONFIG["timeformate"], $ptextad['Ptextad']['pruchase_date']);
					$tk='<b>Date Approved :</b> '.$this->Time->format($SITECONFIG["timeformate"], $ptextad['Ptextad']['approve_date']).'<br /><b>Expiry Date :</b> '.$this->Time->format($SITECONFIG["timeformate"], $ptextad['Ptextad']['expire_date']);?>
					<a class="vtip" title='<?php echo $tk;?>'><?php echo $this->html->image('information.png', array('alt'=>'', 'align' =>'absmiddle'));?></a>
					</div>
					<div><?php echo $ptextad['Ptextadplan']['plan_name']; ?></div>
					<div>
						<a href="<?php echo $ptextad['Ptextad']['site_url'];?>" target="_blank" class="vtip" title="<b>Title : </b><?php echo $ptextad['Ptextad']['title']; ?>"> <?php echo $ptextad['Ptextad']['title']; ?></a>
					</div>
					<div><?php echo $ptextad['Ptextad']['display_counter']; ?></div>
					<div><?php echo $ptextad['Ptextad']['click_counter']; ?></div>
					<div>
					<div class="actionmenu">
					  <div class="btn-group">
						<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
						Action <span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
							
							<?php if(!isset($SubadminAccessArray) || in_array('textad',$SubadminAccessArray) || in_array('textad/adminpanel_planmemberadd/$', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Text Ad'))." Edit", array('controller'=>'textad', "action"=>"planmemberadd/".$planid."/".$ptextad['Ptextad']['id']), array(
									'update'=>'#textadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('textad',$SubadminAccessArray) || in_array('textad/adminpanel_planmemberstatus', $SubadminAccessArray)){ ?>
							<li>
								<?php
								if($ptextad['Ptextad']['status']==0){
									$statusaction='1';
									$statusicon='red-icon.png';
									$statustext='Approve';
								}else{
									$statusaction='0';
									$statusicon='blue-icon.png';
									$statustext='Disapprove';}
								echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'textad', "action"=>"planmemberstatus/".$planid."/".$statusaction."/".$ptextad['Ptextad']['id']."/".$currentpagenumber), array(
									'update'=>'#textadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('textad',$SubadminAccessArray) || in_array('textad/adminpanel_planmemberremove', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Text Ad'))." Delete", array('controller'=>'textad', "action"=>"planmemberremove/".$planid."/".$ptextad['Ptextad']['id']."/".$currentpagenumber), array(
									'update'=>'#textadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'',
									'confirm'=>"Do You Really Want to Delete This Text Ad?"
								));?>
							</li>
							<?php } ?>
						</ul>
					  </div>
					</div>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($ptextads)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Ptextad']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Ptextad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'textad','action'=>'planmember/'.$planid.'/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#textadpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'textad',
			  'action'=>'planmember/'.$planid.'/rpp',
			  'url'   => array('controller' => 'textad', 'action' => 'planmember/'.$planid.'/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
    </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
	<?php if(!$ajax){?>
</div><!--#textadpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>