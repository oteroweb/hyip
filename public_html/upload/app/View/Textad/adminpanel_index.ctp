<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Text Ads</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
		<ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Text Ads", array('controller'=>'textad', "action"=>"index"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("View Credits", array('controller'=>'textad', "action"=>"textcredit"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Text Ad Plans", array('controller'=>'textad', "action"=>"plan"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
		</ul>
	</div>
</div>
<div class="tab-content">
<div id="textadpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
    <div class="helpicon"><a href="https://www.proxscripts.com/docs/Text_Ads#Text_Ads" target="_blank">Help</a></div>
	 <div class="tab-innar">
		<ul>
		    <li>
				<?php echo $this->Js->link("Credit Text Ads", array('controller'=>'textad', "action"=>"index"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'active'
				));?>
		    </li>
		    <li>
				<?php echo $this->Js->link("Plan Text Ads", array('controller'=>'textad', "action"=>"planmember"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
		</ul>
	</div>

    <div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Textadd',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'textad','action'=>'textads')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
					<?php 
					echo $this->Form->input('searchby', array(
						  'type' => 'select',
						  'options' => array('all'=>'Select Parameter', 'textadd_id'=>'Text Ad Id', 'active'=>'Active Text Ads', 'inactive'=>'Inactive / Unapproved Text Ads', 'running'=>'Running Text Ads', 'expire'=>'Expire Text Ads', 'user_id'=>'Member Id'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => '',
						  'onchange'=>'if($(this).val()=="inactive" || $(this).val()=="active" || $(this).val()=="expire" || $(this).val()=="running"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);}'
					));
					?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=="inactive" || $searchby=="active" || $searchby=='expire' || $searchby=='running'){ echo 'style="display:none"';} ?>>
			<span>Search For :</span>
			<span class="searchforfields_s"><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
                  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#textadpage',
                  'class'=>'searchbtn',
                  'controller'=>'textad',
                  'action'=>'index',
                  'url'=> array('controller' => 'textad', 'action' => 'index')
                ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->

        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#textadpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'textad', 'action'=>'index')
        ));
        $currentpagenumber=$this->params['paging']['Textadd']['page'];
        ?>

<div id="gride-bg">
    <div class="Xpadding10">

	<?php echo $this->Form->create('Textadd',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'textad','action'=>'textadstatus')));?>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php 
		echo $this->Form->checkbox('selectAllCheckboxes', array(
		  'hiddenField' => false,
		  'onclick' => 'selectAllCheckboxes("textadIds",this.checked)'
		));
		?>
		<label for="TextaddSelectAllCheckboxes"></label>
	</div>
	<div class="addnew-button">
        <?php if(!isset($SubadminAccessArray) || in_array('textad',$SubadminAccessArray) || in_array('textad/adminpanel_textadadd',$SubadminAccessArray)){ ?>
                <?php echo $this->Js->link("+ Add New", array('controller'=>'textad', "action"=>"textadadd"), array(
                    'update'=>'#textadpage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'class'=>'btnorange'
                ));?>
        <?php } ?>
	<?php if(!isset($SubadminAccessArray) || in_array('textad',$SubadminAccessArray) || in_array('textad/adminpanel_textadstatus',$SubadminAccessArray))
		{
			echo $this->Js->submit('Approve Selected Record(s)', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'escape'=>false,
			  'update'=>'#textadpage',
			  'class'=>'btngray',
			  'div'=>false,
			  'controller'=>'textad',
			  'action'=>'textadstatus',
			  'confirm' => 'Are You Sure?',
			  'url'   => array('controller' => 'textad', 'action' => 'textadstatus')
			));
		
		}?>
		
        
	</div>
	<div class="clear-both"></div>
        
		<div class="tablegrid">
				<div class="tablegridheader">
				    <div>
                        <?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('M. Id', array('controller'=>'textad', "action"=>"index/0/0/user_id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#textadpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Member Id'
                        ));?>
                    </div>
				    <div>
                        <?php echo $this->Js->link('Date', array('controller'=>'textad', "action"=>"index/0/0/create_date/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#textadpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Date'
                        ));?>
                    </div>
				    <div><?php echo 'Username';?></div>
				    <div>
                        <?php echo $this->Js->link('Title', array('controller'=>'textad', "action"=>"index/0/0/title/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#textadpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Title'
                        ));?>
                    </div>
				    <div><?php echo "Text Ad";?></div>
				    <div><?php echo "Clicks";?></div>
				    <div><?php echo "CTR";?></div>
				    <div>
                        Credit (<?php echo $this->Js->link('Used', array('controller'=>'textad', "action"=>"index/0/0/disp_counter/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#textadpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Used Credits'
                        ));
						?>/<?php 
						echo $this->Js->link('Allowed', array('controller'=>'textad', "action"=>"index/0/0/credit/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#textadpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Allowed Credits'
                        ));?>)
                    </div>
				    <div>
                        <?php echo $this->Js->link('Action', array('controller'=>'textad', "action"=>"index/0/0/status/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#textadpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Status'
                        ));?>
                    </div>
				    <div></div>
                </div>
                <?php foreach ($textadds as $textadd): ?>
                    <div class="tablegridrow">
					    <div>
							<?php 
							echo $this->Js->link($textadd['Textadd']['user_id'], array('controller'=>'member', "action"=>"memberadd/".$textadd['Textadd']['user_id']."/top/textad/index~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
						</div>
						<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $textadd['Textadd']['create_date']); ?></div>
						<div>
							<?php 
							echo $this->Js->link($textadd['Member']['user_name'], array('controller'=>'member', "action"=>"memberadd/".$textadd['Textadd']['user_id']."/top/textad/index~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
						</div>
						<div><?php echo '<a href="'.$textadd['Textadd']['urllink'].'" target="_blank">'.$textadd['Textadd']['title'].'</a>'; ?></div>
						<div>
                            <a class="vtip" title='<?php 
                            echo '<a style="color:'.$SITECONFIG["title_color"].';" href="'.$textadd['Textadd']['urllink'].'" target="_blank">'.$textadd['Textadd']['title'].'</a>';
                            echo '<br><span style="color:'.$SITECONFIG["desc1_color"].';">'.$textadd['Textadd']['desc1'].'</span><br><span style="color:'.$SITECONFIG["desc2_color"].';">'.$textadd['Textadd']['desc2'].'</span><br>';
                            if($textadd['Textadd']['showurl']==1)
                                echo '<span style="color:'.$SITECONFIG["url_color"].';">'.$textadd['Textadd']['urllink']."<span/>";
                            ?>'><?php echo $this->html->image('search.png', array('alt'=>'', 'align' =>'absmiddle'));?></a>
                        </div>
						<div><?php echo $textadd['Textadd']['click']; ?></div>
						<div><?php echo @number_format((($textadd['Textadd']['click']*100)/$textadd['Textadd']['disp_counter']),2,'.',''); ?>%</div>
						<div><?php echo $textadd['Textadd']['disp_counter'].'/'.$textadd['Textadd']['credit']; ?></div>
						<div class="textcenter">
								<span class="actionmenu">
								  <span class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
										
									<?php if(!isset($SubadminAccessArray) || in_array('textad',$SubadminAccessArray) || in_array('textad/adminpanel_textadadd/$',$SubadminAccessArray)){?>
									<li>
									<?php 
										echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Text Ad'))." Edit", array('controller'=>'textad', "action"=>"textadadd/".$textadd['Textadd']['textadd_id']), array(
											'update'=>'#textadpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'class'=>''
										));?>
									</li>
									<?php }?>
									
												<?php 
									if(!isset($SubadminAccessArray) || in_array('textad',$SubadminAccessArray) || in_array('textad/adminpanel_textadstatus',$SubadminAccessArray)){?>
									<li>
										<?php
										if($textadd['Textadd']['status']==0){
											$statusaction='1';
											$statusicon='red-icon.png';
											$statustext='Activate';
										}else{
											$statusaction='0';
											$statusicon='blue-icon.png';
											$statustext='Inactivate';}
										echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'textad', "action"=>"textadstatus/".$statusaction."/".$textadd['Textadd']['textadd_id']."/".$textadd['Textadd']['user_id']."/".$currentpagenumber), array(
											'update'=>'#textadpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'class'=>''
										));?>
									</li>	
									<?php }?>
									
									<?php if(!isset($SubadminAccessArray) || in_array('textad',$SubadminAccessArray) || in_array('textad/adminpanel_textadremove',$SubadminAccessArray)){?>
									<li>
									<?php 
										echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Text Ad'))." Delete", array('controller'=>'textad', "action"=>"textadremove/".$textadd['Textadd']['textadd_id']."/".$currentpagenumber), array(
											'update'=>'#textadpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'class'=>'',
											'confirm'=>"Do You Really Want to Delete This Text Ad?"
										));?>
									</li>
									<?php }?>
									
									</ul>
								  </span>
								</span>
                        </div>
						<div class="checkbox">
								<?php
								if($textadd['Textadd']['status']==0)
								{
									echo $this->Form->checkbox('textadIds.', array(
									  'value' => $textadd['Textadd']['textadd_id'],
									  'class' => 'textadIds',
									  'id'=>'textadIds'.$textadd['Textadd']['textadd_id'],
									  'hiddenField' => false
									));
									echo '<label for="textadIds'.$textadd['Textadd']['textadd_id'].'"></label>';
								}
								?>
                        </div>
                    </div>
                <?php endforeach; ?>
        </div>
	<?php if(count($textadds)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php echo $this->Form->end();
        if($this->params['paging']['Textadd']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Textadd',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'textad','action'=>'index/0/rpp')));?>
			<div class="resultperpage">
				<label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
				</label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#textadpage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'textad',
                  'action'=>'index/0/rpp',
                  'url'   => array('controller' => 'textad', 'action' => 'index/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
     </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#textadpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>