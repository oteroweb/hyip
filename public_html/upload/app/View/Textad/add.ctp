<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('framechecker');?>
<div id="UpdateMessage"></div>

<?php //Frame Break Code Start ?>
<input type="hidden" id='framebreakmessage' value='<?php echo __('Site URL Not Valid'); ?>|<?php echo __('Breaking out of frames'); ?>|<?php echo __('Inspecting Website'); ?>|<?php echo __('Framebreaker test passed'); ?>|<?php echo __('Error').' : '; ?>'/>
<div id="check" class="frambreakmain">
	<div class='textleft'>
		<span id='framebreakimg'></span>
		<span class="framebreaktext"><?php echo __('Inspecting Framebreaker'); ?></span>
	</div>
	<div class='textleft'>
		<span id="check_final"><?php echo __('Inspecting Website'); ?></span>
	</div>
</div>
<?php //Frame Break Code Over ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Text Ads');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">

	<?php // Text ad add/edit form starts here ?>
	<?php echo $this->Form->create('Textadd',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'textad','action'=>'addaction')));?>
		<div class="form-box">
				<?php if(isset($textaddata['Textadd']["textadd_id"])){
					$action='addaction/'.$textaddata['Textadd']["textadd_id"];
				}else{
					$action='addaction';
				}?>
				<div class="form-row">
					<div class="form-col-1"><?php echo __('Available Credits')?> :</div>
					<div class="form-col-2 form-text"><?php echo $addtext_text_credit;?></div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __('Title')?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('title' ,array('type'=>'text', "class"=>"formtextbox", 'label'=>false, 'div' => false, 'value'=>$textaddata['Textadd']["title"]));?>
					
					<span class="helptooltip vtip" title="<?php echo __('Maximum characters allowed').' : '.$SITECONFIG["max_title"]."<br>"; ?><?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?), Dollar sign($) and Dot(.)') ?>"></span>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __('Allocate Credits')?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('credit' ,array('type'=>'text', "class"=>"formtextbox", 'label'=>false, 'value'=>$textaddata['Textadd']["credit"]));?></div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __('Description Line 1')?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('desc1' ,array('type'=>'text', "class"=>"formtextbox", 'label'=>false, 'div' => false, 'value'=>$textaddata['Textadd']["desc1"]));?>
					
					<span class="helptooltip vtip" title="<?php echo __('Maximum characters allowed').' : '.$SITECONFIG["max_desc1"]."<br>"; ?><?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?), Dollar sign($) and Dot(.)') ?>"></span>
					
					</div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __('Description Line 2')?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('desc2' ,array('type'=>'text', "class"=>"formtextbox", 'label'=>false, 'div' => false, 'value'=>$textaddata['Textadd']["desc2"]));?>
					
					<span class="helptooltip vtip" title="<?php echo __('Maximum characters allowed').' : '.$SITECONFIG["max_desc2"]."<br>"; ?><?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?), Dollar sign($) and Dot(.)') ?>"></span>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __('Site URL')?> : <span class="required">*</span></div>
					<?php if($textaddata['Textadd']["urllink"]=='') $textaddata['Textadd']["urllink"]='http://' ?>
					<div class="form-col-2"><?php echo $this->Form->input('urllink' ,array('type'=>'text', "class"=>"formtextbox txtframebreaker", 'label'=>false, 'value'=>$textaddata['Textadd']["urllink"]));?></div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __('Show URL in Zone')?> : </div>
					<div class="form-col-2 form-text">
						<?php if($textaddata['Textadd']["showurl"]){$checked="checked";}else{$checked="";}
							echo $this->Form->input('showurl', array('type' => 'checkbox', 'div'=>'true', 'label' =>'', 'checked'=>$checked));?>
					</div>
				</div>
				<?php if($AdvertisementTextAddCaptcha){ ?>
					<div class="form-row captchrow">
						<div class="form-col-1"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
							<?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formcapthcatextbox", 'div'=>false, 'label'=>false));?>
							<span><?php echo __('Code')?> :</span>
							<?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'AdvertisementTextAddCaptcha','vspace'=>2, "style"=>"vertical-align: middle")); ?> 
							<a href="javascript:void(0);" onclick="javascript:document.images.AdvertisementTextAddCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "style"=>"vertical-align: middle"));?></a>
					</div>
				<?php }?>
		</div>
		<div class="formbutton">
			<?php echo $this->Js->link(__("Back"), array('controller'=>'textad', "action"=>"index"), array(
				'update'=>'#textadpage',
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button',
				'style'=>'float:left',
				'controller'=>'textad',
				'action'=>'index',
				'url'=> array('controller' => 'textad', 'action' => 'index')
			));?>
			<input type="button" value="<?php echo __('Submit'); ?>" class="button" onclick="return checkframebreaker(0,'<?php echo $SITEURL; ?>','framebreaker');" />
			<?php echo $this->Js->submit(__('Submit'), array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'div'=>false,
			  'class'=>'button framebreaker',
			  'style'=>'display:none',
			  'controller'=>'textad',
			  'action'=>$action,
			  'url'=> array('controller'=>'textad', 'action'=>$action)
			));?>
		</div>
		<div class="clear-both"></div>
		<?php echo $this->Form->end();?>
		<?php // Text ad add/edit form ends here ?>

</div>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>