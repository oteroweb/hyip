<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($EnableTextad==1) { ?>
<?php if(!$ajax){ ?>
<div id="textadpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('My Text Ad Plans');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#textadpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'textad', 'action'=>'myplans')
	));
	$currentpagenumber=$this->params['paging']['Ptextad']['page'];
	?>
		<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			
			<?php // My Text Ad Plans table starts here ?>
			<div class="divtable">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam">
							<?php 
							if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
							echo $this->Js->link(__("Id"), array('controller'=>'textad', "action"=>"myplans/0/id/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#textadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Id')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Plan"), array('controller'=>'textad', "action"=>"myplans/0/plan_id/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#textadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Plan')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Title"), array('controller'=>'textad', "action"=>"myplans/0/title/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#textadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Title')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Purchase Date"), array('controller'=>'textad', "action"=>"myplans/0/pruchase_date/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#textadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Purchase Date')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Approved Date"), array('controller'=>'textad', "action"=>"myplans/0/approve_date/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#textadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Approved Date')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Expiry Date"), array('controller'=>'textad', "action"=>"myplans/0/expire_date/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#textadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Expiry Date')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Displayed"), array('controller'=>'textad', "action"=>"myplans/0/display_counter/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#textadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Displayed')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Clicked"), array('controller'=>'textad', "action"=>"myplans/0/click_counter/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#textadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Clicked')
							));?>
						</div>
						<div class="divth textcenter vam"><?php echo __("Status"); ?></div>
						<div class="divth textcenter vam"><?php echo __("Action"); ?></div>
					</div>
				</div>
				<div class="divtbody">
					<?php $i=1;
					foreach ($textads as $textad):
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam"><?php echo $textad['Ptextad']['id'];?></div>
							<div class="divtd textcenter vam"><?php echo $textad['Ptextadplan']['plan_name']; ?></div>
							<div class="divtd textcenter vam">
								<a href="<?php echo $textad['Ptextad']['site_url'];?>" target="_blank"><?php echo $textad['Ptextad']['title']; ?></a>
							</div>
							<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $textad['Ptextad']['pruchase_date']); ?></div>
							<div class="divtd textcenter vam"><?php if($textad['Ptextad']['expire_date'] != '0000-00-00'){ echo $this->Time->format($SITECONFIG["timeformate"], $textad['Ptextad']['approve_date']); } else { echo '-';} ?></div>
							<div class="divtd textcenter vam"><?php if($textad['Ptextad']['expire_date'] != '0000-00-00'){ echo $this->Time->format($SITECONFIG["timeformate"], $textad['Ptextad']['expire_date']); } else { echo '-';}?></div>
							<div class="divtd textcenter vam"><?php echo $textad['Ptextad']['display_counter'];?></div>
							<div class="divtd textcenter vam"><?php echo $textad['Ptextad']['click_counter'];?></div>
							<div class="divtd textcenter vam">
								<?php if ($textad['Ptextad']['expire_date'] < date('Y-m-d H:i:s') && $textad['Ptextad']['expire_date'] != '0000-00-00' && $textad['Ptextad']['expire_date'] != NULL) { 
									echo __("Expired");
								}
								elseif($textad['Ptextad']['status']==1) {
									echo __("Running");
								}
								elseif($textad['Ptextad']['status']==0){
									echo __("Pending");
								}
								?>
							</div>
							<div class="divtd textcenter vam">
								<?php
									echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit Text Ad'))), array('controller'=>'textad', "action"=>"myplanadd/".$textad['Ptextad']['id']), array(
										'update'=>'#textadpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false,
										'class'=>'vtip',
										'title'=>__('Edit Text Ad')
									));
								?>
							</div>
						</div>
					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($textads)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // My Text Ad Plans table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Ptextad']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Ptextad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'textad','action'=>'myplans/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#textadpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'textad',
						  'action'=>'myplans/rpp',
						  'url'=> array('controller' => 'textad', 'action' => 'myplans/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
			</div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<div class="formbutton">
			<?php echo $this->Js->link(__("Back"), array('controller'=>'textad', "action"=>"plans/0"), array(
			'update'=>'#textadpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'button',
			'style'=>'float:left'
			));?>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		
</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>