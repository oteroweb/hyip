<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($EnableTextad==1) { ?>
<?php if(!$ajax){ ?>
<div id="textadpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Text Ad Details");?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
		<div class="height10"></div>	
			
			<?php // Text Ad Details edit form starts here ?>
			<?php echo $this->Form->create('Ptextad',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'textad','action'=>'myplanaddaction')));?>
			<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$textaddata['Ptextad']["id"], 'label' => false)); ?>
			<div class="formgrid">
					
					<div class="form-row">
						<div class="form-col-1"><?php echo __("Title");?> : <span class="required">*</span></div>
						<?php echo $this->Form->input('title', array('type'=>'text', 'label' => false, 'value'=>$textaddata['Ptextad']["title"], 'class'=>'formtextbox login-from-box-1','div'=>false));?>
						<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)') ?>"></span>
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __("Site URL");?> : <span class="required">*</span></div>
						<?php echo $this->Form->input('site_url', array('type'=>'text', 'label' => false,'div'=>false, 'value'=>$textaddata['Ptextad']["site_url"], 'class'=>'formtextbox'));?>
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __("Description Line 1");?> : <span class="required">*</span></div>
						<?php echo $this->Form->input('description1', array('type'=>'text', 'label'=>false, 'value'=>$textaddata['Ptextad']["description1"],'class'=>'formtextbox login-from-box-1','div'=>false));?>
						<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)') ?>"></span>
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __("Description Line 2");?> : <span class="required">*</span></div>
						<?php echo $this->Form->input('description2', array('type'=>'text', 'label'=>false, 'value'=>$textaddata['Ptextad']["description2"], 'class'=>'formtextbox login-from-box-1','div'=>false));?>
						<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)') ?>"></span>
						
					</div>
					<div class="form-row">
						<div class="form-col-1 form-text"><?php echo __("Show URL in Zone");?> : </div>
						<?php if($textaddata['Ptextad']["showurl"]){$checked="checked";}else{$checked="";}
								echo $this->Form->input('showurl', array('type' => 'checkbox', 'div' => false, 'label' =>'', 'checked'=>$checked));?>
								<span class="helptooltip vtip" title="<?php echo __('Choose whether the site URL will be shown with the text ad or not.') ?>"></span>
						
					</div>
			</div>
			<div class="height10"></div>
			<div class="formbutton">
				<?php echo $this->Js->link(__("Back"), array('controller'=>'textad', "action"=>"plans"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'button',
					'style'=>'float:left'
				));?>
				<?php echo $this->Js->submit(__('Update'), array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#UpdateMessage',
					'class'=>'button',
					'div'=>false,
					'controller'=>'textad',
					'action'=>'myplanaddaction',
					'url'   => array('controller' => 'textad', 'action' => 'myplanaddaction')
				));?>
			</div>
			<div class="clear-both"></div>
			<?php echo $this->Form->end();?>
			<?php // Text Ad Details edit form ends here ?>
			
		</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>