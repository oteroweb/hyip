<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Text Ads</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Text Ads", array('controller'=>'textad', "action"=>"index"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("View Credits", array('controller'=>'textad', "action"=>"textcredit"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Text Ad Plans", array('controller'=>'textad', "action"=>"plan"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="textadpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Text_Ads#Text_Ads" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<?php echo $this->Form->create('Textadd',array('type' => 'post', 'type' =>'file', 'onsubmit' => 'return true;','url'=>array('controller'=>'textad','action'=>'textadaddaction')));?>
	<?php if(isset($textadddata['Textadd']["textadd_id"])){
		echo $this->Form->input('textadd_id', array('type'=>'hidden', 'value'=>$textadddata['Textadd']["textadd_id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
	<?php
	echo $this->Form->input('max_title', array('type'=>'hidden', 'value'=>$SITECONFIG["max_title"], 'label' => false));
	echo $this->Form->input('max_desc1', array('type'=>'hidden', 'value'=>$SITECONFIG["max_desc1"], 'label' => false));
	echo $this->Form->input('max_desc2', array('type'=>'hidden', 'value'=>$SITECONFIG["max_desc2"], 'label' => false));
	?>
	
	<div class="frommain">
		
		<?php if(!isset($textadddata['Textadd']["textadd_id"])){?>
		<div class="fromnewtext">Status :</div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
					<?php 
					echo $this->Form->input('status', array(
						'type' => 'select',
						'options' => array('1'=>'Active', '0'=>'Inactive'),
						'selected' => $textadddata['Textadd']["status"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
					?>
				</label>
			</div>
		</div>
		<?php }?>
	
		<div class="fromnewtext">Title :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('title', array('type'=>'text', 'value'=>$textadddata['Textadd']["title"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Description Line 1 :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('desc1', array('type'=>'text', 'value'=>$textadddata['Textadd']["desc1"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Description Line 2 :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('desc2', array('type'=>'text', 'value'=>$textadddata['Textadd']["desc2"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">URL :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('urllink', array('type'=>'text', 'value'=>$textadddata['Textadd']["urllink"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
	
		<div class="fromnewtext">Allocate Credits :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('credit', array('type'=>'text', 'value'=>$textadddata['Textadd']["credit"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Show URL With Text Ad : </div>
		<div class="fromborderdropedown3 checkboxlist">
			<?php if($textadddata['Textadd']["showurl"]){$checked="checked";}else{$checked="";}
			echo $this->Form->input('showurl', array('type' => 'checkbox', 'div'=>true, 'label' =>"", 'checked'=>$checked));?>
		</div>
		
	
	<div class="formbutton">
		<?php echo $this->Js->submit('Submit', array(
		  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
		  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		  'update'=>'#UpdateMessage',
		  'class'=>'btnorange',
		  'div'=>false,
		  'controller'=>'textad',
		  'action'=>'textadaddaction',
		  'url'   => array('controller' => 'textad', 'action' => 'textadaddaction')
		));?>
		<?php echo $this->Js->link("Back", array('controller'=>'textad', "action"=>"index"), array(
			'update'=>'#textadpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'div'=>false,
			'class'=>'btngray'
		));?>
	</div>
<?php echo $this->Form->end();?>

<?php if(!$ajax){?>
</div><!--#textadpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>