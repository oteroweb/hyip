<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Text Ads</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Text Ads", array('controller'=>'textad', "action"=>"index"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("View Credits", array('controller'=>'textad', "action"=>"textcredit"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Text Ad Plans", array('controller'=>'textad', "action"=>"plan"), array(
					'update'=>'#textadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="textadpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Text_Ads#Text_Ad_Plans" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Ptextadplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'textad','action'=>'plan')));?>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>Search By :</span>
			<span>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
						<?php 
						echo $this->Form->input('searchby', array(
							  'type' => 'select',
							  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'plan_name'=>'Plan Name', 'price'=>'Price', 'days'=>'Days/Credits', 'active'=>'Active Plans', 'inactive'=>'Inactive Plans'),
							  'selected' => $searchby,
							  'class'=>'',
							  'label' => false,
							  'style' => '',
							  'onchange'=>'if($(this).val()=="inactive" || $(this).val()=="active"){$(".SearchFor").hide(500);}else{$(".SearchFor").show(500);}'
						));
						?>
						</label>
					</div>
				</div>
			</span>
		</div>
		 <div class="fromboxmain">
			<span class="SearchFor" <?php if($searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>>Search For :</span>
			<span class="SearchFor" <?php if($searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#textadpage',
				'class'=>'searchbtn',
				'controller'=>'textad',
				'action'=>'textadplan',
				'url'=> array('controller' => 'textad', 'action' => 'plan')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->


	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#textadpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'textad', 'action'=>'plan')
	));
	$currentpagenumber=$this->params['paging']['Ptextadplan']['page'];
	?>
	
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('textad',$SubadminAccessArray) || in_array('textad/adminpanel_planadd', $SubadminAccessArray)){ ?>
		<?php echo $this->Js->link("+ Add New", array('controller'=>'textad', "action"=>"planadd"), array(
			'update'=>'#textadpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btnorange'
		));?>
	<?php } ?>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Ptextadplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'textad','action'=>'plan')));?>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'textad', "action"=>"plan/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#textadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Plan Name', array('controller'=>'textad', "action"=>"plan/0/plan_name/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#textadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Name'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Price', array('controller'=>'textad', "action"=>"plan/0/price/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#textadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Price'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Days/Credits', array('controller'=>'textad', "action"=>"plan/0/days/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#textadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Days/Credits'
					));?>
				</div>
                <div><?php echo 'Action';?></div>
			</div>
			<?php foreach ($textadplans as $textadplan): ?>
				<div class="tablegridrow">
					<div><?php echo $textadplan['Ptextadplan']['id']; ?></div>
					<div><?php echo $textadplan['Ptextadplan']['plan_name']; ?></div>
                    <div>$<?php echo $textadplan['Ptextadplan']['price']; ?></div>
					<div><?php echo $textadplan['Ptextadplan']['days']; ?></div>
					<div>
					<div class="actionmenu">
					  <div class="btn-group">
						<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
						Action <span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
							
							<?php if(!isset($SubadminAccessArray) || in_array('textad',$SubadminAccessArray) || in_array('textad/adminpanel_planadd/$', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Text Ads Plan'))." Edit", array('controller'=>'textad', "action"=>"planadd/".$textadplan['Ptextadplan']['id']), array(
									'update'=>'#textadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('textad',$SubadminAccessArray) || in_array('textad/adminpanel_planstatus', $SubadminAccessArray)){ ?>
							<li>
								<?php
								if($textadplan['Ptextadplan']['status']==0){
									$statusaction='1';
									$statusicon='red-icon.png';
									$statustext='Activate';
								}else{
									$statusaction='0';
									$statusicon='blue-icon.png';
									$statustext='Inactivate';}
								echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'textad', "action"=>"planstatus/".$statusaction."/".$textadplan['Ptextadplan']['id']."/".$currentpagenumber), array(
									'update'=>'#textadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
							<?php } ?>
							<?php if(!isset($SubadminAccessArray) || in_array('textad',$SubadminAccessArray) || in_array('textad/adminpanel_planmember', $SubadminAccessArray)){ ?>
							<li>
								<?php 
								echo $this->Js->link($this->html->image('view-members.png', array('alt'=>'View Members'))." View Members", array('controller'=>'textad', "action"=>"planmember/".$textadplan['Ptextadplan']['id']), array(
									'update'=>'#textadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								)); ?>
							</li>
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('textad',$SubadminAccessArray) || in_array('textad/adminpanel_planremove', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Text Ads Plan'))." Delete", array('controller'=>'textad', "action"=>"planremove/".$textadplan['Ptextadplan']['id']."/".$currentpagenumber), array(
									'update'=>'#textadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'',
									'confirm'=>"Do You Really Want to Delete This Text Ads Plan?"
								));?>
							</li>
							<?php } ?>
						</ul>
					  </div>
					</div>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($textadplans)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Ptextadplan']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Ptextadplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'textad','action'=>'plan/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#textadpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'textad',
			  'action'=>'plan/rpp',
			  'url'   => array('controller' => 'textad', 'action' => 'plan/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#textadpage over-->
</div>
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>