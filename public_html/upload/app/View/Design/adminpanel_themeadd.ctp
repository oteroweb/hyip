<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Member Themes</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Themes", array('controller'=>'design', "action"=>"index"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Admin Theme", array('controller'=>'design', "action"=>"admintheme"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="designpage">
<?php }?>
<div class="tab-innar">
	<ul>
		<li>
			<?php echo $this->Js->link("Themes", array('controller'=>'design', "action"=>"index"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>'active'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Member Menu", array('controller'=>'design', "action"=>"thememembermenu"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Public Menu", array('controller'=>'design', "action"=>"themepublicmenu"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Advertisement Settings", array('controller'=>'design', "action"=>"advertisement"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Settings", array('controller'=>'design', "action"=>"themesetting"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
	</ul>
</div>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Themes#Themes" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>
	
<div class="backgroundwhite">
				
	<?php echo $this->Form->create('Theme',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'themeaddaction')));?>
	<?php if(isset($themedata['Theme']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$themedata['Theme']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
		
	<div class="frommain">
		
		<div class="fromnewtext">Theme Path : </div>
		<div class="fromborderdropedown3">app/View/Themed/</div>
		
			
		<div class="fromnewtext">Theme Name :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('theme_name', array('type'=>'text', 'value'=>$themedata['Theme']['theme_name'], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Thumb Image Full Name :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('thumb', array('type'=>'text', 'value'=>$themedata['Theme']['thumb'], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Display in Theme Change Option : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('memberstatus', array(
						'type' => 'select',
						'options' => array('1'=>'Yes', '0'=>'No'),
						'selected' => $themedata['Theme']['memberstatus'],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
			'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'update'=>'#UpdateMessage',
			'class'=>'btnorange',
			'div'=>false,
			'controller'=>'design',
			'action'=>'themeaddaction',
			'url'   => array('controller' => 'design', 'action' => 'themeaddaction')
		  ));?>
		  <?php echo $this->Js->link("Back", array('controller'=>'design', "action"=>"index"), array(
			  'update'=>'#designpage',
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'escape'=>false,
			  'class'=>'btngray'
		  ));?>
		</div>
	
	</div>
	<?php echo $this->Form->end();?>
	
	
</div>
<?php if(!$ajax){?>
</div><!--#designpage over-->
</div>
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>