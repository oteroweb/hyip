<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 03-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Member Themes</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Themes", array('controller'=>'design', "action"=>"index"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Admin Theme", array('controller'=>'design', "action"=>"admintheme"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="designpage">
<?php }?>
<div class="tab-innar">
	<ul>
		<li>
			<?php echo $this->Js->link("Themes", array('controller'=>'design', "action"=>"index"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Member Side", array('controller'=>'design', "action"=>"thememembermenu"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>'active'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Public Side", array('controller'=>'design', "action"=>"themepublicmenu"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Advertisement Settings", array('controller'=>'design', "action"=>"advertisement"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Settings", array('controller'=>'design', "action"=>"themesetting"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			));?>
		</li>
	</ul>
</div>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Themes#Member_Side" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<div id="Xgride-bg">
    <div class="Xpadding10">
	<div class="greenbottomborder">
		<div class="height10"></div>
<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'thememembersubmenu')));?>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_thememembersubmenu', $SubadminAccessArray)){ ?>
		<?php echo $this->Js->submit('Update Submenu', array(
		  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
		  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		  'update'=>'#UpdateMessage',
		  'class'=>'btngray',
		  'div'=>false,
		  'controller'=>'design',
		  'action'=>'thememembersubmenu',
		  'url'   => array('controller' => 'design', 'action' => 'thememembersubmenu')
		));?>
	<?php } ?>
	</div>
	<div class="clear-both"></div>
	
	<div class="tablegrid nomobilecss">
			<div class="tablegridheader">
				<div><?php echo 'Id';?></div>
				<div class="textleft"><?php echo 'Menu Name';?></div>
				<div><?php echo 'Status';?></div>
			</div>
			<?php $i = 0;foreach ($menus as $menu): $i++?>
				<?php if($menu['Menu']['menu_name']=='Banner Ads'){?>
					</div>
					<div class="tablegrid">
						<div class="tablegridheader nofixheader">
							<div><?php echo 'Id';?></div>
							<div class="textleft"><?php echo 'Element Name';?></div>
							<div><?php echo 'Status';?></div>
						</div>
				<?php }?>
				
				<div class="tablegridrow">
				    <div><?php echo $menu['Menu']['id']; ?></div>
					<div class="textleft">
						<?php if(in_array($menu['Menu']['menu_name'], array('Account Settings','Member Tools','Finance','Advertisement','Promotional Tools'))){?>
							<?php echo $this->html->image('updown.png', array('alt'=>''));?> <a href="javascript:void(0)" onclick="if($('.class<?php echo $i;?>').css('display')=='none'){$('.class<?php echo $i;?>').fadeIn('slow')}else{$('.class<?php echo $i;?>').fadeOut(500)};">
							<?php echo ($menu['Menu']['menu_name']=="Account Settings")?"Account Activity":$menu['Menu']['menu_name']; ?>
							</a>
						<?php }else{
							echo $menu['Menu']['menu_name'];
						}?>
					</div>
					<?php /* <td  align="center" valign="middle"><?php echo $menu['Menu']['menu_order']; ?></td>
					<td  align="center" valign="middle">
						<?php 
						if($menu['Menu']['menu_order']==1 && $maxo>1)
						{
							echo $this->Js->link($this->html->image('down.png', array('alt'=>'')), array('controller'=>'design', "action"=>"menuformaction/down/member/".$menu['Menu']['menu_order']."/".$menu['Menu']['id']), array(
								'update'=>'#designpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'Move Down'
							));
						}
						elseif($menu['Menu']['menu_order']==$maxo && $maxo>1)
						{
							echo $this->Js->link($this->html->image('up.png', array('alt'=>'')), array('controller'=>'design', "action"=>"menuformaction/up/member/".$menu['Menu']['menu_order']."/".$menu['Menu']['id']), array(
								'update'=>'#designpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'Move Up'
							));
						}
						elseif($menu['Menu']['menu_order']<$maxo && $menu['Menu']['menu_order']>$mino)
						{
							echo $this->Js->link($this->html->image('down.png', array('alt'=>'')), array('controller'=>'design', "action"=>"menuformaction/down/member/".$menu['Menu']['menu_order']."/".$menu['Menu']['id']), array(
								'update'=>'#designpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'Move Down'
							));
							echo '&nbsp;';
							echo $this->Js->link($this->html->image('up.png', array('alt'=>'')), array('controller'=>'design', "action"=>"menuformaction/up/member/".$menu['Menu']['menu_order']."/".$menu['Menu']['id']), array(
								'update'=>'#designpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'Move Up'
							));
						}
						?>
					</td> */?>
					<div>
						
						<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_thememembersubmenu', $SubadminAccessArray)){ ?>
						<?php 
						if($menu['Menu']['status']==0){
							$statusaction='1';
							$statusicon='red-icon.png';
							$statustext='Activate Menu';
						}else{
							$statusaction='0';
							$statusicon='blue-icon.png';
							$statustext='Inactivate Menu';}
						
						echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)), array('controller'=>'design', "action"=>"menuformaction/status/member/".$statusaction."/".$menu['Menu']['id']), array(
							'update'=>'#designpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>$statustext
						));
						?>
						<?php } ?>
					</div>
				</div>
				<?php if($menu['Menu']['menu_name']=='Account Settings'){?>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkprofile' type="checkbox" name="submenu[]" value="Profile" <?php if(in_array('Profile', $themesubmenu)){echo 'checked="checked"';}?> /><label for="checkprofile"> Profile</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checksecurity' type="checkbox" name="submenu[]" value="Security" <?php if(in_array('Security', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checksecurity'> Security</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkUpgradeAccount' type="checkbox" name="submenu[]" value="Upgrade Account" <?php if(in_array('Upgrade Account', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkUpgradeAccount'> Upgrade Account</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkrateus' type="checkbox" name="submenu[]" value="Rate Us" <?php if(in_array('Rate Us', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkrateus'> Rate Us</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkActivityLogs' type="checkbox" name="submenu[]" value="Activity Logs" <?php if(in_array('Activity Logs', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkActivityLogs'> Activity Logs</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkAdminMessages' type="checkbox" name="submenu[]" value="Admin Messages" <?php if(in_array('Admin Messages', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkAdminMessages'> Admin Messages</label></div>
						<div>&nbsp;</div>
					</div>
					<?php }elseif($menu['Menu']['menu_name']=='Member Tools'){?>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkViewPTC' type="checkbox" name="submenu[]" value="View PTC" <?php if(in_array('View PTC', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkViewPTC'> View PTC</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkURLRotator' type="checkbox" name="submenu[]" value="URL Rotator" <?php if(in_array('URL Rotator', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkURLRotator'> URL Rotator</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkURLshortener' type="checkbox" name="submenu[]" value="URL Shortener" <?php if(in_array('URL Shortener', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkURLshortener'> URL Shortener</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkContest' type="checkbox" name="submenu[]" value="Contest" <?php if(in_array('Contest', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkContest'> Contest</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkJackpot' type="checkbox" name="submenu[]" value="Jackpot" <?php if(in_array('Jackpot', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkJackpot'> Jackpot</label></div>
						<div>&nbsp;</div>
					</div>
				<?php }elseif($menu['Menu']['menu_name']=='Finance'){ ?>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id="checkAddFund" type="checkbox" name="submenu[]" value="Add Fund" <?php if(in_array('Add Fund', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkAddFund'> Add Fund</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkPurchasePosition' type="checkbox" name="submenu[]" value="Purchase Position" <?php if(in_array('Purchase Position', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkPurchasePosition'> Purchase Position</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkEarningHistory' type="checkbox" name="submenu[]" value="Earning History" <?php if(in_array('Earning History', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkEarningHistory'> Earning History</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkPaymentHistory' type="checkbox" name="submenu[]" value="Payment History" <?php if(in_array('Payment History', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkPaymentHistory'> Payment History</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkWithdrawalRequest' type="checkbox" name="submenu[]" value="Withdrawal Request" <?php if(in_array('Withdrawal Request', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkWithdrawalRequest' > Request Withdrawal</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkWithdrawalHistory' type="checkbox" name="submenu[]" value="Withdrawal History" <?php if(in_array('Withdrawal History', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkWithdrawalHistory'> Withdrawal History</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkBalanceTransfer' type="checkbox" name="submenu[]" value="Balance Transfer" <?php if(in_array('Balance Transfer', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkBalanceTransfer'> Balance Transfer</label></div>
						<div>&nbsp;</div>
					</div>
				<?php }elseif($menu['Menu']['menu_name']=='Advertisement'){ ?>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkBannerAds' type="checkbox" name="submenu[]" value="Banner Ads" <?php if(in_array('Banner Ads', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkBannerAds'> Banner Ads</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkTextAds' type="checkbox" name="submenu[]" value="Text Ads" <?php if(in_array('Text Ads', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkTextAds'> Text Ads</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkSoloAds' type="checkbox" name="submenu[]" value="Solo Ads" <?php if(in_array('Solo Ads', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkSoloAds'> Solo Ads</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkLoginAdPlans' type="checkbox" name="submenu[]" value="Login Ad Plans" <?php if(in_array('Login Ad Plans', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkLoginAdPlans'> Login Ad Plans</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkPPC' type="checkbox" name="submenu[]" value="PPC" <?php if(in_array('PPC', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkPPC'> PPC</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkPTC' type="checkbox" name="submenu[]" value="PTC" <?php if(in_array('PTC', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkPTC'> PTC</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkBusinessDirectory' type="checkbox" name="submenu[]" value="BusinessDirectory" <?php if(in_array('BusinessDirectory', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkBusinessDirectory'> BusinessDirectory</label></div>
						<div>&nbsp;</div>
					</div>
					
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkMyWebsites' type="checkbox" name="submenu[]" value="My Websites" <?php if(in_array('My Websites', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkMyWebsites'>My Websites</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkWebsiteCreditPlans' type="checkbox" name="submenu[]" value="Website Credit Plans" <?php if(in_array('Website Credit Plans', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkWebsiteCreditPlans'> Website Credit Plans</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkStartSurfing' type="checkbox" name="submenu[]" value="Start Surfing" <?php if(in_array('Start Surfing', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkStartSurfing'> Start Surfing</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkStartPage' type="checkbox" name="submenu[]" value="Start Page" <?php if(in_array('Start Page', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkStartPage'> Start Page</label></div>
						<div>&nbsp;</div>
					</div>
					
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkPPCTextAds' type="checkbox" name="submenu[]" value="PPC Text Ads" <?php if(in_array('PPC Text Ads', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkPPCTextAds'> PPC Text Ads</label></div>
						<div>&nbsp;</div>
					</div>
					
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkCreditStatement' type="checkbox" name="submenu[]" value="Credit Statement" <?php if(in_array('Credit Statement', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkCreditStatement'> Credit Statement</label></div>
						<div>&nbsp;</div>
					</div>
				<?php }elseif($menu['Menu']['menu_name']=='Promotional Tools'){?>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkPromotionalBanners' type="checkbox" name="submenu[]" value="Banner" <?php if(in_array('Banner', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkPromotionalBanners'> Promotional Banners</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkDynamicBanners' type="checkbox" name="submenu[]" value="Dynamic Banners" <?php if(in_array('Dynamic Banners', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkDynamicBanners' > Dynamic Banners</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkSplashPages' type="checkbox" name="submenu[]" value="Splash Pages" <?php if(in_array('Splash Pages', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkSplashPages'> Splash Pages</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkLandingPages' type="checkbox" name="submenu[]" value="Landing Pages" <?php if(in_array('Landing Pages', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkLandingPages'> Landing Pages</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkTextLinks' type="checkbox" name="submenu[]" value="Text Links" <?php if(in_array('Text Links', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkTextLinks'> Text Links</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkPromotionalEmails' type="checkbox" name="submenu[]" value="Promotional Emails" <?php if(in_array('Promotional Emails', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkPromotionalEmails'> Promotional Emails</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkTellFriends' type="checkbox" name="submenu[]" value="Tell Friends" <?php if(in_array('Tell Friends', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkTellFriends'> Tell Friends</label></div>
						<div>&nbsp;</div>
					</div>
					<div class="tablegridrow class<?php echo $i;?>" style="display:none;">
						<div>&nbsp;</div>
						<div class="textleft checkbox"><input id='checkMyReferrals' type="checkbox" name="submenu[]" value="My Referrals" <?php if(in_array('My Referrals', $themesubmenu)){echo 'checked="checked"';}?> /><label for='checkMyReferrals'> My Referrals</label></div>
						<div>&nbsp;</div>
					</div>
				<?php }?>
				
			<?php endforeach; ?>
	</div>
<?php echo $this->Form->end();?>
    <div class="height10"></div>
	</div>
	</div>
</div>
<?php }else{
?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#designpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>