<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Member Themes</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Themes", array('controller'=>'design', "action"=>"index"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Admin Theme", array('controller'=>'design', "action"=>"admintheme"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="designpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>

<div class="tab-innar">
	<ul>
		<li>
			<?php echo $this->Js->link("Themes", array('controller'=>'design', "action"=>"index"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>'active'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Member Side", array('controller'=>'design', "action"=>"thememembermenu"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Public Side", array('controller'=>'design', "action"=>"themepublicmenu"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Advertisement Settings", array('controller'=>'design', "action"=>"advertisement"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Settings", array('controller'=>'design', "action"=>"themesetting"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
	</ul>
</div>

<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Themes#Themes" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<?php
$this->Paginator->options(array(
	'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
	'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
	'update' => '#designpage',
	'evalScripts' => true,
	'url'=> array('controller'=>'design', 'action'=>'index')
));
$currentpagenumber=$this->params['paging']['Theme']['page'];
?>

<div id="Xgride-bg" class="marginnone greenbottomborder">
    
	<div class="actmain">
		<div class="act_them_box">
			<img src="<?php echo Router::url('/', true)."theme/".$activetheme["Theme"]["theme_name"]."/img/".$activetheme["Theme"]["thumb"];?>" alt='' align="absmiddle" style="max-width:100%;" />
		</div>
		<div class="activtext">Active Theme Name : <?php echo $activetheme["Theme"]["theme_name"];?></div>
	</div>
	
	<div class="theam_right">
		<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
		<div class="addnew-button">
		<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_themeadd',$SubadminAccessArray)){ ?>
				<?php echo $this->Js->link("+ Add New", array('controller'=>'design', "action"=>"themeadd"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btnorange'
				));?>
		<?php } ?>
		   </div>
            <div class="clearboth"></div>
			<?php
				$i = 0;
				foreach ($themes as $theme):
				?>
					
					<div class="themboxmain">
						<div class="themtitle"><?php echo $theme['Theme']['theme_name']; ?></div>
						<div class="themtitle-right">
							<div class="btn-group">
								<button data-toggle="dropdown" type="button" class="btn btn-default btn-xs dropdown-toggle">
									Action <span class="caret"></span>
								</button>
								<ul role="menu" class="dropdown-menu">
									<li>
										<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_themeadd/$',$SubadminAccessArray)){ ?>
										<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Theme')).' Edit Theme', array('controller'=>'design', "action"=>"themeadd/".$theme['Theme']['id']), array(
											'update'=>'#designpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'class'=>'vtip',
											'title'=>'Edit Theme'
										));?>
									<?php }else{ 
										echo '&nbsp;';
									} ?>
									</li>
									<li>
										<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_themestatus',$SubadminAccessArray)){ ?>
											<?php 
											echo $this->Js->link($this->html->image('red-icon.png', array('alt'=>'Activate Theme')).' Activate Theme', array('controller'=>'design', "action"=>"themestatus/yes/".$theme['Theme']['id']."/".$currentpagenumber), array(
												'update'=>'#designpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'vtip',
												'title'=>'Activate Theme'
											));?>
										<?php }else{ 
											echo '&nbsp;';
										} ?>
									</li>
									<li>
										<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_themeremove',$SubadminAccessArray)){ ?>
											<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Theme')).' Delete Theme', array('controller'=>'design', "action"=>"themeremove/".$theme['Theme']['id']."/".$currentpagenumber), array(
												'update'=>'#designpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'vtip',
												'title'=>'Delete Theme',
												'confirm'=>"Are You Sure?"
											));?>
										<?php }else{ 
											echo '&nbsp;';
										} ?>
									</li>
								</ul>
							</div>
						</div>
						<div class="thembox">
							<img src="<?php echo $SITEURL.'theme/'.$theme["Theme"]["theme_name"].'/img/'.$theme["Theme"]["thumb"];?>" alt="" width="100%" height="100%"/>
						</div>      
					</div>
					
				
			<?php endforeach; ?>	
			
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
</div>
<?php }else{
?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#designpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>