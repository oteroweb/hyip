<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Template Edit</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Template Directory", array('controller'=>'design', "action"=>"template"), array(
					'update'=>'#success',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Layout Directory", array('controller'=>'design', "action"=>"layout"), array(
					'update'=>'#success',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="success">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Template_Edit#Layout_Directory" target="_blank">Help</a></div>
<div class="whitenotebox marginnone"><?php echo $this->html->image("ctp.gif",  array("alt"=>"Directory", "align"=>"absmiddle", 'title'=>'View Directory'));?>&nbsp;Layout Directory/<?php echo $foldername;?>/Layout File</div>


		<div class="tablegrid">
			<div class="tablegridheader">
				<div class="textleft">&nbsp;&nbsp;&nbsp;<?php echo $foldername;?> <?php echo 'Layout File';?></div>
			</div>
		</div>
		<div class="height10"></div>	
        <?php echo $this->Form->create('TemplateEdit',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'updatelayoutfile/'.$foldername.'/'.$filename)));?>
			
			<div class="nameandbox">
				<div class="fromboxhelpmain" style="width:98%">
					<?php echo $this->Form->input('filecontent', array('type'=>'textarea', 'id'=>'filecontent','value'=>$filecontent["content"], 'label' => false,  'div' => false, 'class'=>'from-textarea', 'style'=>'width:99%;height:500px;margin-left:0px;padding:5px 0 0 5px;'));?>
				</div>
			</div>
			<div class="height10"></div>	
			<?php if($foldername!='Under-Construction' && substr($foldername,0,6)!="Admin-"){?>
				<div class="tablegrid">
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $this->Time->format($SITECONFIG["timeformate"], $StatisticLaunchDate);?&gt;</div>
						<div class="textleft">- Launch Date</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $StatisticAlexaRank;?&gt;</div>
						<div class="textleft">- Alexa Rank</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $StatisticTotalMember;?&gt;</div>
						<div class="textleft">- Total Members</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $StatisticMemberCash;?&gt;</div>
						<div class="textleft">- Member Cash Balance</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $StatisticMemberRepurchaseCash;?&gt;</div>
						<div class="textleft">- Member Repurchase Balance</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $StatisticMemberBannerCredits;?&gt;</div>
						<div class="textleft">- Member Banner Credits</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $StatisticMemberSoloCredits;?&gt;</div>
						<div class="textleft">- Member Solo Credits</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $StatisticTotalTextAds;?&gt;</div>
						<div class="textleft">- Total Text Ads</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $StatisticTotalBannerAds;?&gt;</div>
						<div class="textleft">- Total Banners Ads</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $StatisticTotalDiposit;?&gt;</div>
						<div class="textleft">- Total Deposits</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $StatisticTotalPayouts;?&gt;</div>
						<div class="textleft">- Total Payouts</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $StatisticTotalLastPayouts;?&gt;</div>
						<div class="textleft">- Total Last Payouts</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $StatisticTotalCommission;?&gt;</div>
						<div class="textleft">- Total Commissions</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php foreach($StatisticRecentlyJoined as $key=>$value){ echo $value."-".$key; } ?&gt;</div>
						<div class="textleft">- Recently Joined Members</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php foreach($StatisticTopCommissionEarners as $earners){ echo $earners['Member']['user_name']."-".$earners[0]['earning']; } ?&gt;</div>
						<div class="textleft">- Top Commission Earners</div>
					</div>
					<div class="tablegridrow">
						<div class="textleft padding-left">&lt;?php echo $StatisticTotalCommission;?&gt;</div>
						<div class="textleft">- Total Commissions</div>
					</div>
				</div>	
			
			
<?php }?>
			<div class="nameandbox">
				<div class="from-text"></div>
				<div class="fromboxhelpmain">
					<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_layoutupdate', $SubadminAccessArray)){ ?>
					<?php echo $this->Js->submit('Update', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#success',
						'class'=>'btnorange',
						'controller'=>'design',
						'action'=>'updatelayoutfile/'.$foldername.'/'.$filename,
						'url'=> array('controller' => 'design', 'action' => 'updatelayoutfile/'.$foldername.'/'.$filename),
						'div'=>false
					  ));?>
					<?php } ?>
				  <?php echo $this->Js->link("Back", array('controller'=>'design', "action"=>"layout"), array(
					  'update'=>'#success',
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'escape'=>false,
					  'class'=>'btngray'
				  ));?>
				</div>
			</div>
          <?php echo $this->Form->end();?>
          <div class="height5"></div>

<?php if(!$ajax){?>
</div>
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>