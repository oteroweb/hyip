<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Language Edit Phase</div>
<div id="designpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Language_Edit_Phase" target="_blank">Help</a></div>
    <div id="UpdateMessage"></div>
        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#designpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'design', 'action'=>'languageeditphase')
        ));
        $currentpagenumber=$this->params['paging']['Language']['page'];
        ?>
 <div id="Xgride-bg">
    <div class="padding10 backgroundwhite">
	
	<div class="greenbottomborder">
	<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
        <?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_languageeditphaseadd',$SubadminAccessArray)){ ?>
                <?php echo $this->Js->link("+ Add New", array('controller'=>'design', "action"=>"languageeditphaseadd"), array(
                    'update'=>'#designpage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'class'=>'btnorange'
                ));?>
        <?php } ?>
	</div>
	<div class="clear-both"></div>
        
        <div class="tablegrid">
			<div class="tablegridheader">
					<div>Id</div>
					<div>Display Name</div>
					<div>Code</div>
					<div style=" width:100px;">Action</div>
			</div>
                 <?php foreach ($languages as $language): ?>
                    <div class="tablegridrow">
						<div><?php echo $language['Language']['id']; ?></div>
                        <div><?php echo $language['Language']['displayname']; ?></div>
                        <div><?php echo $language['Language']['code']; ?></div>
                        <div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
                                <li>
                                    <?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_languageeditphaseadd/$',$SubadminAccessArray)){ ?>
										<?php if($language['Language']['id']>1){?>
										<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Language'))." Edit Language", array('controller'=>'design', "action"=>"languageeditphaseadd/".$language['Language']['id']), array(
                                            'update'=>'#designpage',
                                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                            'escape'=>false
                                        ));?>
										<?php }?>
                                    <?php }else{
											echo '&nbsp;';
									} ?>
                                </li>
								
								<li>
								<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_languagephase',$SubadminAccessArray)){ ?>
									<?php echo $this->Js->link($this->html->image('search.png', array('alt'=>'Edit Language Phase'))." Edit Language Phase", array('controller'=>'design', "action"=>"languagephase/".$language['Language']['code']), array(
										'update'=>'#designpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));?>
								<?php }else{
										echo '&nbsp;';
								} ?>
								</li>
								
                                <li>
                                	<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_languageeditphaseremove',$SubadminAccessArray)){ ?>
										<?php if($language['Language']['id']>1){?>
										<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Language'))." Delete Language", array('controller'=>'design', "action"=>"languageeditphaseremove/".$language['Language']['id']."/".$currentpagenumber), array(
                                            'update'=>'#designpage',
                                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                            'escape'=>false,
                                            'confirm'=>'Are You Sure?'
                                        ));?>
										<?php }?>
                                    <?php }else{
											echo '&nbsp;';
									} ?>
                                </li>
								
								</ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
                    
		</div>
        <?php 
        if($this->params['paging']['Language']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Language',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'languageeditphase/rpp')));?>
			<div class="resultperpage">
				<label>
					<?php 
					echo $this->Form->input('resultperpage', array(
					  'type' => 'select',
					  'options' => $resultperpage,
					  'selected' => $this->Session->read('pagerecord'),
					  'class'=>'',
					  'label' => false,
					  'div'=>false,
					  'style' => '',
					  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
					));
					?>
				</label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#designpage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'design',
                  'action'=>'languageeditphase/0/rpp',
                  'url'   => array('controller' => 'design', 'action' => 'languageeditphase/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '...', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
		</div>
	</div>
 </div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#designpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>