<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Email Templates</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Site Email Templates", array('controller'=>'design', "action"=>"siteemailtemplate"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Mass Mail Templates", array('controller'=>'design', "action"=>"massemailtemplate"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Site Email Template Categories", array('controller'=>'design', "action"=>"emailcategory"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="designpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Email_Templates#Mass_Mail_Templates" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
    
        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#designpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'design', 'action'=>'massemailtemplate')
        ));
        $currentpagenumber=$this->params['paging']['Admin_template_email']['page'];
        ?>
		
<div id="Xgride-bg">
    <div class="Xpadding10">
		<div class="greenbottomborder">
		<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_massemailtemplateadd', $SubadminAccessArray)){ ?>
            <?php echo $this->Js->link("+ Add New", array('controller'=>'design', "action"=>"massemailtemplateadd"), array(
                'update'=>'#designpage',
                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                'escape'=>false,
                'class'=>'btnorange'
            ));?>
	<?php } ?>
	</div>
	<div class="clear-both"></div>
        
		<div class="tablegrid">
				<div class="tablegridheader">
				    <div><?php echo 'Id';?></div>
                    <div><?php echo 'Description';?></div>
                    <div><?php echo 'Action';?></div>
                </div>
                <?php foreach ($admin_template_emails as $admin_template_email): ?>
                    <div class="tablegridrow">
					    <div><?php echo $admin_template_email['Admin_template_email']['id']; ?></div>
                        <div><?php echo stripslashes($admin_template_email['Admin_template_email']['description']); ?></div>
                        <div>
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
										
												<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_massemailtemplateadd/$',$SubadminAccessArray)){ ?>
												<li>
												<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Email Template'))." Edit", array('controller'=>'design', "action"=>"massemailtemplateadd/".$admin_template_email['Admin_template_email']['id']), array(
													'update'=>'#designpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>''
												));?>
												</li>
												<?php }?>
												
												<?php 
												if($admin_template_email['Admin_template_email']['status']==0){
													$statusaction='1';
													$statusicon='red-icon.png';
													$statustext='Activate';
												}else{
													$statusaction='0';
													$statusicon='blue-icon.png';
													$statustext='Inactivate';}
												
												if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_massemailtemplatestatus',$SubadminAccessArray)){?>
												<li>
												<?php
													echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'design', "action"=>"massemailtemplatestatus/".$statusaction."/".$admin_template_email['Admin_template_email']['id']."/".$currentpagenumber), array(
														'update'=>'#designpage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>''
													));?>
												</li>
												<?php }?>
												
												<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_massemailtemplateremove',$SubadminAccessArray)){ ?>
												<li>
												<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Email Template'))." Delete", array('controller'=>'design', "action"=>"massemailtemplateremove/".$admin_template_email['Admin_template_email']['id']."/".$currentpagenumber), array(
													'update'=>'#designpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>'',
													'confirm'=>"Are You Sure?"
												));?>
												</li>
												<?php }?>
									</ul>
								  </div>
								</div>
                        </div>
                    </div>
                <?php endforeach; ?>
		</div>
		<?php if(count($admin_template_emails)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php 
        if($this->params['paging']['Admin_template_email']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Admin_template_email',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'massemailtemplate/0/rpp')));?>
	    
			<div class="resultperpage">
				<label>
					<?php 
					echo $this->Form->input('resultperpage', array(
					  'type' => 'select',
					  'options' => $resultperpage,
					  'selected' => $this->Session->read('pagerecord'),
					  'class'=>'',
					  'label' => false,
					  'div'=>false,
					  'style' => '',
					  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
					));
					?>
				</label>
			</div>
			
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#designpage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'Design',
                  'action'=>'massemailtemplate/rpp',
                  'url'   => array('controller' => 'design', 'action' => 'massemailtemplate/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '...', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
		</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#designpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>