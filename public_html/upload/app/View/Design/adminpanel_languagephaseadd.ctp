<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Language Edit Phase</div>
<div id="designpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Language_Edit_Phase" target="_blank">Help</a></div>
	<div class="whitenoteboxinner"><b><span class="red-color">*<?php echo "Note";?> :</span></b> <?php echo "Phases Should Be Covered in The .ctp Files Within - '<b>__(</b>' and '<b>)</b>'. Here is an Example : <br /><br /> <b>&lt;?php echo __('DEFAULT PHASE'); ?&gt;</b> Would Result in <b>CUSTOMIZED PHASE</b>"?></div>
    <div id="UpdateMessage"></div>
<div class="backgroundwhite">
	
		<?php echo $this->Form->create('Languagephase',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'languagephaseaddaction')));?>
		<?php echo $this->Form->input('language_id', array('type'=>'hidden', 'value'=>$phasetype, 'label' => false));?>
		
		<div class="frommain">
			
			<div class="fromnewtext">Default Phase (Find) :<span class="red-color">*</span></div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('phase', array('type'=>'textarea', 'label' => false,'div' => false, 'class'=>'from-textarea'));?>
				
			</div>
			
			<div class="fromnewtext">Customized Phase (Replace) :<span class="red-color">*</span></div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('value', array('type'=>'textarea', 'label' => false,'div' => false, 'class'=>'from-textarea'));?>
			</div>
			
			<div class="formbutton">
				<?php echo $this->Js->submit('Submit', array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'class'=>'btnorange',
				  'div'=>false,
				  'controller'=>'design',
				  'action'=>'languagephaseaddaction',
				  'url'   => array('controller' => 'design', 'action' => 'languagephaseaddaction')
				));?>
				<?php echo $this->Js->link("Back", array('controller'=>'design', "action"=>"languagephase/".$phasetype), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'div'=>false,
					'class'=>'btngray'
				));?>
			</div>
			
		</div>
		<?php echo $this->Form->end();?>
	
</div>
	<?php if(!$ajax){?>
</div><!--#designpage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>