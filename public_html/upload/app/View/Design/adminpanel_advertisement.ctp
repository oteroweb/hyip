<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Member Themes</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Themes", array('controller'=>'design', "action"=>"index"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Admin Theme", array('controller'=>'design', "action"=>"admintheme"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="designpage">
<?php }?>
<div class="tab-innar">
	<ul>
		<li>
			<?php echo $this->Js->link("Themes", array('controller'=>'design', "action"=>"index"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Member Side", array('controller'=>'design', "action"=>"thememembermenu"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Public Side", array('controller'=>'design', "action"=>"themepublicmenu"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Advertisement Settings", array('controller'=>'design', "action"=>"advertisement"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>'active'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Settings", array('controller'=>'design', "action"=>"themesetting"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			));?>
		</li>
	</ul>
</div>
<?php echo $this->Javascript->link('allpage');?>
<div class="subbar">Credit Based Advertisements Display Settings</div>
<div class="height10"></div>
<script type="text/javascript">
function show_systemad_code(textfielddisp,textareadisp,viewside)
{
	var textarea=document.getElementById(viewside);
	var numberofads=document.getElementById(textfielddisp).value;
	if(numberofads>0)
    {
	    var codeinsert='';
	    for(var i=1;i<=numberofads;i++)
	    {
		      codeinsert=codeinsert + i +') &lt;span id="advertisement_'+textareadisp+'_'+i+'"&gt;&lt;/span&gt; \n';
		}
	    textarea.innerHTML=codeinsert;
    }
	else
	{
		textarea.innerHTML='Please Enter a Number in The Textbox First.';		
	}
}

function show_ad_code(textfielddisp,textareadisp,viewside)
{
	var textarea=document.getElementById(textareadisp+'_'+viewside);
	var numberofads=document.getElementById(textfielddisp).value;
	
	if(numberofads>0)
    {
	    var codeinsert='';
	    for(var i=1;i<=numberofads;i++)
	    {
		      codeinsert=codeinsert + i +') &lt;span id="'+textareadisp+'_'+i+'"&gt;&lt;/span&gt; \n';
		}

	    textarea.innerHTML=codeinsert;
    }
	else
	{
		textarea.innerHTML='Please Enter a Number in The Textbox First.';		
	}
}
</script>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Themes#Advertisement_Settings" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>
	<?php echo $this->Form->create('Theme',array('type' => 'post', 'id'=>'ThemeForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'advertisementaction')));?>
	<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$id, 'label' => false));?>
	
	<div class="frommain">
		
		<div class="fromnewtext">Number of Text Ads to be Shown on Public Area :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('textshownpubarea', array('type'=>'text', 'value'=>@$textadp, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Number of Text Ads to be Shown Inside Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('textshownmbrarea', array('type'=>'text', 'value'=>@$textadm, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Number of Text Ads to be Shown Inside Member Area." data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Number of Banners (125X125) to be Shown on Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('banner125shownpubarea', array('type'=>'text', 'value'=>@$banner125p, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_systemad_code('ThemeBanner125shownpubarea','125','banner125shownpubarea');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Specify The Minimum Amount That Can Be Added in a Member Account at The Time of Add Fund. <textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='banner125shownpubarea' style='width:198px;'><?php for($banner=1;$banner<=@$banner125p;$banner++){ echo $banner.') <span id=\'advertisement_125_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Number of Banners (125X125) to be Shown Inside Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('banner125shownmbrarea', array('type'=>'text', 'value'=>@$banner125m, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_systemad_code('ThemeBanner125shownmbrarea','125','banner125shownmbrarea');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Specify The Minimum Amount That Can Be Added in a Member Account at The Time of Add Fund.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='banner125shownmbrarea' style='width:198px;'><?php for($banner=1;$banner<=@$banner125m;$banner++){ echo $banner.') <span id=\'advertisement_125_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Number of Banners (468X60) to be Shown on Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('banner468shownpubarea', array('type'=>'text', 'value'=>@$banner468p, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_systemad_code('ThemeBanner468shownpubarea','468','banner468shownpubarea');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Specify The Minimum Amount That Can Be Added in a Member Account at The Time of Add Fund.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='banner468shownpubarea' style='width:198px;'><?php for($banner=1;$banner<=@$banner468p;$banner++){ echo $banner.') <span id=\'advertisement_468_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Number of Banners (468X60) to be Shown Inside Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('banner468shownmbrarea', array('type'=>'text', 'value'=>@$banner468m, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_systemad_code('ThemeBanner468shownmbrarea','468','banner468shownmbrarea');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Specify The Minimum Amount That Can Be Added in a Member Account at The Time of Add Fund.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='banner468shownmbrarea' style='width:198px;'><?php for($banner=1;$banner<=@$banner468m;$banner++){ echo $banner.') <span id=\'advertisement_468_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_advertisementaction',$SubadminAccessArray)){ ?>
		<div class="formbutton">
			<?php echo $this->Js->submit('Update', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'btnorange',
				'div'=>false,
				'controller'=>'design',
				'action'=>'advertisementaction',
				'url'   => array('controller' => 'design', 'action' => 'advertisementaction')
			  ));?>
		</div>
		<?php } ?>
		
				  
	</div>
	<?php echo $this->Form->end();?>
	
	
<div class="subbar">Plan Banner Ads Display Settings</div>	  

<?php echo $this->Form->create('Theme',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'banneradplanaction')));?>
<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$id, 'label' => false)); ?>
	
	<div class="frommain">
		
		<div class="fromnewtext">Rotating Banners (125X125) - Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('rotating_public125', array('type'=>'text', 'value'=>@$rotating_public125, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeRotatingPublic125','rotating_125','pub');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='rotating_125_pub' style='width:198px;'><?php for($banner=1;$banner<=@$rotating_public125;$banner++){ echo $banner.') <span id=\'rotating_125_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Static Banners (125X125) - Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('static_public125', array('type'=>'text', 'value'=>@$static_public125, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeStaticPublic125','static_125','pub');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='static_125_pub' style='width:198px;'><?php for($banner=1;$banner<=@$static_public125;$banner++){ echo $banner.') <span id=\'static_125_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Rotating Banners (125X125) - Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('rotating_member125', array('type'=>'text', 'value'=>@$rotating_member125, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeRotatingMember125','rotating_125','mbr');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='rotating_125_mbr' style='width:198px;'><?php for($banner=1;$banner<=@$rotating_member125;$banner++){ echo $banner.') <span id=\'rotating_125_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Static Banners (125X125) - Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('static_member125', array('type'=>'text', 'value'=>@$static_member125, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeStaticMember125','static_125','mbr');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='static_125_mbr' style='width:198px;'><?php for($banner=1;$banner<=@$static_member125;$banner++){ echo $banner.') <span id=\'static_125_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Rotating Banners (468X60) - Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('rotating_public468', array('type'=>'text', 'value'=>@$rotating_public468, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeRotatingPublic468','rotating_468','pub');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='rotating_468_pub' style='width:198px;'><?php for($banner=1;$banner<=@$rotating_public468;$banner++){ echo $banner.') <span id=\'rotating_468_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Static Banners (468X60) - Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('static_public468', array('type'=>'text', 'value'=>@$static_public468, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeStaticPublic468','static_468','pub');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='static_468_pub' style='width:198px;'><?php for($banner=1;$banner<=@$static_public468;$banner++){ echo $banner.') <span id=\'static_468_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Rotating Banners (468X60) - Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('rotating_member468', array('type'=>'text', 'value'=>@$rotating_member468, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeRotatingMember468','rotating_468','mbr');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='rotating_468_mbr' style='width:198px;'><?php for($banner=1;$banner<=@$rotating_member468;$banner++){ echo $banner.') <span id=\'rotating_468_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Static Banners (468X60) - Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('static_member468', array('type'=>'text', 'value'=>@$static_member468, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeStaticMember468','static_468','mbr');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='static_468_mbr' style='width:198px;'><?php for($banner=1;$banner<=@$static_member468;$banner++){ echo $banner.') <span id=\'static_468_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Rotating Banners (728X90) - Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('rotating_public728', array('type'=>'text', 'value'=>@$rotating_public728, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeRotatingPublic728','rotating_728','pub');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='rotating_728_pub' style='width:198px;'><?php for($banner=1;$banner<=@$rotating_public728;$banner++){ echo $banner.') <span id=\'rotating_728_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Static Banners (728X90) - Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('static_public728', array('type'=>'text', 'value'=>@$static_public728, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeStaticPublic728','static_728','pub');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='static_728_pub' style='width:198px;'><?php for($banner=1;$banner<=@$static_public728;$banner++){ echo $banner.') <span id=\'static_728_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Rotating Banners (728X90) - Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('rotating_member728', array('type'=>'text', 'value'=>@$rotating_member728, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeRotatingMember728','rotating_728','mbr');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='rotating_728_mbr' style='width:198px;'><?php for($banner=1;$banner<=@$rotating_member728;$banner++){ echo $banner.') <span id=\'rotating_728_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Static Banners (728X90) - Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('static_member728', array('type'=>'text', 'value'=>@$static_member728, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeStaticMember728','static_728','mbr');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='static_728_mbr' style='width:198px;'><?php for($banner=1;$banner<=@$static_member728;$banner++){ echo $banner.') <span id=\'static_728_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="formbutton">
			<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_advertisementaction',$SubadminAccessArray)){ ?>
			<?php echo $this->Js->submit('Update', array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'class'=>'btnorange',
			  'div'=>false,
			  'controller'=>'design',
			  'action'=>'banneradplanaction',
			  'url'   => array('controller' => 'design', 'action' => 'banneradplanaction')
			));?>
			<?php } ?>
		</div>
		
	</div>
	<?php echo $this->Form->end();?>
	
	  
<div class="subbar">Plan Text Ads Display Settings</div>
<div class="height10"></div>
<?php echo $this->Form->create('Theme',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'textadplanaction')));?>
<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$id, 'label' => false)); ?>

	<div class="frommain">
		
		<div class="fromnewtext">Rotating Text Ads - Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('rotating_public', array('type'=>'text', 'value'=>@$rotating_public, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeRotatingPublic','rotating_textad','pub');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='rotating_textad_pub' style='width:198px;'><?php for($banner=1;$banner<=@$rotating_public;$banner++){ echo $banner.') <span id=\'rotating_textad_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Static Text Ads - Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('static_public', array('type'=>'text', 'value'=>@$static_public, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeStaticPublic','static_textad','pub');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='static_textad_pub' style='width:198px;'><?php for($banner=1;$banner<=@$static_public;$banner++){ echo $banner.') <span id=\'static_textad_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Rotating Text Ads - Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('rotating_member', array('type'=>'text', 'value'=>@$rotating_member, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeRotatingMember','rotating_textad','mbr');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='rotating_textad_mbr' style='width:198px;'><?php for($banner=1;$banner<=@$rotating_member;$banner++){ echo $banner.') <span id=\'rotating_textad_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Static Text Ads - Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('static_member', array('type'=>'text', 'value'=>@$static_member, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeStaticMember','static_textad','mbr');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='static_textad_mbr' style='width:198px;'><?php for($banner=1;$banner<=@$static_member;$banner++){ echo $banner.') <span id=\'static_textad_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="formbutton">
			<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_advertisementaction', $SubadminAccessArray)){ ?>
			<?php echo $this->Js->submit('Update', array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'class'=>'btnorange',
			  'div'=>false,
			  'controller'=>'design',
			  'action'=>'textadplanaction',
			  'url'   => array('controller' => 'design', 'action' => 'textadplanaction')
			));?>
			<?php } ?>
		</div>
	
	</div>
<?php echo $this->Form->end();?>
	
	  
<div class="subbar">Plan PPC Display Settings</div>
<div class="height10"></div>
<?php echo $this->Form->create('Theme',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'ppcplanaction')));?>
<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$id, 'label' => false)); ?>

	<div class="frommain">
		
		<div class="fromnewtext">Banners (125X125) - Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('bp125', array('type'=>'text', 'value'=>@$bp125, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeBp125','ppc125','pub');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='ppc125_pub' style='width:198px;'><?php for($banner=1;$banner<=@$bp125;$banner++){ echo $banner.') <span id=\'ppc125_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Banners (125X125) - Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('bm125', array('type'=>'text', 'value'=>@$bm125, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeBm125','ppc125','mbr');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='ppc125_mbr' style='width:198px;'><?php for($banner=1;$banner<=@$bm125;$banner++){ echo $banner.') <span id=\'ppc125_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Banners (468X60) - Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('bp468', array('type'=>'text', 'value'=>@$bp468, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeBp468','ppc468','pub');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='ppc468_pub' style='width:198px;'><?php for($banner=1;$banner<=@$bp468;$banner++){ echo $banner.') <span id=\'ppc468_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Banners (468X60) - Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('bm468', array('type'=>'text', 'value'=>@$bm468, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeBm468','ppc468','mbr');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='ppc468_mbr' style='width:198px;'><?php for($banner=1;$banner<=@$bm468;$banner++){ echo $banner.') <span id=\'ppc468_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Banners (728X90) - Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('bp728', array('type'=>'text', 'value'=>@$bp728, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeBp728','ppc728','pub');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='ppc728_pub' style='width:198px;'><?php for($banner=1;$banner<=@$bp728;$banner++){ echo $banner.') <span id=\'ppc728_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Banners (728X90) - Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('bm728', array('type'=>'text', 'value'=>@$bm728, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemeBm728','ppc728','mbr');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='ppc728_mbr' style='width:198px;'><?php for($banner=1;$banner<=@$bm728;$banner++){ echo $banner.') <span id=\'ppc728_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="formbutton">
			<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_advertisementaction', $SubadminAccessArray)){ ?>
			<?php echo $this->Js->submit('Update', array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'class'=>'btnorange',
			  'div'=>false,
			  'controller'=>'design',
			  'action'=>'ppcplanaction',
			  'url'   => array('controller' => 'design', 'action' => 'ppcplanaction')
			));?>
			<?php } ?>
		</div>
	
	</div>
<?php echo $this->Form->end();?>

<div class="subbar">PPC Text Ads Display Settings</div>
<div class="height10"></div>
<?php echo $this->Form->create('Theme',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'ppctextadplanaction')));?>
<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$id, 'label' => false)); ?>

	<div class="frommain">
		
		<div class="fromnewtext">Text Ads - Public Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('ppc_public', array('type'=>'text', 'value'=>@$ppc_public, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemePpcPublic','ppc_textad','pub');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='ppc_textad_pub' style='width:198px;'><?php for($banner=1;$banner<=@$ppc_public;$banner++){ echo $banner.') <span id=\'ppc_textad_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Text Ads - Member Area :<span class="red-color">*</span> </div>
		<div class="fromborder">
			<?php echo $this->Form->input('ppc_member', array('type'=>'text', 'value'=>@$ppc_member, 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'onchange'=>"show_ad_code('ThemePpcMember','ppc_textad','mbr');"));?>
		</div>
		<span class="tooltipmain glyphicon-question-sign" data-original-title="Copy the Code From Below And Put it in The File Specified Above.<textarea name='textarea' readonly='readonly' rows='5' class='sta-from-box' id='ppc_textad_mbr' style='width:198px;'><?php for($banner=1;$banner<=@$ppc_member;$banner++){ echo $banner.') <span id=\'ppc_textad_'.$banner.'\'></span>&#13;&#10;'; } ?></textarea>" data-toggle="tooltip"> </span>
		
		
		<div class="formbutton">
			<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_advertisementaction', $SubadminAccessArray)){ ?>
			<?php echo $this->Js->submit('Update', array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'class'=>'btnorange',
			  'div'=>false,
			  'controller'=>'design',
			  'action'=>'ppctextadplanaction',
			  'url'   => array('controller' => 'design', 'action' => 'ppctextadplanaction')
			));?>
			<?php } ?>
		</div>
	
	</div>
<?php echo $this->Form->end();?>

<?php }else{ ?><div class="accessdenied"><?php echo "Access Denied";?></div><?php }?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>