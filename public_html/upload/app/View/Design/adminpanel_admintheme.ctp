<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Member Themes</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Themes", array('controller'=>'design', "action"=>"index"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Admin Theme", array('controller'=>'design', "action"=>"admintheme"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="designpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Themes#Admin_Theme" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<?php
$this->Paginator->options(array(
	'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
	'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
	'update' => '#designpage',
	'evalScripts' => true,
	'url'=> array('controller'=>'design', 'action'=>'index')
));
$currentpagenumber=$this->params['paging']['Admintheme']['page'];
?>

<div id="gride-bg" class="marginnone">
	
	<div class="actmain">
		<div class="act_them_box">
			<img src="<?php echo Router::url('/', true)."theme/".$activetheme["Admintheme"]["theme_name"]."/img/".$activetheme["Admintheme"]["thumb"];?>" alt='' align="absmiddle" style="max-width:100%;" />
		</div>
		<div class="activtext">Active Theme Name : <?php echo $activetheme["Admintheme"]["theme_name"];?></div>
	</div>
	
	<div class="theam_right">
		<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
		<div class="addnew-button">
			<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_adminthemeadd',$SubadminAccessArray)){ ?>
				<?php echo $this->Js->link("+ Add New", array('controller'=>'design', "action"=>"adminthemeadd"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btnorange'
				));?>
			<?php } ?>
		
		</div>
		<div class="clearboth"></div>
			<?php
				$i = 0;
				foreach ($themes as $theme):
				?>
					
					<div class="themboxmain">
						<div class="themtitle"><?php echo $theme['Admintheme']['theme_name']; ?></div>
						<div class="themtitle-right">
							<div class="btn-group">
								<button data-toggle="dropdown" type="button" class="btn btn-default btn-xs dropdown-toggle">
									Action <span class="caret"></span>
								</button>
								<ul role="menu" class="dropdown-menu">
									<li>
										<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_adminthemeadd/$',$SubadminAccessArray)){ ?>
											<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Theme')).' Edit Theme', array('controller'=>'design', "action"=>"adminthemeadd/".$theme['Admintheme']['id']), array(
												'update'=>'#designpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'vtip',
												'title'=>'Edit Theme'
											));?>
										<?php }else{ 
											echo '&nbsp;';
										} ?>
									</li>
									<li>
										<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_adminthemestatus',$SubadminAccessArray)){ ?>
											<?php 
											echo $this->Js->link($this->html->image('red-icon.png', array('alt'=>'Activate Theme')).' Activate Theme', array('controller'=>'design', "action"=>"adminthemestatus/yes/".$theme['Admintheme']['id']."/".$currentpagenumber), array(
												'update'=>'#designpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'vtip',
												'title'=>'Activate Theme'
											));?>
										<?php }else{ 
											echo '&nbsp;';
										} ?>
									</li>
									<li>
										<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_adminthemeremove',$SubadminAccessArray)){ ?>
											<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Theme')).' Delete Theme', array('controller'=>'design', "action"=>"adminthemeremove/".$theme['Admintheme']['id']."/".$currentpagenumber), array(
												'update'=>'#designpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'vtip',
												'title'=>'Delete Theme',
												'confirm'=>"Are You Sure?"
											));?>
										<?php }else{ 
											echo '&nbsp;';
										} ?>
									</li>
								</ul>
							</div>
						</div>
						<div class="thembox">
							<img src="<?php echo $SITEURL.'theme/'.$theme["Admintheme"]["theme_name"].'/img/'.$theme["Admintheme"]["thumb"];?>" alt="" width="100%" height="100%"/>
						</div>      
					</div>
			<?php endforeach; ?>	
	</div>
	<div class="clear-both"></div>
	
	<div class="height10"></div>
</div>
<?php }else{
?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#designpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>