<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Language Edit Phase</div>
<div id="designpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Language_Edit_Phase" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>
<div class="backgroundwhite">
	
    	<?php echo $this->Form->create('Language',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'languageeditphaseaddaction')));?>
		<?php if(isset($languagedata['Language']["id"])){
			echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$languagedata['Language']["id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}?>
		<div class="frommain">
			
			<div class="fromnewtext">Display Name :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('displayname', array('type'=>'text', 'value'=>$languagedata['Language']["displayname"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			
			<div class="fromnewtext">Code :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('code', array('type'=>'text', 'value'=>$languagedata['Language']["code"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			
			
			<div class="formbutton">
				<?php echo $this->Js->submit('Submit', array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'class'=>'btnorange',
				  'div'=>false,
				  'controller'=>'design',
				  'action'=>'languageeditphaseaddaction',
				  'url'   => array('controller' => 'design', 'action' => 'languageeditphaseaddaction')
				));?>
				<?php echo $this->Js->link("Back", array('controller'=>'design', "action"=>"languageeditphase"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btngray'
				));?>
			</div>
			
		</div>
		<?php echo $this->Form->end();?>
	
</div>
<?php if(!$ajax){?>
</div><!--#designpage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>