<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Email Templates</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Site Email Templates", array('controller'=>'design', "action"=>"siteemailtemplate"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Mass Mail Templates", array('controller'=>'design', "action"=>"massemailtemplate"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Site Email Template Categories", array('controller'=>'design', "action"=>"emailcategory"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="designpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Email_Templates#Site_Email_Templates" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
    
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Add Email</div>
	<?php echo $this->Form->create('Template_email',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'siteemailtemplateaddaction')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Category :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php echo $this->Form->input('category', array(
							'type' => 'select',
							'options' => $category,
							'selected' => '',
							'class'=>'',
							'label' => false,
							'style' => ''
						));?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span></span>
			<span class="searchforfields_s"></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>Name :</span>
			<span><?php echo $this->Form->input('description', array('type'=>'text', 'value'=>'', 'label' => false, 'class'=>'searchfor'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>Tag :</span>
			<span><?php echo $this->Form->input('tag', array('type'=>'text', 'value'=>'', 'label' => false, 'class'=>'searchfor'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#UpdateMessage',
					'class'=>'searchbtn',
					'controller'=>'design',
					'div'=>false,
					'action'=>'siteemailtemplateaddaction',
					'url'   => array('controller' => 'design', 'action' => 'siteemailtemplateaddaction')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
        
<div id="gride-bg">
	<div class="Xpadding10">
		
		<div class="emailcentermain">
				<?php foreach($category as $categoryid=>$categoryname){ ?>
				<div class="emailmain">
					<div class="emailtitle"><?php echo $categoryname; ?></div>
					<div class="tablegrid">
						<div class="tablegridheader">
							<div style="width: 59px;"><?php echo 'Id';?></div>
							<div><?php echo 'Description';?></div>
							<div><?php echo 'Action';?></div>
						</div>
						<?php foreach ($template_emails as $template_email){?>
						<?php if($categoryid==$template_email['Template_email']['category'])
						{ ?>
							<div class="tablegridrow">
								<div><?php echo $template_email['Template_email']['template_id']; ?></div>
								<div><?php echo stripslashes($template_email['Template_email']['description']); ?></div>
								<div>
									<div class="actionmenu">
									  <div class="btn-group">
										<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
											Action <span class="caret"></span>
										</button>
										<ul class="dropdown-menu" role="menu">
											
											<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_siteemailtemplateadd/$',$SubadminAccessArray)){ ?>
											<li>
											<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Email Template'))." Edit", array('controller'=>'design', "action"=>"siteemailtemplateadd/".$template_email['Template_email']['template_id']), array(
												'update'=>'#designpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>''
											));?>
											</li>
											<?php }?>
											
											<?php 
											if($template_email['Template_email']['status']==0){
												$statusaction='1';
												$statusicon='red-icon.png';
												$statustext='Activate';
											}else{
												$statusaction='0';
												$statusicon='blue-icon.png';
												$statustext='Inactivate';}
											
											if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_siteemailtemplatestatus',$SubadminAccessArray)){?>
											<li>
											<?php
												echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'design', "action"=>"siteemailtemplatestatus/".$statusaction."/".$template_email['Template_email']['template_id']), array(
													'update'=>'#designpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>''
												));?>
											</li>
											<?php }?>
											
											<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_siteemailtemplateremove',$SubadminAccessArray)){ ?>
											<li>
											<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Email Template'))." Delete", array('controller'=>'design', "action"=>"siteemailtemplateremove/".$template_email['Template_email']['template_id']), array(
												'update'=>'#designpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'confirm'=>"Are You Sure?"
											));?>
											</li>
											<?php }?>
										</ul>
									  </div>
									</div>
								</div>
							</div>
						<?php } }?>
					</div>
				</div>
				<?php }?>
		</div>
		<div class="clear-both"></div>
		<div class="height10"></div>	
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#designpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>