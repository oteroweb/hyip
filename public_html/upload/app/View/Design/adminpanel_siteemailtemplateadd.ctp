<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php 
if($textarea=='advanced')
{
	echo $this->Javascript->link('tiny_mce/tiny_mce.js');
	echo $this->Javascript->link('tinymce');
	$textareaclass='advancededitor';
	$submitbuttonclass='tinyMCEtriggerSavetk';
}
elseif($textarea=='simple')
{
	$textareaclass='';
	$submitbuttonclass='';
}
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Email Templates</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Site Email Templates", array('controller'=>'design', "action"=>"siteemailtemplate"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Mass Mail Templates", array('controller'=>'design', "action"=>"massemailtemplate"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Site Email Template Categories", array('controller'=>'design', "action"=>"emailcategory"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="designpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Email_Templates#Site_Email_Templates" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<div class="backgroundwhite">
				
    	<?php echo $this->Form->create('Template_email',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'siteemailtemplateaddaction')));?>
		
		<?php if(isset($template_emaildata['Template_email']["template_id"])){
			echo $this->Form->input('template_id', array('type'=>'hidden', 'value'=>$template_emaildata['Template_email']["template_id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}?>
		<div class="frommain">
			
			<div class="fromnewtext">Category :</div>
			<div class="fromborderdropedown3">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('category', array(
						  'type' => 'select',
						  'options' => $category,
						  'selected' => $template_emaildata['Template_email']["category"],
						  'class'=>'',
						  'div' => false,
						  'label' => false,
						  'style' => ''
						));
						?>
					</label>
			  </div>  
			</div>
			
			<div class="fromnewtext">Status :</div>
			<div class="fromborderdropedown3">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('status', array(
							'type' => 'select',
							'options' => array('1'=>'Active', '0'=>'Inactive'),
							'selected' => $template_emaildata['Template_email']["status"],
							'class'=>'',
							'div' => false,
							'label' => false,
							'style' => ''
						));
						?>
					</label>
			  </div>  
			</div>
			<?php if(!isset($template_emaildata['Template_email']['template_id'])){ ?>
			<div class="fromnewtext">Unique Name :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('description', array('type'=>'text', 'value'=>stripslashes($template_emaildata['Template_email']["description"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<?php } ?>
			<div class="fromnewtext" style="display: none;">Tags :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3" style="display: none;">
				<?php echo $this->Form->input('tag', array('type'=>'text', 'value'=>$template_emaildata['Template_email']['tag'], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			
			<div class="fromnewtext">Subject :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('subject', array('type'=>'text', 'value'=>stripslashes($template_emaildata['Template_email']["subject"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			
			<div class="formbutton">
			<?php 
				if($textarea=='simple'){$showtextarea='advanced';}
				if($textarea=='advanced'){$showtextarea='simple';}
				$link='siteemailtemplateadd/'.$template_emaildata['Template_email']["template_id"].'/'.$showtextarea;
				echo $this->Js->link("Switch To ".ucfirst($showtextarea)." Mode", array('controller'=>'design', "action"=>$link), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btngray'
				));
			?>
			</div>
			<div class="fromnewtext">Message :<span class="red-color">*</span></div>
			<div class="frombordermain">
				<?php echo $this->Form->input('message', array('type'=>'textarea', 'value'=>stripslashes($template_emaildata['Template_email']["message"]),'label' => false, 'div' => false, 'style'=>'height:150px;', 'class'=>'from-textarea '.$textareaclass));?>
			</div>
			
			<div class="fromnewtext">Mail Template Fields : </div>
			<div class="fromborderdropedown3">
				<?php foreach($tagdata as $tag){?>
					<div class="linktotext"><a onClick="AddTags(document.getElementById('Template_emailMessage'),'[<?php echo $tag['Template_tag']['tag_name'];?>]');" title="click to paste" href="javascript:void(0);"><?php echo '['.$tag['Template_tag']['tag_name'].']';?></a><?php echo ' - '.$tag['Template_tag']['tag_desc'];?></div>
				<?php }?>
			</div>
			
			<div class="formbutton">
				<?php echo $this->Js->submit('Update', array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'class'=>'btnorange '.$submitbuttonclass,
				  'controller'=>'design',
				  'div'=>false,
				  'action'=>'siteemailtemplateaddaction',
				  'url'   => array('controller' => 'design', 'action' => 'siteemailtemplateaddaction')
				));?>
				<?php echo $this->Js->link("Back", array('controller'=>'design', "action"=>"siteemailtemplate"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btngray'
				));?>
			</div>
			
		</div>
		<?php echo $this->Form->end();?>

	
</div>
<?php if(!$ajax){?>
</div><!--#designpage over-->
</div>
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>