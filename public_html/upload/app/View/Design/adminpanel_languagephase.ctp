<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>	
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Language Edit Phase</div>
<div id="designpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Language_Edit_Phase" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Languagephase',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'languagephase/'.$phasetype)));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('searchby', array(
							  'type' => 'select',
							  'options' => array('all'=>'Select Parameter', 'phase'=>'Phase', 'value'=>'Phase Value'),
							  'selected' => $searchby,
							  'class'=>'',
							  'label' => false,
							  'style' => ''
						));
						?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#designpage',
					'class'=>'searchbtn',
					'controller'=>'design',
					'action'=>'languagephase/'.$phasetype,
					'url'=> array('controller' => 'design', 'action' => 'languagephase/'.$phasetype)
				));?>
			</span>
		</div>
	 </div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#designpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'design', 'action'=>'languagephase/'.$phasetype)
	));
	$currentpagenumber=$this->params['paging']['Languagephase']['page'];
	?>
<div id="gride-bg" class="noborder">
    <div class="padding10">
		
	<div class="greenbottomborder">
	<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	 	<?php if($languagephases[0]['Languagephase']['language_id']=='eng' && (!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_languagephaseadd',$SubadminAccessArray))){ ?>
			<?php echo $this->Js->link("+ Add New", array('controller'=>'design', "action"=>"languagephaseadd/".$phasetype), array(
                'update'=>'#designpage',
                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                'escape'=>false,
                'class'=>'btnorange'
            ));?>
        <?php } ?>
		<?php echo $this->Js->link("Apply Phase Update to Language File", array('controller'=>'design', "action"=>"languagephaseapply/".$phasetype), array(
			'update'=>'#designpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btngray'
		));?>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Languagephase',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'languagephaseupdate')));?>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>Default Phase (Find)</div>
				<div>Customized Phase (Replace)</div>
				<div>Action</div>
		  </div>
			<?php foreach ($languagephases as $languagephase): ?>
				<div class="tablegridrow">
					<div><?php echo $languagephase['Languagephase']['phase'];?></div>
					<div>
						<textarea  class="from-textarea" placeholder="Enter Phase" name="txt_<?php echo $languagephase['Languagephase']['id'];?>" style="width:100%;height:25px"><?php echo stripslashes($languagephase['Languagephase']['value']);?></textarea>
					</div>
				  	<div>
								<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_languagephaseadd/$',$SubadminAccessArray)){ ?>
								<?php echo $this->Js->submit('Update', array(
								  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								  'update'=>'#designpage',
								  'class'=>'btngray vtip',
								  'controller'=>'design',
								  'div'=>false,
								  'title'=>'Update Phase of Language Phase Id : '.$languagephase['Languagephase']['id'],
								  'action'=>'languagephaseupdate/'.$languagephase['Languagephase']['id']."/".$phasetype."/".$currentpagenumber,
								  'url'=> array('controller' => 'design', 'action' => 'languagephaseupdate/'.$languagephase['Languagephase']['id']."/".$phasetype."/".$currentpagenumber)
									));?>
									<?php }else{
										echo '&nbsp;';
									} ?>
									
									<?php
									if($languagephase['Languagephase']['language_id']=='eng' && $languagephase['Languagephase']['defaultphase']==0 && (!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_languagephaseremove',$SubadminAccessArray)))
									{	
										echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Language Phase','align'=>'absmiddle')), array('controller'=>'design', "action"=>"languagephaseremove/".$phasetype."/".$languagephase['Languagephase']['id']."/".$currentpagenumber), array(
											'update'=>'#designpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'class'=>'vtip',
											'confirm'=>'Are You Sure?',
											'title'=>'Delete Language Phase'
										));
									}
									?>
					</div>
				</div>
				
			<?php endforeach; ?>
			<?php if(count($languagephases)==0){ echo '<tr><td colspan="11" class="padding-tabal"></td></tr><tr class="blue-color"><td class=" new-border-left"></td><td  align="center" valign="middle" colspan="9">No records available</td><td class=" new-border-right"></td></tr>';} ?>
	</div>
    <?php echo $this->Form->end();
	if($this->params['paging']['Languagephase']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Languagephase',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'design', 'action'=>'languagephase/'.$phasetype.'/rpp')));?>
		<div class="combobox1">
		<?php 
		echo $this->Form->input('resultperpage', array(
		  'type' => 'select',
		  'options' => $resultperpage,
		  'selected' => $this->Session->read('pagerecord'),
		  'class'=>'',
		  'label' => false,
		  'div'=>false,
		  'style' => '',
		  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
		));
		?>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#designpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'design',
			  'action'=>'languagephase/'.$phasetype.'/rpp',
			  'url'   => array('controller' => 'design', 'action' => 'languagephase/'.$phasetype.'/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
	<?php if(!$ajax){?>
</div><!--#designpage over-->

					
				
				<div class="clear-both"></div>
			</div>
			<div class="clear-both"></div>
			
			</div>
	   </div>
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>