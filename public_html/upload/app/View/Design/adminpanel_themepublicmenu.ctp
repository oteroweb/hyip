<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 15-12-2014
  * Last Modified: 15-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Member Themes</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Themes", array('controller'=>'design', "action"=>"index"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Admin Theme", array('controller'=>'design', "action"=>"admintheme"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="designpage">
<?php }?>
<div class="tab-innar">
	<ul>
		<li>
			<?php echo $this->Js->link("Themes", array('controller'=>'design', "action"=>"index"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Member Side", array('controller'=>'design', "action"=>"thememembermenu"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Public Side", array('controller'=>'design', "action"=>"themepublicmenu"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>'active'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Advertisement Settings", array('controller'=>'design', "action"=>"advertisement"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Settings", array('controller'=>'design', "action"=>"themesetting"), array(
				'update'=>'#designpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
	</ul>
</div>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Themes#Public_Side" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div id="Xgride-bg">
    <div class="Xpadding10">
	<div class="greenbottomborder">
		
	<div class="height10"></div>			
	<div class="records records-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="clear-both"></div>
	
	<div class="tablegrid nomobilecss">
			<div class="tablegridheader">
				<div><?php echo 'Id';?></div>
				<div><?php echo 'Element Name';?></div>
				<div><?php echo 'Status';?></div>
			</div>
			<?php $i = 0; foreach ($menus as $menu): $i++
				?>
				<div class="tablegridrow">
				    <div><?php echo $menu['Menu']['id']; ?></div>
					<div><?php echo $menu['Menu']['menu_name']; ?></div>
					<?php /*<td  align="center" valign="middle"><?php echo $menu['Menu']['menu_order']; ?></td>
					<td  align="center" valign="middle">
						<?php 
						if($menu['Menu']['menu_order']==1 && $maxo>1)
						{
							echo $this->Js->link($this->html->image('down.png', array('alt'=>'')), array('controller'=>'design', "action"=>"menuformaction/down/public/".$menu['Menu']['menu_order']."/".$menu['Menu']['id']), array(
								'update'=>'#designpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'Move Down'
							));
						}
						elseif($menu['Menu']['menu_order']==$maxo && $maxo>1)
						{
							echo $this->Js->link($this->html->image('up.png', array('alt'=>'')), array('controller'=>'design', "action"=>"menuformaction/up/public/".$menu['Menu']['menu_order']."/".$menu['Menu']['id']), array(
								'update'=>'#designpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'Move Up'
							));
						}
						elseif($menu['Menu']['menu_order']<$maxo && $menu['Menu']['menu_order']>$mino)
						{
							echo $this->Js->link($this->html->image('down.png', array('alt'=>'')), array('controller'=>'design', "action"=>"menuformaction/down/public/".$menu['Menu']['menu_order']."/".$menu['Menu']['id']), array(
								'update'=>'#designpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'Move Down'
							));
							echo '&nbsp;';
							echo $this->Js->link($this->html->image('up.png', array('alt'=>'')), array('controller'=>'design', "action"=>"menuformaction/up/public/".$menu['Menu']['menu_order']."/".$menu['Menu']['id']), array(
								'update'=>'#designpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'Move Up'
							));
						}
						?>
					</td>*/?>
					<div>
						<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_menuformaction', $SubadminAccessArray)){ ?>
								<?php 
								if($menu['Menu']['status']==0){
									$statusaction='1';
									$statusicon='red-icon.png';
									$statustext='Activate Menu';
								}else{
									$statusaction='0';
									$statusicon='blue-icon.png';
									$statustext='Inactivate Menu';}
								
								echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)), array('controller'=>'design', "action"=>"menuformaction/status/public/".$statusaction."/".$menu['Menu']['id']), array(
									'update'=>'#designpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>$statustext
								));
								?>
						<?php } ?>
					</div>
					
				</div>
			<?php endforeach; ?>
	</div>
	<div class="height10"></div>
	</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#designpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>