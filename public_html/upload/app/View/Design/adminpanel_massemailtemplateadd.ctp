<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Email Templates</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Site Email Templates", array('controller'=>'design', "action"=>"siteemailtemplate"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Mass Mail Templates", array('controller'=>'design', "action"=>"massemailtemplate"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Site Email Template Categories", array('controller'=>'design', "action"=>"emailcategory"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="designpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Email_Templates#Mass_Mail_Templates" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
	
<div class="backgroundwhite">
				
		<?php echo $this->Form->create('Admin_template_email',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'massemailtemplateaddaction')));?>
		
		<?php if(isset($admin_template_emaildata['Admin_template_email']["id"])){
			echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$admin_template_emaildata['Admin_template_email']["id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}?>
		<div class="frommain">
			
			<div class="fromnewtext">Status :</div>
			<div class="fromborderdropedown3">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('status', array(
							'type' => 'select',
							'options' => array('1'=>'Active', '0'=>'Inactive'),
							'selected' => $admin_template_emaildata['Admin_template_email']["status"],
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));
						?>
					</label>
			  </div>  
			</div>
			
			<div class="fromnewtext">Description :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('description', array('type'=>'text', 'value'=>stripslashes($admin_template_emaildata['Admin_template_email']["description"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			
			<div class="fromnewtext">Subject :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('subject', array('type'=>'text', 'value'=>stripslashes($admin_template_emaildata['Admin_template_email']["subject"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			
			<div class="fromnewtext">Message :<span class="red-color">*</span></div>
			<div class="frombordermain">
				<?php echo $this->Form->input('message', array('type'=>'textarea', 'value'=>stripslashes($admin_template_emaildata['Admin_template_email']["message"]),'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
			</div>
			
			<div class="fromnewtext">Mail Template Fields : </div>
			<div class="fromborderdropedown3">
				<?php foreach($tagdata as $tag){?>
					<div class="linktotext"><a onClick="AddTags(document.getElementById('Admin_template_emailMessage'),'[<?php echo $tag['Template_tag']['tag_name'];?>]');" title="click to paste" href="javascript:void(0);"><?php echo '['.$tag['Template_tag']['tag_name'].']';?></a><?php echo ' - '.$tag['Template_tag']['tag_desc'];?></div>
				<?php }?>
			</div>
			
			<div class="formbutton">
				<?php echo $this->Js->submit('Update', array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'class'=>'btnorange',
				  'controller'=>'design',
				  'div'=>false,
				  'action'=>'massemailtemplateaddaction',
				  'url'   => array('controller' => 'design', 'action' => 'massemailtemplateaddaction')
				));?>
				<?php echo $this->Js->link("Back", array('controller'=>'design', "action"=>"massemailtemplate"), array(
					'update'=>'#designpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btngray'
				));?>
			</div>
			
		</div>  
		<?php echo $this->Form->end();?>
	
	
</div>
<?php if(!$ajax){?>
</div><!--#designpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>