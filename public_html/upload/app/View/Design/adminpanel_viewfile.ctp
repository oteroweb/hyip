<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Template Edit</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Template Directory", array('controller'=>'design', "action"=>"template"), array(
					'update'=>'#success',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Layout Directory", array('controller'=>'design', "action"=>"layout"), array(
					'update'=>'#success',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="success">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Template_Edit#Template_Directory" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
	
<div class="whitenotebox marginnone"><?php echo $this->html->image("folder.gif",  array("alt"=>"Directory", "align"=>"absmiddle", 'title'=>'View Directory'));?>&nbsp;Template Directory/<?php echo $foldername;?></div>

	
		<div class="tablegrid">
			<div class="tablegridheader">
				<div class="textleft">&nbsp;&nbsp;&nbsp;<?php echo $foldername;?> Directory</div>
				<div>&nbsp;</div>
				<div>&nbsp;</div>
				<div>&nbsp;</div>
			</div>
			<?php
			$pag=APP."View".DS.$foldername;
			$dir = new Folder($pag);
			$viewfile= $dir->read();
			$i = 0;
			$counter=0;
			foreach ($viewfile[1] as $filename) 
			{
				$class = 'class="blue-color"';if ($i++ % 2 == 0){$class = 'class="white-color"';}
				$pos = strpos($filename, 'adminpanel_');
				if ($pos === false)
				{
					if($counter%4==0) echo '<div class="tablegridrow">';	
					?>
						<div class="textleft dirpagelink">
							<?php echo $this->Js->link($filename, array('controller'=>'design', "action"=>"editfile/".$foldername."_|_".str_replace(".ctp","",$filename)), array(
								'update'=>'#success',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
							));?>
						</div>
					<?php
					$counter++;
					if($counter%4==0) echo '</div>';
				}
			} ?>
		</div>
<div align="left">
<div class="height10"></div>	
<?php echo $this->Js->link("Back", array('controller'=>'design', "action"=>"template"), array(
	'update'=>'#success',
	'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
	'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
	'escape'=>false,
	'class'=>'btngray'
));?></div>
<?php if(!$ajax){?>
</div>
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>