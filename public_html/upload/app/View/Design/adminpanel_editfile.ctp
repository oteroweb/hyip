<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php $directoryfilename=explode('_|_',$foldername);?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Template Edit</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Template Directory", array('controller'=>'design', "action"=>"template"), array(
					'update'=>'#success',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Layout Directory", array('controller'=>'design', "action"=>"layout"), array(
					'update'=>'#success',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="success">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Template_Edit#Template_Directory" target="_blank">Help</a></div>
<div class="whitenotebox marginnone"><?php echo $this->html->image("ctp.gif",  array("alt"=>"Directory", "align"=>"absmiddle", 'title'=>'View Directory'));?>&nbsp;Template Directory/<?php echo $directoryfilename[0]."/".$directoryfilename[1].".ctp";?></div>


	<div class="tablegrid">
		<div class="tablegridheader">
			<div class="textleft">&nbsp;&nbsp;&nbsp;<?php echo $directoryfilename[0]."/".$directoryfilename[1].".ctp";?> File</div>
		</div>
	</div>
	<div class="height10"></div>
	<?php echo $this->Form->create('TemplateEdit',array('type' => 'post'/*, 'id'=>'TestfilesForm'*/, 'onsubmit' => 'return false;','url'=>array('controller'=>'design','action'=>'updatefile/'.$foldername)));?>
	<div class="nameandbox">
		<div class="fromboxhelpmain" style='width:99%;'>
			<?php echo $this->Form->input('filecontent', array('type'=>'textarea', 'id'=>'filecontent','value'=>$filecontent["content"], 'label' => false,  'div' => false, 'class'=>'from-textarea', 'style'=>'width:100%;height:500px;margin-left:0px;padding:5px 0 0 5px;'));?>
		</div>
	</div>
	<div class="height10"></div>
	<div class="nameandbox">
		<div class="from-text"></div>
		<div class="fromboxhelpmain">
			<?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_templateupdate', $SubadminAccessArray)){ ?>
			<?php echo $this->Js->submit('Update', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#success',
				'class'=>'btnorange',
				'controller'=>'design',
				'action'=>'updatefile/'.$foldername,
				'url'=> array('controller' => 'design', 'action' => 'updatefile/'.$foldername),
				'div'=>false
			  ));?>
			<?php } ?>
		  <?php echo $this->Js->link("Back", array('controller'=>'design', "action"=>"viewfile/".$directoryfilename[0]), array(
			  'update'=>'#success',
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'escape'=>false,
			  'class'=>'btngray'
		  ));?>
		</div>
	</div>
  <?php echo $this->Form->end();?>

<?php if(!$ajax){?>
</div>
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>