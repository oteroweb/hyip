<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Template Edit</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Template Directory", array('controller'=>'design', "action"=>"template"), array(
					'update'=>'#success',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Layout Directory", array('controller'=>'design', "action"=>"layout"), array(
					'update'=>'#success',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="success">								
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Template_Edit#Template_Directory" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
	<div class="greenbottomborder">
	<div class="tablegrid marginnone">
		<div class="tablegridheader">
			<div class="textleft">&nbsp;&nbsp;&nbsp;<?php echo 'Template Directory';?></div>
			<div>&nbsp;</div>
			<div>&nbsp;</div>
			<div>&nbsp;</div>
		</div>
		<?php
		$pag=APP."View";//dirname(dirname(__FILE__));
		$dir = new Folder($pag);
		$viewfolder= $dir->read();
		$counter=0;
		foreach ($viewfolder[0] as $folder) 
		{
			if(!in_array($folder,$dontviewfolder))
			{
				if($counter%4==0) echo '<div class="tablegridrow">';
				?>
					<div class="textleft dirlink">
						<?php echo $this->Js->link($folder, array('controller'=>'design', "action"=>"viewfile/".$folder), array(
							'update'=>'#success',
							'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
						));?>
					</div>
				<?php
				$counter++;
				if($counter%4==0) echo '</div>';
			}
		} ?>
		<?php }else{ ?><div class="tablegridrow"><div class="accessdenied"><?php echo "Access Denied";?></div></div><?php }?>
    </div>
	<div class="height10"></div>
	</div>
<?php if(!$ajax){?>		
</div>
</div> 
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>