<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 20-11-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($EnableBalancetransfer==1) { ?>
<div id="Balancetransferpanding">

<?php // Table titles start here ?>
<div class="comisson-bg mobilecss">
	<div class="commison-menu">
		<ul>
			<li>
				<?php
				if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){	
					echo $this->Js->link(__('Member Pending Requests'), array('controller'=>'balancetransfer', "action"=>"index"), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>''
					));
				}
				?>
			</li>
			<li>
				<?php
				if(strpos($SITECONFIG['balancetransfersetting'],'isenablepro@1') !== false){
					echo $this->Js->link(__('Processor Pending Requests'), array('controller'=>'balancetransfer', "action"=>"pending"), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'act'
					));
				}
				?>
			</li>
			<li>
				<?php
				if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){
					echo $this->Js->link(__('Member Transfer History'), array('controller'=>'balancetransfer', "action"=>"history"), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false
					));
				}
				?>
			</li>
			<li>
				<?php
				if(strpos($SITECONFIG['balancetransfersetting'],'isenablepro@1') !== false){
					echo $this->Js->link(__('Processor Transfer History'), array('controller'=>'balancetransfer', "action"=>"history2"), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false
					));
				}
				?>
			</li>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Table titles end here ?>

	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#Balancetransferpanding',
		'evalScripts' => true,
		'url'=> array('controller'=>'balancetransfer', 'action'=>'pending')
	));
	$currentpagenumber=$this->params['paging']['Bal_pending_transfer']['page'];
	?>
<div class="main-box-eran">
	<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
	<div class="clear-both"></div>
	
	<?php // Pending Requests table starts here ?>
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam">
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link(__('Date'), array('controller'=>'balancetransfer', "action"=>"pending/0/req_dt/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Date')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('Amount'), array('controller'=>'balancetransfer', "action"=>"pending/0/amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Amount')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('Transfer Amount'), array('controller'=>'balancetransfer', "action"=>"pending/0/tran_amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Transfer Amount')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('Fees'), array('controller'=>'balancetransfer', "action"=>"pending/0/fees/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Fees')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('From'), array('controller'=>'balancetransfer', "action"=>"pending/0/processor_id_from/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('From')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('To'), array('controller'=>'balancetransfer', "action"=>"pending/0/processor_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('To')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('From Balance'), array('controller'=>'balancetransfer', "action"=>"pending/0/frompaymentmethod/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('From Balance')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('To Balance'), array('controller'=>'balancetransfer', "action"=>"pending/0/topaymentmethod/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('To Balance')
					));?>
				</div>
				<div class="divth textcenter vam"><?php echo __('Status');?></div>
			</div>
		</div>
		<div class="divtbody">
			<?php $i=1;
			foreach ($bal_transferdata as $bal_transfer):
			if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $bal_transfer['Bal_pending_transfer']['req_dt']); ?></div>
					<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($bal_transfer['Bal_pending_transfer']['amount']*$Currency['rate'],4)." ".$Currency['suffix']; ?></div>
					<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($bal_transfer['Bal_pending_transfer']['tran_amount']*$Currency['rate'],4)." ".$Currency['suffix']; ?></div>
					<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($bal_transfer['Bal_pending_transfer']['fees']*$Currency['rate'],4)." ".$Currency['suffix']; ?></div>
					<div class="divtd textcenter vam"><?php echo $bal_transfer['Bal_pending_transfer']['processor_from'];?></div>
					<div class="divtd textcenter vam"><?php echo $bal_transfer['Bal_pending_transfer']['processor'];?></div>
					<div class="divtd textcenter vam">
						<?php
							if($bal_transfer['Bal_pending_transfer']['frompaymentmethod']=='cash')
								echo __('Cash Balance');
							elseif($bal_transfer['Bal_pending_transfer']['frompaymentmethod']=='repurchase')
								echo __('Re-purchase Balance');
							elseif($bal_transfer['Bal_pending_transfer']['frompaymentmethod']=='earning')
								echo __('Earning Balance');
							elseif($bal_transfer['Bal_pending_transfer']['frompaymentmethod']=='commission')
								echo __('Commission Balance');
							else
								echo '-';
						?>
					</div>
					<div class="divtd textcenter vam">
						<?php
							if($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='cash')
								echo __('Cash Balance');
							elseif($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='repurchase')
								echo __('Re-purchase Balance');
							elseif($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='earning')
								echo __('Earning Balance');
							elseif($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='commission')
								echo __('Commission Balance');
							else
								echo '-';
						?>
					</div>
					<div class="divtd textcenter vam"><?php echo __($bal_transfer['Bal_pending_transfer']['status']); ?></div>
				</div>
			<?php $i++;endforeach; ?>
		</div>
	</div>
	<?php if(count($bal_transferdata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
	<?php // Pending Requests table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Bal_pending_transfer']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Bal_pending_transfer',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'balancetransfer','action'=>'pending/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#Balancetransferpanding',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'balancetransfer',
						  'action'=>'pending/rpp',
						  'url'   => array('controller' => 'balancetransfer', 'action' => 'pending/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
			</div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		
</div>
</div>
</div>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>