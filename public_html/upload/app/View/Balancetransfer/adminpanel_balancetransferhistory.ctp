<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 20-11-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>		
<?php if(!$ajax){?>
<div class="whitetitlebox">Balance Transfer</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Member to Member Pending Requests", array('controller'=>'balancetransfer', "action"=>"balancetransferpanding"), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Internal Transfer Pending Requests", array('controller'=>'balancetransfer', "action"=>"processorpanding"), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Member to Member Transfer History", array('controller'=>'balancetransfer', "action"=>"balancetransferhistory"), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'class'=>'satting-menu-active'
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Internal Transfer History", array('controller'=>'balancetransfer', "action"=>"processorhistory"), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">
<div id="balancetransferpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Balance_Transfer#Member_to_Member_Transfer_History" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<!-- Search-box-start -->
<script type="text/javascript">
	<?php if($searchby=='processorname'){ ?>
	generatecombo("data[Balransferhistory][proc_name]","Processor","proc_name","proc_name","<?php echo $searchfor; ?>",'#searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } ?>
</script>
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Balransferhistory',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'balancetransfer','action'=>'balancetransferhistory')));?>	
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('searchby', array(
						      'type' => 'select',
						      'options' => array('all'=>'Select Parameter', 'member_id'=>'From Member', 'to_member'=>'To Member', 'amount'=>'Amount', 'tran_amount'=>'Transferred Amount', 'fees'=>'Fees', 'processorname'=>'Processor', 'ip_add'=>'IP Address'),
						      'selected' => $searchby,
						      'class'=>'',
						      'label' => false,
						      'style' => '',
									  'onchange'=>'if($(this).val()=="processorname"){generatecombo("data[Balransferhistory][proc_name]","Processor","proc_name","proc_name","'.$searchfor.'","#searchforcombo","'.$ADMINURL.'","");$("#searchforall").hide();$("#searchforcombo").show(500);}else{$("#searchforcombo").hide();$("#searchforall").show(500);}'
						));
						?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span id='searchforall' style='display: <?php if($searchby=='processorname'){ echo 'none'; } ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span id='searchforcombo' style='display: <?php if($searchby!='processorname'){ echo 'none'; } ?>'></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#balancetransferpage',
						'class'=>'searchbtn',
						'controller'=>'balancetransfer',
						'action'=>'balancetransferhistory',
						'url'=> array('controller' => 'balancetransfer', 'action' => 'balancetransferhistory')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#balancetransferpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'balancetransfer', 'action'=>'balancetransferhistory')
	));
	$currentpagenumber=$this->params['paging']['Balransferhistory']['page'];
	?>
<div id="gride-bg">
    <div class="Xpadding10">
		<div class="height10"></div>
	<div class="paginator-text paddingtop0"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button massactionbox">
		<div class="actionmenu">
			<div class="btn-group">
				<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
				  Action <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('balancetransfer/adminpanel_balancetransferhistoryremove',$SubadminAccessArray)){ ?>
				<li>
					<?php echo $this->Js->submit('Delete All', array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'escape'=>false,
					  'update'=>'#balancetransferpage',
					  'class'=>'massactionbtn',
					  'div'=>false,
					  'controller'=>'balancetransfer',
					  'action'=>'balancetransferhistoryremove/0',
					  'confirm' => 'Are You Sure?',
					  'title'=>'',
					  'url'   => array('controller' => 'balancetransfer', 'action' => 'balancetransferhistoryremove/0')
					));?>
				</li>
				<?php } ?>
				</li>
			  </ul>
			</div>
		</div>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Balransferhistory',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'balancetransfer','action'=>'balancetransferhistory')));?>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Date', array('controller'=>'balancetransfer', "action"=>"balancetransferhistory/0/0/req_dt/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Date'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('From<br />Member', array('controller'=>'balancetransfer', "action"=>"balancetransferhistory/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By From Member'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('To<br />Member', array('controller'=>'balancetransfer', "action"=>"balancetransferhistory/0/0/to_member/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By To Member'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Amount', array('controller'=>'balancetransfer', "action"=>"balancetransferhistory/0/0/amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Amount'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Transferred<br />Amount', array('controller'=>'balancetransfer', "action"=>"balancetransferhistory/0/0/tran_amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Transferred Amount'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Fees', array('controller'=>'balancetransfer', "action"=>"balancetransferhistory/0/0/fees/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Fees'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Processor', array('controller'=>'balancetransfer', "action"=>"balancetransferhistory/0/0/processorname/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Processor'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Balance', array('controller'=>'balancetransfer', "action"=>"balancetransferhistory/0/0/topaymentmethod/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Balance'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('IP Address', array('controller'=>'balancetransfer', "action"=>"balancetransferhistory/0/0/ip_add/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By IP Address'
					));?>
				</div>
				<div><?php echo 'Status';?></div>
				<div><?php echo 'Action';?></div>
            </div>
			<?php foreach ($bal_transferdata as $bal_transfer): ?>
				<div class="tablegridrow">
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $bal_transfer['Balransferhistory']['req_dt']); ?></div>
					<div>
						<?php 
						echo $this->Js->link($bal_transfer['Balransferhistory']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$bal_transfer['Balransferhistory']['member_id']."/top/balancetransfer/balancetransfer~index~top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
                    <div>
						<?php 
						echo $this->Js->link($bal_transfer['Balransferhistory']['to_member'], array('controller'=>'member', "action"=>"memberadd/".$bal_transfer['Balransferhistory']['to_member']."/top/balancetransfer/balancetransfer~index~top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>$<?php echo $bal_transfer['Balransferhistory']['amount']; ?></div>
					<div>$<?php echo $bal_transfer['Balransferhistory']['tran_amount']; ?></div>
					<div>$<?php echo $bal_transfer['Balransferhistory']['fees']; ?></div>
					<div><?php echo $bal_transfer['Balransferhistory']['processorname']; ?></div>
					<div>
						<?php
						if($bal_transfer['Balransferhistory']['topaymentmethod']=='cash')
							echo 'Cash Balance';
						elseif($bal_transfer['Balransferhistory']['topaymentmethod']=='repurchase')
							echo 'Re-purchase Balance';
						elseif($bal_transfer['Balransferhistory']['topaymentmethod']=='earning')
							echo 'Earning Balance';
						elseif($bal_transfer['Balransferhistory']['topaymentmethod']=='commission')
							echo 'Commission Balance';
						else
							echo '-';
						?>
					</div>
					<div><?php echo $bal_transfer['Balransferhistory']['ip_add']; ?></div>
					<div><?php echo $bal_transfer['Balransferhistory']['status']; ?></div>
					<div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('balancetransfer/adminpanel_balancetransferhistoryremove',$SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Balance Transfer History')).' Delete Balance Transfer History', array('controller'=>'balancetransfer', "action"=>"balancetransferhistoryremove/".$bal_transfer['Balransferhistory']['id']."/".$currentpagenumber), array(
									'update'=>'#balancetransferpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'confirm'=>"Do You Really Want to Delete This Record?"
								));?>
							</li>
							<?php } ?>
						  </ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
	</div>
	
	<?php if(count($bal_transferdata)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
    <?php echo $this->Form->end();
	if($this->params['paging']['Balransferhistory']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Balransferhistory',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'balancetransfer','action'=>'balancetransferhistory/0/rpp')));?>
		
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#balancetransferpage',
			  'class'=>'large white button',
			  'div'=>false,
			  'controller'=>'balancetransfer',
			  'action'=>'balancetransferhistory/0/rpp',
			  'url'   => array('controller' => 'balancetransfer', 'action' => 'balancetransferhistory/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#balancetransferpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>