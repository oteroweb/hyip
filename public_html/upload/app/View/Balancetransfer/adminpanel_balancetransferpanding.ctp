<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 20-11-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Balance Transfer</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Member to Member Pending Requests", array('controller'=>'balancetransfer', "action"=>"balancetransferpanding"), array(
					'update'=>'#balancetransferpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Internal Transfer Pending Requests", array('controller'=>'balancetransfer', "action"=>"processorpanding"), array(
					'update'=>'#balancetransferpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Member to Member Transfer History", array('controller'=>'balancetransfer', "action"=>"balancetransferhistory"), array(
					'update'=>'#balancetransferpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Internal Transfer History", array('controller'=>'balancetransfer', "action"=>"processorhistory"), array(
					'update'=>'#balancetransferpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
		</ul>
	</div>
</div>
<div class="tab-content">
<div id="balancetransferpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Balance_Transfer#Member_to_Member_Pending_Requests" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Bal_pending_transfer',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'balancetransfer','action'=>'balancetransferpanding')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('searchby', array(
						      'type' => 'select',
						      'options' => array('all'=>'Select Parameter', 'member_id'=>'From Member', 'to_member'=>'To Member', 'amount'=>'Amount',  'tran_amount'=>'Transferred Amount', 'fees'=>'Fees', 'ip_add'=>'IP Address'),
						      'selected' => $searchby,
						      'class'=>'',
						      'label' => false,
						      'style' => ''
						));
						?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span class="searchforfields_s"><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#balancetransferpage',
					'class'=>'searchbtn',
					'controller'=>'balancetransfer',
					'action'=>'balancetransferpanding',
					'url'=> array('controller' => 'balancetransfer', 'action' => 'balancetransferpanding')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#balancetransferpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'balancetransfer', 'action'=>'balancetransferpanding')
	));
	$currentpagenumber=$this->params['paging']['Bal_pending_transfer']['page'];
	?>
<div id="gride-bg">
    <div class="Xpadding10">	
	<?php echo $this->Form->create('Bal_pending_transfer',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'balancetransfer','action'=>'balancetransferpandingpay')));?>
	<div class="height10"></div>
	<div class="paginator-text paddingtop0"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php 
		echo $this->Form->checkbox('selectAllCheckboxes', array(
		  'hiddenField' => false,
		  'onclick' => 'selectAllCheckboxes("pendingIds",this.checked)',
		  'id'=>'pendingIdAll'
		));
		?>
		<label for="pendingIdAll"></label>
	</div>
	<div class="addnew-button massactionbox">
		<div class="actionmenu">
			<div class="btn-group">
				<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
				  Action <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('balancetransfer/adminpanel_balancetransferpandingpay',$SubadminAccessArray)){ ?>
				<li>
					<?php
					echo $this->Js->submit('Pay to selected list', array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'escape'=>false,
					  'update'=>'#balancetransferpage',
					  'class'=>'massactionbtn',
					  'div'=>false,
					  'controller'=>'balancetransfer',
					  'action'=>'balancetransferpandingpay',
					  'confirm' => 'Are You Sure You Want to Pay Towards Selected Request(s)?',
					  'title'=>'',
					  'url'   => array('controller' => 'balancetransfer', 'action' => 'balancetransferpandingpay')
					));?>
				</li>
				<?php } ?>
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('balancetransfer/adminpanel_balancetransferpandingremove',$SubadminAccessArray)){ ?>
				<li>
					<?php echo $this->Js->submit('Cancel selected request(s)', array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'escape'=>false,
					  'update'=>'#balancetransferpage',
					  'class'=>'massactionbtn',
					  'div'=>false,
					  'controller'=>'balancetransfer',
					  'action'=>'balancetransferpandingremove',
					  'confirm' => 'Are You Sure You Want to Cancel Selected Request(s)?',
					  'title'=>'',
					  'url'   => array('controller' => 'balancetransfer', 'action' => 'balancetransferpandingremove')
					));?>
				</li>
				<?php } ?>
				</li>
			  </ul>
			</div>
		</div>
	</div>
	<div class="clear-both"></div>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Date', array('controller'=>'balancetransfer', "action"=>"balancetransferpanding/0/0/req_dt/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Date'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('From<br />Member', array('controller'=>'balancetransfer', "action"=>"balancetransferpanding/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By From Member'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('To<br />Member', array('controller'=>'balancetransfer', "action"=>"balancetransferpanding/0/0/to_member/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By To Member'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Amount', array('controller'=>'balancetransfer', "action"=>"balancetransferpanding/0/0/amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Amount'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Transferred<br />Amount', array('controller'=>'balancetransfer', "action"=>"balancetransferpanding/0/0/tran_amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Transferred Amount'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Fees', array('controller'=>'balancetransfer', "action"=>"balancetransferpanding/0/0/fees/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Fees'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('IP Address', array('controller'=>'balancetransfer', "action"=>"balancetransferpanding/0/0/ip_add/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By IP Address'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Processor', array('controller'=>'balancetransfer', "action"=>"balancetransferpanding/0/0/processor/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Processor'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Balance', array('controller'=>'balancetransfer', "action"=>"balancetransferpanding/0/0/topaymentmethod/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#balancetransferpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Balance'
					));?>
				</div>
				<div><?php echo 'Status';?></div>
				<div><?php echo 'Action';?></div>
				<div></div>
            </div>
			<?php $i=0; foreach ($bal_transferdata as $bal_transfer): $i++; ?>
				<div class="tablegridrow">
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $bal_transfer['Bal_pending_transfer']['req_dt']); ?></div>
					<div>
						<?php 
						echo $this->Js->link($bal_transfer['Bal_pending_transfer']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$bal_transfer['Bal_pending_transfer']['member_id']."/top/balancetransfer/balancetransfer~index~top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
                    <div>
						<?php 
						echo $this->Js->link($bal_transfer['Bal_pending_transfer']['to_member'], array('controller'=>'member', "action"=>"memberadd/".$bal_transfer['Bal_pending_transfer']['to_member']."/top/balancetransfer/balancetransfer~index~top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>$<?php echo $bal_transfer['Bal_pending_transfer']['amount']; ?></div>
					<div>$<?php echo $bal_transfer['Bal_pending_transfer']['tran_amount']; ?></div>
					<div>$<?php echo $bal_transfer['Bal_pending_transfer']['fees']; ?></div>
					<div><?php echo $bal_transfer['Bal_pending_transfer']['ip_add']; ?></div>
					<div><?php echo $bal_transfer['Bal_pending_transfer']['processor']; ?></div>
					<div>
						<?php
						if($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='cash')
							echo 'Cash Balance';
						elseif($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='repurchase')
							echo 'Re-purchase Balance';
						elseif($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='earning')
							echo 'Earning Balance';
						elseif($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='commission')
							echo 'Commission Balance';
						else
							echo '-';
						?>
					</div>
					<div><?php echo $bal_transfer['Bal_pending_transfer']['status']; ?></div>
					<div class="textcenter">
					  <span class="actionmenu">
						<span class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('balancetransfer/adminpanel_balancetransferpandingremove',$SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Transaction')).' Delete Transaction', array('controller'=>'balancetransfer', "action"=>"balancetransferpandingremove/".$bal_transfer['Bal_pending_transfer']['id']."/".$currentpagenumber), array(
									'update'=>'#balancetransferpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'confirm'=>"Do You Really Want To Delete This Transaction?"
								));?>
							</li>
							<?php } ?>
							<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('balancetransfer/adminpanel_balancetransferpandingpay',$SubadminAccessArray)){ ?>
							<li>
									<?php echo $this->Js->link($this->html->image('check.png', array('alt'=>'Balance Transfer')).' Allow This Balance Transfer', array('controller'=>'balancetransfer', "action"=>"balancetransferpandingpay/".$bal_transfer['Bal_pending_transfer']['id']."/".$currentpagenumber), array(
                                        'update'=>'#balancetransferpage',
                                        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                        'escape'=>false,
                                        'confirm' => 'Are You Sure You Want to Allow This Balance Transfer?'
                                    ));?>
                             </li>
							<?php } ?>
						  </ul>
						</span>
					</span>
				</div>
				<div>
					<span class="checkbox">
					<?php
					echo $this->Form->checkbox('pendingIds.', array(
					  'value' => $bal_transfer['Bal_pending_transfer']['id'],
					  'class' => 'pendingIds',
					  'hiddenField' => false,
					  'id'=>'pendingIds'.$i
					));
					?>
					<label for="pendingIds<?php echo $i;?>"></label>
					</span>
				</div>
			</div>
			<?php endforeach; ?>
	</div>
	
	<?php if(count($bal_transferdata)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
    <?php echo $this->Form->end();
	if($this->params['paging']['Bal_pending_transfer']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Bal_pending_transfer',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'balancetransfer','action'=>'balancetransferpanding/0/rpp')));?>
		
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'searchcombobox',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#balancetransferpage',
			  'class'=>'large white button',
			  'div'=>false,
			  'controller'=>'balancetransfer',
			  'action'=>'balancetransferpanding/0/rpp',
			  'url'   => array('controller' => 'balancetransfer', 'action' => 'balancetransferpanding/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#balancetransferpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>