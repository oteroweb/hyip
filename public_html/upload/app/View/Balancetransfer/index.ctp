<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 20-11-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($EnableBalancetransfer==1) { ?>
<?php if(!$isajax) { ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<?php } ?>
<div id="Balancetransfer">
<div id="UpdateMessage"></div>
<?php if(!$isajax) { ?>

<?php // Current Balance code starts here ?>
<div class="textright"><a class="shbutton currentbalancebutton" href="javascript:void(0)" onclick="currentbalancebox('.balancebox', this,'<?php echo __("[+] Show Balance");?>','<?php echo __("[-] Hide Balance");?>');"><?php if(@$_COOKIE['curbal']==1)echo __("[+] Show Balance"); else echo __("[-] Hide Balance");?>
</a></div>
<div class="comisson-bg balancebox <?php if(@$_COOKIE['curbal']==1)echo 'hide';?>">
	<div class="text-ads-title-text"><?php echo __("Current Balance");?></div>
	<div class="text-ads-title-text-right"></div>
	<div class="clear-both"></div>
</div>
<div id="balancebox" class="main-box-eran balancebox <?php if(@$_COOKIE['curbal']==1)echo 'hide';?>">
	<?php if($SITECONFIG["balance_type"]==1){ ?>
		<div class="divtable">
                        <div class="divtr white-color">
                                <div class="divth textcente vam"><?php echo __("Cash Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCash*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
                        <div class="divtr gray-color">
                                <div class="divth textcente vam"><?php echo __("Earning Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberEarning*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php } ?>
                        <div class="divtr white-color">
                                <div class="divth textcente vam"><?php echo __("Re-purchase Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberRepurchaseCash*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
                        <div class="divtr gray-color">
                                <div class="divth textcente vam"><?php echo __("Commission Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCommission*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php } ?>
                </div>
		<?php }else{ 
			?>
			<div class="divtable">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam"><?php echo __("Payment Processor");?></div>
						<div class="divth textcenter vam"><?php echo __("Cash Balance");?></div>
                                                <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
						<div class="divth textcenter vam"><?php echo __("Earning Balance");?></div>
                                                <?php } ?>
						<div class="divth textcenter vam"><?php echo __("Re-purchase Balance");?></div>
                                                <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
						<div class="divth textcenter vam"><?php echo __("Commission Balance");?></div>
                                                <?php } ?>
					</div>
				</div>
				<?php $i=1;
				foreach($MemberCash as $proc_name=>$cash)
				{
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $proc_name;?> :</div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCash[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberEarning[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php } ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberRepurchaseCash[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCommission[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php } ?>
					</div>
					<?php
				$i++; }?>
			</div>
	<?php } ?>
</div>
<?php // Current Balance code ends here ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Balance Transfer");?></div>
	<div class="clear-both"></div>
</div>

<?php // Code to show Receivable amount (after deducting fees) as member enters Transfer Amount starts here ?>
<script type="text/javascript">
function calculatetotal(amt)
{
	<?php
	if(strpos($SITECONFIG['balancetransfersetting'],'#feestype@0') !== false)
	{ ?>
		if(<?php echo round($minbalancelimit*$Currency['rate'],4);?> <= amt)
			$("#totalamt").text("<?php echo $Currency['prefix'];?>"+parseFloat(amt-<?php echo round($adminfees*$Currency['rate'],4);?>).toFixed(4)+"<?php echo ' '.$Currency['suffix']; ?>"+" (<?php echo $Currency['prefix'].round($adminfees*$Currency['rate'],4).' '.$Currency['suffix']; ?>)");
		else
			$("#totalamt").text("N/A");
	<?php }
	else
	{ ?>
		if(<?php echo round($minbalancelimit*$Currency['rate'],4);?> <= amt)
			$("#totalamt").text("<?php echo $Currency['prefix'];?>"+(amt-(amt*<?php echo $adminfees; ?>/100))+"<?php echo' '.$Currency['suffix']; ?>"+" (<?php echo $adminfees; ?>%)"  );
		else
			$("#totalamt").text("N/A");
	<?php }
	?>
}
function pcalculatetotal(amt)
{
	
		<?php
		if(strpos($SITECONFIG['balancetransfersetting'],'#profeestype@0') !== false)
		{ ?>
			var adminfree="<?php echo $Currency['prefix'].round($proadminfees*$Currency['rate'],4).' '.$Currency['suffix']; ?>";
			paidamount=parseFloat(amt-<?php echo round($proadminfees*$Currency['rate'],4);?>).toFixed(4);
		<?php }
		else
		{ ?>
			var adminfree=<?php echo $proadminfees; ?>+"%";
			paidamount=amt-(amt*<?php echo $proadminfees; ?>/100);
		<?php }
		?>
		var profee='<?php echo $processorfee;?>';
		var prostr=$("#Bal_pending_transferPaymentprocessorfrom").val()+"_"+$("#Bal_pending_transferPaymentprocessorto").val()+"[-]";
		var strpos1 = profee.indexOf(prostr);
		var strpos2 = profee.indexOf("|", strpos1);
		var feerate = profee.substring((strpos1+prostr.length), strpos2);
		var feeratearray = feerate.split(":");
		var ratio1=feeratearray[0];
		var ratio2=feeratearray[1];
		
		var currency = <?php echo $Currency['rate'] ?>
		
		var minlimit='<?php echo $minlimit;?>';
		var minprostr=$("#Bal_pending_transferPaymentprocessorfrom").val()+":";
		var minstrpos1 = minlimit.indexOf(minprostr);
		var minstrpos2 = minlimit.indexOf("|", minstrpos1);
		var minlimitrate = minlimit.substring((minstrpos1+minprostr.length), minstrpos2);
		var minlimitrate = minlimit.substring((minstrpos1+minprostr.length), minstrpos2);
		var minimumlimitamount=Math.round(minlimitrate*currency);
		
		var maxlimit='<?php echo $maxlimit;?>';
		var maxprostr=$("#Bal_pending_transferPaymentprocessorfrom").val()+":";
		var maxstrpos1 = maxlimit.indexOf(maxprostr);
		var maxstrpos2 = maxlimit.indexOf("|", maxstrpos1);
		var maxlimitrate = maxlimit.substring((maxstrpos1+maxprostr.length), maxstrpos2);
		var maxlimitrate = maxlimit.substring((maxstrpos1+maxprostr.length), maxstrpos2);
		var maxlimitamount=Math.round(maxlimitrate*currency);
		//var getamount=Math.round(ratio2*paidamount/ratio1).toFixed(2);
                var getamount=ratio2*paidamount/ratio1;
		<?php
		if(strpos($SITECONFIG['balancetransfersetting'],'pymenttoprocessor@1') !== false)
		{ ?>
		if(minimumlimitamount <= amt && (maxlimitamount >= amt || maxlimitamount==0))
			$("#totalamt").text("<?php echo $Currency['prefix'];?>"+getamount+"<?php echo ' '.$Currency['suffix']; ?> ("+feerate+")+("+adminfree+")");
		else
			$("#totalamt").text("N/A");
		<?php } else{ ?>
		if(minimumlimitamount <= amt && (maxlimitamount >= amt || maxlimitamount==0))
			$("#totalamt").text("<?php echo $Currency['prefix'];?>"+paidamount+"<?php echo ' '.$Currency['suffix']; ?> ("+adminfree+")");
		else
			$("#totalamt").text("N/A");
		<?php } ?>
	
}
function pcalculatetotaltype1(amt)
{
	
		<?php
		if(strpos($SITECONFIG['balancetransfersetting'],'profeestype@0') !== false)
		{ ?>
			if(<?php echo round($minbalancelimitpro*$Currency['rate'],4);?> <= amt)
				$("#totalamt").text("<?php echo $Currency['prefix'];?>"+parseFloat(amt-<?php echo round($proadminfees*$Currency['rate'],4);?>).toFixed(4)+"<?php echo ' '.$Currency['suffix']; ?>"+" (<?php echo $Currency['prefix'].round($proadminfees*$Currency['rate'],4).' '.$Currency['suffix']; ?>)");
			else
				$("#totalamt").text("N/A");
		<?php }
		else
		{ ?>
			if(<?php echo round($minbalancelimitpro*$Currency['rate'],4);?> <= amt)
				$("#totalamt").text("<?php echo $Currency['prefix'];?>"+(amt-(amt*<?php echo $proadminfees; ?>/100))+"<?php echo' '.$Currency['suffix']; ?>"+" (<?php echo $proadminfees; ?>%)"  );
			else
				$("#totalamt").text("N/A");
		<?php } ?>	
}
</script>
<?php // Code to show Receivable amount (after deducting fees) as member enters Transfer Amount ends here ?>

<div class="main-box-eran">
	
	<?php // New balance transfer form starts here ?>
	<?php echo $this->Form->create('Bal_pending_transfer',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'balancetransfer','action'=>'index')));?>
		<?php if($SITECONFIG['balance_type']==1){ ?>
		<div class="form-box">
			<?php if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false && strpos($SITECONFIG['balancetransfersetting'],'isenablepro@1') !== false) { ?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Transfer Type");?> : </div>
				<div class="select-dropdown">
					<label>
						<?php 
						echo $this->Form->input('memberorprocessor', array(
							  'type' => 'select',
							  'options' => array(0=>__('Transfer To Member'),1=>__('Transfer To Internal Balance')),
							  'selected' => '',
							  'class'=>'searchcomboboxwidth',
							  'label' => false,
							  'div' => false,
							  'onchange' => 'if(this.selectedIndex==1){$(".processorfield").show(500);$(".memberfield").hide(0);pcalculatetotaltype1($("#Bal_pending_transferPamount").val())}else{$(".processorfield").hide(0);$(".memberfield").show(500);calculatetotal($("#Bal_pending_transferAmount").val());}'
						));?>
					</label>
				</div>
				
			</div>
			<?php } ?>
			<?php if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){
			
			echo $this->Form->input('paymentprocessor', array('type'=>'hidden', 'value'=>$payment_processorid, 'label' => false));	?>
			
			<div class="form-row memberfield">
				<div class="form-col-1"><?php echo __("Balance");?> : </div>
				<?php $paymentmethod=array();
				if(strpos($balance,'cash') !== false)
					$paymentmethod['cash']=__('Cash Balance');
				if(strpos($balance,'repurchase') !== false)
					$paymentmethod['repurchase']=__('Re-purchase Balance');
				if(strpos($balance,'earning') !== false)
					$paymentmethod['earning']=__('Earning Balance');
				if(strpos($balance,'commission') !== false)
					$paymentmethod['commission']=__('Commission Balance');
					?>
				<div class="select-dropdown">
					<label>
						<?php
						$onchange='calculatetotal($("#Bal_pending_transferAmount").val())';
						echo $this->Form->input('balance', array(
							  'type' => 'select',
							  'options' => $paymentmethod,
							  'selected' => '',
							  'class'=>'searchcomboboxwidth',
							  'label' => false,
							  'div' => false,
							  'style' => '',
							  'onchange' => $onchange
						));?>
					</label>
				</div>
			</div>
			<div class="form-row memberfield">
				<div class="form-col-1 formobileclass" style="min-width: 170px;"><?php echo __("Transfer To")." : ";?><span class="required">*</span></div>
				<div class="form-col-2">
					<div class="select-dropdown" style="max-width: 115px;vertical-align:top">
						<label>
							<?php 
							echo $this->Form->input('transfertype', array(
								'type' => 'select',
								'options' => array(0=>__('Username'),1=>__('Member Id')),
								'selected' => '',
								'class'=>'searchcomboboxwidth',
								'label' => false,
								'style'=>'max-width:115px !important',
								'div' => false,
							));?>
						</label>
					</div>
					<?php echo $this->Form->input('transferto', array('type'=>'text', 'label' => false,'div' => false, 'class'=>'formtextbox', 'style'=>'width:50%; max-width:280px;'));?>
				</div>
				
			</div>
			<div class="form-row memberfield">
				<div class="form-col-1"><?php echo __("Transfer Amount");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('amount', array('type'=>'text', 'label' => false,'div'=>false, 'class'=>'formtextbox', 'style'=>'', 'onkeyup'=>'calculatetotal(this.value)'));?></div>
			</div>
			<?php } if(strpos($SITECONFIG['balancetransfersetting'],'isenablepro@1') !== false){
			$displaypro='';	
			if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){ $displaypro='none';}
			?>
			<div class="form-row processorfield" style="display: <?php echo $displaypro; ?>;">
				<div class="form-col-1"><?php echo __("From Balance");?> : </div>
				
				<?php
				$paymentmethod=array();
				if(strpos($frompaymentmethod,'cash') !== false)
					$paymentmethod['cash']=__('Cash Balance');
				if(strpos($frompaymentmethod,'repurchase') !== false)
					$paymentmethod['repurchase']=__('Re-purchase Balance');
				if(strpos($frompaymentmethod,'earning') !== false)
					$paymentmethod['earning']=__('Earning Balance');
				if(strpos($frompaymentmethod,'commission') !== false)
					$paymentmethod['commission']=__('Commission Balance');
				?>
				<div class="select-dropdown">
					<label>
						<?php
						echo $this->Form->input('paymentmethodfrom', array(
							  'type' => 'select',
							  'options' => $paymentmethod,
							  'selected' => '',
							  'class'=>'searchcomboboxwidth',
							  'label' => false,
							  'div' => false,
							  'style' => '',
							  'onchange' => 'pcalculatetotaltype1($("#Bal_pending_transferPamount").val())'
						));
						?>
					</label>
				</div>
			</div>
			<div class="form-row processorfield" style="display: <?php echo $displaypro; ?>;">
				<div class="form-col-1"><?php echo __("To Balance");?> : </div>
				<?php
				$paymentmethod=array();
				if(strpos($topaymentmethod,'cash') !== false)
					$paymentmethod['cash']=__('Cash Balance');
				if(strpos($topaymentmethod,'repurchase') !== false)
					$paymentmethod['repurchase']=__('Re-purchase Balance');
				if(strpos($topaymentmethod,'earning') !== false)
					$paymentmethod['earning']=__('Earning Balance');
				if(strpos($topaymentmethod,'commission') !== false)
					$paymentmethod['commission']=__('Commission Balance');
				?>
				<div class="select-dropdown">
					<label>
						<?php
							echo $this->Form->input('paymentmethodto', array(
								  'type' => 'select',
								  'options' => $paymentmethod,
								  'selected' => '',
								  'class'=>'searchcomboboxwidth',
								  'label' => false,
								  'div' => false,
								  'style' => '',
								  'onchange' => 'pcalculatetotaltype1($("#Bal_pending_transferPamount").val())'
							));
						?>
					</label>
				</div>
			</div>
			<div class="form-row processorfield" style="display: <?php echo $displaypro; ?>;">
				<div class="form-col-1"><?php echo __("Transfer Amount");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('pamount', array('type'=>'text', 'label' => false,'div'=>false, 'class'=>'formtextbox', 'style'=>'', 'onkeyup'=>'pcalculatetotaltype1(this.value)'));?></div>
			</div>
			<?php } ?>
			<div style="display: none;"></div>
			<div class="form-row">
				<div class="form-col-1 form-text"><?php echo __("Receivable (after deducting fees)");?> : </div>
				<span id="totalamt"><?php echo __("Fill The Amount");?></span>
			</div>
		</div>
		<?php } elseif($SITECONFIG['balance_type']==2){ ?>
		<div class="form-box">
			<?php if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false && strpos($SITECONFIG['balancetransfersetting'],'isenablepro@1') !== false && $SITECONFIG['balance_type']==2) { ?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Transfer Type");?> : </div>
				<div class="select-dropdown">
					<label>
						<?php 
						echo $this->Form->input('memberorprocessor', array(
							  'type' => 'select',
							  'options' => array(0=>__('Transfer To Member'),1=>__('Transfer To Processor')),
							  'selected' => '',
							  'class'=>'searchcomboboxwidth',
							  'label' => false,
							  'div' => false,
							  'onchange' => 'if(this.selectedIndex==1){$(".processorfield").show(500);$(".memberfield").hide(0);pcalculatetotal($("#Bal_pending_transferPamount").val())}else{$(".processorfield").hide(0);$(".memberfield").show(500);calculatetotal($("#Bal_pending_transferAmount").val());}'
						));?>
					</label>
				</div>
				
			</div>
			<?php } ?>
			<?php if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){ ?>
                    <!--    New Fields Added    -->
                            <?php if($SITECONFIG['balance_type']==2){ ?>
                            <div class="form-row memberfield" style="display: <?php echo $displaypro; ?>;">
				<div class="form-col-1"><?php echo __("Payment Processor");?> : </div>
				<div class="form-col-2">
					<?php
					$paymentprocessorfrom_member=array();
					$fromprocessor_member=@explode(",", $fromprocessor);
                                       
					foreach($paymentprocessors as $pid=>$pnm)
					{
						if(in_array($pid, $fromprocessor_member))
						   $paymentprocessorfrom_member[$pid]=$pnm;
					}
					?>
					<div class="select-dropdown">
						<label>
							<?php
							echo $this->Form->input('paymentprocessorfrom_member', array(
								'type' => 'select',
								'options' => $paymentprocessorfrom_member,
								'selected' => '',
								'class'=>'searchcomboboxwidth',
								'label' => false,
								'div' => false,
								'style' => '',
								'onchange' => 'pcalculatetotal($("#Bal_pending_transferPamount").val());$("#toprocessername").val(" "+$(this).find("option:selected").text());'
							));
							?>
						</label>
					</div>
					
				</div>
			</div>
                        <?php } else {
			echo $this->Form->input('paymentprocessor', array('type'=>'hidden', 'value'=>$payment_processorid, 'label' => false));	
			}	?>
                    
                            <div class="form-row memberfield" style="display: <?php echo $displaypro; ?>;">
				<div class="form-col-1"><?php echo __("From Balance");?> : </div>
				<div class="form-col-2">
					<?php
					
					$paymentmethod_member=array();
					if(strpos($frompaymentmethod_member,'cash') !== false)
						$paymentmethod_member['cash']=__('Cash Balance');
					if(strpos($frompaymentmethod_member,'repurchase') !== false)
						$paymentmethod_member['repurchase']=__('Re-purchase Balance');
					if(strpos($frompaymentmethod_member,'earning') !== false)
						$paymentmethod_member['earning']=__('Earning Balance');
					if(strpos($frompaymentmethod_member,'commission') !== false)
						$paymentmethod_member['commission']=__('Commission Balance');
					if(strpos($frompaymentmethod_member,'processor') !== false)
						$paymentmethod_member['processor']=__('Payment Processor');
						?>
						<div class="select-dropdown">
							<label>
								<?php
                                                                if($SITECONFIG['balance_type']==2)
                                                                {
                                                                        $onchange='pcalculatetotal($("#Bal_pending_transferPamount").val())';
                                                                }
                                                                else
                                                                {
                                                                        $onchange='calculatetotal($("#Bal_pending_transferAmount").val())';
                                                                }
								echo $this->Form->input('paymentmethodfrom_member', array(
                                                                    'type' => 'select',
                                                                    'options' => $paymentmethod_member,
                                                                    'selected' => '',
                                                                    'class'=>'searchcomboboxwidth',
                                                                    'label' => false,
                                                                    'div' => false,
                                                                    'style' => '',
                                                                    'onchange' => $onchange
								));?>
							</label>
						</div>
				</div>
			</div>
                            
                            <div class="form-row memberfield" style="display: <?php echo $displaypro; ?>;">
				<div class="form-col-1"><?php echo __("To Balance");?> : </div>
				<div class="form-col-2">
					<?php
					
					$paymentmethod_member=array();
					if(strpos($topaymentmethod_member,'cash') !== false)
						$paymentmethod_member['cash']=__('Cash Balance');
					if(strpos($topaymentmethod_member,'repurchase') !== false)
						$paymentmethod_member['repurchase']=__('Re-purchase Balance');
					if(strpos($topaymentmethod_member,'earning') !== false)
						$paymentmethod_member['earning']=__('Earning Balance');
					if(strpos($topaymentmethod_member,'commission') !== false)
						$paymentmethod_member['commission']=__('Commission Balance');
					if(strpos($topaymentmethod_member,'processor') !== false)
						$paymentmethod_member['processor']=__('Payment Processor');
						?>
						<div class="select-dropdown">
							<label>
								<?php
									echo $this->Form->input('paymentmethodto_member', array(
										  'type' => 'select',
										  'options' => $paymentmethod_member,
										  'selected' => '',
										  'class'=>'searchcomboboxwidth',
										  'label' => false,
										  'div' => false,
										  'style' => '',
										  'onchange' => 'pcalculatetotal($("#Bal_pending_transferPamount").val())'
									));
								?>
							</label>
						</div>
				</div>
			</div>
                    
                    <!--    New Fields Added    -->
                    
		    
			<div class="form-row memberfield">
				<div class="form-col-1 formobileclass" style="min-width: 170px;"><?php echo __("Transfer To")." : ";?><span class="required">*</span></div>
				<div class="form-col-2">
					<div class="select-dropdown" style="max-width: 115px;vertical-align:top">
						<label>
							<?php 
							echo $this->Form->input('transfertype', array(
								'type' => 'select',
								'options' => array(0=>__('Username'),1=>__('Member Id')),
								'selected' => '',
								'class'=>'searchcomboboxwidth',
								'label' => false,
								'style'=>'max-width:115px !important',
								'div' => false,
							));?>
						</label>
					</div>
					<?php echo $this->Form->input('transferto', array('type'=>'text', 'label' => false,'div' => false, 'class'=>'formtextbox', 'style'=>'width:50%; max-width:280px;'));?>
				</div>
				
			</div>
			<div class="form-row memberfield">
				<div class="form-col-1"><?php echo __("Transfer Amount");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('amount', array('type'=>'text', 'label' => false,'div'=>false, 'class'=>'formtextbox', 'style'=>'', 'onkeyup'=>'calculatetotal(this.value)'));?></div>
			</div>
			<?php } if(strpos($SITECONFIG['balancetransfersetting'],'isenablepro@1') !== false  && $SITECONFIG['balance_type']==2){
			$displaypro='';	
			if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){ $displaypro='none';}
			?>
			<div class="form-row processorfield" style="display: <?php echo $displaypro; ?>;">
				<div class="form-col-1"><?php echo __("From Payment Processor");?> : </div>
				<div class="form-col-2">
					<?php
					$paymentprocessorfrom=array();
					$fromprocessor=@explode(",", $fromprocessor);
					foreach($paymentprocessors as $pid=>$pnm)
					{
						if(in_array($pid, $fromprocessor))
						   $paymentprocessorfrom[$pid]=$pnm;
					}
					?>
					<div class="select-dropdown baltransfer">
						<label>
							<?php
							echo $this->Form->input('paymentprocessorfrom', array(
								'type' => 'select',
								'options' => $paymentprocessorfrom,
								'selected' => '',
								'class'=>'searchcomboboxwidth',
								'label' => false,
								'div' => false,
								'style' => '',
								'onchange' => 'pcalculatetotal($("#Bal_pending_transferPamount").val());$("#toprocessername").val(" "+$(this).find("option:selected").text());'
							));
							?>
						</label>
					</div>
					
					<?php
					
					$paymentmethod=array();
					if(strpos($frompaymentmethod,'cash') !== false)
						$paymentmethod['cash']=__('Cash Balance');
					if(strpos($frompaymentmethod,'repurchase') !== false)
						$paymentmethod['repurchase']=__('Re-purchase Balance');
					if(strpos($frompaymentmethod,'earning') !== false)
						$paymentmethod['earning']=__('Earning Balance');
					if(strpos($frompaymentmethod,'commission') !== false)
						$paymentmethod['commission']=__('Commission Balance');
					?>
						<div class="select-dropdown baltransfer">
							<label>
								<?php
								echo $this->Form->input('paymentmethodfrom', array(
									  'type' => 'select',
									  'options' => $paymentmethod,
									  'selected' => '',
									  'class'=>'searchcomboboxwidth',
									  'label' => false,
									  'div' => false,
									  'style' => '',
									  'onchange' => 'pcalculatetotal($("#Bal_pending_transferPamount").val())'
								));?>
							</label>
						</div>
				</div>
				</div>
			<div class="form-row processorfield" style="display: <?php echo $displaypro; ?>;">
				<div class="form-col-1"><?php echo __("To Payment Processor");?> : </div>
				<div class="form-col-2">
					<?php
					$paymentprocessorto=array();
					if(strpos($SITECONFIG['balancetransfersetting'],'pymenttoprocessor@1') !== false){
					$toprocessor=@explode(",", $toprocessor);
					foreach($paymentprocessors as $pid=>$pnm)
					{
						if(in_array($pid, $toprocessor))
						   $paymentprocessorto[$pid]=$pnm;
					}
					?>
						<div class="select-dropdown baltransfer">
							<label>
								<?php
									echo $this->Form->input('paymentprocessorto', array(
										  'type' => 'select',
										  'options' => $paymentprocessorto,
										  'selected' => '',
										  'class'=>'searchcomboboxwidth',
										  'label' => false,
										  'div' => false,
										  'style' => '',
										  'onchange' => 'pcalculatetotal($("#Bal_pending_transferPamount").val())'
									));
									?>
							</label>
						</div>
					<?php
					}
					else
					{
						echo '<input id="toprocessername" type="text" class="formtextbox" style="max-width: 195px;vertical-align:top;" value=" '.reset($paymentprocessorfrom).'" readonly="readonly"/>';
					}
					$paymentmethod=array();
					if(strpos($topaymentmethod,'cash') !== false)
						$paymentmethod['cash']=__('Cash Balance');
					if(strpos($topaymentmethod,'repurchase') !== false)
						$paymentmethod['repurchase']=__('Re-purchase Balance');
					if(strpos($topaymentmethod,'earning') !== false)
						$paymentmethod['earning']=__('Earning Balance');
					if(strpos($topaymentmethod,'commission') !== false)
						$paymentmethod['commission']=__('Commission Balance');
					?>
						<div class="select-dropdown baltransfer">
							<label>
								<?php
									echo $this->Form->input('paymentmethodto', array(
										  'type' => 'select',
										  'options' => $paymentmethod,
										  'selected' => '',
										  'class'=>'searchcomboboxwidth',
										  'label' => false,
										  'div' => false,
										  'style' => '',
										  'onchange' => 'pcalculatetotal($("#Bal_pending_transferPamount").val())'
									));
								?>
							</label>
						</div>
				</div>
			</div>
			<div class="form-row processorfield" style="display: <?php echo $displaypro; ?>;">
				<div class="form-col-1"><?php echo __("Transfer Amount");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('pamount', array('type'=>'text', 'label' => false,'div'=>false, 'class'=>'formtextbox', 'style'=>'', 'onkeyup'=>'pcalculatetotal(this.value)'));?></div>
			</div>
			<?php } ?>
			<div style="display: none;"></div>
			<div class="form-row">
				<div class="form-col-1 form-text"><?php echo __("Receivable (after deducting fees)");?> : </div>
				<span id="totalamt"><?php echo __("Fill The Amount");?></span>
			</div>
		</div>
		<?php } ?>
		<?php if($SITECONFIG["enableagree"]==1){?>
		<div class="profile-bot-left floatright textright"><?php echo $this->Form->input('termsandconditions', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>'')); echo ' <a href="'.$SITEURL.'public/terms" target="_blank">'.__('I agree with the terms and conditions').'</a> '.__('of').' '.$SITECONFIG["sitetitle"]; ?></div>
		<?php } ?>
		<div class="clear-both"></div>
		<div class="formbutton"> 
			<?php echo $this->Js->submit(ucfirst(__('transfer')), array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'button',
				'div'=>false,
				'controller'=>'balancetransfer',
				'action'=>'balancetransferaction',
				'url'   => array('controller' => 'balancetransfer', 'action' => 'balancetransferaction')
			));?>
		</div>
		<div class="clear-both"></div>
	<?php echo $this->Form->end();?>
	<?php // New balance transfer form ends here ?>
	
<?php }?>
</div>
<div id="Balancetransferpanding">

<?php // Table titles start here ?>
<div class="comisson-bg mobilecss">
	<div class="commison-menu">
		<ul>
			<li>
				<?php
				$actclass='act';
				if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){	
					echo $this->Js->link(__('Member Pending Requests'), array('controller'=>'balancetransfer', "action"=>"index"), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>$actclass
					));
					$actclass='';
				}
				?>
			</li>
			<li>
				<?php
				if(strpos($SITECONFIG['balancetransfersetting'],'isenablepro@1') !== false){
					echo $this->Js->link(__('Processor Pending Requests'), array('controller'=>'balancetransfer', "action"=>"pending"), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>$actclass
					));
				}
				?>
			</li>
			<li>
				<?php
				if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){
					echo $this->Js->link(__('Member Transfer History'), array('controller'=>'balancetransfer', "action"=>"history"), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false
					));
				}
				?>
			</li>
			<li>
				<?php
				if(strpos($SITECONFIG['balancetransfersetting'],'isenablepro@1') !== false){
					echo $this->Js->link(__('Processor Transfer History'), array('controller'=>'balancetransfer', "action"=>"history2"), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false
					));
				}
				?>
			</li>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Table titles end here ?>
<?php
$action='pending';
if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false)
	$action='index';
?>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#Balancetransferpanding',
		'evalScripts' => true,
		'url'=> array('controller'=>'balancetransfer', 'action'=>$action)
	));
	$currentpagenumber=$this->params['paging']['Bal_pending_transfer']['page'];
	?>
<div class="main-box-eran">
	<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
	<div class="clear-both"></div>
	
	<?php // Pending Requests table starts here ?>
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam">
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link(__('Date'), array('controller'=>'balancetransfer', "action"=>$action."/0/req_dt/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Date')
					));?>
				</div>
				<?php if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){ ?>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('To Member'), array('controller'=>'balancetransfer', "action"=>$action."/0/to_member/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('To Member')
					));?>
				</div>
				<?php } ?>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('Amount'), array('controller'=>'balancetransfer', "action"=>$action."/0/amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Amount')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('Transfer Amount'), array('controller'=>'balancetransfer', "action"=>$action."/0/tran_amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Transfer Amount')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('Fees'), array('controller'=>'balancetransfer', "action"=>$action."/0/fees/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Fees')
					));?>
				</div>
				<?php if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){ ?>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('Payment Processor'), array('controller'=>'balancetransfer', "action"=>$action."/0/processor_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Payment Processor')
					));?>
				</div>
				<?php } else { ?>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('From'), array('controller'=>'balancetransfer', "action"=>$action."/0/processor_id_from/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('From')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('To'), array('controller'=>'balancetransfer', "action"=>$action."/0/processor_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('To')
					));?>
				</div>
				<?php } ?>
				<?php if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){ ?>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('Balance'), array('controller'=>'balancetransfer', "action"=>$action."/0/topaymentmethod/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Balance')
					));?>
				</div>
				<?php } else { ?>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('From Balance'), array('controller'=>'balancetransfer', "action"=>$action."/0/frompaymentmethod/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('From Balance')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__('To Balance'), array('controller'=>'balancetransfer', "action"=>$action."/0/topaymentmethod/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Balancetransferpanding',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('To Balance')
					));?>
				</div>
				<?php } ?>
				<div class="divth textcenter vam"><?php echo __('Status');?></div>
			</div>
		</div>
		<div class="divtbody">
			<?php $i=1;
			foreach ($bal_transferdata as $bal_transfer):
			if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $bal_transfer['Bal_pending_transfer']['req_dt']); ?></div>
					<?php if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){ ?>
					<div class="divtd textcenter vam"><?php echo $bal_transfer['Bal_pending_transfer']['to_member']; ?></div>
					<?php } ?>
					<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($bal_transfer['Bal_pending_transfer']['amount']*$Currency['rate'],4)." ".$Currency['suffix']; ?></div>
					<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($bal_transfer['Bal_pending_transfer']['tran_amount']*$Currency['rate'],4)." ".$Currency['suffix']; ?></div>
					<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($bal_transfer['Bal_pending_transfer']['fees']*$Currency['rate'],4)." ".$Currency['suffix']; ?></div>
					<?php if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){ ?>
					<div class="divtd textcenter vam"><?php echo $bal_transfer['Bal_pending_transfer']['processor']; ?></div>
					<?php } else { ?>
					<div class="divtd textcenter vam"><?php echo $bal_transfer['Bal_pending_transfer']['processor_from']; ?></div>
					<div class="divtd textcenter vam"><?php echo $bal_transfer['Bal_pending_transfer']['processor']; ?></div>
					<?php } ?>
					<?php if(strpos($SITECONFIG['balancetransfersetting'],'isenable@1') !== false){ ?>
					<div class="divtd textcenter vam">
						<?php
							if($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='cash')
								echo __('Cash Balance');
							elseif($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='repurchase')
								echo __('Re-purchase Balance');
							elseif($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='earning')
								echo __('Earning Balance');
							elseif($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='commission')
								echo __('Commission Balance');
							else
								echo '-';
						?>
					</div>
					<?php } else { ?>
					<div class="divtd textcenter vam">
						<?php
							if($bal_transfer['Bal_pending_transfer']['frompaymentmethod']=='cash')
								echo __('Cash Balance');
							elseif($bal_transfer['Bal_pending_transfer']['frompaymentmethod']=='repurchase')
								echo __('Re-purchase Balance');
							elseif($bal_transfer['Bal_pending_transfer']['frompaymentmethod']=='earning')
								echo __('Earning Balance');
							elseif($bal_transfer['Bal_pending_transfer']['frompaymentmethod']=='commission')
								echo __('Commission Balance');
							else
								echo '-';
						?>
					</div>
					<div class="divtd textcenter vam">
						<?php
							if($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='cash')
								echo __('Cash Balance');
							elseif($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='repurchase')
								echo __('Re-purchase Balance');
							elseif($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='earning')
								echo __('Earning Balance');
							elseif($bal_transfer['Bal_pending_transfer']['topaymentmethod']=='commission')
								echo __('Commission Balance');
							else
								echo '-';
						?>
					</div>
					<?php } ?>
					<div class="divtd textcenter vam"><?php echo __($bal_transfer['Bal_pending_transfer']['status']); ?></div>
				</div>
			<?php $i++;endforeach; ?>
		</div>
	</div>
	<?php if(count($bal_transferdata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
	<?php // Pending Requests table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Bal_pending_transfer']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Bal_pending_transfer',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'balancetransfer','action'=>$action.'/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#Balancetransferpanding',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'balancetransfer',
						  'action'=>$action.'/rpp',
						  'url'   => array('controller' => 'balancetransfer', 'action' => $action.'/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
			</div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		
</div>
</div>
</div>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>