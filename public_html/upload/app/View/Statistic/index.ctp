<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php // Code to show site stats starts here. To be shown only if enabled from adminpanel ?>
<?php if(($isLogin && (strpos($SITECONFIG["memberstatistics"],'AlexaRank|1') !== false || strpos($SITECONFIG["memberstatistics"],'LaunchDate|1') !== false || strpos($SITECONFIG["memberstatistics"],'TotalMember|1') !== false)) || (!$isLogin && (strpos($SITECONFIG["publicstatistics"],'LaunchDate|1') !== false || strpos($SITECONFIG["publicstatistics"],'AlexaRank|1') !== false || strpos($SITECONFIG["publicstatistics"],'TotalMember|1') !== false))){ ?>
<div class="sta-box">
	<div class="sta-in-box">
		<div class="sta-in-menu"></div>
		<div class="clear-both"></div>
		<div class="lonch-icon"><?php echo $this->html->image("launch.png", array('alt'=>'')); ?></div>
		<div class="sta-text">
			<?php if(($isLogin && strpos($SITECONFIG["memberstatistics"],'LaunchDate|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'LaunchDate|1') !== false)){ ?>
				<span class="color-blue"><?php echo __('Launch Date'); ?> : </span> <?php echo $this->Time->format($SITECONFIG["timeformate"], $StatisticLaunchDate);?> |
			<?php }?>
			<?php if(($isLogin && strpos($SITECONFIG["memberstatistics"],'AlexaRank|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'AlexaRank|1') !== false)){ ?>
				<span class="color-blue"><?php echo __('Alexa Rank'); ?> : </span> <?php echo $StatisticAlexaRank; ?> |
			<?php }?>
			<?php if(($isLogin && strpos($SITECONFIG["memberstatistics"],'TotalMember|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'TotalMember|1') !== false)){ ?>
				<span class="color-blue"><?php echo __('Total Members'); ?> : </span> <?php echo $StatisticTotalMember; ?>
			<?php } ?>
		</div>
		<div class="clear-both"></div>
	</div>
</div>
<?php } ?>
<?php // Code to show site stats ends here ?>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>