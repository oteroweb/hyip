<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php // Code to show ads stats starts here. To be shown only if enabled from adminpanel ?>
<?php if(($isLogin && (strpos($SITECONFIG["memberstatistics"],'TextAds|1') !== false || strpos($SITECONFIG["memberstatistics"],'BannerAds|1') !== false)) || (!$isLogin && (strpos($SITECONFIG["publicstatistics"],'TextAds|1') !== false || strpos($SITECONFIG["publicstatistics"],'BannerAds|1') !== false))){ ?>
<div class="sta-box">
	<div class="sta-in-box">
		<div class="sta-in-menu">
	  </div>
		<div class="clear-both"></div>
		<div class="sta-text"><?php echo __('Total Ads'); ?> - 
		<?php if($SITECONFIG['enable_textads']==1 && (($isLogin && strpos($SITECONFIG["memberstatistics"],'TextAds|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'TextAds|1') !== false))){ ?>
		<span class="color-blue"><?php echo __('Text Ads'); ?> : </span> <?php echo $StatisticTotalTextAds;?> 
		<?php }?>
		<?php if($SITECONFIG['enable_textads']==1 && $SITECONFIG['enable_bannerads']==1){ echo "|";} ?>
		<?php if($SITECONFIG['enable_bannerads']==1 && (($isLogin && strpos($SITECONFIG["memberstatistics"],'BannerAds|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'BannerAds|1') !== false))){ ?>
		<span class="color-blue"><?php echo __('Banner Ads'); ?> : </span> <?php echo $StatisticTotalBannerAds; ?>
		<?php } ?>
		</div>
		<div class="clear-both"></div>
	</div>
</div>
<?php } ?>
<?php // Code to show ads stats ends here ?>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>