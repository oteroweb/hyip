<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>

<?php // Code to show pay stats starts here. To be shown only if enabled from adminpanel ?>
<?php if(($isLogin && strpos($SITECONFIG["memberstatistics"],'Deposits|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'Deposits|1') !== false) || ($isLogin && strpos($SITECONFIG["memberstatistics"],'Payouts|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'Payouts|1') !== false) || ($isLogin && strpos($SITECONFIG["memberstatistics"],'Commissions|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'Commissions|1') !== false) || ($isLogin && strpos($SITECONFIG["memberstatistics"],'LastPayout|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'LastPayout|1') !== false)){ ?>
<div class="sta-box">
	<div class="sta-in-box">
		<div class="sta-in-menu">
	  </div>
		<div class="clear-both"></div>
		<div class="sta-text">
			<?php if(($isLogin && strpos($SITECONFIG["memberstatistics"],'Deposits|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'Deposits|1') !== false)){ ?>			
			<span class="color-blue"><?php echo __('Total Deposits'); ?> : </span> <?php echo $Currency['prefix'];?><?php echo ($StatisticTotalDiposit) ?round($StatisticTotalDiposit*$Currency['rate'],2)." ".$Currency['suffix']:0;?> |
			<?php }?>
			<?php if(($isLogin && strpos($SITECONFIG["memberstatistics"],'Payouts|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'Payouts|1') !== false)){ ?>
			<span class="color-blue"><?php echo __('Total Payouts'); ?> : </span> <?php echo $Currency['prefix'];?><?php echo round($StatisticTotalPayouts*$Currency['rate'],2)." ".$Currency['suffix'];?> |
			<?php }?>
			<?php if(($isLogin && strpos($SITECONFIG["memberstatistics"],'Commissions|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'Commissions|1') !== false)){ ?>
			<span class="color-blue"><?php echo __('Total Commissions'); ?> : </span> <?php echo $Currency['prefix'];?><?php echo round($StatisticTotalCommission*$Currency['rate'],2)." ".$Currency['suffix']; ?> |
			<span class="color-blue"><?php echo __('Total Miscellaneous Bonus'); ?> : </span> <?php echo $Currency['prefix'];?><?php echo round($StatisticTotalbonus*$Currency['rate'],2)." ".$Currency['suffix']; ?> |
			<?php }?>
			<?php if(($isLogin && strpos($SITECONFIG["memberstatistics"],'LastPayout|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'LastPayout|1') !== false)){ ?>
			<span class="color-blue"><?php echo __('Last Payout'); ?> : </span> <?php echo $Currency['prefix'];?><?php echo round($StatisticTotalLastPayouts*$Currency['rate'],2)." ".$Currency['suffix']; ?>
			<?php } ?>
		</div>
		<div class="clear-both"></div>
	</div>
</div>
<?php } ?>
<?php // Code to show pay stats ends here ?>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>