<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if(($isLogin && (strpos($SITECONFIG["memberstatistics"],'OnlineMembers|1') !== false || strpos($SITECONFIG["memberstatistics"],'OnlineGuests|1') !== false || strpos($SITECONFIG["memberstatistics"],'RecentlyJoined|1') !== false || strpos($SITECONFIG["memberstatistics"],'TopCommissionEarners|1') !== false)) || (!$isLogin && (strpos($SITECONFIG["publicstatistics"],'OnlineMembers|1') !== false || strpos($SITECONFIG["publicstatistics"],'OnlineGuests|1') !== false || strpos($SITECONFIG["publicstatistics"],'RecentlyJoined|1') !== false || strpos($SITECONFIG["publicstatistics"],'TopCommissionEarners|1') !== false))){ ?>
<div class="sta-box">
	<div class="sta-in-box">
		
		<?php // Code to show menu buttons starts here ?>
		<div class="sta-in-menu">
			<ul>
				<?php if(($isLogin && (strpos($SITECONFIG["memberstatistics"],'OnlineMembers|1') !== false || strpos($SITECONFIG["memberstatistics"],'OnlineGuests|1') !== false)) || (!$isLogin && (strpos($SITECONFIG["publicstatistics"],'OnlineMembers|1') !== false || strpos($SITECONFIG["publicstatistics"],'OnlineGuests|1') !== false))){ ?>
				<li>
					<?php
						echo $this->Js->link(__('Member Stats'), array('controller'=>'statistic', "action"=>"memberstats", 'plugin' => false), array(
							'update'=>'#statecontant',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'act vtip',
							'title'=>__('Member Stats')
						));
					?>
				</li>
				<?php } if(($isLogin && strpos($SITECONFIG["memberstatistics"],'RecentlyJoined|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'RecentlyJoined|1') !== false)){ ?>
				<li>
					<?php
						echo $this->Js->link(__('Recently Joined'), array('controller'=>'statistic', "action"=>"recentlyjoined", 'plugin' => false), array(
							'update'=>'#statecontant',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Recently Joined')
						));
					?>
				</li>
				<?php } if(($isLogin && strpos($SITECONFIG["memberstatistics"],'TopCommissionEarners|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'TopCommissionEarners|1') !== false)){ ?>
				<li>
					<?php
						echo $this->Js->link(__('Top Commission Earners'), array('controller'=>'statistic', "action"=>"topcommissionearners", 'plugin' => false), array(
							'update'=>'#statecontant',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Top Commission Earners')
						));
					?>
				</li>
				<?php } ?>
			</ul>
		</div>
		<?php // Code to show menu buttons ends here ?>
		
		<div class="clear-both"></div>
		
		<?php // Code to show member stats starts here. To be shown only if enabled from adminpanel ?>
		<?php if(($isLogin && (strpos($SITECONFIG["memberstatistics"],'OnlineMembers|1') !== false || strpos($SITECONFIG["memberstatistics"],'OnlineGuests|1') !== false)) || (!$isLogin && (strpos($SITECONFIG["publicstatistics"],'OnlineMembers|1') !== false || strpos($SITECONFIG["publicstatistics"],'OnlineGuests|1') !== false))){ ?>
			<div class="sta-text">
				<?php if(($isLogin && (strpos($SITECONFIG["memberstatistics"],'OnlineMembers|1') !== false || strpos($SITECONFIG["memberstatistics"],'OnlineGuests|1') !== false))){ ?>
				<span class="color-blue"><?php echo __('Online Members'); ?> : </span> <div id="OnlineMembers" style="display:inline;"><?php echo $OnlineMembers;?></div> |
				<?php }?>
				<?php if((!$isLogin && (strpos($SITECONFIG["publicstatistics"],'OnlineMembers|1') !== false || strpos($SITECONFIG["publicstatistics"],'OnlineGuests|1') !== false))){ ?>
				<span class="color-blue"><?php echo __('Online Guests'); ?> : </span><div id="OnlineGuests" style="display:inline;"> <?php echo $OnlineGuests; ?></div> 
				<?php } ?>
			</div>
		<?php } ?>
		<?php // Code to show member stats ends here ?>
		
		<div class="clear-both"></div>
	</div>
</div>
<?php } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>