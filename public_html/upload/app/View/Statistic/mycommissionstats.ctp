<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if($isLogin && (strpos($SITECONFIG["memberstatistics"],'MyCash|1') !== false || strpos($SITECONFIG["memberstatistics"],'RepurchaseBalance|1') !== false || strpos($SITECONFIG["memberstatistics"],'EarningBalance|1') !== false ||  strpos($SITECONFIG["memberstatistics"],'CommissionBalance|1') !== false || strpos($SITECONFIG["memberstatistics"],'BannerAdCredits|1') !== false || strpos($SITECONFIG["memberstatistics"],'TextAdCredits|1') !== false || strpos($SITECONFIG["memberstatistics"],'SoloAdCredits|1') !== false)){ ?>
<div class="sta-box">
	<div class="sta-in-box">
		
		<?php // Code to show menu buttons starts here ?>
		<div class="sta-in-menu">
			<ul>
				<?php if(strpos($SITECONFIG["memberstatistics"],'MyCash|1') !== false){ ?>
				<li>
					<?php
						echo $this->Js->link(__('Cash Balance'), array('controller'=>'statistic', "action"=>"mystats", 'plugin' => false), array(
							'update'=>'#statecontant',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Cash Balance')
						));
					?>
				</li>
				<?php } if(strpos($SITECONFIG["memberstatistics"],'RepurchaseBalance|1') !== false){ ?>
				<li>
					<?php
						echo $this->Js->link(__('Purchase Balance'), array('controller'=>'statistic', "action"=>"myrepruchasestats", 'plugin' => false), array(
							'update'=>'#statecontant',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Purchase Balance')
						));
					?>
				</li>
				<?php } if(strpos($SITECONFIG["memberstatistics"],'EarningBalance|1') !== false){ ?>
				<li>
					<?php
						echo $this->Js->link(__('Earning Balance'), array('controller'=>'statistic', "action"=>"myearningstats", 'plugin' => false), array(
							'update'=>'#statecontant',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Earning Balance')
						));
					?>
				</li>
				<?php } if(strpos($SITECONFIG["memberstatistics"],'CommissionBalance|1') !== false){ ?>
				<li>
					<?php
						echo $this->Js->link(__('Commission Balance'), array('controller'=>'statistic', "action"=>"mycommissionstats", 'plugin' => false), array(
							'update'=>'#statecontant',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'act vtip',
							'title'=>__('Commission Balance')
						));
					?>
				</li>
				<?php } if(strpos($SITECONFIG["memberstatistics"],'BannerAdCredits|1') !== false || strpos($SITECONFIG["memberstatistics"],'TextAdCredits|1') !== false || strpos($SITECONFIG["memberstatistics"],'SoloAdCredits|1') !== false){ ?>
				<li>
					<?php
						echo $this->Js->link(__('Ad Credits'), array('controller'=>'statistic', "action"=>"myadcreditstats", 'plugin' => false), array(
							'update'=>'#statecontant',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Ad Credits')
						));
					?>
				</li>
				<?php } ?>
			</ul>
		</div>
		<?php // Code to show menu buttons ends here ?>
		
		<div class="clear-both"></div>
		
		<?php // Code to show Commission bal. starts here. To be shown only if enabled from adminpanel ?>
		<?php if(strpos($SITECONFIG["memberstatistics"],'CommissionBalance|1') !== false){ ?>
		<div class="sta-text">
			<?php if($SITECONFIG["balance_type"]==1){ ?>
				<span class="color-blue"><?php echo __('Commission Balance'); ?> : </span> <?php echo $Currency['prefix'];?><?php echo round($StatisticMemberCommissionBalance*$Currency['rate'],2)." ".$Currency['suffix'];?>
			<?php }elseif($SITECONFIG["balance_type"]==2){ echo $StatisticMemberCommissionBalance; } ?>
		</div>
		<?php } ?>
		<?php // Code to show Commission bal. ends here ?>
		
		<div class="clear-both"></div>
	</div>
</div>
<?php } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>