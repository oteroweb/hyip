<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php // Do not edit below code ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>[SEOTITLE] - [SITETITLE]</title>
	<meta content="[SEOKEYWORDS]" name="keywords">
	<meta content="[SEODESCRIPTION]" name="description">
	<meta name="robots" content="noindex,nofollow" />
	<style type="text/css">
		#displayCounter{font-size:20px;}
		#credittopframe{padding:5px;background: linear-gradient(to bottom, #41B5CE 0%, #0F839C 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);height: 40px;padding-top:10px;
		font-size:13px;font-family:Verdana;color:#FFFFFF;}
		#credittopframe a{color:#FFFFFF;}
	</style>
	<script type="text/javascript" src="[SITEURL]js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {

		$(function() {
			var cnt =[SECONDWAIT];
			var counter123 = setInterval(function() {
				if (cnt>0) 
				{
					$('#displayCounter').html(" "+cnt+" ");
					cnt--;
				}
				else 
				{
					clearInterval(counter123);
					document.getElementById("credittopframe").innerHTML="";
					var xmlhttp;    
					if (window.XMLHttpRequest)
					{// code for IE7+, Firefox, Chrome, Opera, Safari
					  xmlhttp=new XMLHttpRequest();
					}
					else
					{// code for IE6, IE5
					  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					}
					xmlhttp.onreadystatechange=function()
					{
					  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					  {
						var member=document.getElementById("credittopframe");
						if(member!==null) member.innerHTML=xmlhttp.responseText;
					  }
					}
					xmlhttp.open("GET","[SITEURL]public/creditcounter/bannerad/[USERID]/[ID]",true);
					xmlhttp.send();
					
				}
			},1000);
		});
	});
	</script>
	</head>
	<body style="margin:0px;padding:0px;">
		<div id="credittopframe" align="center">[WAITTEXT] : <span id="displayCounter">[SECONDWAIT]</span> [SECONDS].</div>
	<iframe src="[TARGATURL]" style="width:100%;border:none;overflow:auto;margin:0px;padding:0px;" id="f1"></iframe>
	<script language="javascript">
		function getDocHeight() {	
		 var D = document;
			return Math.max(
				Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
				Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
				Math.max(D.body.clientHeight, D.documentElement.clientHeight)
			);
		}
		var x=getDocHeight();
		document.getElementById("f1").height=x-60;
	</script>
	</body>
</html>
<?php // Do not edit above code ?>