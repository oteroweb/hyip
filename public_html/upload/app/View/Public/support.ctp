<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>

<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Public Support");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php if($SITECONFIG["pticket_chk"]==1){ ?>
	<div class="form-box">
		
		<?php // Public ticket form starts here ?>
		<?php echo $this->Form->create('Public_ticket',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off')); ?>
			<div class="form-row">
				<div class="form-col-1 notmobile"></div>
				<div class="form-col-2 form-text centeronmobile"><?php echo __('If you have already submitted a ticket and wish to check it,').' '.$this->html->link(__("Click here"), array('controller' => 'public', 'action' => 'supportsearch'));?></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __('First Name')?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('f_name' ,array('type'=>'text', "class"=>"login-from-box-1", 'label'=>false,'div' => false));?>
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)') ?>"></span>
				
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __('Last Name')?> : <span class="required">*</span>	</div>
				<?php echo $this->Form->input('l_name' ,array('type'=>'text', "class"=>"login-from-box-1", 'label'=>false,'div' => false));?>
				
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?>"></span>
				
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __('Email')?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('email' ,array('type'=>'text','div'=>false, "class"=>"formtextbox", 'label'=>false));?>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __('Category')?> : <span class="required">*</span>	</div>
				<div class="select-dropdown">
					<label>
					<?php echo $this->Form->input('category', array(
						'type' => 'select',
						'options' => array('General'=>__('General'), 'Technical'=>__('Technical')),
						'selected' => '',
						'class'=>'searchcomboboxwidth selectBox',
						'label' => false,
						'div' => false
					));?>
					</label>
				</div>
				
				<div class="height7"></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __('Subject')?> : <span class="required">*</span>	</div>
				<?php echo $this->Form->input('subject' ,array('type'=>'text', "class"=>"login-from-box-1", 'label'=>false,'div' => false));?>
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)') ?>"></span>
				
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __('Message')?> : <span class="required">*</span>	</div>
				<?php echo $this->Form->input('message' ,array('type'=>'textarea','div'=>false, "class"=>"formtextarea", 'label'=>false));?>
			</div>
			<?php if($PublicSupportCaptcha){ ?>
			  <div class="form-row captchrow">
					<div class="form-col-1"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
					<?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formcapthcatextbox", 'label'=>false,'div'=>false));?>
					<span><?php echo __('Code')?> :</span>
					<?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'PublicSupportCaptcha','vspace'=>2, 'align'=>'absmiddle', 'width'=>'118', 'height'=>'44')); ?> 
					<a href="javascript:void(0);" onclick="javascript:document.images.PublicSupportCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "width"=>"", "height"=>"", 'align'=>'absmiddle'));?></a>
					<div class="clear-both"></div>
			  </div>
			<?php }?>
			<div class="formbutton">
				<?php echo $this->Js->submit(__('Submit'), array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#UpdateMessage',
					'div'=>false,
					'class'=>'button',
					'controller'=>'Public',
					'action'=>'support'
				));?>
			</div>
		<?php echo $this->Form->end();?>
		<?php // Public ticket form ends here ?>
		
	</div>
	<?php }else{?>
		<?php echo __("Admin has stopped new tickets currently. Please try later.");?>
	<?php }?>
	<div class="clear-both"></div>		
</div>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>