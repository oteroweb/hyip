<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="publicpage">
<?php } ?>	
<div id="UpdateMessage"></div>
<?php echo $this->Javascript->link('allpage');?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Testimonials');?></div>
	<div class="clear-both"></div>
</div>
	<?php $this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#publicpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'public', 'action'=>'testimonials')
	));
	$currentpagenumber=$this->params['paging']['Testimonial']['page']; ?>
	<div class="main-box-eran">
		
		<?php // Testimonials table starts here ?>
		<div class="padding-left-serchtabal"></div>
		<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			<div class="divtable">
				<div class="divtbody">	
					<?php $i=1;
					foreach ($testimonialdata as $testimonial):
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
						<div class="divtr <?php echo $class;?>">
							<div class="divtd textleft vam" style="padding: 10px;">
								<?php 
								  if($testimonial['Testimonial']['photo']==NULL || $testimonial['Testimonial']['photo']=='')
									  echo $this->html->image("testimonials/dummy.jpg", array('alt'=>'','width'=>'100px', 'height'=>'100px', 'style'=>'float:left;margin:0 5px 0 0;'));
								  else
									  echo $this->html->image("testimonials/".$testimonial['Testimonial']['photo'], array('alt'=>'','width'=>'100px', 'height'=>'100px', 'style'=>'float:left;margin:0 5px 0 0;'));
								?> 
								<?php echo nl2br(stripslashes($testimonial['Testimonial']['content']));?>
								<div align="right"><b>-<?php echo ucwords($testimonial['Testimonial']['dname']);?></b></div>
								<div class="clear-both"></div>
							</div>
						</div>
					<?php $i++;endforeach; ?>
				</div>	
			</div>
			<?php if(count($testimonialdata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // Testimonials table ends here ?>

	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Testimonial']['count']>$pagerecord)
	{?>	
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Testimonial',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'public','action'=>'testimonials/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#publicpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'public',
						  'action'=>'testimonials/rpp',
						  'url'   => array('controller' => 'public', 'action' => 'testimonials/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
            </div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		
	</div>
<?php if(!$ajax){?>
</div>
<?php } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>