<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>

<div id="publicpage">
<div id="UpdateMessage"></div>

<?php // News details box starts here ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('News');?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
        <div class="divtable">
			<?php foreach ($newsdata as $news):?>
				<div class="divtr tabal-title-text">
				    <div class="divth textcenter vam"><?php echo $news['News']['title']; ?></div>
				</div>
				<div class="divtr white-color">
					  <div class="divtd textcenter vam">
						<div class="height7"></div>
						<div><b><?php echo __("News Date");?> : <?php echo $this->Time->format($SITECONFIG["timeformate"], $news['News']['news_dt']); ?></b></div>
						<div><?php echo nl2br(stripslashes($news['News']['fdesc'.$this->Session->read('Config.language')])); ?></div>
						<div class="height7"></div>
					  </div>
				</div>
			<?php endforeach; ?>
  	    </div>
	</div>
</div>
<?php // News details box ends here ?>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>