<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
	<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
	<div id="publicpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
	<?php if($SITECONFIG["pticket_chk"]==1){ // If public tickets are enabled from adminpanel ?>
		<?php if($ShowTicketInfo){ ?>
			<div id="UpdateMessage"></div>
			<div class="comisson-bg">
				<div class="text-ads-title-text"><?php echo __('Public Support');?></div>
				<div class="clear-both"></div>
			</div>
			<div class="main-box-eran">
				<div class="contentboxhead color-black"><span><?php echo __("Public Support Ticket Details");?></span></div>
					
					<?php // Public ticket details box starts here ?>
					<div class="divtable">
						<div class="divtbody">
							<div class="divtr gray-color">
								<div class="divtd textcenter vam"><?php echo __("Creator");?></div>
								<div class="divtd textcenter vam">&nbsp;<?php echo $public_ticketdata['Public_ticket']['f_name']." ".$public_ticketdata['Public_ticket']['l_name'];?></div>
							</div>
							<div class="divtr white-color">
								<div class="divtd textcenter vam"><?php echo __("Category");?></div>
								<div class="divtd textcenter vam">&nbsp;<?php echo $public_ticketdata['Public_ticket']['category'];?></div>
							</div>
							<div class="divtr gray-color">
								<div class="divtd textcenter vam"><?php echo __("Subject");?></div>
								<div class="divtd textcenter vam">&nbsp;<?php echo $public_ticketdata['Public_ticket']['subject'];?></div>
							</div>
							<div class="divtr white-color">
								<div class="divtd textcenter vam"><?php echo __("Date created");?></div>
								<div class="divtd textcenter vam">&nbsp;<?php echo $this->Time->format($SITECONFIG["timeformate"], $public_ticketdata['Public_ticket']['dt_create']); ?></div>
							</div>
							<div class="divtr gray-color">
								<div class="divtd textcenter vam"><?php echo __("Date modified");?></div>
								<div class="divtd textcenter vam">&nbsp;<?php echo $this->Time->format($SITECONFIG["timeformate"], $public_ticketdata['Public_ticket']['dt_lastupdate']); ?></div>
							</div>
							<div class="divtr white-color">
								<div class="divtd textcenter vam"><?php echo __("Status");?></div>
								<div class="divtd textcenter vam">&nbsp;
									<?php
										if($public_ticketdata['Public_ticket']['status']==1){
											echo __('Open');
											echo $this->Js->link(__("Close Ticket"), array('controller'=>'public', "action"=>"supportstatus/0"), array(
												'update'=>'#publicpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'style'=>'margin-left:15px;'
											));
										}else{
											echo __('Close');
											echo $this->Js->link(__("Open ticket"), array('controller'=>'public', "action"=>"supportstatus/1"), array(
												'update'=>'#publicpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'style'=>'margin-left:15px;'
											));
										}
									?>
								</div>
							</div>
						</div>
					</div>
					<?php // Public ticket details box ends here ?>
					
					<?php // Public ticket reply box starts here ?>
					<?php echo $this->Form->create('Public_ticket_reply',array('type' => 'post', 'onsubmit' => 'return false;')); ?>
					<div class="form-box">
						<div class="form-row">
							<div class="title-box" style="text-align: center;"><?php echo __("Enter your message");?></div>
						</div>
						<div class="form-row">
							<div class="textcenter vam" style="text-align: center;">
								<?php echo $this->Form->input('message', array('type'=>'textarea', 'id' => 'replymessage','div'=>false, 'label' => false, 'class'=>'formtextarea', 'style'=>'width:99.4%;max-width:100%;'));?>
							</div>
						</div>
						<div class="formbutton">
							<?php echo $this->html->link(__("Back"), array('controller' => 'public', 'action' => 'supportsearch'), array('class'=>'button','style'=>'float:left'));?>
							<?php echo $this->Js->submit(__('Reply'), array(
									'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'update'=>'#UpdateMessage',
									'div'=>false,
									'class'=>'button',
									'controller'=>'Public',
									'action'=>'supportreplay',
									'url'   => array('controller' => 'public', 'action' => 'supportreplay')
							  ));?>
						</div>	
						<div class="clear-both"></div>
					</div>
					<?php echo $this->Form->end();?>
					<?php // Public ticket reply box ends here ?>
					
			<div class="height10"></div>
				
				<?php // Public ticket reply history starts here ?>
				<?php if(count($public_ticket_replydata)>0){?>
				<?php $i=1;foreach ($public_ticket_replydata as $public_ticket_reply): ?>
					<div class="tabal-title-heading">
						<div class="float-left mobilefloatnone"><?php echo __("Message from");?> : <?php echo $public_ticket_reply['Public_ticket_reply']['replyer_name'];?></div>
						<div class="float-right mobilefloatnone"><?php echo $this->Time->format($SITECONFIG["timeformate"], $public_ticket_reply['Public_ticket_reply']['dt_reply']); ?></div>
						<div class="clear-both"></div>
					</div>
					<div class="tabal-content-gray textleft">
						<div><?php echo stripslashes(nl2br($public_ticket_reply['Public_ticket_reply']['message']));?></div>
					</div>
				<?php if(count($public_ticket_replydata)!=$i){?><div class="height10"></div><?php }?>
				<?php $i++;endforeach; ?>
				<div class="height10"></div>
				<div class="tabal-title-heading">
					<div class="float-left"><?php echo __("Message from");?> : <?php echo $public_ticketdata['Public_ticket']['f_name']." ".$public_ticketdata['Public_ticket']['l_name'];?></div>
					<div class="float-right"><?php echo $this->Time->format($SITECONFIG["timeformate"], $public_ticketdata['Public_ticket']['dt_create']); ?></div>
					<div class="clear-both"></div>
				</div>
				<div class="tabal-content-gray textleft">
					<div><?php echo nl2br(stripslashes($public_ticketdata['Public_ticket']['message']));?></div>
				</div>
				<?php }else{?>
					<div class="title-box"><div><?php echo __("No replies found");?></div></div>
				<?php }?>
				<?php // Public ticket reply history ends here ?>
				
			</div>
			
		<?php }else{?>
		<div id="SupportSearchForm">
			<div id="UpdateMessage"></div>
			
			<?php // Public Support Ticket Search form starts here ?>
			<div class="comisson-bg">
				<div class="text-ads-title-text"><?php echo __("Public Support Ticket Details");?></div>
				<div class="clear-both"></div>
			</div>
			<div class="main-box-eran">
				<div class="form-box">
					<?php echo $this->Form->create('Public_ticket',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off')); ?>
					<div class="form-row">
						<div class="form-col-1"><?php echo __('Email')?> : <span class="required">*</span>	</div>
						<?php echo $this->Form->input('email' ,array('type'=>'text', "class"=>"formtextbox", 'div'=>false, 'label'=>false, 'value'=>$email));?>
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __('Ticket Id')?> : <span class="required">*</span>	</div>
						<?php echo $this->Form->input('pt_id' ,array('type'=>'text','div'=>false, "class"=>"formtextbox", 'label'=>false, 'value'=>$id));?>
					</div>
					<?php if($PublicSupportSearchCaptcha){ ?>
					  <div class="form-row captchrow">
						<div class="form-col-1"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
							<?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formcapthcatextbox", 'label'=>false, 'div'=>false));?>
							<span><?php echo __('Code')?> :</span>
							<?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'PublicSupportSearchCaptcha','vspace'=>2, 'align'=>'absmiddle', 'width'=>'118', 'height'=>'44')); ?> 
							<a href="javascript:void(0);" onclick="javascript:document.images.PublicSupportSearchCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "width"=>"", "height"=>"", 'align'=>'absmiddle'));?></a>
					  </div>
					<?php }?>
					
					<div class="formbutton">
						<?php echo $this->html->link(__("Back"), array('controller' => 'public', 'action' => 'support'), array('class'=>'button','style'=>'float:left'));?>
						<?php echo $this->Js->submit(__('Submit'), array(
							  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
							  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							  'update'=>'#UpdateMessage',
							  'div'=>false,
							  'class'=>'button',
							  'controller'=>'Public',
							  'action'=>'supportsearch/check',
							  'url'   => array('controller' => 'public', 'action' => 'supportsearch/check')
						));?>
					</div>
					<?php echo $this->Form->end();?>
				</div>
				<div class="clear-both"></div>
			</div>
			<?php // Public Support Ticket Search form ends here ?>
			
		<?php }?>
		
	<?php }else{ // If public tickets are disabled from adminpanel ?>
		<div class="login-details">
			<div class="login-details-border">
				<div class="title-box"><?php echo __("Public Support Ticket Details");?></div>
					<div class="login-details-innar-box">
					<?php echo __("Admin has stopped new tickets currently. Please try later.");?>
				</div>
			</div>
		</div>
	<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>
<?php if(!$ajax){?>
	</div>
<?php }?>