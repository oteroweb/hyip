<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="publicpage">
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('F.A.Q.');?></div>
	<div class="clear-both"></div>
</div>
<?php echo $this->Javascript->link('allpage');?>
	<div class="main-box-eran">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#publicpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'public', 'action'=>'faqs')
	));
	$currentpagenumber=$this->params['paging']['Faq']['page'];?>
		
		<?php // FAQ table starts here ?>
		<div class="padding-left-serchtabal"></div>
		<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
		<div class="textleft">
			<?php $category='';$i=1; if(count($faqdata)){
			foreach ($faqdata as $faq):
				if($category!=$faq['Faq_category']['category'])
				{	
					if($category!='')echo '<div class="height10"></div>';
					$category=$faq['Faq_category']['category'];?>
					<div class="tabal-title-heading"><?php echo $faq['Faq_category']['category']; ?></div><?php
				}
				if($i%2==0){$class1='tabal-content-white';$class2='graybox';}else{$class1='tabal-content-gray';$class2='whitebox';}?>
				
					<div class="<?php echo $class1;?>"><a href="javascript:void(0)" title="<?php echo $i;?>" class="helpicon"><?php echo $faq['Faq']['question'.$this->Session->read('Config.language')]; ?></a></div>
					<div class="<?php echo $class1;?> <?php echo $class2;?> informationbox helpinformation<?php echo $i;?>"><?php echo nl2br(stripslashes($faq['Faq']['answer'.$this->Session->read('Config.language')]));?></div>
			<?php $i++;endforeach; ?>
			<?php }else { echo __("No records available");} ?>
			<?php // FAQ table ends here ?>
		</div>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Faq']['count']>$pagerecord)
	{?>	
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Faq',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'public','action'=>'faqs/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#publicpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'public',
						  'action'=>'faqs/rpp',
						  'url'   => array('controller' => 'public', 'action' => 'faqs/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php }?>
				</ul>
			<div class="clear-both"></div>
		</div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		
	</div>
</div>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>