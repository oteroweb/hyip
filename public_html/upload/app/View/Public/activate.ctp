<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php 
if($ActivationStatus=='Success') // If member successfully verifies the account
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro"><?php echo __('Account verified successfully'); ?>.<br /> <a href="<?php echo $SITEURL;?>login"><?php echo __('Please login to get started'); ?>.</a></font>
		</div>
	  </div>
	<?php
}
elseif($ActivationStatus=='Activated') // If the account is already verified by the member
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro"><?php echo __('Your account is already verified'); ?>.<br /> <a href="<?php echo $SITEURL;?>login"><?php echo __('Please login to get started'); ?>.</a></font>
		</div>
	  </div>
	<?php
}
elseif($ActivationStatus=='InvalidCode') // If the verification code from the activation link is wrong
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro"><?php echo __('Invalid verification code'); ?></font>
		</div>
	  </div>
	<?php
}
elseif($ActivationStatus=='Invalid') // If the arguments passed with the activation link are invalid
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro"><?php echo __('Invalid arguments passed'); ?></font>
		</div>
	  </div>
	<?php
}
?>