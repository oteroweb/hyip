<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if($SITECONFIG["emailconfirmation"]==1){ ?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Resend Activation Link");?></div>
	<div class="clear-both"></div>
</div>

<?php // Resend Activation form starts here ?>
<div class="main-box-eran">
	<div class="form-box">
	<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off')); ?>
	<div class="form-row">
		<div class="form-col-1"><?php echo __("Email");?> : <span class="required">*</span></div>
		<?php echo $this->Form->input('memberemail' ,array('type'=>'text','div'=>false, "class"=>"formtextbox", 'label'=>false));?>
	</div>
	<div class="formbutton"> 
		<?php echo $this->Js->submit(__('Submit'), array(
			'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'update'=>'#UpdateMessage',
			'div'=>false,
			'class'=>'button',
			'controller'=>'Public',
			'action'=>'resendactivation'
		));?>
	</div>
	<?php echo $this->Form->end();?>
	<div class="clear-both"></div>
</div>
<?php // Resend Activation form ends here ?>
	
<?php }else{ echo __("This page is disabled by administrator"); }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>