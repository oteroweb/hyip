<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Terms & Conditions');?></div>
	<div class="clear-both"></div>
</div>

<?php // Content is fetched from Website Pages ?>
<div class="main-box-eran">
	<?php echo stripslashes($web_page_content);?>
</div>