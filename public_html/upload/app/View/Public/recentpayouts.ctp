<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if($SITECONFIG["recentpay"]==1){ ?>
<?php if(!$ajax){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="publicpage">
<?php } ?>	
<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#publicpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'public', 'action'=>'recentpayouts')
	));
	$currentpagenumber=$this->params['paging']['Withdrawal_history']['page'];
	?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Recent Payouts');?></div>
	<div class="clear-both"></div>
</div>	
	<div class="main-box-eran">
		
		<?php // Recent Payout table starts here ?>
		<div class="padding-left-serchtabal"></div>
		<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			<div class="divtable">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam"><?php echo __("Date");?></div>
						<div class="divth textcenter vam"><?php echo __("To Member");?></div>
						<div class="divth textcenter vam"><?php echo __("Amount");?></div>
						<div class="divth textcenter vam"><?php echo __("Payment Processor");?></div>
					</div>
				</div>
				<div class="divtbody">
					<?php $i=1;
						foreach($withdrawal_historydata as $withdrawal_history):
						if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
							<div class="divtr <?php echo $class;?>">
								<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $withdrawal_history['Withdrawal_history']['dt']); ?></div>
								<div class="divtd textcenter vam"><?php echo $withdrawal_history['Member']['user_name'];?></div>
								<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($withdrawal_history['Withdrawal_history']['amount']*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
								<div class="divtd textcenter vam"><?php echo $this->html->image('common/'.$withdrawal_history['Withdrawal_history']['processor'].'.jpg', array('alt'=>''));?></div>
							</div>
						<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($withdrawal_historydata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // Recent Payout table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Withdrawal_history']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
	<div class="pag-float-left">
		<div class="ul-bg">
			<ul class="nice_paging">
			<?php 
			foreach($resultperpage as $rpp)
			{
				?>
				<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
					<?php 
					echo $this->Form->create('Withdrawal_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'public','action'=>'recentpayouts/rpp')));
					echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
				
					echo $this->Js->submit($rpp, array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#publicpage',
					  'class'=>'resultperpagebutton',
					  'div'=>false,
					  'controller'=>'public',
					  'action'=>'recentpayouts/rpp',
					  'url'   => array('controller' => 'public', 'action' => 'recentpayouts/rpp')
					));
					echo $this->Form->end();
					?>
				</li>
				<?php 
			}?>
			</ul>
		<div class="clear-both"></div>
		</div>
	</div>
	<?php }?>
	<div class="floatright ul-bg">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
		</ul>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>
	
	</div>
<?php if(!$ajax){?>	
</div>
<?php } ?>
<?php }else{echo __('This page is disabled by administrator');} ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>