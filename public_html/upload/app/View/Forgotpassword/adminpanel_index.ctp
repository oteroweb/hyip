<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if($locked=='yes'){?>
	<div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
    	<div style="margin:40px;text-align:center;font-size:20px;">
    		<font face="Myriad Pro">You Are Blocked For Two Hours For Giving Wrong Answer Thrice.</font>
        </div>
     </div>
<?php }else{?>
	<?php if($authentication=="yes"){ ?>
            <div style="margin:10px 0;" class="login_username_txt"><strong>Your Password is Sent To Your Email Address. Please Check Your Email Account</strong></div>
	<?php }else{ ?>
	<div class="loginboxmain">
		<div class="loginbox">
			<div class="welcometextmain">
			  <div class="welcometext"><span>Welcome</span> Admin</div>
			  <div>Current Version : Proxscripts <?php echo CURRENTVERSION;?></div>
			</div>
			  
			<?php echo $this->Form->create("forgotform", array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off')); ?>
			<div class="welcometextmain no-margin">
				<div id="CounterAttempt"><?php echo "Attempt". " = ".$this->Session->read('counter')."/3"; ?></div>
			</div>
			<div id="UpdateMessage"></div>
			
			<div class="userform">
			  <div class="textbox1">
				<span class="glyphicon glyphicon-question-sign"></span>
				<div class="question"><?php echo $securityquestion;?></div>
			  </div>
			  <div class="textbox1">
				<span class="glyphicon glyphicon-font"></span>
				<?php echo $this->Form->input('ans',array("type"=>"password", "class"=>"keyboardInput", 'label'=>'', 'placeholder'=>'Answer', 'div'=>false));?>
			  </div>
			</div>
			
			<div class="loginbutton">
				<?php echo $this->Js->submit('Submit', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#UpdateMessage',
					'class'=>'',
					'controller'=>'forgotpassword',
					'action'=>'index'
				));?>
			</div>
			<?php echo $this->Form->end();?>
			
			<div class="forgotpass"><?php echo $this->html->link('Back', array('controller' => 'login', 'action' => 'index'));?></div>
			<div class="whiteborder"></div>
			<div class="copyrighttext">Copyright &copy; 2014 <?php echo "<a href='http://www.".$SITECONFIG["poweredby_link"]."/index.php?ref=".$SITECONFIG["adminid"]."' target='_blank'>".$SITECONFIG["poweredby_link"]."</a>";?> All Rights Reserved</div>
		</div>
	</div>
	<?php } ?>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>