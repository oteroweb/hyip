<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php 
if($textarea=='advanced')
{
	echo $this->Javascript->link('tiny_mce/tiny_mce.js');
	echo $this->Javascript->link('tinymce');
	$textareaclass='advancededitor';
	$submitbuttonclass='tinyMCEtriggerSavetk';
}
elseif($textarea=='simple')
{
	$textareaclass='';
	$submitbuttonclass='';
}
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Message Ads</div>
<div id="advertisementpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Message_Ads" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<div class="backgroundwhite">
		
	<?php echo $this->Form->create('Messagead',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'advertisement','action'=>'messageadsaddaction')));?>
	<?php if(isset($messageaddata['Messagead']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$messageaddata['Messagead']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
	<div class="frommain">
		
		<div class="fromnewtext">Member Type :</div>
		<div class="fromborderdropedown3">
			<span class="fromboxbg"><?php echo $messageaddata['Messagead']["membertype"];?></span>
		</div>
		
	<?php if($messageaddata['Messagead']["id"]==5){?>
		<div class="fromnewtext">Memberships : </div>
		<div class="fromborderdropedown3 checkboxlist">
			<?php
			$selectmembership=explode(',',$messageaddata['Messagead']['membervalue']);
			echo $this->Form->input('membership', array(
				  'type'=>'select',
				  'multiple'=>'checkbox',
				  'label' => '',
				  'class'=>'',
				  'div'=>true,
				  'selected' => $selectmembership,
				  'options'=> $Membership
			));
			?>
		</div>
		
	<?php }else if($messageaddata['Messagead']["id"]==6){?>
		<div class="fromnewtext">Regular Earning Plans : </div>
		<div class="fromborderdropedown3 checkboxlist">
			<?php
			$selectmembership=explode(',',$messageaddata['Messagead']['membervalue']);
			echo $this->Form->input('regularearningplan', array(
				'type'=>'select',
				'multiple'=>'checkbox',
				'label' => '',
				'class'=>'',
				'div'=>true,
				'selected' => $selectmembership,
				'options'=> $Dailyearningplan
			));
			?>
		</div>
		
	<?php }elseif($messageaddata['Messagead']["id"]==7){?>
		<div class="fromnewtext">Dynamic Earning Plans : </div>
		<div class="fromborderdropedown3 checkboxlist">
			<?php
			$selectmembership=explode(',',$messageaddata['Messagead']['membervalue']);
			echo $this->Form->input('dynamicearningplan', array(
				'type'=>'select',
				'multiple'=>'checkbox',
				'label' => '',
				'class'=>'',
				'div'=>true,
				'selected' => $selectmembership,
				'options'=> $Dynamicearningplan
			));
			?>
		</div>
		
	<?php }elseif($messageaddata['Messagead']["id"]==8){?>
		<div class="fromnewtext">Revenue Share Plans : </div>
		<div class="fromborderdropedown3 checkboxlist">
			<?php
			$selectmembership=explode(',',$messageaddata['Messagead']['membervalue']);
			echo $this->Form->input('revenuesharingplan', array(
				'type'=>'select',
				'multiple'=>'checkbox',
				'label' => '',
				'class'=>'',
				'div'=>true,
				'selected' => $selectmembership,
				'options'=> $Revenueplan
			));
			?>
		</div>
		
	<?php }?>
		
		<div class="fromnewtext">Description :</div>
		
		<div class="fromborderdropedown4">
			<span class="fromboxbg"><?php echo $messageaddata['Messagead']["description"];?></span>
		</div>
		
		<div class="formbutton">
			<?php 
			if($textarea=='simple'){$showtextarea='advanced';}
			if($textarea=='advanced'){$showtextarea='simple';}
			$link='messageadsadd/'.$showtextarea.'/'.$messageaddata['Messagead']["id"];
			echo $this->Js->link("Switch To ".ucfirst($showtextarea)." Mode", array('controller'=>'advertisement', "action"=>$link), array(
				'update'=>'#advertisementpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'btngray'
			));?>
			<?php if($textarea=='simple'){ ?>
				<input type="button" value="Copy Same Content To All Language" onclick="if(confirm('Copy English Content Into All Languages')){$('.alllanguagesame').val($('#MessageadMessageeng').val()); }" class="btnorange floatright" style="margin-right: 5px;" />
				<?php }else{ ?>
				<input type="button" value="Copy Same Content To All Language" onclick="if(confirm('Copy English Content Into All Languages')){for (i=0; i < tinyMCE.editors.length; i++){ tinyMCE.editors[i].setContent(tinyMCE.editors[0].getContent()); }}" class="btnorange floatright" style="margin-right: 5px;" />
			<?php } ?>
		</div>
		<div class="clearboth"></div>
		<div class="newgreytabs">

			<ul class="nav nav-tabs" role="tablist">
			     <?php foreach($languagedata as $code=>$displayname){ $activeclass='';if($code=='eng'){ $activeclass='active'; } ?>
				<li role="presentation" class="<?php echo $activeclass; ?>">
					<a onclick="$('.allactive').hide();$('.<?php echo $code; ?>').show();" class="active" role="tab" data-toggle="tab"><?php echo $displayname ?></a>
				</li>
				<?php } ?>
			</ul>
	      
		      <div class="tab-content">
			  <?php foreach($languagedata as $code=>$displayname){ $activeclass='';if($code!='eng'){ $activeclass='style="display: none" '; } ?>
			  <div <?php echo $activeclass; ?> role="tabpanel" class="tab-pane active allactive <?php echo $code; ?>">
			      <div class="masstextnew">Message (<?php echo $displayname;?>) :<span class="red-color">*</span></div>
			      <div class="greyboxnewtabsin"><?php echo $this->Form->input('message'.$code, array('type'=>'textarea', 'label' => false, 'div' => false, 'class'=>'from-textarea alllanguagesame '.$textareaclass,'value'=>stripslashes($messageaddata['Messagead']['message'.$code]), 'style'=>'height: 400px;'));?></div>
			  </div>
			<?php }?>
		      </div>
		</div>
		
	<?php if(!isset($SubadminAccessArray) || in_array('advertisement',$SubadminAccessArray) || in_array('advertisement/adminpanel_messageads',$SubadminAccessArray)){ ?>
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'btnorange '.$submitbuttonclass,
				'controller'=>'advertisement',
				'div'=>false,
				'action'=>'messageadsaddaction',
				'url'   => array('controller' => 'advertisement', 'action' => 'messageadsaddaction')
			));?>
			<?php echo $this->Js->link("Back", array('controller'=>'advertisement', "action"=>"messageads"), array(
				  'update'=>'#advertisementpage',
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'escape'=>false,
				  'div'=>false,
				  'class'=>'btngray'
			));?>
		</div>
	<?php } ?>
	</div>
	<?php echo $this->Form->end();?>
	
</div>
<?php if(!$ajax){?>
</div><!--#advertisementpage over-->
<?php }?>	
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>