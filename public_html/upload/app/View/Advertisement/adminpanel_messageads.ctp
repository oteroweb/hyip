<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Message Ads</div>
<div id="advertisementpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Message_Ads" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
    
	    <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#advertisementpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'advertisement', 'action'=>'messageads')
        ));
        $currentpagenumber=$this->params['paging']['Messagead']['page'];
        ?>

<div id="Xgride-bg">
	<div class="height10"></div>
    <div class="padding10 backgroundwhite">
		<div class="greenbottomborder">
		<div class="height10"></div>
        <div class="records records-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
        <div class="clear-both"></div>
		
        <?php echo $this->Form->create('Messagead',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'advertisement','action'=>'messageadstatus')));?>
		<div class="tablegrid">
				<div class="tablegridheader">
				    <div>
                        <?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('Id', array('controller'=>'advertisement', "action"=>"messageads/0/0/id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#advertisementpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Id'
                        ));?>
                    </div>
                    <div><?php echo "Member Type";?></div>
                    <div>
                        <?php 
                        echo $this->Js->link('Description', array('controller'=>'advertisement', "action"=>"messageads/0/0/membertype/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#advertisementpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Description'
                        ));?>
                    </div>
                    <div><?php echo "Action";?></div>
                </div>
                <?php foreach ($messageads as $messagead): ?>
                    <div class="tablegridrow">
						<div><?php echo $messagead['Messagead']['id']; ?></div>
                        <div><?php echo $messagead['Messagead']['membertype']; ?></div>
                        <div><?php echo $messagead['Messagead']['description']; ?></div>
                        <div>
				        <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
								<?php
                                    if(!isset($SubadminAccessArray) || in_array('advertisement',$SubadminAccessArray) || in_array('advertisement/adminpanel_messageadupdate',$SubadminAccessArray)){?>
									<li>
								    <?php
										echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Message Ad'))." Edit", array('controller'=>'advertisement', "action"=>"messageadsadd/simple/".$messagead['Messagead']['id']), array(
											'update'=>'#advertisementpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'class'=>''
										));?>
								    </li>
									<?php }?>
							
								<?php 
								if(!isset($SubadminAccessArray) || in_array('advertisement',$SubadminAccessArray) || in_array('advertisement/adminpanel_messageadstatus',$SubadminAccessArray)){?>
								<li>
								<?php
									if($messagead['Messagead']['status']==0){
										$statusaction='1';
										$statusicon='red-icon.png';
										$statustext='Activate';
									}else{
										$statusaction='0';
										$statusicon='blue-icon.png';
										$statustext='Inactivate';}
									echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'advertisement', "action"=>"messageadstatus/".$statusaction."/".$messagead['Messagead']['id']."/".$currentpagenumber), array(
										'update'=>'#advertisementpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false,
										'class'=>''
									));?>
									</li>
								<?php }?>
								
								
						  </ul>
						</div>
					  </div>
                        </div>
                    </div>
                <?php endforeach; ?>
        </div>
		<?php if(count($messageads)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php echo $this->Form->end();
        if($this->params['paging']['Messagead']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Messagead',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'advertisement','action'=>'messageads/0/rpp')));?>
			<div class="resultperpage">
				<label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
				</label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#advertisementpage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'advertisement',
                  'action'=>'messageads/0/rpp',
                  'url'   => array('controller' => 'advertisement', 'action' => 'messageads/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
		</div>
    </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#advertisementpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>