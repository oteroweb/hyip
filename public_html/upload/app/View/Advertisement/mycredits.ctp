<?php /*****************************************************************
  * xRevenuePro Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.xrevenuepro.com.
  * Created Date: 16-04-2014
  * Last Modified: 18-07-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($themesubmenuaccess){?>
<?php if($displaystatement=='yes'){ ?>
<?php if(!$ajax){ ?>
	<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
	<div class="comisson-bg">
		<div class="text-ads-title-text"><?php echo __('Credit Statement');?></div>
		<div class="clear-both"></div>
	</div>

	<?php // Credit Statement table starts here ?>
	<div class="main-box-eran">
		<div class="divtable textcenter">
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam"><?php echo __("Credit Type");?></div>
					<div class="divth textcenter vam"><?php echo __("Credits");?></div>
					<div class="divth textcenter vam"><?php echo __("Action");?></div>
				</div>
			</div>
			<div class="divtbody">
				<?php if($SITECONFIG['enable_bannerads']==1){?>
				<div class="divtr gray-color">
					<div class="divtd textcenter vam"><?php if(in_array('Banner Ads', $themesubmenu)){ echo $this->html->link(__('Banner Ad Credits'), array('controller' => 'bannerad', 'action' => 'index')); }else{ echo __('Banner Ad Credits');} ?></div>
					<div class="divtd textcenter vam"><?php echo $banner_credit;?></div>
					<div class="divtd textcenter vam"><?php if(in_array('Banner Ads', $themesubmenu)){ echo $this->html->link(__("Assign"), array('controller' => 'bannerad', 'action' => 'index')); } ?></div>
				</div>
				<?php }?>
				<?php if($SITECONFIG['enable_textads']==1){?>
				<div class="divtr white-color">
					<div class="divtd textcenter vam"><?php if(in_array('Text Ads', $themesubmenu)){ echo $this->html->link(__('Text Ad Credits'), array('controller' => 'textad', 'action' => 'index')); }else{ echo __('Text Ad Credits'); }?></div>
					<div class="divtd textcenter vam"><?php echo $txtadd_credit;?></div>
					<div class="divtd textcenter vam"><?php if(in_array('Text Ads', $themesubmenu)){ echo $this->html->link(__("Assign"), array('controller' => 'textad', 'action' => 'index')); }?></div>
				</div>
				<?php }?>
				<?php if($SITECONFIG['enable_soloads']==1){?>
				<div class="divtr gray-color">
					<div class="divtd textcenter vam"><?php if(in_array('Solo Ads', $themesubmenu)){ echo $this->html->link(__('Solo Ad Credits'), array('controller' => 'soload', 'action' => 'index')); }else{ echo __('Solo Ad Credits');}?></div>
					<div class="divtd textcenter vam"><?php echo $soloads_credit;?></div>
					<div class="divtd textcenter vam"><?php if(in_array('Solo Ads', $themesubmenu)){ echo $this->html->link(__("Assign"), array('controller' => 'soload', 'action' => 'index')); } ?></div>
				</div>
				<?php }?>
				<?php if(strpos($SITECONFIG['trafficsetting'],'enablewebsites|1') !== false){?>
				<div class="divtr gray-color">
					<div class="divtd textcenter vam"><?php if(in_array('My Websites', $themesubmenu)){ echo $this->html->link(__('Websites Credits'), array('controller' => 'traffic', 'action' => 'website')); }else{ echo __('Websites Credits');}?></div>
					<div class="divtd textcenter vam"><?php echo $webcredits;?></div>
					<div class="divtd textcenter vam"><?php if(in_array('My Websites', $themesubmenu)){ echo $this->html->link(__("Assign"), array('controller' => 'traffic', 'action' => 'website')); } ?></div>
				</div>
				<?php }?>
			</div>
		</div>
	</div>
	<?php // Credit Statement table ends here ?>
	
	<?php } ?>
	<div id="advertisementpage">
		<div class="comisson-bg mobilecss">
			<div class="commison-menu">
				<ul>
					<?php if($SITECONFIG['enable_bannerads']==1){?>
						<li>
							<?php
							$class1=''; if($adtype==1) $class1='act';
							echo $this->Js->link(__('Banner Ad Credits'), array('controller'=>'advertisement', "action"=>"mycredits/1"), array(
								'update'=>'#advertisementpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip '.$class1,
								'title'=>__('Banner Ad Credits')
							));
							?>
						</li>
					<?php }?>
					<?php if($SITECONFIG['enable_textads']==1){?>
						<li>
							<?php
							$class2=''; if($adtype==2) $class2='act';
							echo $this->Js->link(__('Text Ad Credits'), array('controller'=>'advertisement', "action"=>"mycredits/2"), array(
								'update'=>'#advertisementpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip '.$class2,
								'title'=>__('Text Ad Credits')
							));
							?>
						</li>
					<?php }?>
					<?php if($SITECONFIG['enable_soloads']==1){?>
						<li>
							<?php
							$class3=''; if($adtype==3) $class3='act';
							echo $this->Js->link(__('Solo Ad Credits'), array('controller'=>'advertisement', "action"=>"mycredits/3"), array(
								'update'=>'#advertisementpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip '.$class3,
								'title'=>__('Solo Ad Credits')
							));
							?>
						</li>
					<?php }?>
					<?php if(strpos($SITECONFIG['trafficsetting'],'enablewebsites|1') !== false){?>
						<li>
							<?php
							$class4=''; if($adtype==4) $class4='act';
							echo $this->Js->link(__('Websites Credits'), array('controller'=>'advertisement', "action"=>"mycredits/4"), array(
								'update'=>'#advertisementpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip '.$class4,
								'title'=>__('Websites Credits')
							));
							?>
						</li>
					<?php }?>
				</ul>
			</div>
			<div class="clear-both"></div>
		</div>
	
		<div class="main-box-eran">
			<?php // Search box code starts here ?>
			<div class="floatleft searchlabel"><?php echo __("Advanced Search");?></div>
			<div class="floatright"><a href="javascript:void(0)" onclick="togglebox('.searchlabel', this,'<?php echo __("[+] Show Search Box");?>','<?php echo __("[-] Hide Search Box");?>');"><?php echo __("[-] Hide Search Box");?></a></div>
			<div class="clear-both"></div>
			<div class="advsearchbox searchlabel">
				<?php echo $this->Form->create('Creditstatement',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'advertisement','action'=>'mycredits/'.$adtype)));?>
					<div class="inlineblock vat">
						<div>
							<span class="searchboxcol1"><?php echo __("Search By");?>  :&nbsp;</span>
							
							<div class="select-dropdown smallsearch">
								<label>
									<?php echo $this->Form->input('searchby', array(
											'type' => 'select',
											'options' => array('all'=>__('All'), 'debit'=>__('Debit'), 'credit'=>__('Credit'), 'balance'=>__('Balance'), 'description'=>__('Description')),
											'selected' => $searchby,
											'class'=>'',
											'label' => false,
											'div' => false,
											'style' => ''
									  ));?>
								</label>
							</div>
							  
							  
						</div>
						<div>
							<span class="searchboxcol1"><?php echo __("Search For");?> :&nbsp;</span>
							<?php echo $this->Form->input('searchfor', array('type'=>'text', 'id'=>'searchfor', 'value'=>$searchfor, 'label' => false, 'class'=>'finece-from-box searchfortxt','div' => false));?>
						</div>
					</div>
					<div  class="inlineblock vat">
						<div class="margintb5">
							<span class="searchboxcol2"><?php echo __("From Date");?> :&nbsp;</span>
							<?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker searchdatetxt'));?>
						</div>
						<div>
							<span class="searchboxcol2"><?php echo __("To Date");?> :&nbsp;</span>
							<?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker searchdatetxt'));?>
						</div>
					</div>
					<div class="textcenter margint5">
						<?php echo $this->Js->submit(__('Update Results'), array(
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'update'=>'#advertisementpage',
							'class'=>'button floatnone',
							'div'=>false,
							'controller'=>'advertisement',
							'action'=>'mycredits/'.$adtype,
							'url'   => array('controller' => 'advertisement', 'action' => 'mycredits/'.$adtype)
						  ));?>
					</div>
				<?php echo $this->Form->end();?>
			</div>
			<?php // Search box code ends here ?>
			
			<?php $this->Paginator->options(array(
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update' => '#advertisementpage',
				'evalScripts' => true,
				'url'=> array('controller'=>'advertisement', 'action'=>'mycredits/'.$adtype)
			));
			$currentpagenumber=$this->params['paging']['Creditstatement']['page'];
			
			// Credit statement table starts here ?>
			<div class="padding-left-serchtabal"></div>
			<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
			<div class="clear-both"></div>
			<div class="height5"></div>
			<div class="divtable textcenter">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam">
							<?php
							if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
							echo $this->Js->link(__('Date'), array('controller'=>'advertisement', "action"=>"mycredits/".$adtype."/0/effect_date/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#advertisementpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Date')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__("Description"), array('controller'=>'advertisement', "action"=>"mycredits/".$adtype."/0/description/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#advertisementpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Description')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Debit'), array('controller'=>'advertisement', "action"=>"mycredits/".$adtype."/0/debit/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#advertisementpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Debit')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Credit'), array('controller'=>'advertisement', "action"=>"mycredits/".$adtype."/0/credit/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#advertisementpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Credit')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Balance'), array('controller'=>'advertisement', "action"=>"mycredits/".$adtype."/0/balance/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#advertisementpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Balance')
							));?>
						</div>
					</div>
				</div>
				<div class="divtbody">
					<?php
					$i = 0;
					foreach ($creditrecords as $creditrecord):
						if($i%2==0){$class='gray-color';}else{$class='white-color';}
						?>
						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $creditrecord['Creditstatement']['effect_date']); ?></div>
							<div class="divtd textcenter vam"><?php echo $creditrecord['Creditstatement']['description']; ?></div>
							<div class="divtd textcenter vam"><?php echo $creditrecord['Creditstatement']['debit']; ?></div>
							<div class="divtd textcenter vam"><?php echo $creditrecord['Creditstatement']['credit']; ?></div>
							<div class="divtd textcenter vam"><?php echo $creditrecord['Creditstatement']['balance']; ?></div>
						</div>
					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($creditrecords)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // Credit statement table ends here ?>
					
			<?php // Paging code starts here ?>
			<?php $pagerecord=$this->Session->read('pagerecord');
			if($this->params['paging']['Creditstatement']['count']>$pagerecord)
			{?>
				<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
				<div class="pag-float-left">
					<div class="ul-bg">
						<ul class="nice_paging">
						<?php 
						foreach($resultperpage as $rpp)
						{
							?>
							<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
								<?php 
								echo $this->Form->create('Creditstatement',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'advertisement','action'=>'mycredits/'.$adtype.'/rpp')));
								echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
								
								echo $this->Js->submit($rpp, array(
								  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								  'update'=>'#advertisementpage',
								  'class'=>'resultperpagebutton',
								  'div'=>false,
								  'controller'=>'advertisement',
								  'action'=>'mycredits/'.$adtype.'/rpp',
								  'url'   => array('controller' => 'advertisement', 'action' => 'mycredits/'.$adtype.'/rpp')
								));
								echo $this->Form->end();
								?>
							</li>
							<?php 
						}?>
						</ul>
						<div class="clear-both"></div>
					</div>
				</div>
			<?php }?>
			<div class="floatright ul-bg">
				<ul class="nice_paging">
					<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
					<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
					<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
					<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
					<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
				</ul>
			</div>
			<div class="clear-both"></div>
			<?php // Paging code ends here ?>
		</div>
	</div>
<?php }else{echo __('Access denied');}?>

<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>