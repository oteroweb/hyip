<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Paid To Click</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("PTC Plans", array('controller'=>'ptc', "action"=>"plan"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC Ads", array('controller'=>'ptc', "action"=>"member"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC Themes", array('controller'=>'ptc', "action"=>"design"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">
<div id="ptcpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Paid_To_Click_-_PTC#PTC_Plans" target="_blank">Help</a></div>
<div id="UpdateMessage" class="UpdateMessage"></div>
<?php echo $this->Form->create('Ptcplan',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'ptc','action'=>'planaction')));?>
	<?php if(isset($ptcplandata['Ptcplan']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$ptcplandata['Ptcplan']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
	<div class="frommain">
	
		<div class="fromnewtext">Advance Settings : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('status', array(
						'type' => 'select',
						'options' => array('1'=>'Active', '0'=>'Inactive'),
						'selected' => $ptcplandata['Ptcplan']["status"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		
		
		<div class="fromnewtext">Hide Plan ? : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
				  echo $this->Form->input('ishide', array(
					'type' => 'select',
					'options' => array('0'=>'No', '1'=>'Yes'),
					'selected' => $ptcplandata['Ptcplan']["ishide"],
					'class'=>'',
					'label' => false,
					'div' => false,
					'style' => ''
				  ));
				?>
			  </label>
		  </div>
		</div>
		
		<div class="fromnewtext">Allow Coupons on Purchase :  </div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>	
				<?php 
				  echo $this->Form->input('iscoupon', array(
					  'type' => 'select',
					  'options' => array('1'=>'Yes', '0'=>'No'),
					  'selected' => $ptcplandata['Ptcplan']["iscoupon"],
					  'class'=>'',
					  'label' => false,
					  'div' => false,
					  'style' => ''
				  ));
				?>
				</label>
			</div>
		</div>
		
		<div class="fromnewtext">Banner : </div>
		<div class="fromborderdropedown3" style="height: 40px">
		  <div class="select-main2 borderselact">
			<div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('banner', array(
						'type' => 'select',
						'options' => array('0'=>'Disable', '1'=>'Enable'),
						'selected' => $ptcplandata['Ptcplan']["banner"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => '',
						'onchange' => 'if(this.selectedIndex!=0){$(".banersize").show(500);}else{$(".banersize").hide(500);}'
					));
				?>
			  </label>
			</div>
		  </div>
			<?php
			if($ptcplandata['Ptcplan']["banner"]==0)
				$display='display:none;';
			else
				$display='display:inline-block;';
			?>
		  <div class="select-main2 banersize" style="<?php echo $display; ?>border-left: solid 1px #dddddd; margin-left: -3px;">
			<div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('banner_size', array(
						'type' => 'select',
						'options' => array('125x125'=>'125X125', '468x60'=>'468X60', '728x90'=>'728X90'),
						'selected' => $ptcplandata['Ptcplan']["banner_size"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
			</div>
		  </div>
		</div>
		
		
		<div class="fromnewtext">Plan Name :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('plan_name', array('type'=>'text', 'value'=>$ptcplandata['Ptcplan']["plan_name"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Seconds to Wait For Successful Click Count : <span class="red-color">*</span></div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('countersecond', array('type'=>'text', 'id'=>'startdate', 'value'=>$ptcplandata['Ptcplan']["countersecond"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		
		<div class="fromnewtext">Number of Clicks :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('total_click', array('type'=>'text', 'value'=>$ptcplandata['Ptcplan']["total_click"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">All Countries Price :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('all_country_price', array('type'=>'text', 'value'=>$ptcplandata['Ptcplan']["all_country_price"], 'label' => false, 'div'=>false, 'onblur'=>'var amount1=(this.value/$("#PtcplanTotalClick").val());$(".allrate").html(amount1);', 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">GEO(Selected Countries) Price :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('geo_price', array('type'=>'text', 'id'=>'startdate', 'value'=>$ptcplandata['Ptcplan']["geo_price"],'onblur'=>'var amount1=(this.value/$("#PtcplanTotalClick").val());$(".georate").html(amount1);', 'label' => false, 'div'=>false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Calculated PTC Rate For All Countries ($) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<span class="allrate"><?php echo '$'.$ptcplandata['Ptcplan']["all_country_price"]/$ptcplandata['Ptcplan']["total_click"]; ?></span>
		</div>
		
		<div class="fromnewtext">Calculated PTC Rate For GEO ($) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<span class="georate"><?php echo '$'.$ptcplandata['Ptcplan']["geo_price"]/$ptcplandata['Ptcplan']["total_click"]; ?></span>
		</div>
		
		<?php $displayrevenue=''; if(strpos($SITECONFIG['modules'], ":module3:1")===false){$displayrevenue='display:none;';}?>
		<div style="<?php echo $displayrevenue; ?>" class="fromnewtext">Revenue Share (%):<span class="red-color">*</span> </div>
		<div style="<?php echo $displayrevenue; ?>" class="fromborderdropedown3">
			<?php echo $this->Form->input('revshare_rate', array('type'=>'text', 'value'=>$ptcplandata['Ptcplan']["revshare_rate"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Pay Commission For The First Purchase Only : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('fpcommission', array(
						'type' => 'select',
						'options' => array('1'=>'Yes', '0'=>'No'),
						'selected' => $ptcplandata['Ptcplan']["fpcommission"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		
		
		<div class="fromnewtext">Referral Commission Re-purchase/Compound Strategy (%) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('commrepurchase', array('type'=>'text', 'value'=>$ptcplandata['Ptcplan']["commrepurchase"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>	
		
		<?php @$commissionlevel=explode(',',$ptcplandata['Ptcplan']["commissionlevel"]); ?>
		<div class="fromnewtext">Referral Commission(%) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<div class="fromborder4left">
				<div class="levalwhitfromtext">Level 1 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 2 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 3 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 4 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 5 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 6 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 7 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[6], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 8 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[7], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 9 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[8], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 10 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[9], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
			</div>
			<div class="tooltipfronright">
				
			</div>
			<div class="clearboth"></div>
		</div>
		
		<div <?php if($SITECONFIG['balance_type']==1){echo 'style="display: none"';}?>>
		<div class="fromnewtext">Earnings Processor Preference : </div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
				<?php 
					echo $this->Form->input('earning_on', array(
						'type' => 'select',
						'options' => array('0'=>'Purchase Processor', '1'=>"Proirity Processor"),
						'selected' => $ptcplandata['Ptcplan']["earning_on"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
				</label>
			</div>
		</div>
		
		</div>
		
		<div class="fromnewtext">Payment Method :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php
			$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning', 'commission'=>'Commission', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase', 'ca:ea'=>'Cash + Earning', 'ca:co'=>'Cash + Commission', 're:ea'=>'Re-purchase + Earning', 're:co'=>'Re-purchase + Commission', 'ea:co'=>'Earning + Commission', 'ca:re:ea'=>'Cash + Re-purchase + Earning', 're:ea:co'=>'Re-purchase + Earning + Commission', 'ea:co:ca'=>'Earning + Commission + Cash', 'co:ca:re'=>'Commission + Cash + Re-purchase', 'ca:ea:re:co'=>'Cash + Re-purchase + Earning + Commission');
			if($SITECONFIG["wallet_for_commission"] == 'cash' && $SITECONFIG["wallet_for_earning"] == 'cash')
			{
				$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase');
			}
			elseif($SITECONFIG["wallet_for_earning"] == 'cash')
			{
				$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'commission'=>'Commission', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase', 'ca:co'=>'Cash + Commission', 're:co'=>'Re-purchase + Commission', 'co:ca:re'=>'Commission + Cash + Re-purchase');
			}
			elseif($SITECONFIG["wallet_for_commission"] == 'cash')
			{
				$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase', 'ca:ea'=>'Cash + Earning', 're:ea'=>'Re-purchase + Earning', 'ca:re:ea'=>'Cash + Re-purchase + Earning');
			}
			$selected = @explode(",",$ptcplandata['Ptcplan']["paymentmethod"]);
			echo $this->Form->input('paymentmethod', array('type' => 'select', 'div'=>false, 'label'=>false, 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $pmethod));?>
		</div>
		
		
		<div class="fromnewtext">Allowed Payment Processors :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php 
			$selected = @explode(",",$ptcplandata['Ptcplan']["paymentprocessors"]);
			echo $this->Form->input('paymentprocessors', array('type' => 'select', 'div'=>false, 'label'=>false, 'class'=>'', 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $paymentprocessors));?>
		</div>
		
		
		<div class="fromnewtext">Click Amount :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('clickcost', array('type'=>'text', 'id'=>'startdate', 'value'=>$ptcplandata['Ptcplan']["clickcost"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Sponsor Get Click Amount :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('clickcostsponsor', array('type'=>'text', 'id'=>'startdate', 'value'=>$ptcplandata['Ptcplan']["clickcostsponsor"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<?php if(isset($ptcplandata['Ptcplan']["id"])){?>
		<div class="fromnewtext">Link : </div>
		<div class="fromborderdropedown3">
			<input type="text" class="fromboxbg" value="<?php echo $SITEURL;?>ptc/purchase/<?php echo $ptcplandata['Ptcplan']["id"];?>" />
		</div>
		
		<?php }?>
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'btnorange',
				'div'=>false,
				'controller'=>'ptc',
				'action'=>'planaction',
				'url'   => array('controller' => 'ptc', 'action' => 'planaction')
			  ));?>
			  
			  <?php echo $this->Js->link("Back", array('controller'=>'ptc', "action"=>"plan"), array(
				  'update'=>'#ptcpage',
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'escape'=>false,
				  'class'=>'btngray'
			  ));?>
		</div>
		
	</div>
<?php echo $this->Form->end();?>
<?php if(!$ajax){?>
</div><!--#ptcpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>