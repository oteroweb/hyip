<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 10-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>		
<?php if(!$ajax){?>
<div class="whitetitlebox">Paid To Click</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("PTC Plans", array('controller'=>'ptc', "action"=>"plan"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("PTC Ads", array('controller'=>'ptc', "action"=>"member"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC Themes", array('controller'=>'ptc', "action"=>"design"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">
<div id="ptcpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Paid_To_Click_-_PTC#PTC_Ads" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#ptcpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'ptc', 'action'=>'click/'.$planid.'/'.$banerid)
	));
	$currentpagenumber=$this->params['paging']['Ptc_historie']['page'];
	?>
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
<?php echo $this->Js->link("Back", array('controller'=>'ptc', "action"=>"member/".$planid), array(
	'update'=>'#ptcpage',
	'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
	'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
	'escape'=>false,
	'class'=>'btnorange'
));?>
	<span class="tabal-title-text content-label"><b><?php echo 'Banner Id';?> : #<?php echo $banerid;?></b></span>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Ptc_historie',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ptc','action'=>'click')));?>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Click Time', array('controller'=>'ptc', "action"=>"click/".$planid."/".$banerid."/0/click_time/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Click Time'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Member Id', array('controller'=>'ptc', "action"=>"click/".$planid."/".$banerid."/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Amount Credited', array('controller'=>'ptc', "action"=>"click/".$planid."/".$banerid."/0/amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Amount Credited'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('IP Address', array('controller'=>'ptc', "action"=>"click/".$planid."/".$banerid."/0/ip_address/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By IP Address'
					));?>
				</div>
			</div>
			<?php foreach ($ptc_histories as $ptc_historie): ?>
				<div class="tablegridrow">
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $ptc_historie['Ptc_historie']['click_time']); ?></div>
					<div>
						<?php 
						echo $this->Js->link($ptc_historie['Ptc_historie']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$ptc_historie['Ptc_historie']['member_id']."/top/ptc/index~top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>$<?php echo round($ptc_historie['Ptc_historie']['amount'],4); ?></div>
					<div><?php echo $ptc_historie['Ptc_historie']['ip_address']; ?></div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($ptc_histories)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Ptc_historie']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Ptc_historie',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ptc','action'=>'click/'.$planid.'/'.$banerid.'/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#ptcpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'ptc',
			  'action'=>'click/'.$planid.'/'.$banerid.'/rpp',
			  'url'   => array('controller' => 'ptc', 'action' => 'click/'.$planid.'/'.$banerid.'/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	</div>
</div>
<?php if(!$ajax){?>
</div><!--#ptcpage over-->
</div>
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>