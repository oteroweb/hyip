<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($Enableptc==1) { ?>
<?php if(!$ajax){ ?>
<div id="Ptcpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>

<?php // Code for showing free available plans start
if(isset($pending_free_plans) && count($pending_free_plans)){
?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Free Plans Available");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php // PTC free Plans table starts here ?>
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam"><?php echo __('Plan Name');?></div>
				<div class="divth textcenter vam"><?php echo __("Action");?></div>
			</div>
	        </div>
	        <div class="divtbody">
		        <tr><td colspan="8" class="ovw-padding-tabal"></td></tr>
		        <?php
		        $i = 0;
		        foreach ($pending_free_plans as $pending_free_plan):
			       if($i%2==0){$class='white-color';}else{$class='gray-color';}
			       ?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $pending_free_plan['Ptcplan']['plan_name'];?></div>
					<div class="divtd textcenter vam">
					<?php
						echo $this->Js->link('<span class="purchasebutton getitbutton">Get it</span>', array('controller'=>'ptc', "action"=>"getfreeptc/".$pending_free_plan['Pending_free_plan']['id']), array(
							'update'=>'#Ptcpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Get it')
						));
					?>
					</div>
				</div>
		        <?php $i++;endforeach; ?>
	       </div>
	</div>
	<?php // PTC free Plans table ends here ?>
</div>
<div class="height5"></div>
<?php } // Code for showing free available plans end ?>

<div class="clear-both"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("PTC");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php // PTC plans table starts here ?>
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam"><?php echo __('Plan Name');?></div>
				<div class="divth textcenter vam"><?php echo __('Banner Size');?></div>
				<div class="divth textcenter vam"><?php echo __('Clicks');?></div>
				<div class="divth textcenter vam"><?php echo __('All Countries').' '.__('Price');?></div>
				<div class="divth textcenter vam"><?php echo __('Selected Countries').' '.__('Price');?></div>
				<div class="divth textcenter vam"><?php echo __('Action');?></div>
			</div>
		</div>
		<div class="divtbody">
			<?php $i=1;
			foreach ($ptcplandata as $ptcplan):
			if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $ptcplan['Ptcplan']['plan_name']; ?></div>
					<div class="divtd textcenter vam"><?php echo ($ptcplan['Ptcplan']['banner']==1)?$ptcplan['Ptcplan']['banner_size']:"N/A"; ?></div>
					<div class="divtd textcenter vam"><?php echo $ptcplan['Ptcplan']['total_click']; ?></div>
					<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($ptcplan['Ptcplan']['all_country_price']*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
					<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($ptcplan['Ptcplan']['geo_price']*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
					<div class="divtd textcenter vam">
						<?php
							echo $this->Js->link('<span class="purchasebutton">Purchase</span>', array('controller'=>'ptc', "action"=>"purchase/".$ptcplan['Ptcplan']['id']), array(
								'update'=>'#Ptcpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Purchase')
							));
						?>
					</div>
				</div>
			<?php $i++;endforeach; ?>
		</div>
	</div>
	<?php if(count($ptcplandata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
</div>
	<?php // PTC plans table ends here ?>
	
<script>
$(document).ready(function(){
	//$("a[rel='lightboxtext']").colorbox({width:"80%", height:"80%", inline:true, href:"#"+atag.title});
	$(".iframeclass").colorbox({width:"80%", height:"80%", iframe:true});
});
</script>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('My PTC Ads');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#Ptcpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'ptc', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Ptcbanner']['page'];
	?>
		<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
		
		<?php // My PTC Ads table starts here ?>
		<div class="divtable">
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam">
						<?php 
						if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
						echo $this->Js->link(__("Id"), array('controller'=>'ptc', "action"=>"index/0/id/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Ptcpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Id')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Plan"), array('controller'=>'ptc', "action"=>"index/0/ptc_id/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Ptcpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Plan')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Banner"), array('controller'=>'ptc', "action"=>"index/0/banner_title/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Ptcpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Banner')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Displayed"), array('controller'=>'ptc', "action"=>"index/0/display_counter/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Ptcpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Displayed')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Clicked"), array('controller'=>'ptc', "action"=>"index/0/click_counter/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Ptcpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Clicked')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Likes"), array('controller'=>'ptc', "action"=>"index/0/like_counter/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Ptcpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Likes')
						));?>
					</div>
					<div class="divth textcenter vam"><?php echo __("Status"); ?></div>
					<div class="divth textcenter vam"><?php echo __("Action"); ?></div>
				</div>
			</div>
			<div class="divtbody">
				<?php $i=1;
				foreach ($Ptcbanners as $Ptcbanner):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $Ptcbanner['Ptcbanner']['id'];?></div>
						<div class="divtd textcenter vam">
							<a href="#" class='vtip' title="<?php echo __('Plan Name')." : ".$Ptcbanner['Ptcplan']['plan_name'];?>"><?php echo $Ptcbanner['Ptcbanner']['ptc_id'];?></a>
						</div>
						<div class="divtd textcenter vam">
							<p><b><?php echo __("Purchase Date"); ?> : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $Ptcbanner['Ptcbanner']['purchasedate']); ?></p>
							<?php if($Ptcbanner['Ptcbanner']['banner_size'] != "") { ?>
							<a href="<?php echo $Ptcbanner['Ptcbanner']['target_url'];?>" target="_blank" class="vtip" title="<b><?php echo __('Title'); ?> : </b><?php echo $Ptcbanner['Ptcbanner']['banner_title']; ?>">
								<?php
									if($Ptcbanner['Ptcbanner']['banner_size']=='125x125'){$height="50px";$maxwidth='';}
									elseif($Ptcbanner['Ptcbanner']['banner_size']=='468x60'){$height="95%";$maxwidth="468px";}
									else{$height="95%";$maxwidth="728px";}
									echo '<img src="'.$Ptcbanner['Ptcbanner']['banner_url'].'" alt="'.$Ptcbanner['Ptcbanner']['banner_title'].'" width="'.$height.'" style="max-width:'.$maxwidth.';" />';
								?>
							</a>
							<?php } else { ?>
							<a href="<?php echo $Ptcbanner['Ptcbanner']['target_url'];?>" target="_blank" class="vtip" title="<b><?php echo __('Title'); ?> : </b><?php echo $Ptcbanner['Ptcbanner']['banner_title']; ?>"><b><?php echo __('Title'); ?> : </b><?php echo $Ptcbanner['Ptcbanner']['banner_title']; ?></a>
							<?php } ?>
							<div><b><?php echo __("Display Start Date"); ?> : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $Ptcbanner['Ptcbanner']['start_date']); ?></div>
						</div>
						<div class="divtd textcenter vam"><?php echo $Ptcbanner['Ptcbanner']['display_counter'];?></div>
						<div class="divtd textcenter vam">
							<?php if($Ptcbanner['Ptcbanner']['display_area']!='all'){?>
								<a href="<?php echo $SITEURL;?>ptc/ptc/geo/<?php echo $Ptcbanner['Ptcbanner']['id'];?>" class="iframeclass">
									<span class="vtip" title="<?php echo __('Click here to view country wise clicks'); ?>"><?php echo $Ptcbanner['Ptcbanner']['click_counter'];?></span>
								</a>
								<div style="display:none;"><div id="inlinebox<?php echo $Ptcbanner['Ptcbanner']['id'];?>"><?php echo $Ptcbanner['Ptcbanner']['click_counter'];?></div></div>
							<?php }else{
								echo $Ptcbanner['Ptcbanner']['click_counter'];
							}?>
						</div>
						<div class="divtd textcenter vam"><?php echo $Ptcbanner['Ptcbanner']['like_counter'];?></div>
						<div class="divtd textcenter vam">
							<?php 
							if($Ptcbanner['Ptcbanner']['total_click']<=$Ptcbanner['Ptcbanner']['click_counter'])
								echo __("Expired");
							elseif($Ptcbanner['Ptcbanner']['pause']==0)
								echo __("Paused");
							elseif($Ptcbanner['Ptcbanner']['status']==1) 
								echo __("Running");
							elseif($Ptcbanner['Ptcbanner']['status']==0)
								echo __("Pending");
							?>
						</div>
						<div class="divtd textcenter vam">
							
							<div class="actionmenu">
								<div class="btn-group">
								  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
									  Action <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu">
									  <li>
									      <?php 
											if($Ptcbanner['Ptcbanner']['pause']==0){
												$pauseaction='1';
												$pauseicon='pause.png';
												$pausetext='Unpause PTC Ad';
											}else{
												$pauseaction='0';
												$pauseicon='play.png';
												$pausetext='Pause PTC Ad';}
											echo $this->Js->link($this->html->image($pauseicon, array('alt'=>__($pausetext)))." ".__($pausetext), array('controller'=>'ptc', "action"=>"status/".$pauseaction."/".$Ptcbanner['Ptcbanner']['id']."/".$currentpagenumber), array(
												'update'=>'#Ptcpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));
										?>
									</li>
									 <li>
										<?php
											echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit PTC Ad')))." ".__('Edit PTC Ad'), array('controller'=>'ptc', "action"=>"add/".$Ptcbanner['Ptcbanner']['id']), array(
												'update'=>'#Ptcpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));
										?>
									 </li>
								   </ul>
								</div>
							</div>
							
						</div>
					</div>
				<?php $i++;endforeach; ?>
			</div>
		</div>
		<?php if(count($Ptcbanners)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
		<?php // My PTC Ads table ends here ?>
	
	<?php // Paging code starts here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Ptcbanner']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Ptcplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ptc','action'=>'index/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#Ptcpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'ptc',
						  'action'=>'index/rpp',
						  'url'   => array('controller' => 'ptc', 'action' => 'index/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
			</div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
	</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>