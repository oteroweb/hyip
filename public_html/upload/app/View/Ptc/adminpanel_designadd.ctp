<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<script type="text/javascript"> 
(function() {
$('#PtcdesignForm').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();
</script>
<script type="text/javascript">$(function() {$('.colorpicker').wheelColorPicker({ sliders: "whsvp", preview: true, format: "css" });});</script>
<script type="text/javascript">
function previewtheme()
{
	var statstyle='height: 110px;';
	if($("#PtcdesignDtype").val()==1)
	{
		statstyle=statstyle+$("#PtcdesignDesign").val();
		$('.precssbox .setbackground').attr('style', statstyle);
	}
	else
	{
		//statstyle=statstyle+"<?php echo 'background:url(\''.$SITEURL."img/ptc/background/".$ptcdesigndata['Ptcdesign']["design"].'\');'.$ptcdesigndata['Ptcdesign']["imagecss"];?>";
		statstyle=statstyle+"<?php echo $ptcdesigndata['Ptcdesign']["imagecss"];?>;";
		//$('.preimgbox .setbackground').attr('style', statstyle);
		$('.preimgbox .setbackground').attr('style', $('.preimgbox .setbackground').attr('style')+statstyle);
	}
	$('.ptcboxbottom').attr('style', $("#PtcdesignButtoncss").val());
	$('.ptcboxbottomborder').css('background-color', $("#PtcdesignBordercolor").val());
	$('.ptcboxtitle').attr('style', $("#PtcdesignTitlecss").val());
	return false;
}
function setimage(input)
{
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
		   $('.preimgbox .setbackground').attr('style', 'height: 110px;background:url(\''+e.target.result+'\');');
		};
		reader.readAsDataURL(input.files[0]);
	}
}
</script>
<?php if(!$ajax){?>
<div class="whitetitlebox">Paid To Click</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("PTC Plans", array('controller'=>'ptc', "action"=>"plan"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC Ads", array('controller'=>'ptc', "action"=>"member"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("PTC Themes", array('controller'=>'ptc', "action"=>"design"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">
<div id="ptcpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Paid_To_Click_-_PTC#PTC_Themes" target="_blank">Help</a></div>
<div id="UpdateMessage" class="UpdateMessage"></div>
	
	<div style="width: 455px;margin: 0 auto;">
		<style>.ptcboxbottom { background: linear-gradient(to bottom, #3eb2cb 0%, #2a9eb7 51%, #0f839c 100%) repeat scroll 0 0 rgba(0, 0, 0, 0); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3eb2cb', endColorstr='#0f839c'),GradientType=1; border-top-left-radius : 7px; border-top-right-radius: 7px; display: inline-block; padding: 3px 13px; font-size: 15px; font-weight: bold; color: #FFF;}
		.ptcboxbottomborder{height: 2px;background-color: #0f839c;}
		.ptcboxtitle {color: #FFFFFF;float: left;font-size: 17px;font-weight: bold;padding: 65px 0 0 115px;max-width: 70%;text-align: left;}
		</style>
		
		<div class="preimgbox imagefields" style="display:<?php if($ptcdesigndata['Ptcdesign']["dtype"]==0){echo '';}else{echo 'none';}?>;">
			<div style="position: relative;width: 454px; height: 269px;">
				<div class="setbackground" style="height: 110px;<?php echo 'background:url(\''.$SITEURL."img/ptc/background/".$ptcdesigndata['Ptcdesign']["design"].'\');'.$ptcdesigndata['Ptcdesign']["imagecss"];?>">
					<div class="ptcboxtitle" style="<?php echo $ptcdesigndata['Ptcdesign']['titlecss'];?>">Your Title Here</div>
				</div>
				<img src="<?php echo $SITEURL;?>img/ptc/thumb.png" style="top: 0; position: absolute;" />
			</div>
			<div style="position: relative;width: 454px;text-align: center;">
				<div class="ptcboxbottom" style="<?php echo $ptcdesigndata['Ptcdesign']['buttoncss'];?>">Click Here To Earn</div>
				<div class="ptcboxbottomborder" style="background-color:<?php echo $ptcdesigndata['Ptcdesign']['bordercolor'];?>"></div>
			</div>
		</div>
		<div class="precssbox cssfields" style="display:<?php if($ptcdesigndata['Ptcdesign']["dtype"]==1){echo '';}else{echo 'none';}?>;">
			<div style="position: relative;width: 454px; height: 269px;">
				<div class="setbackground" style="height: 110px;<?php echo $ptcdesigndata['Ptcdesign']['design'];?>">
					<div class="ptcboxtitle" style="<?php echo $ptcdesigndata['Ptcdesign']['titlecss'];?>">Your Title Here</div>
				</div>
				<img src="<?php echo $SITEURL;?>img/ptc/thumb.png" style="top: 0; position: absolute;" />
			</div>
			<div style="position: relative;width: 454px;text-align: center;">
				<div class="ptcboxbottom" style="<?php echo $ptcdesigndata['Ptcdesign']['buttoncss'];?>">Click Here To Earn</div>
				<div class="ptcboxbottomborder" style="background-color:<?php echo $ptcdesigndata['Ptcdesign']['bordercolor'];?>"></div>
			</div>
		</div>
	</div>
<?php echo $this->Form->create('Ptcdesign',array('id'=>'PtcdesignForm', 'type' => 'file', 'onsubmit' => 'return true;', 'url'=>array('controller'=>'ptc','action'=>'designaction')));?>
<?php if(isset($ptcdesigndata['Ptcdesign']["id"])){
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$ptcdesigndata['Ptcdesign']["id"], 'label' => false));
	echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
}?>
		
	<div class="frommain">
	
		<div class="fromnewtext">Status : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('status', array(
						'type' => 'select',
						'options' => array('1'=>'Active', '0'=>'Inactive'),
						'selected' => $ptcdesigndata['Ptcdesign']["status"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		
		
		<div class="fromnewtext">Name :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('dname', array('type'=>'text', 'value'=>$ptcdesigndata['Ptcdesign']["dname"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Background Type :<span class="red-color">*</span>  </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('dtype', array(
						'type' => 'select',
						'options' => array('0'=>'Image' ,'1'=>'CSS'),
						'selected' => $ptcdesigndata['Ptcdesign']["dtype"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => '',
						'onchange' => 'if(this.selectedIndex!=0){$(".cssfields").show(500);$(".imagefields").hide(0);}else{$(".imagefields").show(500);$(".cssfields").hide(0);}'
					));
				?>
			  </label>
		  </div>
		</div>
		
		
		<div class="imagefields" style="display:<?php if($ptcdesigndata['Ptcdesign']["dtype"]==0){echo '';}else{echo 'none';}?>;">
			<div class="fromnewtext">Background Image : <span class="red-color">*</span></div>
			<div class="fromborderdropedown3">
			  <div class="btnorange browsebutton">Browse<?php echo $this->Form->input('image', array('type' => 'file','label' => false, 'div' => false,'class'=>'browserbuttonlink')); ?></div>
			</div>
			
		</div>
	
		<div class="imagefields" style="display:<?php if($ptcdesigndata['Ptcdesign']["dtype"]==0){echo '';}else{echo 'none';}?>;">
			<div class="fromnewtext">Background Image CSS :</div>
			<div class="fromborderdropedown3">
			  <?php echo $this->Form->input('imagecss', array('type'=>'textarea', 'value'=>$ptcdesigndata['Ptcdesign']["imagecss"],'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
			  
			</div>
		</div>
		<div class="cssfields" style="display:<?php if($ptcdesigndata['Ptcdesign']["dtype"]==1){echo '';}else{echo 'none';}?>;">
			<div class="fromnewtext">Background CSS :</div>
			<div class="fromborderdropedown3">
			  <?php echo $this->Form->input('design', array('type'=>'textarea', 'value'=>$ptcdesigndata['Ptcdesign']["design"],'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
			  
			</div>
		</div>
		
		<div class="fromnewtext">Title CSS :</div>
		<div class="fromborderdropedown3">
		  <?php echo $this->Form->input('titlecss', array('type'=>'textarea', 'value'=>$ptcdesigndata['Ptcdesign']["titlecss"],'label' => false,  'div' => false,'class'=>'from-textarea'));?>
		  
		</div>
		
		<div class="fromnewtext">Click Button CSS :</div>
		<div class="fromborderdropedown3">
		  <?php echo $this->Form->input('buttoncss', array('type'=>'textarea', 'value'=>$ptcdesigndata['Ptcdesign']["buttoncss"],'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
		  
		</div>
		
		<div class="fromnewtext">Bottom Border Color : </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('bordercolor', array('type'=>'text', 'value'=>$ptcdesigndata['Ptcdesign']["bordercolor"],'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="formbutton">
			<?php echo $this->Form->input('Preview', array(
				'type' => 'button',
				'class'=>'btnorange',
				'div'=>false,
				'label'=>false,
				'onclick'=>"return previewtheme();"
			));?>
			
			<?php echo $this->Form->submit('Submit', array(
			  'class'=>'btnorange',
			  'div'=>false
			));?>
			
			<?php echo $this->Js->link("Back", array('controller'=>'ptc', "action"=>"design"), array(
				'update'=>'#ptcpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'btngray'
			));?>
		</div>
	</div>
<?php echo $this->Form->end();?>
<?php if(!$ajax){?>
</div><!--#ptcpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>