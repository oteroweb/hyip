<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Paid To Click</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("PTC Ads", array('controller'=>'ptc', "action"=>"member"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC Plans", array('controller'=>'ptc', "action"=>"plan"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("PTC Themes", array('controller'=>'ptc', "action"=>"design"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">
<div id="ptcpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Paid_To_Click_-_PTC#PTC_Themes" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#ptcpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'ptc', 'action'=>'design')
	));
	$currentpagenumber=$this->params['paging']['Ptcdesign']['page'];
	?>
<div id="Xgride-bg" class="greenbottomborder">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('ptc',$SubadminAccessArray) || in_array('ptc/adminpanel_designadd', $SubadminAccessArray)){ ?>
		<?php echo $this->Js->link("+ Add New", array('controller'=>'ptc', "action"=>"designadd"), array(
			'update'=>'#ptcpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btnorange'
		));?>
	<?php } ?>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Ptcdesign',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ptc','action'=>'design')));?>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>Id</div>
				<div>Name</div>
				<div>Type</div>
				<div>Action</div>
            </div>
			<?php foreach ($ptcdesigndata as $ptcdesign): ?>
				<div class="tablegridrow">
					<div><?php echo $ptcdesign['Ptcdesign']['id']; ?></div>
					<div><?php echo $ptcdesign['Ptcdesign']['dname']; ?></div>
					<div><?php echo ($ptcdesign['Ptcdesign']['dtype']==0)?"Image":"CSS"; ?></div>
					<div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							
							<?php if(!isset($SubadminAccessArray) || in_array('ptc',$SubadminAccessArray) || in_array('ptc/adminpanel_designadd/$', $SubadminAccessArray)){ ?>
						  	<li>
								<?php
									echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit PTC Theme')).' Edit PTC Theme', array('controller'=>'ptc', "action"=>"designadd/".$ptcdesign['Ptcdesign']['id']), array(
										'update'=>'#ptcpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('ptc',$SubadminAccessArray) || in_array('ptc/adminpanel_designstatus', $SubadminAccessArray)){ ?>
							<li>
								<?php 
									if($ptcdesign['Ptcdesign']['status']==0){
										$statusaction='1';
										$statusicon='red-icon.png';
										$statustext='Activate PTC Theme';
									}else{
										$statusaction='0';
										$statusicon='blue-icon.png';
										$statustext='Inactivate PTC Theme';}
									echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'ptc', "action"=>"designstatus/".$statusaction."/".$ptcdesign['Ptcdesign']['id']."/".$currentpagenumber), array(
										'update'=>'#ptcpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('ptc',$SubadminAccessArray) || in_array('ptc/adminpanel_designremove', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete PTC Theme')).' Delete PTC Theme', array('controller'=>'ptc', "action"=>"designremove/".$ptcdesign['Ptcdesign']['id']."/".$currentpagenumber), array(
									'update'=>'#ptcpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'confirm'=>"Do You Really Want To Delete This PTC Design?"
								));?>
							</li>
							<?php } ?>
						  </ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($ptcdesigndata)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Ptcdesign']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Ptcdesign',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ptc','action'=>'design/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#ptcpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'ptc',
			  'action'=>'design/rpp',
			  'url'   => array('controller' => 'ptc', 'action' => 'design/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
	</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#ptcpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>