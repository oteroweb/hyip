<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>		
<?php if(!$ajax){?>
<div class="whitetitlebox">Paid To Click</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("PTC Ads", array('controller'=>'ptc', "action"=>"member"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("PTC Plans", array('controller'=>'ptc', "action"=>"plan"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC Themes", array('controller'=>'ptc', "action"=>"design"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">
<div id="ptcpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Paid_To_Click_-_PTC#PTC_Plans" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="serchmainbox">
	 <?php echo $this->Form->create('Ptcplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ptc','action'=>'plan')));?>			
    <div class="serchgreybox">
		Search Option
    </div>
    <div class="from-box">
        <div class="fromboxmain">
            <span>Search By :</span>
            <span>                     
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
						<?php 
						echo $this->Form->input('searchby', array(
							  'type' => 'select',
							  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'plan_name'=>'Plan Name', 'banner_size'=>'Banner Size', 'total_click'=>'Clicks',  'all_country_price'=>'All Countries Price', 'geo_price'=>'GEO(Selected Countries) Price', 'clickcost'=>'Click Amount'),
							  'selected' => $searchby,
							  'class'=>'',
							  'label' => false,
							  'style' => '',
							  'onchange'=>'if($(this).val()=="banner_size"){$(".searchfor_all").hide();$(".searchfor_size").show(500);}else{$(".searchfor_size").hide();$(".searchfor_all").show(500);}'
						));
						?>
						</label>
					</div>
				</div>
            </span>
		</div>
        <div class="fromboxmain">
				<span>Search For :</span>
				<span  class='searchfor_all' style='display:<?php if($searchby=="banner_size"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?></span>
				<span class='searchfor_size' style='display:<?php if($searchby!="banner_size"){ echo "none";} ?>'>
					<div class="searchoptionselect">
						<div class="select-main">
							<label>
							<?php 
							echo $this->Form->input('banner_size', array(
								  'type' => 'select',
								  'options' => array('125x125'=>'125x125', '468x60'=>'468x60', '728x90'=>'728x90'),
								  'selected' => $searchfor,
								  'class'=>'',
								  'label' => false,
								  'style' => ''
							));
							?>
							</label>
						</div>
					</div>	
				</span>
				<span class="padding-left">
					<?php echo $this->Js->submit('', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#ptcpage',
						'class'=>'searchbtn',
						'controller'=>'ptc',
						'action'=>'plan',
						'url'=> array('controller' => 'ptc', 'action' => 'plan')
					));?>
				</span>
		</div>
   </div>
   
   <?php echo $this->Form->end();?>
</div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#ptcpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'ptc', 'action'=>'plan')
	));
	$currentpagenumber=$this->params['paging']['Ptcplan']['page'];
	?>
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('ptc',$SubadminAccessArray) || in_array('ptc/adminpanel_planadd', $SubadminAccessArray)){ ?>
		<?php echo $this->Js->link("+ Add New", array('controller'=>'ptc', "action"=>"planadd"), array(
			'update'=>'#ptcpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btnorange'
		));?>
	<?php } ?>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Ptcplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ptc','action'=>'plan')));?>
		<div class="tablegrid">
			<div class="tablegridheader">
                <div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'ptc', "action"=>"plan/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Plan Name', array('controller'=>'ptc', "action"=>"plan/0/0/plan_name/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Name'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Banner Size', array('controller'=>'ptc', "action"=>"plan/0/0/banner_size/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Banner Size'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Clicks', array('controller'=>'ptc', "action"=>"plan/0/0/total_click/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Clicks'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('All Countries Price', array('controller'=>'ptc', "action"=>"plan/0/0/all_country_price/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By All Countries Price'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('GEO Price', array('controller'=>'ptc', "action"=>"plan/0/0/geo_price/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By GEO(Selected Countries) Price'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Click Amount', array('controller'=>'ptc', "action"=>"plan/0/0/clickcost/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Click Amount'
					));?>
				</div>
				<div><?php echo 'Action';?></div>
            </div>
			<?php foreach ($ptcplandata as $ptcplan): ?>
				<div class="tablegridrow">
					<div><?php echo $ptcplan['Ptcplan']['id']; ?></div>
					<div><?php echo $ptcplan['Ptcplan']['plan_name']; ?></div>
					<div><?php echo ($ptcplan['Ptcplan']['banner']==1)?$ptcplan['Ptcplan']['banner_size']:"N/A"; ?></div>
					<div><?php echo $ptcplan['Ptcplan']['total_click']; ?></div>
					<div>$<?php echo $ptcplan['Ptcplan']['all_country_price']; ?></div>
					<div>$<?php echo $ptcplan['Ptcplan']['geo_price']; ?></div>
					<div><?php echo $ptcplan['Ptcplan']['clickcost']; ?></div>
					<div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							
							<?php if(!isset($SubadminAccessArray) || in_array('ptc',$SubadminAccessArray) || in_array('ptc/adminpanel_planadd/$', $SubadminAccessArray)){ ?>
							<li>
								<?php
									echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit PTC Plan')).' Edit PTC Plan', array('controller'=>'ptc', "action"=>"planadd/".$ptcplan['Ptcplan']['id']), array(
										'update'=>'#ptcpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<?php } ?>
							
						  	<?php if(!isset($SubadminAccessArray) || in_array('ptc',$SubadminAccessArray) || in_array('ptc/adminpanel_planstatus', $SubadminAccessArray)){ ?>
							<li>
								<?php 
									if($ptcplan['Ptcplan']['status']==0){
										$statusaction='1';
										$statusicon='red-icon.png';
										$statustext='Activate PTC Plan';
									}else{
										$statusaction='0';
										$statusicon='blue-icon.png';
										$statustext='Inactivate PTC Plan';}
									echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'ptc', "action"=>"planstatus/".$statusaction."/".$ptcplan['Ptcplan']['id']."/".$currentpagenumber), array(
										'update'=>'#ptcpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<?php } ?>
							<?php if(!isset($SubadminAccessArray) || in_array('ptc',$SubadminAccessArray) || in_array('ptc/adminpanel_member', $SubadminAccessArray)){ ?>
							<li>
								<?php 
									echo $this->Js->link($this->html->image('view-members.png', array('alt'=>'View PTC Plan Members')).' View PTC Plan Members', array('controller'=>'ptc', "action"=>"member/".$ptcplan['Ptcplan']['id']), array(
										'update'=>'#ptcpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('ptc',$SubadminAccessArray) || in_array('ptc/adminpanel_planremove', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete PTC Plan')).' Delete PTC Plan', array('controller'=>'ptc', "action"=>"planremove/".$ptcplan['Ptcplan']['id']."/".$currentpagenumber), array(
									'update'=>'#ptcpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'confirm'=>"Do You Really Want To Delete This PTC Plan?"
								));?>
							</li>
							<?php } ?>
						  </ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
	</div>
		<?php if(count($ptcplandata)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Ptcplan']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Ptcplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ptc','action'=>'plan/0/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#ptcpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'ptc',
			  'action'=>'plan/0/rpp',
			  'url'   => array('controller' => 'ptc', 'action' => 'plan/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#ptcpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>