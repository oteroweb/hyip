<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Paid To Click</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("PTC Plans", array('controller'=>'ptc', "action"=>"plan"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("PTC Ads", array('controller'=>'ptc', "action"=>"member"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC Themes", array('controller'=>'ptc', "action"=>"design"), array(
						'update'=>'#ptcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">
<div id="ptcpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Paid_To_Click_-_PTC#PTC_Ads" target="_blank">Help</a></div>
<div id="UpdateMessage" class="UpdateMessage"></div>
<?php echo $this->Form->create('Ptcbanner',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'ptc','action'=>'addaction')));?>
	<?php if(isset($ptcbannerdata['Ptcbanner']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$ptcbannerdata['Ptcbanner']["id"], 'label' => false));
		echo $this->Form->input('clicklimit', array('type'=>'hidden', 'value'=>$ptcbannerdata['Ptcbanner']["total_click"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
	<div class="frommain">
	
		<div class="fromnewtext">Advance Settings : </div>
		<div class="fromborderdropedown">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('status', array(
						'type' => 'select',
						'options' => array('1'=>'Active', '0'=>'Inactive'),
						'selected' => $ptcbannerdata['Ptcbanner']["status"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		<span data-original-title="If You Want to Provide <b>Paid Credits</b> to The Members, Select Credit Plan. And if You Want to Allow Them to Add a Banner on The Site For Specific Days, Select <b>Banner Plan</b>." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="fromnewtext">Pause : </div>
		<div class="fromborderdropedown">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('pause', array(
						'type' => 'select',
						'options' => array('1'=>'Unpause', '0'=>'Pause'),
						'selected' => $ptcbannerdata['Ptcbanner']["pause"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		<span data-original-title="Set This to 'No' if You Do Not Want Members to See This Plan on Banner Ad Plan Purchase Page. This Option is Useful When You Want to Allow Only <b>Selected Members</b> to Purchase This Plan or Award This With Plan(s) Position, Memberships or Signup. Just Provide The <b>Direct Link</b> to Your Selected Members to Allow Them to Purchase This Plan. The Link Can Be Found at The End of Plan Edit Page Once You Have Created One." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		
		<?php if($ptcbannerdata['Ptcbanner']["banner_size"]!="") { ?>
		<div class="fromnewtext">Banner Size :</div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
				  <?php 
					echo $this->Form->input('banner_size', array(
						'type' => 'select',
						'options' => array('125x125'=>'125X125', '468x60'=>'468X60', '728x90'=>'728X90'),
						'selected' => $ptcbannerdata['Ptcbanner']["banner_size"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
				</label>
			</div>
		</div>
		<?php } ?>
		
		<div class="fromnewtext">Price ($) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('price', array('type'=>'text', 'value'=>$banneradplandata['Banneradplan']["price"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Title :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('banner_title', array('type'=>'text', 'value'=>$ptcbannerdata['Ptcbanner']["banner_title"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<?php if($ptcbannerdata['Ptcbanner']["banner_size"]!="") { ?>
		<div class="fromnewtext">Banner URL :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('banner_url', array('type'=>'text', 'value'=>$ptcbannerdata['Ptcbanner']["banner_url"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		<?php } ?>
		
		<div class="fromnewtext">Destination URL :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('target_url', array('type'=>'text', 'value'=>$ptcbannerdata['Ptcbanner']["target_url"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Description :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('banner_description', array('type'=>'textarea', 'value'=>stripslashes($ptcbannerdata['Ptcbanner']["banner_description"]),'label' => false, 'div'=>false, 'class'=>'from-textarea'));?>
		</div>
		
		<div class="fromnewtext">Daily Click Limit :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('daily_budget', array('type'=>'text', 'id'=>'startdate', 'value'=>$ptcbannerdata['Ptcbanner']["daily_budget"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Display Area : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
					if($ptcbannerdata['Ptcbanner']["display_area"]=='all'){$display_area='all';}else{$display_area='country';}
					echo $this->Form->input('display_area', array(
						'type' => 'select',
						'options' => array('all'=>'All Countries', 'country'=>'Selected Countries'),
						'selected' => $display_area,
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => '',
						'onchange' => 'if(this.selectedIndex!=0){$(".countryfields").show(500);}else{$(".countryfields").hide(500);}'
					));
				?>
			  </label>
		  </div>
		</div>
		
		<div class="countryfields" style="display:<?php if($ptcbannerdata['Ptcbanner']["display_area"]!='all'){echo '';}else{echo 'none';}?>;">
			<div class="fromnewtext">Country : </div>
			<div class="fromborderdropedown3 checkboxlist" style="height:250px;overflow:auto;">
				<?php
				$ccounter=0;
				$replacecountry=array('India','Dominica','Guinea','Mali','Netherlands','Niger','Oman','Samoa','Curacao');
				$country=array('India_2','Dominica_2','Guinea_2','Mali_2','Netherlands_2','Niger_2','Oman_2','Samoa_2','Netherlands Antilles');
				$display_area=str_replace($country,$replacecountry,$ptcbannerdata['Ptcbanner']["display_area"]);
				$countrynames=@explode(",", $display_area);
				foreach($countries as $cid=>$cname)
				{
					if(in_array(trim($cname), $countrynames)){$selected = 'checked="checked"';}else{ $selected = '';}
					echo '<div class="checkbox"><input id="chk'.$ccounter.'" type="checkbox" name="country[]" value="'.trim($cname).'" '.$selected.' /> <label for="chk'.$ccounter.'">'.trim($cname).'</label></div>';
					$ccounter++;
				}
				?>
			</div>
			
		</div>
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'btnorange',
				'div'=>false,
				'controller'=>'ptc',
				'action'=>'addaction',
				'url'   => array('controller' => 'ptc', 'action' => 'addaction')
			  ));?>
			  
			  <?php echo $this->Js->link("Back", array('controller'=>'ptc', "action"=>"member/".$planid), array(
				  'update'=>'#ptcpage',
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'escape'=>false,
				  'class'=>'btngray'
			  ));?>
		</div>
		
	</div>
<?php echo $this->Form->end();?>
<?php if(!$ajax){?>
</div><!--#ptcpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>