<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php // This file is used to show PTC view page. Member earns through PTC ads using this page ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>[SEOTITLE] - [SITETITLE]</title>
	<meta content="[SEOKEYWORDS]" name="keywords">
	<meta content="[SEODESCRIPTION]" name="description">
	<meta name="robots" content="noindex,nofollow" />
	<style type="text/css">
		.clearboth{clear: both;}
		.heught6{height: 6px;}
		#wrapper{background-image: url("[SITEURL]img/ptc/gradient.jpg");background-repeat: repeat-x;height:100px;}
		#credittopframe{color: #ffffff;font-size: 13px;font-weight: bold;font-family: Arial;}
		#progressbar{border: 1px solid #00c0e8;width: 183px;height: 25px;float: left;margin: 3px 0 0 0;
		-moz-border-radius:5px;-webkit-border-radius:5px;-khtml-border-radius:5px;border-radius:5px;}
		#progressbar div{width: 0px;height: 25px;
			background-image: url("[SITEURL]img/ptc/progressbar.jpg");
			background-repeat: repeat-x; 
			-moz-border-radius:5px;-webkit-border-radius:5px;-khtml-border-radius:5px;border-radius:5px;}
		#displayCounter{width: 25px;height: 27px;float: left;padding: 7px 0 0 6px;text-align: center;
			color: #00d3ff;font-size: 15px;font-weight: bold;font-family: Arial;
			background-image: url("[SITEURL]img/ptc/number.jpg");
			background-repeat: no-repeat;}
		#header{margin: 0 auto;width: 950px;padding: 12px 0 0 0;}
		#header .logo img{
			float: left;
			-moz-border-radius:5px 5px 0px 0px;-webkit-border-radius:5px 5px 0px 0px;-khtml-border-radius:5px 5px 0px 0px;border-radius:5px 5px 0px 0px;
			background-repeat: no-repeat;
			background-position: right;
			background-color: #ececec;
			width: 266px;
			height:56px;
		}
		#header .content{float: left;width: 671px;padding: 9px 0 0 8px;}
		#header .link1{display: none;float: left;font-family: arial;font-size: 12px;font-weight: bold;color: #2c2b2b;background-color: #cbd3d9;padding: 3px 15px;text-decoration: none;
		-moz-border-radius:5px;-webkit-border-radius:5px;-khtml-border-radius:5px;border-radius:5px;}
		#header .link2{display: none;float: right;font-family: arial;font-size: 12px;font-weight: bold;color: #2c2b2b;background-color: #f1b700;padding: 3px 15px;text-decoration: none;
		-moz-border-radius:5px;-webkit-border-radius:5px;-khtml-border-radius:5px;border-radius:5px;}
	</style>
	<script type="text/javascript" src="[SITEURL]js/jquery-1.8.3.min.js"></script>
	
	<script type="text/javascript">
	function ptc_counter_start(){
		
		//var cnt =10;
		var cnt =[PTCSECOND];
		var counter123 = setInterval(function() {
			if (cnt>0) 
			{
				$('#displayCounter').html(" "+cnt+" ");
				cnt--;
				
				curcount=[PTCSECOND]-cnt;
				$('#progressbar div').width((curcount*100)/[PTCSECOND]+"%");
			}
			else 
			{
				clearInterval(counter123);
				document.getElementById("credittopframe").innerHTML="";
				$.ajax({
																								url: '[SITEURL]ptc/creditcounter',
																								type: 'POST',
																								data: {'id':[ID]},
																								success: function(data) {
																									$('.link1, .link2').css({'display':'block'});
						var member=document.getElementById("credittopframe");
						if(member!==null) member.innerHTML=data;
						window.opener.checkptc_ad([ID]);
					}
				});	
				
				/*var xmlhttp;    
				if (window.XMLHttpRequest)
				{// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				}
				else
				{// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function()
				{
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				  {
					$('.link1, .link2').css({'display':'block'});
					var member=document.getElementById("credittopframe");
					if(member!==null) member.innerHTML=xmlhttp.responseText;
					
					window.opener.checkptc_ad([ID]);
				  }
				}
				xmlhttp.open("GET","[SITEURL]ptc/creditcounter/[USERID]/[ID]/[MEMBERCLICKLIMIT]",true);
				xmlhttp.send();*/
				
			}
		},1000);
	}
	</script>
	[COUNTERSTART]
	</head>
	<body style="margin:0px;padding:0px;">
		<div id="wrapper">
			<div id="header">
				<div class="logo"><img src="[SITEURL]img/logo.png" width="266" height="56" /></div>
				<div class="content">
					<div id="credittopframe">
						<div id="progressbar"><div>&nbsp;</div></div>
						<div id="displayCounter">[PTCSECOND]</div>
						<div class="clearboth"></div>
					</div>
				</div>
				<div class="clearboth"></div>
				
				<div class="heught6"></div>
				<a target="_blank" class="link1" href="[TARGATURL]">[OPENNEWWINDOW]</a>
				<a class="link2" href="[SITEURL]member/index">[GOBACK]</a>
				<div class="clearboth"></div>
			</div>
		</div>
	[IFRAME]
	<script language="javascript">
		function getDocHeight() {	
		 var D = document;
			return Math.max(
				Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
				Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
				Math.max(D.body.clientHeight, D.documentElement.clientHeight)
			);
		}
		var x=getDocHeight();
		document.getElementById("f1").height=x-100;
	</script>
	</body>
</html>