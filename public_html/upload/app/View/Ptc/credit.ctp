<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($Enableptc==1) { ?>
<?php if(!$ajax){ ?>
<div id="Ptcpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('PTC').' '.__('Earning');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	
<?php // Search box code starts here ?>
<div class="floatleft searchlabel"><?php echo __("Advanced Search");?></div>
<div class="floatright"><a href="javascript:void(0)" onclick="togglebox('.searchlabel', this,'<?php echo __("[+] Show Search Box");?>','<?php echo __("[-] Hide Search Box");?>');"><?php echo __("[-] Hide Search Box");?></a></div>
<div class="clear-both"></div>
<div class="advsearchbox searchlabel">
<?php echo $this->Form->create('Ptc_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ptc','action'=>'credit')));?>
	<div class="searchboxrow">
		<div class="inlineblock">
			<span class="searchboxcol2"><?php echo __("From Date");?> :&nbsp;</span>
			<?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker', 'style'=>'width:100px; height:35px;'));?>
		</div>
		<div class="inlineblock margint5">
			<span class="searchboxcol2"><?php echo __("To Date");?> :&nbsp;</span>
			<?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker', 'style'=>'width:100px; height:35px;'));?>
		</div>
		
	</div>
	<div align="center"><div class="advsearchbtn" style="margin-bottom: 5px;">
		<?php echo $this->Js->submit(__('Update Results'), array(
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'update'=>'#Ptcpage',
			'class'=>'add-new',
			'div'=>false,
			'style'=>'float:none',
			'controller'=>'ptc',
			'action'=>'credit',
			'url'   => array('controller' => 'ptc', 'action' => 'credit')
		  ));?>
	</div></div>
<?php echo $this->Form->end();?>
</div>
<?php // Search box code ends here ?>

	<?php $this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#Ptcpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'ptc', 'action'=>'credit')
	));
	$currentpagenumber=$this->params['paging']['Ptc_history']['page'];
	?>
		<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		
		<?php // PTC Credits table starts here ?>
		<div class="divtable">
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam">
						<?php 
						if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
						echo $this->Js->link(__("Click Time"), array('controller'=>'ptc', "action"=>"credit/0/click_time/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Ptcpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Click Time')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Earning"), array('controller'=>'ptc', "action"=>"credit/0/amount/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Ptcpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Earning')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Type"), array('controller'=>'ptc', "action"=>"credit/0/type/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Ptcpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Type')
						));?>
					</div>
				</div>
			</div>
			<div class="divtbody">
				<?php $i=1;
				foreach ($Ptc_historydata as $Ptc_history):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $Ptc_history['Ptc_history']['click_time']); ?></div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($Ptc_history['Ptc_history']['amount']*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
						<div class="divtd textcenter vam">
						<?php if($Ptc_history['Ptc_history']['type']==0){ echo 'Click Credit';}else{ echo 'Referral Click Credit';}?>
						</div>
					</div>
				<?php $i++;endforeach; ?>
			</div>
		</div>
		<?php if(count($Ptc_historydata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
		<?php // PTC Credits table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Ptc_history']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Ptc_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ptc','action'=>'credit/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#Ptcpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'ptc',
						  'action'=>'credit/rpp',
						  'url'   => array('controller' => 'ptc', 'action' => 'credit/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
			</div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<div class="formbutton"> 
			<?php echo $this->Js->link(__("Back"), array('controller'=>'ptc', "action"=>"view"), array(
				'update'=>'#Ptcpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button',
				'style'=>'float:left'
			));?>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		
</div>

<?php // Do not edit below code ?>
<?php  if($ajax){?>
<script>
$(document).ready(function() {
	$("#Ptc_historyCreditForm .ui-datepicker-trigger").attr('src',$("#Ptc_historyCreditForm .ui-datepicker-trigger").attr('src').replace('../',''));
});
</script>
</div>
<?php } else { ?>
<script>
$(document).ready(function() {
	$("#Ptc_historyCreditForm .ui-datepicker-trigger").attr('src',$("#Ptc_historyCreditForm .ui-datepicker-trigger").attr('src').replace('../','../../'));
});
</script>
<?php } ?>
<?php // Do not edit above code ?>

<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>