<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
	<?php 
		echo $this->Html->css('scrollbar');
		echo $this->Javascript->link('jquery.mousewheel.min');
		echo $this->Javascript->link('scrollbar');
	?>
	<script type="text/javascript">
	$(document).ready(function() {
		$(".ptcboxmiddle").mCustomScrollbar({
			scrollInertia:500,
			theme:"dark-thin"
		});
	});
	</script>

<div id="Ptcpage">
<?php echo $this->Javascript->link('allpage');?>
<?php if($Enableptc==1) { ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<?php // Top menu code starts here ?>
<div class="floatright">
	<?php echo $this->Js->link(__("PTC").' '.__("Earning"), array('controller'=>'ptc', "action"=>"credit"), array(
		'update'=>'#Ptcpage',
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'escape'=>false,
		'class'=>'button marginright5'
	));?>
</div>
<div class="clear-both"></div>
<?php // Top menu code ends here ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('View PTC Ads');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran" style="text-align: center;">
<script type="text/javascript">
function checkptc_ad(id)
{
	document.getElementById("ptcbannerid"+id).style.opacity = 0.3;
	totalclickcounter();
	$('#viewbanner_overlay').height(0);
	$('#viewbanner_overlay').width(0);
}
function totalclickcounter()
{
	$.ajax({
		type: "POST",
		url: "todayclick",
		data: {func:"totalclickcounter"},
		cache: false
		}).done(function(msg) {
			$("#totalclickcounter").html(msg);
		});
}
totalclickcounter();
function viewbanner()
{
	$('#viewbanner_overlay').height($('#viewbanner_ad').height());
	$('#viewbanner_overlay').width($('#viewbanner_ad').width());
}
</script>
	<div align="left"><?php echo __("Today's Total Clicks");?> : <span id="totalclickcounter"></span>
	<?php if($memberclicklimit>0){echo " | ".__("Clicks allowed per day").' : '.$memberclicklimit;}?></div>
	<div class="height5"></div>
	<div id="viewbanner_ad">
	<div id="viewbanner_overlay">&nbsp;</div>
	<?php // View PTC list starts here ?>
	<?php foreach ($ptcbanners as $ptcbanner): ?>
	
	<div id="ptcbannerid<?php echo $ptcbanner['Ptcbanner']['id'];?>" class="mainptcbox" style="border-color:<?php echo $ptcbanner['Ptcdesign']['bordercolor'];?>">
	
		<div class="ptcbackground" style="<?php echo ($ptcbanner['Ptcdesign']['dtype']==1)?$ptcbanner['Ptcdesign']['design']:'background:url(\''.$SITEURL."img/ptc/background/".$ptcbanner['Ptcdesign']["design"].'\');'.$ptcbanner['Ptcdesign']["imagecss"];?>">
			<div class="ptcavatarbox"><?php if($ptcbanner['Member']['member_photo']==NULL || $ptcbanner['Member']['member_photo']=='') echo $this->html->image("member/dummy.jpg", array('class'=>'profilepicture','alt'=>'','id'=>'memberimg','width'=>'80','height'=>'80')); else echo $this->html->image("member/".$ptcbanner['Member']['member_photo'].'?tk='.date('sHi'), array('class'=>'profilepicture','alt'=>'','id'=>'memberimg','width'=>'80','height'=>'80'));?></div>
			<div class="ptcboxtitle" style="<?php echo $ptcbanner['Ptcdesign']['titlecss'];?>"><?php echo $ptcbanner['Ptcbanner']['banner_title'];?></div>
			<div class="clear-both"></div>
		</div>
		<div class="ptccontent">
			<div class="ptcusername"><?php echo $ptcbanner['Member']['user_name'];?></div>
			<div class="ptcstats">
				<span class="ptcearning" ptc-tooltip="<?php echo __('Earning');?>"><span class="glyphicon glyphicon-usd"></span><?php echo $Currency['prefix'];?><?php echo round($ptcbanner['Ptcplan']['clickcost']*$Currency['rate'],6)." ".$Currency['suffix'];?></span>
				<span class="ptcclicks" ptc-tooltip="<?php echo __('Clicks');?>"><span class="glyphicon glyphicon-globe"></span><?php echo $ptcbanner['Ptcbanner']['click_counter'];?></span>
				<?php echo $this->Js->link('<span class="ptclikes" ptc-tooltip="'.__('Likes').'"><span class="glyphicon glyphicon-thumbs-up"></span>'.$ptcbanner['Ptcbanner']['like_counter'].'</span>', array('controller'=>'ptc', "action"=>"like/".$ptcbanner['Ptcbanner']['id']), array(
					'update'=>'#ptclick'.$ptcbanner['Ptcbanner']['id'],
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'id'=>'ptclick'.$ptcbanner['Ptcbanner']['id']
				));?>
				<span class="ptcviews" ptc-tooltip="<?php echo __('Views'); ?>"><span class="glyphicon glyphicon-signal"></span><?php echo $ptcbanner['Ptcbanner']['display_counter'];?></span>
			</div>
			<div class="clear-both"></div>
			<hr />
			<div class="ptcboxmiddle">
				<?php
				if($ptcbanner['Ptcbanner']['banner_url']!=''){
					if($ptcbanner['Ptcbanner']['banner_size']=='125x125')
					{
						echo '<div class="onetwofivebox"><img src="'.$ptcbanner['Ptcbanner']['banner_url'].'" alt="" width="111px" height="111px" style="vertical-align: middle;padding-right:10px;" /></div>';
					}
					elseif($ptcbanner['Ptcbanner']['banner_size']=='468x60')
					{
						echo '<img src="'.$ptcbanner['Ptcbanner']['banner_url'].'" alt="" />';
					}
					elseif($ptcbanner['Ptcbanner']['banner_size']=='728x90')
					{
						echo '<img src="'.$ptcbanner['Ptcbanner']['banner_url'].'" alt="" />';
					}
				}
				?>
				<p <?php if($ptcbanner['Ptcbanner']['banner_size']=='125x125'){echo 'class="onetwofivetext"';}?>><?php echo nl2br(substr(stripslashes($ptcbanner['Ptcbanner']['banner_description']), 0, 500));?></p>
			</div>
			<div class="ptcboxbottom" style="<?php echo $ptcbanner['Ptcdesign']['buttoncss'];?>"><a href="<?php echo str_replace("https://", "http://", $SITEURL);?>ptc/counter/<?php echo $ptcbanner['Ptcbanner']['id'];?>" target="_blank" onclick="viewbanner();"><?php echo __('Click here to earn'); ?></a></div>
		</div>
	</div>
	<?php endforeach; ?>
	</div>
	<?php if(count($ptcbanners)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
	<?php // View PTC list ends here ?>
	<div class="clear-both"></div>
</div>
<?php } else { echo __('This page is disabled by administrator'); } ?>
</div><!--#Ptcpage over-->
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>