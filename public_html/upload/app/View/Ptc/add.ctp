<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('framechecker');?>
<?php if($Enableptc==1) { ?>
<?php if(!$ajax){ ?>
<div id="Ptcpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div class="height5"></div>
<div id="UpdateMessage"></div>

<?php //Frame Break Code Start ?>
<input type="hidden" id='framebreakmessage' value='<?php echo __('Destination URL Not Valid'); ?>|<?php echo __('Breaking out of frames'); ?>|<?php echo __('Inspecting Website'); ?>|<?php echo __('Framebreaker test passed'); ?>|<?php echo __('Error').' : '; ?>'/>
<div id="check" class="frambreakmain">
	<div class='textleft'>
		<span id='framebreakimg'></span>
		<span class="framebreaktext"><?php echo __('Inspecting Framebreaker'); ?></span>
	</div>
	<div class='textleft'>
		<span id="check_final"><?php echo __('Inspecting Website'); ?></span>
	</div>
</div>
<?php //Frame Break Code Over ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("PTC");?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
		<?php // Edit PTC form starts here ?>
		<?php echo $this->Form->create('Ptcbanner',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'ptc','action'=>'addaction')));?>
		<div class="form-box">
			<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$Ptcbannerdata['Ptcbanner']["id"], 'label' => false)); ?>
			<div class="form-row">
				<div class="form-col-1 form-text"><?php echo __("Plan Name");?> : </div>
				<div class="form-col-2 form-text"><?php echo $Ptcbannerdata['Ptcplan']['plan_name']; ?></div>
			</div>
			<div class="form-row">
				<div class="form-col-1 form-text"><?php echo __("Purchase Date");?> : </div>
				<div class="form-col-2 form-text"><?php echo $this->Time->format($SITECONFIG["timeformate"], $Ptcbannerdata['Ptcbanner']['purchasedate']); ?></div>
			</div>
			<div class="form-row">
				<div class="form-col-1 form-text"><?php echo __("Display Start Date");?> : </div>
				<div class="form-col-2 form-text"><?php echo $this->Time->format($SITECONFIG["timeformate"], $Ptcbannerdata['Ptcbanner']['start_date']); ?></div>
			</div>
			<?php if($Ptcbannerdata['Ptcbanner']['banner_size']!="") { ?>
			<div class="form-row">
				<div class="form-col-1 form-text"><?php echo __("Banner Size");?> : </div>
				<div class="form-col-2 form-text"><?php echo $Ptcbannerdata['Ptcbanner']['banner_size']; ?></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Banner URL");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('banner_url', array('type'=>'text', 'label' => false, 'div' => false, 'value'=>$Ptcbannerdata['Ptcbanner']['banner_url'], 
				'onblur'=>'if(this.value!==""){document.getElementById("img1").src = this.value;$("#img1").show();}else{$("#img1").hide();}',
				'class'=>'formtextbox'));?>
				
				<div class="textcenter"><div class="form-col-1 forbanneronly"></div><div class="bannerdisplay"><img src="<?php echo $Ptcbannerdata['Ptcbanner']['banner_url']; ?>" id="img1"/></div></div>
			</div>
			<?php } ?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Banner Title");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('banner_title', array('type'=>'text','div'=>false, 'label' => false, 'value'=>$Ptcbannerdata['Ptcbanner']['banner_title'], 'class'=>'formtextbox login-from-box-1'));?>
				
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?>"></span>
				
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Destination URL");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('target_url', array('type'=>'text', 'label' => false, 'div' => false, 'value'=>$Ptcbannerdata['Ptcbanner']['target_url'], 'class'=>'formtextbox txtframebreaker'));?>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Daily Clicks Limit");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('daily_budget', array('type'=>'text', 'label' => false, 'value'=>$Ptcbannerdata['Ptcbanner']['daily_budget'], 'class'=>'formtextbox login-from-box-1', 'div' => false));?>
				<span class="helptooltip vtip" title="<?php echo __('Specify the maximum possible clicks per day for this ad. Ad will not be shown on View PTC Ads page once this number is reached.'); ?>"></span>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Description");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('banner_description', array('type'=>'textarea', 'label'=>false, 'div' => false, 'title'=>__('Description'), 'value'=>stripslashes($Ptcbannerdata['Ptcbanner']['banner_description']), 'class'=>'formtextarea '));?>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Theme");?> : </div>
				
					<?php $ptcdesignlist=array();$jsstring='var typearray=new Array();var themearray=new Array();';
					foreach ($ptcdesigns as $ptcdesign):
						$ptcdesignlist[$ptcdesign['Ptcdesign']['id']]=$ptcdesign['Ptcdesign']['dname'];
						$jsstring.='typearray['.$ptcdesign['Ptcdesign']['id'].'] = '.$ptcdesign['Ptcdesign']['dtype'].';';
						$jsstring.='themearray['.$ptcdesign['Ptcdesign']['id'].'] = "'.$ptcdesign['Ptcdesign']['design'].'";';
					endforeach;
					?>
				<div class="select-dropdown">
					<label>
						<?php
						echo $this->Form->input('ptctheme', array(
							'type' => 'select',
							'options' => $ptcdesignlist,
							'selected' => $Ptcbannerdata['Ptcbanner']['ptctheme'],
							'class'=>'searchcomboboxwidth',
							'label' => false,
							'div' => false,
							'onchange' => 'previewdesign(this.value)'
						));
						?>
					</label>
				</div>
			<style>#themepriview{height: 110px;width: 454px;}</style>
			<script type="text/javascript">
				function previewdesign(objval)
				{
					<?php echo $jsstring;?>
					if(typearray[objval]==1)
						$("#themepriview").attr('style',themearray[objval]).html("");
					else
						$("#themepriview").html('<img src="<?php echo $SITEURL."img/ptc/background/";?>'+themearray[objval]+'" />');
				}
				$( document ).ready(function() {
					previewdesign($("#PtcbannerPtctheme").val());
					
				});
			</script>
			
			<div class="form-row priviewfield">
				<div class="form-col-1 notmobile"></div>
				<div class="form-col-2 fullwidthonmobile">
					<div id="themepriview" style="height:110px;"></div>
				</div>
			</div>
			<?php if($Ptcbannerdata['Ptcbanner']['display_area']=='all'){?>
			<div class="form-row">
				<div class="form-col-1 form-text"><?php echo __("Display Area");?> : <span class="required">*</span></div>
				<div class="form-col-2 form-text">
					<?php echo __("All Countries");?>
				</div>
			</div>
			<?php } else {?>
			<div class="tabal-content-gray">
				<fieldset>
					<div style="height:250px;overflow:auto;">
						<?php
						$ccounter=0;
						$replacecountry=array('India','Dominica','Guinea','Mali','Netherlands','Niger','Oman','Samoa','Curacao');
						$country=array('India_2','Dominica_2','Guinea_2','Mali_2','Netherlands_2','Niger_2','Oman_2','Samoa_2','Netherlands Antilles');
						$display_area=str_replace($country,$replacecountry,$Ptcbannerdata['Ptcbanner']["display_area"]);
						
						$countrynames=@explode(",", $display_area);
						foreach($countries as $id => $val)
						{
							if(in_array(trim($val), $countrynames)){$selected = 'checked';}else{$selected = '';} ?>
							<div class="line-height float-left-profile" style="width:215px;">
								<div class="line-height profile-bot-left" style="padding-top:0px;">
									<?php 
										echo $this->Form->checkbox('country.', array(
										  'value' => trim($val),
										  'class' => '',
										  'div'=>false,
										  'checked'=>$selected,
										  'style'=>'display:inline',
										  'hiddenField' => false
										));
									?>
								</div>
								<div class="line-height float-left-profile"><?php echo trim($val); ?></div>
							</div>
						<?php	$ccounter++;
						if($ccounter%3==0)
						{
							echo '<div class="clear-both"></div>';
						}
					 }?>
					</div>
				</fieldset>
			</div>
			<?php } ?>
		</div>
		
		<div class="height10"></div>
		<div class="formbutton">
			<?php echo $this->Js->link(__("Back"), array('controller'=>'ptc', "action"=>"index"), array(
				'update'=>'#Ptcpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button',
				'style'=>'float:left'
			));?>
		
			<input type="submit" value="<?php echo __('Update'); ?>" class="button" onclick="return checkframebreaker(0,'<?php echo $SITEURL; ?>','framebreaker');" />
			<?php echo $this->Js->submit(__('Update'), array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'button framebreaker',
				'style'=>'display:none',
				'div'=>false,
				'controller'=>'ptc',
				'onfocus'=>'',
				'action'=>'addaction',
				'url'   => array('controller' => 'ptc', 'action' => 'addaction')
			));?>
		</div>
		<div class="clear-both"></div>
		<?php echo $this->Form->end();?>
		<?php // Edit PTC form ends here ?>
		
	</div>
	<div class="height10"></div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>