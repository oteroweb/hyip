<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('framechecker');?>
<?php if($Enableurlshortener==1) { ?>
<?php if(!$ajax){ ?>
<div id="urlshortenerpage">
<?php } ?>
<div id="UpdateMessage"></div>

<?php //Frame Break Code Start ?>
<input type="hidden" id='framebreakmessage' value='<?php echo __('Target URL Not Valid'); ?>|<?php echo __('Breaking out of frames'); ?>|<?php echo __('Inspecting Website'); ?>|<?php echo __('Framebreaker test passed'); ?>|<?php echo __('Error').' : '; ?>'/>
<div id="check" class="frambreakmain">
	<div class='textleft'>
		<span id='framebreakimg'></span>
		<span class="framebreaktext"><?php echo __('Inspecting Framebreaker'); ?></span>
	</div>
	<div class='textleft'>
		<span id="check_final"><?php echo __('Inspecting Website'); ?></span>
	</div>
</div>
<?php //Frame Break Code Over ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("URL Shortener");?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
		<?php // URL Shortener add form starts here ?>
		<?php echo $this->Form->create('Shortener',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'urlshortener','action'=>'addaction')));?>
		<div class="form-box">
			<?php if(isset($urlshortenerdata['Shortener']["id"])){
				echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$urlshortenerdata['Shortener']["id"], 'label' => false));
				echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
			}?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Title");?> : <span class="required">*</span></div>
				<div class="form-col-2">
					<?php echo $this->Form->input('title', array('type'=>'text', 'label' => false, 'value'=>@$urlshortenerdata['Shortener']['title'], 'class'=>'formtextbox','div'=>false));?>
					<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?>"></span>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Target URL");?> : <span class="required">*</span></div>
				<div class="form-col-2">
					<?php echo $this->Form->input('target', array('type'=>'text', 'label' => false, 'value'=>isset($urlshortenerdata['Shortener']['target']) ? $urlshortenerdata['Shortener']['target'] : 'http://', 'class'=>'formtextbox txtframebreaker','div' => false));?>
				</div>
			</div>
		</div>
		<div class="formbutton">
			<?php echo $this->Js->link(__("Back"), array('controller'=>'urlshortener', "action"=>"index"), array(
				'update'=>'#urlshortenerpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button',
				'style'=>'float:left'
			));?>
			<input type="submit" value="<?php echo __('Submit'); ?>" class="button" onclick="return checkframebreaker(0,'<?php echo $SITEURL; ?>','framebreaker');" />
			<?php echo $this->Js->submit(__('Submit'), array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'button framebreaker',
				'style'=>'display:none',
				'div'=>false,
				'controller'=>'urlshortener',
				'action'=>'addaction',
				'url'   => array('controller' => 'urlshortener', 'action' => 'addaction')
			));?>
		</div>
		<div class="clear-both"></div>
		<?php echo $this->Form->end();?>
		<?php // URL Shortener add form ends here ?>
		
	</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>	
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>