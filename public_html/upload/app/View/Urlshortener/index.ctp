<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($Enableurlshortener==1) { ?>
<?php if(!$ajax){ ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="urlshortenerpage">	
<?php } ?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('URL Shortener');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#urlshortenerpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'urlshortener', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Shortener']['page'];
	?>
		<div class="padding-left-serchtabal">
			<?php echo $this->Js->link(__("Add New"), array('controller'=>'urlshortener', "action"=>"add"), array(
			'update'=>'#urlshortenerpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'button'
		));?>
		</div>
		<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			
			<?php // URL Shortener table starts here ?>
			<div class="divtable textcenter">
				<div class="divthead">
					<div class="divtr tabal-title-text" >
						<div class="divth textcenter vam">
							<?php 
							if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
							echo $this->Js->link(__("Id"), array('controller'=>'urlshortener', "action"=>"index/0/id/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#urlshortenerpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Id')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Title"), array('controller'=>'urlshortener', "action"=>"index/0/title/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#urlshortenerpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Title')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("URL"), array('controller'=>'urlshortener', "action"=>"index/0/tinyurl/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#urlshortenerpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('URL')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Clicked"), array('controller'=>'urlshortener', "action"=>"index/0/clicks/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#urlshortenerpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Clicked')
							));?>
						</div>
						<div class="divth textcenter vam"><?php echo __("Action"); ?></div>
					</div>
				</div>
				<div class="divtbody">
					<?php $i=1;
					foreach ($urlshortenerdata as $urlshortener):
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam"><?php echo $urlshortener['Shortener']['id'];?></div>
							<div class="divtd textcenter vam"><?php echo $urlshortener['Shortener']['title'];?></div>
							<div class="divtd textcenter vam">
								<b><?php echo __("Tiny URL");?></b> : <a href="<?php echo str_replace("https://", "http://", $SITEURL).'x/u'.$urlshortener['Shortener']['tinyurl'];?>" target="_blank"><?php echo str_replace("https://", "http://", $SITEURL).'x/u'.$urlshortener['Shortener']['tinyurl'];?></a><br />
								<b><?php echo __("Target URL");?></b> : <?php echo $urlshortener['Shortener']['target'];?>
							</div>
							<div class="divtd textcenter vam"><?php echo $urlshortener['Shortener']['clicks'];?></div>
							<div class="divtd textcenter vam">
								
								<div class="actionmenu">
								      <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
									        <li>
										     <?php
											if($urlshortener['Shortener']['pause']==0){
												$statusaction='1';
												$statusicon='pause.png';
												$statustext='Unpause Shortener';
											}else{
												$statusaction='0';
												$statusicon='play.png';
												$statustext='Pause Shortener';}
											echo $this->Js->link($this->html->image($statusicon, array('alt'=>__($statustext)))." ".__($statustext), array('controller'=>'urlshortener', "action"=>"status/".$statusaction."/".$urlshortener['Shortener']['id']."/".$currentpagenumber), array(
												'update'=>'#urlshortenerpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));
											?>
									      </li>
									      <li>
										      <?php
											if($urlshortener['Shortener']['status']==0){
												$statusicon='red-icon.png';
												$statustext='Inactive Shortener';
											}else{
												$statusicon='blue-icon.png';
												$statustext='Active Shortener';}
											echo '<a>'.$this->html->image($statusicon, array('alt'=>__($statustext),'class'=>'','title'=>__($statustext),'style'=>'vertical-align:top;'));
											echo " ".__($statustext)."</a>"
											?>
									      </li>
									      <li>
											<?php
											echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit Shortener')))." ".__('Edit Shortener'), array('controller'=>'urlshortener', "action"=>"add/".$urlshortener['Shortener']['id']), array(
												'update'=>'#urlshortenerpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));
											?>
									      </li>
									      <li>
											<?php
											echo $this->Js->link($this->html->image('delete.png', array('alt'=>__('Delete Shortener')))." ".__('Delete Shortener'), array('controller'=>'urlshortener', "action"=>"remove/".$urlshortener['Shortener']['id']."/".$currentpagenumber), array(
												'update'=>'#urlshortenerpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>'',
												'confirm'=>__("Do you really want to delete this shortener?")
											));
											?>
									      </li>
									 </ul>
								      </div>
								</div>
								
								
							</div>
						</div>
					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($urlshortenerdata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // URL Shortener table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Shortener']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Shortener',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'urlshortener','action'=>'index/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#urlshortenerpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'urlshortener',
						  'action'=>'index/rpp',
						  'url'   => array('controller' => 'urlshortener', 'action' => 'index/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
			</div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		
</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>