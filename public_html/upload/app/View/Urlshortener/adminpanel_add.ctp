<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">URL Shortener</div>
<div id="urlshortenerpage">
<?php }?>
<div id="UpdateMessage" class="UpdateMessage"></div>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/URL_Shortener" target="_blank">Help</a></div>
<div class="backgroundwhite">
	
		<?php echo $this->Form->create('Shortener',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'urlshortener','action'=>'addaction')));?>
	<?php if(isset($urlshortenerdata['Shortener']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$urlshortenerdata['Shortener']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
		
	<div class="frommain">
	
		<div class="fromnewtext">Status :</div>
		<div class="fromborderdropedown3">
			<div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('status', array(
						'type' => 'select',
						'options' => array('1'=>'Active', '0'=>'Inactive'),
						'selected' => $urlshortenerdata['Shortener']["status"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
			</div>
        </div>
		
		<div class="fromnewtext">Tiny URL : </div>
		<div class="fromborderdropedown4">
		<a href="<?php echo $SITEURL.'x/u'.$urlshortenerdata['Shortener']['tinyurl'];?>" target="_blank" style="margin-left:9px;"><?php echo $SITEURL.'x/u'.$urlshortenerdata['Shortener']['tinyurl'];?></a>
		</div>

		<div class="fromnewtext">Title : <span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('title', array('type'=>'text', 'value'=>$urlshortenerdata['Shortener']["title"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Target URL : <span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('target', array('type'=>'text', 'value'=>$urlshortenerdata['Shortener']["target"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="formbutton">
				<?php echo $this->Js->submit('Submit', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'btnorange',
				'div'=>false,
				'controller'=>'urlshortener',
				'action'=>'urlshortenerurladdaction',
				'url'   => array('controller' => 'urlshortener', 'action' => 'addaction')
			  ));?>
			  
			  <?php echo $this->Js->link("Back", array('controller'=>'urlshortener', "action"=>"url"), array(
				  'update'=>'#urlshortenerpage',
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'escape'=>false,
				  'class'=>'btngray'
			  ));?>
		</div>
		
	</div>
	<?php echo $this->Form->end();?>
	
	
</div>
<?php if(!$ajax){?>
</div><!--#balancetransferpage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>