<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="promotoolspage">
<script>
function Dynamicbox(atag)
{
	var popwid="60%";
	if($(window).width()<700)
		var popwid="100%";
	$("a[rel='lightboxtext']").colorbox({width:popwid, height:"77%", iframe:true});
}
</script>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Splash Pages');?></div>
	<div class="clear-both"></div>
</div>
<?php echo $this->Javascript->link('allpage');?>

<?php // Splash pages list starts here ?>
<div class="main-box-eran textcenter">
	<?php
	foreach ($splashpagedata as $splash): ?>
		<div class="fixcontentbox">
			<a href="showsplashpage/<?php echo $splash['Splashpage']['id'];?>" rel="lightboxtext" onclick="Dynamicbox(this)">
				<div class="imagetextbox">
					<?php
					if($splash['Splashpage']['photo']!='')
						echo $this->html->image('splashpages/'.$splash['Splashpage']['photo'], array('alt'=>'', 'align' =>'absmiddle')); 
					else
						echo $this->html->image('splashpages/splashpage.jpg', array('alt'=>'', 'align' =>'absmiddle')); 
					?>
					<span class="cover"></span>
					<span class="text"><?php echo __('Show Details'); ?></span>
				</div>
			</a>
		</div>
	<?php endforeach; ?>
	<?php if(count($splashpagedata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
	<div class="clear-both"></div>
</div>
<?php // Splash pages list ends here ?>

</div>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>