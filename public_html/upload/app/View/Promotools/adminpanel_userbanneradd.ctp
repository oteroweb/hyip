<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<script type="text/javascript"> 
(function() {
$('#Userbanneradd').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();
</script>
<?php if(!$ajax){?>
<div class="whitetitlebox">Promo Tools / Banners</div>
<div id="promotoolspage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Banners" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<div class="backgroundwhite">
		
<?php echo $this->Form->create('Userbanner',array('type' => 'post', 'id'=>'Userbanneradd', 'type' =>'file', 'onsubmit' => 'return true;','url'=>array('controller'=>'promotools','action'=>'userbanneraddaction')));?>
	<?php if(isset($userbannerdata['Userbanner']["banner_id"])){
		echo $this->Form->input('banner_id', array('type'=>'hidden', 'value'=>$userbannerdata['Userbanner']["banner_id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		echo $this->Form->input('current_style', array('type'=>'hidden', 'value'=>$userbannerdata['Userbanner']["style"], 'label' => false));
	}?>
		
	<div class="frommain">
		<div class="height10"></div>	
		<div class="textcenter">
			<?php 
			if($userbannerdata['Userbanner']['is_upload']==1)	
				echo $this->html->image('banners/'.$userbannerdata['Userbanner']['photo'], array('alt'=>'', 'align' =>'absmiddle')); 
			else
				echo '<img src="'.$userbannerdata['Userbanner']['photo'].'" alt="" />';
			?>
		</div>
	
		<div class="fromnewtext">Status :</div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
					<?php 
						echo $this->Form->input('status', array(
							'type' => 'select',
							'options' => array('1'=>'Active', '0'=>'Inactive'),
							'selected' => $userbannerdata['Userbanner']["status"],
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));
					?>
				</label>
		   </div>
		</div>
		
		<div class="fromnewtext">Title :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('title', array('type'=>'text', 'value'=>$userbannerdata['Userbanner']["title"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Upload banner : </div>
		<div class="fromborderdropedown3">
			<?php 
			if($userbannerdata['Userbanner']["is_upload"]){$checked="checked";$styletext="style='display:none'";$stylefile="";}else{$checked="";$styletext="";$stylefile="style='display:none'";}
			echo $this->Form->input('is_upload', array('type' => 'checkbox', 'div'=>true, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if(this.checked==true){$(".bannerfilefields").show(500);$(".bannertextfields").hide(500);}else{$(".bannertextfields").show(500);$(".bannerfilefields").hide(500);}'));
			?>
		</div>
		
		
		<div class="bannertextfields" <?php echo $styletext;?>>
			<div class="fromnewtext">Banner URL :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('photo', array('type'=>'text', 'value'=>$userbannerdata['Userbanner']["photo"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
		</div>
		
		<div class="bannerfilefields" <?php echo $stylefile;?>>
			<div class="fromnewtext">Select Banner : <span class="red-color">*</span></div>
			<div class="fromborderdropedown3">
			  <div class="btnorange browsebutton">Browse<?php echo $this->Form->input('bannerfile', array('type'=>'file', 'label' => false, 'div' => false,'class'=>'browserbuttonlink'));?></div>
			</div>
		</div>
		
		<div class="fromnewtext">Size :</div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			<label>
				<?php 
					echo $this->Form->input('style', array(
						'type' => 'select',
						'options' => array('728x90'=>'728X90', '468x60'=>'468X60', '234x60'=>'234X60', '120x240'=>'120X240', '125x125'=>'125X125', '120x600'=>'120X600','160x600'=>'160X600', '180x50'=>'180X50', '200x200'=>'200X200', '250x250'=>'250X250', '300x250'=>'300X250', '336x280'=>'336X280', '600x300'=>'600X300'),
						'selected' => $userbannerdata['Userbanner']["style"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			</label>
		 </div>
		</div>
		
		<div class="formbutton">
			<?php echo $this->Form->submit('Submit', array(
				'class'=>'btnorange',
				'div'=>false
			));?>
			<?php echo $this->Js->link("Back", array('controller'=>'promotools', "action"=>"index"), array(
				  'update'=>'#promotoolspage',
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'escape'=>false,
				  'class'=>'btngray'
			));?>
		</div>
	</div>
	  
<?php echo $this->Form->end();?>

</div>
<?php if(!$ajax){?>
</div><!--#promotoolspage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>