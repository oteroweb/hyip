<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Promo Tools / Promotional Emails</div>
<div id="promotoolspage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Promotional_Emails" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>

<div class="backgroundwhite">
				
		<?php echo $this->Form->create('Emailad',array('type' => 'post', 'id'=>'EmailadForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'promotools','action'=>'emailadaddaction')));?>
		<?php if(isset($emailads['Emailad']["email_id"])){
			echo $this->Form->input('email_id', array('type'=>'hidden', 'value'=>$emailads['Emailad']["email_id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}?>
		<div class="frommain">
			
			<div class="fromnewtext">Subject :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('subject', array('type'=>'text', 'value'=>stripslashes($emailads['Emailad']["subject"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
		
			<div class="fromnewtext">Message :<span class="red-color">*</span></div>
			<div class="frombordermain">
				<?php echo $this->Form->input('message', array('type'=>'textarea', 'value'=>stripslashes($emailads['Emailad']['message']),'label' => false, 'div' => false,  'class'=>'from-textarea', 'id'=>'EmailadContent'));?>
			</div>
			
			<div class="fromnewtext">Mail Template Fields : </div>
			<div class="fromborderdropedown3">
				<?php foreach($tagdata as $tag){?>
					<div class="linktotext"><a onClick="AddTags(document.getElementById('EmailadContent'),'[<?php echo $tag['Template_tag']['tag_name'];?>]');" title="Click to Paste" href="javascript:void(0);"><?php echo '['.$tag['Template_tag']['tag_name'].']';?></a><?php echo ' - '.$tag['Template_tag']['tag_desc'];?></div>
				<?php }?>
			</div>
				
			<div class="formbutton">
				<?php echo $this->Js->submit('Update', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#UpdateMessage',
					'class'=>'btnorange',
					'div'=>false,
					'controller'=>'promotools',
					'action'=>'emailadaddaction',
					'url'   => array('controller' => 'promotools', 'action' => 'emailadaddaction')
				));?>
				<?php 
				echo $this->Js->link("Back", array('controller'=>'promotools', "action"=>"emailads"), array(
					'update'=>'#promotoolspage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btngray'
				));?>
			</div>
			
		</div>
		<?php echo $this->Form->end();?>
    
</div>
<?php if(!$ajax){?>
</div><!--#promotoolspage over-->
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>