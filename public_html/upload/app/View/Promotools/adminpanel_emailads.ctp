<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Promo Tools / Promotional Emails</div>
<div id="promotoolspage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Promotional_Emails" target="_blank">Help</a></div>
<script>
function ViewDescription(atag)
{
	$("a[rel='lightboxtext']").colorbox({width:"80%", height:"80%", inline:true, href:"#"+atag.title});
}
</script>
<div id="UpdateMessage"></div>

        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#promotoolspage',
            'evalScripts' => true,
            'url'=> array('controller'=>'promotools', 'action'=>'emailads')
        ));
        $currentpagenumber=$this->params['paging']['Emailad']['page'];
        ?>

<div id="Xgride-bg">
	<div class="height10"></div>
    <div class="padding10 backgroundwhite">
		<div class="greenbottomborder">
		<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">		
        <?php if(!isset($SubadminAccessArray) || in_array('promotools',$SubadminAccessArray) || in_array('promotools/adminpanel_emailadadd',$SubadminAccessArray)){ ?>
                <?php echo $this->Js->link("+ Add New", array('controller'=>'promotools', "action"=>"emailadadd"), array(
                    'update'=>'#promotoolspage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'class'=>'btnorange'
                ));?>
        <?php } ?>
	</div>
	<div class="clear-both"></div>
		
		<div class="tablegrid">
				<div class="tablegridheader">
				    <div>
                        <?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('Email Ad Id', array('controller'=>'promotools', "action"=>"emailads/0/0/email_id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#promotoolspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Email Ad Id'
                        ));?>
                    </div>
                    <div>
                        <?php 
                        echo $this->Js->link('Subject', array('controller'=>'promotools', "action"=>"emailads/0/0/subject/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#promotoolspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Subject'
                        ));?>
                    </div>
				    <div><?php echo "Email Ad";?></div>
                    <div><?php echo 'Action';?></div>
                </div>
                <?php foreach ($emailads as $emailad): ?>
                    <div class="tablegridrow">
			<div><?php echo $emailad['Emailad']['email_id']; ?></div>
                        <div><?php echo stripslashes($emailad['Emailad']['subject']); ?></div>
                        <div>
                            <a href="#" rel="lightboxtext" title="ViewDescription-<?php echo $emailad['Emailad']['email_id'];?>" onclick="ViewDescription(this)">
							<?php echo $this->html->image('search.png', array('alt'=>'Notes', 'align'=>'absmiddle'));?>
							</a>
							<span style="display:none;">
								<span id="ViewDescription-<?php echo $emailad['Emailad']['email_id'];?>"><?php echo nl2br(stripslashes($emailad['Emailad']['message'])); ?></span></span>
                        </div>
				        <div>
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
												
												<?php if(!isset($SubadminAccessArray) || in_array('promotools',$SubadminAccessArray) || in_array('promotools/adminpanel_emailadadd/$',$SubadminAccessArray)){?>
												<li>
												<?php
													echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Emailad'))." Edit", array('controller'=>'promotools', "action"=>"emailadadd/".$emailad['Emailad']['email_id']), array(
														'update'=>'#promotoolspage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>''
													));?>
												</li>
												<?php }?>
										
												<?php if(!isset($SubadminAccessArray) || in_array('promotools',$SubadminAccessArray) || in_array('promotools/adminpanel_emailadstatus',$SubadminAccessArray)){?>
												<li>
												<?php
													if($emailad['Emailad']['status']==0)
													{
														$field_reqaction='1';
														$field_reqicon='red-icon.png';
														$field_reqtext='Activate';
													}
													else
													{
														$field_reqaction='0';
														$field_reqicon='blue-icon.png';
														$field_reqtext='Inactivate';
													}
													echo $this->Js->link($this->html->image($field_reqicon, array('alt'=>$field_reqtext))." ".$field_reqtext, array('controller'=>'promotools', "action"=>"emailadstatus/".$field_reqaction."/".$emailad['Emailad']['email_id']."/".$currentpagenumber), array(
														'update'=>'#promotoolspage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>''
													));?>
												</li>
												<?php }?>
												
												<?php if(!isset($SubadminAccessArray) || in_array('promotools',$SubadminAccessArray) || in_array('promotools/adminpanel_emailadremove',$SubadminAccessArray)){?>
												<li>
												<?php
													echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Emailad'))." Delete", array('controller'=>'promotools', "action"=>"emailadremove/".$emailad['Emailad']['email_id']."/".$currentpagenumber), array(
														'update'=>'#promotoolspage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'confirm'=>'Are You Sure?',
														'class'=>''
													));?>
												</li>
												<?php }?>
									</ul>
								  </div>
								</div>
                        </div>
                    </div>
                <?php endforeach; ?>
				
		</div>
		<?php if(count($emailads)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php
        if($this->params['paging']['Emailad']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Emailad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'promotools','action'=>'emailads/0/rpp')));?>
			<div class="resultperpage">
				<label>
					<?php 
					echo $this->Form->input('resultperpage', array(
					  'type' => 'select',
					  'options' => $resultperpage,
					  'selected' => $this->Session->read('pagerecord'),
					  'class'=>'',
					  'label' => false,
					  'div'=>false,
					  'style' => '',
					  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
					));
					?>
				</label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#promotoolspage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'promotools',
                  'action'=>'emailads/0/rpp',
                  'url'   => array('controller' => 'promotools', 'action' => 'emailads/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
		</div>
    </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#promotoolspage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>