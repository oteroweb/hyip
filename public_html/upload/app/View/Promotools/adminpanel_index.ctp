<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Promo Tools / Banners</div>
<div id="promotoolspage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Banners" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Userbanner',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'promotools','action'=>'index')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
				if($searchby=='style'){$searchforstyle="style='display:none'";$searchfordropdownstyle="";}else{$searchforstyle="";$searchfordropdownstyle="style='display:none'";}?>
						<?php 
						echo $this->Form->input('searchby', array(
							  'type' => 'select',
							  'options' => array('all'=>'Select Parameter', 'style'=>'Banner Style', 'active'=>'Active Banners', 'inactive'=>'Inactive Banners'),
							  'selected' => $searchby,
							  'onchange'=>'if(this.value=="style"){$(".serachfor").hide();$(".serachfordropdown").fadeIn();}else{$(".serachfordropdown").hide();$(".serachfor").fadeIn();}',
							  'class'=>'',
							  'label' => false,
							  'style' => '',
							  'onchange'=>'if($(this).val()=="active" || $(this).val()=="inactive"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);}'
						));
						?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>>
			<span>Search For :</span>
			<span class="searchforfields_s serachfor" <?php echo $searchforstyle; ?>><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class="searchforfields_s serachfordropdown" <?php echo $searchfordropdownstyle; ?>>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
							<?php 
							echo $this->Form->input('searchfordropdown', array(
							      'type' => 'select',
							      'options' => array('728x90'=>'728X90', '468x60'=>'468X60', '234x60'=>'234X60', '120x240'=>'120X240', '125x125'=>'125X125', '120x600'=>'120X600','160x600'=>'160X600', '180x50'=>'180X50', '200x200'=>'200X200', '250x250'=>'250X250', '300x250'=>'300X250', '336x280'=>'336X280', '600x300'=>'600X300'),
							      'selected' => $searchfordropdown,
							      'class'=>'',
							      'label' => false,
							      'style' => ''
							));
							?>
						</label>
					</div>
				</div>
			</span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
                  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#promotoolspage',
                  'class'=>'searchbtn',
                  'controller'=>'promotools',
                  'action'=>'index',
                  'url'=> array('controller' => 'promotools', 'action' => 'index')
                ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->

        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#promotoolspage',
            'evalScripts' => true,
            'url'=> array('controller'=>'promotools', 'action'=>'index')
        ));
        $currentpagenumber=$this->params['paging']['Userbanner']['page'];
        ?>
		
<div id="gride-bg" class="noborder">
    <div class="padding10">
		<div class="greenbottomborder">
			<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">		
        <?php if(!isset($SubadminAccessArray) || in_array('promotools',$SubadminAccessArray) || in_array('promotools/adminpanel_userbanneradd',$SubadminAccessArray)){ ?>
            
                <?php echo $this->Js->link("+ Add New", array('controller'=>'promotools', "action"=>"userbanneradd"), array(
                    'update'=>'#promotoolspage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'class'=>'btnorange'
                ));?>
        <?php } ?>
	</div>
	<div class="clear-both"></div>
		
		<div class="tablegrid">
				<div class="tablegridheader">
				    <div>
                        <?php
						if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
						echo $this->Js->link('Date Created', array('controller'=>'promotools', "action"=>"index/0/0/create_date/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#promotoolspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Date'
                        ));?>
                    </div>
				    <div>
                        <?php 
                        echo $this->Js->link('Title', array('controller'=>'promotools', "action"=>"index/0/0/title/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#promotoolspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Title'
                        ));?>
                    </div>
				    <div><?php echo "Banner Size";?></div>
				    <div><?php echo "Banner";?></div>
				    <div>
                        <?php echo $this->Js->link('Action', array('controller'=>'promotools', "action"=>"index/0/0/status/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#promotoolspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Status'
                        )); ?>
                    </div>
                </div>
                <?php foreach ($userbanners as $userbanner): ?>
                    <div class="tablegridrow">
			<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $userbanner['Userbanner']['create_date']); ?></div>
			<div><?php echo $userbanner['Userbanner']['title']; ?></div>
                        <div><?php echo str_replace(",","X",$userbanner['Userbanner']['style']); ?></div>
                        <div>
                            <a class="vtip" title='<?php
                                if($userbanner['Userbanner']['is_upload']==1)	
                                    echo $this->html->image('banners/'.$userbanner['Userbanner']['photo'], array('alt'=>'', 'align' =>'absmiddle')); 
                                else
                                    echo '<img src="'.$userbanner['Userbanner']['photo'].'" alt="" />';
                            ?>'><?php echo $this->html->image('search.png', array('alt'=>'', 'align' =>'absmiddle'));?></a>
                        </div>
                        <div>
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
										
												<?php if(!isset($SubadminAccessArray) || in_array('promotools',$SubadminAccessArray) || in_array('promotools/adminpanel_userbanneradd/$',$SubadminAccessArray)){?>
												<li>
												<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Banner'))." Edit", array('controller'=>'promotools', "action"=>"userbanneradd/".$userbanner['Userbanner']['banner_id']), array(
														'update'=>'#promotoolspage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>'openaddform'
													));?>
												</li>
												<?php }?>
												
												<?php 
												if(!isset($SubadminAccessArray) || in_array('promotools',$SubadminAccessArray) || in_array('promotools/adminpanel_userbannerstatus',$SubadminAccessArray)){?>
												<li>
												<?php
													if($userbanner['Userbanner']['status']==0){
														$statusaction='1';
														$statusicon='red-icon.png';
														$statustext='Activate';
													}else{
														$statusaction='0';
														$statusicon='blue-icon.png';
														$statustext='Inactivate';}
													echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'promotools', "action"=>"userbannerstatus/".$statusaction."/".$userbanner['Userbanner']['banner_id']."/".$currentpagenumber), array(
														'update'=>'#promotoolspage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>''
													)); ?>
												</li>
												<?php }?>
												
												<?php if(!isset($SubadminAccessArray) || in_array('promotools',$SubadminAccessArray) || in_array('promotools/adminpanel_userbannerremove',$SubadminAccessArray)){?>
												<li>
												<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Banner'))." Delete", array('controller'=>'promotools', "action"=>"userbannerremove/".$userbanner['Userbanner']['banner_id']."/".$currentpagenumber), array(
														'update'=>'#promotoolspage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>'',
														'confirm'=>"Do You Really Want to Delete This Banner Ad?"
													)); ?>
												</li>
												<?php }?>
									</ul>
								  </div>
								</div>
                        </div>
                    </div>
                <?php endforeach; ?>
        </div>
	<?php if(count($userbanners)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
		
        <?php echo $this->Form->end();
        if($this->params['paging']['Userbanner']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Userbanner',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'promotools','action'=>'index/0/rpp')));?>
			<div class="resultperpage">
				<label>
					<?php 
					echo $this->Form->input('resultperpage', array(
					  'type' => 'select',
					  'options' => $resultperpage,
					  'selected' => $this->Session->read('pagerecord'),
					  'class'=>'',
					  'label' => false,
					  'div'=>false,
					  'style' => '',
					  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
					));
					?>
				</label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#promotoolspage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'promotools',
                  'action'=>'index/0/rpp',
                  'url'   => array('controller' => 'promotools', 'action' => 'index/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
		</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#promotoolspage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>