<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(!$ajax){ ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="promotoolspage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>

<?php // Tell friends email template starts here ?>
<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'promotools','action'=>'tellfriends/invite')));?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Tell Friends').' '.__('Email Template');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<div class="form-box">
		<div class="form-row">
			<div class="form-col-1"><?php echo __("Subject");?>&nbsp;:&nbsp;</div>
			<div class="form-col-2">
				<?php echo $this->Form->input('subject', array('type'=>'text', 'value'=>stripslashes($template_email_data['Template_email']['subject']), 'label' => false, 'class'=>'formtextbox'));?>
			</div>
		</div>
		<div class="form-row">
			<div class="form-col-1"><?php echo __("Message");?>&nbsp;:&nbsp;</div>
			<div class="form-col-2">
				<?php echo $this->Form->input('message', array('type'=>'textarea', 'value'=>stripslashes($template_email_data['Template_email']['message']), 'label' => false, 'class'=>'formtextarea', 'style'=>'height:200px;'));?>
			</div>
		</div>
		<div class="form-row">
			<div class="form-col-1"><?php echo __("Mail Template Fields");?>&nbsp;:&nbsp;</div>
			<div class="form-col-2" style="line-height: 25px;">
				<?php foreach($tagdata as $key=>$value){?>
					<a onClick="AddTags(document.getElementById('MemberMessage'),'[<?php echo $key;?>]');" title="<?php echo __('Click to paste');?>" href="javascript:void(0);"><?php echo '['.$key.']';?></a><?php echo ' - '.__($value);?><br />
				<?php }?>
			</div>
		</div>
	</div>
</div>
<?php // Tell friends email template ends here ?>

<?php if($addressbook==0){?>
<div id="UpdateMessage"></div>

<?php // Tell friends list starts here ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Tell Friends');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam"><?php echo __("First Name");?></div>
				<div class="divth textcenter vam"><?php echo __("Last Name");?></div>
				<div class="divth textcenter vam"><?php echo __("Email");?></div>
				<div class="divth textcenter vam"></div>
			</div>
		</div>
		<div class="divtbody">	
			<div class="divtr contact-gray">
				<?php echo $this->Form->input('errornotice', array('type'=>'hidden', 'value'=>__('Please enter valid data in following fields')));?>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('f_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;', 'title'=>__('First Name')));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('l_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;', 'title'=>__('Last Name')));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('email.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;', 'title'=>__('Email')));?></div>
				<div class="divtd textcenter vam"></div>
			</div>
			<div class="divtr">
				<div class="divtd textcenter vam"><?php echo $this->Form->input('f_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('l_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('email.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"></div>
			</div>
			<div class="divtr contact-gray">
				<div class="divtd textcenter vam"><?php echo $this->Form->input('f_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('l_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('email.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><input type="button" class="large white button" style="float: none" value="<?php echo __('Add New'); ?>" onclick='$(this).hide(500);$(".tellfriendcol1").css({"display":"table-row","opacity":0}).animate({"opacity":"1"},500);' /></div>
			</div>
			<div class="divtr tellfriendcol1" style="display:none;">
				<div class="divtd textcenter vam"><?php echo $this->Form->input('f_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('l_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('email.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><input type="button" class="large white button" style="float: none" value="<?php echo __('Add New'); ?>" onclick='$(this).hide(500);$(".tellfriendcol2").css({"display":"table-row","opacity":0}).animate({"opacity":"1"},500);' /></div>
			</div>
			<div class="divtr tellfriendcol2 contact-gray" style="display:none;">
				<div class="divtd textcenter vam"><?php echo $this->Form->input('f_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('l_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('email.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><input type="button" class="large white button" style="float: none" value="<?php echo __('Add New'); ?>" onclick='$(this).hide(500);$(".tellfriendcol3").css({"display":"table-row","opacity":0}).animate({"opacity":"1"},500);' /></div>
			</div>
			<div class="divtr tellfriendcol3" style="display:none;">
				<div class="divtd textcenter vam"><?php echo $this->Form->input('f_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('l_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('email.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><input type="button" class="large white button" style="float: none" value="<?php echo __('Add New'); ?>" onclick='$(this).hide(500);$(".tellfriendcol4").css({"display":"table-row","opacity":0}).animate({"opacity":"1"},500);' /></div>
			</div>
			<div class="divtr tellfriendcol4 contact-gray" style="display:none;">
				<div class="divtd textcenter vam"><?php echo $this->Form->input('f_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('l_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('email.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><input type="button" class="large white button" style="float: none" value="<?php echo __('Add New'); ?>" onclick='$(this).hide(500);$(".tellfriendcol5").css({"display":"table-row","opacity":0}).animate({"opacity":"1"},500);' /></div>
			</div>
			<div class="divtr tellfriendcol5" style="display:none;">
				<div class="divtd textcenter vam"><?php echo $this->Form->input('f_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('l_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('email.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><input type="button" class="large white button" style="float: none" value="<?php echo __('Add New'); ?>" onclick='$(this).hide(500);$(".tellfriendcol6").css({"display":"table-row","opacity":0}).animate({"opacity":"1"},500);' /></div>
			</div>
			<div class="divtr tellfriendcol6 contact-gray" style="display:none;">
				<div class="divtd textcenter vam"><?php echo $this->Form->input('f_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('l_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('email.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><input type="button" class="large white button" style="float: none" value="<?php echo __('Add New'); ?>" onclick='$(this).hide(500);$(".tellfriendcol7").css({"display":"table-row","opacity":0}).animate({"opacity":"1"},1000);' /></div>
			</div>
			<div class="divtr tellfriendcol7" style="display:none;">
				<div class="divtd textcenter vam"><?php echo $this->Form->input('f_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('l_name.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"><?php echo $this->Form->input('email.', array('type'=>'text', 'label'=>false, 'class'=>'formtextbox', 'div'=>false, 'style'=>'width:200px;'));?></div>
				<div class="divtd textcenter vam"></div>
			</div>
		</div>
	</div>
	<div class="formbutton"> 
		<?php echo $this->Js->submit(__('Send'), array(
		  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
		  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		  'update'=>'#UpdateMessage',
		  'div'=>false,
		  'class'=>'button',
		  'onfocus'=>'return ValidateTellfriendForm(this.form);',
		  'controller'=>'promotools',
		  'action'=>'tellfriends/invite',
		  'url'   => array('controller' => 'promotools', 'action' => 'tellfriends/invite')
		));?>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Tell friends list ends here ?>

<?php }elseif($addressbook==1){?>
<div id="UpdateMessage"></div>

<?php // Contacts list code starts here ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Your contacts');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam"><?php echo __("Name");?></div>
				<div class="divth textcenter vam"><?php echo __("Email");?></div>
				<div class="divth textcenter vam">
					<?php echo $this->Form->input('selectAllCheckboxes', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'value'=>$key, 'hiddenField' => false, 'onclick' => 'selectAllCheckboxes("EmailIds",this.checked)'));?>
					<?php echo __("Invite?");?>
				</div>
			</div>
		</div>
		<div class="divtbody">
			<?php $i=1;
			foreach ($mycontactsdata as $key=>$value):
			if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
			<div class="divtr <?php echo $class;?>">
				<div class="divtd textcenter vam"><?php echo $value;?></div>
				<div class="divtd textcenter vam"><?php echo $key;?></div>
				<div class="divtd textcenter vam">
				<?php echo $this->Form->input('EmailIds.', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'value'=>$key, 'id'=>"chk".$i, 'class'=>'EmailIds', 'hiddenField' => false));?>
				</div>
			</div>
			<?php $i++;endforeach; ?>
		</div>
	</div>
	<div class="formbutton">
		<?php echo $this->Js->submit(__('Send'), array(
		  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
		  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		  'update'=>'#UpdateMessage',
		  'div'=>false,
		  'class'=>'button',
		  'controller'=>'promotools',
		  'action'=>'tellfriends/inviteimport',
		  'url'   => array('controller' => 'promotools', 'action' => 'tellfriends/inviteimport')
		));?>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Contacts list code ends here ?>

<?php }?>
<?php echo $this->Form->end();?>

<?php // Import Contacts code starts here ?>
<?php if($SITECONFIG["importcontact"]==1){ ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Import Contacts');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'promotools','action'=>'tellfriends/import')));?>
	<div class="form-box">
		<div class="form-row">
			<div class="form-col-1"><?php echo __("Email");?>&nbsp;:&nbsp;</div>
			<div class="form-col-2">
				<?php echo $this->Form->input('email', array('type'=>'text', 'label' => false, 'class'=>'formtextbox'));?>
			</div>
		</div>
		<div class="form-row">
			<div class="form-col-1"><?php echo __("Password");?>&nbsp;:&nbsp;</div>
			<div class="form-col-2">
				<?php echo $this->Form->input('password', array('type'=>'password', 'label' => false, 'class'=>'login-from-box-1 keyboardInput'));?>
			</div>
		</div>
		<div class="form-row">
			<div class="form-col-1"><?php echo __("Provider");?>&nbsp;:&nbsp;</div>
			<div class="select-dropdown">
					<label>
						<select name="provider" id="provider" class="searchcomboboxwidth">
							<option value="">Select Provider</option>
							<optgroup label="Email Providers">
							<option value="abv">Abv</option>
							<option value="aol">AOL</option>
							<option value="apropo">Apropo</option>
							<option value="atlas">Atlas</option>
							<option value="aussiemail">Aussiemail</option>
							<option value="azet">Azet</option>
							<option value="bigstring">Bigstring</option>
							<option value="bordermail">Bordermail</option>
							<option value="canoe">Canoe</option>
							<option value="care2">Care2</option>
							<option value="clevergo">Clevergo</option>
							<option value="doramail">Doramail</option>
							<option value="evite">Evite</option>
							<option value="fastmail">FastMail</option>
							<option value="fm5">5Fm</option>
							<option value="freemail">Freemail</option>
							<option value="gawab">Gawab</option>
							<option value="gmail">GMail</option>
							<option value="gmx_net">GMX.net</option>
							<option value="graffiti">Grafitti</option>
							<option value="hotmail">Live/Hotmail</option>
							<option value="hushmail">Hushmail</option>
							<option value="inbox">Inbox.com</option>
							<option value="india">India</option>
							<option value="indiatimes">IndiaTimes</option>
							<option value="inet">Inet</option>
							<option value="interia">Interia</option>
							<option value="katamail">KataMail</option>
							<option value="kids">Kids</option>
							<option value="libero">Libero</option>
							<option value="linkedin">LinkedIn</option>
							<option value="lycos">Lycos</option>
							<option value="mail2world">Mail2World</option>
							<option value="mail_com">Mail.com</option>
							<option value="mail_in">Mail.in</option>
							<option value="mail_ru">Mail.ru</option>
							<option value="meta">Meta</option>
							<option value="msn">MSN</option>
							<option value="mynet">Mynet.com</option>
							<option value="netaddress">Netaddress</option>
							<option value="nz11">Nz11</option>
							<option value="o2">O2</option>
							<option value="operamail">OperaMail</option>
							<option value="plaxo">Plaxo</option>
							<option value="pochta">Pochta</option>
							<option value="popstarmail">Popstarmail</option>
							<option value="rambler">Rambler</option>
							<option value="rediff">Rediff</option>
							<option value="sapo">Sapo.pt</option>
							<option value="techemail">Techemail</option>
							<option value="terra">Terra</option>
							<option value="uk2">Uk2</option>
							<option value="virgilio">Virgilio</option>
							<option value="walla">Walla</option>
							<option value="web_de">Web.de</option>
							<option value="wpl">Wp.pt</option>
							<option value="xing">Xing</option>
							<option value="yahoo">Yahoo!</option>
							<option value="yandex">Yandex</option>
							<option value="youtube">YouTube</option>
							<option value="zapak">Zapakmail</option>
							</optgroup><optgroup label="Social Networks">
							<option value="badoo">Badoo</option>
							<option value="bebo">Bebo</option>
							<option value="bookcrossing">Bookcrossing</option>
							<option value="brazencareerist">Brazencareerist</option>
							<option value="cyworld">Cyworld</option>
							<option value="eons">Eons</option>
							<option value="facebook">Facebook</option>
							<option value="faces">Faces</option>
							<option value="famiva">Famiva</option>
							<option value="fdcareer">Fdcareer</option>
							<option value="flickr">Flickr</option>
							<option value="flingr">Flingr</option>
							<option value="flixster">Flixster</option>
							<option value="friendfeed">Friendfeed</option>
							<option value="friendster">Friendster</option>
							<option value="hi5">Hi5</option>
							<option value="hyves">Hyves</option>
							<option value="kincafe">Kincafe</option>
							<option value="konnects">Konnects</option>
							<option value="koolro">Koolro</option>
							<option value="lastfm">Last.fm</option>
							<option value="livejournal">Livejournal</option>
							<option value="lovento">Lovento</option>
							<option value="meinvz">Meinvz</option>
							<option value="mevio">Mevio</option>
							<option value="motortopia">Motortopia</option>
							<option value="multiply">Multiply</option>
							<option value="mycatspace">Mycatspace</option>
							<option value="mydogspace">Mydogspace</option>
							<option value="myspace">MySpace</option>
							<option value="netlog">NetLog</option>
							<option value="ning">Ning</option>
							<option value="orkut">Orkut</option>
							<option value="perfspot">Perfspot</option>
							<option value="plazes">Plazes</option>
							<option value="plurk">Plurk</option>
							<option value="skyrock">Skyrock</option>
							<option value="tagged">Tagged</option>
							<option value="twitter">Twitter</option>
							<option value="vimeo">Vimeo</option>
							<option value="vkontakte">Vkontakte</option>
							<option value="xanga">Xanga</option>
							<option value="xuqa">Xuqa</option></optgroup>
						</select>
					</label>
			</div>
		</div>
	</div>
	<div class="formbutton"> 
		<?php echo $this->Js->submit(__('Import Contacts'), array(
		  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
		  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		  'update'=>'#promotoolspage',
		  'div'=>false,
		  'class'=>'button',
		  'controller'=>'promotools',
		  'action'=>'tellfriends/import',
		  'url'   => array('controller' => 'promotools', 'action' => 'tellfriends/import')
		));?>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->end();?>
</div>
<?php }?>
<?php // Import Contacts code ends here ?>
	
<?php if(!$ajax){?>
</div>
<?php }?>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>