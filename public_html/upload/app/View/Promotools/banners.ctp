<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<?php if(!$ajax){?>
<script>
function Dynamicbox(atag)
{
	var popwid="60%";
	if($(window).width()<700)
		var popwid="100%";
	$("a[rel='lightboxtext']").colorbox({width:popwid, height:"71%", inline:true, href:"#"+atag.title});
}
</script>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Promotional Banners');?></div>
	<div class="clear-both"></div>
</div>
<div class="clear-both"></div>
<?php }?>

<?php // Promotional banners list starts here ?>
<div id="promotoolspage">
<?php echo $this->Javascript->link('allpage');?>
<div class="main-box-eran textcenter" style="padding: 0px;">
	<?php $n=1; $totalbanners=count($userbannerdata);
	foreach ($userbannerdata as $userbanner): ?>
		<?php if($n==1 || $userbanner['Userbanner']['style']!=$currentstyle){
			$currentstyle=$userbanner['Userbanner']['style'];
			?>
			<div class="banners-title">
				<?php echo $currentstyle; ?>
			</div>
			<?php
		} ?>
			
		<div class="fixcontentbox">
			<a href="#" rel="lightboxtext" title="showbannercode<?php echo $userbanner['Userbanner']['banner_id'];?>" onclick="Dynamicbox(this)">
				<div class="imagetextbox">
					<?php
					if($userbanner['Userbanner']['is_upload']==1){	
						$banner_path=$SITEURL."img/banners/".$userbanner['Userbanner']['photo'];
						echo $this->html->image('banners/'.$userbanner['Userbanner']['photo'], array('alt'=>'', 'align' =>'absmiddle')); 
					}else{
						$banner_path=$userbanner['Userbanner']['photo'];
						echo '<img src="'.$userbanner['Userbanner']['photo'].'" alt="" />';
					}
					$widthheight=@explode('x', $userbanner['Userbanner']['style']);
					?>
					<span class="cover"></span>
					<span class="text"><?php echo __('Show Code'); ?></span>
				</div>
			</a>
		</div>
		
		<?php // Banner details box code starts here ?>
		<div style="display: none;">
			<div id="showbannercode<?php echo $userbanner['Userbanner']['banner_id']; ?>">
				<div class="formgrid">
					<div class="formgridrow color-black contact-gray">
						<div class="textleft vam"><?php echo __("Banner");?> :</div>
						<div class="textleft vam"><?php
							if($userbanner['Userbanner']['is_upload']==1){	
								$banner_path=$SITEURL."img/banners/".$userbanner['Userbanner']['photo'];
								echo $this->html->image('banners/'.$userbanner['Userbanner']['photo'], array('alt'=>'', 'align' =>'absmiddle')); 
							}else{
								$banner_path=$userbanner['Userbanner']['photo'];
								echo '<img src="'.$userbanner['Userbanner']['photo'].'" alt="" />';
							}
							$widthheight=@explode('x', $userbanner['Userbanner']['style']);
							?></div>
					</div>
					<div class="formgridrow color-black">
						<div class="textleft vam"><?php echo __("Banner Title");?> :</div>
						<div class="textleft vam"><?php echo $userbanner['Userbanner']['title'];?></div>
					</div>
					<div class="formgridrow color-black contact-gray">
						<div class="textleft vam"><?php echo __("Banner Size");?> :</div>
						<div class="textleft vam"><?php echo $userbanner['Userbanner']['style']; ?></div>
					</div>
					<div class="formgridrow color-black">
						<div class="textleft vam"><?php echo __("Banner URL");?> :</div>
						<div class="textleft vam"><?php echo $banner_path; ?></div>
					</div>
					<div class="formgridrow color-black contact-gray">
						<div class="textleft vam"><?php echo __("Destination URL");?> :</div>
						<div class="textleft vam"><?php echo $SITEURL;?>ref/<?php echo ($SITECONFIG["reflinkiduser"]==1)?$UNAME:$UID;?></div>
					</div>
					<div class="formgridrow color-black">
						<div class="textleft vam"><?php echo __("Code");?> :</div>
						<div class="textleft vam"><textarea class="formtextarea" style="width:99%;margin:0;background-color:transparent;line-height: normal;"><a href="<?php echo $SITEURL;?>ref/<?php echo ($SITECONFIG["reflinkiduser"]==1)?$UNAME:$UID;?>"><img src="<?php echo $banner_path;?>" width="<?php echo $widthheight[0];?>" height="<?php echo $widthheight[1];?>" /></a></textarea></div>
					</div>
				</div>
			</div>
		</div>
		<?php // Banner details box code ends here ?>
		
		<?php $n++;?>
	<?php endforeach; ?>
	<?php if(count($userbannerdata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
	<div class="clear-both"></div>
</div>
</div>
<?php // Promotional banners list ends here ?>

<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>