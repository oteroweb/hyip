<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="promotoolspage">
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Text Links');?></div>
	<div class="clear-both"></div>
</div>
<?php echo $this->Javascript->link('allpage');?>

<?php // Textlink ads list starts here ?>
<div class="main-box-eran">
	<div class="divtable">
		<div class="divtbody">
			<?php $i=1;
			foreach ($textlinkdata as $textlink):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
				<div class="divtr <?php echo $class;?>">
					  <div class="divtd textcenter vam borderrightnone">
						<div class="height7"></div>
						<div align="center">
                                                    <?php  if($SITECONFIG["reflinkiduser"]==1)
                                                    {
                                                        $UID = $UNAME;
                                                    }?>
                                                    <?php echo $content=str_replace("[RefLink]", $SITEURL.'ref/'.$UID,nl2br(stripslashes($textlink['Textlink']['content'])));?>
						</div>
						<div class="height7"></div>
						<div><textarea class="formtextarea" style="width:99%;margin:0;background-color:transparent;"><?php echo stripslashes($content);?></textarea></div>
						<div class="height7"></div>
					</div>
				</div>
			<?php $i++;endforeach; ?>
		</div>
  	</div>
	<?php if(count($textlinkdata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
</div>
<?php // Textlink ads list ends here ?>

</div>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>