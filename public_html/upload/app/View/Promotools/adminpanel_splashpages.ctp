<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Promo Tools / Splash Pages</div>
<div id="promotoolspage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Splash_Pages" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#promotoolspage',
            'evalScripts' => true,
            'url'=> array('controller'=>'promotools', 'action'=>'splashpages')
        ));
        $currentpagenumber=$this->params['paging']['Splashpage']['page'];
        ?>

<div id="Xgride-bg">
	<div class="height10"></div>
    <div class="padding10 backgroundwhite">
		<div class="greenbottomborder">
			<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">		
        <?php if(!isset($SubadminAccessArray) || in_array('promotools',$SubadminAccessArray) || in_array('promotools/adminpanel_splashpageadd',$SubadminAccessArray)){ ?>
                <?php echo $this->Js->link("+ Add New", array('controller'=>'promotools', "action"=>"splashpageadd"), array(
                    'update'=>'#promotoolspage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'class'=>'btnorange'
                ));?>
        <?php } ?>
	</div>
	<div class="clear-both"></div>
		
		<div class="tablegrid">
				<div class="tablegridheader">
				    <div>
                        <?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('Title', array('controller'=>'promotools', "action"=>"splashpages/0/0/title/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#promotoolspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Title'
                        ));?>
                    </div>
                    <div><?php echo "Splash Page";?></div>
                    <div>
                        <?php echo $this->Js->link('Action', array('controller'=>'promotools', "action"=>"splashpages/0/0/status/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#promotoolspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Status'
                        ));?>
                    </div>
                </div>
                <?php foreach ($splashpages as $splashpage):
                    if($splashpage['Splashpage']['photo']=='' || $splashpage['Splashpage']['photo']==NULL) $splashpage['Splashpage']['photo']='splashpage.jpg';?>
                    <div class="tablegridrow">
					    <div><?php echo $splashpage['Splashpage']['title']; ?></div>
                        <div><?php echo $this->html->image('splashpages/'.$splashpage['Splashpage']['photo'], array('alt'=>'', 'align' =>'absmiddle','width'=>'37px','height'=>'37px')); ?></div>
                        <div>
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
												<li><a href="<?php echo Router::url('/', true)."page/splash/".$splashpage['Splashpage']['id']; ?>" class="vtip" title="View Splash page" target="_blank"><?php echo $this->html->image('search.png', array('alt'=>'Preview'));?> View Splash page</a></li>
												
												<?php if(!isset($SubadminAccessArray) || in_array('promotools',$SubadminAccessArray) || in_array('promotools/adminpanel_splashpageadd/$',$SubadminAccessArray)){?>
												<li>
												<?php
													echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Splash page'))." Edit", array('controller'=>'promotools', "action"=>"splashpageadd/".$splashpage['Splashpage']['id']), array(
														'update'=>'#promotoolspage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>'openaddform'
													));?>
												</li>
												<?php }?>
												
												<?php  if(!isset($SubadminAccessArray) || in_array('promotools',$SubadminAccessArray) || in_array('promotools/adminpanel_splashpagestatus',$SubadminAccessArray)){?>
												<li>
												<?php
												if($splashpage['Splashpage']['status']==0){
													$statusaction='1';
													$statusicon='red-icon.png';
													$statustext='Activate Splash page';
												}else{
													$statusaction='0';
													$statusicon='blue-icon.png';
													$statustext='Inactivate Splash page';}
													echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'promotools', "action"=>"splashpagestatus/".$statusaction."/".$splashpage['Splashpage']['id']."/".$currentpagenumber), array(
														'update'=>'#promotoolspage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>''
													));?>
												</li>
												<?php }?>
												
												<?php if(!isset($SubadminAccessArray) || in_array('promotools',$SubadminAccessArray) || in_array('promotools/adminpanel_splashpageremove',$SubadminAccessArray)){?>
												<li>
												<?php
													echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Splash page'))." Delete", array('controller'=>'promotools', "action"=>"splashpageremove/".$splashpage['Splashpage']['id']."/".$currentpagenumber), array(
														'update'=>'#promotoolspage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>'',
														'confirm'=>"Do You Really Want to Delete This Splash page?"
													));?>
												</li>
												<?php }?>
									</ul>
								  </div>
								</div>
                        </div>
                    </div>
                <?php endforeach; ?>
		</div>
		
		<?php if(count($splashpages)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
		
        <?php echo $this->Form->end();
        if($this->params['paging']['Splashpage']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Splashpage',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'promotools','action'=>'splashpages/0/rpp')));?>
			<div class="resultperpage">
			    <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			    </label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#promotoolspage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'promotools',
                  'action'=>'splashpages/0/rpp',
                  'url'   => array('controller' => 'promotools', 'action' => 'splashpages/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
		</div>
     </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#promotoolspage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>