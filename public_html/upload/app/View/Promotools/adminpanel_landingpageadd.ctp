<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<script type="text/javascript"> 
(function() {
$('#Landingpageadd').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();       
</script>
<?php if(!$ajax){?>
<div class="whitetitlebox">Promo Tools / Landing Pages</div>
<div id="promotoolspage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Landing_Pages" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<div class="height10"></div>
<div class="backgroundwhite">
		
<?php echo $this->Form->create('Landingpage',array('type' => 'post', 'id'=>'Landingpageadd', 'type' =>'file', 'onsubmit' => 'return true;','url'=>array('controller'=>'promotools','action'=>'landingpageaddaction')));?>
	<?php if(isset($landingpagedata['Landingpage']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$landingpagedata['Landingpage']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		echo $this->Form->input('preview', array('type'=>'hidden', 'value'=>$landingpagedata['Landingpage']["preview"], 'label' => false));
	}?>
	<div class="frommain">
		
		<?php if(!isset($landingpagedata['Landingpage']["id"])){ ?>
		<div class="fromnewtext">Landingpage Directory :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('preview', array('type'=>'text', 'value'=>$landingpagedata['Landingpage']["preview"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<?php } ?>
	
		<div class="fromnewtext">Upload Images to : </div>
		<div class="fromborderdropedown3">
			<div class="linktotext">
				<?php if(isset($landingpagedata['Landingpage']["id"])){ ?>
                    app/webroot/landingpage/<?php echo $landingpagedata['Landingpage']["preview"]; ?>/images/
                <?php }else{ ?>
                    app/webroot/landingpage/<?php echo "[Landingpage Directory]/images/";?>
                <?php } ?>
			</div>
		</div>
		
		<div class="fromnewtext">Status :</div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
					<?php 
					echo $this->Form->input('status', array(
						'type' => 'select',
						'options' => array('1'=>'Active', '0'=>'Inactive'),
						'selected' => $landingpagedata['Landingpage']["status"],
						'class'=>'',
						'div' => false,
						'label' => false,
						'style' => ''
					));
					?>
				</label>
			</div>
		</div>
		
		<div class="fromnewtext">Page Name :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('title', array('type'=>'text', 'value'=>$landingpagedata['Landingpage']["title"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Select Thumbnail Image : </div>
		<div class="fromborderdropedown3">
		  <div class="btnorange browsebutton">Browse<?php echo $this->Form->input('photo', array('type'=>'file', 'label' => false, 'div' => false,'class'=>'browserbuttonlink'));?></div>
		</div>
		
	
		<?php if(isset($landingpagedata['Landingpage']["id"])){ if($landingpagedata['Landingpage']['photo']=='' || $landingpagedata['Landingpage']['photo']==NULL) $landingpagedata['Landingpage']['photo']='landingpage.jpg';?>
			<div class="fromnewtext">Current Image : </div>
			<div class="fromborderdropedown3">
				<div class="linktotext">
					<?php echo $this->html->image("landingpages/".$landingpagedata['Landingpage']['photo'], array('alt'=>'Image','width'=>'37px','height'=>'37px')); ?>
				</div>
			</div>
		<?php }?>
		
		<div class="fromnewtext">Description :</div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('description', array('type'=>'textarea', 'value'=>stripslashes($landingpagedata['Landingpage']["description"]), 'label' => false, 'div'=> false, 'class'=>'from-textarea', 'style'=>''));?>
			
		</div>
		
		<div class="fromnewtext">Content(HTML) :<span class="red-color">*</span></div>
		<div class="frombordermain">
			<?php echo $this->Form->input('content', array('type'=>'textarea', 'value'=>stripslashes($landingpagedata['Landingpage']["content"]), 'label' => false, 'div' => false, 'class'=>'from-textarea', 'style'=>'width:98%;height:400px;margin-left:0px;'));?>
		</div>
	
	
		<div class="formbutton">
			<?php echo $this->Form->submit('Submit', array(
			  'class'=>'btnorange',
			  'div'=>false
			));?>
			<?php echo $this->Js->link("Back", array('controller'=>'promotools', "action"=>"landingpages"), array(
				'update'=>'#promotoolspage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'btngray'
			));?>
		</div>
		
	</div>
	
    <center>
		<div class="instructions-page-width">
            <div class="instructions-for-payza">Allowed Tags</div>
			<div class="instructions-for-payza-text">
				<p><b>You can use following tags in content box above. They will be replaced with their respective values.</b></p>
				<ul class="new-payment-ul">
					<li>[#LANDINGPAGE_TITLE#] - <span class="new-blue-color">Landing Page Title</span></li>
					<li>[#REF_LINK#] - <span class="new-blue-color">Member's Referral Link</span></li>
                    <li>[#REF_ID#] - <span class="new-blue-color">Member Id</span></li>
                    <li>[#LANDING_ID#] - <span class="new-blue-color">Landing Page Id</span></li>
                    <li>[#SITEADDRESS#] - <span class="new-blue-color">URL of The Site</span></li>
					<li>[#ERRORMESSAGE#] - <span class="new-blue-color">Error Message</span></li>
					<div class="new-note"><span class="red-color">Note : </span>All the tags above can be used any number of times.</div>
				</ul>
				</div>
		<div class="height-12"></div>
		</div>
	</center>
<?php echo $this->Form->end();?>

</div>
<?php if(!$ajax){?>
</div><!--#promotoolspage over-->
<?php }?>					
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>