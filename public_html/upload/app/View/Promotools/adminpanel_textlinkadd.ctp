<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Promo Tools / Text Links</div>
<div id="promotoolspage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Text_Links" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>

<div class="backgroundwhite">
		
		<?php echo $this->Form->create('Textlink',array('type' => 'post', 'id'=>'TextlinkForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'promotools','action'=>'textlinkaddaction')));?>
		<?php if(isset($textlinks['Textlink']["textlink_id"])){
			echo $this->Form->input('textlink_id', array('type'=>'hidden', 'value'=>$textlinks['Textlink']["textlink_id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}?>
		
		<div class="frommain">
			
			<div class="fromnewtext">Message :<span class="red-color">*</span>  </div>
			<div class="frombordermain">
				<?php echo $this->Form->input('content', array('type'=>'textarea', 'value'=>stripslashes($textlinks['Textlink']['content']),'label' => false, 'div' => false, 'class'=>'from-textarea', 'id'=>'TextlinkContent'));?>
			</div>
			
			<div class="fromnewtext">Mail Template Fields : </div>
			<div class="fromborderdropedown3">
				<?php foreach($tagdata as $tag){?>
					<div class="linktotext"><a onClick="AddTags(document.getElementById('TextlinkContent'),'[<?php echo $tag['Template_tag']['tag_name'];?>]');" title="Click to Paste" href="javascript:void(0);"><?php echo '['.$tag['Template_tag']['tag_name'].']';?></a><?php echo ' - '.$tag['Template_tag']['tag_desc'];?></div>
				<?php }?>
			</div>
			
			<div class="formbutton">
					<?php echo $this->Js->submit('Update', array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage',
					  'class'=>'btnorange',
					  'div'=>false,
					  'controller'=>'promotools',
					  'action'=>'textlinkaddaction',
					  'url'   => array('controller' => 'promotools', 'action' => 'textlinkaddaction')
					));?>
					<?php 
					echo $this->Js->link("Back", array('controller'=>'promotools', "action"=>"textlinks"), array(
						'update'=>'#promotoolspage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'btngray'
					));?>
			</div>
			
		</div>
		<?php echo $this->Form->end();?>
			
</div>
<?php if(!$ajax){?>
</div><!--#promotoolspage over-->
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>