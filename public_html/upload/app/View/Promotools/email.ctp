<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="promotoolspage">
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Promotional Emails');?></div>
	<div class="clear-both"></div>
</div>
<?php echo $this->Javascript->link('allpage');?>

<?php // Email ads list starts here ?>
<div class="main-box-eran">
	<div class="divtable">
		<div class="divtbody">
			<?php $i=1;
			foreach ($emailaddata as $emailad):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}
				
				$subject = str_replace($tagfields, $tagvalues, $emailad['Emailad']["subject"]);
				$message = str_replace($tagfields, $tagvalues, $emailad['Emailad']["message"]);
				?>
				<div class="divtr <?php echo $class;?>">
					   <div class="divtd textcenter vam borderrightnone">
					  		<div class="height7"></div>
							<div align="center">
								<?php echo $this->Form->input('subject', array('type'=>'text', 'value'=>stripslashes($subject), 'label' => false, 'class'=>'formtextbox', 'style'=>'width:99%;background-color:transparent;'));?>
							</div>
							<div class="height7"></div>
							<div><textarea class="formtextarea" style="width:99%;height:200px;margin:0;background-color:transparent;"><?php echo stripslashes($message);?></textarea></div>
						<div class="height7"></div>
					</div>
				</div>
			<?php $i++;endforeach; ?>
		</div>
  	</div>
	<?php if(count($emailaddata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
</div>
<?php // Email ads list ends here ?>

</div>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>