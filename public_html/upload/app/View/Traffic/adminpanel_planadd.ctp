<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 10-12-2014
  * Last Modified: 11-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Traffic Exchange</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Website Credit Plans", array('controller'=>'traffic', "action"=>"plan"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Website Credit Plan Member", array('controller'=>'traffic', "action"=>"planmember"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("View Credits", array('controller'=>'traffic', "action"=>"webcredit"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Websites", array('controller'=>'traffic', "action"=>"website"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Start Page", array('controller'=>'traffic', "action"=>"startpage"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Start Page Member", array('controller'=>'traffic', "action"=>"startpagemember"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Pending Start Page Member", array('controller'=>'traffic', "action"=>"startpagememberpending"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="trafficpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Biz_Directory#Biz_Directory_Plans" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<?php echo $this->Form->create('Webcreditplan',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'traffic','action'=>'planaddaction')));?>
	<?php if(isset($webcreditplandata['Webcreditplan']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$webcreditplandata['Webcreditplan']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
	
	<div class="frommain">
	
		<div class="fromnewtext">Plan Name :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('plan_name', array('type'=>'text', 'value'=>$webcreditplandata['Webcreditplan']["plan_name"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Plan Status : </div>
		<div class="fromborderdropedown">
		  <div class="select-main">
			  <label>
				<?php 
				echo $this->Form->input('status', array(
					'type' => 'select',
					'options' => array('1'=>'Active', '0'=>'Inactive'),
					'selected' => $webcreditplandata['Webcreditplan']["status"],
					'class'=>'',
					'label' => false,
					'div' => false,
					'style' => ''
				));
				?>
			  </label>
		  </div>
		</div>
		<span data-original-title="If Set to 'Active', The Plan Will Be Displayed on Member Side." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="fromnewtext">Purchase Status : </div>
		<div class="fromborderdropedown">
		  <div class="select-main">
			  <label>
				<?php 
				echo $this->Form->input('allow_new_purchase', array(
					'type' => 'select',
					'options' => array('1'=>'Active', '0'=>'Inactive'),
					'selected' => $webcreditplandata['Webcreditplan']["allow_new_purchase"],
					'class'=>'',
					'label' => false,
					'div' => false,
					'style' => ''
				));
				?>
			  </label>
		  </div>
		</div>
		<span data-original-title="If Set to 'Active', The Plan Will Be Displayed on Member Side When Purchasing." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="fromnewtext">Hide Plan ? : </div>
		<div class="fromborderdropedown">
		  <div class="select-main">
			  <label>
				<?php 
				echo $this->Form->input('ishide', array(
				  'type' => 'select',
				  'options' => array('0'=>'No', '1'=>'Yes'),
				  'selected' => $webcreditplandata['Webcreditplan']["ishide"],
				  'class'=>'',
				  'label' => false,
				  'div' => false,
				  'style' => ''
				));
				?>
			  </label>
		  </div>
		</div>
		<span data-original-title="Set This to 'No' if You Do Not Want Members to See This Plan on Purchase Page. This Option is Useful When You Want to Allow Only <b>Selected Members</b> to Purchase Ads(s) of This Plan or Award This Plan With Plan(s) Position, Memberships or Signup. Just Provide The <b>Direct Link</b> to Your Selected Members to Allow Them to Purchase This Plan. The Link Can Be Found at The End of Plan Edit Page Once You Have Created One." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="fromnewtext">Allow Coupons on Purchase :  </div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>	
				<?php 
				  echo $this->Form->input('iscoupon', array(
					  'type' => 'select',
					  'options' => array('1'=>'Yes', '0'=>'No'),
					  'selected' => $webcreditplandata['Webcreditplan']["iscoupon"],
					  'class'=>'',
					  'label' => false,
					  'div' => false,
					  'style' => ''
				  ));
				?>
				</label>
			</div>
		</div>
		
		
		<div class="fromnewtext">Plan Price :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('price', array('type'=>'text', 'value'=>$webcreditplandata['Webcreditplan']["price"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Credits :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('credits', array('type'=>'text', 'value'=>$webcreditplandata['Webcreditplan']["credits"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<?php $displayrevenue=''; if(strpos($SITECONFIG['modules'], ":module3:1")===false){$displayrevenue='display:none;';}?>
		<div style="<?php echo $displayrevenue; ?>" class="fromnewtext">Revenue Share (%):<span class="red-color">*</span> </div>
		<div style="<?php echo $displayrevenue; ?>" class="fromborder">
			<?php echo $this->Form->input('revshare_rate', array('type'=>'text', 'value'=>$webcreditplandata['Webcreditplan']["revshare_rate"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		<span style="<?php echo $displayrevenue; ?>" class="tooltipmain glyphicon-question-sign" data-original-title="Specify The Percentage(%) of Income That Should Be Shared Whether You Want to Share The Income From This Plan With The Shareholders or Set '0'. The Rest Will Be Admin's Profit." data-toggle="tooltip"> </span>
		
		<div class="fromnewtext">Pay Commission For The First Purchase Only : </div>
		<div class="fromborderdropedown">
		  <div class="select-main">
			  <label>
				<?php 
				echo $this->Form->input('fpcommission', array(
					'type' => 'select',
					'options' => array('1'=>'Yes', '0'=>'No'),
					'selected' => $webcreditplandata['Webcreditplan']["fpcommission"],
					'class'=>'',
					'label' => false,
					'div' => false,
					'style' => ''
				));
				?>
			  </label>
		  </div>
		</div>
		<span data-original-title="Choose Whether You Want to Provide Referral Commission For The Referral's First Banner Ad Plan Purchase Only. If No, Referral Commission Will Be Provided For All The Banner Ad Plan Purchases By a Referral." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="fromnewtext">Referral Commission Re-purchase/Compound Strategy (%) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('commrepurchase', array('type'=>'text', 'value'=>$webcreditplandata['Webcreditplan']["commrepurchase"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<?php @$commissionlevel=explode(',',$webcreditplandata['Webcreditplan']["commissionlevel"]); ?>
		<div class="fromnewtext">Referral Commission(%) :<span class="red-color">*</span> </div>
		<div class="fromborder4">
			<div class="fromborder4left">
				<div class="levalwhitfromtext">Level 1 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 2 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 3 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 4 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 5 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 6 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 7 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[6], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 8 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[7], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 9 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[8], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 10 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[9], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
			</div>
			<div class="tooltipfronright">
				
			</div>
			<div class="clearboth"></div>
		</div>
		
		<div <?php if($SITECONFIG['balance_type']==1){echo 'style="display: none"';}?>>
		<div class="fromnewtext">Earnings Processor Preference : </div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
				<?php 
					echo $this->Form->input('earning_on', array(
						'type' => 'select',
						'options' => array('0'=>'Purchase Processor', '1'=>"Proirity Processor"),
						'selected' => $webcreditplandata['Webcreditplan']["earning_on"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
				</label>
			</div>
		</div>
		
		</div>
		
		<div class="fromnewtext">Payment Method :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3 checkboxlist">
			<?php
			$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning', 'commission'=>'Commission', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase', 'ca:ea'=>'Cash + Earning', 'ca:co'=>'Cash + Commission', 're:ea'=>'Re-purchase + Earning', 're:co'=>'Re-purchase + Commission', 'ea:co'=>'Earning + Commission', 'ca:re:ea'=>'Cash + Re-purchase + Earning', 're:ea:co'=>'Re-purchase + Earning + Commission', 'ea:co:ca'=>'Earning + Commission + Cash', 'co:ca:re'=>'Commission + Cash + Re-purchase', 'ca:ea:re:co'=>'Cash + Re-purchase + Earning + Commission');
			if($SITECONFIG["wallet_for_commission"] == 'cash' && $SITECONFIG["wallet_for_earning"] == 'cash')
			{
				$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase');
			}
			elseif($SITECONFIG["wallet_for_earning"] == 'cash')
			{
				$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'commission'=>'Commission', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase', 'ca:co'=>'Cash + Commission', 're:co'=>'Re-purchase + Commission', 'co:ca:re'=>'Commission + Cash + Re-purchase');
			}
			elseif($SITECONFIG["wallet_for_commission"] == 'cash')
			{
				$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase', 'ca:ea'=>'Cash + Earning', 're:ea'=>'Re-purchase + Earning', 'ca:re:ea'=>'Cash + Re-purchase + Earning');
			}
			$selected = @explode(",",$webcreditplandata['Webcreditplan']["paymentmethod"]);
			echo $this->Form->input('paymentmethod', array('type' => 'select', 'div'=>false, 'label'=>false, 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $pmethod));?>
		</div>
		
		
		<div class="fromnewtext">Allowed Payment Processors :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3 checkboxlist">
			<?php 
			$selected = @explode(",",$webcreditplandata['Webcreditplan']["paymentprocessors"]);
			echo $this->Form->input('paymentprocessors', array('type' => 'select', 'div'=>false, 'label'=>false, 'class'=>'', 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $paymentprocessors));?>
		</div>
		
		
		
		<?php if(isset($webcreditplandata['Webcreditplan']["id"])){ ?>
		<div class="fromnewtext">Link : </div>
		<div class="fromborderdropedown3">
			<input type="text" class="fromboxbg" value="<?php echo $SITEURL;?>traffic/purchase/<?php echo $webcreditplandata['Webcreditplan']["id"];?>" />
		</div>
		
		<?php }?>
	
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'class'=>'btnorange',
			  'div'=>false,
			  'controller'=>'traffic',
			  'action'=>'planaddaction',
			  'url'   => array('controller' => 'traffic', 'action' => 'planaddaction')
			));?>
			
			<?php echo $this->Js->link("Back", array('controller'=>'traffic', "action"=>"plan"), array(
				'update'=>'#trafficpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'div'=>false,
				'class'=>'btngray'
			));?>
		</div>
		
	</div>	
<?php echo $this->Form->end();?>

<?php if(!$ajax){?>
</div><!--#trafficpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>