<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 10-12-2014
  * Last Modified: 11-12-2014
  *********************************************************************/
?>

<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Traffic Exchange</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Start Page", array('controller'=>'traffic', "action"=>"startpage"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Start Page Member", array('controller'=>'traffic', "action"=>"startpagemember"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Pending Start Page Member", array('controller'=>'traffic', "action"=>"startpagememberpending"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Websites", array('controller'=>'traffic', "action"=>"website"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Website Credit Plans", array('controller'=>'traffic', "action"=>"plan"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Website Credit Plan Member", array('controller'=>'traffic', "action"=>"planmember"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("View Credits", array('controller'=>'traffic', "action"=>"webcredit"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
                        <li>
				<?php echo $this->Js->link("Website Surf History", array('controller'=>'traffic', "action"=>"surfhistory"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="trafficpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Biz_Directory#Biz_Directory_Plans" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Webcreditplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'traffic','action'=>'plan')));?>
	<div class="from-box">
		<div class="fromboxmain width480">
			<span>Search By :</span>
			<span>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
						<?php 
						echo $this->Form->input('searchby', array(
							  'type' => 'select',
							  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'plan_name'=>'Plan Name', 'price'=>'Price', 'credits'=>'Credits', 'active'=>'Active Plans', 'inactive'=>'Inactive Plans'),
							  'selected' => $searchby,
							  'class'=>'',
							  'label' => false,
							  'style' => '',
							  'onchange'=>'if($(this).val()=="inactive" || $(this).val()=="active"){$(".SearchFor").hide(500);}else{$(".SearchFor").show(500);}'
						));
						?>
						</label>
					</div>
				</div>
			</span>
		</div>
		 <div class="fromboxmain" >
			<span class="SearchFor" <?php if($searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>>Search For :</span>
			<span class="SearchFor" <?php if($searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#trafficpage',
				'class'=>'searchbtn',
				'controller'=>'traffic',
				'action'=>'plan',
				'url'=> array('controller' => 'traffic', 'action' => 'plan')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->


	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#trafficpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'traffic', 'action'=>'plan')
	));
	$currentpagenumber=$this->params['paging']['Webcreditplan']['page'];
	?>
	
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('traffic',$SubadminAccessArray) || in_array('traffic/adminpanel_planadd', $SubadminAccessArray)){ ?>
		<?php echo $this->Js->link("+ Add New", array('controller'=>'traffic', "action"=>"planadd"), array(
			'update'=>'#trafficpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btnorange'
		));?>
	<?php } ?>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Webcreditplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'traffic','action'=>'plan')));?>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'traffic', "action"=>"plan/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Plan Name', array('controller'=>'traffic', "action"=>"plan/0/0/plan_name/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Name'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Price', array('controller'=>'traffic', "action"=>"plan/0/0/price/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Price'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Credits', array('controller'=>'traffic', "action"=>"plan/0/0/credits/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Credits'
					));?>
				</div>
                <div><?php echo 'Action';?></div>
			</div>
			<?php foreach ($webcreditplans as $webcreditplan): ?>
				<div class="tablegridrow">
				    <div><?php echo $webcreditplan['Webcreditplan']['id']; ?></div>
					<div><?php echo $webcreditplan['Webcreditplan']['plan_name']; ?></div>
					<div>$<?php echo $webcreditplan['Webcreditplan']['price']; ?></div>
					<div><?php echo $webcreditplan['Webcreditplan']['credits']; ?></div>
					<div>
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
											
											<?php if(!isset($SubadminAccessArray) || in_array('traffic',$SubadminAccessArray) || in_array('traffic/adminpanel_planadd/$', $SubadminAccessArray)){ ?>
											<li>
												<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Plan')).' Edit Plan', array('controller'=>'traffic', "action"=>"planadd/".$webcreditplan['Webcreditplan']['id']), array(
													'update'=>'#trafficpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
												));?>
											</li>
											<?php } ?>
											
											<?php if(!isset($SubadminAccessArray) || in_array('traffic',$SubadminAccessArray) || in_array('traffic/adminpanel_status', $SubadminAccessArray)){ ?>
										    <li>
												<?php
												if($webcreditplan['Webcreditplan']['status']==0){
													$statusaction='1';
													$statusicon='red-icon.png';
													$statustext='Activate Plan';
												}else{
													$statusaction='0';
													$statusicon='blue-icon.png';
													$statustext='Inactivate Plan';}
												echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'traffic', "action"=>"status/".$statusaction."/".$webcreditplan['Webcreditplan']['id']."/".$currentpagenumber), array(
													'update'=>'#trafficpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
												));?>
											</li>
										    <?php } ?>
										    <?php if(!isset($SubadminAccessArray) || in_array('traffic',$SubadminAccessArray) || in_array('traffic/adminpanel_loginad', $SubadminAccessArray)){ ?>
											<li>
												<?php 
												echo $this->Js->link($this->html->image('view-members.png', array('alt'=>'View Members')).' View Members', array('controller'=>'traffic', "action"=>"planmember/".$webcreditplan['Webcreditplan']['id']), array(
													'update'=>'#trafficpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
												)); ?>
											</li>
											<?php } ?>
											
											<?php if(!isset($SubadminAccessArray) || in_array('traffic',$SubadminAccessArray) || in_array('traffic/adminpanel_purchasestatus', $SubadminAccessArray)){ ?>
										    <li>
												<?php
												if($webcreditplan['Webcreditplan']['allow_new_purchase']==0){
													$statusaction='1';
													$statusicon='close.png';
													$statustext='Allow New Purchases';
												}else{
													$statusaction='0';
													$statusicon='open.png';
													$statustext='Deny New Purchases';}
												echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'traffic', "action"=>"purchasestatus/".$statusaction."/".$webcreditplan['Webcreditplan']['id']."/".$currentpagenumber), array(
													'update'=>'#trafficpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
												));?>
											</li>
										    <?php } ?>
											
											<?php if(!isset($SubadminAccessArray) || in_array('traffic',$SubadminAccessArray) || in_array('traffic/adminpanel_loginadremove', $SubadminAccessArray)){ ?>
											<li>
												<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Plan')).' Delete Plan', array('controller'=>'traffic', "action"=>"remove/".$webcreditplan['Webcreditplan']['id']."/".$currentpagenumber), array(
													'update'=>'#trafficpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'confirm'=>"Do You Really Want to Delete This Plan?"
												));?>
											</li>
											<?php } ?>
									</ul>
								  </div>
								</div>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($webcreditplans)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Webcreditplan']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Webcreditplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'traffic','action'=>'plan/0/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#trafficpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'traffic',
			  'action'=>'plan/0/rpp',
			  'url'   => array('controller' => 'traffic', 'action' => 'plan/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
    </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#trafficpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>