<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 12-12-2014
  * Last Modified: 12-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($Enablewebsite==1) { ?>
<?php if(!$ajax){ ?>
<div id="Trafficpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<div class="comisson-bg mobilecss">
	<div class="commison-menu">
		<ul>
			<li>
				<?php
					echo $this->Js->link(__('My Websites'), array('controller'=>'traffic', "action"=>"website"), array(
						'update'=>'#Trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('My Websites')
					));
				?>
			</li>
			<li>
				<?php
					echo $this->Js->link(__('My Start Page'), array('controller'=>'traffic', "action"=>"mystartpage"), array(
						'update'=>'#Trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip act',
						'title'=>__('My Start Page')
					));
				?>
			</li>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
<?php if(count($Startpagebooks)>0) { ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Start Page Pending');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
		<div class="divtable">
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam"><?php echo __("Id");?></div>
					<div class="divth textcenter vam"><?php echo __("Websites Id");?></div>
					<div class="divth textcenter vam"><?php echo __("Purchase Date");?></div>
					<div class="divth textcenter vam"><?php echo __("Book Date");?></div>
				</div>
			</div>
			<div class="divtbody">
				<?php $i=1;
				foreach ($Startpagebooks as $Startpagebook):
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam">#<?php echo $Startpagebook['Startpagebook']['id'];?></div>
						<div class="divtd textcenter vam"><?php echo $Startpagebook['Startpagebook']['webid'];?></div>
						<div class="divtd textcenter vam">
							<?php echo $this->Time->format($SITECONFIG["timeformate"], $Startpagebook['Startpagebook']['create_date']); ?>
						</div>
						<div class="divtd textcenter vam">
							<?php echo $this->Time->format($SITECONFIG["timeformate"], $Startpagebook['Startpagebook']['bookdate']); ?>
						</div>
					</div>
				<?php $i++;endforeach; ?>
			</div>
		</div>
	<div class="clear-both"></div>	
</div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('My Start Page');?></div>
	<div class="clear-both"></div>
</div>
<?php } ?>
	<div class="main-box-eran">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#Trafficpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'traffic', 'action'=>'mystartpage')
	));
	$currentpagenumber=$this->params['paging']['Webstartpagehistory']['page'];
	?>
		<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			
		<?php // My Popup Login ads table starts here ?>
		<div class="divtable">
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam">
						<?php 
						if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
						echo $this->Js->link(__("Id"), array('controller'=>'traffic', "action"=>"mystartpage/0/id/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Trafficpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Id')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Websites Id"), array('controller'=>'traffic', "action"=>"mystartpage/0/plan_id/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Trafficpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Websites Id')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Paid Amount"), array('controller'=>'traffic', "action"=>"mystartpage/0/paid_amount/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Trafficpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Paid Amount')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Discount"), array('controller'=>'traffic', "action"=>"mystartpage/0/discount/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Trafficpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Discount')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Purchase Date"), array('controller'=>'traffic', "action"=>"mystartpage/0/pruchase_date/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Trafficpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Purchase Date')
						));?>
					</div>
				</div>
			</div>
			<div class="divtbody">
				<?php $i=1;
				foreach ($Webstartpages as $Webstartpage):
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam">#<?php echo $Webstartpage['Webstartpagehistory']['id'];?></div>
						<div class="divtd textcenter vam">
							<a href="#" class="vtip" title="<b><?php echo __("Title"); ?> : </b><?php echo $Webstartpage['Website']['title']; ?> <br> <b><?php echo __('Book Date'); ?> : </b> <?php echo $Webstartpage['Webstartpagehistory']['bookdate']; ?>">
								<?php echo $Webstartpage['Webstartpagehistory']['plan_id']; ?>
							</a>
						</div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($Webstartpage['Webstartpagehistory']['paid_amount']*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($Webstartpage['Webstartpagehistory']['discount']*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
						<div class="divtd textcenter vam">
							<?php echo $this->Time->format($SITECONFIG["timeformate"], $Webstartpage['Webstartpagehistory']['pruchase_date']); ?>
						</div>
					</div>
				<?php $i++;endforeach; ?>
			</div>
		</div>
		<?php if(count($Webstartpages)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
		<?php // My Popup Login ads table ends here ?>
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Webstartpagehistory']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
	<div class="pag-float-left">
		<div class="ul-bg">
			<ul class="nice_paging">
			<?php 
			foreach($resultperpage as $rpp)
			{
				?>
				<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
					<?php 
					echo $this->Form->create('Webstartpagehistory',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'traffic','action'=>'mystartpage/rpp')));
					echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
				
					echo $this->Js->submit($rpp, array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#Trafficpage',
					  'class'=>'resultperpagebutton',
					  'div'=>false,
					  'controller'=>'traffic',
					  'action'=>'mystartpage/rpp',
					  'url'   => array('controller' => 'traffic', 'action' => 'mystartpage/rpp')
					));
					echo $this->Form->end();
					?>
				</li>
				<?php 
			}?>
			</ul>
		<div class="clear-both"></div>
		</div>
	</div>
	<?php }?>
	<div class="floatright ul-bg">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
		</ul>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>
	
</div>

<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>