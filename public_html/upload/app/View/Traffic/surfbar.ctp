<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 13-12-2014
  * Last Modified: 13-21-2014
  *********************************************************************/
?>
<?php // This file is used to show PTC view page. Member earns through PTC ads using this page ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>[SEOTITLE] - [SITETITLE]</title>
	<meta content="[SEOKEYWORDS]" name="keywords">
	<meta content="[SEODESCRIPTION]" name="description">
	<meta name="robots" content="noindex,nofollow" />
	<style type="text/css">
		.clearboth{clear: both;}
		.heught6{height: 6px;}
		#wrapper{background-image: url("[SITEURL]img/gradient.jpg");background-repeat: repeat-x;height:100px;}
		#credittopframe{color: #ffffff;font-size: 13px;font-weight: bold;font-family: Arial;}
		#progressbar{border: 1px solid #c6deff;width: 183px;height: 28px;float: left;margin: 3px 0 0 0;}
		#progressbar div{width: 0px;margin-top: 2px;
			background-image: url("[SITEURL]img/progressbarsurf.jpg");
			background-repeat: repeat-x;height: 22px;}
		#displayCounter{width: 26px;height: 28px;float: left;padding: 5px 0 0 6px;text-align: center;
			color: #ffffff;font-size: 12px;font-weight: bold;font-family: Arial;margin-left: 5px;margin-top: 2px;
			background-image: url("[SITEURL]img/number.jpg?");
			background-repeat: no-repeat;}
		#header{margin: 0 auto;width: 950px;padding: 12px 0 0 0;}
		#header .logo img{
			-moz-border-radius:5px 5px 0px 0px;-webkit-border-radius:5px 5px 0px 0px;-khtml-border-radius:5px 5px 0px 0px;border-radius:5px 5px 0px 0px;
			background-repeat: no-repeat;
			background-position: right;
			background-color: #ffffff;
			
			height:56px;
		}
		#header .content{float: left;width: 350px;padding: 7px 0 0 8px;}
		#header .link1{display: none;float: left;font-family: arial;font-size: 12px;font-weight: bold;color: #2c2b2b;background-color: #cbd3d9;padding: 3px 15px;text-decoration: none;
		-moz-border-radius:5px;-webkit-border-radius:5px;-khtml-border-radius:5px;border-radius:5px;margin-right: 10px;}
		#header .link2{display: none;float: right;font-family: arial;font-size: 12px;font-weight: bold;color: #2c2b2b;background-color: #f1b700;padding: 3px 15px;text-decoration: none;
		-moz-border-radius:5px;-webkit-border-radius:5px;-khtml-border-radius:5px;border-radius:5px;}
		#header .link4{display: none;float: right;font-family: arial;font-size: 12px;font-weight: bold;color: #2c2b2b;background-color: #f1b700;padding: 3px 15px;text-decoration: none;
		-moz-border-radius:5px;-webkit-border-radius:5px;-khtml-border-radius:5px;border-radius:5px; margin-right: 10px;}
		#header .link3{float: left;font-family: arial;font-size: 12px;font-weight: bold;color: #174279;padding: 3px 15px;text-decoration: none;}
		#header .linkcaptcha{float: left;font-family: arial;font-size: 12px;font-weight: bold;color: #174279;padding: 3px 15px;text-decoration: none;display: none;}
		.formcapthcatextbox{border:none;background-color:#fff;border:1px solid #ddd;height:29px;width:20%;max-width:435px;min-width: 100px;padding-left:5px;margin-top:0px;color:#575757;vertical-align:top}.formcapthcatextbox:hover{ border: 1px solid #bfbfbf;}
		input.button,a.button{background:none;border:none;background-color:#f1b700;height:33px;padding-left:10px;padding-right:10px;text-align:center;line-height:34px;color:#000;text-decoration:none;display:inline-block;}input.button:hover,a.button:hover{background-color:#d09600;height:33px;padding-left:10px;padding-right:10px;text-align:center;line-height:34px;color:#000}input.button{line-height:30px}input.button:hover{line-height:30px}
                #credittopcaptcha .glyphicon{font-size: 30px;color: #bed9ff;margin-top: 0px !important;margin-bottom: 0px !important;}
		.mainImage{background-image: url("[SITEURL]img/surfarrow.jpg");background-repeat: no-repeat;background-position: 58px 13px;display: block;padding-right: 4px;}
		.mainImage .glyphicon{display: block;border: 2px solid #bed9ff;padding:5px 7px;border-radius:5px;}
		#credittopcaptcha table{margin-top: -5px !important;}
	</style>
        <link rel="stylesheet" type="text/css" href="[SITEURL]css/bootstrap.css" />
	<script type="text/javascript" src="[SITEURL]js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript">
            function checkImage(id){
                var optid = 'chk_'+id;
                var mainid = $(".mainImage").attr('id');
                if(optid === mainid){
                        //$("#frmCaptcha").submit();
                        traffic_post(optid);
                }else{
                        loadCaptcha();
	
                }
            }

            function loadCaptcha(){
                $.ajax({
                        url: '[SITEURL]/app/generateImageCaptcha/ajax',
                        success: function(res){
                                $("#credittopcaptcha").html(res);
                        }
                });
            }
        </script>
	<script type="text/javascript">
	function directory_counter_start(){
		//var cnt =10;
		var cnt =[DIRECTORYSECOND];
		var counter123 = setInterval(function() {
			if (cnt>0) 
			{
				$('#displayCounter').html(" "+cnt+" ");
				cnt--;
				
				curcount=[DIRECTORYSECOND]-cnt;
				$('#progressbar div').width((curcount*100)/[DIRECTORYSECOND]+"%");
			}
			else 
			{
				clearInterval(counter123);
				document.getElementById("credittopframe").innerHTML="";
				$.ajax({
																				url: '[SITEURL]traffic/creditcounter',
																				type: 'POST',
																				data: {},
																				success: function(data) {
						if (data=="SessionCreate")
						{
							$("#credittopcaptcha").show();
							$(".linkcaptcha").show();
						}
					}
																			});
				/*var xmlhttp;    
				if (window.XMLHttpRequest)
				{// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				}
				else
				{// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function()
				{
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				  {
					if (xmlhttp.responseText=="SessionCreate") {
						$("#credittopcaptcha").show();
					}
				  }
				}
				xmlhttp.open("GET","[SITEURL]traffic/creditcounter",true);
				xmlhttp.send();*/
				
			}
		},1000);
	}

            function traffic_post(str){
		document.getElementById("credittopframe").innerHTML="";
		var xmlhttp;    
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		  {
			var member=document.getElementById("credittopframe");
			if (xmlhttp.responseText=="captcha") {
				loadCaptcha();
				
			}
			else
			{
				$("#credittopcaptcha").hide();
				$(".linkcaptcha").hide();
				$('.link1, .link2, .link4').css({'display':'block'});
				if(member!==null) member.innerHTML=xmlhttp.responseText;
			}
		  }
		}
		xmlhttp.open("GET","[SITEURL]traffic/credit/[USERID]/[ID]/"+str,true);
		xmlhttp.send();
				
	}
	</script>
	[COUNTERSTART]
	</head>
	<body style="margin:0px;padding:0px;">
		<div id="wrapper">
			<div id="header">
				<div class="content">
					<div id="credittopframe">
						<div id="progressbar"><div>&nbsp;</div></div>
						<div id="displayCounter">[DIRECTORYSECOND]</div>
						<div class="clearboth"></div>
					</div>
					<div id="credittopcaptcha" style="display: none;">
                                                [IMAGE_CAPTCHA]
<!--						<input id="captchacode" class="formcapthcatextbox" type="text" name="data[Member][captchacode]">
						<img id="WebCaptcha" width="118" vspace="2" height="33" alt="" style="vertical-align: middle; margin-top: -1px;" src="[SITEURL]login/captcha_image?[RENDNUMBER]">
						<a href="javascript:void(0);" onclick="javascript:document.images.WebCaptcha.src='[SITEURL]login/captcha_image?' + Math.round(Math.random(0)*1000)+1 + ''"><img id="WebCaptcha" width="" vspace="2" height="" alt="" style="vertical-align: middle" src="[SITEURL]img/refreshsurf.png"></a>
						<input class="button" type="button" value="Submit" onclick="traffic_post();">-->
					</div>
					
				</div>
				<div style="display: inline-block; vertical-align: top;">[BANNER]</div>
				<div class="logo" style="margin-left:5px; display: inline-block;"><img src="[SITEURL]img/logo.png" width="180" height="56" /></div>
				<div class="clearboth"></div>
				
				<div class="heught6"></div>
				<span class="linkcaptcha">Click identical image to validate site view.</span>
				<span class="link3" style="float: right;">[TODAYCLICK]</span>
				<a target="_blank" class="link1" href="[TARGATURL]" style="float: right;">[OPENNEWWINDOW]</a>
				
				<a class="link4" href="[SITEURL]traffic/surf" style="float: left;">[NEXTSITE]</a>
				<a class="link2" href="[SITEURL]member/index" style="float: left;">[GOBACK]</a>
				<div class="clearboth"></div>
			</div>
		</div>
	[IFRAME]
	<script language="javascript">
		function getDocHeight() {	
		 var D = document;
			return Math.max(
				Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
				Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
				Math.max(D.body.clientHeight, D.documentElement.clientHeight)
			);
		}
		var x=getDocHeight();
		document.getElementById("f1").height=x-100;
	</script>
	</body>
</html>