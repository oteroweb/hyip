<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 12-12-2014
  * Last Modified: 12-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<div id="Trafficpage">
	<?php echo $this->Form->create('WebCaptcha',array('id' => 'WebCaptcha', 'type' => 'post', 'autocomplete'=>'off', 'onsubmit' => 'return false;','url'=>array('controller'=>'traffic','action'=>'credit')));?>
	<?php // Captcha Code starts here ?>
	<div class="captchrow form-row">
		<div class="form-col-1"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
			<?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formcapthcatextbox", 'div'=>false, 'label'=>false));?>
			<span><?php echo __('Code')?> :</span>
			<?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'MemberWebCaptcha','vspace'=>2, "style"=>"vertical-align: middle", 'width'=>'118', 'height'=>'44')); ?> 
				<a href="javascript:void(0);" onclick="javascript:document.images.MemberWebCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "style"=>"vertical-align: middle"));?></a>
	</div>
	<?php // Captcha Code ends here ?>
	<div class="formbutton">
		<?php echo $this->Js->submit(__('Update'), array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage2')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage2',
			  'div'=>false,
			  'class'=>'button',
			  'controller'=>'traffic',
			  'action'=>'credit',
			  'url'   => array('controller' => 'traffic', 'action' => 'credit')
		));?>
	</div>
	<?php echo $this->Form->end();?>
</div>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>