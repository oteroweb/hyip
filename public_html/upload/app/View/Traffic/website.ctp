<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 12-12-2014
  * Last Modified: 12-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($Enablewebsite==1) { ?>
<?php if(!$ajax){ ?>
<div id="Trafficpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>

<div class="comisson-bg mobilecss">
	<div class="commison-menu">
		<ul>
			<li>
				<?php
					echo $this->Js->link(__('My Websites'), array('controller'=>'traffic', "action"=>"website"), array(
						'update'=>'#Trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip act',
						'title'=>__('My Websites')
					));
				?>
			</li>
			<li>
				<?php
					echo $this->Js->link(__('My Start Page'), array('controller'=>'traffic', "action"=>"mystartpage"), array(
						'update'=>'#Trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip ',
						'title'=>__('My Start Page')
					));
				?>
			</li>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#Trafficpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'traffic', 'action'=>'website')
	));
	$currentpagenumber=$this->params['paging']['Website']['page'];
	?>
		<div class="padding-left-serchtabal">
			<?php echo $this->Js->link(__("Add New"), array('controller'=>'traffic', "action"=>"add"), array(
				'update'=>'#Trafficpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button'
			));?>
		</div>
		
		<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			
		<?php // My Popup Login ads table starts here ?>
		<div class="divtable">
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam">
						<?php 
						if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
						echo $this->Js->link(__("Id"), array('controller'=>'traffic', "action"=>"website/0/id/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Trafficpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Id')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Site Title"), array('controller'=>'traffic', "action"=>"website/0/title/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Trafficpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Site Title')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Used Credits"), array('controller'=>'traffic', "action"=>"website/0/display_counter/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Trafficpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Used Credits')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Allowed Credits"), array('controller'=>'traffic', "action"=>"website/0/total_click/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Trafficpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Allowed Credits')
						));?>
					</div>
					<div class="divth textcenter vam"><?php echo __("Status"); ?></div>
					<div class="divth textcenter vam"><?php echo __("Action"); ?></div>
				</div>
			</div>
			<div class="divtbody">
				<?php $i=1;
				foreach ($Websites as $Website):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam">#<?php echo $Website['Website']['id'];?></div>
						<div class="divtd textcenter vam">
							<p><b><?php echo __("Purchase Date"); ?> : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $Website['Website']['create_date']); ?></p>
								<a href="<?php echo $Website['Website']['site_url'];?>" target="_blank" class="vtip" title="<b><?php echo __("Title"); ?> : </b><?php echo $Website['Website']['title']; ?>">
								<?php echo $Website['Website']['site_url']; ?>
									</a>
								<div class="noborderonmobile"><b><?php echo __("Approved Date"); ?> : </b><?php if($Website['Website']['approve_date'] != '0000-00-00'){ echo $this->Time->format($SITECONFIG["timeformate"], $Website['Website']['approve_date']); } else { echo '-';}?></div>
						</div>
						<div class="divtd textcenter vam"><?php echo $Website['Website']['display_counter'];?></div>
						<div class="divtd textcenter vam"><?php echo $Website['Website']['total_click'];?></div>
						<div class="divtd textcenter vam">
							<?php 
							if($Website['Website']['total_click']<=$Website['Website']['display_counter'])
								echo __("Expired");
							elseif($Website['Website']['status']==0)
								echo __("Pending");
							elseif($Website['Website']['display_status']==0)
								echo __("Paused");
							else
								echo __("Running");
							?>
						</div>
						<div class="divtd textcenter vam">
							
							<div class="actionmenu">
								<div class="btn-group">
								  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
									  Action <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu">
									  <li>
									      <?php 
											if($Website['Website']['display_status']==0){
												$pauseaction='1';
												$pauseicon='pause.png';
												$pausetext='Unpause';
											}else{
												$pauseaction='0';
												$pauseicon='play.png';
												$pausetext='Pause';}
											echo $this->Js->link($this->html->image($pauseicon, array('alt'=>__($pausetext)))." ".__($pausetext), array('controller'=>'traffic', "action"=>"status/".$pauseaction."/".$Website['Website']['id']."/".$currentpagenumber), array(
												'update'=>'#Trafficpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));
										?>
									</li>
									 <li>
										<?php
											echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit')))." ".__('Edit'), array('controller'=>'traffic', "action"=>"add/".$Website['Website']['id']), array(
												'update'=>'#Trafficpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));
										?>
									 </li>
									 <li>
										<?php
											echo $this->Js->link($this->html->image('delete.png', array('alt'=>__('Delete')))." ".__('Delete'), array('controller'=>'traffic', "action"=>"remove/".$Website['Website']['id']), array(
												'update'=>'#Trafficpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));
										?>
									 </li>
								   </ul>
								</div>
							</div>
						  
						</div>
					</div>
				<?php $i++;endforeach; ?>
			</div>
		</div>
		<?php if(count($Websites)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
		<?php // My Popup Login ads table ends here ?>
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Website']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
	<div class="pag-float-left">
		<div class="ul-bg">
			<ul class="nice_paging">
			<?php 
			foreach($resultperpage as $rpp)
			{
				?>
				<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
					<?php 
					echo $this->Form->create('Website',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'traffic','action'=>'website/rpp')));
					echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
				
					echo $this->Js->submit($rpp, array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#Trafficpage',
					  'class'=>'resultperpagebutton',
					  'div'=>false,
					  'controller'=>'traffic',
					  'action'=>'website/rpp',
					  'url'   => array('controller' => 'traffic', 'action' => 'website/rpp')
					));
					echo $this->Form->end();
					?>
				</li>
				<?php 
			}?>
			</ul>
		<div class="clear-both"></div>
		</div>
	</div>
	<?php }?>
	<div class="floatright ul-bg">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
		</ul>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>
	
</div>

<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>