<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 11-12-2014
  * Last Modified: 11-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Traffic Exchange</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Start Page", array('controller'=>'traffic', "action"=>"startpage"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Start Page Member", array('controller'=>'traffic', "action"=>"startpagemember"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Pending Start Page Member", array('controller'=>'traffic', "action"=>"startpagememberpending"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Websites", array('controller'=>'traffic', "action"=>"website"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Website Credit Plans", array('controller'=>'traffic', "action"=>"plan"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Website Credit Plan Member", array('controller'=>'traffic', "action"=>"planmember"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("View Credits", array('controller'=>'traffic', "action"=>"webcredit"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
                        <li>
				<?php echo $this->Js->link("Website Surf History", array('controller'=>'traffic', "action"=>"surfhistory"), array(
					'update'=>'#trafficpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="trafficpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Biz_Directory#Plan_Member" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Startpagebook',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'traffic','action'=>'startpagememberpending')));?>
	
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
					<?php 
					echo $this->Form->input('searchby', array(
						  'type' => 'select',
						  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'webid'=>'Plan Id', 'member_id'=>'Member Id'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => '',
						  'onchange'=>''
					));
					?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain" id="SearchFor">
			<span>Search For :</span>
			<span class="searchforall"><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#trafficpage',
				'class'=>'searchbtn',
				'controller'=>'traffic',
				'action'=>'startpagememberpending/'.$planid,
				'url'=> array('controller' => 'traffic', 'action' => 'startpagememberpending/'.$planid)
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->


	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#trafficpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'traffic', 'action'=>'startpagememberpending/'.$planid)
	));
	$currentpagenumber=$this->params['paging']['Startpagebook']['page'];
	?>

<div id="gride-bg">
    <div class="Xpadding10">
	<?php echo $this->Form->create('Startpagebook',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'traffic','action'=>'startpagememberpending')));?>		
    <div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
      
	</div>
	<div class="clear-both"></div>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'traffic', "action"=>"startpagememberpending/".$planid."/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('M. Id', array('controller'=>'traffic', "action"=>"startpagememberpending/".$planid."/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Website Id', array('controller'=>'traffic', "action"=>"startpagememberpending/".$planid."/0/webid/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Website Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Book Date', array('controller'=>'traffic', "action"=>"startpagememberpending/".$planid."/0/bookdate/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Book Date'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Purchase Date', array('controller'=>'traffic', "action"=>"startpagememberpending/".$planid."/0/create_date/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Purchase Date'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Status', array('controller'=>'traffic', "action"=>"startpagememberpending/".$planid."/0/status/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#trafficpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Status'
					));?>
				</div>
				<div><?php echo 'Action';?></div>
			</div>
			<?php foreach ($Startpagebooks as $Startpagebook): ?>
				<div class="tablegridrow">
					<div><?php echo $Startpagebook['Startpagebook']['id']; ?></div>
					<div>
						<?php 
						echo $this->Js->link($Startpagebook['Startpagebook']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$Startpagebook['Startpagebook']['member_id']."/top/traffic/plan~top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>
						<a href="#" class="vtip" title="<b>Website Title : </b><?php echo $Startpagebook['Website']['title']; ?>"> <?php echo $Startpagebook['Startpagebook']['webid']; ?></a>
				    </div>
					<div class="textcenter"><?php echo $this->Time->format($SITECONFIG["timeformate"], $Startpagebook['Startpagebook']['bookdate']); ?></div>
					<div class="textcenter"><?php echo $this->Time->format($SITECONFIG["timeformate"], $Startpagebook['Startpagebook']['create_date']); ?></div>
					<div>
						<?php if($Startpagebook['Startpagebook']['status']==1){ echo 'Not Paid';}else{ echo 'Pending Processor';} ?>
				    </div>
					<div>
								<span class="actionmenu">
								  <span class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
										
											<li>
												<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete'))." Delete", array('controller'=>'traffic', "action"=>"startpagememberpendingremove/".$planid."/".$Startpagebook['Startpagebook']['id']."/".$currentpagenumber), array(
													'update'=>'#trafficpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>'',
													'confirm'=>"Do You Really Want to Delete This History?"
												));?>
											</li>
									</ul>
								  </span>
								</span>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($Startpagebooks)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Startpagebook']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Startpagebook',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'traffic','action'=>'startpagememberpending/'.$planid.'/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#trafficpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'traffic',
			  'action'=>'startpagememberpending/'.$planid.'/rpp',
			  'url'   => array('controller' => 'traffic', 'action' => 'startpagememberpending/'.$planid.'/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#trafficpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>