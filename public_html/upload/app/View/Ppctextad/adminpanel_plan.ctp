<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>		
<?php if(!$ajax){?>
<div class="whitetitlebox">Pay Per Click Text Ad</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("PPC Text Ad", array('controller'=>'ppctextad', "action"=>"member"), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("PPC Text Ad Plans", array('controller'=>'ppctextad', "action"=>"plan"), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Text Ad Widget", array('controller'=>'ppctextad', "action"=>"bannerwidget"), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">		
<div id="ppctextadpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Pay_Per_Click_-_PPC#PPC_Plans" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="serchmainbox">
   <div class="serchgreybox">
       Search Option
   </div>
   <?php echo $this->Form->create('Ppctextadplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ppctextad','action'=>'plan')));?>
   <div class="from-box">
        <div class="fromboxmain">
             <span>Search By :</span>
             <span>                     
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
							<?php 
							echo $this->Form->input('searchby', array(
								'type' => 'select',
								'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'plan_name'=>'Plan Name', 'total_click'=>'Clicks',  'all_country_price'=>'All Countries Price', 'geo_price'=>'GEO(Selected Countries) Price'),
								'selected' => $searchby,
								'class'=>'',
								'label' => false,
								'style' => '',
								'onchange'=>''
							));
							?>
						</label>
					</div>
				</div>
             </span>
		</div>
        <div class="fromboxmain">
				<span>Search For :</span>
				<span class='searchfor_all' style='display:<?php if($searchby=="banner_size"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?></span>
				<span class="padding-left">
					<?php echo $this->Js->submit('', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#ppctextadpage',
						'class'=>'searchbtn',
						'controller'=>'ppctextad',
						'action'=>'plan',
						'url'=> array('controller' => 'ppctextad', 'action' => 'plan')
					  ));?>
				</span>
        </div>
    </div>
    <?php echo $this->Form->end();?>
</div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#ppctextadpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'ppctextad', 'action'=>'plan')
	));
	$currentpagenumber=$this->params['paging']['Ppctextadplan']['page'];
	?>
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('ppctextad',$SubadminAccessArray) || in_array('ppctextad/adminpanel_planadd', $SubadminAccessArray)){ ?>
	<?php echo $this->Js->link("+ Add New", array('controller'=>'ppctextad', "action"=>"planadd"), array(
			'update'=>'#ppctextadpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btnorange'
	));?>
	<?php } ?>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Ppctextadplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ppctextad','action'=>'plan')));?>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'ppctextad', "action"=>"plan/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Plan Name', array('controller'=>'ppctextad', "action"=>"plan/0/0/plan_name/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Name'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Clicks', array('controller'=>'ppctextad', "action"=>"plan/0/0/total_click/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Clicks'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('All Countries Price', array('controller'=>'ppctextad', "action"=>"plan/0/0/all_country_price/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By All Countries Price'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('GEO Price', array('controller'=>'ppctextad', "action"=>"plan/0/0/geo_price/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By GEO Price'
					));?>
				</div>
				<div><?php echo 'Action';?></div>
           </div>
			<?php foreach ($ppcplandata as $ppcplan): ?>
				<div class="tablegridrow">
					<div><?php echo $ppcplan['Ppctextadplan']['id']; ?></div>
					<div><?php echo $ppcplan['Ppctextadplan']['plan_name']; ?></div>
					<div><?php echo $ppcplan['Ppctextadplan']['total_click']; ?></div>
					<div>$<?php echo $ppcplan['Ppctextadplan']['all_country_price']; ?></div>
					<div>$<?php echo $ppcplan['Ppctextadplan']['geo_price']; ?></div>
					<div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							
							<?php if(!isset($SubadminAccessArray) || in_array('ppctextad',$SubadminAccessArray) || in_array('ppctextad/adminpanel_planadd/$', $SubadminAccessArray)){ ?>
							<li>
								<?php
									echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit PPC Plan')).' Edit Plan', array('controller'=>'ppctextad', "action"=>"planadd/".$ppcplan['Ppctextadplan']['id']), array(
										'update'=>'#ppctextadpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('ppctextad',$SubadminAccessArray) || in_array('ppctextad/adminpanel_planstatus', $SubadminAccessArray)){ ?>
							<li>
								<?php 
									if($ppcplan['Ppctextadplan']['status']==0){
										$statusaction='1';
										$statusicon='red-icon.png';
										$statustext='Activate Plan';
									}else{
										$statusaction='0';
										$statusicon='blue-icon.png';
										$statustext='Inactivate Plan';}
									echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'ppctextad', "action"=>"planstatus/".$statusaction."/".$ppcplan['Ppctextadplan']['id']."/".$currentpagenumber), array(
										'update'=>'#ppctextadpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<?php } ?>
							<?php if(!isset($SubadminAccessArray) || in_array('ppctextad',$SubadminAccessArray) || in_array('ppctextad/adminpanel_member', $SubadminAccessArray)){ ?>
							<li>
								<?php 
									echo $this->Js->link($this->html->image('view-members.png', array('alt'=>'View Plan Members')).' View Plan Members', array('controller'=>'ppctextad', "action"=>"member/".$ppcplan['Ppctextadplan']['id']), array(
										'update'=>'#ppctextadpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('ppctextad',$SubadminAccessArray) || in_array('ppctextad/adminpanel_planremove', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Plan')).' Delete Plan', array('controller'=>'ppctextad', "action"=>"planremove/".$ppcplan['Ppctextadplan']['id']."/".$currentpagenumber), array(
									'update'=>'#ppctextadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'confirm'=>"Do You Really Want To Delete This Plan?"
								));?>
							</li>
							<?php } ?>
						  </ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($ppcplandata)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Ppctextadplan']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Ppctextadplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ppctextad','action'=>'plan/0/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#ppctextadpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'ppctextad',
			  'action'=>'plan/0/rpp',
			  'url'   => array('controller' => 'ppctextad', 'action' => 'plan/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>	
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#ppcpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>