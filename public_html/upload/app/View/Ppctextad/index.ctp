<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($Enableppc==1) { ?>
<?php if(!$ajax) { ?>
<div id="ppctextadpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<?php // Code for showing free available plans start
if(isset($pending_free_plans) && count($pending_free_plans)){
?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Free Plans Available");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php // PPC free Plans table starts here ?>
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam"><?php echo __('Plan Name');?></div>
				<div class="divth textcenter vam"><?php echo __("Action");?></div>
			</div>
	        </div>
	        <div class="divtbody">
		        <tr><td colspan="8" class="ovw-padding-tabal"></td></tr>
		        <?php
		        $i = 0;
		        foreach ($pending_free_plans as $pending_free_plan):
			       if($i%2==0){$class='white-color';}else{$class='gray-color';}
			       ?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $pending_free_plan['Ppctextadplan']['plan_name'];?></div>
					<div class="divtd textcenter vam">
					<?php
						echo $this->Js->link('<span class="purchasebutton getitbutton">Get it</span>', array('controller'=>'ppctextad', "action"=>"getfreeppc/".$pending_free_plan['Pending_free_plan']['id']), array(
							'update'=>'#ppctextadpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Get it')
						));
					?>
					</div>
				</div>
		        <?php $i++;endforeach; ?>
	       </div>
	</div>
	<?php // PPC free Plans table ends here ?>
</div>
<div class="height5"></div>
<?php } // Code for showing free available plans end ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("PPC Text Ad");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
		<?php // PPC plans table starts here ?>
		<div class="divtable">
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam"><?php echo __('Plan Name'); ?></div>
					<div class="divth textcenter vam"><?php echo __('Clicks');?></div>
					<div class="divth textcenter vam"><?php echo __('All Countries');?></div>
					<div class="divth textcenter vam"><?php echo __('Selected Countries').' '.__('Price');?></div>
					<div class="divth textcenter vam"><?php echo __('Action');?></div>
				</div>
			</div>
			<div class="divtbody">
				<?php $i=1;
				foreach ($ppcplandata as $ppcplan):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $ppcplan['Ppctextadplan']['plan_name']; ?></div>
						<div class="divtd textcenter vam"><?php echo $ppcplan['Ppctextadplan']['total_click']; ?></div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($ppcplan['Ppctextadplan']['all_country_price']*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($ppcplan['Ppctextadplan']['geo_price']*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
						<div class="divtd textcenter vam">
							<?php
								echo $this->Js->link('<span class="purchasebutton">Purchase</span>', array('controller'=>'ppctextad', "action"=>"purchase/".$ppcplan['Ppctextadplan']['id']), array(
									'update'=>'#ppctextadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>__('Purchase')
								));
							?>
						</div>
					</div>
				<?php $i++;endforeach; ?>
			</div>
		</div>
		<?php if(count($ppcplandata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
</div>
		<?php // PPC plans table ends here ?>
	
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('My PPC Text Ad');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#ppctextadpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'ppctextad', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Ppctextad']['page'];
	?>
	<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
	<div class="clear-both"></div>
	<div class="height5"></div>
		
		<?php // My PPC table starts here ?>
		<div class="divtable">
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam">
						<?php 
						if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
						echo $this->Js->link(__("Id"), array('controller'=>'ppctextad', "action"=>"index/0/id/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#ppctextadpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Id')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Plan"), array('controller'=>'ppctextad', "action"=>"index/0/ppc_id/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#ppctextadpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Plan')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Title"), array('controller'=>'ppctextad', "action"=>"index/0/title/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#ppctextadpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Title')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Displayed"), array('controller'=>'ppctextad', "action"=>"index/0/display_counter/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#ppctextadpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Displayed')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Clicked"), array('controller'=>'ppctextad', "action"=>"index/0/click_counter/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#ppctextadpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Clicked')
						));?>
					</div>
					<div class="divth textcenter vam"><?php echo __("Status"); ?></div>
					<div class="divth textcenter vam"><?php echo __("Action"); ?></div>
				</div>
			</div>
			<div class="divtbody">
				<?php $i=1;
				foreach ($Ppcbanners as $Ppcbanner):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $Ppcbanner['Ppctextad']['id'];?></div>
						<div class="divtd textcenter vam">
							<a href="#" class='vtip' title="<?php echo __('Plan Name'); ?> : <?php echo $Ppcbanner['Ppctextadplan']['plan_name'];?>"><?php echo $Ppcbanner['Ppctextad']['ppc_id'];?></a>
						</div>
						
						<div class="divtd textcenter vam">
							<p><b><?php echo __("Purchase Date"); ?> : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $Ppcbanner['Ppctextad']['purchasedate']); ?></p>
							<a href="<?php echo $Ppcbanner['Ppctextad']['site_url'];?>" target="_blank" class="vtip" title="<b><?php echo $Ppcbanner['Ppctextad']['title']."<br>".$Ppcbanner['Ppctextad']['description1']."<br>".$Ppcbanner['Ppctextad']['description2']; ?> </b>">
								<?php echo $Ppcbanner['Ppctextad']['title']; ?>
							</a>
							<div><b><?php echo __("Display Start Date"); ?> : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $Ppcbanner['Ppctextad']['start_date']); ?></div>
						</div>
						<div class="divtd textcenter vam"><?php echo $Ppcbanner['Ppctextad']['display_counter'];?></div>
						<div class="divtd textcenter vam">
							<?php if($Ppcbanner['Ppctextad']['display_area']!='all'){?>
								<a href="<?php echo $SITEURL;?>ppctextad/geo/<?php echo $Ppcbanner['Ppctextad']['id'];?>" class="iframeclass">
									<span class="vtip" title="<?php echo __('Click here to view country wise clicks'); ?>"><?php echo $Ppcbanner['Ppctextad']['click_counter'];?></span>
								</a>
								<div style="display:none;"><div id="inlinebox<?php echo $Ppcbanner['Ppctextad']['id'];?>"><?php echo $Ppcbanner['Ppctextad']['click_counter'];?></div></div>
							<?php }else{
								echo $Ppcbanner['Ppctextad']['click_counter'];
							}?>
						</div>
						<div class="divtd textcenter vam">
							<?php 
							if($Ppcbanner['Ppctextad']['total_click']<=$Ppcbanner['Ppctextad']['click_counter'])
								echo __("Expired");
							elseif($Ppcbanner['Ppctextad']['pause']==0)
								echo __("Paused");
							elseif($Ppcbanner['Ppctextad']['status']==1) 
								echo __("Running");
							elseif($Ppcbanner['Ppctextad']['status']==0)
								echo __("Pending");
							?>
						</div>
						<div class="divtd textcenter vam">
							
							<div class="actionmenu">
								<div class="btn-group">
								  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
									  Action <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu">
									  <li>
									      <?php 
											if($Ppcbanner['Ppctextad']['pause']==0){
												$pauseaction='1';
												$pauseicon='pause.png';
												$pausetext='Unpause PPC Text Ad';
											}else{
												$pauseaction='0';
												$pauseicon='play.png';
												$pausetext='Pause PPC Text Ad';}
											echo $this->Js->link($this->html->image($pauseicon, array('alt'=>__($pausetext)))." ".__($pausetext), array('controller'=>'ppctextad', "action"=>"status/".$pauseaction."/".$Ppcbanner['Ppctextad']['id']."/".$currentpagenumber), array(
												'update'=>'#ppctextadpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));
										?>
									</li>
									 <li>
										<?php
											echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit PPC Text Ad')))." ".__('Edit PPC Text Ad'), array('controller'=>'ppctextad', "action"=>"add/".$Ppcbanner['Ppctextad']['id']), array(
												'update'=>'#ppctextadpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));
										?>
									 </li>
								   </ul>
								</div>
							</div>
						  
						</div>
					</div>
				<?php $i++;endforeach; ?>
			</div>
		</div>
		<?php if(count($Ppcbanners)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
		<?php // My PPC table ends here ?>
	
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Ppctextad']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
	<div class="pag-float-left">
		<div class="ul-bg">
			<ul class="nice_paging">
			<?php 
			foreach($resultperpage as $rpp)
			{
				?>
				<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
					<?php 
					echo $this->Form->create('Ppctextadplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ppctextad','action'=>'index/rpp')));
					echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
					
					echo $this->Js->submit($rpp, array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#ppctextadpage',
					  'class'=>'resultperpagebutton',
					  'div'=>false,
					  'controller'=>'ppctextad',
					  'action'=>'index/rpp',
					  'url'   => array('controller' => 'ppctextad', 'action' => 'index/rpp')
					));
					echo $this->Form->end();
					?>
				</li>
				<?php 
			}?>
			</ul>
		<div class="clear-both"></div>
		</div>
	</div>
	<?php }?>
	<div class="floatright ul-bg">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
		</ul>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>

</div>
<?php if(!$ajax) { ?>
</div>
<?php }
	} else { echo __('This page is disabled by administrator'); } ?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>