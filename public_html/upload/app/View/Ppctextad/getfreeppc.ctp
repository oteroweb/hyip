<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 25-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('framechecker');?>
<?php if($Enableppc==1) { ?>
<?php if(!$ajax) { ?>
<div id="ppctextadpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<?php //Frame Break Code Start ?>
<input type="hidden" id='framebreakmessage' value='<?php echo __('Destination URL Not Valid'); ?>|<?php echo __('Breaking out of frames'); ?>|<?php echo __('Inspecting Website'); ?>|<?php echo __('Framebreaker test passed'); ?>|<?php echo __('Error').' : '; ?>'/>
<div id="check" class="frambreakmain">
	<div class='textleft'>
		<span id='framebreakimg'></span>
		<span class="framebreaktext"><?php echo __('Inspecting Framebreaker'); ?></span>
	</div>
	<div class='textleft'>
		<span id="check_final"><?php echo __('Inspecting Website'); ?></span>
	</div>
</div>
<?php //Frame Break Code Over ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Get Free PPC Text Ad");?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
		<?php // PPC purchase form starts here ?>
		<?php echo $this->Form->create('Ppctextad',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'ppctextad','action'=>'getfreeppcaction/'.$pending_free_plan_id)));?>
		<div class="form-box">
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Plan Name");?> : </div>
				<div class="form-col-2 form-text"><?php echo $Ppcplandata['Ppctextadplan']['plan_name'];?></div>
			</div>
			
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Total Clicks");?> : </div>
				<div class="form-col-2 form-text"><div id="totalclick"><?php echo $Ppcplandata['Ppctextadplan']['total_click'];?></div></div>
			</div>
			<?php if($Ppcplandata['Ppctextadplan']['fpcommission']==1){?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Pay Commission For The First Position Only");?> : </div>
				<div class="form-col-2 form-text"><?php echo __("Yes");?></div>
			</div>
			<?php }?>
			
			<?php $commissionlevel=explode(",", $Ppcplandata['Ppctextadplan']['commissionlevel']);if($commissionlevel[0]>0){?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Referral Commission Structure");?> : </div>
				<div class="form-col-2 form-text">
					<?php $commilev="";for($i=0;$i<count($commissionlevel);$i++){if($commissionlevel[$i]>0){$commilev.="Level ".($i+1)." : ".$commissionlevel[$i]."%, ";}else{break;}}echo trim($commilev,", ")?>
				</div>
			</div>
			<?php }?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Title");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('title', array('type'=>'text','div'=>false, 'label' => false, 'class'=>'formtextbox'));?>
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?> <?php echo __('Maximum characters allowed').' : '.$SITECONFIG["max_title"]; ?>"></span>
				</div>
			</div>
			
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Description Line 1");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('description1', array('type'=>'text', 'label' => false,'div'=>false, 'class'=>'formtextbox login-from-box-1'));?>
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?> <?php echo __('Maximum characters allowed').' : '.$SITECONFIG["max_desc1"]; ?>"></span>
			</div>
			
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Description Line 2");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('description2', array('type'=>'text', 'label' => false,'div'=>false, 'class'=>'formtextbox login-from-box-1'));?>
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?> <?php echo __('Maximum characters allowed').' : '.$SITECONFIG["max_desc2"]; ?>"></span>
			</div>
			
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Site URL");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('site_url', array('type'=>'text', 'label' => false,'div'=>false, 'value'=>'http://', 'class'=>'formtextbox txtframebreaker'));?>
			</div>
			<div class="form-row">
					<div class="form-col-1"><?php echo __('Show URL in Zone')?> : </div>
					<div class="form-col-2 form-text">
						<?php if($Ppcbannerdata['Ppctextad']["showurl"]){$checked="checked";}else{$checked="";}
							echo $this->Form->input('showurl', array('type' => 'checkbox', 'div'=>'true', 'label' =>'', 'checked'=>$checked));?>
					</div>
				</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Display Start Date");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('start_date', array('type'=>'text', 'id'=>'fromdate', 'value'=>date('Y-m-d'), 'style'=>'max-width:185px', 'div' => false, 'label' => false, 'class'=>'formtextbox datepickerajax'));?></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Daily Clicks Limit");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('daily_budget', array('type'=>'text', 'label' => false, 'class'=>'formtextbox', 'div' => false));?>	
				<span class="helptooltip vtip" title="<?php echo __('Specify the maximum possible clicks per day for this banner. It will not be shown once this number is reached.'); ?>"></span>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Display Area");?> : <span class="required">*</span></div>
				<div class="form-col-2 form-text forradioalignment">
					<?php 
						echo $this->Form->radio('display_area', array('all' => __('All Countries'), 'selected' => __('Selected Countries')), array('value'=>'all', 'legend' => false, 'separator'=>'&nbsp;&nbsp;', 'onchange'=>'if(this.value=="all"){$(".c2").show(500);$(".c1").hide(500);var amount1=($("#priceall").text()/$("#totalclick").text());$(".rate").html("'.$Currency['prefix'].'"+amount1+"'.' '.$Currency['suffix'].'");getpriceamount(0);}else if(this.value=="selected"){$(".c2").hide(500);$(".c1").show(500);var amount1=($("#pricegeo").text()/$("#totalclick").text());$(".rate").html("'.$Currency['prefix'].'"+amount1+"'.' '.$Currency['suffix'].'");getpriceamount(1);}'));
					?>
				</div>
			</div>
			<div class="tabal-content-white  c1" style="display: none;">
				<fieldset>
					<div class="line-height" style="height:250px;overflow:auto;">
						<?php
						$ccounter=0;
						foreach($countries as $id => $val)
						{ ?>
							<div class="float-left-profile line-height" style="min-width:170px;">
								<div class="profile-bot-left line-height" style="padding-top:0px;">
									<?php 
										echo $this->Form->checkbox('country.', array(
										  'value' => trim($val),
										  'class' => '',
										  'div'=>false,
										  'style'=>'display:inline',
										  'hiddenField' => false
										));
									?>
								</div>
								<div class="float-left-profile line-height"><?php echo trim($val); ?></div>
							</div>
						<?php	$ccounter++;
						if($ccounter%3==0)
						{
							echo '<div class="clear-both"></div>';
						}
					 }?>
					</div>
				</fieldset>
			</div>
			<div class="height10"></div>
			<div>
				<div class="purchaseplanbuttonbox floatright" style="width:50%;">
					<div class="form-box">
						<div class="profile-bot-left floatright"><?php if($SITECONFIG["enableagree"]==1){echo $this->Form->input('termsandconditions', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>'')); echo ' <a href="'.$SITEURL.'public/terms" target="_blank">'.__('I agree with the terms and conditions').'</a> '.__('of').' '.$SITECONFIG["sitetitle"];} ?></div>
						<div class="clear-right"></div>
						<div class="clear-both"></div>
						<div class="formbutton">
							
							<input type="submit" value="<?php echo __('Purchase'); ?>" class="button ml10" onclick="return checkframebreaker(0,'<?php echo $SITEURL; ?>','framebreaker');" />
							<?php echo $this->Js->submit(__('Purchase'), array(
								'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'update'=>'#UpdateMessage',
								'class'=>'button framebreaker',
								'style'=>'display:none',
								'div'=>false,
								'controller'=>'ppctextad',
								'action'=>'getfreeppcaction',
								'url'   => array('controller' => 'ppctextad', 'action' => 'getfreeppcaction/'.$pending_free_plan_id)
							));?>
							<?php echo $this->Js->link(__("Back"), array('controller'=>'ppctextad', "action"=>"index"), array(
								'update'=>'#ppctextadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'button',
								'style'=>''
							));?>
						</div>
						<div class="clear-both"></div>
					</div>
				</div>
				<div class="clear-both"></div>
			</div>
		</div>
		<?php echo $this->Form->end();?>
		<?php // PPC purchase form ends here ?>
	</div>
<?php if(!$ajax) { ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>