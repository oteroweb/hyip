<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Pay Per Click Text Ad</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("PPC Text Ad", array('controller'=>'ppctextad', "action"=>"member"), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li >
					<?php echo $this->Js->link("PPC Plans Text Ad", array('controller'=>'ppctextad', "action"=>"plan"), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Text Ad Widget", array('controller'=>'ppctextad', "action"=>"bannerwidget"), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">		
<div id="ppctextadpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Pay_Per_Click_-_PPC#Banner_Widget" target="_blank">Help</a></div>			
<?php $widgetdivid=date("ihYdms");?>
<script type="text/javascript">
function showwidgetcode(chk,size,only)
{
	if(chk.checked==true)
	{
		document.getElementById("bannerwidget").innerHTML='&lt;div id="widget_<?php echo $widgetdivid;?>"&gt;&lt;script type="text/javascript" src="<?php echo $SITEURL;?>widget/ppctextad/normal/widget_<?php echo $widgetdivid;?>/'+size+'/"&gt;&lt;/script>&lt;/div&gt;';
		document.getElementById("current").value=size;
		
		var SRC = document.getElementById("widgetimage").src.split('/');
		var STRSRC = document.getElementById("widgetimage").src.replace(SRC[SRC.length-1], 'Text_widget2.jpg');
		document.getElementById("widgetimage").src=STRSRC;	
	}
	else
	{
		document.getElementById("bannerwidget").innerHTML='&lt;table id="popup_main" width="180px" cellpadding="0" cellspacing="0"&gt;<tr&gt;&lt;td valign="top"&gt;&lt;div id="widget_<?php echo $widgetdivid;?>"&gt;&lt;script type="text/javascript" src="<?php echo $SITEURL;?>widget/ppctextad/custom/widget_<?php echo $widgetdivid;?>/'+size+'/"&gt;&lt;/script&gt;&lt;/div&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;';
		document.getElementById("current").value=size;

		var SRC = document.getElementById("widgetimage").src.split('/');
		var STRSRC = document.getElementById("widgetimage").src.replace(SRC[SRC.length-1], 'Text_widget1.jpg');
		document.getElementById("widgetimage").src=STRSRC;
	
	}
}
</script>
<form name="form3" id="form3" method="post" >
	  <div class="frommain">
		    <div class="nameandbox textadwidget">
			      <?php echo 'Number of Text Ads';?> :
			      <div class="fromborderdropedown">
					<div class="select-main">
						  <label>
							    <?php 
							    echo $this->Form->input('noofshow', array(
								    'type' => 'select',
								    'options' => array('1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'9', '10'=>'10'),
								    'class'=>'',
								    'div' => false,
								    'label' => false,
								    'onchange' => 'showwidgetcode(this.form.theme,this.value,1);',
								    'style' => ''
							    ));
							    ?>
						  </label>
					</div>
			      </div>
				  
				  
			      <input type="hidden" name="current" id="current" value="125">
			      <div class="checkbox">
					<input id="withoutnotice" type="checkbox" name="theme" onclick="showwidgetcode(this,this.form.noofshow.value,1);" />
					<label for="withoutnotice"><?php echo 'Without powered by notice';?></label>
			      </div>
		    </div>
		    <div class="nameandbox">
			    <?php echo $this->html->image("Text_widget1.jpg", array("alt"=>"", "id"=>"widgetimage"));?>
		    </div>
		    <div class="nameandbox">
				  <textarea id="bannerwidget" class="from-textarea" style="">&lt;table id="popup_main" width="180px" cellpadding="0" cellspacing="0"&gt;<tr&gt;&lt;td valign="top"&gt;&lt;div id="widget_<?php echo $widgetdivid;?>"&gt;&lt;script type="text/javascript" src="<?php echo $SITEURL;?>widget/ppctextad/custom/widget_<?php echo $widgetdivid;?>"&gt;&lt;/script&gt;&lt;/div&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</textarea>
		    </div>
	  
	  </div>
<?php if(!$ajax){?>
</div><!--#balancetransferpage over-->
</div>
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>