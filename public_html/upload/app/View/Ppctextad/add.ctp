<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('framechecker');?>
<?php if($Enableppc==1) { ?>
<?php if(!$ajax) { ?>
<div id="ppctextadpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>

<?php //Frame Break Code Start ?>
<input type="hidden" id='framebreakmessage' value='<?php echo __('Destination URL Not Valid'); ?>|<?php echo __('Breaking out of frames'); ?>|<?php echo __('Inspecting Website'); ?>|<?php echo __('Framebreaker test passed'); ?>|<?php echo __('Error').' : '; ?>'/>
<div id="check" class="frambreakmain">
	<div class='textleft'>
		<span id='framebreakimg'></span>
		<span class="framebreaktext"><?php echo __('Inspecting Framebreaker'); ?></span>
	</div>
	<div class='textleft'>
		<span id="check_final"><?php echo __('Inspecting Website'); ?></span>
	</div>
</div>
<?php //Frame Break Code Over ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("PPC Text Ad");?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
		<?php // Edit PPC form starts here ?>
		<?php echo $this->Form->create('Ppctextad',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'ppctextad','action'=>'addaction')));?>
		<div class="form-box">
			<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$Ppcbannerdata['Ppctextad']["id"], 'label' => false)); ?>
			
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Title");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('title', array('type'=>'text', 'label' => false, 'value'=>$Ppcbannerdata['Ppctextad']['title'],'div'=>false, 'class'=>'formtextbox login-from-box-1'));?>
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?> <?php echo __('Maximum characters allowed').' : '.$SITECONFIG["max_title"]; ?>"></span>
			</div>
			
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Description Line 1");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('description1', array('type'=>'text', 'label' => false, 'value'=>$Ppcbannerdata['Ppctextad']['description1'],'div'=>false, 'class'=>'formtextbox login-from-box-1'));?>
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?> <?php echo __('Maximum characters allowed').' : '.$SITECONFIG["max_desc1"]; ?>"></span>
			</div>
			
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Description Line 2");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('description2', array('type'=>'text', 'label' => false, 'value'=>$Ppcbannerdata['Ppctextad']['description2'],'div'=>false, 'class'=>'formtextbox login-from-box-1'));?>
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?> <?php echo __('Maximum characters allowed').' : '.$SITECONFIG["max_desc2"]; ?>"></span>
			</div>
			
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Site URL");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('site_url', array('type'=>'text', 'label' => false,'div'=>false, 'value'=>$Ppcbannerdata['Ppctextad']['site_url'], 'class'=>'formtextbox txtframebreaker'));?>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Daily Clicks Limit");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('daily_budget', array('type'=>'text', 'label' => false, 'value'=>$Ppcbannerdata['Ppctextad']['daily_budget'], 'class'=>'formtextbox login-from-box-1', 'div' => false));?>
				
				<span class="helptooltip vtip" title="<?php echo __('Specify the maximum possible clicks per day for this banner. It will not be shown once this number is reached.'); ?>"></span>
			</div>
			<div class="form-row">
					<div class="form-col-1"><?php echo __('Show URL in Zone')?> : </div>
					<div class="form-col-2 form-text">
						<?php if($Ppcbannerdata['Ppctextad']["showurl"]){$checked="checked";}else{$checked="";}
							echo $this->Form->input('showurl', array('type' => 'checkbox', 'div'=>'true', 'label' =>'', 'checked'=>$checked));?>
					</div>
				</div>
			<?php if($Ppcbannerdata['Ppctextad']['display_area']=='all'){ ?>
			<div class="form-row">
				<div class="form-col-1 form-text"><?php echo __("Display Area");?> : <span class="required">*</span></div>
				<div class="form-col-2">
					<?php echo __("All Countries");?>
				</div>
			</div>
			<?php } else { ?>
			<div class="tabal-content-white">
					<fieldset>
						<div style="height:250px;overflow:auto;">
							<?php
							$ccounter=0;
							$replacecountry=array('India','Dominica','Guinea','Mali','Netherlands','Niger','Oman','Samoa','Curacao');
							$country=array('India_2','Dominica_2','Guinea_2','Mali_2','Netherlands_2','Niger_2','Oman_2','Samoa_2','Netherlands Antilles');
							$display_area=str_replace($country,$replacecountry,$Ppcbannerdata['Ppctextad']["display_area"]);
							$countrynames=@explode(",", $display_area);
							foreach($countries as $id => $val)
							{
								if(in_array(trim($val), $countrynames)){$selected = 'checked';}else{$selected = '';} ?>
								<div class="float-left-profile line-height" style="width:215px;">
									<div class="profile-bot-left line-height"  style="padding-top:0px;">
										<?php 
											echo $this->Form->checkbox('country.', array(
											  'value' => trim($val),
											  'class' => '',
											  'div'=>false,
											  'checked'=>$selected,
											  'style'=>'display:inline',
											  'hiddenField' => false
											));
										?>
									</div>
									<div class="float-left-profile line-height"><?php echo trim($val); ?></div>
								</div>
							<?php	$ccounter++;
							if($ccounter%3==0)
							{
								echo '<div class="clear-both"></div>';
							}
						 }?>
						</div>
					</fieldset>
					
			</div>
			<?php } ?>
		</div>
		
		<div class="height10"></div>
		<div class="formbutton">
			<?php echo $this->Js->link(__("Back"), array('controller'=>'ppctextad', "action"=>"index"), array(
				'update'=>'#ppctextadpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button',
				'style'=>'float:left'
			));?>
		
			<input type="submit" value="<?php echo __('Update'); ?>" class="button" onclick="return checkframebreaker(0,'<?php echo $SITEURL; ?>','framebreaker');" />
			<?php echo $this->Js->submit(__('Update'), array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'button framebreaker',
				'style'=>'display:none',
				'div'=>false,
				'controller'=>'ppctextad',
				'action'=>'addaction',
				'url'   => array('controller' => 'ppctextad', 'action' => 'addaction')
			));?>
		</div>
		<div class="clear-both"></div>
		<?php echo $this->Form->end();?>
		<?php // Edit PPC form ends here ?>
		
	</div>
<?php if(!$ajax) { ?>
</div>
<?php } ?>	
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>