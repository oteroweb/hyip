<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<script>
$(document).ready(function(){
	//$("a[rel='lightboxtext']").colorbox({width:"80%", height:"80%", inline:true, href:"#"+atag.title});
	$(".iframeclass").colorbox({width:"80%", height:"80%", iframe:true});
});
</script>
<?php if(!$ajax){?>
<div class="whitetitlebox">Pay Per Click Text Ad</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("PPC Text Ad", array('controller'=>'ppctextad', "action"=>"member"), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li >
					<?php echo $this->Js->link("PPC Plans Text Ad", array('controller'=>'ppctextad', "action"=>"plan"), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Text Ad Widget", array('controller'=>'ppctextad', "action"=>"bannerwidget"), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">		
<div id="ppctextadpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Pay_Per_Click_-_PPC#PPC_Banners" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="serchmainbox">
		<?php echo $this->Form->create('Ppctextad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ppctextad','action'=>'member')));?>		
            <div class="serchgreybox">
                Search Option
            </div>
            <div class="from-box">
                <div class="fromboxmain">
                    <span>Search By :</span>
                    <span>                     
					  <div class="searchoptionselect">
						<div class="select-main">
							<label>
								<?php 
								echo $this->Form->input('searchby', array(
									  'type' => 'select',
									  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'member_id'=>'Member Id', 'title'=>'Title', 'display_counter'=>'Displayed', 'click_counter'=>'Clicked', 'running'=>'Running TextAd', 'expire'=>'Expire TextAd', 'approve'=>'Approved TextAd', 'unapprove'=>'Disapproved TextAd', 'pause'=>'Paused TextAd', 'unpause'=>'Active(Unpaused) TextAd'),
									  'selected' => $searchby,
									  'class'=>'',
									  'label' => false,
									  'style' => '',
									  'onchange'=>'if($(this).val()=="approve" || $(this).val()=="unapprove" || $(this).val()=="pause" || $(this).val()=="unpause" || $(this).val()=="expire" || $(this).val()=="running"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);}'
								));
								?>
							</label>
						</div>
					  </div>
                    </span>
				 </div>
                 <div class="fromboxmain" id="SearchFor" <?php if($searchby=="approve" || $searchby=="unapprove" || $searchby=="pause" || $searchby=="unpause" || $searchby=='expire' || $searchby=='running'){ echo 'style="display:none"';} ?>>
                    <span>Search For :</span>
                    <span class='searchfor_all' style='display:<?php if($searchby=="banner_size"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?></span>
                </div>
             </div>
             <div class="from-box">
                <div class="fromboxmain width480">
                    <span>From :</span>
                    <span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
                </div>
                 <div class="fromboxmain">
                    <span>To :</span>
                    <span><?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
                    <span class="padding-left">
						<?php echo $this->Js->submit('', array(
								'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'update'=>'#ppctextadpage',
								'class'=>'searchbtn',
								'controller'=>'ppctextad',
								'action'=>'member/'.$planid,
								'url'=> array('controller' => 'ppctextad', 'action' => 'member/'.$planid)
						));?>
					</span>
                 </div>
            </div>
	 <?php echo $this->Form->end();?>		 
</div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#ppctextadpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'ppctextad', 'action'=>'member/'.$planid)
	));
	$currentpagenumber=$this->params['paging']['Ppctextad']['page'];
	?>
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Ppctextad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ppctextad','action'=>'member')));?>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'ppctextad', "action"=>"member/".$planid."/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div><?php echo 'Plan';?></div>
                <div>
					<?php 
					echo $this->Js->link('M. Id', array('controller'=>'ppctextad', "action"=>"member/".$planid."/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Title', array('controller'=>'ppctextad', "action"=>"member/".$planid."/0/title/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Title'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Displayed', array('controller'=>'ppctextad', "action"=>"member/".$planid."/0/display_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Displayed'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Clicked', array('controller'=>'ppctextad', "action"=>"member/".$planid."/0/click_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Clicked'
					));?>
				</div>
				<div><?php echo 'Action';?></div>
            </div>
			<?php foreach ($ppcbannerdata as $ppcbanner): ?>
				<div class="tablegridrow">
					<div><?php echo $ppcbanner['Ppctextad']['id']; ?></div>
					<div>
						<a href="#" class="vtip" title="<b>Plan Name : </b><?php echo $ppcbanner['Ppctextadplan']['plan_name']; ?>"><?php echo $ppcbanner['Ppctextad']['ppc_id'];?></a>
					</div>
					<div>
						<?php 
						echo $this->Js->link($ppcbanner['Ppctextad']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$ppcbanner['Ppctextad']['member_id']."/top/ppctextad/index~top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div class="textcenter">
						<br/>
						<b>Purchase Date : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $ppcbanner['Ppctextad']['purchasedate']); ?>
						<br/>
						<a href="<?php echo $ppcbanner['Ppctextad']['site_url'];?>" target="_blank" class="vtip" title="<?php echo $ppcbanner['Ppctextad']['description1']; ?><br><?php echo $ppcbanner['Ppctextad']['description2']; ?>">
							<?php echo $ppcbanner['Ppctextad']['title']; ?>
						</a>
						<br/>
						<b>Display Start Date : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $ppcbanner['Ppctextad']['start_date']); ?>
						<br/><br/>
					</div>
					<div><?php echo $ppcbanner['Ppctextad']['display_counter']; ?></div>
					<div>
						<?php if($ppcbanner['Ppctextad']['display_area']!='all'){?>
							<a href="<?php echo $ADMINURL;?>/ppctextad/geo/<?php echo $ppcbanner['Ppctextad']['id'];?>" class="iframeclass">
								<span class="vtip" title="Click here to view country wise clicks"><?php echo $ppcbanner['Ppctextad']['click_counter'];?></span>
							</a>
							<div style="display:none;"><div id="inlinebox<?php echo $ppcbanner['Ppctextad']['id'];?>"><?php echo $ppcbanner['Ppctextad']['click_counter'];?></div></div>
						<?php }else{
							echo $ppcbanner['Ppctextad']['click_counter'];
						}?>
					</div>
					<div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							
							<?php if(!isset($SubadminAccessArray) || in_array('ppctextad',$SubadminAccessArray) || in_array('ppctextad/adminpanel_add/$', $SubadminAccessArray)){ ?>
						  	<li>
								<?php
									echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit PPC Textad')).' Edit PPC Textad', array('controller'=>'ppctextad', "action"=>"add/".$planid."/".$ppcbanner['Ppctextad']['id']), array(
										'update'=>'#ppctextadpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<?php } ?>
							
						  	<?php if(!isset($SubadminAccessArray) || in_array('ppctextad',$SubadminAccessArray) || in_array('ppctextad/adminpanel_status', $SubadminAccessArray)){ ?>
							<li>
								<?php 
									if($ppcbanner['Ppctextad']['pause']==0){
										$pauseaction='1';
										$pauseicon='pause.png';
										$pausetext='Unpause PPC Textad';
									}else{
										$pauseaction='0';
										$pauseicon='play.png';
										$pausetext='Pause PPC Textad';}
									echo $this->Js->link($this->html->image($pauseicon, array('alt'=>$pausetext)).' '.$pausetext, array('controller'=>'ppctextad', "action"=>"pause/".$planid."/".$pauseaction."/".$ppcbanner['Ppctextad']['id']."/".$currentpagenumber), array(
										'update'=>'#ppctextadpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<li>
								<?php 
									if($ppcbanner['Ppctextad']['status']==0){
										$statusaction='1';
										$statusicon='red-icon.png';
										$statustext='Approve PPC Textad';
									}else{
										$statusaction='0';
										$statusicon='blue-icon.png';
										$statustext='Disapprove PPC Textad';}
									echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'ppctextad', "action"=>"status/".$planid."/".$statusaction."/".$ppcbanner['Ppctextad']['id']."/".$currentpagenumber), array(
										'update'=>'#ppctextadpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('ppctextad',$SubadminAccessArray) || in_array('ppctextad/adminpanel_remove', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete PPC Banner')).' Delete PPC Textad', array('controller'=>'ppctextad', "action"=>"remove/".$planid."/".$ppcbanner['Ppctextad']['id']."/".$currentpagenumber), array(
									'update'=>'#ppctextadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'confirm'=>"Do You Really Want To Delete This PPC Textad?"
								));?>
							</li>
							<?php } ?>
						  </ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($ppcbannerdata)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Ppctextad']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Ppctextad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ppctextad','action'=>'member/'.$planid.'/rpp')));?>
		<div class="resultperpage">
			<label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#ppctextadpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'ppctextad',
			  'action'=>'member/'.$planid.'/rpp',
			  'url'   => array('controller' => 'ppctextad', 'action' => 'member/'.$planid.'/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#ppcpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>