<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($webexposure==1) { ?>
<?php if(!$ajax) { ?>
<div id="ppctextadpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Website Exposure");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<div style="vertical-align: middle;text-align: center; padding-bottom: 5px;"><?php echo $banners728ppc[0]; ?></div>
	<div style="vertical-align: middle;text-align: center; padding-bottom: 5px;">
		<div style="text-align: center; display: inline-block; width: 130px;">
			<div style="padding-bottom: 5px;">
			<?php echo $banners125ppc[0]; ?>
			</div>
			<?php echo $banners125ppc[1]; ?>
		</div>
		<div style="text-align: center; display: inline-block; width: 470px;">
			<div style="padding-bottom: 5px;">
			<?php echo $banners468ppc[0]; ?>
			</div>
			<div style="padding-bottom: 5px;">
			<?php echo $banners468ppc[1]; ?>
			</div>
			<?php echo $banners468ppc[2]; ?>
		</div>
	</div>
	
	<div style="vertical-align: middle;text-align: center; padding-bottom: 5px;">
		
		<?php $i=0; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
		<?php $i=1; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
		<?php $i=2; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
	</div>
	
	<div style="vertical-align: middle;text-align: center; padding-bottom: 5px;">
		<div style="text-align: center; display: inline-block; width: 130px;">
			<div style="padding-bottom: 5px;">
			<?php echo $banners125ppc[2]; ?>
			</div>
			<?php echo $banners125ppc[3]; ?>
		</div>
		<div style="text-align: center; display: inline-block; width: 470px;">
			<div style="padding-bottom: 5px;">
			<?php echo $banners468ppc[3]; ?>
			</div>
			<div style="padding-bottom: 5px;">
			<?php echo $banners468ppc[4]; ?>
			</div>
			<?php echo $banners468ppc[5]; ?>
		</div>
	</div>
	
	<div style="vertical-align: middle;text-align: center; padding-bottom: 5px;">
		
		<?php $i=3; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
		<?php $i=4; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
		<?php $i=5; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
	</div>
	
	<div style="vertical-align: middle;text-align: center; padding-bottom: 5px;">
		<div style="text-align: center; display: inline-block; width: 130px;">
			<div style="padding-bottom: 5px;">
			<?php echo $banners125ppc[4]; ?>
			</div>
			<?php echo $banners125ppc[5]; ?>
		</div>
		<div style="text-align: center; display: inline-block; width: 470px;">
			<div style="padding-bottom: 5px;">
			<?php echo $banners468ppc[6]; ?>
			</div>
			<div style="padding-bottom: 5px;">
			<?php echo $banners468ppc[7]; ?>
			</div>
			<?php echo $banners468ppc[8]; ?>
		</div>
	</div>
	
	<div style="vertical-align: middle;text-align: center; padding-bottom: 5px;">
		
		<?php $i=6; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
		<?php $i=7; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
		<?php $i=8; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
	</div>
	
	<div style="vertical-align: middle;text-align: center; padding-bottom: 5px;">
		<div style="text-align: center; display: inline-block; width: 130px;">
			<div style="padding-bottom: 5px;">
			<?php echo $banners125ppc[6]; ?>
			</div>
			<?php echo $banners125ppc[7]; ?>
		</div>
		<div style="text-align: center; display: inline-block; width: 470px;">
			<div style="padding-bottom: 5px;">
			<?php echo $banners468ppc[9]; ?>
			</div>
			<div style="padding-bottom: 5px;">
			<?php echo $banners468ppc[10]; ?>
			</div>
			<?php echo $banners468ppc[11]; ?>
		</div>
	</div>
	
	<div style="vertical-align: middle;text-align: center; padding-bottom: 5px;">
		
		<?php $i=9; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
		<?php $i=10; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
		<?php $i=11; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
	</div>
	
	<div style="vertical-align: middle;text-align: center; padding-bottom: 5px;">
		<div style="text-align: center; display: inline-block; width: 130px;">
			<div style="padding-bottom: 5px;">
			<?php echo $banners125ppc[8]; ?>
			</div>
			<?php echo $banners125ppc[9]; ?>
		</div>
		<div style="text-align: center; display: inline-block; width: 470px;">
			<div style="padding-bottom: 5px;">
			<?php echo $banners468ppc[12]; ?>
			</div>
			<div style="padding-bottom: 5px;">
			<?php echo $banners468ppc[13]; ?>
			</div>
			<?php echo $banners468ppc[14]; ?>
		</div>
	</div>
	
	<div style="vertical-align: middle;text-align: center; padding-bottom: 5px;">
		
		<?php $i=12; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
		<?php $i=13; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
		<?php $i=14; if(!is_array($ppctextad[$i])){ ?><div class="textadbox" style="display: inline-block; vertical-align: middle;"><?php echo $ppctextad[$i]; ?></div><?php }else{ ?>
			<div class="textadbox" style="display: inline-block; margin-right: 20px;">
				<div class="text-add-title" style="background: none repeat scroll 0 0 #1356ab;color: #fff;display: block;padding: 5px;width: 200px;">
					<?php echo $this->Html->link($ppctextad[$i]["title"], array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[0]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:#fff;')); ?>
				</div>
				<div class="textadcont" style="border: 1px solid #d8d8d8;padding: 8px;">
					<?php 
					echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$ppctextad[$i]["desc1"].'</span>';
					echo '<br />';
					echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$ppctextad[$i]["desc2"].'</span>';
					if($ppctextad[$i]["showurl"])
						echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'ppctextad', 'action' => 'counter/'.$ppctextad[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
					?>
				</div>
			</div>
		<?php } ?>
		
	</div>
	
	<div style="vertical-align: middle;text-align: center; padding-bottom: 5px;">
		<div style="text-align: center; display: inline-block; width: 130px;">
			<div style="padding-bottom: 5px;">
			<?php echo $banners125ppc[10]; ?>
			</div>
			<?php echo $banners125ppc[11]; ?>
		</div>
		<div style="text-align: center; display: inline-block; width: 470px;">
			<div style="padding-bottom: 5px;">
			<?php echo $banners468ppc[15]; ?>
			</div>
			<div style="padding-bottom: 5px;">
			<?php echo $banners468ppc[16]; ?>
			</div>
			<?php echo $banners468ppc[17]; ?>
		</div>
		<div style="text-align: center; display: inline-block; width: 130px;">
			<div style="padding-bottom: 5px;">
			<?php echo $banners125ppc[12]; ?>
			</div>
			<?php echo $banners125ppc[13]; ?>
		</div>
	</div>
	
</div>
<div class="height5"></div>
<?php  } else { echo __('This page is disabled by administrator'); } ?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>