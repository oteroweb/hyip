<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Pay Per Click Text Ad</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li >
					<?php echo $this->Js->link("PPC Plans Text Ad", array('controller'=>'ppctextad', "action"=>"plan"), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("PPC Text Ad", array('controller'=>'ppctextad', "action"=>"member"), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Text Ad Widget", array('controller'=>'ppctextad', "action"=>"bannerwidget"), array(
						'update'=>'#ppctextadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">		
<div id="ppctextadpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Pay_Per_Click_-_PPC#PPC_Banners" target="_blank">Help</a></div>
<div id="UpdateMessage" class="UpdateMessage"></div>
<?php echo $this->Form->create('Ppctextad',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'ppctextad','action'=>'addaction')));?>
	<?php if(isset($ppcbannerdata['Ppctextad']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$ppcbannerdata['Ppctextad']["id"], 'label' => false));
		echo $this->Form->input('clicklimit', array('type'=>'hidden', 'value'=>$ppcbannerdata['Ppctextad']["total_click"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
	
	<div class="frommain">
	
		<div class="fromnewtext">Status : </div>
		<div class="fromborderdropedown">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('status', array(
						'type' => 'select',
						'options' => array('1'=>'Active', '0'=>'Inactive'),
						'selected' => $ppcbannerdata['Ppctextad']["status"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		<span data-original-title="If Set to 'Active', The Plan Will Be Displayed on Member Side When Purchasing PPC Plans." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="fromnewtext">Pause : </div>
		<div class="fromborderdropedown">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('pause', array(
						'type' => 'select',
						'options' => array('1'=>'Unpause', '0'=>'Pause'),
						'selected' => $ppcbannerdata['Ppctextad']["pause"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		<span data-original-title="You Can Pause The Banner Temporarily At Your Will." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="fromnewtext">Title :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('title', array('type'=>'text', 'value'=>$ppcbannerdata['Ppctextad']["title"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Description Line 1 :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('description1', array('type'=>'text', 'value'=>$ppcbannerdata['Ppctextad']["description1"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Description Line 2 :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('description2', array('type'=>'text', 'value'=>$ppcbannerdata['Ppctextad']["description2"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Site URL :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('site_url', array('type'=>'text', 'value'=>$ppcbannerdata['Ppctextad']["site_url"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Show URL With Text Ad : </div>
		<div class="fromborderdropedown3 checkboxlist">
			<?php if($ppcbannerdata['Ppctextad']["showurl"]){$checked="checked";}else{$checked="";}
			echo $this->Form->input('showurl', array('type' => 'checkbox', 'div'=>true, 'label' =>"", 'checked'=>$checked));?>
		</div>
		
		<div class="fromnewtext">Daily Click Limit :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('daily_budget', array('type'=>'text', 'id'=>'startdate', 'value'=>$ppcbannerdata['Ppctextad']["daily_budget"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Display Area : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
					if($ppcbannerdata['Ppctextad']["display_area"]=='all'){$display_area='all';}else{$display_area='country';}
					echo $this->Form->input('display_area', array(
						'type' => 'select',
						'options' => array('all'=>'All Countries', 'country'=>'Selected Countries'),
						'selected' => $display_area,
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => '',
						'onchange' => 'if(this.selectedIndex!=0){$(".countryfields").show(500);}else{$(".countryfields").hide(500);}'
					));
				?>
			  </label>
		  </div>
		</div>
		
		
		<div class="countryfields" style="display:<?php if($ppcbannerdata['Ppctextad']["display_area"]!='all'){echo '';}else{echo 'none';}?>;">
			<div class="fromnewtext">Country : </div>
			<div class="fromborderdropedown3 checkboxlist" style="height:250px;overflow:auto;">
				<?php
				$ccounter=0;
				$replacecountry=array('India','Dominica','Guinea','Mali','Netherlands','Niger','Oman','Samoa','Curacao');
				$country=array('India_2','Dominica_2','Guinea_2','Mali_2','Netherlands_2','Niger_2','Oman_2','Samoa_2','Netherlands Antilles');
				$display_area=str_replace($country,$replacecountry,$ppcbannerdata['Ppctextad']["display_area"]);
				$countrynames=@explode(",", $display_area);
				
				foreach($countries as $cid=>$cname)
				{
					if(in_array(trim($cname), $countrynames)){$selected = 'checked="checked"';}else{ $selected = '';}
					echo '<div class="checkbox"><input id="chk'.$ccounter.'" type="checkbox" name="country[]" value="'.trim($cname).'" '.$selected.' /> <label for="chk'.$ccounter.'">'.trim($cname).'</label></div>';
					$ccounter++;
				}
				?>
			</div>
			
		</div>
		
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'btnorange',
				'div'=>false,
				'controller'=>'ppctextad',
				'action'=>'addaction',
				'url'   => array('controller' => 'ppctextad', 'action' => 'addaction')
			  ));?>
			  
			  <?php echo $this->Js->link("Back", array('controller'=>'ppctextad', "action"=>"member/".$planid), array(
				  'update'=>'#ppctextadpage',
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'escape'=>false,
				  'class'=>'btngray'
			  ));?>
		</div>
	</div>
<?php echo $this->Form->end();?>
<?php if(!$ajax){?>
</div><!--#balancetransferpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>