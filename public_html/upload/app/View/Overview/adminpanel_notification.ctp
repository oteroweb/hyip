<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<div class="contentpadding">
	<div class="planstats">
			<div class="divtable">
			  <?php $i=1;
			foreach($Notifications as $notification):
			if($i%2==0){$class='admin-box-white';}else{$class='admin-box-grey';}
			?>
				<?php
				if($notification['id']==1)
				{
					$controller="textad"; $action="index/top"; $description='Pending Text Ads';
				}
				elseif($notification['id']==2)
				{
					$controller="bannerad"; $action="index/top"; $description='Pending Banner Ads';
				}
				elseif($notification['id']==3)
				{
					$controller="soload"; $action="index/top"; $description='Pending Solo Ads';
				}
				elseif($notification['id']==4)
				{
					$controller="ppc"; $action="member"; $description='Pending PPC Ads';
				}
				elseif($notification['id']==5)
				{
					$controller="ptc"; $action="member"; $description='Pending PTC Ads';
				}
				elseif($notification['id']==6)
				{
					$controller="loginad"; $action="loginad"; $description='Pending Login Ads';
				}
				elseif($notification['id']==7)
				{
					$controller="directory"; $action="planmember"; $description='Pending Biz Directory Ads';
				}
				elseif($notification['id']==8)
				{
					$controller="traffic"; $action="website/top"; $description='Pending Website';
				}
				elseif($notification['id']==9)
				{
					$controller="finance"; $action="withdrawrequest/0/top"; $description='Pending Withdrawal Requests';
				}
				elseif($notification['id']==10)
				{
					$controller="finance"; $action="pendingdeposit/0/top"; $description='Pending Add Fund Requests';
				}
				elseif($notification['id']==11)
				{
					$controller="Support"; $action="index/public/0/top"; $description='Member Tickets Awaiting Reply';
				}
				elseif($notification['id']==12)
				{
					$controller="Support"; $action="index/public/0/top"; $description='Public Tickets Awaiting Reply';
				}
				elseif($notification['id']==13)
				{
					$controller="contentmanagement"; $action="testimonials/top"; $description='Pending Testimonials';
				}
				?>
				<div class="divtr">
					<div class="divtd">
						<?php echo $this->Js->link($description, array('controller'=>$controller, "action"=>$action, 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>$class,
							'style'=>'text-decoration:none'
						));?>
					</div>
					<div class="divtd">
						<?php echo $this->Js->link($notification['counter'], array('controller'=>$controller, "action"=>$action, 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>$class,
							'style'=>'text-decoration:none'
						)); ?>
					</div>
				</div>
				
			<?php $i++;endforeach; ?>
				
			</div>
		    </div>
</div>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>