<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<div class="contentpadding">
	<div class="adsstatleft mobilewidth100">
		    
		    <div class="adsstatbox">
		      <div class="adsstattext">
			Total Text Ads <br /> Shown <span><?php echo $TextAdsShown; ?></span>
		      </div>
		    </div>
		    
		    <div class="adsstatbox redborder">
		      <div class="adsstattext">
			Total Text Ads Yet to <br />Be Displayed <span><?php echo $TotalTextAdsRemain;?></span>
		      </div>
		    </div>
		    
		    <div class="adsstatbox redborder">
		      <div class="adsstattext">
			Total Banner Ads <br /> Shown <span><?php echo $BannerAdsShown;?></span>
		      </div>
		    </div>
		    
		    <div class="adsstatbox">
		      <div class="adsstattext">
			Total Banner Ads Yet <br/> to Be Displayed <span><?php echo $TotalBannerAdsRemain;?></span>
		      </div>
		    </div>
		    
		    <div class="adsstatbox">
		      <div class="adsstattext">
			Total Solo Ads<br />Sent <span><?php echo $TotalSoloadsend;?></span>
		      </div>
		    </div>
		    
		    <div class="adsstatbox redborder">
		      <div class="adsstattext">
			Total Solo Ads<br />Pending <span><?php echo $TotalSoloadPending;?></span>
		      </div>
		    </div>
		    
		    <div class="adsstatbox redborder">
		      <div class="adsstattext">
			Total Solo Ads<br />Approved <span><?php echo $TotalSoloadApproved;?></span>
		      </div>
		    </div>
		    
		    <div class="adsstatbox">
		      <div class="adsstattext">
			Total Solo Ads Being<br />Sent (Partially Sent) <span><?php echo $TotalSoloadSending;?></span>
		      </div>
		    </div>
		    
		  </div>
		  <div class="adsstatright mobilewidth100">
		  
		  <?php echo $this->Javascript->link('charts');?>
				<script type="text/javascript">
				var chartData = [{
					adtype: "Banners",
					shown: <?php echo $BannerAdsShown;?>,
					pending: <?php echo $TotalBannerAdsRemain;?>
				}, {
					adtype: "Text Ads",
					shown: <?php echo $TextAdsShown;?>,
					pending: <?php echo $TotalTextAdsRemain;?>
				}, {
					adtype: "Solo Ads",
					shown: <?php echo $TotalSoloadPending+$TotalSoloadSending+$TotalSoloadApproved;?>,
					pending: <?php echo $TotalSoloadsend;?>
				}];
	
	
				$(function(){
					// SERIAL CHART
					chart = new AmCharts.AmSerialChart();
					chart.type = "line";
					chart.dataProvider = chartData;
					chart.categoryField = "adtype";
					chart.startDuration = 1;
					chart.plotAreaBorderColor = "#DEDEDE";
					chart.plotAreaBorderAlpha = 1;
					// this single line makes the chart a bar chart          
					chart.rotate = true;
					chart.depth3D = 10;
					chart.angle = 30;
					
					// AXES
	                // Category
					var categoryAxis = chart.categoryAxis;
					categoryAxis.axisAlpha = 1;
					categoryAxis.gridAlpha = 0.1;
					categoryAxis.position = "left";
					categoryAxis.axisColor = "#DEDEDE";
					categoryAxis.fillAlpha = 1;
					categoryAxis.fillColor = "#FAFAFA";
					// Value
					var valueAxis = new AmCharts.ValueAxis();
					valueAxis.axisAlpha = 1;
					valueAxis.gridAlpha = 0.1;
					valueAxis.position = "bottom";
					valueAxis.axisColor = "#DEDEDE";
					valueAxis.fillAlpha = 0;
					valueAxis.fillColor = "#FAFAFA";
					valueAxis.labelRotation = 30;
					chart.addValueAxis(valueAxis);
	
					// GRAPHS
					// first graph
					var graph1 = new AmCharts.AmGraph();
					graph1.type = "column";
					graph1.title = "Total Ads Shown";
					graph1.valueField = "shown";
					graph1.balloonText = "Shown:[[value]]";
					graph1.lineAlpha = 0;
					graph1.fillColors = "#1993ab";
					graph1.fillAlphas = 1;
					graph1.labelText = "[[value]] Shown";
					graph1.labelPosition = "inside";
					graph1.labelColorField = "#FF0000";
					graph1.gradientOrientation = "vertical";
					graph1.backgroundAlpha = 1;
					graph1.backgroundColor = "#dedede";
					chart.addGraph(graph1);
	
					// second graph
					var graph2 = new AmCharts.AmGraph();
					graph2.type = "column";
					graph2.title = "Total Ads Yet to Be Displayed";
					graph2.valueField = "pending";
					graph2.balloonText = "Remain:[[value]]";
					graph2.lineAlpha = 0;
					graph2.fillColors = "#f4c601";
					graph2.fillAlphas = 1;
					graph2.labelText = "[[value]] Remain";
					graph2.labelPosition = 'inside';
					graph2.gradientOrientation = "vertical";
					chart.addGraph(graph2);
					
					// LEGEND
					legend = new AmCharts.AmLegend();
					legend.align = "center";
					legend.markerType = "bubble";
					legend.fontSize = 10;
					legend.valueText = '[[value]]';
					legend.valueWidth = 5;
					legend.verticalGap = 0;
					chart.addLegend(legend);
	
					// WRITE
					chart.write("adstatistics_chart1");
				});
				</script>
	
				<center>
				<div id="adstatistics_chart1" style="width:100%;height: 352px;"></div>
				</center>
		  
		  </div>
</div>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>