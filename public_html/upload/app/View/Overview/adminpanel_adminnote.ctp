<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>

<div class="contentpadding">
	<div class="height10"></div>
	<div id="UpdateMessage"></div>
	
	<?php echo $this->Form->create('Admin_note',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'overview','action'=>'adminnoteaction')));?>
	<div class="adminnote"><?php echo $this->Form->input('note', array('type'=>'textarea', 'value'=>stripslashes($admin_notes),'label' => false));?></div>
	<div class="textright">
		<?php echo $this->Js->submit('Update', array(
			'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'update'=>'#UpdateMessage',
			'class'=>'btnorange',
			'div'=>false,
			'style'=>'margin-bottom:5px',
			'controller'=>'overview',
			'action'=>'adminnoteaction',
			'url'   => array('controller' => 'overview', 'action' => 'adminnoteaction')
		      ));?>
	</div>
	<?php echo $this->Form->end();?>
</div>


<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>