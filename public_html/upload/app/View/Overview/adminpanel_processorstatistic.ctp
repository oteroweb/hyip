<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 09-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<div class="contentpadding">
	<div class="planstats">
			<div class="divtable tablegrid">
				<div class="divtr tablegridheader">
					<div class="divth">Processor</div>
					<div class="divth">Income</div>
					<div class="divth">Income Fees</div>
					<div class="divth">Expense</div>
					<div class="divth">Balance</div>
					<div class="divth">API Balance</div>
				</div>
				<?php 
				$i=0;
				foreach($processerdata as $processer) {
				if($i%2==0){$class='admin-box-grey';}else{$class='admin-box-white';}
				      isset($processer['Income'])?$Income=$processer['Income']:$Income=0;
				      isset($processer['Outcome'])?$Outcome=$processer['Outcome']:$Outcome=0;
					?>
					<div class="divtr tablegridrow">
					      <div class="divtd"><?php echo $processer['name']; ?></div>
					      <div class="divtd">$<?php echo round($Income, 2); ?></div>
						  <div class="divtd">$<?php echo round($processer['IncomeFees'],2); ?></div>
					      <div class="divtd">$<?php echo round($Outcome, 2); ?></div>
					      <div class="divtd">$<?php echo round((($Income-$Outcome)+$processer['IncomeFees']), 2); ?></div>
						  <div class="divtd">
							<span id="processor<?php echo $i;?>">
							<?php
							if(!in_array($processer['name'],array('Bank Wire','SolidTrustPay')))
							{
								echo $this->Js->link('Get', array('controller'=>'overview', "action"=>"getbalance/".$processer['name']), array(
									'update'=>'#processor'.$i,
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));
							}
							else
							{
								echo "N/A";
							}	?>
							</span>
						  </div>
					</div>
			       <?php $i++; } ?>
			  
			</div>
	</div>
</div>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>