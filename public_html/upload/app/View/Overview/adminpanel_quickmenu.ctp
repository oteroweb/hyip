<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<!-- Quick Menu Box Start -->
<script type="text/javascript">
$(function() {
	$("#droppable ul").css({'min-height':$("#quickmenubox").height()-50});
	$("#quickmenubox, #cfubox").draggable();
	$( "#menu ul ul li a" ).draggable({
		appendTo: "body",
		helper: "clone"
	});
	$( "#droppable ul" ).droppable({
			activeClass: "ui-state-default",
			hoverClass: "ui-state-hover",
			accept: ":not(.ui-sortable-helper)",
			drop: function( event, ui ) {
				$( this ).find( ".placeholder" ).remove();
				$( "<li></li>" ).text( " "+ui.draggable.html() ).appendTo( this ).prepend('<span class="floatleft '+ui.draggable.html().replace(/ /g, "-").toLowerCase()+'"></span><input type="hidden" value="'+ui.draggable.attr('href')+'" name="quickmenu['+ui.draggable.html()+']" />');
			}
		}).sortable({
			sort: function() {
			$( this ).removeClass( "ui-state-default" );
		}
	});
	
	$("#droppable ul li").draggable({ revert: "invalid" });
    $( "#erasedroppable ul" ).droppable({
	    drop: function( event, ui ) {
			//$( this ).addClass( "ui-state-highlight" ).find( "li" ).html( "Removed" );
			ui.draggable.fadeOut(function() { 
				$(this).remove(); 
			});
		}
	});
});
</script>
<div id="quickmenubox">
	<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'quickmenuupdate')));?>
	
	<div class="quick-closed"><a href="javascript:void(0)" onclick="closeQuickMenu()"><?php echo $this->Html->image('menu/quick-closed.jpg', array('alt'=>'X'))?></a></div>
	<div id="erasedroppable"><ul class="trushmenu"><li>Drag To Remove</li></ul></div>
	<div class="remove-all"><a href="javascript:void(0)" onclick="RemoveAllQuickMenu()">Remove All</a></div>
	<div class="helplink"><a href="https://www.proxscripts.com/docs/Quick_Menu" title="Help" target="_blank"><?php echo $this->Html->image('menu/help.png', array('alt'=>'?'))?></a></div>
	<div class="clear-both"></div>
	
	<div id="droppable" class="menu-box">
		<div class="menulistoverflow">
			<ul class="quick-menu"></ul>
		</div>
		<div align="center">
			<div id="UpdateMessage2" style="color:red;font-size:9px;text-align:center;height:12px;"></div>
			<?php echo $this->Js->submit('Submit Changes', array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage2',
			  'class'=>'quickmenubutton',
			  'div'=>false,
			  'controller'=>'sitesetting',
			  'action'=>'quickmenuupdate',
			  'url'   => array('controller' => 'sitesetting', 'action' => 'quickmenuupdate')
			));?>
		</div>
	</div>
	<?php echo $this->Form->end();?>
</div>
<?php $QuickMenuCuurent=''; if($SITECONFIG['quickmenu']!=''){
	$quickarray=@explode('[#]', $SITECONFIG['quickmenu']);
	foreach($quickarray as $quckm)
	{
		$quickmarray=@explode('[@]', $quckm);
		$QuickMenuCuurent.='<li class="ui-sortable"><input type="hidden" name="quickmenu['.$quickmarray[0].']" value="'.$quickmarray[1].'"><span class="floatleft '.strtolower(str_replace(" ", "-", $quickmarray[0])).'"></span> '.$quickmarray[0].'</li>';
	}
}?>
<script type="text/javascript">
$(function() {
	$("#droppable ul").html('<?php echo $QuickMenuCuurent;?>');
	$("#droppable.menu-box .menulistoverflow").height($("#quickmenubox").height()-103);
});
</script>
<script type="text/javascript">
$(document).ready(function() {
	$("#droppable .menulistoverflow").mCustomScrollbar({
		scrollInertia:1000,
		theme:"dark-thin"
	});
	//$("#quickmenubox").hide();
});
</script>
<!-- Quick Menu Box Over -->
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>