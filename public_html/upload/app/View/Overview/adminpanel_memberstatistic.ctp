<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<div class="contentpadding">
	 <div class="tableleft mobilewidth100">
		    
		    <div class="statboxmain">
		      <div class="statbox">
			<div class="stattext">Total Registered<br />
			  Members
			  <span><?php echo $ActiveMember+$InActiveMember; ?></span>
			</div>
		      </div>
		      
		      <div class="statbox">
			<div class="stattext">Total Active<br />
			  Members
			  <span><?php echo $ActiveMember;?></span>
			</div>
		      </div>
		      
		      <div class="statbox">
			<div class="stattext">Total Inactive<br />
			  Members
			  <span><?php echo $InActiveMember;?></span>
			</div>
		      </div>
		    </div>
		    
		    <div class="planstatsmain">
			<?php if(count($moduledata)>0)
			{
				foreach($moduledata as $module)
				{ ?>
					<div class="planstats">
					<div class="planstathead"><?php echo $module['planname'] ?></div>
					<div class="divtable">
					  <div class="divtr">
					    <div class="divtd">Total Members With Active <?php echo $module['planname'] ?> Positions</div>
					    <div class="divtd"><?php echo $module['active'] ?></div>
					  </div>
					  <div class="divtr">
					    <div class="divtd">Total Members With Inactive <?php echo $module['planname'] ?> Positions</div>
					    <div class="divtd"><?php echo $module['inactive'] ?></div>
					  </div>
					</div>
				      </div>
				<?php
				}
			} ?>
		    </div>
		    
		  </div>
		  
		  <div class="tableright mobilewidth100">
			
			<?php echo $this->Javascript->link('charts');?>
			<script type="text/javascript">
			function memberstatistics1()
			{
				var chartData = [{
					membertype: "Total Active Members",
					memberstype: "Active",
					members: <?php echo $ActiveMember;?>
				}, {
					membertype: "Total Inactive Members",
					memberstype: "Inactive",
					members: <?php echo $InActiveMember;?>
				}];
	
				$(function(){
					chart = new AmCharts.AmPieChart();
					//chart.addTitle("Member Statistics", 10);
					chart.dataProvider = chartData;
					chart.titleField = "membertype";
					chart.valueField = "members";
					chart.colors = ["#19b698","#eaeff2"];
					chart.gradientRatio =  [-0.4,0.2];
					chart.fontSize = 9;
					chart.radius = 135;
					chart.sequencedAnimation = true;
					chart.startDuration = 2;
					chart.startEffect = 'elastic';
					chart.startRadius = '300%';
					
					chart.labelRadius = -50;
                    chart.labelText = "[[value]] [[memberstype]]";
					chart.depth3D = 10;
                    chart.angle = 10;
					
					legend = new AmCharts.AmLegend();
					legend.align = "center";
					legend.markerType = "bubble";
					legend.fontSize=12;
					legend.valueText = '[[value]]';
					legend.rollOverColor = '#FF0000';
					chart.addLegend(legend);
	
					chart.write("memberstatistics_chart1");
				});
			}
			<?php if(count($moduledata)>0){
			$functionid=1;
			foreach($moduledata as $module){ $functionid++; ?>
			function memberstatistics<?php echo $functionid; ?>()
			{
				var chartData = [{
					membertype: "Members With Active Positions",
					memberstype: "Active",
					members: <?php echo $module['active'];?>
				}, {
					membertype: "Members With Inactive Positions",
					memberstype: "Inactive",
					members: <?php echo $module['inactive'];?>
				}];
	
				$(function(){
					chart = new AmCharts.AmPieChart();
					//chart.addTitle("Member Statistics", 10);
					chart.dataProvider = chartData;
					chart.titleField = "membertype";
					chart.valueField = "members";
					chart.colors = ["#19b698","#eaeff2"];
					chart.gradientRatio =  [-0.4,0.2];
					chart.fontSize = 9;
					chart.radius = 135;
					chart.sequencedAnimation = true;
					chart.startDuration = 2;
					chart.startEffect = 'elastic';
					chart.startRadius = '300%';
					
					chart.labelRadius = -50;
                    chart.labelText = "[[value]] [[memberstype]]";
					chart.depth3D = 10;
                    chart.angle = 10;
					
					legend = new AmCharts.AmLegend();
					legend.align = "center";
					legend.markerType = "bubble";
					legend.fontSize=12;
					legend.verticalGap = 0;
					chart.addLegend(legend);
	
					chart.write("memberstatistics_chart1");
				});
			}
			<?php } } ?>
			
			memberstatistics1();
			</script>

			<div class="textcenter">
			<input type="button" OnClick="memberstatistics1();" value="Members" class="btnorange" />
			<?php if(count($moduledata)>0){
			$functionid=1;
			foreach($moduledata as $module){ $functionid++; $buttonname=explode(' ',$module['planname']); ?>
				<input type="button" OnClick="memberstatistics<?php echo $functionid; ?>();" value="<?php echo $buttonname[0]; ?>" class="btnorange" />
			<?php } } ?>
			</div>
			<div id="memberstatistics_chart1" style="width:100%;height: 352px;"></div>
			
		  </div>
</div>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>