<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 18-11-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<div class="contentpadding">
		<div class="financialstat">
		    <div class="financialstattitle">Positions/Shares</div>
		    
		    <?php if(count($moduledata)>0)
		    {
				foreach($moduledata as $module)
				{ ?>
					<div class="financialstatbox mobilewidth100">
						<div class="planstats">
						  <div class="planstathead"><?php echo $module['planname']; ?></div>
						  <div class="divtable">
							<div class="divtr">
							  <div class="divtd">Total Sold <?php echo trim($module['planname'],' Plan'); ?> Positions</div>
							  <div class="divtd"><?php echo $module['TotalPosition']; ?></div>
							</div>
							<div class="divtr">
							  <div class="divtd">Total Active <?php echo trim($module['planname'],' Plan'); ?> Positions</div>
							  <div class="divtd"><?php echo $module['ActivePosition']; ?></div>
							</div>
							<div class="divtr">
							  <div class="divtd">Total <?php echo trim($module['planname'],' Plan'); ?> Positions Amount</div>
							  <div class="divtd">$<?php echo round($module['TotalPositionAmount'], 2); ?></div>
							</div>
							<div class="divtr">
							  <div class="divtd">Total Active <?php echo trim($module['planname'],' Plan'); ?> Positions Amount</div>
							  <div class="divtd">$<?php echo round($module['ActivePositionAmount'], 2); ?></div>
							</div>
							<div class="divtr">
							  <div class="divtd">Total <?php echo trim($module['planname'],' Plan'); ?> Position Earnings</div>
							  <div class="divtd">$<?php echo round($module['TotalPositionEarning'], 2); ?></div>
							</div>
						  </div>
						</div>
					</div>
				  <?php
				}
		    } ?>
		    
		  </div>
		  <div class="financialstat textcenter">
		    <div class="adsstatbox">
		      <div class="adsstattext">
			Total Payout<br />(Actual) <span>$<?php echo round($TotalPayout, 2);?></span>
		      </div>
		    </div>
		    
		    <div class="adsstatbox redborder">
		      <div class="adsstattext">
			Total Pending<br />Withdrawal Amount <span>$<?php echo round($PendingWithdraw, 2);?></span>
		      </div>
		    </div>
		    
			<div class="adsstatbox yellowborder">
		      <div class="adsstattext">
			Total<br />Cash Balance <span>$<?php echo round($TotalCashbalance, 2);?></span>
		      </div>
		    </div>
		    
			<div class="adsstatbox yellowborder">
		      <div class="adsstattext">
			Total<br />Re-purchase Balance <span>$<?php echo round($TotalRepurchasebalance, 2);?></span>
		      </div>
		    </div>
			
			<div class="adsstatbox yellowborder">
		      <div class="adsstattext">
			Total<br />Earning Balance <span>$<?php echo round($TotalEarningbalance, 2);?></span>
		      </div>
		    </div>
			
			<div class="adsstatbox yellowborder">
		      <div class="adsstattext">
			Total<br />Commission Balance <span>$<?php echo round($TotalCommissionbalance, 2);?></span>
		      </div>
		    </div>
			
		    <div class="adsstatbox darkgrayborder">
		      <div class="adsstattext">
			Total<br />Commission Paid<span>$<?php echo round($TotalCommission, 2);?></span>
		      </div>
		    </div>
		    <?php if(strpos($SITECONFIG['modules'], ":module1:1")!==false){?>
		    <div class="adsstatbox blueborder">
		      <div class="adsstattext">
			Total Quickener Amount<br />(Regular Earning Plans)<span>$<?php echo round($TotalBonus, 2);?></span>
		      </div>
		    </div>
                    <?php } if(strpos($SITECONFIG['modules'], ":module2:1")!==false){?>
		    <div class="adsstatbox lightorangeborder">
		      <div class="adsstattext">
			Total Matrix<br />Completion Bonus Paid <span>$<?php echo round($TotalMatrixBonus, 2);?></span>
		      </div>
		    </div>
                    <?php } ?>
		    <div class="adsstatbox lightgreenborder">
		      <div class="adsstattext">
			Total<br />Fees Charged <span>$<?php echo round($TotalFees, 2);?></span>
		      </div>
		    </div>
			
			<div class="adsstatbox lightgreenborder">
		      <div class="adsstattext">
			Total<br />Withdraw Fees Charged <span>$<?php echo round($WithdrawFees, 2);?></span>
		      </div>
		    </div>
		    
		    <div class="adsstatbox grayborder">
		      <div class="adsstattext">
			Total<br />Add Fund Amount<span>$<?php echo round($TotalDeposit, 2);?></span>
		      </div>
		    </div>
			
			<div class="adsstatbox grayborder">
		      <div class="adsstattext">
			Total<br />Plan Purchase Amount<span>$<?php echo round($TotalPurchase, 2);?></span>
		      </div>
		    </div>
		    
		    <div class="adsstatbox differ_box">
		      <div class="adsstattext">
			<span class="textleft">Cash By Admin</span>
			<div class="inlineblock textleft">Total Added <span>$<?php echo round($CashByAdminAdded, 2);?></span></div>
			<div class="inlineblock textright">Total Deducted <span>$<?php echo round($CashByAdminDeducted, 2);?></span></div>
		      </div>
		    </div>
		    
		  </div>
		  
		
		  <div class="textcenter">
			<?php echo $this->Javascript->link('charts');?>
					<script type="text/javascript">
					var chart;
		
					var chartData = [{
						country: "Total Payout",
						visits: <?php echo round($TotalPayout, 2);?>,
						color: "#FF0F00"
					}, {
						country: "Total Pending Withdrawal Amount",
						visits: <?php echo round($PendingWithdraw, 2);?>,
						color: "#FF6600"
					}, {
						country: "Total Cash Balance",
						visits: <?php echo round($TotalCashbalance, 2);?>,
						color: "#FF9E01"
					}, {
						country: "Total Re-purchase Balance",
						visits: <?php echo round($TotalRepurchasebalance, 2);?>,
						color: "#de8900"
					}, {
						country: "Total Earning Balance",
						visits: <?php echo round($TotalEarningbalance, 2);?>,
						color: "#c77b01"
					}, {
						country: "Total Commission Balance",
						visits: <?php echo round($TotalCommissionbalance, 2);?>,
						color: "#9f6507"	
					}, {
						country: "Total Commission Paid",
						visits: <?php echo round($TotalCommission, 2);?>,
						color: "#FCD202"
					}, {
                                        <?php if(strpos($SITECONFIG['modules'], ":module1:1")!==false){?>
						country: "Total Quickener Amount",
						visits: <?php echo round($TotalBonus, 2);?>,
						color: "#F8FF01"
					}, {
                                        <?php } ?>
                                        <?php if(strpos($SITECONFIG['modules'], ":module2:1")!==false){?>
						country: "Total Matrix Bonus Paid",
						visits: <?php echo round($TotalMatrixBonus, 2);?>,
						color: "#B0DE09"
					}, {
                                         <?php }   ?>
						country: "Total  Fees Charged",
						visits: <?php echo round($TotalFees, 2);?>,
						color: "#04D215"
					},{
						country: "Total Withdraw Fees Charged",
						visits: <?php echo round($WithdrawFees, 2);?>,
						color: "#04D215"
					},{
						country: "Total Add Fund Amount",
						visits: <?php echo round($TotalDeposit, 2);?>,
						color: "#0D8ECF"
					},{
						country: "Total Plan Purchase Amount",
						visits: <?php echo round($TotalPurchase, 2);?>,
						color: "#0D8ECF"	
					}, {
						country: "Cash By Admin (Added)",
						visits: <?php echo round($CashByAdminAdded, 2);?>,
						color: "#8A0CCF"
					}, {
						country: "Cash By Admin (Deducted)",
						visits: <?php echo round($CashByAdminDeducted, 2);?>,
						color: "#CD0D74"
					}];
		
		
					$(function(){
						// SERIAL CHART
						chart = new AmCharts.AmSerialChart();
						chart.dataProvider = chartData;
						chart.categoryField = "country";
						chart.depth3D = 20;
						chart.angle = 30;
						chart.startDuration = 1;
		
						// AXES
						// category
						var categoryAxis = chart.categoryAxis;
						categoryAxis.labelRotation = 30;
						categoryAxis.gridAlpha = 0.1;
						categoryAxis.title = "Financial Overview";
						categoryAxis.gridPosition = "start";
						// value
						var valueAxis = new AmCharts.ValueAxis();
						valueAxis.title = "Amount";
						valueAxis.axisAlpha = 1;
						valueAxis.gridAlpha = 0.1;
						valueAxis.fillAlpha = 0.5;
						valueAxis.fillColor = "#FAFAFA";
						chart.addValueAxis(valueAxis);
		
						// GRAPH            
						var graph = new AmCharts.AmGraph();
						graph.valueField = "visits";
						graph.colorField = "color";
						graph.balloonText = "[[category]]: $[[value]]";
						graph.type = "column";
						graph.lineAlpha = 0;
						graph.fillAlphas = 1;
						graph.labelText = "$[[value]]";
						graph.labelColorField = "color";
						graph.labelRotation = 90;
						chart.addGraph(graph);
		
						// WRITE
						chart.write("financialstatistic_chart1");
					});
				</script>
			<div id="financialstatistic_chart1" style="width:100%;height: 500px;"></div>
		  </div>
</div>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>