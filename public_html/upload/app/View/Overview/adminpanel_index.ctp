<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<!--Check for Updates Code start-->
<?php
$isdisplaypopup=0;
if($SITECONFIG['cfu_popup']=='0000-00-00 00:00:00')
{
	if(CURRENTVERSION!=LATESTVERSION)
		$isdisplaypopup=1;
}
else
{
	if($SITECONFIG['cfu_popup']<date("Y-m-d H:i:s"))
		$isdisplaypopup=1;
}
if($isdisplaypopup==1){?>
<div id="cboxOverlay" style="display: block; opacity: 0.9; cursor: pointer;"></div>
<div id="cfubox" style="position: fixed;width: 60%;height: 556px;background-color: #ffffff;box-shadow: 0 0 10px #C3C3C3;z-index: 10000;left:20%;top:8%;">
	<div id="cboxClose" onclick='$("#cboxOverlay, #cfubox").hide();'>X</div>
	<div id='cfupopup'>
			<div class="admin-o-menu">
				<ul class="cfu-tab-menu">
					<li><a class="active" href="javascript:void(0)" onclick='$(".cfu-tab").fadeOut(0);$(".cfu-tab1").fadeIn(500);'><?php echo __('Check for Updates'); ?></a></li>
					<li><a href="javascript:void(0)" onclick='$(".cfu-tab").fadeOut(0);$(".cfu-tab2").fadeIn(500);'><?php echo __('Latest News');?></a></li>
				</ul>
			</div>
			<div class="clear-both"></div>
			<div class="cfi-border" style="border-top: 1px solid #dddddd;"></div>
			<div class="cfu-content" style="height: 500px;overflow: auto;">
				
			<div class="cfu-tab cfu-tab1 textcenter">
				<div id="updatecontent"><?php echo $this->Html->image('wait.gif', array('alt' => 'Please Wait!!'));?></div>
				<div class="height20"></div>
				<span class="btngray">Your Version : <?php echo CURRENTVERSION;?></span>
				<span class="btnorange">Latest Version : <?php echo LATESTVERSION;?></span>
				<div class="height20"></div><div class="height20"></div>
				<div>
					<div class="rediobtn">
						<div class="retbtn">
						  <div class="rediobox">
							<input id="Option1" type="radio" name="cfu_popup" value="<?php echo date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s"). ' + 1 days'));?>" />
							<label for="Option1"> </label>
						   </div>
						</div>
						<div class="cont">Do not display this pop-up until tomorrow</div>
					</div>
					<div class="rediobtn">
						<div class="retbtn">
						  <div class="rediobox">
							<input id="Option2" type="radio" name="cfu_popup" value="<?php echo date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s"). ' + 7 days'));?>" />
							<label for="Option2"> </label>
						   </div>
						</div>
						<div class="cont">Until next week</div>
					</div>
					<div class="rediobtn">
						<div class="retbtn">
						  <div class="rediobox">
							<input id="Option3" type="radio" name="cfu_popup" value="0000-00-00 00:00:00" />
							<label for="Option3"> </label>
						   </div>
						</div>
						<div class="cont">Until new version is available</div>
					</div>
					<br>
					<input type="button" value="Submit" class="btnorange" onclick='CheckForUpdateSetting($("input:radio[name=cfu_popup]:checked").val(),"<?php echo $ADMINURL;?>");' />
				</div>
			</div>
			
			<div class="cfu-tab cfu-tab2" style="display: none;">
			<div id="feedcontent"><?php echo $this->Html->image('wait.gif', array('alt' => 'Please Wait!!'));?></div>
			<script type="text/javascript">
				$(document).ready( function() {
					CheckForUpdate("#feedcontent", "#updatecontent", "<?php echo $ADMINURL;?>");
				});
			</script>
			</div>
			</div>
	</div>
</div>
<?php }?>
<!--Check for Updates Code Over-->

<!-- Simple AnythingSlider -->
<?php if(!$ajax) { ?>

<!-- Quick Menu Box Start -->
<script type="text/javascript">
$(function() {
	$("#droppable ul").css({'min-height':$("#quickmenubox").height()-50});
	$("#quickmenubox, #cfubox").draggable();
	$( "#menu ul ul li a" ).draggable({
		appendTo: "body",
		helper: "clone"
	});
	$( "#droppable ul" ).droppable({
			activeClass: "ui-state-default",
			hoverClass: "ui-state-hover",
			accept: ":not(.ui-sortable-helper)",
			drop: function( event, ui ) {
				$( this ).find( ".placeholder" ).remove();
				$( "<li></li>" ).text( " "+ui.draggable.html() ).appendTo( this ).prepend('<span class="floatleft '+ui.draggable.html().replace(/ /g, "-").toLowerCase()+'"></span><input type="hidden" value="'+ui.draggable.attr('href')+'" name="quickmenu['+ui.draggable.html()+']" />');
			}
		}).sortable({
			sort: function() {
			$( this ).removeClass( "ui-state-default" );
		}
	});
	
	$("#droppable ul li").draggable({ revert: "invalid" });
    $( "#erasedroppable ul" ).droppable({
	    drop: function( event, ui ) {
			//$( this ).addClass( "ui-state-highlight" ).find( "li" ).html( "Removed" );
			ui.draggable.fadeOut(function() { 
				$(this).remove(); 
			});
		}
	});
});
</script>
<div id="quickmenubox">
	<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'quickmenuupdate')));?>
	
	<div class="quick-closed"><a href="javascript:void(0)" onclick="closeQuickMenu()"><?php echo $this->Html->image('menu/quick-closed.jpg', array('alt'=>'X'))?></a></div>
	<div id="erasedroppable"><ul class="trushmenu"><li>Drag To Remove</li></ul></div>
	<div class="remove-all"><a href="javascript:void(0)" onclick="RemoveAllQuickMenu()">Remove All</a></div>
	<div class="clear-both"></div>
	
	<div id="droppable" class="menu-box">
		<div class="menulistoverflow">
			<ul class="quick-menu"></ul>
		</div>
		<div align="center">
			<div id="UpdateMessage2" style="color:red;font-size:9px;text-align:center;height:12px;"></div>
			<?php echo $this->Js->submit('Submit Changes', array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage2',
			  'class'=>'quickmenubutton',
			  'div'=>false,
			  'controller'=>'sitesetting',
			  'action'=>'quickmenuupdate',
			  'url'   => array('controller' => 'sitesetting', 'action' => 'quickmenuupdate')
			));?>
		</div>
	</div>
	<?php echo $this->Form->end();?>
</div>
<?php $QuickMenuCuurent=''; if($SITECONFIG['quickmenu']!=''){
	$quickarray=@explode('[#]', $SITECONFIG['quickmenu']);
	foreach($quickarray as $quckm)
	{
		$quickmarray=@explode('[@]', $quckm);
		$QuickMenuCuurent.='<li class="ui-sortable"><input type="hidden" name="quickmenu['.$quickmarray[0].']" value="'.$quickmarray[1].'"><span class="floatleft '.strtolower(str_replace(" ", "-", $quickmarray[0])).'"></span> '.$quickmarray[0].'</li>';
	}
}?>
<script type="text/javascript">
$(function() {
	$("#droppable ul").html('<?php echo $QuickMenuCuurent;?>');
	$("#droppable.menu-box .menulistoverflow").height($("#quickmenubox").height()-103);
});
</script>
<script type="text/javascript">
$(document).ready(function() {
	$("#droppable .menulistoverflow").mCustomScrollbar({
		scrollInertia:1000,
		theme:"dark-thin"
	});
	$("#quickmenubox").hide();
});
</script>
<!-- Quick Menu Box Over -->

<div class="whitetitlebox">Dashboard</div>
<div class="helpicon"><a target="_blank" href="https://www.proxscripts.com/docs/Dashboard_Overview">Help</a></div>
	    <div class="statistictablesmain">
	      <div class="statistictables">
			<div class="statistictablehead">
				<a class="adminnote2" href="javascript:void(0)" style="display: none;" onclick="FadeInOut('#adminnote');">Admin Note</a>
				<?php echo $this->Js->link('Admin Note', array('controller'=>'overview', "action"=>"adminnote"), array(
					'update'=>'#adminnote',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'adminnote1'
				));?>
			</div>
			<div class="statistictablecontent" id="adminnote"></div>
	      </div>
	      
	      <div class="statistictables">
			<div class="statistictablehead">
				<a class="memberstat2" href="javascript:void(0)" style="display: none;" onclick="FadeInOut('#memberstat');">Member Statistics</a>
				<?php echo $this->Js->link('Member Statistics', array('controller'=>'overview', "action"=>"memberstatistic"), array(
					'update'=>'#memberstat',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'memberstat1'
				));?>
			</div>
			<div class="statistictablecontent" id="memberstat"></div>
	      </div>
	      
	      <div class="statistictables">
			<div class="statistictablehead">
				<a class="adstatistics2" href="javascript:void(0)" style="display: none;" onclick="FadeInOut('#adstatistics');">Ads Statistics</a>
				<?php echo $this->Js->link('Ads Statistics', array('controller'=>'overview', "action"=>"adsstatistic"), array(
					'update'=>'#adstatistics',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'adstatistics1'
				));?>
			</div>
			<div class="statistictablecontent" id="adstatistics"></div>
	      </div>
	      
	      <div class="statistictables">
			<div class="statistictablehead">
				<a class="financialstat2" href="javascript:void(0)" style="display: none;" onclick="FadeInOut('#financialstat');">Financial Statistics</a>
				<?php echo $this->Js->link('Financial Statistics', array('controller'=>'overview', "action"=>"financialstatistic"), array(
					'update'=>'#financialstat',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'financialstat1'
				));?>
			</div>
			<div class="statistictablecontent" id="financialstat"></div>
	      </div>
	      
	      <div class="statistictables notification">
		
		<div class="tableleft mobilewidth100">
		  <div class="statistictablecontent">
		    <div class="statistictablehead">
				<a class="notificationstat2" href="javascript:void(0)" style="display: none;" onclick="FadeInOut('#notificationstat');">Notifications</a>
				<?php echo $this->Js->link('Notifications', array('controller'=>'overview', "action"=>"notification"), array(
					'update'=>'#notificationstat',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'notificationstat1'
				));?>
		    </div>
		    <div id="notificationstat"></div>
		  </div>
		</div>
		
		<div class="tableright mobilewidth100">
		  <div class="statistictablecontent">
		    <div class="statistictablehead textleft">
				<a class="processorstat2" href="javascript:void(0)" style="display: none;" onclick="FadeInOut('#processorstat');">Processors Statistics</a>
				<?php echo $this->Js->link('Processors Statistics', array('controller'=>'overview', "action"=>"processorstatistic"), array(
					'update'=>'#processorstat',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'processorstat1'
				));?>
		    </div>
		    <div id="processorstat"></div>
		  </div>
		</div>
		<div class="clearboth"></div>
	      </div>
	      
	    </div>




<div id="overviewpage">
<?php } ?>	
	
<?php if(!$ajax) { ?>
</div>
<?php } ?>
<!-- END AnythingSlider -->