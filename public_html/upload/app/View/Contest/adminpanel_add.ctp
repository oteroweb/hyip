<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Contest</div>
<div id="contestpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Text_Links" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>

<div class="backgroundwhite">
		
		<?php echo $this->Form->create('Contest',array('type' => 'post', 'id'=>'TextlinkForm', 'onsubmit' => 'return false;','novalidate' => 'novalidate','url'=>array('controller'=>'contest','action'=>'addaction')));?>
		<?php if(isset($contestdata['Contest']["id"])){
			echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$contestdata['Contest']["id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}?>
		
		<div class="frommain">
			
			<div class="fromnewtext">Status :</div>
			<div class="fromborderdropedown3">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('status', array(
							'type' => 'select',
							'options' => array('1'=>'Active', '0'=>'Inactive'),
							'selected' => $contestdata['Contest']["status"],
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));
						?>
					</label>
				</div>
			</div>
			<div class="fromnewtext">Contest Name :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('nm', array('type'=>'text', 'value'=>$contestdata['Contest']["nm"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			
			<div class="fromnewtext">Start Date :<span class="red-color">*</span> </div>
			<?php echo $this->Form->input('startdate', array('type'=>'text', 'readonly'=>true, 'value'=>$contestdata['Contest']["startdate"], 'label' => false, 'div' => false, 'class'=>'fromboxbgcal width93 datepicker'));?>
			
			<div class="fromnewtext">End Date :<span class="red-color">*</span> </div>
			<?php echo $this->Form->input('enddate', array('type'=>'text', 'readonly'=>true, 'value'=>$contestdata['Contest']["enddate"], 'label' => false, 'div' => false, 'class'=>'fromboxbgcal width93 datepicker'));?>
			
			<div class="fromnewtext">Description : </div>
			<div class="frombordermain">
				<?php echo $this->Form->input('description', array('type'=>'textarea', 'value'=>stripslashes($contestdata['Contest']['description']),'label' => false, 'div' => false, 'class'=>'from-textarea', 'id'=>'TextlinkContent'));?>
			</div>
			
			<div class="fromnewtext">Number of winner :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('numofwinner', array('type'=>'text', 'value'=>$contestdata['Contest']["numofwinner"], 'label' => false, 'div' => false, 'class'=>'fromboxbg' ,'onkeyup'=>'contestprizelevel(this.value)'));?>
			</div>
			<script>
				function contestprizelevel(plavel) {
					for (cpl=1;cpl<=plavel;cpl++) {
						$('.contestprize'+cpl).show();
					}
					plavel=Number(plavel)+1;
					for (cpl=plavel;cpl<=50;cpl++) {
						$('.contestprize'+cpl).hide();
					}
				}	
			</script>
			<?php
			$amount=explode(',',$contestdata['Contest']["amount"]);
			$balance=explode(',',$contestdata['Contest']["balance"]);
			$processor=explode(',',$contestdata['Contest']["processor"]);
			$bennercredit=explode(',',$contestdata['Contest']["bennercredit"]);
			$textcredit=explode(',',$contestdata['Contest']["textcredit"]);
			$solocredit=explode(',',$contestdata['Contest']["solocredit"]);
			$ppc=explode(',',$contestdata['Contest']["ppc"]);
			$ptc=explode(',',$contestdata['Contest']["ptc"]);
			$loginad=explode(',',$contestdata['Contest']["loginad"]);
			$bizdirectory=explode(',',$contestdata['Contest']["bizdirectory"]);
			$webcredits=explode(',',$contestdata['Contest']["webcredits"]);
			$displayplan="";
			if(isset($contestdata['Contest']["id"]))
			{
				$displayplan="display:none;";
				$modulesprize=@explode('|',$contestdata['Contest']["module"]);
			} ?>
			
			<?php for($p=1;$p<=50;$p++){ ?>
			<div class="fromnewtext contestprize<?php echo $p;?>">Prize <?php echo $p;?> :</div>
			<div class="fromgreybox textleft contestprize<?php echo $p;?>">
				<div>
					<div class="levalwhitfromtext contestfromtext" >Amount </div>
					<div class="levalwhitfrombox contestselectbox" ><?php echo $this->Form->input('amount.', array('type'=>'text', 'value'=>@$amount[$p-1], 'label' => false, 'div' => false, 'class'=>'contestfromboxbg4'));?></div>
					<div class="levalwhitfromtext contestfromtext">Balance </div>
					<div class="levalwhitfrombox contestselectbox">
					 <div class="select-main">
						  <label>
							<?php
								$pmethod=array('cash'=>'Cash Balance','repurchase'=>'Re-purchase Balance','earning'=>'Earning Balance','commission'=>'Commission Balance');
								if($SITECONFIG["wallet_for_commission"] == 'cash' && $SITECONFIG["wallet_for_earning"] == 'cash')
								{
									$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase');
								}
								elseif($SITECONFIG["wallet_for_earning"] == 'cash')
								{
									$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'commission'=>'Commission');
								}
								elseif($SITECONFIG["wallet_for_commission"] == 'cash')
								{
									$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning');
								}
							echo $this->Form->input('balance.', array(
								'type' => 'select',
								'options' => $pmethod,
								'selected' => @$balance[$p-1],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));?>
						  </label>
					  </div>
					 <?php if($SITECONFIG["balance_type"]==1){ echo $this->Form->input('processor.', array('type'=>'hidden','value'=>0, 'label' => false)); }?>
					</div>
					<?php if($SITECONFIG["balance_type"]==2){ ?>
					<div class="levalwhitfromtext contestfromtext">Payment Processor </div>
					<div class="levalwhitfrombox contestselectbox">
					 <div class="select-main">
						  <label>
							<?php
							echo $this->Form->input('processor.', array(
										'type' => 'select',
										'options' => $paymentprocessors,
										'selected' => @$processor[$p-1],
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
							));?>
						  </label>
					  </div>
					</div>
					<?php } ?>
					<?php if(strpos($SITECONFIG["banneradplansetting"],'isenable|1') !== false){ ?>
					<div class="levalwhitfromtext contestfromtext">Banner Ad Plan </div>
					<div class="levalwhitfrombox contestselectbox">
					 <div class="select-main">
						  <label>
							<?php
							$bannerads[0]="Select Banner Ad Plan";
							ksort($bannerads);
							echo $this->Form->input('bennercredit.', array(
										'type' => 'select',
										'options' => $bannerads,
										'selected' => @$bennercredit[$p-1],
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
							));?>
						  </label>
					  </div>
					</div>
					<?php } ?>
					<?php if(strpos($SITECONFIG["textadplansetting"],'isenable|1') !== false){ ?>
					<div class="levalwhitfromtext contestfromtext">Text Ad Plan </div>
					<div class="levalwhitfrombox contestselectbox">
						<div class="select-main">
						  <label>
							<?php
							$ptextadplans[0]="Select Text Ad Plan";
							ksort($ptextadplans);
							echo $this->Form->input('textcredit.', array(
										'type' => 'select',
										'options' => $ptextadplans,
										'selected' => @$textcredit[$p-1],
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
							));?>
						  </label>
					  </div>
					</div>
					<?php } ?>
					<?php if(strpos($SITECONFIG["soloadplansetting"],'isenable|1') !== false && $SITECONFIG["enable_soloads"]==1){ ?>
					<div class="levalwhitfromtext contestfromtext">Solo Ad Plan </div>
					<div class="levalwhitfrombox contestselectbox">
						<div class="select-main">
							  <label>
								<?php
								$soloadplans[0]="Select Solo Ad Plan";
								ksort($soloadplans);
								echo $this->Form->input('solocredit.', array(
											'type' => 'select',
											'options' => $soloadplans,
											'selected' => @$solocredit[$p-1],
											'class'=>'',
											'label' => false,
											'div' => false,
											'style' => ''
								));?>
							  </label>
						  </div>
					</div>
					<?php } ?>
					
					<?php if(strpos($SITECONFIG["ppcsetting"],'isenable|1') !== false){ ?>
					<div class="levalwhitfromtext contestfromtext">PPC Plan </div>
					<div class="levalwhitfrombox contestselectbox">
					 <div class="select-main">
						  <label>
							<?php
							$ppcplans[0]="Select PPC Plan";
							ksort($ppcplans);
							echo $this->Form->input('ppc.', array(
										'type' => 'select',
										'options' => $ppcplans,
										'selected' => @$ppc[$p-1],
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
							));?>
						  </label>
					  </div>
					</div>
					
					<?php } ?>
					
					<?php if(strpos($SITECONFIG["ptcsetting"],'isenable|1') !== false){ ?>
					<div class="levalwhitfromtext contestfromtext">PTC Plan </div>
					<div class="levalwhitfrombox contestselectbox">
					 <div class="select-main">
						  <label>
							<?php
							$ptcplans[0]="Select PTC Plan";
							ksort($ptcplans);
							echo $this->Form->input('ptc.', array(
										'type' => 'select',
										'options' => $ptcplans,
										'selected' => @$ptc[$p-1],
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
							));?>
						  </label>
					  </div>
					</div>
					
					<?php } ?>
					
					<?php if(strpos($SITECONFIG["loginadsetting"],'isenable|1') !== false){ ?>
					<div class="levalwhitfromtext contestfromtext">Login Ad Plan </div>
					<div class="levalwhitfrombox contestselectbox">
					<div class="select-main">
						  <label>
							<?php
							$loginads[0]="Select Login Ad Plan";
							ksort($loginads);
							echo $this->Form->input('loginad.', array(
								'type' => 'select',
								'options' => $loginads,
								'selected' => @$loginad[$p-1],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));?>
						  </label>
					  </div>
					</div>
					<?php } ?>
					
					<?php if(strpos($SITECONFIG["bizdirectorysetting"],'enablesurfing|1') !== false){ ?>
					<div class="levalwhitfromtext contestfromtext">Biz Directory Plan :</div>
					<div class="levalwhitfrombox contestselectbox">
					<div class="select-main">
						  <label>
							<?php
							$directorys[0]="Select Biz Directory Plan";
							ksort($directorys);
							echo $this->Form->input('bizdirectory.', array(
								'type' => 'select',
								'options' => $directorys,
								'selected' => @$bizdirectory[$p-1],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));?>
						  </label>
					  </div>
					</div>
					<?php } ?>
					
					<?php if(strpos($SITECONFIG["trafficsetting"],'enablewebcredit|1') !== false){ ?>
					<div class="levalwhitfromtext contestfromtext">Website Credit Plan </div>
					<div class="levalwhitfrombox contestselectbox">
					<div class="select-main">
						  <label>
							<?php
								$Webcredits[0]="Select Website Credit Plan";
								ksort($Webcredits);
								echo $this->Form->input('webcredits', array(
									'type' => 'select',
									'options' => $Webcredits,
									'selected' => @$webcredits[$p-1],
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => ''
								));?>
						  </label>
					  </div>
					</div>
					<?php } ?>
				
					<div class="clearboth"></div>
					<?php
					$searchbyoptions=array('0'=>'Select Parameter');
					$onchangestr="";
					$modules=@explode(",",trim($SITECONFIG["modules"],","));
					foreach($modules as $module)
					{
						$modulearray=@explode(":", $module);
						
						if($modulearray[2]==1 && $modulearray[3]!="" && $modulearray[4]!="" && $modulearray[14]==1)
						{
							$subplanarray=explode("-",$modulearray[3]);
							$subpositionarray=explode("-",$modulearray[4]);
							$subplanname=explode("-",$modulearray[13]);
							$subcounter=1;
							foreach($subplanarray as $subplan)
							{
								if(count($subplanarray)==1)
								{
									echo '<div class="fromnewtext">'.$modulearray[0].'</div>';
								}	
								else
								{
									echo '<div class="fromnewtext">'.$subplanname[$subcounter-1]." ".$modulearray[0].'</div>';
								}
								$onchangestr='if($(this).val()==1){generatefreeorderform("'.$modulearray[1].$subcounter.'-'.$p.'", "'.$ADMINURL.'");}';
								?>
								<div class="fromborderdropedown3 <?php echo $modulearray[1].$subcounter.'-'.$p; ?>" style="<?php echo $displayplan; ?>">
									<div class="select-main">
									<label>
									<?php 
										echo $this->Form->input('plantype', array(
											'type' => 'select',
											'options' => array('0'=>'No', '1'=>'Yes'),
											'selected' => '',
											'name'=>'data['.$p.']['.$modulearray[1].$subcounter.'][plantype]',
											'class'=>'',
											'label' => false,
											'div' => false,
											'style' => '',
											'onchange' => $onchangestr.' else{$("#GenerateForm'.$modulearray[1].$subcounter.'-'.$p.'").html("");}'
										));
									?>
									</label>
									</div>
								</div>
								<div id="GenerateForm<?php echo $modulearray[1].$subcounter.'-'.$p; ?>"></div><?php
								if(isset($contestdata['Contest']["id"]))
								{
									$pmoduledata=explode(',',$modulesprize[$p-1]);
									
									foreach($pmoduledata as $pmoduleval)
									{
										if(strpos($pmoduleval,$modulearray[1].$subcounter.'-1') !== false)
										{
											echo $this->Form->input('editmodule', array('type'=>'hidden','id'=>'blank'.$modulearray[1].$subcounter.'-'.$p,'name'=>'data['.$p.']['.$modulearray[1].$subcounter.'][editmodule]', 'value'=>$pmoduleval, 'label' => false));
										}
									}
									$pmodule=explode($modulearray[1].$subcounter."-",$modulesprize[$p-1]);
									
									$pmodulearray=explode('@',$pmodule[1]);
									echo '<span class="hide'.$modulearray[1].$subcounter.'-'.$p.'">';
									if($pmodulearray[0]==1)
										echo $planData[$modulearray[1].$subcounter][$pmodulearray[1]]." ";
									else
										echo 'No Plan Selected ';
									echo '</span>';
									echo "<button class='btngray' onclick='$(this).hide();$(\".hide".$modulearray[1].$subcounter."-".$p."\").hide();$(\".".$modulearray[1].$subcounter."-".$p."\").show();$(\"#blank".$modulearray[1].$subcounter."-".$p."\").val(\"\");'>Change</button>";
								} 
								$subcounter++;
							}
						}
					}
					?>
				</div>
				
			</div>
			<?php } ?>
			<div class="formbutton">
				<?php echo $this->Js->submit('Update', array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'class'=>'btnorange',
				  'div'=>false,
				  'controller'=>'contest',
				  'action'=>'addaction',
				  'url'   => array('controller' => 'contest', 'action' => 'addaction')
				));?>
				<?php 
				echo $this->Js->link("Back", array('controller'=>'contest', "action"=>"index"), array(
					'update'=>'#contestpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btngray'
				));?>
			</div>
			
		</div>
		<?php echo $this->Form->end();?>
			
</div>
<script>contestprizelevel($("#ContestNumofwinner").val());</script>
<?php if(!$ajax){?>
</div><!--#contestpage over-->
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>