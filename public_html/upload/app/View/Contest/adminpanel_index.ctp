<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 25-03-2015
  * Last Modified: 25-03-2015
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Contest</div>
<div id="contestpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Contest" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Contest',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'contest','action'=>'index')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
					<?php 
					echo $this->Form->input('searchby', array(
						  'type' => 'select',
						  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'numofwinner'=>'Number of winner', 'active'=>'Active Contest', 'inactive'=>'Inactive Contest', 'running'=>'Running Contest', 'expire'=>'Expire Contest'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => '',
						  'onchange'=>'if($(this).val()=="inactive" || $(this).val()=="active" || $(this).val()=="expire" || $(this).val()=="running"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);}'
					));
					?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=="inactive" || $searchby=="active" || $searchby=='expire' || $searchby=='running'){ echo 'style="display:none"';} ?>>
			<span>Search For :</span>
			<span class="searchforfields_s"><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#contestpage',
				'class'=>'searchbtn',
				'controller'=>'contest',
				'action'=>'index',
				'url'=> array('controller' => 'contest', 'action' => 'index')
			      ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#contestpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'contest', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Contest']['page'];
	?>
	
<div id="Xgride-bg">
	<div class="height10"></div>
    <div class="padding10 backgroundwhite">
		<div class="greenbottomborder">
		<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
				
    <?php if(!isset($SubadminAccessArray) || in_array('contest',$SubadminAccessArray) || in_array('contest/adminpanel_add',$SubadminAccessArray)){ ?>
            <?php echo $this->Js->link("+ Add New", array('controller'=>'contest', "action"=>"add"), array(
                'update'=>'#contestpage',
                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                'escape'=>false,
                'class'=>'btnorange'
            ));?>
    <?php } ?>
	</div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'contest', "action"=>"index/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#contestpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Contest Name', array('controller'=>'contest', "action"=>"index/0/0/nm/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#contestpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Contest Name'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Start Date', array('controller'=>'contest', "action"=>"index/0/0/startdate/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#contestpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Start Date'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('End Date', array('controller'=>'contest', "action"=>"index/0/0/enddate/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#contestpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By End Date '
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Number of winner', array('controller'=>'contest', "action"=>"index/0/0/numofwinner/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#contestpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Number of winner'
					));?>
				</div>
				<div><?php echo 'Status';?></div>
				<div><?php echo 'Action';?></div>
			</div>
			<?php foreach ($contests as $contest): ?>
				<div class="tablegridrow">
					<div><?php echo $contest['Contest']['id']; ?></div>
					<div><?php echo $contest['Contest']['nm']; ?></div>
					<div><?php echo trim($this->Time->format($SITECONFIG["timeformate"], $contest['Contest']['startdate']),' 00:00:00'); ?></div>
					<div><?php echo trim($this->Time->format($SITECONFIG["timeformate"], $contest['Contest']['enddate']),' 00:00:00'); ?></div>
					<div><?php echo $contest['Contest']['numofwinner']; ?></div>
					<div><?php if($contest['Contest']['enddate']<date('Y-m-d')){ echo 'Expire';}else{ echo 'Running';} ?></div>
					
						<div>
						       <div class="actionmenu">
							 <div class="btn-group">
							       <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
								       Action <span class="caret"></span>
							       </button>
							       <ul class="dropdown-menu" role="menu">
										       
								       <?php if(!isset($SubadminAccessArray) || in_array('contest',$SubadminAccessArray) || in_array('contest/adminpanel_add/$',$SubadminAccessArray)){?>
								       <li>
								       <?php
									       echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Contest'))." Edit", array('controller'=>'contest', "action"=>"add/".$contest['Contest']['id']), array(
										       'update'=>'#contestpage',
										       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										       'escape'=>false,
										       'class'=>''
									       ));?>
								       </li>
								       <?php }?>
						       
								       <?php 
								       if(!isset($SubadminAccessArray) || in_array('contest',$SubadminAccessArray) || in_array('contest/adminpanel_status',$SubadminAccessArray)){?>
								       <li>
								       <?php
									       if($contest['Contest']['status']==0)
									       {
										       $field_reqaction='1';
										       $field_reqicon='red-icon.png';
										       $field_reqtext='Activate';
									       }
									       else
									       {
										       $field_reqaction='0';
										       $field_reqicon='blue-icon.png';
										       $field_reqtext='Inactivate';
									       }
									       echo $this->Js->link($this->html->image($field_reqicon, array('alt'=>$field_reqtext))." ".$field_reqtext, array('controller'=>'contest', "action"=>"status/".$field_reqaction."/".$contest['Contest']['id']."/".$currentpagenumber), array(
										       'update'=>'#contestpage',
										       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										       'escape'=>false,
										       'class'=>''
									       ));?>
								       </li>
								       <?php }?>
								       
								       <?php if(!isset($SubadminAccessArray) || in_array('contest',$SubadminAccessArray) || in_array('contest/adminpanel_remove',$SubadminAccessArray)){?>
								       <li>
								       <?php
									       echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Contest'))." Delete", array('controller'=>'contest', "action"=>"remove/".$contest['Contest']['id']."/".$currentpagenumber), array(
										       'update'=>'#contestpage',
										       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										       'escape'=>false,
										       'confirm'=>'Are You Sure?',
										       'class'=>''
									       ));?>
								       </li>
								       <?php }?>
								       <li>
								       <?php
									       echo $this->Js->link($this->html->image('view-members.png', array('alt'=>'Member'))." Member", array('controller'=>'contest', "action"=>"member/".$contest['Contest']['id']), array(
										       'update'=>'#contestpage',
										       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										       'escape'=>false,
										       'class'=>''
									       ));?>
								       </li>
								        <?php if($contest['Contest']['enddate']<date('Y-m-d') && $contest['Contest']['ispaid']==0){ ?>
									<li>
								       <?php
									       echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Pay To Winners'))." Pay To Winners", array('controller'=>'contest', "action"=>"pay/".$contest['Contest']['id']), array(
										       'update'=>'#contestpage',
										       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										       'escape'=>false,
										       'class'=>''
									       ));?>
								       </li>
								       <?php }?>
								       
							       </ul>
							 </div>
						 </div>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($contests)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php
	if($this->params['paging']['Contest']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Contest',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'contest','action'=>'index/0/rpp')));?>
		
		<div class="resultperpage">
                        <label>
			    <?php 
			    echo $this->Form->input('resultperpage', array(
			      'type' => 'select',
			      'options' => $resultperpage,
			      'selected' => $this->Session->read('pagerecord'),
			      'class'=>'',
			      'label' => false,
			      'div'=>false,
			      'style' => '',
			      'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			    ));
			    ?>
			</label>
		</div>
		
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#contestpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'contest',
			  'action'=>'index/0/rpp',
			  'url'   => array('controller' => 'contest', 'action' => 'index/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
		</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#contestpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>