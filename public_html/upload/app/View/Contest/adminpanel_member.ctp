<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 25-03-2015
  * Last Modified: 25-03-2015
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Contest</div>
<div id="contestpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Contest" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
	
<div id="Xgride-bg">
	<div class="height10"></div>
    <div class="padding10 backgroundwhite">
	<div class="greenbottomborder">
	<div class="height10"></div>
	<div class="addnew-button">
	 <?php echo $this->Js->link("Back", array('controller'=>'contest', "action"=>"index"), array(
                'update'=>'#contestpage',
                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                'escape'=>false,
                'class'=>'btngray'
            ));?>
	</div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
		<div class="tablegridheader">
			<div>M. Id</div>
			<div>Reg. Date</div>
			<div>Username</div>
			<div>Full Name</div>
			<div>Email</div>
			<div>Tot Ref.</div>
		</div>
		<?php foreach ($members as $member): ?>
			<div class="tablegridrow">
				<div>
					<?php echo $this->Js->link($member['Member']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/contest/index~top"), array(
						'update'=>'#pagecontent',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'View Member'
					));?>
				</div>
				<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $member['Member']['reg_dt']);?></div>
				<div>
					<?php echo $this->Js->link($member['Member']['user_name'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/contest/index~top"), array(
						'update'=>'#pagecontent',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'View Member'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link($member['Member']['f_name']." ".$member['Member']['l_name'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/contest/index~top"), array(
						'update'=>'#pagecontent',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'View Member'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link($member['Member']['email'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/contest/index~top"), array(
						'update'=>'#pagecontent',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'View Member'
					));?>
				</div>
				<div><?php echo $member['Member']['total_referrer']; ?></div>
			</div>
		<?php endforeach; ?>
	
	</div>
	<?php if(count($members)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	</div>
	
    </div>
    
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#contestpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>