<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<script type="text/javascript">
	$(".included-menu ul li a").click(function()
	{
		$(".includedoverlay").addClass("addoverlay");
	});
	$(".includedoverlay").click(function()
	{
		$(".included-menu ul li span").hide();
		$(this).removeClass("addoverlay");
	});
</script>
<?php if($themesubmenuaccess==1) { ?>
<?php if(!$ajax){ ?>
<div id="contestpage">
<?php } ?>

<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>

<div class="includedoverlay"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Contest Details');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php // Text ad add/edit form starts here ?>
	<div class="form-box">
		<div class="purchaseplanmain">	
		<div class="form-row">
			<div class="form-col-1"><?php echo __('Name')?> :</div>
			<div class="form-col-2 form-text"><?php echo $ContestData['Contest']['nm'];?></div>
		</div>
		<div class="form-row">
			<div class="form-col-1"><?php echo __('Start Date')?> :</div>
			<div class="form-col-2 form-text"><?php echo trim($this->Time->format($SITECONFIG["timeformate"], $ContestData['Contest']['startdate']),' 00:00:00'); ?></div>
		</div>
		<div class="form-row">
			<div class="form-col-1"><?php echo __('End Date')?> :</div>
			<div class="form-col-2 form-text"><?php echo trim($this->Time->format($SITECONFIG["timeformate"], $ContestData['Contest']['enddate']),' 00:00:00'); ?></div>
		</div>
		<?php
		$amount=explode(',',$ContestData['Contest']["amount"]);
		$balance=explode(',',$ContestData['Contest']["balance"]);
		$processor=@explode(',',$contestdata['Contest']["processor"]);
		$bennercredit=explode(',',$ContestData['Contest']["bennercredit"]);
		$textcredit=explode(',',$ContestData['Contest']["textcredit"]);
		$solocredit=explode(',',$ContestData['Contest']["solocredit"]);
		$ppc=explode(',',$ContestData['Contest']["ppc"]);
		$ptc=explode(',',$ContestData['Contest']["ptc"]);
		$loginad=explode(',',$ContestData['Contest']["loginad"]);
		$bizdirectory=explode(',',$ContestData['Contest']["bizdirectory"]);
		$webcredits=explode(',',$ContestData['Contest']["webcredits"]);
		$modulesprize=@explode('|',$ContestData['Contest']["module"]);
		
		for($p=0;$p<$ContestData['Contest']['numofwinner'];$p++)
		{ ?>
		<div class="form-row">
			<div class="form-col-1"><?php echo __('Prizes')." ".($p+1);?> :</div>
			<div class="form-col-2 form-text">
				<div class="included-menu">
					<ul>
					<?php 
					if(isset($bennercredit[$p]) && $bennercredit[$p]>0 && strpos($SITECONFIG["banneradplansetting"],'isenable|1') !== false)
					{
						echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Banneradplan\', \'.bannerplanlist\', \'id:'.$bennercredit[$p].'\', \''.$SITEURL.'\', \'bannerad/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("Banner Ad Plan")."</a>";
						echo '<span class="showincludecontent bannerplanlist"></span></li>';
					}
					
					if(isset($textcredit[$p]) && $textcredit[$p]>0 && strpos($SITECONFIG["textadplansetting"],'isenable|1') !== false)
					{
						echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Ptextadplan\', \'.textplanlist\', \'id:'.$textcredit[$p].'\', \''.$SITEURL.'\', \'textad/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("Text Ad Plan")."</a>";
						echo '<span class="showincludecontent textplanlist"></span></li>';
					}
			
					if(isset($solocredit[$p]) && $solocredit[$p]>0 && strpos($SITECONFIG["soloadplansetting"],'isenable|1') !== false)
					{
						echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Soloadplan\', \'.soloplanlist\', \'id:'.$solocredit[$p].'\', \''.$SITEURL.'\', \'soload/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("Solo Ad Plan")."</a>";
						echo '<span class="showincludecontent soloplanlist"></span></li>';
					}
					if(isset($ppc[$p]) && $ppc[$p]>0 && strpos($SITECONFIG["ppcsetting"],'isenable|1') !== false)
					{
						echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Ppcplan\', \'.ppcplanlist\', \'id:'.$ppc[$p].'\', \''.$SITEURL.'\', \'ppc/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("PPC Ad Plan")."</a>";
						echo '<span class="showincludecontent ppcplanlist"></span></li>';
					}
					if(isset($ptc[$p]) && $ptc[$p]>0 && strpos($SITECONFIG["ptcsetting"],'isenable|1') !== false)
					{
						echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Ptcplan\', \'.ptcplanlist\', \'id:'.$ptc[$p].'\', \''.$SITEURL.'\', \'ptc/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("PTC Ad Plan")."</a>";
						echo '<span class="showincludecontent ptcplanlist"></span></li>';
					}
					if(isset($loginad[$p]) && $loginad[$p]>0 && strpos($SITECONFIG["loginadsetting"],'isenable|1') !== false)
					{
						echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Loginad\', \'.loginadplanlist\', \'id:'.$loginad[$p].'\', \''.$SITEURL.'\', \'loginad/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("Login Ad Plan")."</a>";
						echo '<span class="showincludecontent loginadplanlist"></span></li>';
					}
					if(isset($bizdirectory[$p]) && $bizdirectory[$p]>0 && strpos($SITECONFIG["bizdirectorysetting"],'enablesurfing|1') !== false)
					{
						echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Directoryplan\', \'.directoryplanlist\', \'id:'.$bizdirectory[$p].'\', \''.$SITEURL.'\', \'directory/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("Biz Directory Plan")."</a>";
						echo '<span class="showincludecontent directoryplanlist"></span></li>';
					}
					if(isset($webcredits[$p]) && $webcredits[$p]>0 && strpos($SITECONFIG["trafficsetting"],'enablewebcredit|1') !== false)
					{
						echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Webcreditplan\', \'.webcreditplanlist\', \'id:'.$webcredits[$p].'\', \''.$SITEURL.'\', \'traffic/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("Traffic Plan")."</a>";
						echo '<span class="showincludecontent webcreditplanlist"></span></li>';
					}
					?>
					</ul>
				</div>
				<?php if(isset($amount[$p]) && $amount[$p]>0) 
				{
					echo 'Amount : '.$Currency['prefix'].round($amount[$p]*$Currency['rate'],4)." ".$Currency['suffix']."</br>";
					if($balance[$p]=='repurchase')
						echo 'Balance : Re-purchase Balance';
					else
						echo ' Balance : '.ucwords($balance[$p]).' Balance';
					if($SITECONFIG["balance_type"]==2 && isset($processor[$p]) && $processor[$p]>0)
						echo '</br> Payment Processor : '.$paymentprocessors[$processor[$p]];
					echo '</br>';	
				} ?>
				<?php
				$modules=@explode(",",trim($SITECONFIG["modules"],","));
				foreach($modules as $module)
				{
					$modulearray=@explode(":", $module);
					
					if($modulearray[2]==1 && $modulearray[3]!="" && $modulearray[4]!="" && $modulearray[14]==1)
					{
						$subplanarray=explode("-",$modulearray[3]);
						$subpositionarray=explode("-",$modulearray[4]);
						$subplanname=explode("-",$modulearray[13]);
						$subcounter=1;
						foreach($subplanarray as $subplan)
						{
							$pmoduledata=explode(',',$modulesprize[$p]);
							$pmodule=explode($modulearray[1].$subcounter."-",$modulesprize[$p]);
							
							$pmodulearray=explode('@',$pmodule[1]);
							
							if($pmodulearray[0]==1)
							{
								
								if(count($subplanarray)==1)
								{
									echo '</br><b><span>'.$modulearray[0].'</b></span>';
								}	
								else
								{
									echo '</br><b><span>'.$subplanname[$subcounter-1]." ".$modulearray[0].'</b></span>';
								}
								echo '</br><span> Plan Name : '.$planData[$modulearray[1].$subcounter][$pmodulearray[1]].'</span>';
								if(strpos($pmodulearray[2], 'A:')!==false)
								{
									$planamount=str_replace('A:','',$pmodulearray[2]);
									echo '</br><span> Investment Amount : '.$Currency['prefix'].round($planamount*$Currency['rate'],4)." ".$Currency['suffix'].'</span></br>';
								}
								else
								{
									echo '</br><span> Positions : '.$pmodulearray[2].'</span></br>';
								}
								
							}	
							
							$subcounter++;
						}
					}
				}
				?>
			</br>
			</div>
		</div>
		<?php } ?>
		<div class="form-row">
			<div class="form-col-1"><?php echo __('Status')?> :</div>
			<div class="form-col-2 form-text"><?php if($ContestData['Contest']['enddate']<date('Y-m-d')){ echo __("Expire");}else{ echo __("Running");}?></div>
		</div>
		<div class="form-row">
			<div class="form-col-1"><?php echo __('Description')?> :</div>
			<div class="form-col-2 form-text"><?php echo $ContestData['Contest']['description'];?></div>
		</div>
		</div>
	</div>
	<div class="clear-both"></div>
	<?php // Text ad add/edit form ends here ?>
</div>

<script>$('.showincludecontent').hide();</script>
<script type="text/javascript">
	$(".included-menu ul li a").click(function()
	{
		$(".includedoverlay").addClass("addoverlay");
	});
	$(".includedoverlay").click(function()
	{
		$(".included-menu ul li span").hide();
		$(this).removeClass("addoverlay");
	});
</script>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Leader Bord');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php // My Text Ad Plans table starts here ?>
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam"><?php echo __("No"); ?></div>
				<div class="divth textcenter vam"><?php echo __("Member Id"); ?></div>
				<div class="divth textcenter vam"><?php echo __("Username"); ?></div>
				<div class="divth textcenter vam"><?php echo __("Tot Ref."); ?></div>
			</div>
		</div>
		<div class="divtbody">
			<?php $i=1;
			
			foreach ($members as $member):
			if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $i;?></div>
					<div class="divtd textcenter vam"><?php echo $member['Member']['member_id'];?></div>
					<div class="divtd textcenter vam"><?php echo $member['Member']['user_name'];?></div>
					<div class="divtd textcenter vam"><?php echo $member['Member']['total_referrer'];?></div>
					
				</div>
			<?php $i++;endforeach; ?>
		</div>
	</div>
	<?php if(count($members)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
	<?php // My Text Ad Plans table ends here ?>
	<div class="formbutton">
		<?php echo $this->Js->link(__("Back"), array('controller'=>'contest', "action"=>"index"), array(
		'update'=>'#contestpage',
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'escape'=>false,
		'class'=>'button',
		'style'=>'float:left'
		));?>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>
		
</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>