<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 15-04-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($themesubmenuaccess){?>
<?php if(!$ajax){ ?>
<div id="contestpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<?php // Top menu code starts here ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Referral Contests");?></div>
	<div class="clear-both"></div>
</div>
<?php echo $this->Javascript->link('allpage');?>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#contestpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'Contest', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Contest']['page'];
	?>
	<div class="main-box-eran">
	
	<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
	<div class="clear-both"></div>
	<div class="height5"></div>
			<div class="divtable textcenter">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam"><?php echo __("Name");?></div>
						<div class="divth textcenter vam"><?php echo __("Start Date");?></div>
						<div class="divth textcenter vam"><?php echo __("End Date");?></div>
						<div class="divth textcenter vam"><?php echo __("Status");?></div>
						<div class="divth textcenter vam"><?php echo __("Action");?></div>
					</div>
				</div>
				<div class="divtbody">
					<div class="divtr">
						<div class="divtd ovw-padding-tabal"></div>
					</div>
					<?php $i=1;
					foreach($contests as $contest):
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>

						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam"><?php echo $contest['Contest']['nm'];?></div>
							<div class="divtd textcenter vam"><?php echo trim($this->Time->format($SITECONFIG["timeformate"], $contest['Contest']['startdate']),' 00:00:00'); ?></div>
							<div class="divtd textcenter vam"><?php echo trim($this->Time->format($SITECONFIG["timeformate"], $contest['Contest']['enddate']),' 00:00:00'); ?></div>
							<div class="divtd textcenter vam"><?php if($contest['Contest']['enddate']<date('Y-m-d')){ echo __("Expire");}else{ echo __("Running");}?></div>
							<div class="divtd textcenter vam">
								<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Details'))), array('controller'=>'Contest', "action"=>"details/".$contest['Contest']['id']), array(
									'update'=>'#contestpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>__('Details')
								));?>
							</div>
						</div>
					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($contests)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // Text ads table starts here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Contest']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
	<div class="pag-float-left">
		<div class="ul-bg">
			<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Contest',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'Contest','action'=>'index/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#contestpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'Contest',
						  'action'=>'index/rpp',
						  'url'   => array('controller' => 'Contest', 'action' => 'index/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
			</ul>
		<div class="clear-both"></div>
		</div>
	</div>
	<?php }?>
	<div class="floatright ul-bg">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
		</ul>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>
	
</div>
<?php if(!$ajax){?>
</div>
<?php } ?>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>