<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Member Group / List</div>

<div id="memberpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_Group" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'membergroup', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Membergroup']['page'];
	?>
	
<div id="Xgride-bg" class="noborder">
    <div class="padding10 backgroundwhite">
		
		<div class="greenbottomborder">
			<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
		<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_add',$SubadminAccessArray)){ ?>
			<?php echo $this->Js->link("+ Add New", array('controller'=>'membergroup', "action"=>"add"), array(
                'update'=>'#memberpage',
                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                'escape'=>false,
                'class'=>'btnorange'
            ));?>
        <?php } ?>
    </div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Group Id', array('controller'=>'membergroup', "action"=>"index/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Group Id'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Group Name', array('controller'=>'membergroup', "action"=>"index/0/0/groupname/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Registration Date'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Group Color', array('controller'=>'membergroup', "action"=>"index/0/0/groupcolor/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Username'
					));?>
				</div>
				<div>Action</div>
			</div>
			<?php foreach ($membergroups as $membergroup): ?>
				<div class="tablegridrow">
					<div><?php echo $membergroup['Membergroup']['id'];?></div>
					<div><?php echo $membergroup['Membergroup']['groupname'];?></div>
					<div><span style='padding:5px 10px 5px 10px;background-color:<?php echo $membergroup['Membergroup']['groupcolor'];?>' ></span></div>
					<div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
                          <?php if(!isset($SubadminAccessArray) || in_array('membergroup',$SubadminAccessArray) || in_array('membergroup/adminpanel_add/$',$SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Member Group')).' Edit Member Group', array('controller'=>'membergroup', "action"=>"add/".$membergroup['Membergroup']['id']."/top"), array(
									'update'=>'#memberpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));?>
							</li>
                            <?php } ?>
                            <?php if(!isset($SubadminAccessArray) || in_array('membergroup',$SubadminAccessArray) || in_array('membergroup/adminpanel_remove',$SubadminAccessArray)){ ?>
							<li>
									<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Member Group')).' Delete Member Group', array('controller'=>'membergroup', "action"=>"remove/".$membergroup['Membergroup']['id']."/".$currentpagenumber), array(
                                        'update'=>'#memberpage',
                                        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                        'escape'=>false,
                                        'confirm'=>"Do You Really Want to Delete This Group?"
                                    ));?>
								<?php }?>
							</li>
                          </ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>		
	</div>
	
	<?php if(count($membergroups)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
	<?php 
	if($this->params['paging']['Membergroup']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Membergroup',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'membergroup','action'=>'index/0/rpp')));?>
		<div class="resultperpage">
                        <label>
			    <?php 
			    echo $this->Form->input('resultperpage', array(
			      'type' => 'select',
			      'options' => $resultperpage,
			      'selected' => $this->Session->read('pagerecord'),
			      'class'=>'',
			      'label' => false,
			      'div'=>false,
			      'style' => '',
			      'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			    ));
			    ?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#memberpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'membergroup',
			  'action'=>'index/0/rpp',
			  'url'   => array('controller' => 'membergroup', 'action' => 'index/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#memberpage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>