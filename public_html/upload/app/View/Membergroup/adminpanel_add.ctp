<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<script type="text/javascript">$(function() {$('.colorpicker').wheelColorPicker({ sliders: "whsvp", preview: true, format: "css" });});</script>
<?php if(!$ajax){?>
<div class="whitetitlebox">Member Group / List</div>
<div id="memberpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_Group" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="backgroundwhite">
	
			
	<?php echo $this->Form->create('Membergroup',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'membergroup','action'=>'addaction')));?>
	<?php if(isset($membergroup['Membergroup']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$membergroup['Membergroup']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
			
	<div class="frommain">
		
		<div class="fromnewtext">Group Name :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('groupname', array('type'=>'text', 'value'=>$membergroup['Membergroup']["groupname"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Group Color :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('groupcolor', array('type'=>'text', 'value'=>$membergroup['Membergroup']["groupcolor"], 'label' => false, 'div'=>false, 'class'=>'fromboxbg colorpicker'));?>
		</div>
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'class'=>'btnorange',
			  'div'=>false,
			  'controller'=>'membergroup',
			  'action'=>'addaction',
			  'url'   => array('controller' => 'membergroup', 'action' => 'addaction')
			));?>
			<?php echo $this->Js->link("Back", array('controller'=>'membergroup', "action"=>"index"), array(
				'update'=>'#memberpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'btngray'
			));?>
		</div>
		
	</div>
	<?php echo $this->Form->end();?>
	
	
</div>
	<?php if(!$ajax){?>
</div><!--#memberpage over-->
<?php }?>
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>