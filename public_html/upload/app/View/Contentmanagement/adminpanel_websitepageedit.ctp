<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 29-12-2014
  *********************************************************************/
?>
<?php 
if($textarea=='advanced')
{
	echo $this->Javascript->link('tiny_mce/tiny_mce.js');
	echo $this->Javascript->link('tinymce');
	$textareaclass='advancededitor';
	$submitbuttonclass='tinyMCEtriggerSavetk';
}
elseif($textarea=='simple')
{
	$textareaclass='';
	$submitbuttonclass='';
}
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Website Pages</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Website Pages", array('controller'=>'contentmanagement', "action"=>"index"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Website Page Categories", array('controller'=>'contentmanagement', "action"=>"pagecategory"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="contentmanagementpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Website_Pages#Website_Pages" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="backgroundwhite">
	
		<?php echo $this->Form->create('Web_page',array('type' => 'post', 'id'=>'Web_pageForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'contentmanagement','action'=>'websitepageupdate')));?>
						
		<?php if(isset($web_pagedata['Web_page']['page_id'])){
			echo $this->Form->input('page_id', array('type'=>'hidden', 'value'=>$web_pagedata['Web_page']['page_id'], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}?>
			
		<div class="frommain">
			
			<div class="fromnewtext">Category :</div>
			<div class="fromborderdropedown3">
				<div class="select-main">
				  <label>
					<?php 
						echo $this->Form->input('category', array(
							'type' => 'select',
							'options' => $category,
							'selected' => $web_pagedata['Web_page']["category"],
							'class'=>'',
							'div' => false,
							'label' => false,
							'style' => ''
						));
					?>
				  </label>
				</div>
            </div>
			<?php if(!isset($web_pagedata['Web_page']['page_id'])){ ?>
			<div class="fromnewtext">Unique Name :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('page', array('type'=>'text', 'value'=>stripslashes($web_pagedata['Web_page']["page"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<?php } ?>
			<div class="fromnewtext">Title (For SEO) :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('title', array('type'=>'text', 'value'=>stripslashes($web_pagedata['Web_page']["title"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			
			<div class="fromnewtext">Keywords (For SEO) : </div>
			<div class="frombordermain">
				<?php echo $this->Form->input('keywords', array('type'=>'textarea', 'value'=>stripslashes($web_pagedata['Web_page']["keywords"]), 'label' => false, 'div' => false,  'class'=>'from-textarea', 'style'=>''));?>
			</div>
			
			<div class="fromnewtext">Description (For SEO) : </div>
			<div class="frombordermain">
				<?php echo $this->Form->input('description', array('type'=>'textarea', 'value'=>stripslashes($web_pagedata['Web_page']["description"]), 'label' => false, 'div' => false,  'class'=>'from-textarea', 'style'=>''));?>
			</div>
			
			<div class="formbutton">
			<?php 
				if($textarea=='simple'){$showtextarea='advanced';}
				if($textarea=='advanced'){$showtextarea='simple';}
				$link='websitepageedit/'.$showtextarea.'/'.$web_pagedata['Web_page']['page_id'];
				echo $this->Js->link("Switch To ".ucfirst($showtextarea)." Mode", array('controller'=>'contentmanagement', "action"=>$link), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btngray'
				));
			?>
			<?php if($textarea=='simple'){ ?>
				<input type="button" value="Copy Same Content To All Language" onclick="if(confirm('Copy English Content Into All Languages')){$('.alllanguagesame').val($('#Web_pageContenteng').val()); }" class="btnorange floatright" style="margin-right: 5px;" />
				<?php }else{ ?>
				<input type="button" value="Copy Same Content To All Language" onclick="if(confirm('Copy English Content Into All Languages')){for (i=0; i < tinyMCE.editors.length; i++){ tinyMCE.editors[i].setContent(tinyMCE.editors[0].getContent()); }}" class="btnorange floatright" style="margin-right: 5px;" />
			<?php } ?>
			</div>
			
			<div class="clearboth"></div>
			
			<div class="newgreytabs">

				<ul class="nav nav-tabs" role="tablist">
				     <?php foreach($languagedata as $code=>$displayname){ $activeclass='';if($code=='eng'){ $activeclass='active'; } ?>
					<li role="presentation" class="<?php echo $activeclass; ?>">
						<a onclick="$('.allactive').hide();$('.<?php echo $code; ?>').show();" class="active" role="tab" data-toggle="tab"><?php echo $displayname ?></a>
					</li>
					<?php } ?>
				</ul>
		      
			      <div class="tab-content">
				  <?php foreach($languagedata as $code=>$displayname){ $activeclass='';if($code!='eng'){ $activeclass='style="display: none" '; } ?>
				  <div <?php echo $activeclass; ?> role="tabpanel" class="tab-pane active allactive <?php echo $code; ?>">
				      <div class="masstextnew">Message (<?php echo $displayname;?>) :<span class="red-color">*</span></div>
				      <div class="greyboxnewtabsin"><?php echo $this->Form->input('content'.$code, array('type'=>'textarea', 'label' => false, 'div' => false, 'class'=>'from-textarea alllanguagesame '.$textareaclass,'value'=>stripslashes($web_pagedata['Web_page']['content'.$code]), 'style'=>'height: 400px;'));?></div>
				  </div>
				<?php }?>
			      </div>
		      </div>
				
			<div class="formbutton">
				<?php echo $this->Js->submit('Submit', array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'class'=>'btnorange '.$submitbuttonclass,
				  'div'=>false,
				  'controller'=>'contentmanagement',
				  'action'=>'websitepageupdate',
				  'url'   => array('controller' => 'contentmanagement', 'action' => 'websitepageupdate')
				));?>
				<?php 
				echo $this->Js->link("Back", array('controller'=>'contentmanagement', "action"=>"index"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'div'=>false,
					'class'=>'btngray'
				));?>
			</div>
			
		</div>
		<?php echo $this->Form->end();?>

</div>
<?php if(!$ajax){?>
</div><!--#contentmanagementpage over-->
</div>
<?php }?>	
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>