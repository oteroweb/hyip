<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Testimonials</div>
<div id="contentmanagementpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Testimonials" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
   
        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#contentmanagementpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'contentmanagement', 'action'=>'testimonials')
        ));
        $currentpagenumber=$this->params['paging']['Testimonial']['page'];
        ?>

<div class="backgroundwhite">
	<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">			
        <?php if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_testimonialadd',$SubadminAccessArray)){ ?>
                <?php echo $this->Js->link("+ Add New", array('controller'=>'contentmanagement', "action"=>"testimonialadd"), array(
                    'update'=>'#contentmanagementpage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'class'=>'btnorange'
                ));?>
        <?php } ?>
	</div>
	<div class="clear-both"></div>
	
		<div class="bordertestimonial"></div>
		<?php
			$i = 0;
			foreach ($testimonialdata as $testimonials):
				if($testimonials['Testimonial']['photo']=='' || $testimonials['Testimonial']['photo']==NULL) $testimonials['Testimonial']['photo']='dummy.jpg';
				$class = 'class="testimonial-blue-box"';if ($i++ % 2 == 0){$class = 'class="testimonial-white-box"';}
				?>
						<div class="testimonalmain">
							<div class="testimonialphoto">
								<div class="testiminalbox">
									<?php echo $this->html->image("testimonials/".$testimonials['Testimonial']['photo'], array('alt'=>'Image','width'=>'68px','height'=>'68px')); ?>
								</div>
							</div>
							<div class="testimonialright">
								<div class="testimonialtitle">
									<?php echo $testimonials['Member']['user_name']; ?>
									<?php 
										echo $this->Js->link($testimonials['Testimonial']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$testimonials['Testimonial']['member_id']."/top/contentmanagement/testimonials~top"), array(
										'update'=>'#pagecontent',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false,
										'class'=>'vtip testimonial-member-id-text',
										'title'=>'View Member',
										'style'=>'font-weight:bold;'
									));?>
								</div>
								<div class="testimonialtexts"><?php echo nl2br(stripslashes($testimonials['Testimonial']['content']));?></div>
								<div class="btn-group">
									<button data-toggle="dropdown" type="button" class="btn btn-default btn-xs dropdown-toggle">
										Action <span class="caret"></span>
									</button>
									<ul role="menu" class="dropdown-menu">
										
										<li>
											<?php 
												if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_testimonialadd/$',$SubadminAccessArray)){
													echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Testimonial')).' Edit Testimonial', array('controller'=>'contentmanagement', "action"=>"testimonialadd/simple/".$testimonials['Testimonial']['test_id']), array(
														'update'=>'#contentmanagementpage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>'vtip',
														'title'=>'Edit Testimonial'
													));
												}
												else
												{
													echo '&nbsp;';
											} ?>
										</li>
										
										<li>
											<?php 
												if($testimonials['Testimonial']['status']==0)
												{
													$field_reqaction='1';
													$field_reqicon='red-icon.png';
													$field_reqtext='Activate Testimonial';
												}
												else
												{
													$field_reqaction='0';
													$field_reqicon='blue-icon.png';
													$field_reqtext='Inactivate Testimonial';
												}
												if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_testimonialstatus',$SubadminAccessArray)){
													echo $this->Js->link($this->html->image($field_reqicon, array('alt'=>$field_reqtext)).' '.$field_reqtext, array('controller'=>'contentmanagement', "action"=>"testimonialstatus/".$field_reqaction."/".$testimonials['Testimonial']['test_id']."/".$currentpagenumber), array(
														'update'=>'#contentmanagementpage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>'vtip',
														'title'=>$field_reqtext
													));
												}
												else
												{
													echo '&nbsp;';
												} ?>
										</li>
										
										<li>
											<?php 
												if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_testimonialremove',$SubadminAccessArray)){
													echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Testimonial')).' Delete Testimonial', array('controller'=>'contentmanagement', "action"=>"testimonialremove/".$testimonials['Testimonial']['test_id']."/".$currentpagenumber), array(
														'update'=>'#contentmanagementpage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'confirm'=>'Are You Sure?',
														'class'=>'vtip',
														'title'=>'Delete Testimonial'
													));
												}
												else
												{
													echo '&nbsp;';
												} ?>
										</li>
									</ul>
								</div>
							</div>
						</div>
		<?php endforeach; ?>
		<?php if(count($testimonialdata)==0){ echo '<div>No records available</div>';} ?>
       	
        <?php
        if($this->params['paging']['Testimonial']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Testimonial',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'contentmanagement','action'=>'testimonials/0/rpp')));?>
			<div class="combobox1">
            <?php 
            echo $this->Form->input('resultperpage', array(
              'type' => 'select',
              'options' => $resultperpage,
              'selected' => $this->Session->read('pagerecord'),
              'class'=>'',
              'label' => false,
              'div'=>false,
              'style' => '',
              'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
            ));
            ?>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#contentmanagementpage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'contentmanagement',
                  'action'=>'testimonials/0/rpp',
                  'url'   => array('controller' => 'contentmanagement', 'action' => 'testimonials/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#contentmanagementpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>