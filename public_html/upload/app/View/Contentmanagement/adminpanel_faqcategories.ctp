<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 21-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / FAQs</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("FAQs", array('controller'=>'contentmanagement', "action"=>"faqs"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("FAQ Categories", array('controller'=>'contentmanagement', "action"=>"faqcategories"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="contentmanagementpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/FAQs#FAQ_Categories" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
   
        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#contentmanagementpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'contentmanagement', 'action'=>'faqcategories')
        ));
        $currentpagenumber=$this->params['paging']['Faq_category']['page'];
        ?>
		
<div id="Xgride-bg">
    <div class="Xpadding10">
	<div class="greenbottomborder">
		<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">		
        <?php if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_faqcategoryadd',$SubadminAccessArray)){ ?>
                <?php echo $this->Js->link("+ Add New", array('controller'=>'contentmanagement', "action"=>"faqcategoryadd"), array(
                    'update'=>'#contentmanagementpage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'class'=>'btnorange'
                ));?>
        <?php } ?>
	</div>
	<div class="clear-both"></div>
		
		<div class="tablegrid">
				<div class="tablegridheader">
				    <div>
                        <?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('Id', array('controller'=>'contentmanagement', "action"=>"faqcategories/0/0/cat_id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#contentmanagementpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Id'
                        ));?>
                    </div>
                    <div>
                        <?php 
                        echo $this->Js->link('Category', array('controller'=>'contentmanagement', "action"=>"faqcategories/0/0/category/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#contentmanagementpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Category'
                        ));?>
                    </div>
                    <div>
                        <?php 
                        echo $this->Js->link('Display Order', array('controller'=>'contentmanagement', "action"=>"faqcategories/0/0/categoriorder/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#contentmanagementpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Display Order'
                        ));?>
                    </div>
                    <div><?php echo 'Action';?></div>
                </div>
                <?php foreach ($faqcategorydata as $faqcategories): ?>
                    <div class="tablegridrow">
					    <div><?php echo $faqcategories['Faq_category']['cat_id']; ?></div>
                        <div><?php echo $faqcategories['Faq_category']['category']; ?></div>
                        <div>
                            <?php
                            if($faqcategories['Faq_category']['categoriorder']==$mino && $maxo>1)
                            {
								echo $this->Js->link($this->html->image('down.png', array('alt'=>'')), array('controller'=>'contentmanagement', "action"=>"faqcategoriesaction/down/".$faqcategories['Faq_category']['categoriorder']."/".$faqcategories['Faq_category']['cat_id']."/".$currentpagenumber), array(
                                    'update'=>'#contentmanagementpage',
                                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                    'escape'=>false,
                                    'class'=>'vtip',
                                    'title'=>'Move Down'
                                ));
                            }
                            elseif($faqcategories['Faq_category']['categoriorder']==$maxo && $maxo>1)
                            {
                                echo $this->Js->link($this->html->image('up.png', array('alt'=>'')), array('controller'=>'contentmanagement', "action"=>"faqcategoriesaction/up/".$faqcategories['Faq_category']['categoriorder']."/".$faqcategories['Faq_category']['cat_id']."/".$currentpagenumber), array(
                                    'update'=>'#contentmanagementpage',
                                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                    'escape'=>false,
                                    'class'=>'vtip',
                                    'title'=>'Move Up'
                                ));
                            }
                            elseif($faqcategories['Faq_category']['categoriorder']<$maxo && $faqcategories['Faq_category']['categoriorder']>$mino)
                            {
                                echo $this->Js->link($this->html->image('down.png', array('alt'=>'')), array('controller'=>'contentmanagement', "action"=>"faqcategoriesaction/down/".$faqcategories['Faq_category']['categoriorder']."/".$faqcategories['Faq_category']['cat_id']."/".$currentpagenumber), array(
                                    'update'=>'#contentmanagementpage',
                                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                    'escape'=>false,
                                    'class'=>'vtip',
                                    'title'=>'Move Down'
                                ));
								echo "&nbsp;";
                                echo $this->Js->link($this->html->image('up.png', array('alt'=>'')), array('controller'=>'contentmanagement', "action"=>"faqcategoriesaction/up/".$faqcategories['Faq_category']['categoriorder']."/".$faqcategories['Faq_category']['cat_id']."/".$currentpagenumber), array(
                                    'update'=>'#contentmanagementpage',
                                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                    'escape'=>false,
                                    'class'=>'vtip',
                                    'title'=>'Move Up'
                                ));
                            }
                            ?>
                        </div>
                        <div>
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
												<?php 
												if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_faqcategoryadd/$',$SubadminAccessArray)){?>
												<li>
												<?php
													echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit FAQ Category'))." Edit", array('controller'=>'contentmanagement', "action"=>"faqcategoryadd/".$faqcategories['Faq_category']['cat_id']), array(
														'update'=>'#contentmanagementpage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>''
													));?>
												</li>
												<?php } ?>
												<?php if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_faqcategoryremove',$SubadminAccessArray)){?>
												<li>
												<?php
													echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete FAQ Category'))." Delete", array('controller'=>'contentmanagement', "action"=>"faqcategoryremove/".$faqcategories['Faq_category']['cat_id']."/".$currentpagenumber), array(
														'update'=>'#contentmanagementpage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'confirm'=>'Are You Sure? All The FAQs With This Category Will Also Be Deleted.',
														'class'=>''
													));?>
												</li>
												<?php }?>
									</ul>
								  </div>
								</div>
                        </div>
                    </div>
                <?php endforeach; ?>
		</div>
		<?php if(count($faqcategorydata)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php
        if($this->params['paging']['Faq_category']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Faq_category',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'contentmanagement','action'=>'faqcategories/0/rpp')));?>
	    
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#contentmanagementpage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'contentmanagement',
                  'action'=>'faqcategories/0/rpp',
                  'url'   => array('controller' => 'contentmanagement', 'action' => 'faqcategories/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
		</div>
    </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#contentmanagementpage over-->
</div>
<?php }?>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>