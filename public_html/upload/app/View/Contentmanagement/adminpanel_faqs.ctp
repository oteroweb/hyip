<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / FAQs</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("FAQs", array('controller'=>'contentmanagement', "action"=>"faqs"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("FAQ Categories", array('controller'=>'contentmanagement', "action"=>"faqcategories"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="contentmanagementpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/FAQs#FAQs" target="_blank">Help</a></div>
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Faq',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'contentmanagement','action'=>'faqs')));?>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>Search By :</span>
			<span>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
							<?php 
							echo $this->Form->input('searchby', array(
								  'type' => 'select',
								  'options' => $fagcatdata,
								  'selected' => $searchby,
								  'class'=>'',
								  'label' => false,
								  'style' => ''
							));
							?>
						</label>
					</div>
				</div>
			</span>
		</div>
		 <div class="fromboxmain">
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#contentmanagementpage',
				'class'=>'searchbtn',
				'controller'=>'contentmanagement',
				'action'=>'faqs',
				'url'=> array('controller' => 'contentmanagement', 'action' => 'faqs')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->

	    <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#contentmanagementpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'contentmanagement', 'action'=>'faqs')
        ));
        $currentpagenumber=$this->params['paging']['Faq']['page'];
        ?>
		
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">			
        <?php if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_faqadd',$SubadminAccessArray)){ ?>
                <?php echo $this->Js->link("+ Add New", array('controller'=>'contentmanagement', "action"=>"faqadd"), array(
                    'update'=>'#contentmanagementpage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'class'=>'btnorange'
                ));?>
        <?php } ?>
	</div>
	<div class="clear-both"></div>
		<div class="tablegrid">
            <div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'contentmanagement', "action"=>"faqs/0/0/faq_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#contentmanagementpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Question', array('controller'=>'contentmanagement', "action"=>"faqs/0/0/question/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#contentmanagementpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Question'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Category', array('controller'=>'contentmanagement', "action"=>"faqs/0/0/category/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#contentmanagementpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Category'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Display Order', array('controller'=>'contentmanagement', "action"=>"faqs/0/0/fagorder/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#contentmanagementpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Display Order'
					));?>
				</div>
				<div><?php echo 'Action';?></div>
			</div>
                <?php $i=0; foreach($faqdata as $faqs): $i++;
                    ?>
					<div class="tablegridrow">
                        <div class="faqcaret"><a href="javascript:void(0)" onclick="FadeInOut2('<?php echo ".class".$i;?>','.faqclose<?php echo $i;?>')"><span class="caret faqclose<?php echo $i;?> closed"></span> <?php echo $faqs['Faq']['faq_id']; ?></a></div>
                        <div><a href="javascript:void(0)" onclick="FadeInOut2('<?php echo ".class".$i;?>','.faqclose<?php echo $i;?>')"><?php echo stripslashes($faqs['Faq']['questioneng']); ?></a></div>
                        <div><?php echo nl2br(stripslashes($faqs['Faq_category']['category'])); ?></div>
                        <div>
                            <?php
							if($faqs['Faq']['fagorder']==$mino && $maxo>1)
                            {
								echo $this->Js->link($this->html->image('down.png', array('alt'=>'')), array('controller'=>'contentmanagement', "action"=>"faqstatus/down/".$faqs['Faq']['fagorder']."/".$faqs['Faq']['faq_id']."/".$searchby."/".$currentpagenumber), array(
                                    'update'=>'#contentmanagementpage',
                                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                    'escape'=>false,
                                    'class'=>'vtip',
                                    'title'=>'Move Down'
                                ));
                            }
                            elseif($faqs['Faq']['fagorder']==$maxo && $maxo>1)
                            {
                                echo $this->Js->link($this->html->image('up.png', array('alt'=>'')), array('controller'=>'contentmanagement', "action"=>"faqstatus/up/".$faqs['Faq']['fagorder']."/".$faqs['Faq']['faq_id']."/".$searchby."/".$currentpagenumber), array(
                                    'update'=>'#contentmanagementpage',
                                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                    'escape'=>false,
                                    'class'=>'vtip',
                                    'title'=>'Move Up'
                                ));
                            }
                            elseif($faqs['Faq']['fagorder']<$maxo && $faqs['Faq']['fagorder']>$mino)
                            {
                                echo $this->Js->link($this->html->image('down.png', array('alt'=>'')), array('controller'=>'contentmanagement', "action"=>"faqstatus/down/".$faqs['Faq']['fagorder']."/".$faqs['Faq']['faq_id']."/".$searchby."/".$currentpagenumber), array(
                                    'update'=>'#contentmanagementpage',
                                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                    'escape'=>false,
                                    'class'=>'vtip',
                                    'title'=>'Move Down'
                                ));
								echo "&nbsp;";
                                echo $this->Js->link($this->html->image('up.png', array('alt'=>'')), array('controller'=>'contentmanagement', "action"=>"faqstatus/up/".$faqs['Faq']['fagorder']."/".$faqs['Faq']['faq_id']."/".$searchby."/".$currentpagenumber), array(
                                    'update'=>'#contentmanagementpage',
                                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                    'escape'=>false,
                                    'class'=>'vtip',
                                    'title'=>'Move Up'
                                ));
                            }
                            ?>
                        </div>
						
						<div class="textcenter">
							<div class="actionmenu">
							  <div class="btn-group">
								<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
									Action <span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
									
									<?php if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_faqadd/$',$SubadminAccessArray)){ ?>
										<li>
											<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit FAQ')).' Edit FAQ', array('controller'=>'contentmanagement', "action"=>"faqadd/simple/".$faqs['Faq']['faq_id']), array(
											'update'=>'#contentmanagementpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'class'=>'vtip',
											'title'=>'Edit FAQ'
											)); ?>
										</li>
									<?php } ?>
									
									<?php
									if($faqs['Faq']['status']==0)
                                    {
                                        $field_reqaction='1';
                                        $field_reqicon='red-icon.png';
                                        $field_reqtext='Activate FAQ';
                                    }
                                    else
                                    {
                                        $field_reqaction='0';
                                        $field_reqicon='blue-icon.png';
                                        $field_reqtext='Inactivate FAQ';
                                    }
									
									if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_faqstatus',$SubadminAccessArray)){ ?>
										<li>
											<?php echo $this->Js->link($this->html->image($field_reqicon, array('alt'=>$field_reqtext)).' '.$field_reqtext, array('controller'=>'contentmanagement', "action"=>"faqstatus/status/".$field_reqaction."/".$faqs['Faq']['faq_id']."/".$currentpagenumber), array(
												  'update'=>'#contentmanagementpage',
												  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												  'escape'=>false,
												  'class'=>'vtip',
												  'title'=>$field_reqtext
											  ));?>
										</li>
									<?php } ?>
									
									<?php if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_faqremove',$SubadminAccessArray)){ ?>
										<li>
											<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete FAQ')).' Delete FAQ', array('controller'=>'contentmanagement', "action"=>"faqremove/".$faqs['Faq']['faq_id']."/".$currentpagenumber), array(
											'update'=>'#contentmanagementpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'confirm'=>'Are You Sure?',
											'class'=>'vtip',
											'title'=>'Delete FAQ'
											)); ?>
										</li>
									<?php } ?>
								</ul>
							  </div>
						  </div>
						</div>
						
						
                    </div>
					<div class="tablegridrow class<?php echo $i;?>" style="display: none;">
						<div></div>
						<div><?php echo nl2br(stripslashes($faqs['Faq']['answereng']));?></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
				<?php endforeach; ?>
				
        </div>
		<?php if(count($faqdata)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php
        if($this->params['paging']['Faq']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Faq',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'contentmanagement','action'=>'faqs/0/rpp')));?>
			<div class="resultperpage">
				<label>
					<?php 
					echo $this->Form->input('resultperpage', array(
					  'type' => 'select',
					  'options' => $resultperpage,
					  'selected' => $this->Session->read('pagerecord'),
					  'class'=>'',
					  'label' => false,
					  'div'=>false,
					  'style' => '',
					  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
					));
					?>
				</label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#contentmanagementpage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'contentmanagement',
                  'action'=>'faqs/0/rpp',
                  'url'   => array('controller' => 'contentmanagement', 'action' => 'faqs/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
    </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#contentmanagementpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>