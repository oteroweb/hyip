<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php 
if($textarea=='advanced')
{
	echo $this->Javascript->link('tiny_mce/tiny_mce.js');
	echo $this->Javascript->link('tinymce');
	$textareaclass='advancededitor';
	$submitbuttonclass='tinyMCEtriggerSavetk';
}
elseif($textarea=='simple')
{
	$textareaclass='';
	$submitbuttonclass='';
}
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Testimonials</div>
<div id="contentmanagementpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<script type="text/javascript"> 
(function() {
$('#Testimonial').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();
</script>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Testimonials" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>
	
<div class="backgroundwhite">
		
	<?php echo $this->Form->create('Testimonial',array('id' => 'Testimonial', 'type' => 'post', 'type' => 'file', 'onsubmit' => 'return true;','url'=>array('controller'=>'contentmanagement','action'=>'testimonialaddaction')));?>
	<?php if(isset($testimonialdata['Testimonial']["test_id"])){
		echo $this->Form->input('test_id', array('type'=>'hidden', 'value'=>$testimonialdata['Testimonial']["test_id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
	
	<div class="frommain">
		
		<div class="formbutton">
			<?php 
			if($textarea=='simple'){$showtextarea='advanced';}
			if($textarea=='advanced'){$showtextarea='simple';}
			if(isset($testimonialdata['Testimonial']["test_id"])){
				$link='testimonialadd/'.$showtextarea.'/'.$testimonialdata['Testimonial']["test_id"];
			}
			else
			{
				$link='testimonialadd/'.$showtextarea;
			}
			echo $this->Js->link("Switch To ".ucfirst($showtextarea)." Mode", array('controller'=>'contentmanagement', "action"=>$link), array(
				'update'=>'#contentmanagementpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'btngray'
			));?>
		</div>
		
		<div class="frombordermain">
			<?php echo $this->Form->input('content', array('type'=>'textarea', 'value'=>stripslashes($testimonialdata['Testimonial']['content']),'label' => false, 'div' => false, 'class'=>'from-textarea '.$textareaclass, 'style'=>'width:98%;height:400px;margin-left:0px;'));?>
		</div>
		
		<div class="fromnewtext">Display Name :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('dname', array('type'=>'text', 'value'=>$testimonialdata['Testimonial']["dname"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Image : </div>
		<div class="fromborderdropedown3">
		  <div class="btnorange browsebutton">Browse<?php echo $this->Form->input('photo', array('type' => 'file','label' => false,'div' => false,'class'=>'browserbuttonlink')); ?></div>
		</div>
		
		
		<?php if(isset($testimonialdata['Testimonial']["test_id"])){ 
			if($testimonialdata['Testimonial']['photo']=='' || $testimonialdata['Testimonial']['photo']==NULL) $testimonialdata['Testimonial']['photo']='dummy.jpg';?>
			<div class="fromnewtext">Current Image : </div>
			<div class="fromborderdropedown4">
				<?php echo $this->html->image("testimonials/".$testimonialdata['Testimonial']['photo'], array('alt'=>'Image','width'=>'37px','height'=>'37px')); ?>
			</div>
		<?php }?>
		
		<div class="formbutton">
			  <?php echo $this->Form->submit('Submit', array(
				'class'=>'btnorange '.$submitbuttonclass,
				'div'=>false
			  ));
			  ?>
			  <?php 
			  echo $this->Js->link("Back", array('controller'=>'contentmanagement', "action"=>"testimonials"), array(
				  'update'=>'#contentmanagementpage',
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'escape'=>false,
				  'class'=>'btngray'
			  ));?>
		  </div>
	</div>
	<?php echo $this->Form->end();?>
	
</div>
<?php if(!$ajax){?>
</div><!--#contentmanagementpage over-->
</div>
<?php }?>	
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>