<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php 
if($textarea=='advanced')
{
	echo $this->Javascript->link('tiny_mce/tiny_mce.js');
	echo $this->Javascript->link('tinymce');
	$textareaclass='advancededitor';
	$submitbuttonclass='tinyMCEtriggerSavetk';
}
elseif($textarea=='simple')
{
	$textareaclass='';
	$submitbuttonclass='';
}
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Faqs</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("FAQs", array('controller'=>'contentmanagement', "action"=>"faqs"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("FAQ Categories", array('controller'=>'contentmanagement', "action"=>"faqcategories"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="contentmanagementpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/FAQs#FAQs" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>
	
<div class="backgroundwhite">
		
	<?php echo $this->Form->create('Faq',array('type' => 'post', 'id'=>'FaqForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'contentmanagement','action'=>'faqaddaction')));?>
	<?php if(isset($faqdata['Faq']["faq_id"])){
		echo $this->Form->input('faq_id', array('type'=>'hidden', 'value'=>$faqdata['Faq']["faq_id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}else{
		echo $this->Form->input('fagorder', array('type'=>'hidden', 'value'=>$this->Session->read('max_order')+1, 'label' => false));
	}?>
	
	<div class="frommain">
		
		<div class="fromnewtext">Category :</div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
				<?php 
					echo $this->Form->input('category', array(
						'type' => 'select',
						'options' => $categories,
						'selected' => $faqdata['Faq']["category"],
						'class'=>'',
						'div' => false,
						'label' => false,
						'style' => ''
					));
				?>
				</label>
			</div>
		</div>
		<?php foreach($languagedata as $code=>$displayname){ ?>	   
		<div class="fromnewtext">Question (<?php echo $displayname;?>) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('question'.$code, array('type'=>'text', 'value'=>stripslashes($faqdata['Faq']["question".$code]), 'label' => false, 'div' => false, 'class'=>'fromboxbg faqallquestion'));?>
		</div>
		<?php } ?>
		<div class="formbutton">
			<?php 
			if($textarea=='simple'){$showtextarea='advanced';}
			if($textarea=='advanced'){$showtextarea='simple';}
			if(isset($faqdata['Faq']["faq_id"])){
				$link='faqadd/'.$showtextarea.'/'.$faqdata['Faq']["faq_id"];
			}
			else
			{
				$link='faqadd/'.$showtextarea;
			}
			echo $this->Js->link("Switch To ".ucfirst($showtextarea)." Mode", array('controller'=>'contentmanagement', "action"=>$link), array(
				'update'=>'#contentmanagementpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'btngray'
			));?>
			<?php if($textarea=='simple'){ ?>
				<input type="button" value="Copy Same Content To All Language" onclick="if(confirm('Copy English Content Into All Languages')){$('.alllanguagesame').val($('#FaqAnswereng').val()); $('.faqallquestion').val($('#FaqQuestioneng').val()); }" class="btnorange floatright" style="margin-right: 5px;" />
				<?php }else{ ?>
				<input type="button" value="Copy Same Content To All Language" onclick="if(confirm('Copy English Content Into All Languages')){for (i=0; i < tinyMCE.editors.length; i++){ tinyMCE.editors[i].setContent(tinyMCE.editors[0].getContent()); } $('.faqallquestion').val($('#FaqQuestioneng').val());}" class="btnorange floatright" style="margin-right: 5px;" />
			<?php } ?>
		</div>
		
		<div class="clearboth"></div>
		
		<div class="newgreytabs">

			<ul class="nav nav-tabs" role="tablist">
			     <?php foreach($languagedata as $code=>$displayname){ $activeclass='';if($code=='eng'){ $activeclass='active'; } ?>
				<li role="presentation" class="<?php echo $activeclass; ?>">
					<a onclick="$('.allactive').hide();$('.<?php echo $code; ?>').show();" class="active" role="tab" data-toggle="tab"><?php echo $displayname ?></a>
				</li>
				<?php } ?>
			</ul>
	      
		      <div class="tab-content">
			  <?php foreach($languagedata as $code=>$displayname){ $activeclass='';if($code!='eng'){ $activeclass='style="display: none" '; } ?>
			  <div <?php echo $activeclass; ?> role="tabpanel" class="tab-pane active allactive <?php echo $code; ?>">
			      <div class="masstextnew">Message (<?php echo $displayname;?>) :<span class="red-color">*</span></div>
			      <div class="greyboxnewtabsin"><?php echo $this->Form->input('answer'.$code, array('type'=>'textarea', 'label' => false, 'div' => false, 'class'=>'from-textarea alllanguagesame '.$textareaclass,'value'=>stripslashes($faqdata['Faq']['answer'.$code]), 'style'=>'height: 400px;'));?></div>
			  </div>
			<?php }?>
		      </div>
	      </div>
		
		<div class="formbutton">
				<?php echo $this->Js->submit('Update', array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'class'=>'btnorange '.$submitbuttonclass,
				  'div'=>false,
				  'controller'=>'contentmanagement',
				  'action'=>'faqaddaction',
				  'url'   => array('controller' => 'contentmanagement', 'action' => 'faqaddaction')
				));?>
				<?php 
				echo $this->Js->link("Back", array('controller'=>'contentmanagement', "action"=>"faqs"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btngray'
				));?>
		</div>
	</div>
	<?php echo $this->Form->end();?>
	
	
</div>
<?php if(!$ajax){?>
</div><!--#contentmanagementpage over-->
</div>
<?php }?>
	
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>