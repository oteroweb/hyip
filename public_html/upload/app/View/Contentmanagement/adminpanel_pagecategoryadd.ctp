<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 21-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Website Pages</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Website Pages", array('controller'=>'contentmanagement', "action"=>"index"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Website Page Categories", array('controller'=>'contentmanagement', "action"=>"pagecategory"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="contentmanagementpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Website_Pages#Website_Pages" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="backgroundwhite">
		
	<?php echo $this->Form->create('Web_page_category',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'contentmanagement','action'=>'pagecategoryaddaction')));?>
	
	<?php if(isset($web_page_categorydata['Web_page_category']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$web_page_categorydata['Web_page_category']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
	
	<div class="frommain">
		
		<div class="fromnewtext">Category :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('category', array('type'=>'text', 'value'=>stripslashes($web_page_categorydata['Web_page_category']["category"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'class'=>'btnorange',
			  'controller'=>'contentmanagement',
			  'div'=>false,
			  'action'=>'pagecategoryaddaction',
			  'url'   => array('controller' => 'contentmanagement', 'action' => 'pagecategoryaddaction')
			));?>
			<?php echo $this->Js->link("Back", array('controller'=>'contentmanagement', "action"=>"pagecategory"), array(
				'update'=>'#contentmanagementpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'btngray'
			));?>
		</div>
		
	</div>
	<?php echo $this->Form->end();?>

	
</div>
<?php if(!$ajax){?>
</div><!--#contentmanagementpage over-->
</div>
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>