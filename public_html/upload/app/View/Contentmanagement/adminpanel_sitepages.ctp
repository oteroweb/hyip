<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){
$memberact='';$publicact='';
if($pagearea=='member')
	$memberact='satting-menu-active';
else
	$publicact='satting-menu-active'; ?>
<div class="whitetitlebox">Design & CMS / Virtual Pages</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Public Pages", array('controller'=>'contentmanagement', "action"=>"sitepages/public"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>$publicact
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Member Pages", array('controller'=>'contentmanagement', "action"=>"sitepages/member"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>$memberact
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="contentmanagementpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<?php if($pagearea=='public'){ ?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Virtual_Pages#Public_Pages" target="_blank">Help</a></div>
<?php } else { ?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Virtual_Pages#Member_Pages" target="_blank">Help</a></div>
<?php } ?>
    <div id="UpdateMessage"></div>
   
        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#contentmanagementpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'contentmanagement', 'action'=>'sitepages/'.$pagearea)
        ));
        $currentpagenumber=$this->params['paging']['Sitepage']['page'];
        ?>
		
<div id="Xgride-bg">
    <div class="Xpadding10">
	<div class="greenbottomborder">
		<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	
	<div class="addnew-button">			
        <?php if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_sitepageadd',$SubadminAccessArray)){ ?>
	            <?php echo $this->Js->link("+ Add New", array('controller'=>'contentmanagement', "action"=>"sitepageadd/".$pagearea), array(
                    'update'=>'#contentmanagementpage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'class'=>'btnorange'
                ));?>
        <?php } ?>
	</div>
	<div class="clear-both"></div>
		
		<div class="tablegrid">
				<div class="tablegridheader">
				    <div>
                        <?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('Title', array('controller'=>'contentmanagement', "action"=>"sitepages/".$pagearea."/0/0/title/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#contentmanagementpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Title'
                        ));?>
                    </div>
                    <div>
                        <?php 
                        echo $this->Js->link('Page Name', array('controller'=>'contentmanagement', "action"=>"sitepages/".$pagearea."/0/0/pagename/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#contentmanagementpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Page Name'
                        ));?>
                    </div>
                    <div><?php echo 'Page URL';?></div>
                    <div>
                        <?php 
                        echo $this->Js->link('Display Order', array('controller'=>'contentmanagement', "action"=>"sitepages/".$pagearea."/0/0/pageorder/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#contentmanagementpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Display Order'
                        ));?>
                    </div>
                    <div><?php echo 'Action';?></div>
                </div>
                <?php foreach ($sitepages as $sitepage): ?>
                    <div class="tablegridrow">
					    <div><?php echo stripslashes($sitepage['Sitepage']['title']); ?></div>
                        <div><?php echo stripslashes($sitepage['Sitepage']['pagename']); ?></div>
                        <div><?php echo $SITEURL.$pagearea.'/page/'.$sitepage['Sitepage']['page_id']; ?></div>
                        <div>
                            <?php
                            if($sitepage['Sitepage']['pageorder']==$mino && $maxo>1)
                            {
                                echo $this->Js->link($this->html->image('down.png', array('alt'=>'')), array('controller'=>'contentmanagement', "action"=>"sitepageaction/".$pagearea."/down/".$sitepage['Sitepage']['pageorder']."/".$sitepage['Sitepage']['page_id']."/".$currentpagenumber), array(
                                    'update'=>'#contentmanagementpage',
                                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                    'escape'=>false,
                                    'class'=>'vtip',
                                    'title'=>'Move Down'
                                ));
                            }
                            elseif($sitepage['Sitepage']['pageorder']==$maxo && $maxo>1)
                            {
                                echo $this->Js->link($this->html->image('up.png', array('alt'=>'')), array('controller'=>'contentmanagement', "action"=>"sitepageaction/".$pagearea."/up/".$sitepage['Sitepage']['pageorder']."/".$sitepage['Sitepage']['page_id']."/".$currentpagenumber), array(
                                    'update'=>'#contentmanagementpage',
                                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                    'escape'=>false,
                                    'class'=>'vtip',
                                    'title'=>'Move Up'
                                ));
                            }
                            elseif($sitepage['Sitepage']['pageorder']<$maxo && $sitepage['Sitepage']['pageorder']>$mino)
                            {
                                echo $this->Js->link($this->html->image('down.png', array('alt'=>'')), array('controller'=>'contentmanagement', "action"=>"sitepageaction/".$pagearea."/down/".$sitepage['Sitepage']['pageorder']."/".$sitepage['Sitepage']['page_id']."/".$currentpagenumber), array(
                                    'update'=>'#contentmanagementpage',
                                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                    'escape'=>false,
                                    'class'=>'vtip',
                                    'title'=>'Move Down'
                                ));
                                echo $this->Js->link($this->html->image('up.png', array('alt'=>'')), array('controller'=>'contentmanagement', "action"=>"sitepageaction/".$pagearea."/up/".$sitepage['Sitepage']['pageorder']."/".$sitepage['Sitepage']['page_id']."/".$currentpagenumber), array(
                                    'update'=>'#contentmanagementpage',
                                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                    'escape'=>false,
                                    'class'=>'vtip',
                                    'title'=>'Move Up'
                                ));
                            }
                            ?>
                        </div>
                        <div>
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
										
												<?php if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_sitepageadd/$',$SubadminAccessArray)){?>
												<li>
												<?php
													echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Page'))." Edit", array('controller'=>'contentmanagement', "action"=>"sitepageadd/".$pagearea."/simple/".$sitepage['Sitepage']['page_id']), array(
														'update'=>'#contentmanagementpage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>''
													));?>
												</li>
												<?php }?>
										
												<?php if($sitepage['Sitepage']['status']==0)
												{
													$field_reqaction='1';
													$field_reqicon='red-icon.png';
													$field_reqtext='Activate';
												}
												else
												{
													$field_reqaction='0';
													$field_reqicon='blue-icon.png';
													$field_reqtext='Inactivate';
												}
												if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_sitepageaction',$SubadminAccessArray)){?>
												<li>
												<?php
													echo $this->Js->link($this->html->image($field_reqicon, array('alt'=>$field_reqtext))." ".$field_reqtext, array('controller'=>'contentmanagement', "action"=>"sitepageaction/".$pagearea."/status/".$field_reqaction."/".$sitepage['Sitepage']['page_id']."/".$currentpagenumber), array(
														'update'=>'#contentmanagementpage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>''
													));?>
												</li>
												<?php }?>
												
												<?php  if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_sitepageremove',$SubadminAccessArray)){?>
												<li>
												<?php
													echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Page'))." Delete", array('controller'=>'contentmanagement', "action"=>"sitepageremove/".$pagearea."/".$sitepage['Sitepage']['page_id']."/".$currentpagenumber), array(
														'update'=>'#contentmanagementpage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'confirm'=>'Are You Sure?',
														'class'=>''
													));?>
												</li>
												<?php }?>
									</ul>
								  </div>
								</div>
								
                        </div>
                    </div>
                <?php endforeach; ?>
		</div>
		<?php if(count($sitepages)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php
        if($this->params['paging']['Sitepage']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Sitepage',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'contentmanagement','action'=>'sitepages/'.$pagearea.'/rpp')));?>
		
		<div class="resultperpage">
			<label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>	
		</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#contentmanagementpage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'contentmanagement',
                  'action'=>'sitepages/'.$pagearea.'/rpp',
                  'url'   => array('controller' => 'contentmanagement', 'action' => 'sitepages/'.$pagearea.'/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
		</div>
    </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#contentmanagementpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>