<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 21-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / FAQs</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("FAQs", array('controller'=>'contentmanagement', "action"=>"faqs"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("FAQ Categories", array('controller'=>'contentmanagement', "action"=>"faqcategories"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="contentmanagementpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/FAQs#FAQ_Categories" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>
	
<div class="backgroundwhite">
		
	<?php echo $this->Form->create('Faq_category',array('type' => 'post', 'id'=>'Faq_categoryForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'contentmanagement','action'=>'faqcategoryaddaction')));?>
	<?php if(isset($faqcategorydata['Faq_category']["cat_id"])){
		echo $this->Form->input('cat_id', array('type'=>'hidden', 'value'=>$faqcategorydata['Faq_category']["cat_id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}else{
		echo $this->Form->input('categoriorder', array('type'=>'hidden', 'value'=>$this->Session->read('max_order')+1, 'label' => false));
	}?>
	  
	<div class="frommain">
		
		<div class="fromnewtext">Category :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('category', array('type'=>'text', 'value'=>stripslashes($faqcategorydata['Faq_category']["category"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Update', array(
			'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'update'=>'#UpdateMessage',
			'class'=>'btnorange',
			'div'=>false,
			'controller'=>'contentmanagement',
			'action'=>'faqcategoryaddaction',
			'url'   => array('controller' => 'contentmanagement', 'action' => 'faqcategoryaddaction')
		  ));?>
		  <?php 
		  echo $this->Js->link("Back", array('controller'=>'contentmanagement', "action"=>"faqcategories"), array(
			  'update'=>'#contentmanagementpage',
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'escape'=>false,
			  'class'=>'btngray'
		  ));?>
		</div>
		
	</div>
	<?php echo $this->Form->end();?>

</div>
<?php if(!$ajax){?>
</div><!--#contentmanagementpage over-->
</div>
<?php }?>
	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>