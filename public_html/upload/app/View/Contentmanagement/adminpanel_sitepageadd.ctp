<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php 
if($textarea=='advanced')
{
	echo $this->Javascript->link('tiny_mce/tiny_mce.js');
	echo $this->Javascript->link('tinymce');
	$textareaclass='advancededitor';
	$submitbuttonclass='tinyMCEtriggerSavetk';
}
elseif($textarea=='simple')
{
	$textareaclass='';
	$submitbuttonclass='';
}
$memberact='';$publicact='';
if($pagearea=='member')
	$memberact='satting-menu-active';
else
	$publicact='satting-menu-active';
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Virtual Pages</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Public Pages", array('controller'=>'contentmanagement', "action"=>"sitepages/public"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>$publicact
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Member Pages", array('controller'=>'contentmanagement', "action"=>"sitepages/member"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>$memberact
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="contentmanagementpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($pagearea=='public'){ ?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Virtual_Pages#Public_Pages" target="_blank">Help</a></div>
<?php } else { ?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Virtual_Pages#Member_Pages" target="_blank">Help</a></div>
<?php } ?>
	<div id="UpdateMessage"></div>

<div class="backgroundwhite">
		
	<?php echo $this->Form->create('Sitepage',array('type' => 'post', 'id'=>'SitepageForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'contentmanagement','action'=>'sitepageaddaction')));?>
	<?php if(isset($sitepages['Sitepage']["page_id"])){
		echo $this->Form->input('page_id', array('type'=>'hidden', 'value'=>$sitepages['Sitepage']["page_id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}else{
		echo $this->Form->input('pageorder', array('type'=>'hidden', 'value'=>$this->Session->read('max_order')+1, 'label' => false));
	}?>
	
	<div class="frommain">
		
		<div class="fromnewtext">Title :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('title', array('type'=>'text', 'value'=>stripslashes($sitepages['Sitepage']["title"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Name of Page in Menu :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('pagename', array('type'=>'text', 'value'=>stripslashes($sitepages['Sitepage']["pagename"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<?php if(isset($sitepages['Sitepage']["page_id"])){ ?>
		<div class="fromnewtext">Page URL : </div>
		<div class="fromborderdropedown4"><?php echo $SITEURL.$pagearea.'/page/'.$sitepages['Sitepage']['page_id'];?></div>
		<?php } ?>
		
		<div class="fromnewtext">Keywords (For SEO) : </div>
		<div class="frombordermain">
			<?php echo $this->Form->input('key_seo', array('type'=>'textarea', 'value'=>stripslashes($sitepages['Sitepage']["key_seo"]), 'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
		</div>
		
		<div class="fromnewtext">Description (For SEO) : </div>
		<div class="frombordermain">
			<?php echo $this->Form->input('desc_seo', array('type'=>'textarea', 'value'=>stripslashes($sitepages['Sitepage']["desc_seo"]), 'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
		</div>
		
        <div class="fromnewtext">Display Page in :</div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
				 <?php 
					 echo $this->Form->input('pagearea', array(
						 'type' => 'select',
						 'options' => array('public'=>'Public Area', 'member'=>'Member Area'),
						 'selected' => $pagearea,
						 'class'=>'',
						 'div' => false,
						 'label' => false,
						 'style' => '',
						 'onchange'=>'if(this.value=="member"){$(".showmembership").show(500);$(".memberstatus").show(500);}else{$(".showmembership").hide(500);$(".memberstatus").hide(500);}'
					 ));
				 ?>
				</label>
			</div>
        </div>
			   
							
		<?php if($pagearea=='member'){?>
		<div class="memberstatus">
			<div class="fromnewtext">Filter Audience :</div>
			<div class="fromborderdropedown3">
				<div class="select-main">
					<label>
						<?php
						$option=array(0=>'All Members', 1=>'Paid Members', 2=>'Unpaid Members', 3=>'Membership Wise');
						echo $this->Form->input('paid_unpaid', array(
							'type' => 'select',
							'options' => $option,
							'selected' => $sitepages['Sitepage']["paid_unpaid"],
							'class'=>'',
							'div' => false,
							'label' => false,
							'style' => '',
							'onchange'=>'if(this.value==3){$(".showmembership").show(500);}else{$(".showmembership").hide(500);}'
						));
						?>
					</label>
				</div>
			</div>
		</div>
		<div style="<?php if($sitepages['Sitepage']["paid_unpaid"]!=3){echo 'display:none';}?>" class="showmembership">
			<div class="fromnewtext">Memberships :</div>
			<div class="fromborderdropedown3 checkboxlist">
				<?php
				$selected = @explode(",",$sitepages['Sitepage']["membership"]);
				echo $this->Form->input('membership', array('type' => 'select', 'div'=>true, 'label'=>false, 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $membership));?>
			</div>
			
		</div>
		<?php } ?>
		<div class="formbutton">
			<?php 
			if($textarea=='simple'){$showtextarea='advanced';}
			if($textarea=='advanced'){$showtextarea='simple';}
			if(isset($sitepages['Sitepage']["page_id"])){
				$link='sitepageadd/'.$pagearea.'/'.$showtextarea.'/'.$sitepages['Sitepage']["page_id"];
			}
			else
			{
				$link='sitepageadd/'.$pagearea.'/'.$showtextarea;
			}
			echo $this->Js->link("Switch To ".ucfirst($showtextarea)." Mode", array('controller'=>'contentmanagement', "action"=>$link), array(
				'update'=>'#contentmanagementpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'btngray'
			));?>
			<?php if($textarea=='simple'){ ?>
			<input type="button" value="Copy Same Content To All Language" onclick="if(confirm('Copy English Content Into All Languages')){$('.alllanguagesame').val($('#SitepageContenteng').val()); }" class="btnorange floatright" style="margin-right: 5px;" />
			<?php }else{ ?>
			<input type="button" value="Copy Same Content To All Language" onclick="if(confirm('Copy English Content Into All Languages')){for (i=0; i < tinyMCE.editors.length; i++){ tinyMCE.editors[i].setContent(tinyMCE.editors[0].getContent()); }}" class="btnorange floatright" style="margin-right: 5px;" />
			<?php } ?>
		</div>
		
		<div class="clearboth"></div>
		
		<div class="newgreytabs">

			<ul class="nav nav-tabs" role="tablist">
			     <?php foreach($languagedata as $code=>$displayname){ $activeclass='';if($code=='eng'){ $activeclass='active'; } ?>
				<li role="presentation" class="<?php echo $activeclass; ?>">
					<a onclick="$('.allactive').hide();$('.<?php echo $code; ?>').show();" class="active" role="tab" data-toggle="tab"><?php echo $displayname ?></a>
				</li>
				<?php } ?>
			</ul>
	      
		      <div class="tab-content">
			  <?php foreach($languagedata as $code=>$displayname){ $activeclass='';if($code!='eng'){ $activeclass='style="display: none" '; } ?>
			  <div <?php echo $activeclass; ?> role="tabpanel" class="tab-pane active allactive <?php echo $code; ?>">
			      <div class="masstextnew">Message (<?php echo $displayname;?>) :<span class="red-color">*</span></div>
			      <div class="greyboxnewtabsin"><?php echo $this->Form->input('content'.$code, array('type'=>'textarea', 'label' => false, 'div' => false, 'class'=>'from-textarea alllanguagesame '.$textareaclass,'value'=>stripslashes($sitepages['Sitepage']['content'.$code]), 'style'=>'height: 400px;'));?></div>
			  </div>
			<?php }?>
		      </div>
	      </div>
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Update', array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'class'=>'btnorange '.$submitbuttonclass,
			  'div'=>false,
			  'controller'=>'contentmanagement',
			  'action'=>'sitepageaddaction',
			  'url'   => array('controller' => 'contentmanagement', 'action' => 'sitepageaddaction')
			));?>
			<?php 
			echo $this->Js->link("Back", array('controller'=>'contentmanagement', "action"=>"sitepages/".$pagearea), array(
				'update'=>'#contentmanagementpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'btngray'
			));?>
		</div>
	<?php echo $this->Form->end();?>
	</div>
	
	
	
</div>
<?php if(!$ajax){?>
</div><!--#contentmanagementpage over-->
</div>
<?php }?>	
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>