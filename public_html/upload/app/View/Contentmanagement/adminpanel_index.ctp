<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 17-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Design & CMS / Website Pages</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Website Pages", array('controller'=>'contentmanagement', "action"=>"index"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Website Page Categories", array('controller'=>'contentmanagement', "action"=>"pagecategory"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="contentmanagementpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Website_Pages#Website_Pages" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<div id="Xgride-bg">
	<div class="Xpadding10">
		<div class="greenbottomborder">
		<div class="height10"></div>
		<div class="addnew-button">
			<?php if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_websitepageedit', $SubadminAccessArray)){ ?>
				<?php echo $this->Js->link("+ Add New", array('controller'=>'contentmanagement', "action"=>"websitepageedit"), array(
					'update'=>'#contentmanagementpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btnorange'
				));?>
			<?php } ?>
		</div>
		<div class="clear-both"></div>
		
		<div class="emailcentermain">
				<?php foreach($category as $categoryid=>$categoryname){ ?>
				<div class="emailmain">
					<div class="emailtitle"><?php echo $categoryname; ?></div>
					<div class="tablegrid">
						<div class="tablegridheader">
							<div style="width: 100px;"><?php echo 'Page';?></div>
							<div><?php echo 'Content';?></div>
							<div><?php echo 'Action';?></div>
						</div>
						<?php foreach ($web_pages as $web_page){?>
						<?php if($categoryid==$web_page['Web_page']['category'])
						{ ?>
							<div class="tablegridrow">
							<div><?php echo $web_page['Web_page']['page']; ?></div>
							<div><?php echo (strlen($web_page['Web_page']['content'])>300) ? substr(strip_tags(stripslashes($web_page['Web_page']['content'])), 0, 300)."  . . ." : stripslashes(strip_tags($web_page['Web_page']['content']));?></div>
							 <div>
								  <div class="actionmenu">
									  <div class="btn-group">
											  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
												  Action <span class="caret"></span>
											  </button>
											  <ul class="dropdown-menu" role="menu">
													  <?php if(!isset($SubadminAccessArray) || in_array('contentmanagement',$SubadminAccessArray) || in_array('contentmanagement/adminpanel_websitepageedit/$',$SubadminAccessArray)){
														  echo '<li>';
														  echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Page Content'))." Edit Page Content", array('controller'=>'contentmanagement', "action"=>"websitepageedit/simple/".$web_page['Web_page']['page_id']), array(
															  'update'=>'#contentmanagementpage',
															  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
															  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
															  'escape'=>false,
														  ));
														  echo '</li>';
													  } ?>
													  <?php if(!isset($SubadminAccessArray) || in_array('design',$SubadminAccessArray) || in_array('design/adminpanel_websitepageremove',$SubadminAccessArray)){ ?>
													  <li>
													  <?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Email Template'))." Delete", array('controller'=>'contentmanagement', "action"=>"websitepageremove/".$web_page['Web_page']['page_id']), array(
														  'update'=>'#contentmanagementpage',
														  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														  'escape'=>false,
														  'class'=>'',
														  'confirm'=>"Are You Sure?"
													  ));?>
													  </li>
													  <?php }?>
											  </ul>
									  </div>
								  </div>
							  </div>
						  </div>
						<?php } }?>
					</div>
				</div>
				<?php }?>
		</div>
		
		<div class="clear-both"></div>
		<div class="height10"></div>
		</div>	
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#contentmanagementpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>