<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Help / Check for Updates</div>
<div id="helppage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
    <div id="UpdateMessage"></div>
<div class="helpicon"><a href="https://www.proxscripts.com/docs" target="_blank">Help</a></div>
<div id="Xgride-bg" class="marginnone backgroundwhite">
    <div class="Xpadding10">
				<div class="height10"></div>
				<div class="version cversion">Your Version : <?php echo CURRENTVERSION;?></div>
				<div class="version lversion">Latest Version : <?php echo LATESTVERSION;?></div>
				<div class="version floatright">
					<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'help','action'=>'updates')));?>
					<?php echo $this->Js->submit('Apply Updates', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#UpdateMessage',
						'class'=>'btnorange',
						'controller'=>'help',
						'action'=>'updates',
						'div'=>false,
						'url'   => array('controller' => 'help', 'action' => 'updates')
					));?>
					<?php echo $this->Form->end();?>
				</div>
				<div style="clear: both;"></div>
				<div style="height: 10px;"></div>
				<?php echo $feeds;?>
				<?php /*<table class="versionmain" cellpadding="0" cellspacing="0" width="100%" border="0" align="center">
					<tbody>
						<?php
						$i = 0;
						foreach ($feeds as $feed):
							$class = 'class="blue-color"';if ($i++ % 2 == 0){$class = 'class="white-color"';}
							?>
							<tr><td colspan="8" class="padding-tabal"></td></tr> 
							<tr class="white-color">
								<td align="left" valign="top">
										<div class="versiontitle"><a href="<?php echo $feed['link'];?>" target="_blank"><?php echo $feed['title'];?></a></div>
										<div class="versiontext"><?php echo trim($feed['description']);?></div>
										<p><small><?php echo $this->Time->format($SITECONFIG["timeformate"], $feed['pubDate']);?></small></p>
								</td>
							</tr>
						<?php endforeach; ?>
						<?php if(count($feeds)==0){ echo '<tr><td colspan="3" class="padding-tabal"></td></tr><tr class="blue-color"><td>&nbsp;</td><td align="center" valign="middle" colspan="6">No records available</td><td>&nbsp;</td></tr>';} ?>
					</tbody>
				</table> */?>
				
	</div>
	
</div>
<div class="height10"></div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#helppage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>