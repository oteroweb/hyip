<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if($locked=='yes'){?>
	<div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;">
		  <font face="Myriad Pro">You Are Blocked For <?php echo $SITECONFIG["antibrutehours"];?> Hours.</font>
		</div>
	</div>
<?php }elseif($plocked=='yes'){?>
	<div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;">
		  <font face="Myriad Pro">You Are Permanently Blocked.</font>
		</div>
	</div>
<?php }elseif($memberblockper=='yes'){?>
	<div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;">
		  <font face="Myriad Pro">You Are Permanently Blocked.</font>
		</div>
	</div>
<?php }else{?>
	<div class="loginboxmain">
		<div class="loginbox">
			<div class="welcometextmain">
			  <div class="welcometext"><span>Welcome</span> Admin</div>
			  <div>Current Version : Proxscripts <?php echo CURRENTVERSION;?></div>
			</div>
			
			<?php echo $this->Form->create('Member',array('type' => 'post', 'id'=>'loginform', 'onsubmit' => 'return false;', 'autocomplete'=>'off')); ?>
			<?php if($emailsent=='emailsent'){?><div id="UpdateMessage" class="formsuccess" style="display:block;"><?php echo 'Password Successfully Sent To Your Email Account'?></div><?php }else{?><div id="UpdateMessage"></div><?php }?>
			
			<div class="admintypemain">
			  <div class="admintype">Admin Type</div>
			  <div class="select-main">
				<label>
					<?php echo $this->Form->input('admintype', array(
						'type' => 'select',
						'options' => array('Main'=>'Main Admin', 'Sub'=>'Sub Admin'),
						'class'=>'',
						'label' => false,
						'style' => ''
					));?>
				</label>
			  </div>
			</div>
			
			<div class="userform">
			  <div class="textbox1">
				<span class="glyphicon glyphicon-user"></span>
				<?php echo $this->Form->input('user_name' ,array('id'=>'user_name', "placeholder"=>"Username", 'label'=>'', 'div'=>false));?>
			  </div>
			  <div class="textbox1">
				<span class="glyphicon glyphicon-lock"></span>
				<?php echo $this->Form->input('password' ,array('id'=>'password', "class"=>"keyboardInput", 'label'=>'', 'placeholder'=>'Password', 'div'=>false));?>
			  </div>
			</div>
			
			<?php if($AdminLoginCaptcha){ ?>
				<div class="textbox1 capthamain">
					<div class="capthaimage"><?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'AdminLoginCaptcha','vspace'=>2,"style"=>"vertical-align: middle", 'width'=>'118', 'height'=>'44')); ?></div>
					
					<div class="refreshcaptcha"><a href="javascript:void(0);" onclick="javascript:document.images.AdminLoginCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"","style"=>"vertical-align: middle"));?></a></div>
					
					<div class="capthainput"><?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"", 'label'=>'', 'placeholder'=>'Enter Captcha'));?></div>
				</div>
				<div style="height: 10px;"></div>
			<?php }?>
			
			<div class="loginbutton">
				<?php echo $this->Js->submit('Login', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#UpdateMessage',
					'class'=>'',
					'controller'=>'Member',
					'action'=>'index'
				));?>
			</div>
			<?php echo $this->Form->end();?>
			
			<div class="forgotpass" id="forgetpassworddiv" style="display:none;"><?php echo $this->html->link('Forgot Your Password?', array('controller' => 'forgotpassword', 'action' => 'index'));?></div>
			<div class="whiteborder"></div>
			<div class="copyrighttext">Copyright &copy; <?php echo date("Y"); ?> <?php echo "<a href='http://www.".$SITECONFIG["poweredby_link"]."/index.php?ref=".$SITECONFIG["adminid"]."' target='_blank'>".$SITECONFIG["poweredby_link"]."</a>";?> All Rights Reserved</div>
		</div>
	</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>