<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 24-09-2014
  *********************************************************************/
?>

<div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
  <div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
	<font face="Myriad Pro"><?php echo __('Your last session was terminated incorrectly. Please try logging in again after some time.'); ?>.<br /> <a href="<?php echo $SITEURL;?>login"><?php echo __('Please login to get started'); ?>.</a></font>
  </div>
</div>