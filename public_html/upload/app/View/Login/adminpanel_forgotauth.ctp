<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
	<div class="loginboxmain">
		<div class="loginbox">
			<div class="welcometextmain">
			  <div class="welcometext"><span>Welcome</span> Admin</div>
			</div>
			<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'login','action'=>'forgotauth')));?>
			<div id="UpdateMessage"></div>
			<div class="userform">
			<div class="textbox1">
				<?php echo $this->Form->input('email' ,array('type'=>'text', "class"=>"", 'label'=>'', 'placeholder'=>'Email ID', 'div'=>false));?>
			</div>
			</div>
			<div class="loginbutton">
				<?php echo $this->Js->submit(__('Update'), array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#UpdateMessage',
					'class'=>'button',
					'div'=>false,
					'controller'=>'login',
					'action'=>'forgotauth',
					'url'   => array('controller' => 'login', 'action' => 'forgotauth')
				));?>
			</div>
			<?php echo $this->Form->end();?>
		</div>
	</div>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>