<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php 
if($ActivationStatus=='Success')
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro">Your IP Address is Verified,<br /> <a href="<?php echo $ADMINURL;?>/login">Please Login to Get Started</a></font>
		</div>
	  </div>
	<?php
}
elseif($ActivationStatus=='Activated')
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro">Your IP Address is Already Verified,<br /> <a href="<?php echo $ADMINURL;?>/login">Please Login to Get Started</a></font>
		</div>
	  </div>
	<?php
}
elseif($ActivationStatus=='InvalidCode')
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro">Invalid Code</font>
		</div>
	  </div>
	<?php
}
elseif($ActivationStatus=='Invalid')
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro">Invalid Argument Given</font>
		</div>
	  </div>
	<?php
}
?>