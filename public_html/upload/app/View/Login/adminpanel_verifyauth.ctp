<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php 
if($ActivationStatus=='Activated')
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro">You Have Already Disable Google Authenticator,<br /> <a href="<?php echo $ADMINURL;?>/login">Please Login to Get Started</a></font>
		</div>
	  </div>
	<?php
}
elseif($ActivationStatus=='Invalid')
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro">Invalid Argument Given</font>
		</div>
	  </div>
	<?php
}
elseif($ActivationStatus=='Expired')
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro">Your Link Is Expired</font>
		</div>
	  </div>
	<?php
}
else
{ ?>
	<div class="loginboxmain">
		<div class="loginbox">
			<div class="welcometextmain">
			  <div class="welcometext"><span>Welcome</span> Admin</div>
			</div>
			<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'login','action'=>'verifyauth/'.$code)));?>
			<div id="UpdateMessage"></div>
			<div class="userform">
				<div class="textbox1">
					<span class="glyphicon glyphicon-question-sign"></span>
					<div class="question"><?php echo $code;?></div>
				  </div>
				<div class="textbox1">
					<span class="glyphicon glyphicon-lock"></span>
					<?php echo $this->Form->input('current_pwd' ,array('type'=>'password','id'=>'password', "class"=>"keyboardInput", 'label'=>'', 'placeholder'=>'Password', 'div'=>false));?>
				</div>
			</div>
			<div class="loginbutton">
				<?php echo $this->Js->submit(__('Update'), array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#UpdateMessage',
					'class'=>'button',
					'div'=>false,
					'controller'=>'login',
					'action'=>'forgotauth/'.$code,
					'url'   => array('controller' => 'login', 'action' => 'verifyauth/'.$code)
				));?>
			</div>
			<?php echo $this->Form->end();?>
			<div class="forgotpass" id="forgetpassworddiv"><?php echo $this->html->link('Login', array('controller' => 'login', 'action' => 'index'));?></div>
		</div>
	</div>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>
<?php }
?>