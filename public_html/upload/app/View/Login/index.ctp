<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 24-09-2014
  *********************************************************************/
?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<?php if($locked=='yes'){ // If Anti Brute Force lock is placed on member due to incorrect answers ?>
	<div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;">
		  <font face="Myriad Pro"><?php echo __('Access to the site has been blocked for you. Please try after following hours').' : '.$SITECONFIG["antibrutehours"]; ?></font>
		</div>
	</div>
<?php }elseif($plocked=='yes'){ // If Anti Brute Force permanent lock is placed on member due to incorrect answers within specified time ?>
	<div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;">
		  <font face="Myriad Pro"><?php echo __('You are blocked. Please confirm your account first.');?></font>
		</div>
	</div>
<?php }elseif($memberblockper=='yes'){ // If admin has blocked the IP address to access the site ?>
	<div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;">
		  <font face="Myriad Pro"><?php echo __('Your account or IP address is blocked.');?></font>
		</div>
	</div>
<?php }else{?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Login');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<div class="form-box">
		
		<?php // Login form starts here ?>
		<?php echo $this->Form->create('Member',array('type' => 'post', 'id' => 'RegistrationForm', 'onsubmit' => 'return false;', 'autocomplete'=>'off')); ?>
			<?php if($SITECONFIG["chkfb"]==1 || $SITECONFIG["chkgoogle"]==1){ ?>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Sign in With");?> : </div>
					<div class="form-col-2 imagetag">
						<?php if($SITECONFIG["chkfb"]==1){ ?>
							<span class="textleft vam">
								<?php echo $this->Js->link($this->html->image('facebook.jpg', array('alt'=>'', 'align' =>'absmiddle')), array('controller'=>'login', "action"=>"with/facebook"), array(
								'update'=>'#UpdateMessage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false
							));?>
							</span>
						<?php }?>
						<?php if($SITECONFIG["chkgoogle"]==1){ ?>
							<span class="textleft vam">
							<?php echo $this->Js->link($this->html->image('google.jpg', array('alt'=>'', 'align' =>'absmiddle')), array('controller'=>'login', "action"=>"with/google"), array(
								'update'=>'#UpdateMessage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false
							));?>
							</span>
						<?php } ?>
					</div>
				</div>
			<?php }?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Username");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('user_name' ,array('type'=>'text', "class"=>"formtextbox","div"=>false, 'label'=>false));?>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Password");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('password' ,array('type'=>'password',"div"=>false, "class"=>"login-from-box-1 keyboardInput", 'label'=>false));?>
			</div>
			<?php if($this->Session->check('verified_ip')){ ?>
			  <div class="form-row">
				<div class="form-col-1"><?php echo __('Verified IP Address')?> :</div>
				<?php echo $this->Session->read('verified_ip'); ?>
			  </div>
			<?php } ?>
			<?php if($MemberLoginCaptcha){ ?>
				<div class="form-row captchrow">
				  <div class="form-col-1"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
					  <?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formcapthcatextbox", 'div'=>false, 'label'=>false));?>
					  <span><?php echo __('Code')?> :</span>
					  <?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'MemberSignupCaptcha','vspace'=>2, 'align'=>'absmiddle', 'width'=>'118', 'height'=>'44')); ?> 
					  <a href="javascript:void(0);" onclick="javascript:document.images.MemberSignupCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "width"=>"", "height"=>"", "align"=>"absmiddle"));?></a>
					  <div class="clear-both"></div>
				</div>
			<?php }?>
		
		<div class="formbutton">
			<?php echo $this->Js->submit(__('Login'), array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'div'=>false,
			  'class'=>'button',
			  'controller'=>'Member',
			  'action'=>'index'
			));?>
		</div>
		<div class="profile-bot-left">
			<div class="creat-pass">
				<?php echo $this->html->link(__("Create an account"), array('controller' => 'register', 'action' => 'index')).'&nbsp;&nbsp;|&nbsp;&nbsp;'.$this->html->link(__("Forgot Password"), array('controller' => 'forgotpassword', 'action' => 'index')); if($SITECONFIG["emailconfirmation"]==1){ echo '&nbsp;&nbsp;|&nbsp;&nbsp;'.$this->html->link(__("Resend Activation Link"), array('controller' => 'public', 'action' => 'resendactivation'));?>
				<?php }?>
			</div>
		</div>
		<div class="clear-both"></div>
		<?php echo $this->Form->input('loginpage', array('type'=>'hidden', 'label' => false));?>
		<?php echo $this->Form->end();?>
		<?php // Login form ends here ?>
		
	</div>	
<div class="clear-both"></div>
</div>
<div class="height10"></div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>