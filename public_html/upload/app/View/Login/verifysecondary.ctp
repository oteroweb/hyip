<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 24-09-2014
  *********************************************************************/
?>
<?php 
if($ActivationStatus=='Activated') // If the IP address is already verified by the member
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro"><?php echo __('You Have Already Disable Two Step Authenticator'); ?>.<br /> <a href="<?php echo $SITEURL;?>login"><?php echo __('Please login to get started'); ?>.</a></font>
		</div>
	  </div>
	<?php
}
elseif($ActivationStatus=='Expired') // If the verification code from the activation link is wrong
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro"><?php echo __('Your Link Is Expired'); ?></font>
		</div>
	  </div>
	<?php
}
elseif($ActivationStatus=='Invalid') // If the arguments passed with the activation link are invalid
{
	?>
	  <div style="border:1px solid #B9B9B9;width:70%;margin: 40px auto;border-radius:10px;">
		<div style="margin:40px;text-align:center;font-size:20px;text-transform:capitalize;">
		  <font face="Myriad Pro"><?php echo __('Invalid arguments passed'); ?></font>
		</div>
	  </div>
	<?php
}
else
{
?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Two Step Verification');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<div class="form-box">
		
		<?php // Secondary Password form starts here ?>
		<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'login','action'=>'verifysecondary/'.$id.'/'.$code)));?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Code");?> : </div>
				<?php echo $code;?>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Password");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('current_pwd' ,array('type'=>'password',"div"=>false, "class"=>"login-from-box-1 keyboardInput", 'label'=>false));?>
			</div>
		<div class="formbutton">
			<?php echo $this->Js->submit(__('Submit'), array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'div'=>false,
			  'class'=>'button',
			  'controller'=>'login',
			  'action'=>'verifysecondary/'.$id.'/'.$code,
			  'url'   => array('controller' => 'login', 'action' => 'verifysecondary/'.$id.'/'.$code)
			));?>
		</div>
		<div class="clear-both"></div>
		<?php echo $this->Form->end();?>
		<?php // Secondary Password form ends here ?>
		
	</div>	
<div class="clear-both"></div>
</div>
<?php } ?>