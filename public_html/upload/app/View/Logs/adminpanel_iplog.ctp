<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">IP Logs</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Login IP Logs", array('controller'=>'logs', "action"=>"iplog"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Registration IP Logs", array('controller'=>'logs', "action"=>"iplogregistration"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
		</ul>
	</div>
</div>
<div class="tab-content">
<div id="memberpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/IP_Logs#Login_IP_Logs" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<?php if($ipaddress==''){?>
<div class="serchmainbox">
   <div class="serchgreybox">
       Search Option
   </div>
   <?php echo $this->Form->create('Iplog',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'logs','action'=>'iplog')));?>
   <div class="from-box">
        <div class="fromboxmain">
             <span>Search By :</span>
             <span>                     
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
						<?php 
						echo $this->Form->input('searchby', array(
							'type' => 'select',
							'options' => array('all'=>'Select Parameter', 'ip'=>'IP Address'),
							'selected' => $searchby,
							'class'=>'',
							'label' => false,
							'style' => ''
						));
						?>
						</label>
					</div>
				</div>
             </span>
		</div>
        <div class="fromboxmain">
				<span>Search For :</span>
				<span><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?></span>
				<span class="padding-left">
							<?php echo $this->Js->submit('', array(
								'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'update'=>'#memberpage',
								'class'=>'searchbtn',
								'controller'=>'logs',
								'action'=>'memberactivity',
								'url'=> array('controller' => 'logs', 'action' => 'iplog')
							  ));?>
				</span>
        </div>
    </div>
    <?php echo $this->Form->end();?>
</div>
<?php } ?>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'logs', 'action'=>'iplog/0/0/'.urlencode($ipaddress)."/".$type)
	));
	$currentpagenumber=$this->params['paging']['User_ip_log']['page'];
	?>
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
		<?php if($ipaddress!=''){
				echo $this->Js->link("Back", array('controller'=>'logs', "action"=>"iplog"), array(
					'update'=>'#memberpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btngray'
				));
		
				echo '&nbsp;&nbsp;<span class="tabal-title-text content-label">IP Address : '.$ipaddress.'</span>';
		} ?> 
	</div>
	<div class="clear-both"></div>
	
	<?php if($ipaddress==''){?>
		<div class="tablegrid">
				<div class="tablegridheader">
					<div>IP Address</div>
					<div>Login IP Statistics</div>
				</div>
				<?php foreach ($user_ip_logs as $user_ip_log): ?>
					<div class="tablegridrow">
						<div>
							<?php 
							echo $this->Js->link($user_ip_log['User_ip_log']['ip'], array('controller'=>'logs', "action"=>"iplog/0/0/".urlencode($user_ip_log['User_ip_log']['ip'])."/0"), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Logs'
							));?>
						</div>
						<div>
							<?php 
							echo $this->Js->link($user_ip_log[0]['counter'], array('controller'=>'logs', "action"=>"iplog/0/0/".urlencode($user_ip_log['User_ip_log']['ip'])."/0"), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Logs'
							));?>
						</div>
					</div>
				<?php endforeach; ?>
		</div>
		<?php if(count($user_ip_logs)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	<?php }else{?>
		<div class="tablegrid">
				<div class="tablegridheader">
					<div><?php echo "Username";?></div>
					<div><?php echo "Login Date";?></div>
				</div>
				<?php foreach ($user_ip_logs as $user_ip_log): ?>
					<div class="tablegridrow">
						<div>
						<?php 
						echo $this->Js->link($user_ip_log['User_ip_log']['user_name'], array('controller'=>'member', "action"=>"memberadd/".$user_ip_log['User_ip_log']['member_id']."/top/logs/iplog~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
						</div>
						<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $user_ip_log['User_ip_log']['login_date']);?></div>
					</div>
				<?php endforeach; ?>
		</div>
		<?php if(count($user_ip_logs)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	<?php }
	if($this->params['paging']['User_ip_log']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'logs','action'=>'iplog/0/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#memberpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'logs',
			  'action'=>'iplog/0/rpp/'.$ipaddress,
			  'url'   => array('controller' => 'logs', 'action' => 'iplog/0/rpp/'.urlencode($ipaddress).'/'.$type)
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php } ?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#memberpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>