<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Logs / Admin Logs</div>
<div id="logspage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Admin_Logs" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Admin_ip_log',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'logs','action'=>'admin')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php echo $this->Form->input('searchby', array(
						'type' => 'select',
						'options' => array('all'=>'Select Parameter', 'member_id'=>'Member Id', 'admintype'=>'Admin Type', 'user_name'=>'Username', 'ip'=>'IP Address'),
						'selected' => $searchby,
						'class'=>'',
						'label' => false,
						'style' => '',
						'onchange'=>'if($(this).val()=="admintype"){$(".searchfor_all").hide();$(".searchfor_size").show(500);}else{$(".searchfor_size").hide();$(".searchfor_all").show(500);}'
						));?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span class='searchfor_all' style='display:<?php if($searchby=="admintype"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class='searchfor_size' style='display:<?php if($searchby!="admintype"){ echo "none";} ?>'>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
						<?php 
						echo $this->Form->input('admintype', array(
							  'type' => 'select',
							  'options' => array('Main'=>'Main Admin', 'Sub'=>'Sub Admin'),
							  'selected' => $searchfor,
							  'class'=>'',
							  'label' => false,
							  'style' => ''
						));
						?>
						</label>
					</div>
				</div>
			</span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
                  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#logspage',
                  'class'=>'searchbtn',
                  'controller'=>'logs',
                  'action'=>'admin',
                  'url'=> array('controller' => 'logs', 'action' => 'admin')
                ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->

        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#logspage',
            'evalScripts' => true,
            'url'=> array('controller'=>'logs', 'action'=>'admin')
        ));
        $currentpagenumber=$this->params['paging']['Admin_ip_log']['page'];
        ?>
		
<div id="gride-bg" class="noborder">
    <div class="padding10">
	<div class="greenbottomborder">			
	<?php echo $this->Form->create('Admin_ip_log',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'logs','action'=>'adminremove')));?>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	    <?php if(!isset($SubadminAccessArray) || in_array('logs',$SubadminAccessArray) || in_array('logs/adminpanel_admincsv',$SubadminAccessArray)){ ?>
                <?php echo $this->Html->link("Download CSV File", array('controller'=>'logs', "action"=>"admincsv"), array(
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'class'=>'btngray',
                    'target'=>'_blank'
                ));?>
        <?php } ?>
        <?php if(!isset($SubadminAccessArray) || in_array('logs',$SubadminAccessArray) || in_array('logs/adminpanel_adminremove',$SubadminAccessArray)){ ?>
                <?php echo $this->Js->link("Delete All Logs", array('controller'=>'logs', "action"=>"adminremove/all"), array(
                    'update'=>'#logspage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'confirm' => 'Are You Sure?',
                    'class'=>'btngray'
                ));?>
        <?php } ?>
	</div>
	<div class="clear-both"></div>
		
		<div class="tablegrid">
				<div class="tablegridheader">
				    <div>
				    <?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('M. Id', array('controller'=>'logs', "action"=>"admin/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#logspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Member Id'
                        ));?>
				    </div>
					<div>
                        <?php echo $this->Js->link('Login Date', array('controller'=>'logs', "action"=>"admin/0/0/login_date/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#logspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Login Date'
                        ));?>
                    </div>
					<div>
                        <?php 
                        echo $this->Js->link('Admin Type', array('controller'=>'logs', "action"=>"admin/0/0/admintype/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#logspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Admin Type'
                        ));?>
                    </div>
					<div>
                        <?php 
                        echo $this->Js->link('Username', array('controller'=>'logs', "action"=>"admin/0/0/user_name/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#logspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Username'
                        ));?>
                    </div>
					<div>
                        <?php echo $this->Js->link("IP Address", array('controller'=>'logs', "action"=>"admin/0/0/ip/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#logspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>"Sort By IP Address"
                        ));?>
                    </div>
                </div>
                <?php foreach ($admin_ip_logs as $admin_ip_log): ?>
                    <div class="tablegridrow">
					    <div>
							<?php 
							if($admin_ip_log['Admin_ip_log']['admintype']=='Main')
							{
								echo $this->Js->link($admin_ip_log['Admin_ip_log']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$admin_ip_log['Admin_ip_log']['member_id']."/top/logs/admin~top"), array(
									'update'=>'#pagecontent',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>'View Member'
								));
							}else{echo $admin_ip_log['Admin_ip_log']['member_id'];}?>
						</div>
						<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $admin_ip_log['Admin_ip_log']['login_date']); ?></div>
                        <div><?php echo $admin_ip_log['Admin_ip_log']['admintype'].' Admin'; ?></div>
                        <div><?php echo $admin_ip_log['Admin_ip_log']['user_name']; ?></div>
                        <div><?php echo $admin_ip_log['Admin_ip_log']['ip']; ?></div>
                    </div>
                <?php endforeach; ?>
        </div>
		<?php if(count($admin_ip_logs)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php echo $this->Form->end();
        if($this->params['paging']['Admin_ip_log']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Admin_ip_log',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'logs','action'=>'admin/0/rpp')));?>
			<div class="resultperpage">
				<label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
				</label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#logspage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'logs',
                  'action'=>'admin/0/rpp',
                  'url'   => array('controller' => 'logs', 'action' => 'admin/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
	</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#logspage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>