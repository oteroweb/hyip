<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Logs / Cronjob Logs</div>
<div id="logspage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Cronjob_Logs" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Cronjob',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'logs','action'=>'cronjob')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php
						$searchbyoptions=array('all'=>'Select Parameter', 'backup'=>'Backup', 'soload'=>'Soload', 'autowithdraw'=>'Autowithdraw','autoresponder'=>'Autoresponder', 'mail'=>'Mail', 'membership'=>'Membership', 'common'=>'Common');//, 'jackpotwinner'=>'Jackpot Winner',  'dailyearn'=>'Daily Earning', 'dailyearnhours'=>'Dailyearnhours', 'dailyearndays'=>'Dailyearndays', 'revenueshare'=>'Revenue Share', 'dynamicearning'=>'Dynamic Earning', 'dynamicearndays'=>'Dynamicearndays', 'dynamicearnhours'=>'Dynamicearnhours');
						
						//Module Code Start
						$cronjoblist=array();
						$modules=@explode(",",trim($SITECONFIG["modules"],","));
						foreach($modules as $module)
						{
							$subcounter=1;
							$modulearray=@explode(":", $module);
							if($modulearray[2]==1 && trim($modulearray[15])!="")
							{
								$cronarray=explode("-",$modulearray[15]);
								$subplannamearray=explode("-",$modulearray[13]);
								foreach($subplannamearray as $subplanname)
								{
									$cjlist=array();
									$cjarray=explode(".",$cronarray[$subcounter-1]);
									foreach($cjarray as $cj)
									{
										$cjlst=array();
										$carray=explode(";",$cj);
										foreach($carray as $c)
										{
											
											if(count($subplannamearray)==1)
												$cjlist[ucfirst($modulearray[1]).$subcounter."_".$c] = $c;
											else
												$cjlist[ucfirst($modulearray[1]).$subcounter."_".$c] = $subplanname." ".$c;
										}
									}
									if(count($subplannamearray)==1)
										$cronjoblist[$modulearray[0]] = $cjlist;
									else
										$cronjoblist[$subplanname." ".$modulearray[0]] = $cjlist;
									
									$subcounter++;
								}
							}
						}
						//Module Code Over
						
						$searchbyoptions=array_merge($searchbyoptions, $cronjoblist);
						
						//$modules=@explode(",",trim($SITECONFIG["modules"],","));
						//foreach($modules as $module)
						//{
						//	$modulearray=@explode(":", $module);
						//	if($modulearray[2]==1 && $modulearray[15]!="")
						//	{
						//		$subplanarray=explode("-",$modulearray[3]);
						//		$subcronjobarray=explode("-",$modulearray[15]);
						//		$subplanname=explode("-",$modulearray[13]);
						//		$subcounter=1;
						//		foreach($subplanarray as $subplan)
						//		{
						//			if(count($subplanarray)==1)
						//				$searchbyoptions[$modulearray[1].$subcounter.$subcronjobarray[$subcounter-1]]=$modulearray[0];
						//			else
						//				$searchbyoptions[$modulearray[1].$subcounter.$subcronjobarray[$subcounter-1]]=$subplanname[$subcounter-1]." ".$modulearray[0];
						//			$subcounter++;
						//		}
						//	}
						//}
						?>
						<?php echo $this->Form->input('searchby', array(
								  'type' => 'select',
								  'options' => $searchbyoptions,
								  'selected' => $searchby,
								  'class'=>'',
								  'label' => false,
								  'div'=>false,
								  'style' => ''
						));?>
					</label>
				</div>
			</div>
		  </span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
                  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#logspage',
                  'class'=>'searchbtn',
                  'controller'=>'logs',
                  'action'=>'cronjob',
                  'url'=> array('controller' => 'logs', 'action' => 'cronjob')
                ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->

        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#logspage',
            'evalScripts' => true,
            'url'=> array('controller'=>'logs', 'action'=>'cronjob')
        ));
        $currentpagenumber=$this->params['paging']['Cronjob']['page'];
        ?>

<div id="gride-bg" class="noborder">
    <div class="padding10">
	<div class="greenbottomborder">			
	<?php echo $this->Form->create('Cronjob',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'logs','action'=>'cronjobremove')));?>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
        <?php if(!isset($SubadminAccessArray) || in_array('logs',$SubadminAccessArray) || in_array('logs/adminpanel_cronjobremove',$SubadminAccessArray)){ ?>
                <?php echo $this->Js->link("Delete All Logs", array('controller'=>'logs', "action"=>"cronjobremove/all"), array(
                    'update'=>'#logspage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'confirm' => 'Are You Sure?',
                    'class'=>'btngray'
                ));?>
        <?php } ?>
	</div>
	<div class="clear-both"></div>
		
		<div class="tablegrid">
				<div class="tablegridheader">
				    <div>
						<?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('Id', array('controller'=>'logs', "action"=>"cronjob/0/0/id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#logspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Id'
                        ));?>
					</div>
					<div>
						<?php echo $this->Js->link('Run Time', array('controller'=>'logs', "action"=>"cronjob/0/0/dt/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#logspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Run Time'
                        ));?>		
					</div>
					<div>
						<?php 
                            echo $this->Js->link('File Name', array('controller'=>'logs', "action"=>"cronjob/0/0/filename/".$sorttype."/".$currentpagenumber), array(
                                'update'=>'#logspage',
                                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                'escape'=>false,
                                'class'=>'vtip',
                                'title'=>'Sort By File Name'
							));?>		
					</div>
				</div>	
		
                <?php foreach ($cronjobs as $cronjob): ?>
                    <div class="tablegridrow">
                        <div><?php echo $cronjob['Cronjob']['id']; ?></div>
						<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $cronjob['Cronjob']['dt']); ?></div>
                        <div><?php echo $cronjob['Cronjob']['filename']; ?></div>
                    </div>
                <?php endforeach; ?>        </div>
		<?php if(count($cronjobs)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php echo $this->Form->end();
        if($this->params['paging']['Cronjob']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <div class="height10"></div>
            <?php echo $this->Form->create('Cronjob',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'logs','action'=>'cronjob/0/rpp')));?>
			<div class="resultperpage">
				<label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
				</label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#logspage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'logs',
                  'action'=>'cronjob/0/rpp',
                  'url'   => array('controller' => 'logs', 'action' => 'cronjob/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
	</div>
    </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#logspage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>