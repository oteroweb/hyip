<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Logs / Processor Trace</div>
<div id="logspage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<script>
function ViewDescription(atag)
{
	$("a[rel='lightboxtext']").colorbox({width:"80%", height:"80%", inline:true, href:"#"+atag.title});
}
</script>
<script type="text/javascript">
	<?php if($searchby=='processorid'){ ?>
	generatecombo("data[Processordebug][proc_name]","Processor","id","proc_name","<?php echo $searchfor; ?>",'#searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } elseif($searchby=='type'){ ?>
	generatecombo("data[Processordebug][type]","Processordebug","type","type","<?php echo $searchfor; ?>",'#searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } ?>
</script>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Processor_Trace_Logs" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Processordebug',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'logs','action'=>'posttrace')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php echo $this->Form->input('searchby', array(
						'type' => 'select',
						'options' => array('all'=>'Select Parameter','memberid'=>'Member Id', "processorid"=>'Payment Processor',"transaction_no"=>'Transaction No',"type"=>'Type',"ipaddress"=>'IP Address'),
						'selected' => $searchby,
						'class'=>'',
						'label' => false,
						'style' => '',
						'onchange'=>'if($(this).val()=="processorid"){generatecombo("data[Processordebug][proc_name]","Processor","id","proc_name","'.$searchfor.'","#searchforcombo","'.$ADMINURL.'","");$("#searchforall").hide();$("#searchforcombo").show(500);}else if($(this).val()=="type"){generatecombo("data[Processordebug][type]","Processordebug","type","type","'.$searchfor.'","#searchforcombo","'.$ADMINURL.'","");$("#searchforall").hide();$("#searchforcombo").show(500);}else{$("#searchforcombo").hide();$("#searchforall").show(500);}'
						));?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span id='searchforall' style='display: <?php if($searchby=='processorid' || $searchby=='type'){ echo 'none'; } ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span id='searchforcombo' style='display: <?php if($searchby!='processorid' && $searchby!='type'){ echo 'none'; } ?>'></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
                  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#logspage',
                  'class'=>'searchbtn',
                  'controller'=>'logs',
                  'action'=>'posttrace',
                  'url'=> array('controller' => 'logs', 'action' => 'posttrace')
                ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
 
        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#logspage',
            'evalScripts' => true,
            'url'=> array('controller'=>'logs', 'action'=>'posttrace')
        ));
        $currentpagenumber=$this->params['paging']['Processordebug']['page'];
        ?>
		
<div id="gride-bg" class="noborder">
    <div class="padding10">
	<div class="greenbottomborder">			
	<?php echo $this->Form->create('Processordebug',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'logs','action'=>'memberremove')));?>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	    <?php if(!isset($SubadminAccessArray) || in_array('logs',$SubadminAccessArray) || in_array('logs/adminpanel_posttraceremove',$SubadminAccessArray)){ ?>
                <?php echo $this->Js->link("Delete All Logs", array('controller'=>'logs', "action"=>"posttraceremove/all"), array(
                    'update'=>'#logspage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'confirm' => 'Are You Sure?',
                    'class'=>'btngray'
                )); } ?>
	</div>
	<div class="clear-both"></div>
		
		<div class="tablegrid">
				<div class="tablegridheader">
				    <div>
                        <?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('M. Id', array('controller'=>'logs', "action"=>"posttrace/0/0/memberid/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#logspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Member Id'
                        ));?>
                    </div>
				    <div>
                        <?php echo $this->Js->link('Log Date', array('controller'=>'logs', "action"=>"posttrace/0/0/pay_dt/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#logspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Log Date'
                        ));?>
                    </div>
				    <div>
                        <?php 
                            echo $this->Js->link('Processor id', array('controller'=>'logs', "action"=>"posttrace/0/0/processorid/".$sorttype."/".$currentpagenumber), array(
                                'update'=>'#logspage',
                                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                'escape'=>false,
                                'class'=>'vtip',
                                'title'=>'Sort By Processor id'
                            ));?>
                    </div>
				    <div>
                        <?php echo $this->Js->link('Payment Processor', array('controller'=>'logs', "action"=>"posttrace/0/0/processor/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#logspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Payment Processor'
                        ));?>
                    </div>
				    <div>
                        <?php echo $this->Js->link("Transaction No", array('controller'=>'logs', "action"=>"posttrace/0/0/transaction_no/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#logspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>"Sort By Transaction No"
                        ));?>
                    </div>
				    <div>
                        <?php echo $this->Js->link('Type', array('controller'=>'logs', "action"=>"posttrace/0/0/type/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#logspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Type'
                        ));?>
                    </div>
				    <div>
                        <?php echo $this->Js->link('IP Address', array('controller'=>'logs', "action"=>"posttrace/0/0/ipaddress/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#logspage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By IP Address'
                        ));?>
                    </div>
				    <div>IPN Content</div>
                </div>
                <?php foreach ($processordebugs as $processordebug):?>
                    <div class="tablegridrow">
					    <div>
							<?php 
							echo $this->Js->link($processordebug['Processordebug']['memberid'], array('controller'=>'member', "action"=>"memberadd/".$processordebug['Processordebug']['memberid']."/top/logs/posttrace~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
						</div>
				        <div><?php echo $this->Time->format($SITECONFIG["timeformate"], $processordebug['Processordebug']['pay_dt']); ?></div>
                        <div><?php echo $processordebug['Processordebug']['processorid']; ?></div>
						<div><?php echo $processordebug['Processordebug']['processor']; ?></div>
						<div><?php echo $processordebug['Processordebug']['transaction_no']; ?></div>
						<div><?php echo ucfirst($processordebug['Processordebug']['type']); ?></div>
						<div><?php echo $processordebug['Processordebug']['ipaddress']; ?></div>
						<div>
				            <a href="#" rel="lightboxtext" title="ViewDescription-<?php echo $processordebug['Processordebug']['id'];?>" onclick="ViewDescription(this)">
								<?php echo $this->html->image('search.png', array('alt'=>'Notes', 'align'=>'absmiddle'));?>
							</a>
                            <div style="display:none;"><div id="ViewDescription-<?php echo $processordebug['Processordebug']['id'];?>"><?php echo stripslashes(nl2br($processordebug['Processordebug']['description']));?></div></div>
						</div>
                    </div>
                <?php endforeach; ?>
        </div>
		<?php if(count($processordebugs)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php echo $this->Form->end();
        if($this->params['paging']['Processordebug']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Processordebug',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'logs','action'=>'posttrace/0/rpp')));?>
			<div class="resultperpage">
				<label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
				</label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#logspage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'logs',
                  'action'=>'posttrace/0/rpp',
                  'url'   => array('controller' => 'logs', 'action' => 'posttrace/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
	</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#logspage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>