<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Modules</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Modules", array('controller'=>'managemodule', "action"=>"index"), array(
					'update'=>'#managemodulepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Import Module", array('controller'=>'managemodule', "action"=>"patch"), array(
					'update'=>'#managemodulepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Remove Module", array('controller'=>'managemodule', "action"=>"unpatch"), array(
					'update'=>'#managemodulepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="managemodulepage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
	<div class="helpicon"><a href="https://www.proxscripts.com/docs/Modules#Modules" target="_blank">Help</a></div>
    <div id="UpdateMessage"></div>
	
	<div id="Xgride-bg">
    <div class="Xpadding10">
		<div class="greenbottomborder">
		<div class="height10"></div>
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'managemodule','action'=>'moduleaction')));?>
		<?php echo $this->form->input('id', array('type'=>'hidden','value'=>'1'));?>
		<div class="addnew-button checkbox">
			<?php echo $this->Form->checkbox('selectAllCheckboxes', array(
			'hiddenField' => false,
			'onclick' => 'selectAllCheckboxes("moduledata",this.checked);'
		  ));?>
		  <label for='SitesettingSelectAllCheckboxes'></label>
		</div>
		<div class="addnew-button">
		<?php if(!isset($SubadminAccessArray) || in_array('managemodule',$SubadminAccessArray) || in_array('managemodule/adminpanel_moduleaction',$SubadminAccessArray)){
			echo $this->Js->submit('Update Modules', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'escape'=>false,
			  'update'=>'#managemodulepage',
			  'class'=>'btngray',
			  'div'=>false,
			  'controller'=>'managemodule',
			  'action'=>'moduleaction',
			  'url'   => array('controller' => 'managemodule', 'action' => 'moduleaction')
			));
		} ?>
		</div>
		<div class="clear-both"></div>
		
		<div class="tablegrid">
			<div class="tablegridheader">
				<div>Module Label</div>
				<div>Module Name</div>
				<div>Action</div>
				<div></div>
			</div>
			<?php foreach ($modules as $module):$namestatus=@explode(":", $module);?>
			<div class="tablegridrow">
				<div><?php echo $namestatus[0];?></div>
				<div><?php echo $namestatus[1];?></div>
				<div class="textcenter">
					  <span class="actionmenu">
						<span class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							<li>
							<?php if(!isset($SubadminAccessArray) || in_array('managemodule',$SubadminAccessArray) || in_array('managemodule/adminpanel_patch/$',$SubadminAccessArray)){
								echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Module')).' Edit Module', array('controller'=>'managemodule', "action"=>"patch/".$namestatus[1]), array(
									'update'=>'#managemodulepage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));
							} ?>
							</li>
							</ul>
						</span>
					  </span>
				</div>
				<div class='checkbox'>
					<?php if($namestatus[2]==1){$checked="checked=checked";}else{$checked="";}?>
					<input class='moduledata' type="checkbox" id="Moduledata<?php echo $namestatus[0]?>" name="moduledata[<?php echo $namestatus[0]?>]" <?php echo $checked;?> />
					<label for='Moduledata<?php echo $namestatus[0]?>'></label>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
		<?php if(count($modules)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
		<?php echo $this->Form->end();?>
		<div class="height10"></div>
	</div>
	</div>
	</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#managemodulepage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>