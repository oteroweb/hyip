<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Modules</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Modules", array('controller'=>'managemodule', "action"=>"index"), array(
					'update'=>'#managemodulepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Import Module", array('controller'=>'managemodule', "action"=>"patch"), array(
					'update'=>'#managemodulepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Remove Module", array('controller'=>'managemodule', "action"=>"unpatch"), array(
					'update'=>'#managemodulepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="managemodulepage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<script type="text/javascript"> 
(function() {
$('#Uploadsqlpatch').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();       
</script>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Modules#Remove_Module" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

    
<div class="backgroundwhite">
		
	<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'Uploadsqlpatch', 'type' =>'file', 'onsubmit' => 'return true;','url'=>array('controller'=>'managemodule','action'=>'unpatchaction')));?>
	<div class="frommain">
		<div class="fromnewtext">Module Name : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php
					$modules=@explode(",",trim($SITECONFIG['modules'],","));
					$modulesoption=array();
					foreach($modules as $module)
					{
						$namestatus=@explode(":", $module);
						$modulesoption[$namestatus[1]]=$namestatus[0];
					}
					echo $this->Form->input('moduletype', array(
						'type' => 'select',
						'options' => $modulesoption,
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		
		
		<?php if(@$modulename==''){ ?>
		<div class="fromnewtext">Select SQL UnPatch File : <span class="red-color">*</span></div>
		<div class="fromborderdropedown3">
		  <div class="btnorange browsebutton">Browse<?php echo $this->Form->input('sqlfile', array('type'=>'file', 'label' => false, 'div' => false,'class'=>'browserbuttonlink'));?></div>
		</div>
		
		<?php } ?>
		
		<div class="formbutton">
			<?php echo $this->Form->submit('Submit', array(
				'class'=>'btnorange',
				'div'=>false
			));?>
		</div>
	</div>
	<?php echo $this->Form->end();?>
	
	
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#managemodulepage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>