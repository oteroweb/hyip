<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Modules</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Modules", array('controller'=>'managemodule', "action"=>"index"), array(
					'update'=>'#managemodulepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Import Module", array('controller'=>'managemodule', "action"=>"patch"), array(
					'update'=>'#managemodulepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Remove Module", array('controller'=>'managemodule', "action"=>"unpatch"), array(
					'update'=>'#managemodulepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="managemodulepage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<script type="text/javascript"> 
(function() {
$('#Uploadsqlpatch').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();       
</script>
<?php if($IsAdminAccess){?>
	<div class="helpicon"><a href="https://www.proxscripts.com/docs/Modules#Import_Module" target="_blank">Help</a></div>
    <div id="UpdateMessage"></div>
	
    
<div class="backgroundwhite">
	
	<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'Uploadsqlpatch', 'type' =>'file', 'onsubmit' => 'return true;','url'=>array('controller'=>'managemodule','action'=>'patchaction')));?>
	<?php echo $this->Form->input('editmodulename', array('type'=>'hidden', 'value'=>$modulename, 'label' => false)); ?>
		
	<div class="frommain">
		
		<div class="fromnewtext" <?php if($modulename!=''){  echo 'style="display:none"'; } ?>>License Key :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3" <?php if($modulename!=''){  echo 'style="display:none"'; } ?>>
			<?php echo $this->Form->input('licensekey', array('type'=>'text', 'label' => false, 'error' => false, 'div' => false,'value'=>@$moduledata[17], 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Module Label :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('modulename', array('type'=>'text', 'label' => false, 'error' => false, 'div' => false,'value'=>@$moduledata[0], 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Module Name :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('moduletype', array('type'=>'text', 'label' => false, 'error' => false, 'div' => false,'value'=>@$moduledata[1], 'class'=>'fromboxbg'));?>
		</div>
		
		
		<?php if(@$moduledata[3]==''){ $display='none'; $select=0; }else{ $display=''; $select=1;} ?>
		
		<div class="fromnewtext">Advance Settings : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php echo $this->Form->input('advancesetting', array(
					'type' => 'select',
					'options' => array("0"=>'Disable', "1"=>'Enable'),
					'class'=>'',
					'label' => false,
					'div' => false,
					'selected'=>$select,
					'style' => '',
					'onchange' => 'if(this.selectedIndex==1){$(".advancefields").show(500);}else{$(".advancefields").hide(500);}'
				));?>
			  </label>
		  </div>
		</div>
		
		
		<div class="advancefields" style="display:<?php echo $display; ?>;">
			<div class="fromnewtext">Sub Plan Name :</div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('subplan', array('type'=>'text', 'label' => false, 'error' => false, 'div' => false,'value'=>@$moduledata[13], 'class'=>'fromboxbg'));?>
			</div>
			
		</div>
		
		<div class="advancefields" style="display:<?php echo $display; ?>;">
			<div class="fromnewtext">Plan Model :</div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('planmodel', array('type'=>'text', 'label' => false, 'error' => false, 'div' => false,'value'=>@$moduledata[3], 'class'=>'fromboxbg'));?>
			</div>
			
		</div>

		<div class="advancefields" style="display:<?php echo $display; ?>;">
			<div class="fromnewtext">Position Model :</div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('positionmodel', array('type'=>'text', 'label' => false, 'error' => false, 'div' => false,'value'=>@$moduledata[4], 'class'=>'fromboxbg'));?>
			</div>
			
		</div>

		<div class="advancefields" style="display:<?php echo $display; ?>;">
			<div class="fromnewtext">Purchase Position : (Front Side Purchase Position)</div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('purchaseposition', array('type'=>'text', 'label' => false, 'error' => false, 'div' => false,'value'=>@$moduledata[18], 'class'=>'fromboxbg'));?>
			</div>
			
		</div>
		
		<div class="advancefields" style="display:<?php echo $display; ?>;">
			<div class="fromnewtext">Earning History : (Front Side Earning History)</div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('earninghistory', array('type'=>'text', 'label' => false, 'error' => false, 'div' => false,'value'=>@$moduledata[19], 'class'=>'fromboxbg'));?>
			</div>
			
		</div>
		
		<div class="advancefields" style="display:<?php echo $display; ?>;">
			<div class="fromnewtext">Cronjob :</div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('cronjob', array('type'=>'textarea', 'label' => false, 'error' => false, 'div' => false,'value'=>@$moduledata[15], 'class'=>'from-textarea'));?>
			</div>
			
		</div>

		<div class="advancefields" style="display:<?php echo $display; ?>;">
			<div class="fromnewtext">Title of The Site : </div>
			<div class="fromborderdropedown3 checkboxlist">
				<?php echo $this->Form->input('memstat', array('type'=>'checkbox', 'label' => 'Member Statistics in Overview Page', 'div' => true,'checked'=>(isset($moduledata[7])&&$moduledata[7]==1)?true:false, 'class'=>''));?>
				<?php echo $this->Form->input('notstat', array('type'=>'checkbox', 'label' => '', 'div' => false,'checked'=>false, 'class'=>''));?>
				<?php //echo $this->Form->input('notstat', array('type'=>'checkbox', 'label' => 'Notifications in Overview Page', 'div' => true,'checked'=>(isset($moduledata[9])&&$moduledata[9]==1)?true:false, 'class'=>''));?>
				<?php echo $this->Form->input('finstat', array('type'=>'checkbox', 'label' => 'Finance Statistics in Overview Page', 'div' => true,'checked'=>(isset($moduledata[8])&&$moduledata[8]==1)?true:false, 'class'=>''));?>
				<?php echo $this->Form->input('search', array('type'=>'checkbox', 'label' => 'Search Options in Member List and Massmail Page','checked'=>(isset($moduledata[5])&&$moduledata[5]==1)?true:false, 'div' => true, 'class'=>''));?>
				<?php echo $this->Form->input('position', array('type'=>'checkbox', 'label' => 'Active Positions in Member Edit Page', 'div' => true,'checked'=>(isset($moduledata[11])&&$moduledata[11]==1)?true:false, 'class'=>''));?>
				<?php echo $this->Form->input('pendingposotion', array('type'=>'checkbox', 'label' => 'Pending Positions in Member Edit Page','checked'=>(isset($moduledata[12])&&$moduledata[12]==1)?true:false, 'div' => true, 'class'=>''));?>
				<?php echo $this->Form->input('stat', array('type'=>'checkbox', 'label' => 'Stat in Member Edit Page(Back) And Member Overview(Front)','checked'=>(isset($moduledata[6])&&$moduledata[6]==1)?true:false, 'div' => true, 'class'=>''));?>
				<?php echo $this->Form->input('makeorder', array('type'=>'checkbox', 'label' => 'Add Order in Member Edit Page', 'div' => true,'checked'=>(isset($moduledata[14])&&$moduledata[14]==1)?true:false, 'class'=>''));?>
				<?php echo $this->Form->input('leaderboard', array('type'=>'checkbox', 'label' => 'Top Position Earners in Leader Board Page', 'div' => true,'checked'=>(isset($moduledata[10])&&$moduledata[10]==1)?true:false, 'class'=>''));?>
			</div>
			
		</div>

		<div class="advancefields" style="display:<?php echo $display; ?>;">
			<div class="fromnewtext">Setting Options : </div>
			<div class="fromborderdropedown3 checkboxlist">
				<?php echo $this->Form->input('withdraw', array('type'=>'checkbox', 'label' => 'Withdraw Restriction', 'div' => true,'checked'=>(isset($moduledata[16])&&$moduledata[16]==1)?true:false, 'class'=>''));?>
			</div>
			
		</div>
		
		<?php if($modulename==''){ ?>
		<div class="fromnewtext">Select SQL Patch File : <span class="red-color">*</span></div>
		<div class="fromborderdropedown3">
		  <div class="btnorange browsebutton">Browse<?php echo $this->Form->input('sqlfile', array('type'=>'file', 'label' => false, 'div' => false,'class'=>'browserbuttonlink'));?></div>
		</div>
		
		
		<div class="formborderfinance">
		<div class="fromnewtext">Site Email Template Category #ID :<span class="red-color">*</span></div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('emailcategory', array('type'=>'text', 'label' => false, 'error' => false, 'div' => false,'value'=>@$moduledata[3], 'class'=>'fromboxbg'));?>
		</div>
		
		</div>
		<div class="plussign" style="visibility: hidden;">+</div>
		<div class="formborderfinance">
		<div class="fromnewtext">Website Page Category #ID :<span class="red-color">*</span></div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('webpagecategory', array('type'=>'text', 'label' => false, 'error' => false, 'div' => false,'value'=>@$moduledata[3], 'class'=>'fromboxbg'));?>
		</div>
		</div>
		<?php } ?>
		
		<div class="formbutton">
			<?php echo $this->Form->submit('Submit', array(
				'class'=>'btnorange',
				'div'=>false
			));?>
		</div>
		
	</div>
	<?php echo $this->Form->end();?>

</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#managemodulepage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>