<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Pending Plan</div>
<div id="financepage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Pending_Purchases" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>
<div class="backgroundwhite">				
		<?php echo $this->Form->create('Pending_plan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'pendingplanaddaction')));?> 
		<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$pendingplan["Pending_plan"]["id"], 'label' => false));?>
		<?php echo $this->Form->input('processorsfee', array('type'=>'hidden', 'value'=>$processorsfee, 'label' => false));?>
		
		<div class="frommain">
			
			<?php echo $processorreturn; ?>
				
			<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_pendingplanaddaction',$SubadminAccessArray)){ ?>
			<div class="formbutton">
				<?php echo $this->Js->submit('Update', array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'class'=>'btnorange',
				  'controller'=>'finance',
				  'action'=>'pendingplanaddaction',
				  'div'=>false,
				  'url'   => array('controller' => 'finance', 'action' => 'pendingplanaddaction')
				));?>
				<?php echo $this->Js->link("Back", array('controller'=>'finance', "action"=>"pendingplan"), array(
					'update'=>'#financepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btngray'
				));?>
			</div>
		  <?php } ?>
		  
		</div>
		<?php echo $this->Form->end();?>
		
	
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#financepage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>