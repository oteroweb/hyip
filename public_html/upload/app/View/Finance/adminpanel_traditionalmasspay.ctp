<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 13-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Withdraw Request</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Withdraw Requests", array('controller'=>'finance', "action"=>"withdrawrequest"), array(
					'update'=>'#financepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Traditional Masspay", array('controller'=>'finance', "action"=>"traditionalmasspay"), array(
					'update'=>'#financepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Automatic Payments", array('controller'=>'finance', "action"=>"automaticmasspay"), array(
					'update'=>'#financepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Total Stats", array('controller'=>'finance', "action"=>"totalstats"), array(
					'update'=>'#financepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="financepage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Withdrawal_Requests#Traditional_Masspay" target="_blank">Help</a></div>
    <div id="UpdateMessage"></div>
    <?php if($show=='table'){ ?>
	<script>
	function commentbox(atag)
	{
		$("a[rel='lightboxtext']").colorbox({width:"80%", height:"80%", inline:true, href:"#"+atag.title});
	}
	</script>

    <div class="padding10">
	
	<div class="serchgreybox fontsmallonmobile"><?php echo "Search Withdraw Request By Proccessors or Download Masspay File";?></div>
	
	<div class="traditionalmasspay">
		<?php $i=0;
		foreach ($withdrawprocesser as $currentpagename):
		if($i%3==0){ echo '<div class="height10"></div><div class="textcenter">'; }
		$i++;
			if($i%2==0) $applyclass="btngray"; else $applyclass="btnorange";
			echo $this->Js->link($currentpagename['Withdraw']['payment_processor'].' Requests', array('controller'=>'finance', "action"=>"traditionalmasspay/0/".$currentpagename['Withdraw']['payment_processor']), array(
				'update'=>'#financepage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>$applyclass
			)); ?>
		&nbsp;
		<?php if($i%3==0){ echo '</div>'; }
		endforeach; ?>
	</div>
	
	
	
				
    
    <?php }else{ ?>
    
    
	<div class="greenbottomborder">
    <?php echo $this->Form->create('Withdraw',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'traditionalmasspay')));?>
    <div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php 
		echo $this->Form->checkbox('selectAllCheckboxes', array(
		  'hiddenField' => false,
		  'onclick' => 'selectAllCheckboxes("withIds",this.checked)'
		));
		?>
		<label for="WithdrawSelectAllCheckboxes"></label>
	</div>
	<div class="addnew-button">
    <?php echo $this->Js->link("Back", array('controller'=>'finance', "action"=>"traditionalmasspay"), array(
        'update'=>'#financepage',
        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
        'escape'=>false,
        'class'=>'btngray'
    ));?>
    <?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_traditionalmasspaycsv',$SubadminAccessArray)){ ?>
		<?php if(in_array($show,$processors)){ ?>
                <?php echo $this->Html->link("Download CSV File", array('controller'=>'finance', "action"=>"traditionalmasspaycsv/".$show), array(
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'class'=>'btngray',
                    'target'=>'_blank'
                ));?>
        <?php } ?>
    <?php } ?>
    <?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_cancelselectedtraditional',$SubadminAccessArray)){ ?>
		<?php
        echo $this->Js->submit('Cancel selected request(s)', array(
          'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
          'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
          'escape'=>false,
          'update'=>'#financepage',
          'class'=>'btngray',
          'div'=>false,
          'controller'=>'finance',
          'action'=>'cancelselected',
          'confirm' => 'Are You Sure You Want to Cancel Selected Request(s)?',
          'title'=>'Cancel selected request(s)',
          'url'   => array('controller' => 'finance', 'action' => 'cancelselected/0/traditionalmasspay/'.$show)
        ));
        ?>
    <?php } ?>
    <?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_markselectedtraditional',$SubadminAccessArray)){ ?>
		<?php
        echo $this->Js->submit('Mark selected as paid', array(
          'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
          'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
          'escape'=>false,
          'update'=>'#financepage',
          'class'=>'btngray',
          'div'=>false,
          'controller'=>'finance',
          'action'=>'markselectedaspaid',
          'confirm' => 'Are You Sure You Want to Mark Selected as Paid?',
          'title'=>'Mark selected as paid',
          'url'   => array('controller' => 'finance', 'action' => 'markselected/0/traditionalmasspay/'.$show)
        ));
        ?>
    <?php } ?>
    
    <?php
    $this->Paginator->options(array(
        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
        'update' => '#financepage',
        'evalScripts' => true,
        'url'=> array('controller'=>'finance', 'action'=>'traditionalmasspay/0/'.$show)
    ));
    $currentpagenumber=$this->params['paging']['Withdraw']['page'];
    ?>
    </div>
    <div class="clear-both"></div>
	
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
                    <?php 
                    if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                    echo $this->Js->link('M. Id', array('controller'=>'finance', "action"=>"traditionalmasspay/0/".$show."/member_id/".$sorttype."/".$currentpagenumber), array(
                        'update'=>'#financepage',
                        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                        'escape'=>false,
                        'class'=>'vtip',
                        'title'=>'Sort By Member Id'
                    ));?>
                </div>
                <div>
                    <?php 
                    echo $this->Js->link('Req. Dt.', array('controller'=>'finance', "action"=>"traditionalmasspay/0/".$show."/req_dt/".$sorttype."/".$currentpagenumber), array(
                        'update'=>'#financepage',
                        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                        'escape'=>false,
                        'class'=>'vtip',
                        'title'=>'Sort By Request Date'
                    ));?>
                </div>
                <div><?php echo 'Username';?></div>
                <div>
                    <?php echo $this->Js->link('Payment Processor', array('controller'=>'finance', "action"=>"traditionalmasspay/0/".$show."/payment_processor/".$sorttype."/".$currentpagenumber), array(
                        'update'=>'#financepage',
                        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                        'escape'=>false,
                        'class'=>'vtip',
                        'title'=>'Sort By Payment Processor'
                    ));?>
                </div>
                <div>
                    <?php echo $this->Js->link("Payment Processor Id", array('controller'=>'finance', "action"=>"traditionalmasspay/0/".$show."/pro_acc_id/".$sorttype."/".$currentpagenumber), array(
                        'update'=>'#financepage',
                        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                        'escape'=>false,
                        'class'=>'vtip',
                        'title'=>"Sort By Payment Processor Id"
                    ));?>
                </div>
                <div>
                    <?php echo $this->Js->link('Amount', array('controller'=>'finance', "action"=>"traditionalmasspay/0/".$show."/amount/".$sorttype."/".$currentpagenumber), array(
                        'update'=>'#financepage',
                        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                        'escape'=>false,
                        'class'=>'vtip',
                        'title'=>'Sort By Amount'
                    ));?>
                </div>
                <div>
                    <?php echo $this->Js->link('Fees', array('controller'=>'finance', "action"=>"traditionalmasspay/0/".$show."/fee/".$sorttype."/".$currentpagenumber), array(
                        'update'=>'#financepage',
                        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                        'escape'=>false,
                        'class'=>'vtip',
                        'title'=>'Sort By Fees'
                    ));?>
                </div>
                <div><?php echo 'Action';?></div>
                <div></div>
            </div>
            <?php foreach ($withdraws as $withdraw): ?>
                <div class="tablegridrow">
					<div>
						<?php 
						echo $this->Js->link($withdraw['Withdraw']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$withdraw['Withdraw']['member_id']."/top/finance/withdrawrequest~0~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
                    <div><?php echo $this->Time->format($SITECONFIG["timeformate"], $withdraw['Withdraw']['req_dt']); ?></div>
                    <div>
						<?php 
						echo $this->Js->link($withdraw['Member']['user_name'], array('controller'=>'member', "action"=>"memberadd/".$withdraw['Withdraw']['member_id']."/top/finance/withdrawrequest~0~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
                    <div>
						<?php echo $withdraw['Withdraw']['payment_processor']." "; ?>
						<?php 
						if($withdraw['Withdraw']['comment']!=''){?>
							<a href="#" rel="lightboxtext" title="inlinebox<?php echo $withdraw['Withdraw']['with_id'];?>" onclick="commentbox(this)">
								<?php echo $this->html->image('men-icon.png', array('alt'=>'Notes', 'class'=>'vtip', 'title'=>'View Comment', 'align'=>'absmiddle'));?>
							</a>
						<?php } ?>
						<div style="display:none;"><div id="inlinebox<?php echo $withdraw['Withdraw']['with_id'];?>"><?php echo $withdraw['Withdraw']['comment'];?></div></div>
					</div>
                    <div><?php echo $withdraw['Withdraw']['pro_acc_id']; ?></div>
                    <div>$<?php echo round($withdraw['Withdraw']['amount'],4); ?></div>
                    <div>$<?php echo round($withdraw['Withdraw']['fee'],4); ?></div>
                    <div>
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
											<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_markselectedtraditional',$SubadminAccessArray)){ ?>
												<li>
													<?php echo $this->Js->link($this->html->image('check.png', array('alt'=>'Mark as Paid'))." Mark as Paid", array('controller'=>'finance', "action"=>"markselected/".$withdraw['Withdraw']['with_id']."/traditionalmasspay/".$show), array(
														'update'=>'#financepage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>'vtip',
														'confirm' => 'Are You Sure You Want to Mark Selected as Paid?',
														'title'=>'Click Here if You Want to Pay This Member Manually And Not Through This System. This Will Be Marked as Paid Manually in Withdraw History And No Payment Will Be Done From The System For This Request.'
													));?>
												</li>
											<?php }?>
											
											<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_cancelselectedtraditional',$SubadminAccessArray)){ ?>
												<li>
													<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Cancel Request'))." Cancel Request", array('controller'=>'finance', "action"=>"cancelselected/".$withdraw['Withdraw']['with_id']."/traditionalmasspay/".$show), array(
														'update'=>'#financepage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>'vtip',
														'confirm' => 'Are You Sure You Want to Cancel Selected Request?',
														'title'=>'Click Here to Cancel This Request. You Can Review This Later in Withdraw History.'
													));?>
												</li>
											<?php }?>
									</ul>
								  </div>
								</div>
							
                    </div>
                    <div class="checkbox">
						<?php
						echo $this->Form->checkbox('withIds.', array(
						  'value' => $withdraw['Withdraw']['with_id'],
						  'class' => 'withIds',
						  'id'=>'withIds'.$withdraw['Withdraw']['with_id'],
						  'hiddenField' => false
						));
						?>
						<label for="<?php echo 'withIds'.$withdraw['Withdraw']['with_id']; ?>"></label>
                    </div>
                </div>
            <?php endforeach; ?>
	</div>
	<?php if(count($withdraws)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end(); ?>
    <?php
    if($this->params['paging']['Withdraw']['count']>$this->Session->read('pagerecord'))
    {?>
    <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
    <div class="floatleft margintop19">
        <?php echo $this->Form->create('Withdraw',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'traditionalmasspay/rpp/'.$show)));?>
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
        <span id="resultperpageapply" style="display:none;">
            <?php echo $this->Js->submit('Apply', array(
              'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
              'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
              'update'=>'#financepage',
              'class'=>'',
              'div'=>false,
              'controller'=>'finance',
              'action'=>'traditionalmasspay/rpp/'.$show,
              'url'   => array('controller' => 'finance', 'action' => 'traditionalmasspay/rpp/'.$show)
            ));?>
        </span>
        <?php echo $this->Form->end();?>
    </div>
    <?php }?>
    <div class="floatright">
    <ul class="nice_paging">
        <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
        <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
        <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
        <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
        <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
    </ul>
    </div>
	
    <div class="clear-both"></div>
	<div class="height10"></div>
	</div>
    
	</div>
</div>
    <?php } ?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#financepage over-->
</div>
<?php }?>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>