<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 20-11-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Withdraw History</div>
<div id="financepage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Withdrawal_History" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<!-- Search-box-start -->
<script type="text/javascript">
	<?php if($searchby=='processor_id'){ ?>
	generatecombo("data[Withdrawal_history][proc_name]","Processor","id","proc_name","<?php echo $searchfor; ?>",'#searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } ?>
</script>
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Withdrawal_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'withdrawhistory')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('searchby', array(
						      'type' => 'select',
						      'options' => array('all'=>'Select Parameter', 'amount'=>'Amount', 'receiver_email'=>'Receiver Email', 'receiver_id'=>'Receiver Id', 'user_name'=>'Username', 'processor_id'=>'Payment Processor'),
						      'selected' => $searchby,
						      'class'=>'',
						      'label' => false,
						      'style' => '',
									  'onchange'=>'if($(this).val()=="processor_id"){generatecombo("data[Withdrawal_history][proc_name]","Processor","id","proc_name","'.$searchfor.'","#searchforcombo","'.$ADMINURL.'","");$("#searchforall").hide();$("#searchforcombo").show(500);}else{$("#searchforcombo").hide();$("#searchforall").show(500);}'
						));
						?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span id='searchforall' style='display: <?php if($searchby=='processor_id'){ echo 'none'; } ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span id='searchforcombo' style='display: <?php if($searchby!='processor_id'){ echo 'none'; } ?>'></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#financepage',
					'class'=>'searchbtn',
					'controller'=>'finance',
					'action'=>'withdrawhistory',
					'url'=> array('controller' => 'finance', 'action' => 'withdrawhistory')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#financepage',
		'evalScripts' => true,
		'url'=> array('controller'=>'finance', 'action'=>'withdrawhistory')
	));
	$currentpagenumber=$this->params['paging']['Withdrawal_history']['page'];
	?>
<div id="gride-bg" class="noborder">
    <div class="padding10">	
	<?php echo $this->Form->create('Withdrawal_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'withdrawhistoryremove')));?>
	
	<div class="greenbottomborder">
	<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php 
		echo $this->Form->checkbox('selectAllCheckboxes', array(
		  'hiddenField' => false,
		  'onclick' => 'selectAllCheckboxes("withSerialIds",this.checked)'
		));
		?>
		<label for="Withdrawal_historySelectAllCheckboxes"></label>
	</div>
	<div class="addnew-button massactionbox">
		<div class="actionmenu">
			<div class="btn-group">
				<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
				  Action <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_withdrawhistorycsv',$SubadminAccessArray)){ ?>
					<li>
						<?php echo $this->Html->link("Download CSV File", array('controller'=>'finance', "action"=>"withdrawhistorycsv"), array(
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'',
							'target'=>'_blank'
						));?>
					</li>
				<?php } ?>
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_withdrawhistoryremove',$SubadminAccessArray)){?>
				<li>
					<?php echo $this->Js->submit('Delete Selected Record(s)', array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'escape'=>false,
					  'update'=>'#financepage',
					  'class'=>'massactionbtn',
					  'div'=>false,
					  'controller'=>'finance',
					  'action'=>'withdrawhistoryremove',
					  'confirm' => 'Are You Sure?',
					  'url'   => array('controller' => 'finance', 'action' => 'withdrawhistoryremove')
					));?>
				</li>
				<?php }?>
			  </ul>
			</div>
		</div>
	</div>
	<div class="clear-both"></div>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('R. Id', array('controller'=>'finance', "action"=>"withdrawhistory/0/0/receiver_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Receiver Id'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Pay. Date', array('controller'=>'finance', "action"=>"withdrawhistory/0/0/dt/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Payment Date'
					));?>
				</div>
				<div>Username</div>
				<div>
					<?php echo $this->Js->link("Amount", array('controller'=>'finance', "action"=>"withdrawhistory/0/0/amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>"Sort By Amount"
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Sender', array('controller'=>'finance', "action"=>"withdrawhistory/0/0/sender_email/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Sender'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Receiver', array('controller'=>'finance', "action"=>"withdrawhistory/0/0/receiver_email/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Receiver'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Status', array('controller'=>'finance', "action"=>"withdrawhistory/0/0/return_desc/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Status'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Ref. No.', array('controller'=>'finance', "action"=>"withdrawhistory/0/0/refrence_no/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Reference No.'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Processor', array('controller'=>'finance', "action"=>"withdrawhistory/0/0/processor/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Processor'
					));?>
				</div>
                <div></div>
			</div>
			<?php foreach ($withdrawal_historys as $withdrawal_history): ?>
				<div class="tablegridrow">
					<div>
						<?php 
						echo $this->Js->link($withdrawal_history['Withdrawal_history']['receiver_id'], array('controller'=>'member', "action"=>"memberadd/".$withdrawal_history['Withdrawal_history']['receiver_id']."/top/finance/withdrawhistory~0~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
                    <div><?php echo $this->Time->format($SITECONFIG["timeformate"], $withdrawal_history['Withdrawal_history']['dt']); ?></div>
					<div>
						<?php 
						echo $this->Js->link($withdrawal_history['Member']['user_name'], array('controller'=>'member', "action"=>"memberadd/".$withdrawal_history['Withdrawal_history']['receiver_id']."/top/finance/withdrawhistory~0~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>$<?php echo round($withdrawal_history['Withdrawal_history']['amount'],4); ?></div>
					<div><?php echo $withdrawal_history['Withdrawal_history']['sender_email']; ?></div>
                    <div>
						<?php 
						echo $this->Js->link($withdrawal_history['Withdrawal_history']['receiver_email'], array('controller'=>'member', "action"=>"memberadd/".$withdrawal_history['Withdrawal_history']['receiver_id']."/top/finance/withdrawhistory~0~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div><?php echo $withdrawal_history['Withdrawal_history']['return_desc']; ?></div>
					<div><?php echo $withdrawal_history['Withdrawal_history']['refrence_no']; ?></div>
                    <div><?php echo $withdrawal_history['Withdrawal_history']['processor']; ?></div>
                    <div class="checkbox">
						<?php
						echo $this->Form->checkbox('withSerialIds.', array(
								'value' => $withdrawal_history['Withdrawal_history']['id'],
								  'class' => 'withSerialIds',
								  'id'=>'withSerialIds'.$withdrawal_history['Withdrawal_history']['id'],
								  'hiddenField' => false
						));
						?>
						<label for="<?php echo 'withSerialIds'.$withdrawal_history['Withdrawal_history']['id']; ?>"></label>
                     </div>
				</div>
			<?php endforeach; ?>
	</div>
	
	<?php if(count($withdrawal_historys)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
    <?php echo $this->Form->end();
	if($this->params['paging']['Withdrawal_history']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Withdrawal_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'withdrawhistory/rpp')));?>
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#financepage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'finance',
			  'action'=>'withdrawhistory/rpp',
			  'url'   => array('controller' => 'finance', 'action' => 'withdrawhistory/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#financepage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>