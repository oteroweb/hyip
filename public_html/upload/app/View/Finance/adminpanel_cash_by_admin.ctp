<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Cash By Admin</div>
<div id="financepage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>		
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Cash_By_Admin" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<script type="text/javascript">
	<?php if($searchby=='processor'){ ?>
	generatecombo("data[Cash_by_admin][proc_name]","Processor","proc_name","proc_name","<?php echo $searchfor; ?>",'#searchforpro',"<?php echo $ADMINURL;?>","");
	<?php } ?>		
</script>
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Cash_by_admin',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'cash_by_admin')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
						if($SITECONFIG["balance_type"]==1)
							$options=array('all'=>'Select Parameter', 'member_id'=>'Member Id', 'member_username'=>'Member Username', 'added_type'=>'Type', 'added_only'=>'Added Only', 'deducted_only'=>'Deducted Only');
						else
							$options=array('all'=>'Select Parameter', 'member_id'=>'Member Id', 'member_username'=>'Member Username',  'added_type'=>'Type', 'processor'=>'Payment Processor', 'added_balance'=>'Amount', 'added_only'=>'Added Only', 'deducted_only'=>'Deducted Only');
						
						echo $this->Form->input('searchby', array(
						      'type' => 'select',
						      'options' => $options,
						      'selected' => $searchby,
						      'class'=>'',
						      'label' => false,
						      'style' => '',
							  'onchange'=>'if($(this).val()=="deducted_only" || $(this).val()=="added_only"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);} if($(this).val()=="processor"){generatecombo("data[Cash_by_admin][proc_name]","Processor","proc_name","proc_name","'.$searchfor.'","#searchforpro","'.$ADMINURL.'","");$("#searchall").hide();$("#searchfortype").hide();$("#searchforpro").show(500);}else if($(this).val()=="added_type"){$("#searchforpro").hide();$("#searchall").hide();$("#searchfortype").show(500);} else{$("#searchforpro").hide();$("#searchfortype").hide();$("#searchall").show(500);}'
						));
						?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=="deducted_only" || $searchby=="added_only"){ echo 'style="display:none"';} ?>>
			<span>Search For :</span>
			<span class="searchforfields_s" id='searchall' style='display:<?php if($searchby=="processor" || $searchby=="added_type"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span id='searchforpro' style='display:<?php if($searchby!="processor"){ echo "none";} ?>' class='marginleft9'></span>
			<span id='searchfortype' style='display:<?php if($searchby!="added_type"){ echo "none";} ?>' class='marginleft9'>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
						<?php 
							echo $this->Form->input('added_type', array(
								'type' => 'select',
								'options' => array('cash'=>'Cash', 'repurchase'=>'Purchase', 'earning'=>'Earning', 'commission'=>'Commission'),
								'class'=>'',
								'selected' => $searchfor,
								'label' => false,
								'div' => false,
								'style' => ''
							));
						?>
						</label>
					</div>
				</div>	
			</span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#financepage',
					'class'=>'searchbtn',
					'controller'=>'finance',
					'action'=>'cash_by_admin',
					'url'=> array('controller' => 'finance', 'action' => 'cash_by_admin')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->

	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#financepage',
		'evalScripts' => true,
		'url'=> array('controller'=>'finance', 'action'=>'cash_by_admin')
	));
	$currentpagenumber=$this->params['paging']['Cash_by_admin']['page'];
	?>
<div id="gride-bg" class="noborder">
    <div class="padding10">
	
	<div class="greenbottomborder">
	<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
    <?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_cash_by_adminadd',$SubadminAccessArray)){ ?>
    	<?php echo $this->Js->link("+ Add New", array('controller'=>'finance', "action"=>"cash_by_adminadd"), array(
			'update'=>'#financepage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btnorange'
		));?>
	<?php } ?>
    <?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_cashbyadmincsv',$SubadminAccessArray)){ ?>
    	<?php echo $this->Html->link("Download CSV File", array('controller'=>'finance', "action"=>"cashbyadmincsv"), array(
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btngray',
			'target'=>'_blank'
		));?>
	<?php } ?>
	</div>
	<div class="clear-both"></div>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'finance', "action"=>"cash_by_admin/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Date', array('controller'=>'finance', "action"=>"cash_by_admin/0/0/added_date/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Date'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Member', array('controller'=>'finance', "action"=>"cash_by_admin/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link("Amount", array('controller'=>'finance', "action"=>"cash_by_admin/0/0/added_balance/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>"Sort By Amount"
					));?>
				</div>
                <?php if($SITECONFIG["balance_type"]==2){ ?>
                    <div>
                        <?php echo $this->Js->link('Payment Processor', array('controller'=>'finance', "action"=>"cash_by_admin/0/0/processor/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#financepage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Payment Processor'
                        ));?>
                    </div>
                <?php } ?>
				<div>
					<?php echo $this->Js->link('Type', array('controller'=>'finance', "action"=>"cash_by_admin/0/0/added_type/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Type'
					));?>
				</div>
                <div><?php echo 'Description';?></div>
			</div>
			<?php foreach ($cash_by_admins as $cash_by_admin):
				if($cash_by_admin['Cash_by_admin']['description']=='') $cash_by_admin['Cash_by_admin']['description']='N/A';
				?>
				<div class="tablegridrow">
					<div><?php echo $cash_by_admin['Cash_by_admin']['id']; ?></div>
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $cash_by_admin['Cash_by_admin']['added_date']); ?></div>
					<div>
						<?php 
						echo $this->Js->link($cash_by_admin['Member']['user_name'].'('.$cash_by_admin['Cash_by_admin']['member_id'].')', array('controller'=>'member', "action"=>"memberadd/".$cash_by_admin['Cash_by_admin']['member_id']."/top/finance/cash_by_admin~0~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
                    <div>$<?php echo round($cash_by_admin['Cash_by_admin']['added_balance'],4); ?></div>
                    <?php if($SITECONFIG["balance_type"]==2){ ?>
                    <div><?php echo $cash_by_admin['Cash_by_admin']['processor']; ?></div>
                    <?php } ?>
                    <div><?php if($cash_by_admin['Cash_by_admin']['added_type']=='repurchase') echo 'Re-purchase'; else echo ucwords($cash_by_admin['Cash_by_admin']['added_type']); ?></div>
					<div><?php echo $this->html->image('information.png', array('alt'=>'Description', 'class'=>'vtip', 'title'=>nl2br(stripslashes($cash_by_admin['Cash_by_admin']['description']))));?></div>
				</div>
			<?php endforeach; ?>
				
	</div>
	
	<?php if(count($cash_by_admins)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
    <?php
	if($this->params['paging']['Cash_by_admin']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Cash_by_admin',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'cash_by_admin/rpp')));?>
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#financepage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'finance',
			  'action'=>'cash_by_admin/rpp',
			  'url'   => array('controller' => 'finance', 'action' => 'cash_by_admin/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#financepage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>