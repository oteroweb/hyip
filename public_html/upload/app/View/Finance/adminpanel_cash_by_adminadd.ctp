<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Cash By Admin</div>
<div id="financepage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Cash_By_Admin" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="backgroundwhite">
		
<?php echo $this->Form->create('Cash_by_admin',array('type' => 'post', 'id'=>'FinanceForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'cash_by_adminaddaction')));
echo $this->Form->input('balance_type', array('type'=>'hidden', 'value'=>$SITECONFIG["balance_type"], 'label' => false));?>

    <div class="frommain">
		
		<div class="fromnewtext">Member :<span class="red-color">*</span></div>
		<div class="fromborderdropedown3">
		   <div class="select-main2">
		     <div class="select-main">
				<label>
				  <?php 
						echo $this->Form->input('member_select_by', array(
							'type' => 'select',
							'options' => array('member_id'=>'Member Id', 'user_name'=>"Member Username"),
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));
					?>
				</label>
			 </div>
		   </div>
		   <?php echo $this->Form->input('select_value', array('type'=>'text', 'label' => false, 'class'=>'fromboxbghalf borderwidth', 'div' => false));?>
		</div>
		
		<div class="fromnewtext">Type :</div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
				  <?php
						$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning', 'commission'=>'Commission');
						if($SITECONFIG["wallet_for_commission"] == 'cash' && $SITECONFIG["wallet_for_earning"] == 'cash')
						{
							$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase');
						}
						elseif($SITECONFIG["wallet_for_earning"] == 'cash')
						{
							$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'commission'=>'Commission');
						}
						elseif($SITECONFIG["wallet_for_commission"] == 'cash')
						{
							$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning');
						}
						echo $this->Form->input('added_type', array(
							'type' => 'select',
							'options' => $pmethod,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));
					?>
				</label>
			</div>
        </div>
		
		<div class="fromnewtext">Action :<span class="red-color">*</span></div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
				   <?php 
					  echo $this->Form->input('add_or_deduct', array(
						  'type' => 'select',
						  'options' => array('+'=>'Add', '-'=>'Deduct'),
						  'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
					  ));
					?>
				</label>
			</div>
        </div>
		
		<?php if($SITECONFIG["balance_type"]==2){ ?>   
		<div class="fromnewtext">Payment Processor :<span class="red-color">*</span></div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
				   <?php 
						echo $this->Form->input('processor', array(
							'type' => 'select',
							'options' => $processors,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));
                    ?>
				</label>
			</div>
        </div>
		<?php } ?>
		
		<div class="fromnewtext">Amount ($) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('added_balance', array('type'=>'text','label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Description :</div>
		<div class="frombordermain">
			<?php echo $this->Form->input('description', array('type'=>'textarea', 'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
		</div>
		
		<div class="formbutton">
				<?php echo $this->Js->submit('Submit', array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'class'=>'btnorange',
				  'div'=>false,
				  'controller'=>'finance',
				  'action'=>'cash_by_adminaddaction',
				  'url'   => array('controller' => 'finance', 'action' => 'cash_by_adminaddaction')
				));?>
				<?php echo $this->Js->link("Back", array('controller'=>'finance', "action"=>"cash_by_admin"), array(
					'update'=>'#financepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btngray'
				));?>
		</div>
	</div>  
	<?php echo $this->Form->end();?>
	
</div>	
<?php if(!$ajax){?>
</div><!--#financepage over-->
<?php }?>		
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>