<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 13-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Pending Free Plans</div>
<div id="financepage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Pending_Free_Plans" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<script type="text/javascript">
	<?php if($searchby=='free_plan_type'){ ?>
	generatecombo("data[Pending_free_plan][free_plan_type]","Pending_free_plan","free_plan_type","free_plan_type","<?php echo $searchfor; ?>",'#searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } ?>
</script>
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Pending_free_plan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'pendingfreeplan')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('searchby', array(
						      'type' => 'select',
						      'options' => array('all'=>'Select Parameter', 'member_id'=>'Member Id', 'free_plan_type'=>'Plan Type'),
						      'selected' => $searchby,
						      'class'=>'',
						      'label' => false,
						      'style' => '',
									  'onchange'=>'if($(this).val()=="free_plan_type"){generatecombo("data[Pending_free_plan][free_plan_type]","Pending_free_plan","free_plan_type","free_plan_type","'.$searchfor.'","#searchforcombo","'.$ADMINURL.'","");$("#searchall").hide();$("#searchforcombo").show(500);} else{$("#searchforcombo").hide();$("#searchall").show(500);}'
						));
						?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span id='searchall' style='display:<?php if($searchby=="free_plan_type"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span id='searchforcombo' style='display:<?php if($searchby!="free_plan_type"){ echo "none";} ?>'></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#financepage',
					'class'=>'searchbtn',
					'controller'=>'finance',
					'action'=>'pendingfreeplan',
					'url'=> array('controller' => 'finance', 'action' => 'pendingfreeplan')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#financepage',
		'evalScripts' => true,
		'url'=> array('controller'=>'finance', 'action'=>'pendingfreeplan')
	));
	$currentpagenumber=$this->params['paging']['Pending_free_plan']['page'];
	?>
<div id="gride-bg" class="noborder">
    <div class="padding10">	
	<?php echo $this->Form->create('Pending_free_plan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'pendingfreeplan')));?>
	<div class="greenbottomborder">
	<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php 
		echo $this->Form->checkbox('selectAllCheckboxes', array(
		  'hiddenField' => false,
		  'onclick' => 'selectAllCheckboxes("pendingIds",this.checked)'
		));
		?>
		<label for="Pending_free_planSelectAllCheckboxes"></label>
	</div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_pendingfreeplanremove',$SubadminAccessArray)){
		echo $this->Js->submit('Delete Selected Record(s)', array(
		  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		  'escape'=>false,
		  'update'=>'#financepage',
		  'class'=>'btngray',
		  'div'=>false,
		  'controller'=>'finance',
		  'action'=>'pendingfreeplanremove',
		  'confirm' => 'Are You Sure?',
		  'url'   => array('controller' => 'finance', 'action' => 'pendingfreeplanremove')
		));
	}?>
	</div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'finance', "action"=>"pendingfreeplan/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('M. Id', array('controller'=>'finance', "action"=>"pendingfreeplan/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Purchase Date', array('controller'=>'finance', "action"=>"pendingfreeplan/0/0/purchase_date/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Purchase Date'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Plan Type', array('controller'=>'finance', "action"=>"pendingfreeplan/0/0/free_plan_type/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Type'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Plan ID', array('controller'=>'finance', "action"=>"pendingfreeplan/0/0/free_plan_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan ID'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('From Plan', array('controller'=>'finance', "action"=>"pendingfreeplan/0/0/purchased_plan_type/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By From Plan'
					));?>
				</div>
                <div></div>
			</div>
			<?php foreach ($pendingfreeplans as $pendingfreeplan): ?>
				<div class="tablegridrow">
					<div>#<?php echo $pendingfreeplan['Pending_free_plan']['id']; ?></div>
					<div>
						<?php
						echo $this->Js->link($pendingfreeplan['Pending_free_plan']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$pendingfreeplan['Pending_free_plan']['member_id']."/top/finance/pendingfreeplan~0~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
                    <div><?php echo $this->Time->format($SITECONFIG["timeformate"], $pendingfreeplan['Pending_free_plan']['purchase_date']); ?></div>
					<div><?php if($pendingfreeplan['Pending_free_plan']['free_plan_type']=='ppc') echo 'PPC'; elseif($pendingfreeplan['Pending_free_plan']['free_plan_type']=='ptc') echo 'PTC'; else echo ucwords($pendingfreeplan['Pending_free_plan']['free_plan_type']); ?></div>
					<div><?php echo $pendingfreeplan['Pending_free_plan']['free_plan_id']; ?></div>
                    <div><?php echo $this->html->image('information.png', array('alt'=>'Notes', 'class'=>'vtip', 'title'=>$pendingfreeplan['Pending_free_plan']['purchased_plan_type']." #".$pendingfreeplan['Pending_free_plan']['purchased_plan_id']));?></div>
					<div class="checkbox">
						<?php echo $this->Form->checkbox('pendingIds.', array(
						  'value' => $pendingfreeplan['Pending_free_plan']['id'],
						  'class' => 'pendingIds',
						  'id'=>'pendingIds'.$pendingfreeplan['Pending_free_plan']['id'],
						  'hiddenField' => false
						));?>
						<label for="<?php echo 'pendingIds'.$pendingfreeplan['Pending_free_plan']['id']; ?>"></label>
                    </div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($pendingfreeplans)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
    <?php echo $this->Form->end();
	if($this->params['paging']['Pending_free_plan']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Pending_free_plan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'pendingfreeplan/rpp')));?>
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#financepage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'finance',
			  'action'=>'pendingfreeplan/rpp',
			  'url'   => array('controller' => 'finance', 'action' => 'pendingfreeplan/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#financepage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>