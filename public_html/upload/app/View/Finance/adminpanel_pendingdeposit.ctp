<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 10-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Pending Deposits</div>
<div id="financepage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Pending_Deposits" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<script type="text/javascript">
	<?php if($searchby=='processorid'){ ?>
	generatecombo("data[Pending_deposit][proc_name]","Processor","id","proc_name","<?php echo $searchfor; ?>",'#searchforpro',"<?php echo $ADMINURL;?>","");
	<?php } ?>
</script>
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Pending_deposit',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'pendingdeposit')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('searchby', array(
									'type' => 'select',
									'options' => array('all'=>'Select Parameter', 'member_id'=>'Member Id', 'amount'=>'Amount', 'fees'=>'Fees', 'paidamount'=>'Total', 'processorid'=>'Payment Processor', 'ip_address'=>'IP Address'),
									'selected' => $searchby,
									'class'=>'',
									'label' => false,
									'style' => '',
									'onchange'=>'if($(this).val()=="processorid"){generatecombo("data[Pending_deposit][proc_name]","Processor","id","proc_name","'.$searchfor.'","#searchforpro","'.$ADMINURL.'","");$("#searchall").hide();$("#searchforpro").show(500);}else{$("#searchforpro").hide();$("#searchall").show(500);}'
						));
						?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span class="searchforfields_s" id='searchall' style='display:<?php if($searchby=="processorid"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span id='searchforpro' style='display:<?php if($searchby!="processorid"){ echo "none";} ?>'></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#financepage',
				'class'=>'searchbtn',
				'controller'=>'finance',
				'action'=>'pendingdeposit',
				'url'=> array('controller' => 'finance', 'action' => 'pendingdeposit')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->

	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#financepage',
		'evalScripts' => true,
		'url'=> array('controller'=>'finance', 'action'=>'pendingdeposit')
	));
	$currentpagenumber=$this->params['paging']['Pending_deposit']['page'];
	?>
	
<div id="gride-bg" class="noborder">
    <div class="padding10">
				
	<?php echo $this->Form->create('Pending_deposit',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'pendingdeposit')));?>
	
	<div class="greenbottomborder">
	<div class="height10"></div>
	<div class="paginator-text paddingtop0"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php 
		echo $this->Form->checkbox('selectAllCheckboxes', array(
		  'hiddenField' => false,
		  'onclick' => 'selectAllCheckboxes("pendingIds",this.checked)',
		  'id'=>'pendingIdAll'
		));
		?>
		<label for="pendingIdAll"></label>
	</div>
	<div class="addnew-button massactionbox">
		<div class="actionmenu">
			<div class="btn-group">
				<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
				  Action <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_pendingdepositcsv',$SubadminAccessArray)){ ?>
					<li>
					<?php echo $this->Html->link("Download CSV File", array('controller'=>'finance', "action"=>"pendingdepositcsv"), array(
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'',
						'target'=>'_blank'
					));?>
					</li>
				<?php } ?>
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_pendingdepositpay',$SubadminAccessArray)){?>
					<li>
					<?php echo $this->Js->submit('Mark Selected Record(s) as Paid', array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'escape'=>false,
					  'update'=>'#financepage',
					  'class'=>'massactionbtn',
					  'div'=>false,
					  'controller'=>'finance',
					  'action'=>'pendingdepositpay',
					  'confirm' => 'Are You Sure? Record(s) Will Be Deleted From Here And Added as Successful Payment(s)',
					  'url'   => array('controller' => 'finance', 'action' => 'pendingdepositpay')
					));?>
					</li>
				<?php }?>
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_pendingdepositremove',$SubadminAccessArray)){?>
					<li>
					<?php echo $this->Js->submit('Delete Selected Record(s)', array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'escape'=>false,
					  'update'=>'#financepage',
					  'class'=>'massactionbtn',
					  'div'=>false,
					  'controller'=>'finance',
					  'action'=>'pendingdepositremove',
					  'confirm' => 'Are You Sure?',
					  'url'   => array('controller' => 'finance', 'action' => 'pendingdepositremove')
					));?>
					</li>
				<?php }?>
			  </ul>
			</div>
		</div>
	</div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('M. Id', array('controller'=>'finance', "action"=>"pendingdeposit/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Pay. Date', array('controller'=>'finance', "action"=>"pendingdeposit/0/0/payment_date/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Payment Date'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Amount', array('controller'=>'finance', "action"=>"pendingdeposit/0/0/amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Amount'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Fees', array('controller'=>'finance', "action"=>"pendingdeposit/0/0/fees/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Fees'
					));?>
				</div>
		<div>
					<?php echo $this->Js->link('Total', array('controller'=>'finance', "action"=>"pendingdeposit/0/0/paidamount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Total'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Processor', array('controller'=>'finance', "action"=>"pendingdeposit/0/0/processor/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Processor'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('IP Address', array('controller'=>'finance', "action"=>"pendingdeposit/0/0/ip_address/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By IP Address'
					));?>
				</div>
                <div><?php echo 'Notes';?></div>
                <div></div>
			</div>
			<?php $i=0;foreach ($pendingdeposits as $pendingdeposit): $i++;?>
				<div class="tablegridrow">
					<div>
						<?php 
						echo $this->Js->link($pendingdeposit['Pending_deposit']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$pendingdeposit['Pending_deposit']['member_id']."/top/finance/pendingdeposit~0~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
                    <div><?php echo $this->Time->format($SITECONFIG["timeformate"], $pendingdeposit['Pending_deposit']['payment_date']); ?></div>
                    <div>$<?php echo round($pendingdeposit['Pending_deposit']['amount'],4); ?></div>
                    <div>$<?php echo round($pendingdeposit['Pending_deposit']['fees'],4); ?></div>
					<div>$<?php echo round($pendingdeposit['Pending_deposit']['paidamount'],4); ?></div>
                    <div>
						<?php echo $pendingdeposit['Pending_deposit']['processor']; ?>
						<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_pendingdepositadd',$SubadminAccessArray)){ ?>
							<?php 
							if($pendingdeposit['Pending_deposit']['comment']!=''){
							echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Bank Wire',  'align'=>'absmiddle')), array('controller'=>'finance', "action"=>"pendingdepositadd/".$pendingdeposit['Pending_deposit']['id']), array(
								'update'=>'#financepage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'Edit Details'
							));
							}?>
						<?php } ?>
					</div>
                    <div><?php echo $pendingdeposit['Pending_deposit']['ip_address']; ?></div>
                    <?php if($pendingdeposit['Pending_deposit']['notes']==''){$pendingdeposit['Pending_deposit']['notes']='N/A';} ?>
                    <div><?php echo $this->html->image('information.png', array('alt'=>'Notes', 'class'=>'vtip', 'title'=>$pendingdeposit['Pending_deposit']['notes']));?></div>
                    <div>
						<span class="checkbox">
						<?php
						echo $this->Form->checkbox('pendingIds.', array(
						  'value' => $pendingdeposit['Pending_deposit']['id'],
						  'class' => 'pendingIds',
						  'hiddenField' => false,
						  'id'=>'pendingIds'.$i
						));
						?>
						<label for="pendingIds<?php echo $i;?>"></label>
						</span>
                    </div>
				</div>
			<?php endforeach; ?>
				
	</div>
	<?php if(count($pendingdeposits)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
    <?php echo $this->Form->end();
	if($this->params['paging']['Pending_deposit']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Pending_deposit',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'pendingdeposit/rpp')));?>
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#financepage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'finance',
			  'action'=>'pendingdeposit/rpp',
			  'url'   => array('controller' => 'finance', 'action' => 'pendingdeposit/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
	
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>

<?php if(!$ajax){?>
</div><!--#financepage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>