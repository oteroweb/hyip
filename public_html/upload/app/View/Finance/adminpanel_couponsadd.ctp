<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 13-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Coupons</div>
<div class="height10"></div>
<div id="UpdateMessage"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Coupons", array('controller'=>'finance', "action"=>"coupons"), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Coupons History", array('controller'=>'finance', "action"=>"couponshistory"), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">
<div id="financepage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Coupons#Coupons" target="_blank">Help</a></div>
<?php echo $this->Form->create('Coupon',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'couponsaddaction')));?>
<?php if(isset($coupondata['Coupon']['id'])){
	echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$coupondata['Coupon']['id'], 'label' => false));
	echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
}
else
{
	echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'add', 'label' => false));
}
?>
	<div class="frommain">
	
		<div class="fromnewtext">Status : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('status', array(
						'type' => 'select',
						'options' => array('1'=>'Active', '0'=>'Inactive'),
						'selected' => $coupondata['Coupon']["status"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		
		
		<div class="fromnewtext">Title :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('title', array('type'=>'text', 'value'=>$coupondata['Coupon']["title"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Description :<span class="red-color">*</span> </div>
		<div class="frombordermain">
			<?php echo $this->Form->input('description', array('type'=>'textarea', 'value'=>stripslashes($coupondata['Coupon']["description"]), 'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
		</div>
		
		<div class="fromnewtext">Code :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('code', array('type'=>'text', 'value'=>$coupondata['Coupon']["code"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Type :<span class="red-color">*</span></div>
		<div class="fromborderdropedown5">
			<div class="select-main">
				<label>
					<?php 
						echo $this->Form->input('coupontype', array(
						  'type' => 'select',
						  'options' => array("Discount"=>array('1'=>'One Time', '2'=>"Unlimited", '3'=>"Multiple Times"), "Free Cash"=>array( '4'=>"Multiple Times",'5'=>"Unlimited")),
						  'class'=>'',
						  'selected' => $coupondata['Coupon']['coupontype'],
						  'label' => false,
						  'div' => false,
						  'style' => '',
						  'onchange' => 'if(this.selectedIndex==2 || this.selectedIndex==3) {$(".coupontype_fields").show(500);} else{$(".coupontype_fields").hide(500);} if(this.selectedIndex==3 || this.selectedIndex==4){ $("#CouponMaximumdiscount, #CouponMinimumamount").val(0); $(".amounttypefields, .maximumdiscountfield, .minimumamountfields").hide(500); }else{if($("#CouponAmounttype").prop("selectedIndex")==0) { $(".maximumdiscountfield").hide(500); $(".amounttypefields, .minimumamountfields").show(500); } else { $(".amounttypefields, .maximumdiscountfield, .minimumamountfields").show(500); } }'
						));
					?>
				</label>
			</div>
		</div>
		<div class="fromborderdropedown5 coupontype_fields" style="display:<?php if($coupondata['Coupon']['coupontype']==3 || $coupondata['Coupon']['coupontype']==4){echo '';}else{echo 'none';}?>;">
			<div class="daystexthaf">How? :</div>
			<?php echo $this->Form->input('coupontime', array('type'=>'text', 'value'=>$coupondata['Coupon']["coupontime"], 'label' => false, 'class'=>'fromboxbghalf borderwidth ', 'div' => false));?>
		</div>
		<span data-original-title="Here You Can Select The Type of Coupon. Decide Whether It Can Be Used Only Once or Multiple Times or Unlimited Times. Two Categories Are There. First is Discount Which Can Be Used By Members When They Are Purchasing Something From The Site Like Positions, Shares, Paying Membership Fees, etc. And Second is Free Cash Which Members Can Use in Their Profile Page to Increase Their Cash Balance" data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="fromnewtext">Amount :<span class="red-color">*</span></div>
		<div class="fromborderdropedown5 amounttypefields" style="display:<?php if($coupondata['Coupon']['coupontype']!=4 && $coupondata['Coupon']['coupontype']!=5){echo '';}else{echo 'none';}?>;">
			<div class="select-main">
				<label>
					<?php 
						echo $this->Form->input('amounttype', array(
						'type' => 'select',
						'options' => array('0'=>'Dollar($)', '1'=>"Percentage(%)"),
						'class'=>'',
						'selected' => $coupondata['Coupon']['amounttype'],
						'label' => false,
						'div' => false,
						'style' => '',
						'onchange' => ' if(this.selectedIndex==1){$(".maximumdiscountfield").show(500);}else{$("#CouponMaximumdiscount").val(0); $(".maximumdiscountfield").hide(500);}'
						));
					?>
				</label>
			</div>
		</div>
		<div class="fromborderdropedown5">
			<div class="daystexthaf">Rate :</div>
			<?php echo $this->Form->input('amount', array('type'=>'text', 'value'=>$coupondata['Coupon']["amount"], 'div' => false, 'label' => false, 'class'=>'fromboxbghalf borderwidth'));?>
		</div>
		<span data-original-title="Minimum Purchase Amount Should be Equal to or More Than The Amount When You Select Dollar Here. And in Case of Percentage, Make Sure That The Value is Set to Be Less Than 100." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="maximumdiscountfield"  style="display:<?php if($coupondata['Coupon']['amounttype']==0 || $coupondata['Coupon']['coupontype']==4){echo 'none';}else{echo '';}?>;">
			<div class="fromnewtext">Maximum Discount($) :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('maximumdiscount', array('type'=>'text', 'value'=>($coupondata['Coupon']["maximumdiscount"])?$coupondata['Coupon']["maximumdiscount"]:0, 'label' => false, 'div'=>false, 'class'=>'fromboxbg'));?>
			</div>
			
		</div>

		<div class="minimumamountfields" style="display:<?php if($coupondata['Coupon']['coupontype']!=4 && $coupondata['Coupon']['coupontype']!=5){echo '';}else{echo 'none';}?>;">
			<div class="fromnewtext">Minimum Purchase Amount($) :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('minimumamount', array('type'=>'text', 'value'=>$coupondata['Coupon']["minimumamount"], 'label' => false, 'div' => false,  'class'=>'fromboxbg'));?>
			</div>
			
		</div>
		
		<div class="fromnewtext">Validity :</div>
		<?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$coupondata['Coupon']['fromdate']=='0000-00-00'?'':$coupondata['Coupon']['fromdate'], 'label' => false, 'div' => false, 'class'=>'fromboxbgcal2 datepicker', 'style' => ';'));?>
		<?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$coupondata['Coupon']['todate']=='0000-00-00'?'':$coupondata['Coupon']['todate'], 'label' => false, 'div' => false, 'class'=>'fromboxbgcal2 datepicker', 'style' => ';'));?>
		<span data-original-title="Specify The Validity of The Coupon By Selecting The From And To Date To Mark Starting and Expiry Date Respectively." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign height39"> </span>
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'btnorange',
				'div'=>false,
				'controller'=>'finance',
				'action'=>'couponsaddaction',
				'url'   => array('controller' => 'finance', 'action' => 'couponsaddaction')
			  ));?>
			  <?php echo $this->Js->link("Back", array('controller'=>'finance', "action"=>"coupons"), array(
				  'update'=>'#financepage',
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'escape'=>false,
				  'class'=>'btngray'
			  ));?>
		</div>
		
	</div>
	<?php echo $this->Form->end();?>

<?php if(!$ajax){?>
</div><!--#financepage over-->
</div>
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>