<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 13-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Commission</div>
<div id="financepage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Commission" target="_blank">Help</a></div>
    <div id="UpdateMessage"></div>
	
<!-- Search-box-start -->
<script type="text/javascript">
	<?php if($searchby=='description'){ ?>
	generatecombo("data[Commission][description]","Commission","description","description","<?php echo $searchfor; ?>",'#searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } ?>
</script>
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Commission',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'commission')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
					    <?php 
					    echo $this->Form->input('searchby', array(
						  'type' => 'select',
									  'options' => array('all'=>'Select Parameter', 'from_id'=>'From Member Id', 'to_id'=>'To Member Id', 'from_member'=>'From Member', 'to_member'=>'To Member','description'=>'Type'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => '',
									  'onchange'=>'if($(this).val()=="description"){generatecombo("data[Commission][description]","Commission","description","description","'.$searchfor.'","#searchforcombo","'.$ADMINURL.'","");$("#searchforall").hide();$("#searchforcombo").show(500);}else{$("#searchforcombo").hide();$("#searchforall").show(500);}'
					    ));
					    ?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span id='searchforall' style='display: <?php if($searchby=='description'){ echo 'none'; } ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span id='searchforcombo' style='display: <?php if($searchby!='description'){ echo 'none'; } ?>'></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				 <?php echo $this->Js->submit('', array(
                  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#financepage',
                  'class'=>'searchbtn',
                  'controller'=>'finance',
                  'action'=>'commission',
                  'url'=> array('controller' => 'finance', 'action' => 'commission')
                ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#financepage',
            'evalScripts' => true,
            'url'=> array('controller'=>'finance', 'action'=>'commission')
        ));
        $currentpagenumber=$this->params['paging']['Commission']['page'];
        ?>
<div id="gride-bg" class="noborder">
    <div class="padding10">		
	<?php echo $this->Form->create('Commission',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'commissionremove')));?>
	
	<div class="greenbottomborder">
	<div class="height10"></div>
	<div class="paginator-text paddingtop0"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php 
		echo $this->Form->checkbox('selectAllCheckboxes', array(
		  'hiddenField' => false,
		  'onclick' => 'selectAllCheckboxes("commissionIds",this.checked)'
		));
		?>
		<label for="CommissionSelectAllCheckboxes"></label>
	</div>
	<div class="addnew-button massactionbox">
		<div class="actionmenu">
			<div class="btn-group">
				<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
				  Action <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_commissioncsv',$SubadminAccessArray)){ ?>
				<li>
					<?php echo $this->Html->link("Download CSV File", array('controller'=>'finance', "action"=>"commissioncsv"), array(
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'',
						'target'=>'_blank'
					));?>
				</li>
				<?php } ?>
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_commissionremove',$SubadminAccessArray)){?>
				<li>
					<?php echo $this->Js->submit('Delete Selected Record(s)', array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'escape'=>false,
					  'update'=>'#financepage',
					  'class'=>'massactionbtn',
					  'div'=>false,
					  'controller'=>'finance',
					  'action'=>'commissionremove',
					  'confirm' => 'Are You Sure?',
					  'url'   => array('controller' => 'finance', 'action' => 'commissionremove')
					));?>
				</li>
				<?php }?>
			  </ul>
			</div>
		</div>
	</div>
	<div class="clear-both"></div>
        <div class="tablegrid">
			<div class="tablegridheader">
                    <div>
                        <?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('Id', array('controller'=>'finance', "action"=>"commission/0/0/tran_id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#financepage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Id'
                        ));?>
                    </div>
				    <div>
                        <?php echo $this->Js->link('Date', array('controller'=>'finance', "action"=>"commission/0/0/tran_dt/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#financepage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Date'
                        ));?>
                    </div>
                    <div>
                        <?php echo $this->Js->link('From Member', array('controller'=>'finance', "action"=>"commission/0/0/from_id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#financepage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By From Member'
                        ));?>
                    </div>
                    <div>
                        <?php echo $this->Js->link("To Member", array('controller'=>'finance', "action"=>"commission/0/0/to_id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#financepage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>"Sort By To Member"
                        ));?>
                    </div>
                    <div>
                        <?php echo $this->Js->link('Amount', array('controller'=>'finance', "action"=>"commission/0/0/amount/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#financepage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Amount'
                        ));?>
                    </div>
                    <div>
                        <?php echo $this->Js->link('Description', array('controller'=>'finance', "action"=>"commission/0/0/description/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#financepage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Description'
                        ));?>
                    </div>
                    <div></div>
                </div>
                <?php foreach ($commissions as $commission):
                    if($commission['Commission']['description']=='position') $description='Comm. on Position';
                    elseif($commission['Commission']['description']=='memberfee') $description='Comm. on Fees';
					elseif($commission['Commission']['description']=='bonus') $description='Quickener';
					elseif($commission['Commission']['description']=='matching') $description='Matching Bonus';
					else $description=ucwords($commission['Commission']['description']);
                    ?>
                    <div class="tablegridrow">
                        <div><?php echo $commission['Commission']['tran_id'];?></div>
                        <div><?php echo $this->Time->format($SITECONFIG["timeformate"], $commission['Commission']['tran_dt']); ?></div>
						<div>
							<?php echo $this->Js->link($commission['From_Member']['user_name'].'('.$commission['Commission']['from_id'].')', array('controller'=>'member', "action"=>"memberadd/".$commission['Commission']['from_id']."/top/finance/commission~0~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
						</div>
                        <div>
							<?php echo $this->Js->link($commission['To_Member']['user_name'].'('.$commission['Commission']['to_id'].')', array('controller'=>'member', "action"=>"memberadd/".$commission['Commission']['to_id']."/top/finance/commission~0~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
						</div>
                        <div>$<?php echo round($commission['Commission']['amount'],4); ?></div>
                        <div><?php echo $description; ?></div>
                        <div class="checkbox">
								<?php
								echo $this->Form->checkbox('commissionIds.', array(
								  'value' => $commission['Commission']['tran_id'],
								  'class' => 'commissionIds',
								  'id'=>'commissionIds'.$commission['Commission']['tran_id'],
								  'hiddenField' => false
								));
								?>
								<label for="<?php echo 'commissionIds'.$commission['Commission']['tran_id']; ?>"></label>
                        </div>
                    </div>
                <?php endforeach; ?>
        </div>
	
	<?php if(count($commissions)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
        <?php echo $this->Form->end();
        if($this->params['paging']['Commission']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Commission',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'commission/rpp')));?>
			<div class="resultperpage">
			    <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			    </label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#financepage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'finance',
                  'action'=>'commission/rpp',
                  'url'   => array('controller' => 'finance', 'action' => 'commission/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
		</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#financepage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>