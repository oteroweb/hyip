<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 21-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Withdraw Request</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Withdraw Requests", array('controller'=>'finance', "action"=>"withdrawrequest"), array(
					'update'=>'#financepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Traditional Masspay", array('controller'=>'finance', "action"=>"traditionalmasspay"), array(
					'update'=>'#financepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Automatic Payments", array('controller'=>'finance', "action"=>"automaticmasspay"), array(
					'update'=>'#financepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Total Stats", array('controller'=>'finance', "action"=>"totalstats"), array(
					'update'=>'#financepage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="financepage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Withdrawal_Requests#Total_Stats" target="_blank">Help</a></div>
<div class="marginnone">
    <div class="padding10">
	<div class="serchgreybox"><?php echo "Withdraw Amount Statistics";?></div>
	<div class="withdrawtable">
		<div class="tablegrid">
			<div class="tablegridheader">
				<div>Description</div>
				<?php
				foreach ($withdraws as $withdraw):
					?><div><?php echo $withdraw['Withdraw']['payment_processor']; ?></div><?php
				endforeach;
				?>
				<div>Total</div>
			</div>
			<div class="tablegridrow">
				<div>Total</div>
				<?php
				$total=0;
				foreach ($withdraws as $withdraw):
				$total+=$withdraw[0]['total_amount'];
				?>
				<div>$<?php echo round($withdraw[0]['total_amount'],4); ?></div>
				<?php endforeach; ?>
				<div>$<?php echo round($total,4); ?></div>
			</div>
		</div>
	</div>
	
	
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#financepage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>