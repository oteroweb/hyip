<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 13-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Payment History</div>
<div id="financepage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Payment_History" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<script type="text/javascript">
	<?php if($searchby=='processorid'){ ?>
	generatecombo("data[Member_fee_payment][proc_name]","Processor","id","proc_name","<?php echo $searchfor; ?>",'#searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } elseif($searchby=='ptype'){ ?>
	generatecombo("data[Member_fee_payment][ptype]","Member_fee_payment","ptype","ptype","<?php echo $searchfor; ?>",'#searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } ?>			
	
</script>
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Member_fee_payment',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'index')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('searchby', array(
						      'type' => 'select',
						      'options' => array('all'=>'Select Parameter', 'member_id'=>'Member Id', 'payer_email'=>'Payer Email', 'processorid'=>'Processor', 'transaction_no'=>'Transaction Number','amount'=>'Amount','fees'=>'Fees','discount'=>'Discount', 'ptype'=>'Type', 'ip_add'=>'IP Address','payment_status'=>'Status'),
						      'selected' => $searchby,
						      'class'=>'',
						      'label' => false,
						      'style' => '',
									  'onchange'=>'if($(this).val()=="processorid"){generatecombo("data[Member_fee_payment][proc_name]","Processor","id","proc_name","'.$searchfor.'","#searchforcombo","'.$ADMINURL.'","");$("#searchforstatus").hide();$("#searchall").hide();$("#searchforcombo").show(500);}else if($(this).val()=="ptype"){generatecombo("data[Member_fee_payment][ptype]","Member_fee_payment","ptype","ptype","'.$searchfor.'","#searchforcombo","'.$ADMINURL.'","");$("#searchforstatus").hide();$("#searchall").hide();$("#searchforcombo").show(500);}else if($(this).val()=="payment_status"){$("#searchforcombo").hide();$("#searchall").hide();$("#searchforstatus").show(500);} else{$("#searchforstatus").hide();$("#searchforcombo").hide();$("#searchall").show(500);}'
						));
						?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span id='searchall' style='display:<?php if($searchby=="ptype" || $searchby=='payment_status' || $searchby=="processorid"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span id='searchforcombo' style='display:<?php if($searchby!="processorid" && $searchby!="ptype"){ echo "none";} ?>'></span>
			<span id='searchforstatus' style='display:<?php if($searchby!="payment_status"){ echo "none";} ?>'>
			
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('payment_status', array(
						      'type' => 'select',
						      'options' => array('Completed'=>'Completed','COMPLETE'=>'COMPLETE','CANCELED'=>'CANCELED','Fail'=>'Fail','Done'=>'Done','success'=>'success'),
						      'selected' => $searchfor,
						      'class'=>'',
						      'label' => false,
						      'style' => ''
						));
						?>
					</label>
				</div>
			</div>
			
			</span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#financepage',
				'class'=>'searchbtn',
				'controller'=>'finance',
				'action'=>'index',
				'url'=> array('controller' => 'finance', 'action' => 'index')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->

	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#financepage',
		'evalScripts' => true,
		'url'=> array('controller'=>'finance', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Member_fee_payment']['page'];
	?>
	
<div id="gride-bg" class="noborder">
    <div class="padding10">
				
	<?php echo $this->Form->create('Member_fee_payment',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'paymenthistoryremove')));?>
	
	<div class="greenbottomborder">
	<div class="height10"></div>
	<div class="paginator-text paddingtop0"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php 
		echo $this->Form->checkbox('selectAllCheckboxes', array(
		  'hiddenField' => false,
		  'onclick' => 'selectAllCheckboxes("paymentIds",this.checked)'
		));
		?>
		<label for="Member_fee_paymentSelectAllCheckboxes"></label>
	</div>
	<div class="addnew-button massactionbox">
		<div class="actionmenu">
			<div class="btn-group">
				<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
				  Action <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_paymenthistorycsv',$SubadminAccessArray)){ ?>
					<li>
						<?php echo $this->Html->link("Download CSV File", array('controller'=>'finance', "action"=>"paymenthistorycsv"), array(
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'',
							'target'=>'_blank'
						));?>
					</li>
				<?php } ?>
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_paymenthistoryremove',$SubadminAccessArray)){?>
					<li>
					<?php echo $this->Js->submit('Delete Selected Record(s)', array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'escape'=>false,
					  'update'=>'#financepage',
					  'class'=>'massactionbtn',
					  'div'=>false,
					  'controller'=>'finance',
					  'action'=>'paymenthistoryremove',
					  'confirm' => 'Are You Sure?',
					  'url'   => array('controller' => 'finance', 'action' => 'paymenthistoryremove')
					));
					?>
					</li>
				<?php }?>
			  </ul>
			</div>
		</div>
	</div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('M. Id', array('controller'=>'finance', "action"=>"index/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Pay. Date', array('controller'=>'finance', "action"=>"index/0/0/pay_dt/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Payment Date'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link("Payer's Email", array('controller'=>'finance', "action"=>"index/0/0/payer_email/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>"Sort By Payer's Email"
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Status', array('controller'=>'finance', "action"=>"index/0/0/payment_status/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Status'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Pending Reason', array('controller'=>'finance', "action"=>"index/0/0/pending_reason/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Pending Reason'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Trans. No/Processor', array('controller'=>'finance', "action"=>"index/0/0/processor/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Processor'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Amount', array('controller'=>'finance', "action"=>"index/0/0/amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Amount'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Fees', array('controller'=>'finance', "action"=>"index/0/0/fees/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Fees'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Discount', array('controller'=>'finance', "action"=>"index/0/0/discount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Fees'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Type', array('controller'=>'finance', "action"=>"index/0/0/ptype/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Type'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('IP Address', array('controller'=>'finance', "action"=>"index/0/0/ip_add/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By IP Address'
					));?>
				</div>
                <div>Action</div>
                <div></div>
			</div>
			<?php foreach ($member_fee_payments as $member_fee_payment):?>
				<div class="tablegridrow">
					<div>
						<?php 
						echo $this->Js->link($member_fee_payment['Member_fee_payment']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$member_fee_payment['Member_fee_payment']['member_id']."/top/finance/index~0~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
                    <div><?php echo $this->Time->format($SITECONFIG["timeformate"], $member_fee_payment['Member_fee_payment']['pay_dt']); ?></div>
                    <div>
						<?php 
						echo $this->Js->link($member_fee_payment['Member_fee_payment']['payer_email'], array('controller'=>'member', "action"=>"memberadd/".$member_fee_payment['Member_fee_payment']['member_id']."/top/finance/index~0~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
                    <div><?php echo $member_fee_payment['Member_fee_payment']['payment_status']; ?></div>
                    <div><?php echo $member_fee_payment['Member_fee_payment']['pending_reason']; ?></div>
                    <div><?php echo $member_fee_payment['Member_fee_payment']['transaction_no']."<br />".$member_fee_payment['Member_fee_payment']['processor']; ?></div>
                    <div>$<?php echo round($member_fee_payment['Member_fee_payment']['amount'],4); ?></div>
                    <div>$<?php echo round($member_fee_payment['Member_fee_payment']['fees'],4); ?></div>
					<div>$<?php echo round($member_fee_payment['Member_fee_payment']['discount'],4); ?></div>
                    <div><?php echo ucfirst($member_fee_payment['Member_fee_payment']['ptype']); ?></div>
                    <div><?php echo $member_fee_payment['Member_fee_payment']['ip_add']; ?></div>
                    <div>
								<?php  echo $this->Js->link($this->html->image('search.png', array('alt'=>'View History',  'align'=>'absmiddle')), array('controller'=>'finance', "action"=>"paymenthistory/".$member_fee_payment['Member_fee_payment']['id']), array(
									'update'=>'#financepage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>'View History'
								)); ?>	
                        
					</div>
                    <div class="checkbox">
						<?php
						echo $this->Form->checkbox('paymentIds.', array(
						  'value' => $member_fee_payment['Member_fee_payment']['id'],
						  'class' => 'paymentIds',
						  'id'=>'paymentIds'.$member_fee_payment['Member_fee_payment']['id'],
						  'hiddenField' => false
						));
						?>
						<label for="<?php echo 'paymentIds'.$member_fee_payment['Member_fee_payment']['id'] ?>"></label>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($member_fee_payments)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Member_fee_payment']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Member_fee_payment',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'index/rpp')));?>
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#financepage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'finance',
			  'action'=>'index/rpp',
			  'url'   => array('controller' => 'finance', 'action' => 'index/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#financepage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>