<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 13-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>		
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Coupons</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Coupons", array('controller'=>'finance', "action"=>"coupons"), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Coupons History", array('controller'=>'finance', "action"=>"couponshistory"), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">
<div id="financepage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Coupons#Coupons_History" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="height10"></div>
<!-- Search-box-start -->
<script type="text/javascript">
	<?php if($searchby=='plantype'){ ?>
	generatecombo('data[Couponhistory][plantype]','Couponhistory','plantype','plantype',"<?php echo $searchfor; ?>",'#searchfor_type',"<?php echo $ADMINURL;?>","");
	<?php } ?>			
</script>
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Couponhistory',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'couponshistory')));?>	
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('searchby', array(
							'type' => 'select',
							'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'member_id'=>'Member Id', 'coupon_id'=>'Coupon Id', 'code'=>'Code', 'discount'=>'Discount', 'planprice'=>'Purchase Price', 'plantype'=>'Purchase Type', 'ipaddr'=>'IP Address'),
							'selected' => $searchby,
							'class'=>'',
							'label' => false,
							'style' => '',
									    'onchange'=>'if($(this).val()=="plantype"){generatecombo("data[Couponhistory][plantype]","Couponhistory","plantype","plantype","'.$searchfor.'","#searchfor_type","'.$ADMINURL.'","");$("#searchfor_all").hide();$("#searchfor_type").show(500);}else{$("#searchfor_type").hide();$("#searchfor_all").show(500);}'
						));
						?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span id='searchfor_all' class="searchforfields_s" style='display:<?php if($searchby=="plantype"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span id='searchfor_type' style='display:<?php if($searchby!="plantype"){ echo "none";} ?>'></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#financepage',
						'class'=>'searchbtn',
						'controller'=>'finance',
						'action'=>'couponshistory',
						'url'=> array('controller' => 'finance', 'action' => 'couponshistory')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#financepage',
		'evalScripts' => true,
		'url'=> array('controller'=>'finance', 'action'=>'couponshistory')
	));
	$currentpagenumber=$this->params['paging']['Couponhistory']['page'];
	?>
<div id="gride-bg">
    <div class="Xpadding10">	
	<?php echo $this->Form->create('Couponhistory',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'couponshistory')));?>
	<div class="height10"></div>
	<div class="paginator-text paddingtop0"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php
			echo $this->Form->checkbox('selectAllCheckboxes', array(
			  'hiddenField' => false,
			  'onclick' => 'selectAllCheckboxes("CouponhistoryIds",this.checked)'
			));
		?>
		<label for="CouponhistorySelectAllCheckboxes"></label>
	</div>
	<div class="addnew-button massactionbox">
		<div class="actionmenu">
			<div class="btn-group">
				<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
				  Action <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_couponshistoryremove',$SubadminAccessArray)){ ?>
					<li>
					<?php echo $this->Js->link("Delete All History", array('controller'=>'finance', "action"=>"couponshistoryremove/all"), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'confirm' => 'Are You Sure?',
						'class'=>''
					));?>
					</li>
				<?php } ?>
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_couponshistoryremove',$SubadminAccessArray)) { ?>
				<li>
					<?php echo $this->Js->submit('Delete Selected Record(s)', array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'escape'=>false,
					  'update'=>'#financepage',
					  'class'=>'massactionbtn',
					  'div'=>false,
					  'controller'=>'logs',
					  'action'=>'couponshistoryremove',
					  'confirm' => 'Are You Sure?',
					  'url'   => array('controller' => 'finance', 'action' => 'couponshistoryremove')
					));?>
				</li>
				<?php }?>
			  </ul>
			</div>
		</div>
	</div>
	<div class="clear-both"></div>
	<div class="tablegrid">
			<div class="tablegridheader">
                <div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'finance', "action"=>"couponshistory/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Date', array('controller'=>'finance', "action"=>"couponshistory/0/dt/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Date'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Member', array('controller'=>'finance', "action"=>"couponshistory/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link("Coupon Id", array('controller'=>'finance', "action"=>"couponshistory/0/coupon_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>"Sort By Coupon Id"
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Code', array('controller'=>'finance', "action"=>"couponshistory/0/code/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Code'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Discount', array('controller'=>'finance', "action"=>"couponshistory/0/discount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Discount'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Purchase Price', array('controller'=>'finance', "action"=>"couponshistory/0/planprice/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Purchase Price'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Purchase Type', array('controller'=>'finance', "action"=>"couponshistory/0/plantype/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Purchase Type'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('IP Address', array('controller'=>'finance', "action"=>"couponshistory/0/ipaddr/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By IP Address'
					));?>
				</div>
                <div></div>
			</div>
			<?php
			$i = 0;
			foreach ($couponhistories as $couponhistory):
				$class = 'class="blue-color"';if ($i++ % 2 == 0){$class = 'class="white-color"';}?>
				<div class="tablegridrow">
					<div><?php echo $couponhistory['Couponhistory']['id']; ?></div>
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $couponhistory['Couponhistory']['dt']); ?></div>
					<div>
						<?php 
						echo $this->Js->link($couponhistory['Couponhistory']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$couponhistory['Couponhistory']['member_id']."/top/finance/coupons~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
                    <div><?php echo $couponhistory['Couponhistory']['coupon_id'];?></div>
                    <div><?php echo $couponhistory['Couponhistory']['code'];?></div>
					<div>$<?php echo round($couponhistory['Couponhistory']['discount'],4);?></div>
					<div>$<?php echo round($couponhistory['Couponhistory']['planprice'],4);?></div>
					<div>
						<a href="#" class="vtip" title="<b>Package Id : </b><?php echo $couponhistory['Couponhistory']['plan_id'];?>"> <?php echo $couponhistory['Couponhistory']['plantype'];?></a></div>
					
					<div><?php echo $couponhistory['Couponhistory']['ipaddr'];?></div>
					<div class="checkbox">
						<?php
						echo $this->Form->checkbox('CouponhistoryIds.', array(
							'value' => $couponhistory['Couponhistory']['id'],
							'class' => 'CouponhistoryIds',
							'id'=>'CouponhistoryIds'.$couponhistory['Couponhistory']['id'],
							'hiddenField' => false
						));
						?>
						<label for="<?php echo 'CouponhistoryIds'.$couponhistory['Couponhistory']['id']; ?>"></label>
                    </div>
				</div>
			<?php endforeach; ?>
				
	</div>
    
	<?php if(count($couponhistories)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
	<?php echo $this->Form->end();
	if($this->params['paging']['Couponhistory']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Couponhistory',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'couponshistory/rpp')));?>
		<div class="combobox1">
		<?php 
		echo $this->Form->input('resultperpage', array(
		  'type' => 'select',
		  'options' => $resultperpage,
		  'selected' => $this->Session->read('pagerecord'),
		  'class'=>'',
		  'label' => false,
		  'div'=>false,
		  'style' => '',
		  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
		));
		?>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#financepage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'finance',
			  'action'=>'couponshistory/rpp',
			  'url'   => array('controller' => 'finance', 'action' => 'couponshistory/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#financepage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>