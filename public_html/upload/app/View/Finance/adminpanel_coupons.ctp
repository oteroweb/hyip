<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Coupons</div>
<div class="height10"></div>
<div id="UpdateMessage"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Coupons", array('controller'=>'finance', "action"=>"coupons"), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Coupons History", array('controller'=>'finance', "action"=>"couponshistory"), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">
<div id="financepage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>		
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Coupons#Coupons" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Coupon',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'coupons')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php $options=array('all'=>'Select Parameter', 'id'=>'Id', 'title'=>'Title', 'code'=>'Code', 'amount'=>'Amount', 'maximumdiscount'=>'Maximum Discount','used'=>'Total Used', 'minimumamount'=>'Minimum Criteria', 'active'=>'Active Coupons', 'inactive'=>'Inactive Coupons');
						
						echo $this->Form->input('searchby', array(
						      'type' => 'select',
						      'options' => $options,
						      'selected' => $searchby,
						      'class'=>'',
						      'label' => false,
						      'style' => '',
							  'onchange'=>'if($(this).val()=="inactive" || $(this).val()=="active"){$(".SearchFor").hide(500);}else{$(".SearchFor").show(500);}'
						));
						?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span class="SearchFor" <?php if($searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>>Search For :</span>
			<span class="searchforfields_s SearchFor" <?php if($searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#financepage',
						'class'=>'searchbtn',
						'controller'=>'finance',
						'action'=>'coupons',
						'url'=> array('controller' => 'finance', 'action' => 'coupons')
				));?>
			</span>
		</div>
	 </div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#financepage',
		'evalScripts' => true,
		'url'=> array('controller'=>'finance', 'action'=>'coupons')
	));
	$currentpagenumber=$this->params['paging']['Coupon']['page'];
	?>
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
    <?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_couponsadd',$SubadminAccessArray)){ ?>
    	<?php echo $this->Js->link("+ Add New", array('controller'=>'finance', "action"=>"couponsadd"), array(
			'update'=>'#financepage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btnorange'
		));?>
	<?php } ?>
    </div>
	<div class="clear-both"></div>
	<div class="tablegrid">
			<div class="tablegridheader">
                <div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'finance', "action"=>"coupons/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link("Title", array('controller'=>'finance', "action"=>"coupons/0/0/title/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>"Sort By Title"
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Code', array('controller'=>'finance', "action"=>"coupons/0/0/code/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Code'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Amount', array('controller'=>'finance', "action"=>"coupons/0/0/amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Amount'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Max. Discount', array('controller'=>'finance', "action"=>"coupons/0/0/maximumdiscount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Maximum Discount'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Min. Criteria', array('controller'=>'finance', "action"=>"coupons/0/0/minimumamount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Minimum Criteria'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Type', array('controller'=>'finance', "action"=>"coupons/0/0/coupontype/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Minimum Criteria'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Total Used', array('controller'=>'finance', "action"=>"coupons/0/0/used/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Total Used'
					));?>
				</div>
				<div>Action</div>
           </div>
			<?php foreach ($coupons as $coupon): ?>
				<div class="tablegridrow">
					<div><?php echo $coupon['Coupon']['id'];?></div>
					<div><?php echo $coupon['Coupon']['title'];?></div>
                    <div><?php echo $coupon['Coupon']['code'];?></div>
					<div><?php if($coupon['Coupon']['amounttype']==1 && $coupon['Coupon']['coupontype']!=4){echo $coupon['Coupon']['amount'].'%';}else{echo '$'.$coupon['Coupon']['amount'];} ?></div>
					<div> <?php echo ($coupon['Coupon']['maximumdiscount']) ? '$'.$coupon['Coupon']['maximumdiscount'] : '-';?></div>
					<div>$<?php echo $coupon['Coupon']['minimumamount'];?></div>
					<div>
								<?php  if($coupon['Coupon']['coupontype']==1)
											echo "One Time";
									   elseif($coupon['Coupon']['coupontype']==2)
											echo "Unlimited";	
									   elseif($coupon['Coupon']['coupontype']==3)
											echo $coupon['Coupon']['coupontime']." Time(s)";
									   elseif($coupon['Coupon']['coupontype']==4)
											echo $coupon['Coupon']['coupontime']." Time(s) Free Cash";
								       elseif($coupon['Coupon']['coupontype']==5)
								            echo "Unlimited Free Cash"; ?>
					</div>
					<div><?php echo $coupon['Coupon']['used'];?></div>
					<div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_couponsadd/$',$SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Coupon')).' Edit Coupon', array('controller'=>'finance', "action"=>"couponsadd/".$coupon['Coupon']['id']), array(
									'update'=>'#financepage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));?>
							</li>
                            <?php } ?>
							
						  	<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_couponsstatus',$SubadminAccessArray)){ ?>
							<li>
								<?php 
								if($coupon['Coupon']['status']==0){
									$statusaction='1';
									$statusicon='red-icon.png';
									$statustext='Activate Coupon';
								}else{
									$statusaction='0';
									$statusicon='blue-icon.png';
									$statustext='Inactivate Coupon';}
								echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'finance', "action"=>"couponsstatus/".$statusaction."/".$coupon['Coupon']['id']."/".$currentpagenumber), array(
									'update'=>'#financepage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));?>
							</li>
                            <?php } ?>
                            <?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_couponsremove',$SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Coupon')).' Delete Coupon', array('controller'=>'finance', "action"=>"couponsremove/".$coupon['Coupon']['id']."/".$currentpagenumber), array(
                                        'update'=>'#financepage',
                                        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                        'escape'=>false,
                                        'confirm'=>"Do You Really Want to Delete This Coupon?"
                                ));?>
							</li>
                            <?php } ?>
						  </ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($coupons)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php
	if($this->params['paging']['Coupon']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Coupon',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'coupons/0/rpp')));?>
		
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#financepage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'finance',
			  'action'=>'coupons/0/rpp',
			  'url'   => array('controller' => 'finance', 'action' => 'coupons/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	</div>
	</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#financepage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>