<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 10-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Payment History</div>
<div id="financepage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Payment_History" target="_blank">Help</a></div>
<div class="backgroundwhite">
    <div class="padding10">
			<div class="clear-both"></div>
			
			
			<div class="tablegrid">
			    <div class="tablegridheader textleft">
				<div style="width: 50%;">&nbsp;&nbsp;&nbsp;Field</div>
				<div>Value</div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Id";?> :</div>
				<div><?php echo $member_fee_payment["Member_fee_payment"]["id"]; ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Member Id";?> :</div>
				<div><?php echo $member_fee_payment["Member_fee_payment"]["member_id"]; ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Payment Date";?> :</div>
				<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $member_fee_payment["Member_fee_payment"]["pay_dt"]); ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Payer's Email";?> :</div>
				<div><?php echo $member_fee_payment["Member_fee_payment"]["payer_email"]; ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Receiver Email";?> :</div>
				<div><?php echo $member_fee_payment["Member_fee_payment"]["receiver_email"]; ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Payment Status";?> :</div>
				<div><?php echo $member_fee_payment["Member_fee_payment"]["payment_status"]; ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Pending Reason";?> :</div>
				<div><?php echo $member_fee_payment["Member_fee_payment"]["pending_reason"]; ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Trans. No";?> :</div>
				<div><?php echo $member_fee_payment["Member_fee_payment"]["transaction_no"]; ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Payment Processor";?> :</div>
				<div><?php echo $member_fee_payment["Member_fee_payment"]["processor"]; ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Processor Id";?> :</div>
				<div><?php echo $member_fee_payment["Member_fee_payment"]["processorid"]; ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Amount";?> :</div>
				<div>$<?php echo round($member_fee_payment["Member_fee_payment"]["amount"],4); ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Fees";?> :</div>
				<div>$<?php echo round($member_fee_payment["Member_fee_payment"]["fees"],4); ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Discount";?> :</div>
				<div>$<?php echo round($member_fee_payment["Member_fee_payment"]["discount"],4); ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Type";?> :</div>
				<div><?php echo $member_fee_payment["Member_fee_payment"]["ptype"]; ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Payment Method";?> :</div>
				<div><?php
				$find=array('ca', 'ea', 're', 'co', ':');
				$replace=array('Cash Balance', 'Earning Balance', 'Re-purchase Balance', 'Commission Balance', ' + ');
				$methodnmdisp=$member_fee_payment["Member_fee_payment"]["paymentmethod"];
				if(!in_array($member_fee_payment["Member_fee_payment"]["paymentmethod"],array('cash','repurchase', 'earning', 'commission', 'processor')))
					$methodnmdisp=str_replace($find, $replace, $member_fee_payment["Member_fee_payment"]["paymentmethod"]);
				echo ucfirst($methodnmdisp);
				?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "IP Address";?> :</div>
				<div><?php echo $member_fee_payment["Member_fee_payment"]["ip_add"]; ?></div>
			    </div>
			    
			   
			   <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "Comment";?> :</div>
				<div><?php echo nl2br(stripslashes($member_fee_payment["Member_fee_payment"]["comment"])); ?></div>
			    </div>
			    <div class="tablegridrow textleft">
				<div>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "File";?> :</div>
				<div>
					<?php echo '<a href="'.$SITEURL.'img/bankwire/'.$member_fee_payment["Member_fee_payment"]["file2"].'" rel="lightbox" title="'.$member_fee_payment["Member_fee_payment"]["file2"].'">'.$member_fee_payment["Member_fee_payment"]["file2"].'</a>'; ?>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<?php echo '<a href="'.$SITEURL.'img/bankwire/'.$member_fee_payment["Member_fee_payment"]["file1"].'" rel="lightbox" title="'.$member_fee_payment["Member_fee_payment"]["file1"].'">'.$member_fee_payment["Member_fee_payment"]["file1"].'</a>'; ?>
				</div>
			    </div>
			</div>
			
			<div style="height: 10px;"></div>
			<?php  echo $this->Js->link('Back', array('controller'=>'finance', "action"=>"index"), array(
				'update'=>'#financepage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'btnorange',
			)); ?>
    
</div>
</div>
<?php if(!$ajax){?>
</div><!--#financepage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>