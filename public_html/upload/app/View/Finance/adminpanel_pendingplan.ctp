<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 13-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Finance / Pending Purchases</div>
<div id="financepage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Pending_Purchases" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<script type="text/javascript">
	<?php if($searchby=='processorid'){ ?>
	generatecombo("data[Pending_plan][processorid]","Processor","id","proc_name","<?php echo $searchfor; ?>",'#searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } elseif($searchby=='plantype'){ ?>
	generatecombo("data[Pending_plan][plantype]","Pending_plan","plantype","plantype","<?php echo $searchfor;?>",'#searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } ?>			
</script>
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Pending_plan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'pendingplan')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('searchby', array(
						      'type' => 'select',
						      'options' => array('all'=>'Select Parameter', 'member_id'=>'Member Id', 'amount'=>'Amount', 'processorid'=>'Payment Processor', 'plantype'=>'Plan Type', 'fees'=>'Fees', 'planid'=>'Plan Id', 'ip_address'=>'IP Address'),
						      'selected' => $searchby,
						      'class'=>'',
						      'label' => false,
						      'style' => '',
									  'onchange'=>'if($(this).val()=="processorid"){generatecombo("data[Pending_plan][proc_name]","Processor","id","proc_name","'.$searchfor.'","#searchforcombo","'.$ADMINURL.'","");$("#searchforstatus").hide();$("#searchall").hide();$("#searchforcombo").show(500);}else if($(this).val()=="plantype"){generatecombo("data[Pending_plan][plantype]","Pending_plan","plantype","plantype","'.$searchfor.'","#searchforcombo","'.$ADMINURL.'","");$("#searchforstatus").hide();$("#searchall").hide();$("#searchforcombo").show(500);} else{$("#searchforstatus").hide();$("#searchforcombo").hide();$("#searchall").show(500);}'
						));
						?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span id='searchall' style='display:<?php if($searchby=="plantype" || $searchby=="processorid"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span id='searchforcombo' style='display:<?php if($searchby!="processorid" && $searchby!="plantype"){ echo "none";} ?>'></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#financepage',
					'class'=>'searchbtn',
					'controller'=>'finance',
					'action'=>'pendingplan',
					'url'=> array('controller' => 'finance', 'action' => 'pendingplan')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#financepage',
		'evalScripts' => true,
		'url'=> array('controller'=>'finance', 'action'=>'pendingplan')
	));
	$currentpagenumber=$this->params['paging']['Pending_plan']['page'];
	?>
<div id="gride-bg" class="noborder">
    <div class="padding10">	
	<?php echo $this->Form->create('Pending_plan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'pendingplan')));?>
	
	<div class="greenbottomborder">
	<div class="height10"></div>
	<div class="paginator-text paddingtop0"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php 
		echo $this->Form->checkbox('selectAllCheckboxes', array(
		  'hiddenField' => false,
		  'onclick' => 'selectAllCheckboxes("pendingIds",this.checked)'
		));
		?>
		<label for="Pending_planSelectAllCheckboxes"></label>
	</div>
	<div class="addnew-button massactionbox">
		<div class="actionmenu">
			<div class="btn-group">
				<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
				  Action <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_pendingplancsv',$SubadminAccessArray)){ ?>
					<li>
					<?php echo $this->Html->link("Download CSV File", array('controller'=>'finance', "action"=>"pendingplancsv"), array(
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'',
						'target'=>'_blank'
					));?>
					</li>
				<?php } ?>
				<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_pendingplanremove',$SubadminAccessArray)){?>
				<li>
					<?php echo $this->Js->submit('Delete Selected Record(s)', array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'escape'=>false,
					  'update'=>'#financepage',
					  'class'=>'massactionbtn',
					  'div'=>false,
					  'controller'=>'finance',
					  'action'=>'pendingplanremove',
					  'confirm' => 'Are You Sure?',
					  'url'   => array('controller' => 'finance', 'action' => 'pendingplanremove')
					));?>
				<li>
				<?php }?>
				</li>
			  </ul>
			</div>
		</div>
	</div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'finance', "action"=>"pendingplan/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('M. Id', array('controller'=>'finance', "action"=>"pendingplan/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Pay. Date', array('controller'=>'finance', "action"=>"pendingplan/0/0/payment_date/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Payment Date'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Plan Type', array('controller'=>'finance', "action"=>"pendingplan/0/0/plantype/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Type'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Amount', array('controller'=>'finance', "action"=>"pendingplan/0/0/amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Amount'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Fees', array('controller'=>'finance', "action"=>"pendingplan/0/0/fees/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Fees'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Processor', array('controller'=>'finance', "action"=>"pendingplan/0/0/processor/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Processor'
					));?>
				</div>
                <div>
					<?php echo $this->Js->link('Plan Id', array('controller'=>'finance', "action"=>"pendingplan/0/0/planid/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Id'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('IP Address', array('controller'=>'finance', "action"=>"pendingplan/0/0/ip_address/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#financepage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By IP Address'
					));?>
				</div>
                <div><?php echo 'Notes';?></div>
				<div><?php echo 'Action';?></div>
                <div></div>
			</div>
			<?php foreach ($pendingplans as $pendingplan): ?>
				<div class="tablegridrow">
					<div>#<?php echo $pendingplan['Pending_plan']['id']; ?></div>
					<div>
						<?php 
						echo $this->Js->link($pendingplan['Pending_plan']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$pendingplan['Pending_plan']['member_id']."/top/finance/pendingplan~0~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
                    <div><?php echo $this->Time->format($SITECONFIG["timeformate"], $pendingplan['Pending_plan']['payment_date']); ?></div>
					<div>
					<?php
						if($pendingplan['Pending_plan']['plantype']=='Member1')
						{
							echo 'Membership';
						}
						elseif($pendingplan['Pending_plan']['plantype']=='Ppc1')
						{
							echo 'PPC';
						}
						elseif($pendingplan['Pending_plan']['plantype']=='Ptc1')
						{
							echo 'PTC';
						}
						else
						{
						echo ucwords(substr($pendingplan['Pending_plan']['plantype'], 0, strlen($pendingplan['Pending_plan']['plantype'])-1));
						} ?>
					</div>
					<div>$<?php echo round($pendingplan['Pending_plan']['amount'],4); ?></div>
					<div>$<?php echo round($pendingplan['Pending_plan']['fees'],4); ?></div>
                    <div>
						<?php echo $pendingplan['Pending_plan']['processor']; ?>
						<?php 
						if($pendingplan['Pending_plan']['comment']!=''){
						echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Bank Wire',  'align'=>'absmiddle')), array('controller'=>'finance', "action"=>"pendingplanadd/".$pendingplan['Pending_plan']['id']), array(
							'update'=>'#financepage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'Edit Details'
						));
						} ?>
					</div>
					<div><?php echo $pendingplan['Pending_plan']['planid']; ?></div>
					<div><?php echo $pendingplan['Pending_plan']['ip_address']; ?></div>
                    <div><?php echo $this->html->image('information.png', array('alt'=>'Notes', 'class'=>'vtip', 'title'=>$pendingplan['Pending_plan']['notes']));?></div>
					<div>
								<?php if(!isset($SubadminAccessArray) || in_array('finance',$SubadminAccessArray) || in_array('finance/adminpanel_pendingplanpay',$SubadminAccessArray)){ ?>
									<?php echo $this->Js->link($this->html->image('check.png', array('alt'=>'Mark as Paid')), array('controller'=>'finance', "action"=>"pendingplanpay/".$pendingplan['Pending_plan']['id']."/".$currentpagenumber), array(
										'update'=>'#financepage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false,
										'class'=>'vtip',
										'confirm' => 'Are You Sure You Want to Mark Selected as Paid?',
										'title'=>'Mark Selected as Paid'
									));?>
								<?php }else{
										echo '&nbsp;';
								 } ?>
					</div>
                    <div class="checkbox">
						<?php
						echo $this->Form->checkbox('pendingIds.', array(
						  'value' => $pendingplan['Pending_plan']['id'],
						  'class' => 'pendingIds',
						  'id'=>'pendingIds'.$pendingplan['Pending_plan']['id'],
						  'hiddenField' => false
						));
						?>
						<label for="<?php echo 'pendingIds'.$pendingplan['Pending_plan']['id']; ?>"></label>
                    </div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($pendingplans)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Pending_plan']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Pending_plan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'finance','action'=>'pendingplan/rpp')));?>
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#financepage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'finance',
			  'action'=>'pendingplan/rpp',
			  'url'   => array('controller' => 'finance', 'action' => 'pendingplan/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#financepage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>