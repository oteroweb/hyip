<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Login Ads</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Login Ads", array('controller'=>'loginad', "action"=>"loginad"), array(
					'update'=>'#Loginadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Login Ad Plans", array('controller'=>'loginad', "action"=>"plan"), array(
					'update'=>'#Loginadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="Loginadpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Login_Ads#Login_Ad_Plans" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Loginad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'loginad','action'=>'plan')));?>
	<div class="from-box">
		<div class="fromboxmain width480">
			<span>Search By :</span>
			<span>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
						<?php 
						echo $this->Form->input('searchby', array(
							  'type' => 'select',
							  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'popup'=>'Popup Plan', 'surfbar'=>'Surfbar Plan', 'plan_name'=>'Plan Name', 'price'=>'Price', 'days'=>'Days', 'active'=>'Active Plans', 'inactive'=>'Inactive Plans'),
							  'selected' => $searchby,
							  'class'=>'',
							  'label' => false,
							  'style' => '',
							  'onchange'=>'if($(this).val()=="surfbar" || $(this).val()=="popup" || $(this).val()=="inactive" || $(this).val()=="active"){$(".SearchFor").hide(500);}else{$(".SearchFor").show(500);}'
						));
						?>
						</label>
					</div>
				</div>
			</span>
		</div>
		 <div class="fromboxmain" >
			<span class="SearchFor" <?php if($searchby=="surfbar" || $searchby=="popup" || $searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>>Search For :</span>
			<span class="SearchFor" <?php if($searchby=="surfbar" || $searchby=="popup" || $searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#Loginadpage',
				'class'=>'searchbtn',
				'controller'=>'loginad',
				'action'=>'plan',
				'url'=> array('controller' => 'loginad', 'action' => 'plan')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->


	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#Loginadpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'loginad', 'action'=>'plan')
	));
	$currentpagenumber=$this->params['paging']['Loginad']['page'];
	?>
	
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('loginad',$SubadminAccessArray) || in_array('loginad/adminpanel_planadd', $SubadminAccessArray)){ ?>
		<?php echo $this->Js->link("+ Add New", array('controller'=>'loginad', "action"=>"planadd"), array(
			'update'=>'#Loginadpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btnorange'
		));?>
	<?php } ?>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Loginad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'loginad','action'=>'plan')));?>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'loginad', "action"=>"plan/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Advertisement Type', array('controller'=>'loginad', "action"=>"plan/0/0/ptype/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Advertisement Type'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Plan Name', array('controller'=>'loginad', "action"=>"plan/0/0/plan_name/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Name'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Price', array('controller'=>'loginad', "action"=>"plan/0/0/price/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Price'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Days', array('controller'=>'loginad', "action"=>"plan/0/0/days/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Days'
					));?>
				</div>
                <div><?php echo 'Action';?></div>
			</div>
			<?php foreach ($loginads as $loginad): ?>
				<div class="tablegridrow">
				    <div><?php echo $loginad['Loginad']['id']; ?></div>
					<div><?php echo ($loginad['Loginad']['ptype']==1)?"Popup":"Surfbar"; ?></div>
					<div><?php echo $loginad['Loginad']['plan_name']; ?></div>
					<div>$<?php echo $loginad['Loginad']['price']; ?></div>
					<div><?php echo $loginad['Loginad']['days']; ?></div>
					<div>
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
										
											<?php if(!isset($SubadminAccessArray) || in_array('loginad',$SubadminAccessArray) || in_array('loginad/adminpanel_planadd/$', $SubadminAccessArray)){ ?>
											<li>
												<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Login Ads Plan')).' Edit Login Ads Plan', array('controller'=>'loginad', "action"=>"planadd/".$loginad['Loginad']['id']), array(
													'update'=>'#Loginadpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>'vtip',
													'title'=>'Edit Login Ads Plan'
												));?>
											</li>
											<?php } ?>
										
										    <?php if(!isset($SubadminAccessArray) || in_array('loginad',$SubadminAccessArray) || in_array('loginad/adminpanel_loginadstatus', $SubadminAccessArray)){ ?>
										    <li>
												<?php
												if($loginad['Loginad']['active_status']==0){
													$statusaction='1';
													$statusicon='red-icon.png';
													$statustext='Activate Login Ads Plan';
												}else{
													$statusaction='0';
													$statusicon='blue-icon.png';
													$statustext='Inactivate Login Ads Plan';}
												echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'loginad', "action"=>"loginadstatus/".$statusaction."/".$loginad['Loginad']['id']."/".$currentpagenumber), array(
													'update'=>'#Loginadpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>'vtip',
													'title'=>$statustext
												));?>
											</li>
										    <?php } ?>
										    <?php if(!isset($SubadminAccessArray) || in_array('loginad',$SubadminAccessArray) || in_array('loginad/adminpanel_loginad', $SubadminAccessArray)){ ?>
											<li>
												<?php 
												echo $this->Js->link($this->html->image('view-members.png', array('alt'=>'View Members')).' View Members', array('controller'=>'loginad', "action"=>"loginad/".$loginad['Loginad']['id']), array(
													'update'=>'#Loginadpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>'vtip',
													'title'=>'View Members'
												)); ?>
											</li>
											<?php } ?>
											
											<?php if(!isset($SubadminAccessArray) || in_array('loginad',$SubadminAccessArray) || in_array('loginad/adminpanel_loginadremove', $SubadminAccessArray)){ ?>
											<li>
												<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Login Ads Plan')).' Delete Login Ads Plan', array('controller'=>'loginad', "action"=>"loginadremove/".$loginad['Loginad']['id']."/".$currentpagenumber), array(
													'update'=>'#Loginadpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>'vtip',
													'title'=>'Delete Login Ads Plan',
													'confirm'=>"Do You Really Want to Delete This Login Ads Plan?"
												));?>
											</li>
											<?php } ?>
									</ul>
								  </div>
								</div>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($loginads)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Loginad']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Loginad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'loginad','action'=>'plan/0/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#Loginadpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'loginad',
			  'action'=>'plan/0/rpp',
			  'url'   => array('controller' => 'loginad', 'action' => 'plan/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
    </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#Loginadpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>