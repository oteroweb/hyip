<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($EnableLoginad==1) { ?>
<?php if(!$ajax){ ?>
<div id="Loginadpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<?php // Code for showing free available plans start
if(isset($pending_free_plans) && count($pending_free_plans)){
?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Free Plans Available");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php //free Plans table starts here ?>
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam"><?php echo __('Plan Name');?></div>
				<div class="divth textcenter vam"><?php echo __("Action");?></div>
			</div>
	        </div>
	        <div class="divtbody">
		        <tr><td colspan="8" class="ovw-padding-tabal"></td></tr>
		        <?php
		        $i = 0;
		        foreach ($pending_free_plans as $pending_free_plan):
			       if($i%2==0){$class='white-color';}else{$class='gray-color';}
			       ?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $pending_free_plan['Loginad']['plan_name'];?></div>
					<div class="divtd textcenter vam">
					<?php
						echo $this->Js->link('<span class="purchasebutton">Purchase</span>', array('controller'=>'loginad', "action"=>"getfreeloginad/".$pending_free_plan['Pending_free_plan']['id']), array(
							'update'=>'#Loginadpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Get it')
						));
					?>
					</div>
				</div>
		        <?php $i++;endforeach; ?>
	       </div>
	</div>
	<?php //free Plans table ends here ?>
</div>
<div class="height5"></div>
<?php } // Code for showing free available plans end ?>

<?php // Top menu code starts here ?>
<div class="comisson-bg">
	<div class="commison-menu">
		<ul>
			<li>
				<?php
					echo $this->Js->link(__('Popup').' '.__('Login Ads'), array('controller'=>'loginad', "action"=>"index"), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>''
					));
				?>
			</li>
			<li>
				<?php
					echo $this->Js->link(__('Surfbar').' '.__('Login Ads'), array('controller'=>'loginad', "action"=>"surfbar"), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'act'
					));
				?>
			</li>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Top menu code ends here ?>
<div class="main-box-eran">
	<?php // Surfbar Login ads table starts here ?>
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam"><?php echo __('Plan Name');?></div>
				<div class="divth textcenter vam"><?php echo __('Price');?></div>
				<div class="divth textcenter vam"><?php echo __("Action");?></div>
			</div>
		</div>
		<div class="divtbody">
			<?php
			$i = 0;
			foreach ($Loginaddata as $Loginad):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}
				?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $Loginad['Loginad']['plan_name'];?></div>
					<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($Loginad['Loginad']['price']*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
					<div class="divtd textcenter vam">
					<?php
						echo $this->Js->link('<span class="purchasebutton">Purchase</span>', array('controller'=>'loginad', "action"=>"purchase/".$Loginad['Loginad']['id']."/2"), array(
							'update'=>'#Loginadpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Purchase')
						));
					?>
					</div>
				</div>
			<?php $i++;endforeach; ?>
		</div>
	</div>
	<?php if(count($Loginaddata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
</div>
	<?php // Surfbar Login ads table ends here ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('My Login Ad Plans');?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#Loginadpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'loginad', 'action'=>'surfbar')
	));
	$currentpagenumber=$this->params['paging']['Loginadadd']['page'];
	?>
		<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			
			<?php // My Surfbar Login ads table starts here ?>
			<div class="divtable">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam">
							<?php 
							if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
							echo $this->Js->link(__("Id"), array('controller'=>'loginad', "action"=>"surfbar/0/id/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#Loginadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Id')
							));?>
						</div>
						<div class="divth textcenter vam"><?php echo __("Plan"); ?></div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Title"), array('controller'=>'loginad', "action"=>"surfbar/0/title/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#Loginadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Title')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Purchase Date"), array('controller'=>'loginad', "action"=>"surfbar/0/pruchase_date/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#Loginadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Purchase Date')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Approved Date"), array('controller'=>'loginad', "action"=>"surfbar/0/approve_date/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#Loginadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Approved Date')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Expiry Date"), array('controller'=>'loginad', "action"=>"surfbar/0/expire_date/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#Loginadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Expiry Date')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Displayed"), array('controller'=>'loginad', "action"=>"surfbar/0/display_counter/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#Loginadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Displayed')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__("Clicked"), array('controller'=>'loginad', "action"=>"surfbar/0/click_counter/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#Loginadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Clicked')
							));?>
						</div>
						<div class="divth textcenter vam"><?php echo __("Status"); ?></div>
						<div class="divth textcenter vam"><?php echo __("Action"); ?></div>
					</div>
				</div>
				<div class="divtbody">
					<?php $i=1;
					foreach ($loginads as $loginad):
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam"><?php echo $loginad['Loginadadd']['id'];?></div>
							<div class="divtd textcenter vam">
								<a href="#" class='vtip' title="<?php echo __('Plan Name'); ?> : <?php echo $loginad['Loginad']['plan_name'];?>"><?php echo $loginad['Loginadadd']['plan_id'];?></a>
							</div>
							<div class="divtd textcenter vam"><a href="<?php echo $loginad['Loginadadd']['site_url'] ; ?>" target='_blank' ><?php echo $loginad['Loginadadd']['title'] ; ?></a></div>
							<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $loginad['Loginadadd']['pruchase_date']); ?></div>
							<div class="divtd textcenter vam"><?php if($loginad['Loginadadd']['expire_date'] != '0000-00-00 00:00:00'){ echo $this->Time->format($SITECONFIG["timeformate"], $loginad['Loginadadd']['approve_date']); } else { echo '-';} ?></div>
							<div class="divtd textcenter vam"><?php if($loginad['Loginadadd']['expire_date'] != '0000-00-00 00:00:00'){ echo $this->Time->format($SITECONFIG["timeformate"], $loginad['Loginadadd']['expire_date']); } else { echo '-';} ?></div>
							<div class="divtd textcenter vam"><?php echo $loginad['Loginadadd']['display_counter'];?></div>
							<div class="divtd textcenter vam"><?php echo $loginad['Loginadadd']['click_counter'];?></div>
							<div class="divtd textcenter vam">
								<?php if ($loginad['Loginadadd']['expire_date'] < date('Y-m-d H:i:s') && $loginad['Loginadadd']['expire_date'] != '0000-00-00 00:00:00' && $loginad['Loginadadd']['expire_date'] != NULL) { 
									echo __("Expired");
								}
								elseif($loginad['Loginadadd']['status']==1) {
									echo __("Running");
								}
								elseif($loginad['Loginadadd']['status']==0){
									echo __("Pending");
								}
								?>
							</div>
							<div class="divtd textcenter vam">
								<?php
									echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit Login Ad'))), array('controller'=>'loginad', "action"=>"add/".$loginad['Loginadadd']['id']."/2"), array(
										'update'=>'#Loginadpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false,
										'class'=>'vtip',
										'title'=>__('Edit Login Ad')
									));
								?>
							</div>
						</div>
					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($loginads)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // My Surfbar Login ads table ends here ?>
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Loginadadd']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
	<div class="pag-float-left">
		<div class="ul-bg">
			<ul class="nice_paging">
			<?php 
			foreach($resultperpage as $rpp)
			{
				?>
				<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
					<?php 
					echo $this->Form->create('Loginad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'loginad','action'=>'surfbar/rpp')));
					echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
				
					echo $this->Js->submit($rpp, array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#Loginadpage',
					  'class'=>'resultperpagebutton',
					  'div'=>false,
					  'controller'=>'loginad',
					  'action'=>'surfbar/rpp',
					  'url'   => array('controller' => 'loginad', 'action' => 'surfbar/rpp')
					));
					echo $this->Form->end();
					?>
				</li>
				<?php 
			}?>
			</ul>
		<div class="clear-both"></div>
		</div>
	</div>
	<?php }?>
	<div class="floatright ul-bg">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
		</ul>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>
	
</div>
</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>