<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Login Ads</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Login Ads", array('controller'=>'loginad', "action"=>"loginad"), array(
					'update'=>'#Loginadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Login Ad Plans", array('controller'=>'loginad', "action"=>"plan"), array(
					'update'=>'#Loginadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="Loginadpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Login_Ads#Login_Ads" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Loginadadd',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'loginad','action'=>'loginad')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
					<?php 
					echo $this->Form->input('searchby', array(
						  'type' => 'select',
						  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'plan_id'=>'Plan Id', 'popup'=>'Popup Plan', 'surfbar'=>'Surfbar Plan', 'member_id'=>'Member Id', 'title'=>'Login Ad Title',  'display_counter'=>'Displayed', 'click_counter'=>'Clicked', 'running'=>'Running Login Ads', 'expire'=>'Expire Login Ads',  'active'=>'Active Login Ads', 'inactive'=>'Inactive Login Ads'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => '',
						  'onchange'=>'if($(this).val()=="surfbar" || $(this).val()=="popup" || $(this).val()=="inactive" || $(this).val()=="active" || $(this).val()=="running" || $(this).val()=="expire"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);}'
					));
					?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=="surfbar" || $searchby=="popup" || $searchby=="inactive" || $searchby=="active" || $searchby=='expire' || $searchby=='running'){ echo 'style="display:none"';} ?>>
			<span>Search For :</span>
			<span class="searchforfields_s"><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#Loginadpage',
				'class'=>'searchbtn',
				'controller'=>'loginad',
				'action'=>'loginad/'.$planid,
				'url'=> array('controller' => 'loginad', 'action' => 'loginad/'.$planid)
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->


	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#Loginadpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'loginad', 'action'=>'loginad/'.$planid)
	));
	$currentpagenumber=$this->params['paging']['Loginadadd']['page'];
	?>

<div id="gride-bg">
    <div class="Xpadding10">
			
    <div class="records records-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Loginadadd',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'loginad','action'=>'loginad')));?>
	
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'loginad', "action"=>"loginad/".$planid."/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('M. Id', array('controller'=>'loginad', "action"=>"loginad/".$planid."/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Purchase Date', array('controller'=>'loginad', "action"=>"loginad/".$planid."/0/pruchase_date/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Purchase Date'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Plan Id', array('controller'=>'loginad', "action"=>"loginad/".$planid."/0/plan_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Id'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Ad. Type', array('controller'=>'loginad', "action"=>"loginad/".$planid."/0/ptype/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Advertisement Type'
					));?>
				</div>
                <div><?php echo 'Login Ad Plan Name';?></div>
                <div>
					<?php 
					echo $this->Js->link('Login Ad Title', array('controller'=>'loginad', "action"=>"loginad/".$planid."/0/title/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Login Ad Title'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Displayed', array('controller'=>'loginad', "action"=>"loginad/".$planid."/0/display_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Displayed'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Clicked', array('controller'=>'loginad', "action"=>"loginad/".$planid."/0/click_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#Loginadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Clicked'
					));?>
				</div>
                <div><?php echo 'Action';?></div>
			</div>
			<?php foreach ($loginads as $loginad): ?>
				<div class="tablegridrow">
					<div><?php echo $loginad['Loginadadd']['id']; ?></div>
					<div>
						<?php 
						echo $this->Js->link($loginad['Loginadadd']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$loginad['Loginadadd']['member_id']."/top/loginad/index~top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>
					<?php echo $this->Time->format($SITECONFIG["timeformate"], $loginad['Loginadadd']['pruchase_date']);
					$tk='<b>Approved Date :</b> '.$this->Time->format($SITECONFIG["timeformate"], $loginad['Loginadadd']['approve_date']).'<br /><b>Expiry Date :</b> '.$this->Time->format($SITECONFIG["timeformate"], $loginad['Loginadadd']['expire_date']);?>
					<a class="vtip" title='<?php echo $tk;?>'><?php echo $this->html->image('information.png', array('alt'=>'', 'align' =>'absmiddle'));?></a>
					</div>
					<div><?php echo $loginad['Loginadadd']['plan_id']; ?></div>
					<div><?php echo ($loginad['Loginadadd']['ptype']==1)?"Popup":"Surfbar"; ?></div>
					<div><?php echo $loginad['Loginad']['plan_name']; ?></div>
					<div><a href="<?php echo $loginad['Loginadadd']['site_url'] ; ?>" target='_blank' ><?php echo $loginad['Loginadadd']['title'] ; ?></a></div>
					<div><?php echo $loginad['Loginadadd']['display_counter']; ?></div>
					<div><?php echo $loginad['Loginadadd']['click_counter']; ?></div>
					<div>
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
										
											<?php if(!isset($SubadminAccessArray) || in_array('loginad',$SubadminAccessArray) || in_array('loginad/adminpanel_loginadadd/$', $SubadminAccessArray)){ ?>
											<li>
												<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Login Ad'))." Edit", array('controller'=>'loginad', "action"=>"loginadadd/".$planid."/".$loginad['Loginadadd']['id']), array(
													'update'=>'#Loginadpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>''
												));?>
											</li>
											<?php } ?>
										
										   <?php if(!isset($SubadminAccessArray) || in_array('loginad',$SubadminAccessArray) || in_array('loginad/adminpanel_loginadaddstatus', $SubadminAccessArray)){ ?>
										   <li>
												<?php
												if($loginad['Loginadadd']['status']==0){
													$statusaction='1';
													$statusicon='red-icon.png';
													$statustext='Approve';
												}else{
													$statusaction='0';
													$statusicon='blue-icon.png';
													$statustext='Disapprove';}
												echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'loginad', "action"=>"loginadaddstatus/".$planid."/".$statusaction."/".$loginad['Loginadadd']['id']."/".$currentpagenumber), array(
													'update'=>'#Loginadpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>''
												));?>
											</li>
										   <?php } ?>
										   
											<?php if(!isset($SubadminAccessArray) || in_array('loginad',$SubadminAccessArray) || in_array('loginad/adminpanel_loginadaddremove', $SubadminAccessArray)){ ?>
											<li>
												<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Login Ad'))." Delete", array('controller'=>'loginad', "action"=>"loginadaddremove/".$planid."/".$loginad['Loginadadd']['id']."/".$currentpagenumber), array(
													'update'=>'#Loginadpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>'',
													'confirm'=>"Do You Really Want to Delete This Login Ad?"
												));?>
											</li>
											<?php } ?>
									</ul>
								  </div>
								</div>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($loginads)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Loginadadd']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Loginadadd',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'loginad','action'=>'loginad/'.$planid.'/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#Loginadpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'loginad',
			  'action'=>'loginad/'.$planid.'/rpp',
			  'url'   => array('controller' => 'loginad', 'action' => 'loginad/'.$planid.'/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#Loginadpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>