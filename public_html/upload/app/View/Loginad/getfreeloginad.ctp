<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 24-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('framechecker');?>
<?php if($EnableLoginad==1) { ?>
<?php if(!$ajax){ ?>
<div id="Loginadpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<?php //Frame Break Code Start ?>
<input type="hidden" id='framebreakmessage' value='<?php echo __('Site URL Not Valid'); ?>|<?php echo __('Breaking out of frames'); ?>|<?php echo __('Inspecting Website'); ?>|<?php echo __('Framebreaker test passed'); ?>|<?php echo __('Error').' : '; ?>'/>
<div id="check" class="frambreakmain">
	<div class='textleft'>
		<span id='framebreakimg'></span>
		<span class="framebreaktext"><?php echo __('Inspecting Framebreaker'); ?></span>
	</div>
	<div class='textleft'>
		<span id="check_final"><?php echo __('Inspecting Website'); ?></span>
	</div>
</div>
<?php //Frame Break Code Over ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Get Free Login Ad");?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
			<?php // Purchase Login Ads form starts here ?>
			<?php echo $this->Form->create('Loginadadd',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'loginad','action'=>'getfreeloginadaction/'.$pending_free_plan_id)));?>
			<div class="form-box">
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Plan Name");?> : </div>
					<div class="form-col-2 form-text"><?php echo $Loginaddata['Loginad']['plan_name'];?></div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Days");?> : </div>
					<div class="form-col-2 form-text"><?php echo $Loginaddata['Loginad']['days'];?> days</div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Advertisement Type");?> : </div>
					<div class="form-col-2 form-text"><?php echo ($Loginaddata['Loginad']['loginad_static']==1)? __('Static') : __('Rotating') ;?></div>
				</div>
				
				<?php if($Loginaddata['Loginad']['fpcommission']==1){?>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Pay Commission For The First Position Only");?> : </div>
					<div class="form-col-2 form-text"><?php echo __("Yes");?></div>
				</div>
				<?php }?>
				
				<?php $commissionlevel=explode(",", $Loginaddata['Loginad']['commissionlevel']);if($commissionlevel[0]>0){?>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Referral Commission Structure");?> : </div>
					<div class="form-col-2 form-text">
						<?php $commilev="";for($i=0;$i<count($commissionlevel);$i++){if($commissionlevel[$i]>0){$commilev.="Level ".($i+1)." : ".$commissionlevel[$i]."%, ";}else{break;}}echo trim($commilev,", ")?>
					</div>
				</div>
				<?php }?>
				
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Next Available Date");?> : </div>
					<div class="form-col-2 form-text">
					<?php if($Loginaddata['Loginad']['loginad_static']==1) { ?>
					<?php if(@$Availabledaye['Loginadadd']['expire_date']=='0000-00-00 00:00:00' || @$Availabledaye['Loginadadd']['expire_date']== NULL) {
						echo __('Available From Today');
					 }
					 else {
						echo $this->Time->format($SITECONFIG["timeformate"], $Availabledaye['Loginadadd']['expire_date']); 
					 } ?>
					<?php } else { 
						echo $this->Time->format($SITECONFIG["timeformate"], $Availabledaye);
					 } ?>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Title");?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('title', array('type'=>'text','div'=>false, 'label' => false, 'class'=>'formtextbox'));?>
					<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?>"></span>
					</div>
					
					
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Site URL");?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('site_url', array('type'=>'text', 'label' => false, 'value' => 'http://', 'class'=>'formtextbox txtframebreaker'));?></div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Description");?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('description', array('type'=>'textarea', 'label'=>false,'title'=>__('Description'), 'class'=>'formtextarea '));?></div>
				</div>
				<div class="height10"></div>
				<div>
					<div class="purchaseplanbuttonbox floatright" style="width:50%;">
						<div class="form-box">
							<div class="profile-bot-left floatright"><?php if($SITECONFIG["enableagree"]==1){echo $this->Form->input('termsandconditions', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>'')); echo ' <a href="'.$SITEURL.'public/terms" target="_blank">'.__('I agree with the terms and conditions').'</a> '.__('of').' '.$SITECONFIG["sitetitle"];} ?></div>
							<div class="clear-right"></div>
							<div class="formbutton">
								<?php if(@$type==1){$action='index';}else{$action='surfbar';} ?>
								
								<input type="submit" value="<?php echo __('Purchase'); ?>" class="button ml10" onclick="return checkframebreaker(0,'<?php echo $SITEURL; ?>','framebreaker');" />
								<?php echo $this->Js->submit(__('Purchase'), array(
									'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'update'=>'#UpdateMessage',
									'class'=>'button framebreaker',
									'div'=>false,
									'style'=>'display:none',
									'controller'=>'loginad',
									'action'=>'getfreeloginadaction/'.$pending_free_plan_id,
									'url'   => array('controller' => 'loginad', 'action' => 'getfreeloginadaction/'.$pending_free_plan_id)
								));?>
								<?php echo $this->Js->link(__("Back"), array('controller'=>'loginad', "action"=>$action), array(
									'update'=>'#Loginadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'button',
									'style'=>''
								));?>
							</div>
							<div class="clear-both"></div>	
						</div>
					</div>
					<div class="clear-both"></div>
				</div>
			</div>
			<div class="clear-both"></div>
			<?php echo $this->Form->end();?>
			<?php // Purchase Login Ads form ends here ?>
			
		</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>