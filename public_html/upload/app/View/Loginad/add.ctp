<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('framechecker');?>
<?php if($EnableLoginad==1) { ?>
<?php if(!$ajax){ ?>
<div id="Loginadpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>

<?php //Frame Break Code Start ?>
<input type="hidden" id='framebreakmessage' value='<?php echo __('Site URL Not Valid'); ?>|<?php echo __('Breaking out of frames'); ?>|<?php echo __('Inspecting Website'); ?>|<?php echo __('Framebreaker test passed'); ?>|<?php echo __('Error').' : '; ?>'/>
<div id="check" class="frambreakmain">
	<div class='textleft'>
		<span id='framebreakimg'></span>
		<span class="framebreaktext"><?php echo __('Inspecting Framebreaker'); ?></span>
	</div>
	<div class='textleft'>
		<span id="check_final"><?php echo __('Inspecting Website'); ?></span>
	</div>
</div>
<?php //Frame Break Code Over ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Login Ads");?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
		<div class="height10"></div>	
			
			<?php // Login ad edit form starts here ?>
			<?php echo $this->Form->create('Loginadadd',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'loginad','action'=>'addaction')));?>
				<div class="form-box">
					<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$Loginaddata['Loginadadd']["id"], 'label' => false)); ?>
					<div class="form-row">
						<div class="form-col-1"><?php echo __("Title");?> : <span class="required">*</span></div>
						<?php echo $this->Form->input('title', array('type'=>'text', 'label' => false, 'value'=>$Loginaddata['Loginadadd']["title"], 'class'=>'formtextbox login-from-box-1','div'=>false));?>
						
						<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?>"></span>
						
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __("Site URL");?> : <span class="required">*</span></div>
						<?php echo $this->Form->input('site_url', array('type'=>'text', 'label' => false,'div'=>false, 'value'=>$Loginaddata['Loginadadd']["site_url"], 'class'=>'formtextbox txtframebreaker'));?>
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __("Description");?> : <span class="required">*</span></div>
						<?php echo $this->Form->input('description', array('type'=>'textarea', 'label'=>false,'div'=>false, 'value'=>stripslashes($Loginaddata['Loginadadd']["description"]),'title'=>__('Description'), 'class'=>'formtextarea '));?>
					</div>
				</div>
			<div class="height10"></div>
			<div class="formbutton">
				<?php if($type==1){$action='index';}else{$action='surfbar';} ?>
				<?php echo $this->Js->link(__('Back'), array('controller'=>'loginad', "action"=>$action), array(
					'update'=>'#Loginadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'button',
					'style'=>'float:left'
				));?>
			
				<input type="submit" value="<?php echo __('Update'); ?>" class="button" onclick="return checkframebreaker(0,'<?php echo $SITEURL; ?>','framebreaker');" />
				<?php echo $this->Js->submit(__('Update'), array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#UpdateMessage',
					'class'=>'button framebreaker',
					'div'=>false,
					'style'=>'display:none',
					'controller'=>'loginad',
					'action'=>'addaction',
					'url'   => array('controller' => 'loginad', 'action' => 'addaction')
				));?>
			</div>
			<div class="clear-both"></div>
			<?php echo $this->Form->end();?>
			<?php // Login ad edit form ends here ?>
			
		</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>