<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 24-09-2014
  *********************************************************************/
?>
<?php // This file is used to show popup login ad when member logs in to his account ?>
<script>
	$(document).ready(function(){
		$().colorbox({width:"90%", height:"90%", href:"#colorboxpopuplogin", inline:true, open:true, onClosed:function(){loginaddisplayset()}, overlayClose:false, escKey:false, closeButton:false, fixed:true});
		[LOGINADFUNCTION]
		$("#cboxContent").addClass("colorboxpopuplogin").find("#cboxClose").css( "visibility", "hidden" )
	});
</script>
<script type="text/javascript">
	function loginaddisplayset()
	{
		$.ajax({
			type: "POST",
			evalScripts: true,
			url: "[LOGINADURL]",
			data: ({type:"original"}),
			success: function (data, textStatus){
			}
		});
	}
</script>
<script type="text/javascript">
$(document).ready(function(){
	$("#loginiframe").height($("#cboxContent").height()-$("#pop-box-bg").height()-133);		
});
</script>
<style type="text/css">#cboxLoadedContent{padding:0;}
#pop-box-bg { background-color: #168aa3; border-bottom: solid  3px #004d5e; }
.box-left-pop {padding-top: 4px; width: 47%; margin-left: 3%; float: left; padding-bottom: 6px; }
.box-right-pop { width: 47%; margin-right: 3%; float: left; text-align: right; padding-top: 10px; }
.box-in-pop-box {margin-top: 5px;font-size:12px; color: #ffffff;  font-family: 'Open Sans'; padding: 5px;background-color: #1a93ad; border: solid 1px #43b2ca;-moz-border-radius:5px 5px 5px 5px;-webkit-border-radius:5px 5px 5px 5px;-khtml-border-radius:5px 5px 5px 5px;border-radius:5px 5px 5px 5px; }
.pop-title-text{ margin-left: 3%;  font-size: 18px; color: #ffffff; font-weight: bold; font-family: 'Open Sans'; text-shadow: 1px 1px 1px #004959; }
</style>
<div style="display:none">
	<div id="colorboxpopuplogin" class="MeaasageAdContent">
		<div id="LoginAdtimer">[SECONDWAIT]</div>
		<div id="pop-box-bg">
			<div class="pop-title-text">[TITLE]</div>
			<div id="loginadcontent" class="box-left-pop">
				<div class=" box-in-pop-box">[DESCRIPTION]</div>
			</div>
			<div class="box-right-pop">
				<a href="[SITEURL]loginad/creditcounter/[ID]" target="_blank"><img src="[SITEURL]img/open-button.jpg" border="0" width="230" height="38" /></a>
			</div>
			<div class="clear-both"></div>
		</div>
		[IFRAME]	
		<script type="text/javascript">
		var startcounter=0;	
		function login_counter()
		{
			if(startcounter!=1)
			{
				startcounter=1;
				LoginAdsTimer([SECONDWAIT], "#LoginAdtimer");
			}
		}
		function maxwaitseconds(number)
		{
			number--;
			if (number == 0)
			{
				if(startcounter!=1)
				{
					startcounter=1;
					LoginAdsTimer([SECONDWAIT], "#LoginAdtimer");
				}
			}
			else
			{
				setTimeout ("maxwaitseconds("+number+")",1000);
			}
		}
		</script>
	</div>
</div>