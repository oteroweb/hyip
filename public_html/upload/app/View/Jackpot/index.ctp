<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 15-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($EnableJackpot==1) { ?>
<?php if(!$ajax){ ?>
<div id="jackpotpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Jackpot');?></div>
	<div class="clear-both"></div>
</div>

<div id="UpdateMessage"></div>
<div class="main-box-eran">
	<div class="clear-both"></div>
	<div class="height5"></div>
		
		<?php // Banner Ad Plans table starts here ?>
		<div class="divtable">
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam"><?php echo __('Plan Name');?></div>
					<div class="divth textcenter vam"><?php echo __('Price');?></div>
					<div class="divth textcenter vam"><?php echo __("Action");?></div>
				</div>
	       </div>
	       <div class="divtbody">
		       <tr><td colspan="8" class="ovw-padding-tabal"></td></tr>
		       <?php
		       $i = 0;
		       foreach ($Jackpotdata as $Jackpot):
			       if($i%2==0){$class='white-color';}else{$class='gray-color';}
			       ?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $Jackpot['Jackpot']['nm'];?></div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($Jackpot['Jackpot']['price']*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
						<div class="divtd textcenter vam">
						<?php
							echo $this->Js->link('<span class="purchasebutton">Purchase</span>', array('controller'=>'jackpot', "action"=>"purchase/".$Jackpot['Jackpot']['id']), array(
								'update'=>'#jackpotpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Purchase')
							));
						?>
						</div>
					</div>
		       <?php $i++;endforeach; ?>
	       </div>
	</div>
	<?php if(count($Jackpotdata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
	<?php // Banner Ad Plans table ends here ?>
</div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('My Jackpots');?></div>
	<div class="clear-both"></div>
</div>
	
<div class="main-box-eran">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#jackpotpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'jackpot', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Jackpotmember']['page'];
	?>
	<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
	<div class="clear-both"></div>
	<div class="height5"></div>
		
	<?php // My Banner Ad Plans table starts here ?>
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam">
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link(__("Id"), array('controller'=>'jackpot', "action"=>"index/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#jackpotpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Id')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__("Jackpot Id"), array('controller'=>'jackpot', "action"=>"index/0/jackpot_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#jackpotpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Plan')
					));?>
				</div>
				<div class="divth textcenter vam"><?php echo __("Jackpot Name");?></div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__("Purchase Date"), array('controller'=>'jackpot', "action"=>"index/0/pruchase_date/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#jackpotpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Purchase Date')
					));?>
				</div>
			</div>
		</div>
		<div class="divtbody">
			<?php $i=1;
			foreach ($jackpotmembers as $jackpotmember):
			if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $jackpotmember['Jackpotmember']['id'];?></div>
					<div class="divtd textcenter vam"><?php echo $jackpotmember['Jackpotmember']['jackpot_id']; ?></div>
					<div class="divtd textcenter vam"><?php echo $jackpotmember['Jackpot']['nm']; ?></div>
					<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $jackpotmember['Jackpotmember']['purchase_date']); ?></div>
				</div>
			<?php $i++;endforeach; ?>
		</div>
	</div>
	<?php if(count($jackpotmembers)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
	<?php // My Banner Ad Plans table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Jackpotmember']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
	<div class="pag-float-left">
		<div class="ul-bg">
			<ul class="nice_paging">
			<?php 
			foreach($resultperpage as $rpp)
			{
				?>
				<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
					<?php 
					echo $this->Form->create('Jackpot',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'jackpot','action'=>'index/rpp')));
					echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
				
					echo $this->Js->submit($rpp, array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#jackpotpage',
					  'class'=>'resultperpagebutton',
					  'div'=>false,
					  'controller'=>'jackpot',
					  'action'=>'index/rpp',
					  'url'   => array('controller' => 'jackpot', 'action' => 'index/rpp')
					));
					echo $this->Form->end();
					?>
				</li>
				<?php 
			}?>
			</ul>
		<div class="clear-both"></div>
		</div>
	</div>
	<?php }?>
	<div class="floatright ul-bg">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
		</ul>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>
	
</div>

<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>