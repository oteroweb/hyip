<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 25-03-2015
  * Last Modified: 25-03-2015
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Jackpot</div>
<div id="jackpotpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Jackpot" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Jackpot',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'jackpot','action'=>'index')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
					<?php 
					echo $this->Form->input('searchby', array(
						  'type' => 'select',
						  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'numofwinner'=>'Number of winner', 'active'=>'Active Jackpot', 'inactive'=>'Inactive Jackpot', 'running'=>'Running Jackpot', 'expire'=>'Expire Jackpot'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => '',
						  'onchange'=>'if($(this).val()=="inactive" || $(this).val()=="active" || $(this).val()=="expire" || $(this).val()=="running"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);}'
					));
					?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=="inactive" || $searchby=="active" || $searchby=='expire' || $searchby=='running'){ echo 'style="display:none"';} ?>>
			<span>Search For :</span>
			<span class="searchforfields_s"><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#jackpotpage',
				'class'=>'searchbtn',
				'controller'=>'jackpot',
				'action'=>'index',
				'url'=> array('controller' => 'jackpot', 'action' => 'index')
			      ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#jackpotpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'jackpot', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Jackpot']['page'];
	?>
	
<div id="Xgride-bg">
	<div class="height10"></div>
    <div class="padding10 backgroundwhite">
		<div class="greenbottomborder">
		<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
				
    <?php if(!isset($SubadminAccessArray) || in_array('jackpot',$SubadminAccessArray) || in_array('jackpot/adminpanel_add',$SubadminAccessArray)){ ?>
            <?php echo $this->Js->link("+ Add New", array('controller'=>'jackpot', "action"=>"add"), array(
                'update'=>'#jackpotpage',
                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                'escape'=>false,
                'class'=>'btnorange'
            ));?>
    <?php } ?>
	</div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'jackpot', "action"=>"index/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#jackpotpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Jackpot Name', array('controller'=>'jackpot', "action"=>"index/0/0/nm/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#jackpotpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Jackpot Name'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Start Date', array('controller'=>'jackpot', "action"=>"index/0/0/startdate/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#jackpotpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Start Date'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('End Date', array('controller'=>'jackpot', "action"=>"index/0/0/enddate/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#jackpotpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By End Date '
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Number of winner', array('controller'=>'jackpot', "action"=>"index/0/0/numofwinner/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#jackpotpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Number of winner'
					));?>
				</div>
				<div><?php echo 'Winner<br /> Ticket id';?></div>
				<div><?php echo 'Status';?></div>
				<div><?php echo 'Action';?></div>
			</div>
			<?php foreach ($jackpots as $jackpot): ?>
				<div class="tablegridrow">
					<div><?php echo $jackpot['Jackpot']['id']; ?></div>
					<div><?php echo $jackpot['Jackpot']['nm']; ?></div>
					<div><?php echo trim($this->Time->format($SITECONFIG["timeformate"], $jackpot['Jackpot']['startdate']),' 00:00:00'); ?></div>
					<div><?php echo trim($this->Time->format($SITECONFIG["timeformate"], $jackpot['Jackpot']['enddate']),' 00:00:00'); ?></div>
					<div><?php echo $jackpot['Jackpot']['numofwinner']; ?></div>
					<div><?php echo ($jackpot['Jackpot']['winnerid']!=NULL)?$jackpot['Jackpot']['winnerid']:'-'; ?></div>
					<div><?php if($jackpot['Jackpot']['enddate']<date('Y-m-d')){ echo 'Expire';}else{ echo 'Running';} ?></div>
					<div>
					       <div class="actionmenu">
						 <div class="btn-group">
						       <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							       Action <span class="caret"></span>
						       </button>
						       <ul class="dropdown-menu" role="menu">
									       
							       <?php if(!isset($SubadminAccessArray) || in_array('jackpot',$SubadminAccessArray) || in_array('jackpot/adminpanel_add/$',$SubadminAccessArray)){?>
							       <li>
							       <?php
								       echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Jackpot'))." Edit", array('controller'=>'jackpot', "action"=>"add/".$jackpot['Jackpot']['id']), array(
									       'update'=>'#jackpotpage',
									       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									       'escape'=>false,
									       'class'=>''
								       ));?>
							       </li>
							       <?php }?>
					       
							       <?php 
							       if(!isset($SubadminAccessArray) || in_array('jackpot',$SubadminAccessArray) || in_array('jackpot/adminpanel_status',$SubadminAccessArray)){?>
							       <li>
							       <?php
								       if($jackpot['Jackpot']['status']==0)
								       {
									       $field_reqaction='1';
									       $field_reqicon='red-icon.png';
									       $field_reqtext='Activate';
								       }
								       else
								       {
									       $field_reqaction='0';
									       $field_reqicon='blue-icon.png';
									       $field_reqtext='Inactivate';
								       }
								       echo $this->Js->link($this->html->image($field_reqicon, array('alt'=>$field_reqtext))." ".$field_reqtext, array('controller'=>'jackpot', "action"=>"status/status/".$field_reqaction."/".$jackpot['Jackpot']['id']."/".$currentpagenumber), array(
									       'update'=>'#jackpotpage',
									       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									       'escape'=>false,
									       'class'=>''
								       ));?>
							       </li>
							       <?php }?>
							        <?php 
							       if(!isset($SubadminAccessArray) || in_array('jackpot',$SubadminAccessArray) || in_array('jackpot/adminpanel_status',$SubadminAccessArray)){?>
							       <li>
							       <?php
								       if($jackpot['Jackpot']['allow_new_purchase']==0)
								       {
									       $field_reqaction='1';
									       $field_reqicon='close.png';
									       $field_reqtext='Allow New Purchases';
								       }
								       else
								       {
									       $field_reqaction='0';
									       $field_reqicon='open.png';
									       $field_reqtext='Deny New Purchases';
								       }
								       echo $this->Js->link($this->html->image($field_reqicon, array('alt'=>$field_reqtext))." ".$field_reqtext, array('controller'=>'jackpot', "action"=>"status/purchase/".$field_reqaction."/".$jackpot['Jackpot']['id']."/".$currentpagenumber), array(
									       'update'=>'#jackpotpage',
									       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									       'escape'=>false,
									       'class'=>''
								       ));?>
							       </li>
							       <?php }?>
							       
							       <?php if(!isset($SubadminAccessArray) || in_array('jackpot',$SubadminAccessArray) || in_array('jackpot/adminpanel_remove',$SubadminAccessArray)){?>
							       <li>
							       <?php
								       echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Jackpot'))." Delete", array('controller'=>'jackpot', "action"=>"remove/".$jackpot['Jackpot']['id']."/".$currentpagenumber), array(
									       'update'=>'#jackpotpage',
									       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									       'escape'=>false,
									       'confirm'=>'Are You Sure?',
									       'class'=>''
								       ));?>
							       </li>
							       <?php }?>
							       <li>
							       <?php
								       echo $this->Js->link($this->html->image('view-members.png', array('alt'=>'Member'))." Member", array('controller'=>'jackpot', "action"=>"member/".$jackpot['Jackpot']['id']), array(
									       'update'=>'#jackpotpage',
									       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									       'escape'=>false,
									       'class'=>''
								       ));?>
							       </li>
								<?php if($jackpot['Jackpot']['enddate']<date('Y-m-d') && $jackpot['Jackpot']['ispaid']==0){ ?>
								<li>
							       <?php
								       echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Pay To Winners'))." Pay To Winners", array('controller'=>'jackpot', "action"=>"pay/".$jackpot['Jackpot']['id']), array(
									       'update'=>'#jackpotpage',
									       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									       'escape'=>false,
									       'class'=>''
								       ));?>
							       </li>
							       <?php }?>
							       <?php if($jackpot['Jackpot']['winnerid']!=''){ ?>
								<li>
							       <?php
								       echo $this->Js->link($this->html->image('view-members.png', array('alt'=>'Jackpot Winners'))."Jackpot Winners", array('controller'=>'jackpot', "action"=>"winner/".$jackpot['Jackpot']['id']), array(
									       'update'=>'#jackpotpage',
									       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									       'escape'=>false,
									       'class'=>''
								       ));?>
							       </li>
							       <?php }?>
						       </ul>
						 </div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($jackpots)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php
	if($this->params['paging']['Jackpot']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Jackpot',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'jackpot','action'=>'index/0/rpp')));?>
		
		<div class="resultperpage">
                        <label>
			    <?php 
			    echo $this->Form->input('resultperpage', array(
			      'type' => 'select',
			      'options' => $resultperpage,
			      'selected' => $this->Session->read('pagerecord'),
			      'class'=>'',
			      'label' => false,
			      'div'=>false,
			      'style' => '',
			      'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			    ));
			    ?>
			</label>
		</div>
		
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#jackpotpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'jackpot',
			  'action'=>'index/0/rpp',
			  'url'   => array('controller' => 'jackpot', 'action' => 'index/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
		</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#jackpotpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>