<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/?>
<!DOCTYPE html>
<html lang="en-us">
<head>
	<title><?php echo __('Under Maintenance'); ?></title>
	<?php
		echo $this->Html->meta('icon');
		
		echo $this->Html->css('style');
		
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
	<meta charset="utf-8"></meta>
	<meta name="description" content=""></meta>
	<meta name="keywords" content=""></meta>
</head>
<body>
	<div class="um-header-bg">
		<div class="um-header"></div>
	</div>
	<div class="um-content">
		<div class="um-img"><?php echo $this->Html->image('um-img.png', array('alt' => ''))?></div>
		<div class="um-img-right">
			<div style="text-align: center;"><?php echo $this->Html->image('logo.png', array('alt' => $SITECONFIG['sitetitle'], 'width'=> 190, 'height'=> 50))?></div>
			<div class="um-text">sorry! this site is currently under construction</div>
			<div class="contenttext">
				<?php echo $SITECONFIG["maintenancemodetext"];?>
			</div>
		</div>
	</div>
	<div class="um-footer">Copyright &copy; <?php echo date("Y"); ?> <?php echo $SITECONFIG["sitetitle"];?> <?php echo __('All Rights Reserved'); ?>.<?php if($powered_by!='')echo ' | Powered by ';?><?php
			// Do not remove below code
			echo $powered_by;
			// Do not remove above code</div> ?>
</body>
</html>