<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/?>
<?php // To protect site from XSS and vulnerable uploads
header('X-XSS-Protection: 1; mode=block'); 
header('X-Content-Type-Options: nosniff');?>
<?php // This file is the main template file of the theme. Theme layout design related changes can be done from this file. ?>
<!DOCTYPE html>
<html lang="en-us">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
	<meta charset="utf-8"></meta>
	<title><?php echo __(@$title_for_layout); ?> - <?php echo __($SITECONFIG['sitetitle']);?></title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<script type="text/javascript">var themepath='<?php echo $SITEURL;?>theme/<?php echo $this->theme;?>/';</script>
	<?php echo $this->Html->script('jquery-1.8.3.min');?>
	<?php echo $this->Html->script('jquery-common');?>
	<?php echo $this->Html->css('keyboard');
	echo $this->Html->script('keyboard.min');?>
	
	<?php echo $this->Html->css('bootstrap.min');?>
	
	<?php echo $this->Html->css('style');?>
</head>
<body>
<div id="pleasewait"><?php echo $this->Html->image('wait.gif', array('alt' => 'CakePHP'))?></div>

	<div class="logo-main">
		<div class="logo"><?php echo $this->Html->image('logo.png', array('alt' => $SITECONFIG['sitetitle'], 'width'=> 190, 'height'=> 50));?></div>
	</div>
	<?php echo $this->fetch('content'); ?>
	

</body>
</html>