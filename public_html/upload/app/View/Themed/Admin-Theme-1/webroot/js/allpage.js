$(document).ready( function() {
	$("#tab ul li a").click(function() {
		$("#tab ul li").removeClass('active');
		$(this).parent("li").addClass('active');
	});
});

$(document).ready( function() {
	$("#tab-innar ul li a").click(function() {
		$("#tab-innar ul li a").removeClass('active');
		$(this).addClass('active');
	});
});

var fixhead = [];
$(document).ready(function(){
  $(".tablegridheader").each(function(index, element){
		if(!$(this).hasClass("nofixheader"))
		{
			fixhead[index]=[$(element).offset().top, $(this)];
		}
  });
})
$(document).ready(function(){
  $(window).scroll(function() {
	for( key in fixhead)
	{
		
		if ($(window).scrollTop() >= fixhead[key][0])
		{
		  fixhead[key][1].find('div').each(function(){
			$(this).css('width', $(this).width());
		  });
		  fixhead[key][1].parent('.tablegrid').find('.tablegridrow:first div').each(function(){
			$(this).css('width', $(this).width());
		  });
		  fixhead[key][1].addClass("headfixedposition");
		}
		else
		{
		  fixhead[key][1].removeClass("headfixedposition");
		}
	}
  });
});
$(document).ready(function(){
	$(".tooltipmain").tooltip({
		placement : 'bottom',
		trigger : 'click',
		html : true
	});
	$(".tooltipmain-2").tooltip({
		placement : 'top',
		trigger : 'click',
		html : true
	});
});
///////////////////////////////////////////
$(document).ready( function() {
	$(".satting-menu ul li a").click(function() {
		$(".satting-menu ul li a").removeClass('satting-menu-active');
		$(this).addClass('satting-menu-active');
	});
});
$(document).ready( function() {
	$(".helpicon").click(function() {
		if($(".helpinformation" + this.alt).css('display')=="none"){
			$('.helpinformation' + this.alt).slideDown("slow");
		}else{
			$('.helpinformation' + this.alt).slideUp("slow");
		}
	});
});
$(document).ready(function($){vtip();});
$(function() {
	$( ".datepicker" ).datepicker({
		showOn: "button",
		buttonImage: themepath+"img/calendar.jpg",
		buttonImageOnly: true,
		showAnim: 'clip', //slideDown,fadeIn,blind,bounce,clip,drop,fold,slide
		dateFormat: 'yy-mm-dd',
		showButtonPanel: true
	});
	
	$( ".datetimepicker" ).datetimepicker({
		showOn: "button",
		buttonImage: themepath+"img/calendar.jpg",
		buttonImageOnly: true,
		showAnim: 'clip', //slideDown,fadeIn,blind,bounce,clip,drop,fold,slide
		dateFormat: 'yy-mm-dd',
		timeFormat: 'HH:mm:ss',
		showButtonPanel: true
	});
});
$(".tinyMCEtriggerSavetk").hover(
	function () {tinyMCE.execCommand('mceRemoveControl', true, 'advancededitor');tinyMCE.triggerSave();},function () {}
);
$(document).ready(function(){
	$("a[rel='lightbox']").colorbox({width:"80%", height:"80%", iframe:false});
});

var isprogress;
$('input[type=submit]').mouseup(function() {
	$("html, body").animate({ scrollTop: 180 }, 'slow');
	isprogress=1;
	var nanobar = new Nanobar( options );
	nanobar.go(100);
});

$('#menu ul li ul li a').mouseup(function() {
	if ($(window).width()<700)
	{
		if($("#leftbar").is(".active_toggled")){
			$('#leftbar .leftmenutogglebutton').click();
		}
	}
});

if (isprogress!=1)
	nanobar.go(50);
$(document).ready(function(){
	if (isprogress!=1)
		nanobar.go(100);
	else
		isprogress=0;
});

/*Mobile View Grid Code Start*/
var resettableheader=$(".tablegrid.tableforremoveheader .tablegridheader").html();
$(function() {				
	checkScreen();
});
$( window ).resize(function() {
	checkScreen();
});
function checkScreen(){
	var width = $( window ).width();
	if (width<=769)
		initMobile();
	else
		resetMobile();
	if(this.console){
	  //console.log(width);
	}
}
function resetMobile(){
	$( ".tablegrid").each(function(i) {
		if ($(this).hasClass("mobile")){
			var divtext = "";
			$(this).find(".tablegridrow").each(function(i) {
				$(this).find("div").each(function(x){
					divtext = $(this).find("div:last").html();
					$(this).html(divtext);
				});			
			});
			$(".tablegrid.tableforremoveheader .tablegridheader").html(resettableheader);
		}
	});
	$( ".tablegrid").removeClass("mobile");
}
function initMobile(){
	if ($( ".tablegrid").hasClass("nomobilecss")===false){
		$( ".tablegrid").each(function(i) {
			if ($( ".tablegrid").hasClass("mobile")===false){
				var headers = [];
				var htext = "";
				var divtext = "";
				$(this).find(".tablegridheader div").each(function(i) {
					htext = $.trim($(this).html());
					if (htext!="") headers.push(htext);
				});
				$(this).find(".tablegridrow").each(function(i) {
					$(this).find("div").each(function(x){
					      if (headers[x]!=undefined) {
						$(this).html("<div>"+headers[x]+"</div><div>"+$(this).html()+"</div>");
					      }
					      else
					      {
						      $(this).html("<div></div><div>"+$(this).html()+"</div>");
					      }
					});			
				});
				$(".tablegrid.tableforremoveheader .tablegridheader").html(" ");
			}
		});
	  $( ".tablegrid").removeClass("mobile").addClass("mobile");
	}
}
/*Mobile View Grid Code Over*/