function getCookie(c_name)
{
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++)
    {
	    x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		x=x.replace(/^\s+|\s+$/g,"");
		if (x==c_name)
		{
		return unescape(y);
		}
    }
}
function setCookie(c_name,value,exdays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	//var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString())+"; path=/";
	document.cookie=c_name + "=" + c_value;
}
function eraseCookie(name) {
	setCookie(name,"",-1);
}
function SubmitFormAjax(frmID, frmURL, updateID, sucessURL)
{
	$("#pleasewait").show();
	
	var base=$(frmID);
	var params=base.serialize();
	$.ajax({
	type: "post",
	url: frmURL,
	data: params,
	success: function(response) {
		if(response){
			if(response=="Done" && sucessURL!='NULL'){window.location=sucessURL;}
			$(updateID).html(response).show(500);
			//$(updateID).html(response).show(500).delay(5000).hide(500);
		} else {
			//alert('Error in saved data.');
		}
		$("#pleasewait").hide();
	},
	error:function (XMLHttpRequest, textStatus, errorThrown) {
		alert(textStatus);
		$("#pleasewait").hide();
	}
	});
	return;
}
function AddTags(field,text)
{
	var taField = field;
	//IE support
	if (document.Selection)
	{
		taField.focus();
		sel = document.Selection.createRange ();
		sel.text = text;
	}
	//MOZILLA/NETSCAPE support
	else if (taField.SelectionStart || taField.SelectionStart == '0')
	{
		var startPos = taField.SelectionStart;
		var endPos = taField.SelectionEnd;
		taField.value = taField.value.substring (0, startPos) + text + taField.value.substring (endPos, taField.value.length);
	}
	else
	{
		taField.value += text;
	}
}
function selectAllCheckboxes(elementClass,status) {
	$("."+elementClass).each( function(key) {
		$(this).attr("checked",status);
	})
}
function generateorderform(confun,actionurl)
{
	var contnm = confun.substr(0, confun.length-1);
	var funcno = confun.substr(confun.length-1, 1);
	$.ajax({
		type: "POST",
		url: actionurl+"/"+contnm+"/makeorder/"+funcno,
		data: {id:1}
		}).done(function( html ) {
			$("#GenerateForm").html(html);
	});
}
function generatefreeorderform(confun,actionurl)
{
	var confunarray = confun.split('-');
	
	var contnm = confunarray[0].substr(0, confunarray[0].length-1);
	
	var funcno = confunarray[0].substr(confunarray[0].length-1, 1);
	$.ajax({
		type: "POST",
		url: actionurl+"/"+contnm+"/makefreeorder/"+funcno+"/"+confunarray[1],
		data: {id:1}
		}).done(function( html ) {
			$("#GenerateForm"+confun).html(html);
	});
}
function generatecombo2(comboboxname,model,fieldid,fieldname,selected,field,style,event,actionurl,condation)
{
	$.ajax({
		type: "POST",
		url: actionurl+"/app/generatecombo",
		data: {comboboxname:comboboxname,model:model,fieldid:fieldid,fieldname:fieldname, selected:selected,style:style,event:event,condation:condation}
		}).done(function( html ) {
			$(field).html(html);
	});
}
function generatecombo(comboboxname,model,fieldid,fieldname,selected,field,actionurl,condation)
{
	$.ajax({
		type: "POST",
		url: actionurl+"/app/generatecombo/1",
		data: {comboboxname:comboboxname,model:model,fieldid:fieldid,fieldname:fieldname, selected:selected,condation:condation}
		}).done(function( html ) {
			$(field).html(html);
	});
}
function GetPlanCheckboxs(name,model,fieldid,fieldname,selected,field,condition,actionurl, showother)
{
	$.ajax({
		type: "POST",
		url: actionurl+"/app/checkboxlist",
		data: {name:name,model:model,fieldid:fieldid,fieldname:fieldname,condition:condition,selected:selected}
		}).done(function( html ) {
			$(field).html(html);
			if(html=="" && showother!="")
				$(showother).show();
			else
				$(showother).hide();
	});
}
function GetMessageMemberList(id, field, actionurl)
{
	$.ajax({
		type: "POST",
		url: actionurl+"/member/getmessagememberlist",
		data: { refererid: id}
		}).done(function( html ) {
		$(field).html(html);
	});
}
function GetSponsorName(id, field, actionurl)
{
	$.ajax({
		type: "POST",
		url: actionurl+"/member/getsponsorname",
		data: { refererid: id}
		}).done(function( html ) {
		$(field).html(html);
	});
}
function GetMenuSearchLink(value, field, actionurl)
{
	$("#middlebar #header .searchbox .searchicon .searchbutton").css('background-image', 'url("../img/wait-small.gif")')
	$.ajax({
		type: "POST",
		url: actionurl+"/sitesetting/getmenusearchlink",
		data: { menusearch: value}
		}).done(function( html ) {
		$(field).html(html);$(field).show();
		$("#middlebar #header .searchbox .searchicon .searchbutton").removeAttr('style');
	});
}
function GetNotification(field, actionurl)
{
	$.ajax({
		type: "POST",
		url: actionurl+"/sitesetting/getnotification",
		data: { menusearch: ''}
		}).done(function( html ) {
		$(field).html(html);$(field).show();
	});
}
function LoadTopSearchMenu(lnk, actionurl)
{
	$.ajax({
	url: actionurl+"/"+lnk,
	context: document.body
	}).done(function(html) {
		$("#pagecontent").html(html);
	});
}
function GetAdminTemplateEmailMS(id, subject, message, actionurl, showtextarea)
{
	if(id>0)
	{
		$.ajax({
			type: "POST",
			url: actionurl+"/member/getadmintemplateMS",
			data: { templateid: id}
			}).done(function( html ) {
				var MS = html.split('_|_');
				$(subject).val(MS[0]);
				if (showtextarea=='simple') {
					$(message).html(MS[1]);
				}
				else if (showtextarea=='advanced') {
					tinyMCE.activeEditor.setContent(MS[1]);
				}
		});
	}
	else
	{
		$(subject).val('');
		$(message).html('');
	}
}
function GetPreTemplateSuport(id, message, actionurl)
{
	
	$.ajax({
		type: "POST",
		url: actionurl+"/support/GetPreTemplateSuport",
		data: { templateid: id}
		}).done(function( html ) {
			$(message).val(html);
	});
	
}
function ShowMenuSearchArea()
{
	$("#searchmenulinkshtml").show();
	ChangeImage("header-search-1.jpg", "header-search-2.jpg");
}
function HideMenuSearchArea(removedata)
{
	ChangeImage("header-search-2.jpg", "header-search-1.jpg");
	if(removedata=='yes')
	{
		$("#searchmenulinkshtml").html("");
		$(".header-right-login-box-serch-box").attr("value",'');
	}
}
function ChangeImage(findsrc,replacesrc)
{
	var src = $(".header-search-image").attr("src").replace(findsrc,replacesrc);
	$(".header-search-image").attr("src", src);
}
/* Payment Processor */
function show_textbox(showelement,hidebutton) {
	$(hidebutton).hide();
	$("#"+showelement).show();
}

/* Sub Admin Functions Start */
function selectMenuCheckboxes(elementClass,status) {
	$("."+elementClass).each( function(key) {
		$(this).attr("checked",status);
		if(status)
		{
			$(this).attr("disabled",false);
		}
		else
		{
			if(key>0)
				$(this).attr("disabled",true);
		}
	})
}
function toggleMenuCheckboxes(elementClass,status) {
	$("."+elementClass).each( function() {
		$(this).attr("disabled",status);
	})
}
function disableMenuCheckboxes(elementClass,status) {
	if(status==true) status=false; else status=true;
	$("."+elementClass).each( function(key) {
		if(key>0)
			$(this).attr("disabled",status);
	})
}
/* Sub Admin Functions Over */

function closeQuickMenu()
{	
	//$("#quickmenubox #droppable ul").html("");
	//$("#quickmenubox #erasedroppable ul li").html("Trush");
	$("#quickmenubox").hide(500);
}
function RemoveAllQuickMenu()
{	
	$("#quickmenubox #droppable ul").html("");
}
function progressBarMatrix(percent, $element) {
	//var progressBarWidth = percent * $element.width() / 100;
	var progressBarWidth = 100+'%';
	var perdiv='';
	if(percent==100)
	{
		perdiv += '<div class="part4" style="width:'+percent+'%">&nbsp;</div>';
	}
	else
	{
		perdiv += '<div class="part1" style="width:'+percent+'%">&nbsp;</div>';
	}
	
	$element.find('div').animate({ width: progressBarWidth }, 500).html(perdiv);
}
function CheckForUpdate(field, field2, actionurl)
{
	$.ajax({
		type: "POST",
		url: actionurl+"/overview/checkforupdate",
		data: { refererid: 1}
		}).done(function( html ) {
			var htmlsplitdata = html.split("|TK|");
			$(field).html(htmlsplitdata[0]);
			$(field2).html(htmlsplitdata[1]);
	});
}
function CheckForUpdateSetting(field, actionurl)
{
	$.ajax({
		type: "POST",
		url: actionurl+"/overview/checkforupdatesetting",
		data: { cfu_popup: field}
		}).done(function( html ) {
			$("#cboxOverlay, #cfubox").hide();
	});
}
function SupportNotification(field, actionurl)
{
	$.ajax({
		type: "POST",
		url: actionurl+"/support/supportnotification",
		data: { cfu_popup: field}
		}).done(function( html ) {
			if(html=='0')
			{
				$(field).hide();
			}
			else
			{
				$(field).html(html).show();
			}
	});
}
function NotificationCounter(field, actionurl)
{
	$.ajax({
		type: "POST",
		url: actionurl+"/support/notificationcounter",
		data: { cfu_popup: field}
		}).done(function( html ) {
			if(html=='0')
			{
				$(field).hide();
			}
			else
			{
				$(field).html(html).show();
			}
	});
}
function FadeInOut(cls)
{
	if($(cls).css('display')=='block' || $(cls).css('display')==''  || $(cls).css('display')=='table-row'){$(cls).fadeOut(500);}
	else{$(cls).fadeIn(500);}
}
function FadeInOut2(cls,obj)
{
	if($(cls).css('display')=='block' || $(cls).css('display')==''  || $(cls).css('display')=='table-row'){$(cls).fadeOut(500);}
	else{$(cls).fadeIn(500);}
	$(obj).toggleClass("closed");
}
//Header Script Start
$(document).ready(function(){
	$("a[rel='lightbox']").colorbox({width:"80%", height:"80%", iframe:false});
});

$(document).ready( function() {
	$(".admin-o-menu ul li a").click(function() {
		$(".admin-o-menu ul li a").removeClass('active');
		$(this).addClass('active');
		
		$(".admin-o-menu ul li a span").removeClass('active');
		$(this).find("span").addClass('active');
	});
});

/*Javascript Code For Left Menu Code Start*/
$(function() {
      if ($(window).width()<700) {
        $("#leftbar").removeClass("active_toggled");
      }
      if ($("#leftbar").not(".active_toggled"))
	  { 
          $("#leftbar").css({width: "77px"});
          $(".leftbarfix").css({width: "77px"});
          $("#middlebar").css({left: "77px"});
          $(".welcomebox").css({'display':'none'});
          $(".welcomebox div").css({'display':'none'});
		  $("#header").css({left: "77px"});
      }
      if ($("#leftbar").is(".active_toggled"))
	  {
          $("#leftbar").css({width: "260px"});
          $(".leftbarfix").css({width: "260px"});
          $("#middlebar").css({left: "260px"});
          $(".welcomebox").css({'display':'block'});
          $(".welcomebox div").css({'display':''});
		  $("#header").css({left: "260px"});
          $("body").addClass("overflowhidden");
      }
      $('#leftbar .leftmenutogglebutton').click(function(){
	if ($("#leftbar").is(".active_toggled"))
          {
              heightwithoutwelcome=$("#leftbar .leftbarcontent").height();
              $(".welcomebox div").css({'display':'none'});
              $("#leftbar").removeClass("active_toggled").animate({width: "77px"}, 300, function() {$(".welcomebox").css({'display':'none'});});
              $(".leftbarfix").animate({width: "77px"}, 300, function() {});
              
              if ($(window).width()>700) {
                $("#middlebar").animate({left: "77px"}, 300, function() {$("#middlebar").css({'width':$(window).width()-($("#leftbar").width())});$("#allcontent").css({'width':$("#middlebar").width()-($("#rightbar").width()+30)});$("#header").css({'width':$(window).width()-($("#leftbar").width()),'left':'77px'});});
                $("body").removeClass("overflowhidden");
              }
              else
              {
                $("#middlebar").animate({left: "0px"}, 300, function() {$("#middlebar").css({'width':$(window).width()});$("#allcontent").css({'width':$("#middlebar").width()-($("#rightbar").width()+30)});$("#header .quickmenu").css({'right': "0px"});$("#header").css({left: "77px"});});
                $("body").css({'overflow':''});
              }
              $("#leftbar .leftbarcontent").css({'height':$(window).height()-($("#leftbar .logo").height())});
          }
          else
          {
              heightwithwelcome=$("#leftbar .leftbarcontent").height();
              
              $(".welcomebox div").css({'display':''});
              $("#leftbar").addClass("active_toggled").animate({width: "260px"}, 300, function() {
                $(".welcomebox").css({'display':'block'});                
              });
              
              $("#leftbar .leftbarcontent").css({'height':$(window).height()-($("#leftbar .logo").height())-($("#leftbar .welcomebox").height()-165)});
              $(".leftbarfix").animate({width: "260px"}, 300, function() {});
              if ($(window).width()>700) {
                $("#middlebar").animate({left: "260px"}, 300, function() {$("#middlebar").css({'width':$(window).width()-($("#leftbar").width())});$("#allcontent").css({'width':$("#middlebar").width()-($("#rightbar").width()+30)});$("#header").css({'width':$(window).width()-($("#leftbar").width()),'left':'260px'});});
                $("body").addClass("overflowhidden");
		$("#leftbar .leftbarcontent").css({'height':$(window).height()-($("#leftbar .logo").height()+$("#leftbar .welcomebox").height()-37)});
              }
              else
              {
                $("#rightbar").css({'width':'0px'});$("#rightbar").removeClass("opened");
                $("#middlebar").animate({left: "260px"}, 300, function() {$("#middlebar").css({'width':$(window).width()-($("#leftbar").width())});$("#allcontent").css({'width':$("#middlebar").width()-($("#rightbar").width()+30)});$("#header .quickmenu").css({'right': "-134px"});$("#header").css({left: "260px"});});
                $("body").css({'overflow':'hidden'});
		$("#leftbar .leftbarcontent").css({'height':$(window).height()-($("#leftbar .logo").height()+37)});
              }
          }
      });
      
  });

/*Leftmenu Toggle Javascript Code Start*/
$(function() {
  $("#menu ul li a.submenulink").click(function()
  {
	$("#menu ul li a.submenulink").removeClass("activealink");
	if ($(this).next("ul.submenuleft").css('display')!='block')
	{
	  if ($("#leftbar").is(".active_toggled"))
	  {
		$("#menu ul li ul").slideUp();
	  }
	  else
	  {
		 $("#menu ul li ul").slideUp(0);
	  }
	  $(this).toggleClass("activealink");
	}
	if ($(this).next(".submenuleft"))
	{
	  if ($("#leftbar").is(".active_toggled")) {
		$(this).next(".submenuleft").slideToggle(300);
	  }
	  else
	  {
		 $(this).next(".submenuleft").slideToggle(0);
	  }
	}
  });
});

/*For Close Menu on Click Content Part When Menu not open widely*/
$(function() {
	$("#middlebar *").click(function()
	{
		if (!$("#leftbar").hasClass("active_toggled"))
		{
		      $("ul.submenuleft").css({"display":"none"});
		}
	});

});
$(function() {
	$(".notificationoverlay").click(function()
	{
		$("#notificationmenu").hide();
		$(this).removeClass("showoverlay");
	});

});

/*JavaScript Code for Quick Menu*/
$(function() {
	if($("#rightbar").not(".opened"))
	{
	  $("#rightbar").css({'width':'0px'});
	}
	if($("#rightbar").is(".opened"))
	{
	  $("#rightbar").css({'width':'134px'});
	}
	$('#header .quickmenu').click(function(){
	  if($("#rightbar").is(".opened"))
	  {
		$("#rightbar").removeClass("opened").animate({'width':'0px'}, 300, function() {$("#allcontent").css({'width':$("#middlebar").width()-($("#rightbar").width()+30)});});
		$("#rightbar").css({'height':$(window).height()-($("#middlebar #header").height())});
	  }
	  else
	  {
		$("#rightbar").addClass("opened").animate({'width':'134px'}, 300, function() {$("#allcontent").css({'width':$("#middlebar").width()-($("#rightbar").width()+30)});});
		$("#rightbar").css({'height':$(window).height()-($("#middlebar #header").height())});
	  }
	});
});

$(document).ready(function(){
	$('#menu ul li ul li a').mouseup(function() {
		if ($(window).width()<700)
		{
			if($("#leftbar").is(".active_toggled")){
				$('#leftbar .leftmenutogglebutton').click();
			}
		}
	});
});
//Header Script Over

//Footer Script Start
function Footer()
{
	$(document).ready (function () {
	  $("#leftbar .leftbarcontent").css({'height':$(window).height()-($("#leftbar .logo").height())-($("#leftbar .welcomebox").height()+34)});
	  if ($(window).width()<700) {
		$("#middlebar").css({'width':$(window).width(),'left':'0px'});
	  }
	  else
	  {
		$("#middlebar").css({'width':$(window).width()-($("#leftbar").width())});
		$("#header").css({'width':$(window).width()-($("#leftbar").width())});
	  }
	  $("#allcontent").css({'width':$("#middlebar").width()-($("#rightbar").width()+30)});
	  $("#rightbar").css({'height':$(window).height()-($("#middlebar #header").height())});
	});
	
	$(document).ready(function() {
		if($(window).width()<769)
			$("#leftbar #menu ul:first").prepend($(".topmenu ul").html());
	});
	
	$('#menu ul.submenuleft li a').click(function() {
		$("html, body").animate({ scrollTop: 0 }, 'slow');
	});
	
	$(document).ready (function () {
		$("#content").css({'min-height':$(window).height()-($("#header").height()+$("#footer").height()+15)});
	});

	$(document).ready(function() {
	   $(".leftbarcontent").mCustomScrollbar({
		 scrollInertia:200,
		 theme:"dark",
		 setHeight:"100%",
		 scrollButtons:{enable:true},
		 advanced:{
		   updateOnContentResize: true
		 }
	   });
	 });
	 $(document).ready(function() {
	   $("#rightbar").mCustomScrollbar({
		 scrollInertia:200,
		 theme:"dark",
		 setHeight:"100%",
		 setLeft:"0px",
		 advanced:{
		   updateOnContentResize: true
		 }
	   });
	 });
	 $(document).ready(function() {
	   $("#searchmenulinkshtml").mCustomScrollbar({
		 scrollInertia:200,
		 theme:"dark",
		 setHeight:"100%",
		 setLeft:"0px",
		 advanced:{
		   updateOnContentResize: true
		 }
	   });
	 });
}
//Footer Script Over

//Outside Script Start
this.vtip = function() {    
    this.xOffset = -10;
    this.yOffset = 10;
    $(".vtip").unbind().hover(    
        function(e) {
            this.t = this.title;
            this.title = ''; 
            this.top = (e.pageY + yOffset); this.left = (e.pageX + xOffset);
            $('body').append( '<p id="vtip"><img id="vtipArrow" />' + this.t + '</p>' );
            $('p#vtip').css("top", this.top+"px").css("left", this.left+"px").fadeIn("slow");
        },
        function() {
            this.title = this.t;
            $("p#vtip").fadeOut("slow").remove();
        }
    ).mousemove(
        function(e) {
            this.top = (e.pageY + yOffset);
            this.left = (e.pageX + xOffset);
            $("p#vtip").css("top", this.top+"px").css("left", this.left+"px");
        }
    ).click(
        function(e) {
            $("p#vtip").fadeOut("slow").remove();
        }
    );
};
//Outside Script Over