<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 10-12-2014
  *********************************************************************/?>
<?php // To protect site from XSS and vulnerable uploads
header('X-XSS-Protection: 1; mode=block'); 
header('X-Content-Type-Options: nosniff');?>
<?php // This file is the main template file of the theme. Theme layout design related changes can be done from this file. ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo @$title_for_layout; ?> - <?php echo $SITECONFIG['sitetitle'];?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<script type="text/javascript">var themepath='<?php echo $SITEURL;?>theme/<?php echo $this->theme;?>/';</script>
	<?php echo $this->Html->css('common');?>
	<?php echo $this->Html->script('jquery-1.8.3.min');?>
	<?php echo $this->Html->script('jquery-common');?>
	
	<!--calender start-->
	<?php echo $this->Html->script('jquery-ui.min');?>
	<?php echo $this->Html->script('jquery-ui-timepicker-addon');?>
	<?php echo $this->Html->css('jquery-ui.min');?>
	<!--calender over-->
	
	<?php echo $this->Html->script('bootstrap.min');?>
	<?php echo $this->Html->css('bootstrap.min');?>
	
	<?php echo $this->Html->css('style');?>
	
	<!--tiny_mce start-->
	<?php echo $this->Html->script('tiny_mce/tiny_mce.js');?>
	<?php echo $this->Html->script('tinymce');?>
	<!--tiny_mce over-->
	
	<!--colorpicker start-->
	<?php echo $this->Html->script('wheelcolorpicker');?>
	<?php echo $this->Html->css('wheelcolorpicker/wheelcolorpicker');?>
	<!--colorpicker over-->
    
	<!--ajax file upload start-->
	<?php echo $this->Html->script('jquery.form');?>
	<!--ajax file upload over-->
	
	<!--colorbox start-->
	<?php 
		echo $this->Html->css('colorbox');
		echo $this->Html->script('colorbox/jquery.colorbox');
	?>
	<!--colorbox over-->
	
	<!--Scrollbar start-->
	<?php 
		echo $this->Html->css('scrollbar');
		echo $this->Html->script('jquery.mousewheel.min');
		echo $this->Html->script('scrollbar');
	?>
	<!--Scrollbar over-->
	
	<!--Alert Box start-->
	<?php 
		echo $this->Html->css('jquery.alerts');
		echo $this->Html->script('jquery.alerts');
	?>
	<!--Alert Box over-->
	
	<!--Nenobar Start-->
	<?php  echo $this->Html->script('nanobar');?>
	<!--Nenobar Over-->
<script type="text/javascript">
$( document ).ready(function() {
$("#leftbar .btn-group button").click(function() {
   var currenttime='<?php print $PHPTime;?>';
   var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
   var serverdate=new Date(currenttime);
   displaytime();
});
});
</script>
<script type="text/javascript">
var currenttime = '<?php print $PHPTime;?>'
var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December")
var serverdate=new Date(currenttime)
function padlength(what){
var output=(what.toString().length==1)? "0"+what : what
return output
}
function displaytime(){
serverdate.setSeconds(serverdate.getSeconds()+1)
var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear()
var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds())
document.getElementById("servertime").innerHTML=datestring+" "+timestring
}
window.onload=function(){
setInterval("displaytime()", 1000)
}
</script>
</head>
<body>
<script type="text/javascript">var options = {bg: '#ff0000',id: 'mynano'};var nanobar = new Nanobar( options );</script>
<?php if($NoTiFiCaTiOn!=''){?><span class="NoTiFiCaTiOn"><?php $NoTiFiCaTiOn=explode(",",$NoTiFiCaTiOn); for($Notifymodule=0;$Notifymodule<count($NoTiFiCaTiOn);$Notifymodule++){$expiremodule=explode(':',$NoTiFiCaTiOn[$Notifymodule]);if($Notifymodule==0){echo $expiremodule[0].":".$expiremodule[1].",";}elseif(($Notifymodule+1)==count($NoTiFiCaTiOn)){echo $expiremodule[0].".";}else{echo $expiremodule[0].",";}}?>
<?php echo $this->Html->image('cancel.png', array('alt'=>'', 'class'=>'NTFCTOX', 'onclick'=>"eraseCookie('CakeCookie[_modt]');$('.NoTiFiCaTiOn').slideUp(500);"));?></span><?php }?>
<div id="pleasewait"><?php echo $this->Html->image('wait.gif', array('alt' => 'Please Wait!!'));?></div>

<div id="leftbar" class="active_toggled">
  <div class="leftbarfix">
      <span class="leftmenutogglebutton">
        <span class="glyphicon glyphicon-tasks"></span>
      </span>
      
      <!---Logo Here--->
      <div class="logo textcenter">
        <div class="logo-big"><a href="<?php echo $ADMINURL; ?>/overview"><?php echo $this->Html->image('logo-large.png', array('alt' => ''));?></a></div>
        <div class="logo-small"><a href="javascript:void(0)" onclick="$('#leftbar .leftmenutogglebutton').click();"><?php echo $this->Html->image('logo-small.png', array('alt' => ''));?></a></div>
      </div>
      <!---Logo Here--->
      
      <!---Admin Welcome Box Code Start--->
	  
      <div class="welcomebox">
        <div class="welcomephotomain">
          <?php echo $this->Html->image('member/'.$this->Session->read('adminimg'), array('alt' => ''));?>
        </div>
        <div class="welcometext">
          <span class="welcometextinner">Welcome, <span><?php echo $this->Session->read('admindisplayname'); ?></span></span>
		  <?php echo $this->html->link('<span class="glyphicon glyphicon-picture"></span>Overview', array('controller' => 'overview', 'action' => 'index', 'plugin' => false), array('escape' => false));?>
          <?php echo $this->Js->link('<span class="glyphicon glyphicon-thumbs-up"></span><span>Support</span> <span id="supportnotification"></span>', array('controller'=>'support', "action"=>"index/public/0/top", 'plugin' => false), array(
			'update'=>'#pagecontent',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'class'=>'nosign',
			'escape'=>false
		  ));?>
		  
			<span class="glyphicon glyphicon-info-sign"></span>
			<div class="btn-group">
			  <button type="button" class="btn btn-information dropdown-toggle" data-toggle="dropdown">
				Information <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu" role="menu">
				<li><a class="bold13black">Script Info</a></li>
				<li><a><span class="normal12black">License Key :</span><br><span class="normal12gray"><?php echo SETTINGLICENSEKEY; ?></span></a></li>
				<li><a><span class="normal12black">Registered Domain :</span><br><span class="normal12gray"><?php echo $SITEURL; ?></span></a></li>
				<li><a><span class="normal12black">Script Version :</span><br><span class="normal12black">Current :</span> <span class="normal12gray"><?php echo CURRENTVERSION; ?></span> <span class="normal12black">Latest :</span> <span class="normal12gray"><?php echo LATESTVERSION; ?></span></a></li>
				
				<li class="divider"></li>
				
				<li><a class="bold13black">Login Info</a></li>
				<li><a><span class="normal12black">Last Login Time :</span><br><span class="normal12gray"><?php echo $this->Time->format($SITECONFIG["timeformate"],$this->Session->read('admin_logdate')); ?></span></a></li>
				<li><a><span class="normal12black">Last Login IP :</span><br><span class="normal12gray"><?php echo $this->Session->read('register_ip'); ?></span></a></li>
				<li><a><span class="normal12black">Server Time :</span><br><span class="normal12gray"><span id="servertime"></span></span></a></li>
				<li><a><span class="normal12black">Server TimeZone :</span><br><span class="normal12gray"><?php echo (ini_get('date.timezone')) ? ini_get('date.timezone') : '-'; ?></span></a></li>
                                <li><a><span class="normal12black">Script TimeZone :</span><br><span class="normal12gray"><?php echo (date_default_timezone_get()) ? date_default_timezone_get() : '-'; ?></span></a></li>
				<li style="padding: 0 10px 4px 10px;"><?php echo $this->html->link('Check for Updates', array('controller' => 'help', 'action' => 'index', 'plugin' => false), array('escape' => false, 'class' => 'btnorange'));?></li>
				
			  </ul>
			</div>
		  <?php echo $this->html->link('<span class="glyphicon glyphicon-off"></span>Logout', array('controller' => 'login', 'action' => 'logout', 'plugin' => false), array('escape' => false, 'class'=>"logout"));?>
        </div>
        
      </div>
      <!---Admin Welcome Box Code Over--->
      
      <div class="leftbarcontent">
		  <div id="menu">
            <ul>
              <li>
				<a class="submenulink"><span class="glyphicon glyphicon-wrench"></span><span>Settings</span></a>
                <ul class="submenuleft">
					<a href="#" class="onclosedmenu">Settings</a>
					<li><span>
						<?php echo $this->Js->link("Website Settings", array('controller'=>'sitesetting', "action"=>"index/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Admin Settings", array('controller'=>'sitesetting', "action"=>"account/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Finance Settings", array('controller'=>'sitesetting', "action"=>"paymentprocessor/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Advertisement Settings", array('controller'=>'sitesetting', "action"=>"advertisementcredit/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Membership Settings", array('controller'=>'sitesetting', "action"=>"membership/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('show', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('hide', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Balance Transfer Settings", array('controller'=>'sitesetting', "action"=>"balancetransfer/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Email Settings", array('controller'=>'sitesetting', "action"=>"smtp/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Security Settings", array('controller'=>'sitesetting', "action"=>"antibrute/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Signup Settings", array('controller'=>'sitesetting', "action"=>"registrationform/0/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Referral Settings", array('controller'=>'sitesetting', "action"=>"referral/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Statistics Settings", array('controller'=>'sitesetting', "action"=>"publicstatistics/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
				</ul>
              </li>
              <li><a class="submenulink"><span class="glyphicon glyphicon-user"></span><span>Member Tools</span></a>
                  <ul class="submenuleft">
					<a href="#" class="onclosedmenu">Member Tools</a>
					<li><span>
						<?php echo $this->Js->link("Member List", array('controller'=>'member', "action"=>"index/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Leader Board", array('controller'=>'member', "action"=>"leaderboard/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Mass Mailing", array('controller'=>'member', "action"=>"massmail/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Mass Mailing History", array('controller'=>'member', "action"=>"massmaillisthistory/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Admin Message", array('controller'=>'member', "action"=>"message/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("URL Rotator", array('controller'=>'urlrotator', "action"=>"member/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("URL Shortener", array('controller'=>'urlshortener', "action"=>"url/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Member Group", array('controller'=>'membergroup', "action"=>"index/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Contest", array('controller'=>'Contest', "action"=>"index/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Jackpot", array('controller'=>'jackpot', "action"=>"index/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
				</ul>
              </li>
              <li><a class="submenulink"><span class="glyphicon glyphicon-usd"></span><span>Finance</span></a>
                  <ul class="submenuleft">
					<a href="#" class="onclosedmenu">Finance</a>
					<li><span>
						<?php echo $this->Js->link("Payment History", array('controller'=>'finance', "action"=>"index/0/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Pending Deposits", array('controller'=>'finance', "action"=>"pendingdeposit/0/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Pending Positions", array('controller'=>'finance', "action"=>"pendingposition/0/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Pending Purchases", array('controller'=>'finance', "action"=>"pendingplan/0/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Pending Free Plans", array('controller'=>'finance', "action"=>"pendingfreeplan/0/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Withdrawal Requests", array('controller'=>'finance', "action"=>"withdrawrequest/0/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Withdrawal History", array('controller'=>'finance', "action"=>"withdrawhistory/0/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Commission", array('controller'=>'finance', "action"=>"commission/0/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Cash By Admin", array('controller'=>'finance', "action"=>"cash_by_admin/0/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Balance Transfer", array('controller'=>'balancetransfer', "action"=>"balancetransferpanding/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Coupons", array('controller'=>'finance', "action"=>"coupons/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
				</ul>
              </li>
              <li><a class="submenulink"><span class="glyphicon glyphicon-picture"></span><span>Design & CMS</span></a>
                  <ul class="submenuleft">
					<a href="#" class="onclosedmenu">Design & CMS</a>
					<li><span>
						<?php echo $this->Js->link("Themes", array('controller'=>'design', "action"=>"index/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Template Edit", array('controller'=>'design', "action"=>"template/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Email Templates", array('controller'=>'design', "action"=>"siteemailtemplate/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Language Edit Phase", array('controller'=>'design', "action"=>"languageeditphase/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Website Pages", array('controller'=>'contentmanagement', "action"=>"index/0/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Virtual Pages", array('controller'=>'contentmanagement', "action"=>"sitepages/public/0/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("News", array('controller'=>'contentmanagement', "action"=>"news/public/0/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("FAQs", array('controller'=>'contentmanagement', "action"=>"faqs/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Testimonials", array('controller'=>'contentmanagement', "action"=>"testimonials/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
				</ul>
			  </li>
              <li><a class="submenulink"><span class="glyphicon glyphicon-indent-left"></span><span>Promo Tools</span></a>
				  <ul class="submenuleft">
					<a href="#" class="onclosedmenu">Promo Tools</a>
					<li><span>
						<?php echo $this->Js->link("Banners", array('controller'=>'promotools', "action"=>"index/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Dynamic Banners", array('controller'=>'statbanner', "action"=>"banner/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Splash Pages", array('controller'=>'promotools', "action"=>"splashpages/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Landing Pages", array('controller'=>'promotools', "action"=>"landingpages/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Text Links", array('controller'=>'promotools', "action"=>"textlinks/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Promotional Emails", array('controller'=>'promotools', "action"=>"emailads/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
				</ul>
			  </li>
              <li><a class="submenulink"><span class="glyphicon glyphicon glyphicon-film"></span><span>Advertisement</span></a>
				  <ul class="submenuleft">
					<a href="#" class="onclosedmenu">Advertisement</a>
					<li><span>
						<?php echo $this->Js->link("Banner Ads", array('controller'=>'bannerad', "action"=>"index/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Text Ads", array('controller'=>'textad', "action"=>"index/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Solo Ads", array('controller'=>'soload', "action"=>"index/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Pay Per Click - PPC", array('controller'=>'ppc', "action"=>"member/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Paid To Click - PTC", array('controller'=>'ptc', "action"=>"member/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Login Ads", array('controller'=>'loginad', "action"=>"loginad/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Biz Directory", array('controller'=>'directory', "action"=>"category/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Traffic Exchange", array('controller'=>'traffic', "action"=>"startpage/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("PPC Text Ads", array('controller'=>'ppctextad', "action"=>"member/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Message Ads", array('controller'=>'advertisement', "action"=>"messageads/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
				</ul>
			  </li>
              <li><a class="submenulink"><span class="glyphicon glyphicon glyphicon-file"></span><span>Logs</span></a>
				  <ul class="submenuleft">
					<a href="#" class="onclosedmenu">Logs</a>
					<li><span>
						<?php echo $this->Js->link("Admin Activity", array('controller'=>'logs', "action"=>"index/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Member Activity", array('controller'=>'logs', "action"=>"memberactivity/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Admin Logs", array('controller'=>'logs', "action"=>"admin/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Member Logs", array('controller'=>'logs', "action"=>"member/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Cronjob Logs", array('controller'=>'logs', "action"=>"cronjob/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("IP Logs", array('controller'=>'logs', "action"=>"iplog/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Processor Trace Logs", array('controller'=>'logs', "action"=>"posttrace/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
					<li><span>
						<?php echo $this->Js->link("Sent Emails", array('controller'=>'logs', "action"=>"emailtrace/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
				</ul>
			  </li>
              <li><a class="submenulink"><span class="glyphicon glyphicon-cog"></span><span>Manage Module</span></a>
				  <ul class="submenuleft">
					<a href="#" class="onclosedmenu">Manage Module</a>
					<li><span>
						<?php echo $this->Js->link("Modules", array('controller'=>'managemodule', "action"=>"index/top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					</span></li>
				  </ul>
			  </li>
            </ul>
          </div>
      </div>
      
  </div>
</div>

<div id="middlebar">
    <div id="header">
      
      <div class="searchbox">
		
		<?php echo $this->Form->input('menusearch', array('type'=>'text', 'label' => false, 'div' => false, 'class'=>'searchtextbox', 'autocomplete'=>'off', 'onfocus'=>'if ($(window).width()<700){$(this).css("width","150px");}else{$(this).css("width","250px");}ShowMenuSearchArea();', 'onblur'=>'if($(this).val()==""){$(this).css("width","70px");$("#searchmenulinkshtml .innerblock").html("<div class=title>No Record Found</div>");}HideMenuSearchArea("no");', 'onclick'=>'ShowMenuSearchArea();', 'onkeypress'=>'if(event.keyCode==13){$("#header .searchicon input").click();};'));?>
		
		<div id="searchmenulinkshtml"><div class="innerblock"></div></div>
		<script type="text/javascript">
		$("#searchmenulinkshtml").hover(
			function () {},
			function () { $(this).hide(); }
		);
		</script>
		<span class="searchicon">
			<input type="button" class="searchbutton" value="" onclick='GetMenuSearchLink($("#menusearch").val(),"#searchmenulinkshtml .innerblock", "<?php echo $ADMINURL;?>");' />
		</span>
		<div class="clearboth"></div>
      </div>
      
      <div class="topmenu">
        <ul class="notmobile topmenuinner">
			<?php foreach($ActiveModules as $mkey=>$mval){ ?>
				<li>
					<?php echo $this->Js->link('<span class="glyphicon glyphicon-usd"></span><span>'.$mval.'</span>', array('controller'=>$mkey, "action"=>"index/top", 'plugin' => false), array(
						'update'=>'#pagecontent',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'class'=>"nosign",
						'escape'=>false
					));?>
				</li> 
			<?php } ?>
        </ul>
		
		<span class="quick1 notmobile">
		<?php echo $this->Js->link($this->Html->image('quick-menu-edit.jpg', array('alt' => '')), array('controller'=>'overview', "action"=>"quickmenu", 'plugin' => false), array(
			'update'=>'#quickmenuhtml',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'class'=>"quickmenueditbutton",
			'escape'=>false
		));?>
		</span> 
		<span class="quick2 notmobile"><?php echo $this->Html->image('quick-menu.jpg', array('alt' => '', 'class'=>'quickmenu'));?></span>
		
		<span class="notificationmenu notmobile"><span id="notificationcounter"></span><?php echo $this->Html->image('notifications.jpg', array('alt' => '', 'class'=>'', 'onclick'=>''));?></span>
		<div id="notificationmenu">
			<div class="niticaret"><span>&nbsp;</span></div>
			<div class="innerblock"></div>
		</div>
		<div class="notificationoverlay"></div>
		
		<script type="text/javascript">
		$(".notificationmenu img").click(function () {
			if($("#notificationmenu").css('display')=='none')
			{
				$("#notificationmenu").show();
				GetNotification("#notificationmenu .innerblock", "<?php echo $ADMINURL;?>");
				$(".notificationoverlay").addClass("showoverlay");
			}
			else
			{
				$("#notificationmenu").hide();
			}
		});
		</script>
		
		<div id="quickmenuhtml"></div>
      </div>
      <div class="clearboth"></div>
      
    </div>
    
    <div id="allcontent">
		<!-- content-start -->
		<div id="content">
			<div id="pagecontent"><?php echo $this->fetch('content'); ?></div>
		<!-- content-over -->
		</div>
		
		<div id="footer">Copyright &copy; <?php echo date("Y"); ?> <a href="https://www.proxscripts.com" target="_blank">ProxScripts.com</a> All Rights Reserved | Powered by <a href="https://www.proxscripts.com/" target="_blank">ProxScripts.com</a> </div> <!-- | Template By: <a href="http://cheapminisite.com" target="_blank">CheapMinisite.com</a> -->
	</div>

	<div id="rightbar">
	  <div class="quickmenuinside">
		<div class="quickmenutitle">Quick Menu</div>
		<ul>
			<?php
			$quickmenuarray=explode("[#]", trim($SITECONFIG["quickmenu"]));
			if(count($quickmenuarray)==0)
			{
				echo '<li>You can add quick menus by clicking on Quick Menu Edit button below.</li>';
			}
			else
			{
				foreach($quickmenuarray as $quickmenudata)
				{
					$quickmenudataarray=explode("[@]", $quickmenudata);
					$qca=@explode("adminpanel/", $quickmenudataarray[1]);
					$qc=@explode("/", $qca[1]);
					$qa=@implode("", explode($qc[0]."/", $qca[1], 2));
					$quickmenu=array('name'=>$quickmenudataarray[0], 'controller'=>$qc[0], 'action'=>$qa);
					
					echo '<li>';
					echo $this->Js->link('<span class="'.strtolower(str_replace(" ", "-", $quickmenu['name'])).'"></span>'.$quickmenu['name'], array('controller'=>$quickmenu['controller'], "action"=>$quickmenu['action']), array(
						'update'=>'#pagecontent',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false
					));
					echo '</li>';
				}
			}
			?>
		</ul>
	  </div>
	</div>
<!--Notification Code start-->
<script type="text/javascript">
	$(document).ready( function(){SupportNotification("#supportnotification", "<?php echo $ADMINURL;?>");<?php if(strpos($SITECONFIG["notification"],'1') !== false) {?>NotificationCounter("#notificationcounter", "<?php echo $ADMINURL;?>");<?php } ?>});
	setInterval(function(){SupportNotification("#supportnotification", "<?php echo $ADMINURL;?>");}, 300000);
	<?php if(strpos($SITECONFIG["notification"],'1') !== false) {?>
	setInterval(function(){NotificationCounter("#notificationcounter", "<?php echo $ADMINURL;?>");}, 500000);
	<?php } ?>
</script>
<!--Notification Code Over-->
<script type="text/javascript">$(document).ready(function(){Footer();});</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>
</body>
</html>