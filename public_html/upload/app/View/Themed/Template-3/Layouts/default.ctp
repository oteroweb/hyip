<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 12-12-2014
  *********************************************************************/?>
<?php // To protect site from XSS and vulnerable uploads
header('X-XSS-Protection: 1; mode=block'); 
header('X-Content-Type-Options: nosniff');?>
<?php // This file is the main template file of the theme. Theme layout design related changes can be done from this file. ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo __(@$title_for_layout); ?> - <?php echo __($SITECONFIG['sitetitle']);?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="<?php echo __(@$keywords_for_layout); ?>" />
	<meta name="description" content="<?php echo __(@$description_for_layout); ?>" />
	
	<?php // Code to embed fonts ?>
	<style type="text/css" media="all">
		@font-face{
			font-family:'Open Sans';
			font-style:normal;
			font-weight:400;
			src:local('Open Sans'), local('OpenSans'), url("<?php echo $SITEURL.'theme/'.$this->theme.'/fonts/OpenSans.woff';?>") format('woff');
		}
		@font-face{
			font-family:'Open Sans';
			font-style:normal;
			font-weight:700;
			src:local('Open Sans Bold'), local('OpenSans-Bold'), url("<?php echo $SITEURL.'theme/'.$this->theme.'/fonts/OpenSans-Bold.woff';?>") format('woff');
		}
		@font-face{
			font-family:'Open Sans';
			font-style:normal;
			font-weight:600;
			src:local('Open Sans Semi Bold'), local('OpenSans-SemiBold'), url("<?php echo $SITEURL.'theme/'.$this->theme.'/fonts/OpenSans-SemiBold.woff';?>") format('woff');
		}
	</style>
	
	<?php // Including the css and js files starts here ?>
	<?php
	echo $this->Html->meta('icon');
	echo $this->Html->css('common');
	echo $this->Html->css('style');
	echo $this->fetch('meta');
	?>
	<script type="text/javascript">var themepath='<?php echo $SITEURL;?>theme/<?php echo $this->theme;?>/';</script>
	<?php echo $this->Html->script('jquery-1.8.3.min');?>
	<?php echo $this->Html->script('common');?>
	
	<?php echo $this->Html->script('jquery-ui.min'); // for calendar ?>
	<?php echo $this->Html->css('jquery-ui.min'); // for calendar ?>
	<?php if($isLogin)echo $this->Html->script('jquery.form');?>
	<?php echo $this->Html->css('keyboard');
	echo $this->Html->script('keyboard.min'); // for virtual keyboard ?>
	
	<?php // Including the css and js files ends here ?>
	
	<?php // For Manu Scrollbar
		echo $this->Html->css('scrollbar');
		echo $this->Javascript->link('jquery.mousewheel.min');
		echo $this->Javascript->link('scrollbar');
	?>
	
	<?php // Do not edit below code. Message ads code starts here ?>
	<script type="text/javascript">
	$(document).ready(function(){
		var popwid="60%";
		if($(window).width()<700)
			var popwid="100%";
			
		<?php if(@$_COOKIE['popup1']<date("Ydm") && $MessageAdsPublic!=''){?>
			$("#colorboxpopup1").colorbox({width:popwid, href:"#colorboxpopup1", inline:true, open:true, onClosed:function(){setCookie('popup1',<?php echo date("Ydm");?>,1);}, overlayClose:false, escKey:false, closeButton:false, fixed:true});
			$("#cboxContent").addClass("MeaasageAdcontentclass").find("#cboxClose").css( "visibility", "hidden" );
			MessageAdsTimer(5, '#MeaasageAdtimer');
		<?php }?>
		<?php if(@$_COOKIE['popup2']<date("Ydm") && $MessageAdsActive!=''){?>
			$("#colorboxpopup2").colorbox({width:popwid, href:"#colorboxpopup2", inline:true, open:true, onClosed:function(){setCookie('popup2',<?php echo date("Ydm");?>,1);}, overlayClose:false, escKey:false, closeButton:false, fixed:true});
			$("#cboxContent").addClass("MeaasageAdcontentclass").find("#cboxClose").css( "visibility", "hidden" );
			MessageAdsTimer(5, '#MeaasageAdtimer');
		<?php }?>
		<?php if(@$_COOKIE['popup3']<date("Ydm") && $MessageAdsPaid!=''){?>
			$("#colorboxpopup3").colorbox({width:popwid, href:"#colorboxpopup3", inline:true, open:true, onClosed:function(){setCookie('popup3',<?php echo date("Ydm");?>,1);}, overlayClose:false, escKey:false, closeButton:false, fixed:true});
			$("#cboxContent").addClass("MeaasageAdcontentclass").find("#cboxClose").css( "visibility", "hidden" );
			MessageAdsTimer(5, '#MeaasageAdtimer');
		<?php }?>
		<?php if(@$_COOKIE['popup4']<date("Ydm") && $MessageAdsUnpaid!=''){?>
			$("#colorboxpopup4").colorbox({width:popwid, href:"#colorboxpopup4", inline:true, open:true, onClosed:function(){setCookie('popup4',<?php echo date("Ydm");?>,1);}, overlayClose:false, escKey:false, closeButton:false, fixed:true});
			$("#cboxContent").addClass("MeaasageAdcontentclass").find("#cboxClose").css( "visibility", "hidden" );
			MessageAdsTimer(5, '#MeaasageAdtimer');
		<?php }?>
		<?php if(@$_COOKIE['popup5']<date("Ydm") && $MessageAdsMembership!=''){?>
			$("#colorboxpopup5").colorbox({width:popwid, href:"#colorboxpopup5", inline:true, open:true, onClosed:function(){setCookie('popup5',<?php echo date("Ydm");?>,1);}, overlayClose:false, escKey:false, closeButton:false, fixed:true});
			$("#cboxContent").addClass("MeaasageAdcontentclass").find("#cboxClose").css( "visibility", "hidden" );
			MessageAdsTimer(5, '#MeaasageAdtimer');
		<?php }?>
	});
	</script>
	<?php // Do not edit above code. Message ads code ends here ?>
	
	<?php
	if(!$isLogin)
	{?>
		<script type="text/javascript">
			/*Javascript Code for Fix Menu on Scroll*/
			$(document).ready(function () {
			  if($(window).width()>767)
			  {
			var menuclass="#menubg";
			var menuscrollheight=486;
			
			if ($(this).scrollTop() > menuscrollheight) {
				$(menuclass).addClass("f-nav");
			}
			$(window).scroll(function () {
				if ($(this).scrollTop() > menuscrollheight) {
					$(menuclass).addClass("f-nav");
				} else {
					$(menuclass).removeClass("f-nav");
				}
			});
			  }
			});
			/*Javascript Code for Fix Menu on Scroll*/
		</script>
	<?php
	}
	else
	{?>
		<script type="text/javascript">
			$(function() {
			  $("#members-area-left-side .left-menu-fix").css({"height":$(window).height()-$(".memberlogo").height()})
			});
      
			/*Leftmenu Javascript Start*/
			$(function() {
				  $('.toggle_button, #open_left_menu ul li a .glyphicon, #open_left_menu ul li a span.menu_tooltip').click(function(){
					  if ( $("#open_left_menu").is(".active_toggled")) {
						  $("#open_left_menu").removeClass("active_toggled").animate({width: "80px"}, 300, function() {});
						   $("#open_left_menu .left-menu-fix").animate({width: "80px"}, 300, function() {});
						  $("#open_left_menu ul:first").removeClass("active-menu");
					  }
					  else {
						  $("#open_left_menu").addClass("active_toggled").animate({width: "266px"}, 300, function() {});
						  $("#open_left_menu .left-menu-fix").animate({width: "266px"}, 300, function() {});
						  $("#open_left_menu ul:first").addClass("active-menu");
					  }
					  
				  });
			 
			  $('#open_left_menu ul li a').click(function()
			  {
				  if ( $("#open_left_menu").is(".active_toggled")) {
				  $(this).next("#open_left_menu ul li .leftbar_submenu").slideToggle("medium");
				  }
			  });
			  
			  if ( $("#open_left_menu").is(".active_toggled")) {
				$("#open_left_menu").css({width: "266px"});
				$("#open_left_menu .left-menu-fix").css({width: "266px"});
			  }
		   
			});
			/*Leftmenu Javascript Over*/
		</script>
  
		
	<?php
	}
	?>
	
	
	<?php echo $planbannerdata; echo $plantextdata; // Code to display plan banner ads and plan text ads on their respective positions ?>
</head>
<body>
	<div id="pleasewait"><?php echo $this->Html->image('wait.gif', array('alt' => 'Please Wait'))?></div><?php // Code for loading gif image ?>

<?php // Do not edit below code. Code for plugin starts here
if($isLogin){echo $LoginadPopup;}
// Do not edit above code. Code for plugin ends here ?>
<?php if(!$isLogin){ // If member is not logged in(Public side) ?>
	<div id="logobgmain">
        <div class="topmenu">
            <div class="logo"><a href="<?php echo $SITEURL;?>" ><?php echo $this->Html->image('logo.png', array('alt' => $SITECONFIG['sitetitle'], 'width'=> 266, 'height'=> 56))?></a></div>
			<div class="navbar-header navbar-default">
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			</div>
		
            <div class="logoright">
				
				<div class="inlineblock">
					<?php // Code for sponsor detail starts here 
					if((is_int($REFFERAL) || $REFFERAL > 0 || is_string($REFFERAL)) && $REFFERAL != ""){ ?>
					<span id="sponsordetail"><?php echo $this->Html->image('wait.gif', array('alt' => 'Loding..','style' => 'width:20px;height:20px'))?></span>
                    <script type="text/javascript">showsponsordetail('<?php echo $REFFERAL; ?>', '#sponsordetail', '<?php echo $SITEURL;?>');</script>
					<?php }?>
					<?php // Code for sponsor detail ends here ?>
				</div>
				
				<div class="inlineblock">
					<div class="languagetop">
						<div class="select-main graybg">
							<label>
								<?php // Code for language change box starts here ?>
								<?php if($LanguageBox != ''){ echo $LanguageBox; } ?>
								<?php // Code for language change box ends here ?>
							</label>
						</div>
					</div>
					<div class="joinnow"><?php echo $this->html->link(__("Join Now"), array('controller' => 'register', 'action' => 'index', 'plugin' => false));?></div>
					<div class="login"><?php echo $this->html->link(__("Login"), '#', array('class' => '', 'data-toggle' => 'modal', 'data-target' => '.bs-example-modal-sm'));?></div>
				</div>
            </div>
			<div class="clearboth"></div>
        </div>
    </div>
    <div id="headerbg">
        <div id="header">
            <div class="headertext"><span class="headertitle">Add Your</span><br/> Tagline here</div>
				
			<?php // Code to show login box starts here. Not to be shown on login page. ?>
			<?php if(!$isLogin && $this->params['controller']!='login'){ ?>
			<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			  <div class="modal-dialog modal-sm">
				<div class="modal-content">
				  <div class="loginbox">
						<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button><div class="clearboth"></div></div>
						<div class="login_title"><?php echo __('Member Login'); ?></div>
						<div class="login_style">
							
							<?php if($this->Session->read('locked')=='yes'){ // If Anti Brute Force lock is placed on member due to incorrect answers ?>
								<div class="block_message" style="width:167px;margin: 0 auto;text-align:center;font-size:15px;"><?php echo __('Access to the site has been blocked for you. Please try after following hours').' : '.$SITECONFIG["antibrutehours"]; ?></div>
							
							<?php }elseif($this->Session->read('plocked')=='yes'){ // If Anti Brute Force permanent lock is placed on member due to incorrect answers within specified time ?>
								<div class="block_message" style="width:167px;margin: 0 auto;text-align:center;font-size:15px;"><?php echo __('You are blocked. Please confirm your account first.'); ?></div>
							<?php }else{?>
								<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off', 'url'=>array('controller'=>'login','action'=>'index')));?>
								<div id="LoginMessage" class="error"></div>
								<?php if($this->Session->check('verified_ip')){ // If member has just verified new IP address ?>
									<?php echo __('Verified').' : '.$this->Session->read('verified_ip'); ?>
								<?php } ?>
								
								<span class="username_icon"></span>
								<?php echo $this->Form->input('user_name', array('type'=>'text', 'label' => false, 'div' => false, 'placeholder'=>__('Username')));?>
								<span class="password_icon"></span>
								<div class="passwordkeybord"><?php echo $this->Form->input('password', array('type'=>'password', 'label' => false, 'div' => false, 'class'=>'keyboardInput', 'placeholder'=>__('**********')));?></div>
								
								<?php if($MemberLoginCaptcha){ // If captcha is enabled by admin ?>
								<div class="captha_main">
								  <span class="captcha_image"><?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'MenuLoginCaptcha', "width"=>"118", "height"=>"44")); ?></span>
								  <span><?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"captacha-box-in-login", 'label'=>'', 'div'=>false,  'placeholder'=>__('Enter Captcha')));?></span>
								  <span><a class="reload_captcha" href="javascript:void(0);" onclick="javascript:document.images.MenuLoginCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"></a></span>
								</div>
								<?php } ?>
								
								<span>
									<?php echo $this->Js->submit(__('Login'), array(
										'before'=>$this->Js->get('#pleasewait, #LoginMessage')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'update'=>'#LoginMessage',
										'class'=>'login_button',
										'controller'=>'login',
										'action'=>'index',
										'url'   => array('controller' => 'login', 'action' => 'index')
									));?>
								</span>
								<?php if($SITECONFIG["chkfb"]==1 || $SITECONFIG["chkgoogle"]==1){ ?>
									<div class="sign-in-text">
										<span><?php echo __('Sign in With'); ?> : </span>
										<?php if($SITECONFIG["chkfb"]==1){ // If admin has enabled facebook login ?>
											<?php echo $this->Js->link($this->html->image('facebook.png', array('alt'=>'', 'align' =>'absmiddle')), array('controller'=>'login', "action"=>"with/facebook"), array(
												'update'=>'#LoginMessage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false
											));?>
										<?php }?>
										<?php if($SITECONFIG["chkgoogle"]==1){  // If admin has enabled google login ?>
											&nbsp;<?php echo $this->Js->link($this->html->image('google.png', array('alt'=>'', 'align' =>'absmiddle')), array('controller'=>'login', "action"=>"with/google"), array(
												'update'=>'#LoginMessage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false
											));?>
										<?php }?>
									</div>
								<?php }?>
								<div class="login-links">
									<?php echo $this->html->link(__("Register"), array('controller' => 'register', 'action' => 'index'));?> | 
									<?php echo $this->html->link(__("Forgot Password"), array('controller' => 'forgotpassword', 'action' => 'index'));?>
									<?php if($SITECONFIG["emailconfirmation"]==1){ // To be shown only if email confirmation is required for members ?>
									 | <?php echo $this->html->link(__("Resend Activation Link"), array('controller' => 'public', 'action' => 'resendactivation'));?>
									<?php }?>
								</div>
						<?php echo $this->Form->end();?>
						<?php } ?>
					
						</div>
				  </div>
				</div>
			  </div>
			</div>
			<?php }?>
			<?php // Code to show login box ends here. ?>
		
        </div>
     </div>
	
	<div id="menubg">
		<div class="mainmenu">
			<div class="logotemp"><a href="<?php echo $SITEURL;?>" ><?php echo $this->Html->image('logo.png', array('alt' => $SITECONFIG['sitetitle']))?></a></div>
			<div id="menu">
				<!-- Menu Code Start -->
				<div class="navbar navbar-default" role="navigation">
					<div class="container-fluid">
						<div class="navbar-collapse collapse">
							<ul class="nav navbar-nav">
								<li class="active"><a href="<?php echo $SITEURL;?>"><?php echo __("Home");?></a></li>
								<li><?php echo $this->html->link(__("Details"), array('controller' => 'public', 'action' => 'details', 'plugin' => false));?></li>
								<li><?php echo $this->html->link(__("News"), array('controller' => 'public', 'action' => 'news', 'plugin' => false));?></li>
								<li><?php echo $this->html->link(__("F.A.Q."), array('controller' => 'public', 'action' => 'faqs', 'plugin' => false));?></li>
								<li><?php echo $this->html->link(__("Testimonials"), array('controller' => 'public', 'action' => 'testimonials', 'plugin' => false));?></li>
								<?php // Show only if admin has enabled from adminpanel ?>
						<?php if($SITECONFIG["recentpay"]==1){ ?><li><?php echo $this->html->link(__("Recent Payouts"), array('controller' => 'public', 'action' => 'recentpayouts', 'plugin' => false));?></li><?php }?>
						
								<li><?php echo $this->html->link(__("Support"), array('controller' => 'public', 'action' => 'support', 'plugin' => false));?></li>
								<li class="showonmobile"><?php echo $this->html->link(__("Join Now"), array('controller' => 'register', 'action' => 'index', 'plugin' => false));?></li>
								<li class="showonmobile"><?php echo $this->html->link(__("Login"), array('controller' => 'login', 'action' => 'index', 'plugin' => false));?></li>
								<?php if($PublicMenu["Business Directory"]==1){?>
									<li><?php echo $this->html->link(__("Business Directory"), array('controller' => 'directory', 'action' => 'viewall', 'plugin' => false));?></li>
								<?php } ?>
								
								<?php // Code to show public virtual pages ?>
								<?php if($PublicMenu["Public Pages"]==1 && (count($VirtualPageList)>0)){?>
								<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo __('Public Pages'); ?> <b class="caret"></b></a>
								  <ul class="dropdown-menu">
									<?php if(count($VirtualPageList)>0) {foreach($VirtualPageList as $pageid=>$pagename){?>
									<li><a href="<?php echo $SITEURL."public/page/".$pageid;?>"><?php echo $pagename;?></a></li>
									<?php }}?>
								  </ul>
								</li>
								<?php } ?>
								<li><?php echo $this->html->link(__("Member Representative"), array('controller' => 'member', 'action' => 'representative', 'plugin' => false));?></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Menu Code Over -->
			</div>
			<div class="clearboth"></div>
		</div>
   </div>
	<div class="fixmenuspacing"></div>
	
	<?php if(((!$isLogin && isset($PublicMenu["Statistics"]) && $PublicMenu["Statistics"]==1) || ($isLogin && isset($MemberMenu["Statistics"]) && $MemberMenu["Statistics"]==1)) && (($isLogin && substr($SITECONFIG["memberstatistics"],0,1)==1) || (!$isLogin && substr($SITECONFIG["publicstatistics"],0,1)==1)))
	{ ?>
		<div id="statisticsbg">
			<div class="pagewidth">
				<div class="statisticstitle">Site Statistics</div>
				<div class="statisticsmain">
			  
				  <div class="statisticsbox">
					  <div class="triangle-up1"></div>
					  <div class="statisticsmidal1">
							<div><span class="glyphicon glyphicon-calendar"></span></div>
							<div class="statisticstitle1"><?php echo __('Launch Date');?></div>
							<div class="statisticstext"><?php if($StatisticLaunchDate!=''){ if($StatisticLaunchDate!="N/A"){ echo substr($this->Time->format($SITECONFIG["timeformate"], $StatisticLaunchDate), 0, -9); }else{ echo $StatisticLaunchDate; }}?></div>
					  </div>
					  <div class="triangle-down1"></div>
				  </div>
				
				  <div class="statisticsbox">
					  <div class="triangle-up2"></div>
					  <div class="statisticsmidal2">
							<div><span class="glyphicon glyphicon-user"></span></div>
							<div class="statisticstitle2"><?php echo __('Total Members');?></div>
							<div class="statisticstext"><?php echo $StatisticTotalMember;?></div>
					  </div>
					  <div class="triangle-down2"></div>
				  </div>
			  
				  <div class="statisticsbox">
					  <div class="triangle-up3"></div>
					  <div class="statisticsmidal3">
							<div><span class="glyphicon glyphicon-globe"></span></div>
							<div class="statisticstitle3"><?php echo __('Online Members');?></div>
							<div class="statisticstext"><div id="OnlineMembers"><?php echo $OnlineMembers;?></div></div>
					  </div>
					  <div class="triangle-down3"></div>
				  </div>
			  
				  <div class="statisticsbox">
					  <div class="triangle-up4"></div>
					  <div class="statisticsmidal4">
							<div>
								<span style="display: inline-block; font-size: 25px; width: 9px;" class="glyphicon glyphicon-usd"></span>
								<span style="display: inline-block; font-size: 40px; width: 25px;" class="glyphicon glyphicon-usd"></span>
								<span style="display: inline-block; font-size: 25px;" class="glyphicon glyphicon-usd"></span>
							</div>
							<div class="statisticstitle4"><?php echo __('Total Payouts');?></div>
							<div class="statisticstext">$<?php echo $StatisticTotalPayouts;?></div>
					  </div>
					  <div class="triangle-down4"></div>
				  </div>
			  
				  <div class="statisticsbox" style="margin-right: 0px;">
					  <div class="triangle-up5"></div>
					  <div class="statisticsmidal5">
							<div><span class="glyphicon glyphicon-usd"></span></div>
							<div class="statisticstitle5"><?php echo __('Last Payout');?></div>
							<div class="statisticstext">$<?php echo $StatisticTotalLastPayouts;?></div>
					  </div>
					  <div class="triangle-down5"></div>
				  </div>
	  
				</div>
			</div>
		</div>
	<?php
	} ?>
	
	<?php if((!$isLogin && isset($PublicMenu["Banner Ads"]) && $PublicMenu["Banner Ads"]==1) || ($isLogin && isset($MemberMenu["Banner Ads"]) && $MemberMenu["Banner Ads"]==1))
	{ ?>
		<div id="bannerbg">
			<div class="pagewidth">
			  <div class="bannermain">
					<div class="banner125"><span id="advertisement_125_1"></span></div>
					<div class="banner125"><span id="static_125_1"></span></div>
					<div class="banner468main">
						<div class="banner468"><span id="advertisement_468_1"></span></div>
						 <div class="banner468"><span id="rotating_468_1"></span></div>
					</div>
					<div class="banner125"><span id="rotating_125_1"></span></div>
					<div class="banner125"><span id="advertisement_125_2"></div>
			  </div>
			</div>
		</div>
	<?php
	} ?>
    <div id="allcontentbg">
        <div id="allcontent">
<?php }else{ // If member is logged in(Member side) ?>


	<div class="membertop">
		<div class="memberlogo"><a href="<?php echo $SITEURL;?>member" ><?php echo $this->Html->image('logo.png', array('alt' => $SITECONFIG['sitetitle'], 'width'=> 266, 'height'=> 56))?></a></div>
		
		<!-- Menu Code for Mobile Start -->
		<div class="navbar-header navbar-default">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		</div>
		<div id="menu" class="afterlogin">
		  <div class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
			  <div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
				  <li><?php echo $this->html->link(__("Overview"), array('controller' => 'member', 'action' => 'index', 'plugin' => false), array('class' => 'Overview'));?></li>
				</ul>
			  </div>
			</div>
		  </div>
		</div>
		<!-- Menu Code for Mobile Over -->
	
		<div class="adbtn"><?php echo $this->html->link(__("Add Funds"), array('controller' => 'member', 'action' => 'addfund', 'plugin' => false));?></div>
		<div class="adbtn">
			<?php echo $this->html->link(__("Purchase Position"), 'javascript:void(0)', array('class' => '', 'onclick' => 'window.location=$(".purchaseaction").attr("href");'));?>
		
		</div>
		<div class="toprightmember">
			<div class="languagetop">
				<div class="select-main yellowbg">
				  <label>
					<?php // Code for language change box starts here ?>
					<?php if($LanguageBox != ''){ echo $LanguageBox; } ?>
					<?php // Code for language change box ends here ?>
				  </label>
				</div>
			</div>
			
			<div class="otherbtn">
				<?php echo $this->html->link('<span class="glyphicon glyphicon-user icontop"></span>'.__("Overview"), array('controller' => 'member', 'action' => 'index', 'plugin' => false), array('escape'=>false));?>
			  
			</div>
			
			<div class="btn-group">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<span class="glyphicon glyphicon-th-list icontop"></span>
					Other
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu topothermenu" role="menu">
					<li><?php echo $this->html->link(__("News"), array('controller' => 'public', 'action' => 'news', 'plugin' => false));?></li>
					<li><?php echo $this->html->link(__("F.A.Q."), array('controller' => 'public', 'action' => 'faqs', 'plugin' => false));?></li>
					<?php // Show only if admin has enabled from adminpanel ?>
					<?php if($SITECONFIG["recentpay"]==1)
					{ ?>
						<li><?php echo $this->html->link(__("Recent Payouts"), array('controller' => 'public', 'action' => 'recentpayouts', 'plugin' => false));?></li><?php
					}?>
					<li><?php echo $this->html->link(__("Support"), array('controller' => 'member', 'action' => 'support', 'plugin' => false));?></li>
					<li><?php echo $this->html->link(__("Testimonials"), array('controller' => 'public', 'action' => 'testimonials', 'plugin' => false));?></li>
					<li class="showonmobile"><?php echo $this->html->link(__("Logout"), array('controller' => 'login', 'action' => 'logout', 'plugin' => false), array('class' => 'logout'));?></li>
			   </ul>
			</div>
			
			<div class="logout">
				<?php echo $this->html->link('<span class="glyphicon glyphicon-lock icontop"></span>'.__("Logout"), array('controller' => 'login', 'action' => 'logout', 'plugin' => false), array('escape'=>false));?>
			</div>
		</div>
    </div>
    <div class="membertopspacing"></div>
    <div class="members-area-main">
	<div id="members-area-left-side">
	     <div class="left_menu">

			<div id="open_left_menu" class="active_toggled">
				<div class="left-menu-fix">
					<div class="toggle_button_main">
						<div class="toggle_button"></div>
					</div>
					<div class="navmenuleft">
						<ul>
							<?php $pageulr=str_replace($this->webroot, "",$this->here);?>
							<?php if($MemberMenu["Account Settings"]==1) { ?>
							<?php if($this->params['controller']=="member" && ($this->params['action']=="messageread" || $this->params['action']=="membership" || $this->params['action']=="profile" || $this->params['action']=="addtestimonial" || $this->params['action']=="memberactivity"))
							{
								$menuclass='class="dropdown-toggle mainmanu act"';
								$menucaret='';
								$menuddclass='';
							}else {
								$menuclass='class="dropdown-toggle mainmanu"';
								$menucaret='';
								$menuddclass='';
							}?>
							<li class="dropdown <?php echo $menuddclass?> loginmenu1">
								<a href="javascript:void(0)" onclick="$('#membermenu .navbar-nav li.dropdown').removeClass('tkopen');" data-toggle="dropdown" <?php echo "class='accountactivity'";?>><span><?php echo __('Account Activity');?> <?php echo $menucaret?></span></a>
								<ul class="dropdown-menu">
									<?php if(in_array('Profile', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Profile"), array('controller' => 'member', 'action' => 'profile', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Security', $themesubmenu)){?>	
										<li><?php echo $this->html->link(__("Security"), array('controller' => 'member', 'action' => 'profile/security', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Upgrade Account', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Upgrade Account"), array('controller' => 'member', 'action' => 'membership', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Rate Us', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Rate Us"), array('controller' => 'member', 'action' => 'addtestimonial', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Activity Logs', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Activity Logs"), array('controller' => 'member', 'action' => 'memberactivity', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Admin Messages', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Admin Messages"), array('controller' => 'member', 'action' => 'messageread', 'plugin' => false));?></li>
									<?php } ?>
								</ul>
							</li>
							<?php }?>
							
							<?php if($MemberMenu["Finance"]==1) { ?>
							<?php if(($this->params['controller']=="member" && ($this->params['action']=="withdrawal_history" || $this->params['action']=="paymenthistory" || $this->params['action']=="commission" || $this->params['action']=="withdrawal" || $this->params['action']=="addfund")) || ($this->params['controller']=="module1" && ($this->params['action']=="index" || $this->params['action']=="dynamic" || $this->params['action']=="dailyposition" || $this->params['action']=="dailyposition")) || ($this->params['controller']=="module2" && ($this->params['action']=="index" || $this->params['action']=="position" || $this->params['action']=="matrix")) || ($this->params['controller']=="balancetransfer" && ($this->params['action']=="index" || $this->params['action']=="pending" || $this->params['action']=="history" || $this->params['action']=="history" || $this->params['action']=="history2")))
							{
								$menuclass='class="dropdown-toggle mainmanu act"';
								$menucaret='';
								$menuddclass='';
							}else {
								$menuclass='class="dropdown-toggle mainmanu"';
								$menucaret='';
								$menuddclass='';
							}?>
							<li class="dropdown <?php echo $menuddclass?> loginmenu4">
								<a href="javascript:void(0)" onclick="$('#membermenu .navbar-nav li.dropdown').removeClass('tkopen');" data-toggle="dropdown" <?php echo "class='finance'";?>><span><?php echo __('Finance');?></span> <?php echo $menucaret?></a>
								<ul class="dropdown-menu">
									<?php if(in_array('Add Fund', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Add Funds"), array('controller' => 'member', 'action' => 'addfund', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Purchase Position', $themesubmenu)){ 
										$modulename="";$moduleaction='';
										$modules=@explode(",",trim($SITECONFIG["modules"],","));
										foreach($modules as $module)
										{
											$modulearray=@explode(":", $module);
											if($modulearray[2]==1 && $modulearray[18]!="")
											{
												$subplanarray=explode("-",$modulearray[18]);
												$modulename=$modulearray[1];
												foreach($subplanarray as $subplan)
												{
													if($subplan !='')
													{
														$moduleaction=$subplan;
														break;
													}
												}
											}
											if($moduleaction != '')
												break;
										}
										if($moduleaction!='')
										{
										?>
											<li><?php echo $this->html->link(__("Purchase Position"), array('controller' => $modulename, 'action' => $moduleaction, 'plugin' => false),array('class'=>'purchaseaction'));?></li>
									<?php } }?>
										<?php if(in_array('Earning History', $themesubmenu)){?>
										<?php
										$modulename="";$moduleaction='';
										$modules=@explode(",",trim($SITECONFIG["modules"],","));
										foreach($modules as $module)
										{
											$modulearray=@explode(":", $module);
											if($modulearray[2]==1 && $modulearray[19]!="")
											{
												$subplanarray=explode("-",$modulearray[19]);
												$modulename=$modulearray[1];
												foreach($subplanarray as $subplan)
												{
													if($subplan !='')
													{
														$moduleaction=$subplan;
														break;
													}
												}
											}
											if($moduleaction != '')
												break;
										}
										if($moduleaction=='')
										{
											$modulename='member';
											$moduleaction='commission';
										}
									?>
									
									<li><?php echo $this->html->link(__("Earning History"), array('controller' => $modulename, 'action' => $moduleaction, 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Payment History', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Payment History"), array('controller' => 'member', 'action' => 'paymenthistory', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Withdrawal Request', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Request Withdrawal"), array('controller' => 'member', 'action' => 'withdrawal', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Withdrawal History', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Withdrawal History"), array('controller' => 'member', 'action' => 'withdrawal_history', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Balance Transfer', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Balance Transfer"), array('controller' => 'balancetransfer', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
								</ul>
							</li>
							<?php }?>
							
							<?php if($MemberMenu["Advertisement"]==1) { ?>
							<?php if(($this->params['controller']=="advertisement" && $this->params['action']=="mycredits") || ($this->params['controller']=="bannerad" && ($this->params['action']=="index" || $this->params['action']=="plans")) || ($this->params['controller']=="textad" && ($this->params['action']=="index" || $this->params['action']=="plans")) || ($this->params['controller']=="soload" && ($this->params['action']=="index" || $this->params['action']=="saved" || $this->params['action']=="plans")) || ($this->params['controller']=="loginad" && ($this->params['action']=="index")) || ($this->params['controller']=="directory" && ($this->params['action']=="index"))  || ($this->params['controller']=="ppc" && ($this->params['action']=="index")) || ($this->params['controller']=="ptc" && ($this->params['action']=="index")) || ($this->params['controller']=="traffic" && ($this->params['action']=="index" || $this->params['action']=="startpagepurchase" || $this->params['action']=="surf" || $this->params['action']=="website")) || ($this->params['controller']=="ppctextad" && ($this->params['action']=="index")))
							{
								$menuclass='class="dropdown-toggle mainmanu act"';
								$menucaret='';
								$menuddclass='';
							}else {
								$menuclass='class="dropdown-toggle mainmanu"';
								$menucaret='';
								$menuddclass='';
							}?>
							<li class="dropdown <?php echo $menuddclass?> loginmenu5">
								<a href="javascript:void(0)" onclick="$('#membermenu .navbar-nav li.dropdown').removeClass('tkopen');" data-toggle="dropdown" <?php echo "class='advertisement'";?>><span><?php echo __('Advertisement');?></span> <?php echo $menucaret?></a>
								<ul class="dropdown-menu">
									<?php if(in_array('Banner Ads', $themesubmenu)){?>
										<?php if($SITECONFIG["enable_bannerads"]==1){ ?>
											<li><?php echo $this->html->link(__("Banner Ads"), array('controller' => 'bannerad', 'action' => 'index', 'plugin' => false));?></li>
										<?php }elseif(strpos($SITECONFIG["banneradplansetting"],'isenable|1') !== false){ ?>
											<li><?php echo $this->html->link(__("Banner Ads"), array('controller' => 'bannerad', 'action' => 'plans', 'plugin' => false));?></li>
										<?php }?>
									<?php }?>
									<?php if(in_array('Text Ads', $themesubmenu)){?>	
										<?php if($SITECONFIG["enable_textads"]==1){ ?>
											<li><?php echo $this->html->link(__("Text Ads"), array('controller' => 'textad', 'action' => 'index', 'plugin' => false));?></li>
										<?php }elseif(strpos($SITECONFIG["textadplansetting"],'isenable|1') !== false){ ?>
											<li><?php echo $this->html->link(__("Text Ads"), array('controller' => 'textad', 'action' => 'plans', 'plugin' => false));?></li>
										<?php }?>
									<?php }?>
									<?php if(in_array('Solo Ads', $themesubmenu)){?>	
										<?php if($SITECONFIG["enable_soloads"]==1){ ?>
											<li><?php echo $this->html->link(__("Solo Ads"), array('controller' => 'soload', 'action' => 'index', 'plugin' => false));?></li>
										<?php }elseif(strpos($SITECONFIG["soloadplansetting"],'isenable|1') !== false){ ?>
											<li><?php echo $this->html->link(__("Solo Ads"), array('controller' => 'soload', 'action' => 'plans', 'plugin' => false));?></li>
										<?php }?>
									<?php }?>
									<?php if(in_array('Login Ad Plans', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Login Ad Plans"), array('controller' => 'loginad', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('PPC', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("PPC"), array('controller' => 'ppc', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('PTC', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("PTC"), array('controller' => 'ptc', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('BusinessDirectory', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Business Directory"), array('controller' => 'directory', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
                                    
                                    <?php if(strpos($SITECONFIG["surfingsetting"],'enablesurfing|1') !== false){
                                            if(strpos($SITECONFIG["surfingsetting"],'surfselect|0') !== false && strpos($SITECONFIG["trafficsetting"],'enablesurfing|1') !== false){
                                    ?>
                                        
									<?php if(in_array('Website Credit Plans', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Buy Credits"), array('controller' => 'traffic', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Start Page', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Buy Start Page"), array('controller' => 'traffic', 'action' => 'startpagepurchase', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Start Surfing', $themesubmenu)){?>
										<li>
										<a href="<?php echo str_replace("https://", "http://", $SITEURL);?>traffic/surf" target="_blank"><?php echo __('Start Surfing'); ?></a>
										<?php //echo $this->html->link(__("Start Surfing"), array('controller' => 'traffic', 'action' => 'surf', 'plugin' => false));?></li>
									<?php } ?>
									
									<?php if(in_array('My Websites', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("My Websites"), array('controller' => 'traffic', 'action' => 'website', 'plugin' => false));?></li>
									<?php } ?>
                                    
                                    <?php } // Check Surf Select is Traffic Exchange or not and it must be on ?>        
                                    <?php } //Check Main Surfing ON or OFF  ?>   
									<?php if(in_array('PPC Text Ads', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Buy PPC Text Ads"), array('controller' => 'ppctextad', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Credit Statement', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Credit Statement"), array('controller' => 'advertisement', 'action' => 'mycredits', 'plugin' => false));?></li>
									<?php }?>
								</ul>
							</li>
							<?php }?>
							
							<?php if($MemberMenu["Member Tools"]==1) { ?>
							<?php if(($this->params['controller']=="ptc" && $this->params['action']=="view")||($this->params['controller']=="urlrotator" && $this->params['action']=="index")||($this->params['controller']=="urlshortener" && $this->params['action']=="index"))
							{
								$menuclass='class="dropdown-toggle mainmanu act"';
								$menucaret='';
								$menuddclass='';
							}else {
								$menuclass='class="dropdown-toggle mainmanu"';
								$menucaret='';
								$menuddclass='';
							}?>
							<li class="dropdown <?php echo $menuddclass?> loginmenu2">
								<a href="javascript:void(0)" onclick="$('#membermenu .navbar-nav li.dropdown').removeClass('tkopen');" data-toggle="dropdown" <?php echo "class='membertools'";?>><span><?php echo __('Member Tools');?></span> <?php echo $menucaret?></a>
								<ul class="dropdown-menu">
									<?php if(in_array('View PTC', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("View PTC Ads"), array('controller' => 'ptc', 'action' => 'view', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('URL Rotator', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("URL Rotator"), array('controller' => 'urlrotator', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('URL Shortener', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("URL Shortener"), array('controller' => 'urlshortener', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Contest', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Referral Contests"), array('controller' => 'contest', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Jackpot', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Jackpot"), array('controller' => 'jackpot', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
								</ul>
							</li>						
							<?php } ?>
							
							<?php if($MemberMenu["Promotional Tools"]==1) { ?>
							<?php if(($this->params['controller']=="statbanner" && $this->params['action']=="index")||($this->params['controller']=="promotools" && ($this->params['action']=="banners" || $this->params['action']=="splash" || $this->params['action']=="landing" || $this->params['action']=="textlink" || $this->params['action']=="email" || $this->params['action']=="tellfriends"))||($this->params['controller']=="member" && $this->params['action']=="referrals"))
							{
								$menuclass='class="dropdown-toggle mainmanu act"';
								$menucaret='';
								$menuddclass='';
							}else {
								$menuclass='class="dropdown-toggle mainmanu"';
								$menucaret='';
								$menuddclass='';
							}?>
							<li class="dropdown <?php echo $menuddclass?> loginmenu3">
								<a href="javascript:void(0)" onclick="$('#membermenu .navbar-nav li.dropdown').removeClass('tkopen');" data-toggle="dropdown" <?php echo "class='promotools'";?>><span><?php echo __('Promo Tools');?></span> <?php echo $menucaret?></a>
								<ul class="dropdown-menu openfrombottom">
									<?php if(in_array('Banner', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Promotional Banners"), array('controller' => 'promotools', 'action' => 'banners', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Dynamic Banners', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Dynamic Banners"), array('controller' => 'statbanner', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Splash Pages', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Splash Pages"), array('controller' => 'promotools', 'action' => 'splash', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Landing Pages', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Landing Pages"), array('controller' => 'promotools', 'action' => 'landing', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Text Links', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Text Links"), array('controller' => 'promotools', 'action' => 'textlink', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Promotional Emails', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Promotional Emails"), array('controller' => 'promotools', 'action' => 'email', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Tell Friends', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Tell Friends"), array('controller' => 'promotools', 'action' => 'tellfriends', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('My Referrals', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("My Referrals"), array('controller' => 'member', 'action' => 'referrals', 'plugin' => false));?></li>
									<?php }?>
								</ul>
							</li>
							<?php }?>
							
							<?php if($MemberMenu["Member Pages"]==1 && count($VirtualPageList)>0) { ?>
							<?php if($this->params['controller']=="member" && $this->params['action']=="page")
							{
								$menuclass='class="dropdown-toggle mainmanu act"';
								$menucaret='';
								$menuddclass='';
							}else {
								$menuclass='class="dropdown-toggle mainmanu"';
								$menucaret='';
								$menuddclass='';
							}?>
							<li class="dropdown <?php echo $menuddclass?> loginmenu6" style="">
								<a href="javascript:void(0)" onclick="$('#membermenu .navbar-nav li.dropdown').removeClass('tkopen');" data-toggle="dropdown" <?php echo "class='memberpages'";?>><span><?php echo __('Member Pages');?></span> <?php echo $menucaret?></a>
								<ul class="dropdown-menu openfrombottom">
									<?php if(count($VirtualPageList)>0){ foreach($VirtualPageList as $pageid=>$pagename){?>
									<li><a href="<?php echo $SITEURL."member/page/".$pageid;?>"><?php echo $pagename;?></a></li>
									<?php }}?>
								</ul>
							</li>
							<?php } ?>
							<li class="showonmobile"><?php echo $this->html->link(__("News"), array('controller' => 'public', 'action' => 'news', 'plugin' => false));?></li>
							<li class="showonmobile"><?php echo $this->html->link(__("F.A.Q."), array('controller' => 'public', 'action' => 'faqs', 'plugin' => false));?></li>
							<li class="showonmobile"><?php echo $this->html->link(__("Support"), array('controller' => 'member', 'action' => 'support', 'plugin' => false));?></li>
							<li class="showonmobile"><?php echo $this->html->link(__("Testimonials"), array('controller' => 'public', 'action' => 'testimonials', 'plugin' => false));?></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div id="members-area-right-side">
	
<?php }?>
			
			
	<?php if($isLogin){?>
		
		<?php if(((!$isLogin && isset($PublicMenu["Statistics"]) && $PublicMenu["Statistics"]==1) || ($isLogin && isset($MemberMenu["Statistics"]) && $MemberMenu["Statistics"]==1)) && (($isLogin && substr($SITECONFIG["memberstatistics"],0,1)==1) || (!$isLogin && substr($SITECONFIG["publicstatistics"],0,1)==1)))
		{ ?>
			<div class="memberwhitebox memberwhitebox1">
				<div class="memberlaunchboxmain">
					<div class="memberlaunvhbox">
						<div class="launchicon"><span class="glyphicon glyphicon-calendar"></span></div>
						<div class="launchyellowbox-1"><?php echo __('Launch Date');?></div>
						<div class="launch-down1"></div>
						<div class="launchtextin"><?php if($StatisticLaunchDate!=''){ if($StatisticLaunchDate!="N/A"){ echo substr($this->Time->format($SITECONFIG["timeformate"], $StatisticLaunchDate), 0, -9); }else{ echo $StatisticLaunchDate; }}?></div>
					</div>
					<div class="memberlaunvhbox">
						<div class="launchicon"><span class="glyphicon glyphicon-user"></span></div>
						<div class="launchyellowbox-2"><?php echo __('Total Members');?></div>
						<div class="launch-down2"></div>
						<div class="launchtextin"><?php echo $StatisticTotalMember;?></div>
					</div>
					<div class="memberlaunvhbox">
						<div class="launchicon"><span class="glyphicon glyphicon-globe"></span></div>
						<div class="launchyellowbox-3"><?php echo __('Online Members');?></div>
						<div class="launch-down3"></div>
						<div class="launchtextin"><div id="OnlineMembers"><?php echo $OnlineMembers;?></div></div>
					</div>
					<div class="memberlaunvhbox">
						<div class="launchicon">
								<span style="display: inline-block; font-size: 25px; width: 9px;" class="glyphicon glyphicon-usd"></span>
								<span style="display: inline-block; font-size: 40px; width: 25px;" class="glyphicon glyphicon-usd"></span>
								<span style="display: inline-block; font-size: 25px;" class="glyphicon glyphicon-usd"></span>
						</div>
						<div class="launchyellowbox-4"><?php echo __('Total Payouts');?></div>
						<div class="launch-down4"></div>
						<div class="launchtextin">$<?php echo $StatisticTotalPayouts;?></div>
					</div>
					<div class="memberlaunvhbox">
						<div class="launchicon"><span class="glyphicon glyphicon-usd"></span></div>
						<div class="launchyellowbox-5"><?php echo __('Last Payout');?></div>
						<div class="launch-down5"></div>
						<div class="launchtextin">$<?php echo $StatisticTotalLastPayouts;?></div>
					</div>
				</div>
			</div>
	    <?php
		}?>
		
	    <div id="memberallcontentmain">
		
		<?php if((!$isLogin && isset($PublicMenu["Banner Ads"]) && $PublicMenu["Banner Ads"]==1) || ($isLogin && isset($MemberMenu["Banner Ads"]) && $MemberMenu["Banner Ads"]==1))
		{ ?>
			<div class="bannermain topbannermain" style="padding-top: 10px;padding-bottom: 0px;">
				<?php // Code to show 125x125 advertisement banner ?>
				<div class="banner468main" style="width: auto;">
					<?php // Code to show 468x60 banner from banner ad plans ?>
					<div class="banner468"><span id="static_468_1"></span></div>
					<?php // Code to show 468x60 advertisement banner ?>
					<div class="banner468" style="margin-left: 15px;"><span id="advertisement_468_1"></span></div>
				</div>
				<?php // Code to show 125x125 banner from banner ad plans ?>
			</div>
		<?php
		} ?>
		
	<?php }
	else{?><?php }?>
		<div id="right-bar">
			<div id="pagecontent">
				
				<?php // Code to show launch date widget starts here ?>
				<?php if($LaunchDateWidget!="")
				{
					echo $this->Javascript->link('launchdate');
					echo $LaunchDateWidget;
				} ?>
				<?php // Code to show launch date widget ends here ?>
				
				<?php // Code to show admin message(s) starts here ?>
				<?php $mmdata=$this->Session->read('membermessage_data');
				if(count($mmdata)){?>
				<div id="membermessagemain">
					<div class="j-success-box">
						<div class="success-icon"></div>
						<div class="j-success-text"><a href="javascript:void(0)" onclick="if($('.membermessage_box').css('display')=='none'){$('.membermessage_box').show(800);}else{$('.membermessage_box').hide(500);}"><?php echo count($mmdata).' '.__('Unread Message(s) From Admin');?></a></div>
						<div class="clear-both"></div>
					</div>
					<?php for($mm=0;$mm<count($mmdata);$mm++){?>
					<div class="membermessage_box" style="display:none;">
						<div class="box">
							<div id="updatemsg<?php echo $mm;?>">
							<div class="date">
								<div class="divtable textcenter">
									<div class="divtr">
										<div class="divtd textleft vat">
											<?php echo stripslashes($mmdata[$mm]['Membermessage_history']['description']); ?> | <?php echo $this->Time->format($SITECONFIG["timeformate"], $mmdata[$mm]['Membermessage_history']['messagedate']);?>
										</div>
										<div class="divtd textright vat">
											<?php echo $this->Form->create('Membermessage_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'messageread'.$mmdata[$mm]['Membermessage_history']['messagetype'])));
											echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$mmdata[$mm]['Membermessage_history']["id"], 'label' => false));
											echo $this->Js->submit(__('Mark as read'), array(
											  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
											  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											  'update'=>'#updatemsg'.$mm,
											  'class'=>'large white button',
											  'controller'=>'member',
											  'div'=>false,
											  'action'=>'messageread'.$mmdata[$mm]['Membermessage_history']['messagetype'],
											  'url' => array('controller' => 'member', 'action' => 'messageread'.$mmdata[$mm]['Membermessage_history']['messagetype'], 'plugin' => false)
											));
											echo $this->Form->end();?>
										</div>
									</div>
								</div>
							</div>
							<div class="comment"><?php echo nl2br(stripslashes($mmdata[$mm]['Membermessage_history']['message'.$this->Session->read('Config.language')]));?></div>
							<?php if(count($mmdata)== $mm+1){ echo "<div class='admin_messag_url';>".$this->html->link(__("...Read More"), array('controller' => 'member', 'action' => 'messageread', 'plugin' => false)); echo "</div><div class='clear-both'></div>"; } ?>	
						</div>
						</div>
					</div>
					<?php }?>
					<div class="height10"></div>
				</div>
				<?php }?>
				<?php // Code to show admin message(s) ends here ?>
				
				<?php // Code to show page wise content ?>
				<?php echo $this->fetch('content');?>
			</div>
        </div>
	<?php
	if($isLogin)
	{?>
		</div>
	
		<?php if((!$isLogin && isset($PublicMenu["Banner Ads"]) && $PublicMenu["Banner Ads"]==1) || ($isLogin && isset($MemberMenu["Banner Ads"]) && $MemberMenu["Banner Ads"]==1))
		{ ?>
			<div class="memberwhitebox memberwhitebox2">
			  <div class="bannermain">
				  <div class="banner125"><span id="advertisement_125_1"></span></div>
				  <div class="banner125"><span id="static_125_1"></span></div>
				  <div class="banner468main">
					  <div class="banner468"><span id="advertisement_468_2"></span></div>
					   <div class="banner468"><span id="rotating_468_1"></span></div>
				  </div>
				  <div class="banner125"><span id="rotating_125_1"></span></div>
				  <div class="banner125"><span id="advertisement_125_2"></div>
				  
				  <div class="height10"></div>
				  <div class="textcenter"><span id="static_728_1"></span></div>
				  <div class="height10"></div>
				  <div class="textcenter"><span id="rotating_728_1"></span></div>
			  </div>
			</div>
		<?php
		}?>
	<?php
	}
	else
	{?></div></div><?php
	}?>
	
	<?php // Code to show text ads starts here. ?>
	<?php if((!$isLogin && isset($PublicMenu["Text Ads"]) && $PublicMenu["Text Ads"]==1) || ($isLogin && isset($MemberMenu["Text Ads"]) && $MemberMenu["Text Ads"]==1)) { ?>
	
			<?php
			if($isLogin)
			{
				?>
					
					<div class="memberwhitebox memberwhitebox3">
						<div class="textadsmain membersarea">
							<?php // Code to show advertisement text ads ?>
							<?php for($i=0; $i<count($TextAdsList); $i++){?>
								<?php if(!is_array($TextAdsList[$i])){ ?><div class="textadbox"><?php echo $TextAdsList[$i]; ?></div><?php }else{ ?>
									<div class="textadbox">
										<div class="text-add-title">
											<?php echo $this->Html->link($TextAdsList[$i]["title"], array('controller' => 'public', 'action' => 'counter/textad/'.$TextAdsList[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["title_color"].';')); ?>
										</div>
										<div class="textadcont">
											<?php 
											echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$TextAdsList[$i]["desc1"].'</span>';
											echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$TextAdsList[$i]["desc2"].'</span>';
											if($TextAdsList[$i]["showurl"])
												echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'public', 'action' => 'counter/textad/'.$TextAdsList[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
											?>
										</div>
									</div>
								<?php } ?>
							<?php }?>
							<?php // Code to show text ads from text ad plans. ?>
							<div class="textadbox"><span id="static_textad_1"></span></div>
							<div class="textadbox"><span id="rotating_textad_1"></span></div>
						</div>
					</div>
				<?php
			}
			else
			{
				?>
					
				<div id="textadsbg">
				  <div class="pagewidth">
					<div class="textadsmain pubilcarea">
						<?php // Code to show advertisement text ads ?>
						<?php for($i=0; $i<count($TextAdsList); $i++){?>
							<?php if(!is_array($TextAdsList[$i])){ ?><div class="textadbox"><?php echo $TextAdsList[$i]; ?></div><?php }else{ ?>
								<div class="textadbox">
									<div class="text-add-title">
										<?php echo $this->Html->link($TextAdsList[$i]["title"], array('controller' => 'public', 'action' => 'counter/textad/'.$TextAdsList[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["title_color"].';')); ?>
									</div>
									<div class="textadswhitebox">
										<div class="triangle-down"></div>
										<div class="textadcont">
											<?php 
											echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$TextAdsList[$i]["desc1"].'</span>';
											echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$TextAdsList[$i]["desc2"].'</span>';
											if($TextAdsList[$i]["showurl"])
												echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'public', 'action' => 'counter/textad/'.$TextAdsList[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
											?>
										</div>
									</div>
									<div class="triangle-bottom"></div>
								</div>
							<?php } ?>
						<?php }?>
						<?php // Code to show text ads from text ad plans. ?>
						<div class="textadbox textadtoappend"><span id="static_textad_1"></span><div class="triangle-bottom"></div></div>
						<div class="textadbox textadtoappend"><span id="rotating_textad_1"></span><div class="triangle-bottom"></div></div>
					</div>
				  </div>
				</div>
				<?php
			}?>
		  
		  
    <?php } ?>
	<?php // Code to show text ads ends here. ?>
	
	<span id='advertisement_600_1'></span>
	<span id='advertisement_600_2'></span>
	<span id='advertisement_200_1'></span>
	<span id='advertisement_200_2'></span>
	
	<?php // Do not edit/remove this code line ?>
	<?php echo $BannerAdsList.$PPCBanner.$PPCtextad;?>
	
	<?php // Footer code starts here ?>
	<?php
	if($isLogin)
	{
		?>
			<div class="memberfootertext">
				<?php echo __('Copyright')." &copy; ".date("Y")." ". $SITECONFIG["sitetitle"]." ".__('All Rights Reserved');if($powered_by!='')echo ' | Powered by ';?>
				<?php
				// Do not remove below code
				echo $powered_by;
				// Do not remove above code
				?>
				|
				<?php echo $this->html->link(__("Terms & Conditions"), array('controller' => 'public', 'action' => 'terms', 'plugin' => false));?> |
				<?php echo $this->html->link(__("Privacy Policy"), array('controller' => 'public', 'action' => 'privcypolicy', 'plugin' => false));?> |
				<?php echo $this->html->link(__("Earning Disclaimer"), array('controller' => 'public', 'action' => 'earningdisclaimer', 'plugin' => false));?> |
				Graphics By : <a href="http://CheapMinisite.com">CheapMinisite.com</a>
			</div>
		<?php
	}
	else
	{
		?>
			<div class="footerbg">
				<div class="footer">
					<div class="footerlogobox"><?php echo $this->Html->image('logo.png', array('alt' => $SITECONFIG['sitetitle'], 'width'=> 266, 'height'=> 56))?></div>
					<div class="footermenu-bg">
						<span class="footermenu">
							<?php echo $this->html->link(__("T & C"), array('controller' => 'public', 'action' => 'terms', 'plugin' => false));?> |
							<?php echo $this->html->link(__("Privacy Policy"), array('controller' => 'public', 'action' => 'privcypolicy', 'plugin' => false));?> |
							<?php echo $this->html->link(__("Earning Disclaimer"), array('controller' => 'public', 'action' => 'earningdisclaimer', 'plugin' => false));?>
						</span>
					</div>
					<div class="footertext">
						<?php echo __('Copyright')." &copy; 2015 ". $SITECONFIG["sitetitle"]." ".__('All Rights Reserved');if($powered_by!='')echo ' | Powered by ';?>
						<?php
						// Do not remove below code
						echo $powered_by;
						// Do not remove above code
						?>
						
						| Graphics By:  <a href="http://CheapMinisite.com" target="_blank">CheapMinisite.com</a></div>
				</div>
			</div>
		<?php
	}?>
	


<?php // Scroll to top button ?>
<a class="scrollup" href="#" >^</a>

<?php // Do not edit below code. Code to show message ads ?>
<div style='display:none'>
<?php if(@$_COOKIE['popup1']<date("Ydm") && $MessageAdsPublic!=''){?><div id='colorboxpopup1' class="MeaasageAdContent"><?php echo nl2br(stripslashes($MessageAdsPublic));?><div id="MeaasageAdtimer">5</div></div><?php }?>
<?php if(@$_COOKIE['popup2']<date("Ydm") && $MessageAdsActive!=''){?><div id='colorboxpopup2' class="MeaasageAdContent"><?php echo nl2br(stripslashes($MessageAdsActive));?><div id="MeaasageAdtimer">5</div></div><?php }?>
<?php if(@$_COOKIE['popup3']<date("Ydm") && $MessageAdsPaid!=''){?><div id='colorboxpopup3' class="MeaasageAdContent"><?php echo nl2br(stripslashes($MessageAdsPaid));?><div id="MeaasageAdtimer">5</div></div><?php }?>
<?php if(@$_COOKIE['popup4']<date("Ydm") && $MessageAdsUnpaid!=''){?><div id='colorboxpopup4' class="MeaasageAdContent"><?php echo nl2br(stripslashes($MessageAdsUnpaid));?><div id="MeaasageAdtimer">5</div></div><?php }?>
<?php if(@$_COOKIE['popup5']<date("Ydm") && $MessageAdsMembership!=''){?><div id='colorboxpopup5' class="MeaasageAdContent"><?php echo nl2br(stripslashes($MessageAdsMembership));?><div id="MeaasageAdtimer">5</div></div><?php }?>
</div>

<?php // Do not edit above code ?>
<?php if($isLogin){?>
<script type="text/javascript">
  $(function() {
    if($(window).width()<768)
    {
      $("#menu.afterlogin ul:first").append($("#open_left_menu ul").html());
      $("#menu.afterlogin ul:first").append($("ul.topothermenu").html());
      $("#menu.afterlogin ul li .dropdown-toggle").append("<b class='caret'></b>");
    }
    if($(window).width()>768)
    {
      $("#memberallcontentmain").css({"min-height":$(window).height()-($(".membertop").height()+$(".memberfootertext").height()+$(".memberwhitebox1").height()+$(".memberwhitebox2").height()+$(".memberwhitebox3").height()+137)});
    }
  });
</script>
<script type="text/javascript">
	$(document).ready (function () {
		RV();FooterBottom();
	});
</script>
<script type="text/javascript">
$(document).ready(function() {
	$(".left-menu-fix").mCustomScrollbar({
		scrollInertia:200,
		setHeight:"100%",
		advanced:{
			updateOnContentResize: true
		}
	});
});
</script>

<?php }?>

<?php // Do not edit above code ?>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>
</body>
</html>