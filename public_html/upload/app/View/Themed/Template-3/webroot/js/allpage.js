$(document).ready(function($){vtip();});
$(document).ready( function() {
	$(".helpicon").click(function() {
		if($(".helpinformation" + this.title).css('display')=="none"){
			$('.helpinformation' + this.title).fadeIn("slow");
		}else{
			$('.helpinformation' + this.title).fadeOut("slow");
		}
	});
});
$(function() {
	$( ".datepicker" ).datepicker({
		showOn: "button",
		buttonImage: themepath+"img/common/calendar-t2.jpg",// add this image to :APP/WPROOT/IMG/
		buttonImageOnly: true,
		showAnim: 'clip',
		dateFormat: 'yy-mm-dd',
		showButtonPanel: true
	});
	
	//plugin page
	$( ".datepickerajax" ).datepicker({
		showOn: "button",
		buttonImage: themepath+"img/common/calendar-t2.jpg",// add this image to :APP/WPROOT/IMG/
		buttonImageOnly: true,
		showAnim: 'clip', //slideDown,fadeIn,blind,bounce,clip,drop,fold,slide
		dateFormat: 'yy-mm-dd',
		showButtonPanel: true
	});
});
$(".tinyMCEtriggerSavetk").hover(
	function () {tinyMCE.execCommand('mceRemoveControl', true, 'advancededitor');tinyMCE.triggerSave();},function () {}
);

$(document).ready( function() {
	$('input[type=submit]').mouseup(function() {
		$("html, body").animate({ scrollTop: 80 }, 'slow');
	});
});


/*Table Grid Start*/
$(function() {checkScreen_for_grid();});
$( window ).resize(function() {checkScreen_for_grid();});
function checkScreen_for_grid(){
	var width = $( window ).width();
	if (width<=600) initMobile_for_grid();
	else resetMobile_for_grid();
	//console.log(width);
}
function resetMobile_for_grid(){
	$( "#right-bar .divtable").each(function(i) {
		if ($(this).hasClass("mobile")){
			var divtext = "";
			$(this).find(".divtr").each(function(i) {
				$(this).find(".divtd").each(function(x){
					divtext = $(this).find("div:last-child").html();
					$(this).html(divtext);
				});			
			});
			$(this).removeClass("mobile");
		}
	});
}
function initMobile_for_grid()
{
	$( "#right-bar .divtable").each(function(i){		
		if ($(this).hasClass("mobile")===false){
			var headers = [];
			var htext = "";
			var divtext = "";
			$(this).find(".divtr .divth").each(function(i) {
				htext = $.trim($(this).text());
				headers.push(htext);
			});
			if (headers.length>0)
			{
				$(this).find(".divtr").each(function(i) {
					$(this).find(".divtd").each(function(x){
						if (headers[x]!=undefined)
							$(this).html("<div>"+headers[x]+"</div><div>"+$(this).html()+"</div>");
						else
							$(this).html("<div></div><div>"+$(this).html()+"</div>");
					});			
				});
				$(this).addClass("mobile");
			}
		}
	});
}
/*Table Grid Over*/