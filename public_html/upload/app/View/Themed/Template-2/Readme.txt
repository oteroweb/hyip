***** ProxCore Template-2 Readme *****

READ ME :
========

Advertisement Banners, Text ads and Statistics are really crucial for your site's look as well as performance.
As we provide this template by default with the script, we keep the above elements at the optimum balance to maintain both aspects.

So, when you are planning to keep this theme/template for your frontend, keep below settings to maintain look and feel :


1) Advertisement Settings
=========================

Design & CMS -> Themes -> Advertisement Settings -> Credit Based Advertisements Display Settings
------------------
Number of The Text Ads to be Shown on Public Area : 2
Number of The Text Ads to be Shown Inside Member Area : 2
Number of The Banners (125X125) to be Shown on Public Area : 2
Number of The Banners (125X125) to be Shown Inside Member Area : 2
Number of The Banners (468X60) to be Shown on Public Area : 1
Number of The Banners (468X60) to be Shown Inside Member Area : 2


Design & CMS -> Themes -> Advertisement Settings -> Plan Banner Ads Display Settings
------------------
Rotating Banners (125X125) - Public Area : 1
Static Banners (125X125) - Public Area : 1
Rotating Banners (125X125) - Member Area : 1
Static Banners (125X125) - Member Area : 1
Rotating Banners (468X60) - Public Area : 1
Static Banners (468X60) - Public Area : 0
Rotating Banners (468X60) - Member Area : 1
Static Banners (468X60) - Member Area : 1
Rotating Banners (728X90) - Public Area : 0
Static Banners (728X90) - Public Area : 0
Rotating Banners (728X90) - Member Area : 1
Static Banners (728X90) - Member Area : 1


Design & CMS -> Themes -> Advertisement Settings -> Plan Text Ads Display Settings
------------------
Rotating Text Ads - Public Area : 1
Static Text Ads - Public Area : 1
Rotating Text Ads - Member Area : 1
Static Text Ads - Member Area : 1


Design & CMS -> Themes -> Advertisement Settings -> Plan PPC Display Settings
------------------
Banners (125X125) - Public Area : 0
Banners (125X125) - Member Area : 0
Banners (468X60) - Public Area : 0
Banners (468X60) - Member Area : 0
Banners (728X90) - Public Area : 0
Banners (728X90) - Member Area : 0

If you wish to display PPC Banners, copy banner code from : Advertisement -> Pay Per Click - PPC -> Banner Widget -> Select Banner size that you wish to display in theme. Paste this copied code in template's layout file. And don't forget change above settings after adding PPC banners in theme.


The codes as per the above settings are already put accordingly in this template's layout file.

You need to make changes in the layout file accordingly if you plan to change the above settings.

2) Statistics Settings
======================

Public Statistics
-----------------
This templates shows selected statistics like Launch Date, Total Members, Online Members, Total Payouts, Last Payout.

So, on Settings-> Statistics Settings -> Public Statistics -> Enable(Check the checkbox) only Launch Date Code, Total Members Code, Online Members Code, Payouts Code, Last Payout Code.

Member Statistics
-----------------
Disable/Uncheck all statistics because no stats are displayed in members area in this Template.



Above recommendations are for those who want to use the template as it is without making major changes in the layout.

If you wish to change the places of elements as per your customised design, you can make changes, keeping the rules in your mind, as per your needs.


3) Theme Menu Settings
=======================

Members area
-------------
Design & CMS -> Themes -> Member Side ->

Statistics : Set inactive status for Statistics. 
Latest News : Set inactive status for Latest News.


Public Side
-----------
Design & CMS -> Themes -> Public Side ->

Latest News : Set inactive status for Latest News.