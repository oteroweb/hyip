<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 12-12-2014
  *********************************************************************/?>
<?php // To protect site from XSS and vulnerable uploads
header('X-XSS-Protection: 1; mode=block'); 
header('X-Content-Type-Options: nosniff');?>
<?php // This file is the main template file of the theme. Theme layout design related changes can be done from this file. ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo __(@$title_for_layout); ?> - <?php echo __($SITECONFIG['sitetitle']);?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="<?php echo __(@$keywords_for_layout); ?>" />
	<meta name="description" content="<?php echo __(@$description_for_layout); ?>" />
	
	<?php // Code to embed fonts ?>
	<style type="text/css" media="all">
		@font-face{
			font-family:'Open Sans';
			font-style:normal;
			font-weight:400;
			src:local('Open Sans'), local('OpenSans'), url("<?php echo $SITEURL.'theme/'.$this->theme.'/fonts/OpenSans.woff';?>") format('woff');
		}
		@font-face{
			font-family:'Open Sans';
			font-style:normal;
			font-weight:700;
			src:local('Open Sans Bold'), local('OpenSans-Bold'), url("<?php echo $SITEURL.'theme/'.$this->theme.'/fonts/OpenSans-Bold.woff';?>") format('woff');
		}
	</style>
	
	<?php // Including the css and js files starts here ?>
	<?php
	echo $this->Html->meta('icon');
	echo $this->Html->css('common');
	echo $this->Html->css('style');
	echo $this->fetch('meta');
	?>
	<script type="text/javascript">var themepath='<?php echo $SITEURL;?>theme/<?php echo $this->theme;?>/';</script>
	<?php echo $this->Html->script('jquery-1.8.3.min');?>
	<?php echo $this->Html->script('common');?>
	
	<?php echo $this->Html->script('jquery-ui.min'); // for calendar ?>
	<?php echo $this->Html->css('jquery-ui.min'); // for calendar ?>
	<?php if($isLogin)echo $this->Html->script('jquery.form');?>
	<?php echo $this->Html->css('keyboard');
	echo $this->Html->script('keyboard.min'); // for virtual keyboard ?>
	
	<?php // Including the css and js files ends here ?>
	
	<?php // Do not edit below code. Message ads code starts here ?>
	<script type="text/javascript">
	$(document).ready(function(){
		var popwid="60%";
		if($(window).width()<700)
			var popwid="100%";
			
		<?php if(@$_COOKIE['popup1']<date("Ydm") && $MessageAdsPublic!=''){?>
			$("#colorboxpopup1").colorbox({width:popwid, href:"#colorboxpopup1", inline:true, open:true, onClosed:function(){setCookie('popup1',<?php echo date("Ydm");?>,1);}, overlayClose:false, escKey:false, closeButton:false, fixed:true});
			$("#cboxContent").addClass("MeaasageAdcontentclass").find("#cboxClose").css( "visibility", "hidden" );
			MessageAdsTimer(5, '#MeaasageAdtimer');
		<?php }?>
		<?php if(@$_COOKIE['popup2']<date("Ydm") && $MessageAdsActive!=''){?>
			$("#colorboxpopup2").colorbox({width:popwid, href:"#colorboxpopup2", inline:true, open:true, onClosed:function(){setCookie('popup2',<?php echo date("Ydm");?>,1);}, overlayClose:false, escKey:false, closeButton:false, fixed:true});
			$("#cboxContent").addClass("MeaasageAdcontentclass").find("#cboxClose").css( "visibility", "hidden" );
			MessageAdsTimer(5, '#MeaasageAdtimer');
		<?php }?>
		<?php if(@$_COOKIE['popup3']<date("Ydm") && $MessageAdsPaid!=''){?>
			$("#colorboxpopup3").colorbox({width:popwid, href:"#colorboxpopup3", inline:true, open:true, onClosed:function(){setCookie('popup3',<?php echo date("Ydm");?>,1);}, overlayClose:false, escKey:false, closeButton:false, fixed:true});
			$("#cboxContent").addClass("MeaasageAdcontentclass").find("#cboxClose").css( "visibility", "hidden" );
			MessageAdsTimer(5, '#MeaasageAdtimer');
		<?php }?>
		<?php if(@$_COOKIE['popup4']<date("Ydm") && $MessageAdsUnpaid!=''){?>
			$("#colorboxpopup4").colorbox({width:popwid, href:"#colorboxpopup4", inline:true, open:true, onClosed:function(){setCookie('popup4',<?php echo date("Ydm");?>,1);}, overlayClose:false, escKey:false, closeButton:false, fixed:true});
			$("#cboxContent").addClass("MeaasageAdcontentclass").find("#cboxClose").css( "visibility", "hidden" );
			MessageAdsTimer(5, '#MeaasageAdtimer');
		<?php }?>
		<?php if(@$_COOKIE['popup5']<date("Ydm") && $MessageAdsMembership!=''){?>
			$("#colorboxpopup5").colorbox({width:popwid, href:"#colorboxpopup5", inline:true, open:true, onClosed:function(){setCookie('popup5',<?php echo date("Ydm");?>,1);}, overlayClose:false, escKey:false, closeButton:false, fixed:true});
			$("#cboxContent").addClass("MeaasageAdcontentclass").find("#cboxClose").css( "visibility", "hidden" );
			MessageAdsTimer(5, '#MeaasageAdtimer');
		<?php }?>
	});
	</script>
	<?php // Do not edit above code. Message ads code ends here ?>
	
	<?php echo $planbannerdata; echo $plantextdata; // Code to display plan banner ads and plan text ads on their respective positions ?>
</head>
<body>
	<div id="pleasewait"><?php echo $this->Html->image('wait.gif', array('alt' => 'Please Wait'))?></div><?php // Code for loading gif image ?>

<?php // Do not edit below code. Code for plugin starts here
if($isLogin){echo $LoginadPopup;}
// Do not edit above code. Code for plugin ends here ?>

<?php if(!$isLogin){ // If member is not logged in(Public side) ?>

	<div id="topbg">
        <div class="topmenu">
          <div class="topmain">
			<?php // Code for sponsor detail starts here ?>
			<?php if((is_int($REFFERAL) || $REFFERAL > 0 || is_string($REFFERAL)) && $REFFERAL != ""){?>
			<span id="sponsordetail"><?php echo $this->Html->image('wait.gif', array('alt' => 'Loding..','style' => 'width:20px;height:20px'))?></span>
            <script type="text/javascript">showsponsordetail('<?php echo $REFFERAL; ?>', '#sponsordetail', '<?php echo $SITEURL;?>');</script>
			<?php }?>
			<?php // Code for sponsor detail ends here ?>
			
			<div class="languagetop">
				<div class="select-main">
				  <label>
					<?php // Code for language change box starts here ?>
					<?php if($LanguageBox != ''){ echo $LanguageBox; } ?>
					<?php // Code for language change box ends here ?>
				  </label>
				</div>
			</div>
	<div class="joinnow"><?php echo $this->html->link(__("Join Now"), array('controller' => 'register', 'action' => 'index', 'plugin' => false));?></div>
	<div class="login"><?php echo $this->html->link(__("Login"), array('controller' => 'login', 'action' => 'index', 'plugin' => false));?></div>
			</div>  
        </div>
    </div>
	
	<div id="logobg">
	  <div class="fixtopmenu">
		<div class="topmenu">
		  <div class="logo"><a href="<?php echo $SITEURL;?>" ><?php echo $this->Html->image('logo.png', array('alt' => $SITECONFIG['sitetitle'], 'width'=> 190, 'height'=> 50))?></a></div>
		  <div class="navbar-header navbar-default">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			  <span class="sr-only">Toggle navigation</span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			</button>
		  </div>
		  <div class="logoright">
			<div id="menu">
			  <!-- Menu Code Start -->
			  <div class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
				  <div class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="<?php echo $SITEURL;?>"><?php echo __("Home");?></a></li>
						<li><?php echo $this->html->link(__("Details"), array('controller' => 'public', 'action' => 'details', 'plugin' => false));?></li>
						<li><?php echo $this->html->link(__("News"), array('controller' => 'public', 'action' => 'news', 'plugin' => false));?></li>
						<li><?php echo $this->html->link(__("F.A.Q."), array('controller' => 'public', 'action' => 'faqs', 'plugin' => false));?></li>
						<li><?php echo $this->html->link(__("Testimonials"), array('controller' => 'public', 'action' => 'testimonials', 'plugin' => false));?></li>
						<?php // Show only if admin has enabled from adminpanel ?>
						<?php if($SITECONFIG["recentpay"]==1){ ?><li><?php echo $this->html->link(__("Recent Payouts"), array('controller' => 'public', 'action' => 'recentpayouts', 'plugin' => false));?></li><?php }?>
						<li><?php echo $this->html->link(__("Support"), array('controller' => 'public', 'action' => 'support', 'plugin' => false));?></li>
						<?php if($PublicMenu["Business Directory"]==1){?>
						<li><?php echo $this->html->link(__("Business Directory"), array('controller' => 'directory', 'action' => 'viewall', 'plugin' => false));?></li>
						<?php } ?>
						<?php // Code to show public virtual pages ?>
						<?php if($PublicMenu["Public Pages"]==1 && (count($VirtualPageList)>0)){?>
						<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo __('Public Pages'); ?> <b class="caret"></b></a>
						  <ul class="dropdown-menu">
							<?php if(count($VirtualPageList)>0) {foreach($VirtualPageList as $pageid=>$pagename){?>
							<li><a href="<?php echo $SITEURL."public/page/".$pageid;?>"><?php echo $pagename;?></a></li>
							<?php }}?>
						  </ul>
						</li>
						<?php } ?>
						<li><?php echo $this->html->link(__("Member Representative"), array('controller' => 'member', 'action' => 'representative', 'plugin' => false));?></li>
					</ul>
				  </div>
				</div>
			  </div>
			  <!-- Menu Code Over -->
			</div>
		  </div>
		<div class="clearboth"></div>
		</div>
	  </div>
	  
    </div>
	
	<div class="heightafterfixedmenu"></div>
	
	<div id="headerbg">
        <div id="header">
            <div class="headerleft">
                <div class="headertext"><span class="headertitle">Add Your</span><br/> Tagline here</div>
            </div>
        </div>
     </div>
    
    
	<?php // Code to show login box starts here. Not to be shown on login page. ?>
	<?php if(!$isLogin && isset($PublicMenu["Member Login"]) && $PublicMenu["Member Login"]==1 && $this->params['controller']!='login'){ ?>
	<div class="memberloginbg">
      <div class="pagewidth">
		<div class="textcenter"><div class="memberlogintitle"><?php echo __('Member Login');?></div></div>
		  <div class="loginbox">
			<?php if($this->Session->read('locked')=='yes'){ // If Anti Brute Force lock is placed on member due to incorrect answers ?>
				<div><?php echo __('Access to the site has been blocked for you. Please try after following hours').' : '.$SITECONFIG["antibrutehours"]; ?></div>
			
			<?php }elseif($this->Session->read('plocked')=='yes'){ // If Anti Brute Force permanent lock is placed on member due to incorrect answers within specified time ?>
				<div><?php echo __('You are blocked. Please confirm your account first.'); ?></div>
			<?php }else{?>
				<div id="LoginMessage" class="error"></div>
				<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off', 'url'=>array('controller'=>'login','action'=>'index')));?>
					<?php if($this->Session->check('verified_ip')){ // If member has just verified new IP address ?>
						<div class="textcenter"><?php echo __('Verified').' : '.$this->Session->read('verified_ip'); ?></div>
					<?php } ?>
					<div class="textbox">
						<?php echo $this->Form->input('user_name', array('type'=>'text', 'label' => false, 'div' => false, 'class'=>'', 'placeholder'=>__('Username')));?>
					</div>
					<div class="textbox">
						<div class="passwordkeybord"><?php echo $this->Form->input('password', array('type'=>'password', 'label' => false, 'div' => false, 'class'=>'keyboardInput', 'placeholder'=>'**********'));?></div>
					</div>
					
					<?php if($MemberLoginCaptcha){ // If captcha is enabled by admin ?>
						<div class="captcha_main">
						  <div class="captcha_image"><?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'MenuLoginCaptcha', "width"=>"115", "height"=>"38")); ?></div>
						  <div class="captcha_input"><?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"", 'label'=>'', 'div'=>false,  'placeholder'=>__('Enter Captcha')));?></div>
						  <div class="refresh_captcha"><a href="javascript:void(0);" onclick="javascript:document.images.MenuLoginCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "width"=>"13", "height"=>"15"));?></a></div>
						</div>
					<?php } ?>
					
					<div class="loginbutton">
						<?php echo $this->Js->submit(__('Login'), array(
							'before'=>$this->Js->get('#pleasewait, #LoginMessage')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'update'=>'#LoginMessage',
							'class'=>'',
							'controller'=>'login',
							'action'=>'index',
							'url'   => array('controller' => 'login', 'action' => 'index')
						));?>
					</div>
					
					<?php if($SITECONFIG["chkfb"]==1 || $SITECONFIG["chkgoogle"]==1){ ?>
					<div class="signinwithmain">
					  <?php echo __('Sign in With');?>
					  <div class="signinimg">
						<?php if($SITECONFIG["chkfb"]==1){ // If admin has enabled facebook login ?>
						<?php echo $this->Js->link($this->html->image('facebook.jpg', array('alt'=>'', 'align' =>'absmiddle')), array('controller'=>'login', "action"=>"with/facebook"), array(
								'update'=>'#LoginMessage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false
							));?>
						<?php }?>
					  </div>
					  <div class="signinimg">
						<?php if($SITECONFIG["chkgoogle"]==1){  // If admin has enabled google login ?>
						&nbsp;<?php echo $this->Js->link($this->html->image('google.jpg', array('alt'=>'', 'align' =>'absmiddle')), array('controller'=>'login', "action"=>"with/google"), array(
								'update'=>'#LoginMessage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false
							));?>
						<?php }?>
					  </div>
					</div>
					<?php } ?>
					
					<div class="regtext">
						<?php echo $this->html->link(__("Register"), array('controller' => 'register', 'action' => 'index'));?>&nbsp;|&nbsp;<?php echo $this->html->link(__("Forgot Password"), array('controller' => 'forgotpassword', 'action' => 'index'));?>
						<?php if($SITECONFIG["emailconfirmation"]==1){ // To be shown only if email confirmation is required for members ?>
							&nbsp;|&nbsp;<?php echo $this->html->link(__("Resend Activation Link"), array('controller' => 'public', 'action' => 'resendactivation'));?>
						<?php }?>
					</div>
				<?php echo $this->Form->end();?>
			<?php } ?>
		  </div>
      </div>
    </div>
	<?php }?>
	<?php // Code to show login box ends here. ?>

<?php }else{ // If member is logged in(Member side) ?>

	<div id="memberhader">
		<div class="memheaderleft"><a href="<?php echo $SITEURL;?>member" ><?php echo $this->Html->image('logo.png', array('alt' => $SITECONFIG['sitetitle'], 'width'=> 190, 'height'=> 50))?></a></div>
		
		<div class="navbar-header navbar-default">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			  <span class="sr-only">Toggle navigation</span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			</button>
		</div>
		
		<?php // Code for top navigation bar starts here ?>
		<div class="memheaderright">
        <div class="height20 notmobile"></div>
        <div class="membertopmenu notmobile">
            <ul>
				<li><?php echo $this->html->link(__("News"), array('controller' => 'public', 'action' => 'news', 'plugin' => false));?></li>
				<li><?php echo $this->html->link(__("F.A.Q."), array('controller' => 'public', 'action' => 'faqs', 'plugin' => false));?></li>
                <li><?php echo $this->html->link(__("Support"), array('controller' => 'member', 'action' => 'support', 'plugin' => false));?></li>
				<li><?php echo $this->html->link(__("Testimonials"), array('controller' => 'public', 'action' => 'testimonials', 'plugin' => false));?></li>
				<?php // Show only if admin has enabled from adminpanel ?>
				<?php if($SITECONFIG["recentpay"]==1){ ?>
			<li><?php echo $this->html->link(__("Recent Payouts"), array('controller' => 'public', 'action' => 'recentpayouts', 'plugin' => false));?></li>
				<?php }?>
            </ul>
        </div>
        <div class="languagetop">
			<div class="select-main">
				<label>
				  <?php // Code for language change box starts here ?>
				  <?php if($LanguageBox != ''){ echo $LanguageBox; } ?>
				  <?php // Code for language change box ends here ?>
				</label>
			</div>
        </div>
		<li class="joinnow"><?php echo $this->html->link(__("Overview"), array('controller' => 'member', 'action' => 'index', 'plugin' => false), array('class' => 'Overview'));?></li>
		<li class="login"><?php echo $this->html->link(__("Logout"), array('controller' => 'login', 'action' => 'logout', 'plugin' => false), array('class' => 'logout'));?></li>
      </div>
      <div class="clearboth"></div>
	  <?php // Code for top navigation bar ends here ?>
	  
	</div>
	
	
	<div class="mainmembermenu">
		<div class="membermenufix">
		  <div id="membermenu">
			  <div class="membernav">
				
				<div class="navbar navbar-default" role="navigation">
				  <div class="container-fluid">
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<?php $pageulr=str_replace($this->webroot, "",$this->here);?>
							<?php if($MemberMenu["Account Settings"]==1) { ?>
							<?php if($this->params['controller']=="member" && ($this->params['action']=="messageread" || $this->params['action']=="membership" || $this->params['action']=="profile" || $this->params['action']=="addtestimonial" || $this->params['action']=="memberactivity"))
							{
								$menuclass='class="dropdown-toggle mainmanu act"';
								$menucaret='<b class="caret"></b>';
								$menuddclass='open tkopen';
							}else {
								$menuclass='class="dropdown-toggle mainmanu"';
								$menucaret='';
								$menuddclass='';
							}?>
							<li class="dropdown <?php echo $menuddclass?> loginmenu1">
								<a href="javascript:void(0)" onclick="$('#membermenu .navbar-nav li.dropdown').removeClass('tkopen');" data-toggle="dropdown" <?php echo $menuclass?>><?php echo __('Account Activity');?> <?php echo $menucaret?></a>
								<ul class="dropdown-menu">
									<?php if(in_array('Profile', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Profile"), array('controller' => 'member', 'action' => 'profile', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Security', $themesubmenu)){?>	
										<li><?php echo $this->html->link(__("Security"), array('controller' => 'member', 'action' => 'profile/security', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Upgrade Account', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Upgrade Account"), array('controller' => 'member', 'action' => 'membership', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Rate Us', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Rate Us"), array('controller' => 'member', 'action' => 'addtestimonial', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Activity Logs', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Activity Logs"), array('controller' => 'member', 'action' => 'memberactivity', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Admin Messages', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Admin Messages"), array('controller' => 'member', 'action' => 'messageread', 'plugin' => false));?></li>
									<?php } ?>
								</ul>
							</li>
							<?php }?>
							
							<?php if($MemberMenu["Finance"]==1) { ?>
							<?php if(($this->params['controller']=="member" && ($this->params['action']=="withdrawal_history" || $this->params['action']=="paymenthistory" || $this->params['action']=="commission" || $this->params['action']=="withdrawal" || $this->params['action']=="addfund")) || ($this->params['controller']=="module1" && ($this->params['action']=="index" || $this->params['action']=="dynamic" || $this->params['action']=="dailyposition" || $this->params['action']=="dailyposition")) || ($this->params['controller']=="module2" && ($this->params['action']=="index" || $this->params['action']=="position" || $this->params['action']=="matrix")) || ($this->params['controller']=="balancetransfer" && ($this->params['action']=="index" || $this->params['action']=="pending" || $this->params['action']=="history" || $this->params['action']=="history2")))
							{
								$menuclass='class="dropdown-toggle mainmanu act"';
								$menucaret='<b class="caret"></b>';
								$menuddclass='open tkopen';
							}else {
								$menuclass='class="dropdown-toggle mainmanu"';
								$menucaret='';
								$menuddclass='';
							}?>
							<li class="dropdown <?php echo $menuddclass?> loginmenu4">
								<a href="javascript:void(0)" onclick="$('#membermenu .navbar-nav li.dropdown').removeClass('tkopen');" data-toggle="dropdown" <?php echo $menuclass?>><?php echo __('Finance');?> <?php echo $menucaret?></a>
								<ul class="dropdown-menu">
									<?php if(in_array('Add Fund', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Add Funds"), array('controller' => 'member', 'action' => 'addfund', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Purchase Position', $themesubmenu)){ 
										$modulename="";$moduleaction='';
                                        if($SITECONFIG["modules"] != ""){
										$modules=@explode(",",trim($SITECONFIG["modules"],","));
										foreach($modules as $module)
										{
											$modulearray=@explode(":", $module);
											if($modulearray[2]==1 && $modulearray[18]!="")
											{
												$subplanarray=explode("-",$modulearray[18]);
												$modulename=$modulearray[1];
												foreach($subplanarray as $subplan)
												{
													if($subplan !='')
													{
														$moduleaction=$subplan;
														break;
													}
												}
											}
											if($moduleaction != '')
												break;
										}
                                        }
                                        
										if($moduleaction!='')
										{
										?>
											<li><?php echo $this->html->link(__("Purchase Position"), array('controller' => $modulename, 'action' => $moduleaction, 'plugin' => false));?></li>
									<?php } }?>
										<?php if(in_array('Earning History', $themesubmenu)){?>
										<?php
										$modulename="";$moduleaction='';
                                        if($SITECONFIG["modules"] != ""){
										$modules=@explode(",",trim($SITECONFIG["modules"],","));
										foreach($modules as $module)
										{
											$modulearray=@explode(":", $module);
											if($modulearray[2]==1 && $modulearray[19]!="")
											{
												$subplanarray=explode("-",$modulearray[19]);
												$modulename=$modulearray[1];
												foreach($subplanarray as $subplan)
												{
													if($subplan !='')
													{
														$moduleaction=$subplan;
														break;
													}
												}
											}
											if($moduleaction != '')
												break;
										}
                                        }
										
										if($moduleaction=='')
										{
											$modulename='member';
											$moduleaction='commission';
										}
									?>
									
									<li><?php echo $this->html->link(__("Earning History"), array('controller' => $modulename, 'action' => $moduleaction, 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Payment History', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Payment History"), array('controller' => 'member', 'action' => 'paymenthistory', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Withdrawal Request', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Request Withdrawal"), array('controller' => 'member', 'action' => 'withdrawal', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Withdrawal History', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Withdrawal History"), array('controller' => 'member', 'action' => 'withdrawal_history', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Balance Transfer', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Balance Transfer"), array('controller' => 'balancetransfer', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
								</ul>
							</li>
							<?php }?>
							
							<?php if($MemberMenu["Advertisement"]==1) { ?>
							<?php if(($this->params['controller']=="advertisement" && $this->params['action']=="mycredits") || ($this->params['controller']=="bannerad" && ($this->params['action']=="index" || $this->params['action']=="plans")) || ($this->params['controller']=="textad" && ($this->params['action']=="index" || $this->params['action']=="plans")) || ($this->params['controller']=="soload" && ($this->params['action']=="index" || $this->params['action']=="saved" || $this->params['action']=="plans")) || ($this->params['controller']=="loginad" && ($this->params['action']=="index")) || ($this->params['controller']=="directory" && ($this->params['action']=="index"))  || ($this->params['controller']=="ppc" && ($this->params['action']=="index")) || ($this->params['controller']=="ptc" && ($this->params['action']=="index")) || ($this->params['controller']=="traffic" && ($this->params['action']=="index" || $this->params['action']=="startpagepurchase" || $this->params['action']=="surf" || $this->params['action']=="website")) || ($this->params['controller']=="ppctextad" && ($this->params['action']=="index")))
							{
								$menuclass='class="dropdown-toggle mainmanu act"';
								$menucaret='<b class="caret"></b>';
								$menuddclass='open tkopen';
							}else {
								$menuclass='class="dropdown-toggle mainmanu"';
								$menucaret='';
								$menuddclass='';
							}?>
							<li class="dropdown <?php echo $menuddclass?> loginmenu5">
								<a href="javascript:void(0)" onclick="$('#membermenu .navbar-nav li.dropdown').removeClass('tkopen');" data-toggle="dropdown" <?php echo $menuclass?>><?php echo __('Advertisement');?> <?php echo $menucaret?></a>
								<ul class="dropdown-menu">
									<?php if(in_array('Banner Ads', $themesubmenu)){?>
										<?php if($SITECONFIG["enable_bannerads"]==1){ ?>
											<li><?php echo $this->html->link(__("Banner Ads"), array('controller' => 'bannerad', 'action' => 'index', 'plugin' => false));?></li>
										<?php }elseif(strpos($SITECONFIG["banneradplansetting"],'isenable|1') !== false){ ?>
											<li><?php echo $this->html->link(__("Banner Ads"), array('controller' => 'bannerad', 'action' => 'plans', 'plugin' => false));?></li>
										<?php }?>
									<?php }?>
									<?php if(in_array('Text Ads', $themesubmenu)){?>	
										<?php if($SITECONFIG["enable_textads"]==1){ ?>
											<li><?php echo $this->html->link(__("Text Ads"), array('controller' => 'textad', 'action' => 'index', 'plugin' => false));?></li>
										<?php }elseif(strpos($SITECONFIG["textadplansetting"],'isenable|1') !== false){ ?>
											<li><?php echo $this->html->link(__("Text Ads"), array('controller' => 'textad', 'action' => 'plans', 'plugin' => false));?></li>
										<?php }?>
									<?php }?>
									<?php if(in_array('Solo Ads', $themesubmenu)){?>	
										<?php if($SITECONFIG["enable_soloads"]==1){ ?>
											<li><?php echo $this->html->link(__("Solo Ads"), array('controller' => 'soload', 'action' => 'index', 'plugin' => false));?></li>
										<?php }elseif(strpos($SITECONFIG["soloadplansetting"],'isenable|1') !== false){ ?>
											<li><?php echo $this->html->link(__("Solo Ads"), array('controller' => 'soload', 'action' => 'plans', 'plugin' => false));?></li>
										<?php }?>
									<?php }?>
									<?php if(in_array('Login Ad Plans', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Login Ad Plans"), array('controller' => 'loginad', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('PPC', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("PPC"), array('controller' => 'ppc', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('PTC', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("PTC"), array('controller' => 'ptc', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('BusinessDirectory', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Business Directory"), array('controller' => 'directory', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
                                                                                
                                    <?php if(strpos($SITECONFIG["surfingsetting"],'enablesurfing|1') !== false){
                                            if(strpos($SITECONFIG["surfingsetting"],'surfselect|0') !== false && strpos($SITECONFIG["trafficsetting"],'enablesurfing|1') !== false){
                                    ?>     
                                                                        
                                    <?php if(in_array('Website Credit Plans', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Buy Credits"), array('controller' => 'traffic', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Start Page', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Buy Start Page"), array('controller' => 'traffic', 'action' => 'startpagepurchase', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Start Surfing', $themesubmenu)){?>
										<li>
										<a href="<?php echo str_replace("https://", "http://", $SITEURL);?>traffic/surf" target="_blank"><?php echo __('Start Surfing'); ?></a>
										<?php //echo $this->html->link(__("Start Surfing"), array('controller' => 'traffic', 'action' => 'surf', 'plugin' => false));?></li>
									<?php } ?>
									
									<?php if(in_array('My Websites', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("My Websites"), array('controller' => 'traffic', 'action' => 'website', 'plugin' => false));?></li>
									<?php } ?>
                                                                                
                                    <?php } // Check Surf Select is Traffic Exchange or not and it must be on ?>

                                    <?php } //Check Main Surfing ON or OFF  ?>        
                                                                                
									<?php if(in_array('PPC Text Ads', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Buy PPC Text Ads"), array('controller' => 'ppctextad', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Credit Statement', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Credit Statement"), array('controller' => 'advertisement', 'action' => 'mycredits', 'plugin' => false));?></li>
									<?php }?>
								</ul>
							</li>
							<?php }?>
							
							<?php if($MemberMenu["Member Tools"]==1) { ?>
							<?php if(($this->params['controller']=="ptc" && $this->params['action']=="view")||($this->params['controller']=="urlrotator" && $this->params['action']=="index")||($this->params['controller']=="urlshortener" && $this->params['action']=="index"))
							{
								$menuclass='class="dropdown-toggle mainmanu act"';
								$menucaret='<b class="caret"></b>';
								$menuddclass='open tkopen';
							}else {
								$menuclass='class="dropdown-toggle mainmanu"';
								$menucaret='';
								$menuddclass='';
							}?>
							<li class="dropdown <?php echo $menuddclass?> loginmenu2">
								<a href="javascript:void(0)" onclick="$('#membermenu .navbar-nav li.dropdown').removeClass('tkopen');" data-toggle="dropdown" <?php echo $menuclass?>><?php echo __('Member Tools');?> <?php echo $menucaret?></a>
								<ul class="dropdown-menu">
									<?php if(in_array('View PTC', $themesubmenu)){?>
									<li><a href="<?php echo str_replace("https://", "http://", $SITEURL);?>ptc/view"><?php echo __('View PTC Ads'); ?></a>
									<?php //echo $this->html->link(__("View PTC Ads"), array('controller' => 'ptc', 'action' => 'view', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('URL Rotator', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("URL Rotator"), array('controller' => 'urlrotator', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('URL Shortener', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("URL Shortener"), array('controller' => 'urlshortener', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Contest', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Referral Contests"), array('controller' => 'contest', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Jackpot', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Jackpot"), array('controller' => 'jackpot', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
								</ul>
							</li>						
							<?php } ?>
							
							<?php if($MemberMenu["Promotional Tools"]==1) { ?>
							<?php if(($this->params['controller']=="statbanner" && $this->params['action']=="index")||($this->params['controller']=="promotools" && ($this->params['action']=="banners" || $this->params['action']=="splash" || $this->params['action']=="landing" || $this->params['action']=="textlink" || $this->params['action']=="email" || $this->params['action']=="tellfriends"))||($this->params['controller']=="member" && $this->params['action']=="referrals"))
							{
								$menuclass='class="dropdown-toggle mainmanu act"';
								$menucaret='<b class="caret"></b>';
								$menuddclass='open tkopen';
							}else {
								$menuclass='class="dropdown-toggle mainmanu"';
								$menucaret='';
								$menuddclass='';
							}?>
							<li class="dropdown <?php echo $menuddclass?> loginmenu3">
								<a href="javascript:void(0)" onclick="$('#membermenu .navbar-nav li.dropdown').removeClass('tkopen');" data-toggle="dropdown" <?php echo $menuclass?>><?php echo __('Promo Tools');?> <?php echo $menucaret?></a>
								<ul class="dropdown-menu">
									<?php if(in_array('Banner', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Promotional Banners"), array('controller' => 'promotools', 'action' => 'banners', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Dynamic Banners', $themesubmenu)){?>
									<li><?php echo $this->html->link(__("Dynamic Banners"), array('controller' => 'statbanner', 'action' => 'index', 'plugin' => false));?></li>
									<?php } ?>
									<?php if(in_array('Splash Pages', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Splash Pages"), array('controller' => 'promotools', 'action' => 'splash', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Landing Pages', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Landing Pages"), array('controller' => 'promotools', 'action' => 'landing', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Text Links', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Text Links"), array('controller' => 'promotools', 'action' => 'textlink', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Promotional Emails', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Promotional Emails"), array('controller' => 'promotools', 'action' => 'email', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('Tell Friends', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("Tell Friends"), array('controller' => 'promotools', 'action' => 'tellfriends', 'plugin' => false));?></li>
									<?php }?>
									<?php if(in_array('My Referrals', $themesubmenu)){?>
										<li><?php echo $this->html->link(__("My Referrals"), array('controller' => 'member', 'action' => 'referrals', 'plugin' => false));?></li>
									<?php }?>
								</ul>
							</li>
							<?php }?>
							
							<?php if($MemberMenu["Member Pages"]==1 && count($VirtualPageList)>0) { ?>
							<?php if($this->params['controller']=="member" && $this->params['action']=="page")
							{
								$menuclass='class="dropdown-toggle mainmanu act"';
								$menucaret='<b class="caret"></b>';
								$menuddclass='open tkopen';
							}else {
								$menuclass='class="dropdown-toggle mainmanu"';
								$menucaret='';
								$menuddclass='';
							}?>
							<li class="dropdown <?php echo $menuddclass?> loginmenu6" style="border-right: solid 1px #4885c8;">
								<a href="javascript:void(0)" onclick="$('#membermenu .navbar-nav li.dropdown').removeClass('tkopen');" data-toggle="dropdown" <?php echo $menuclass?>><?php echo __('Member Pages');?> <?php echo $menucaret?></a>
								<ul class="dropdown-menu">
									<?php if(count($VirtualPageList)>0){ foreach($VirtualPageList as $pageid=>$pagename){?>
									<li><a href="<?php echo $SITEURL."member/page/".$pageid;?>"><?php echo $pagename;?></a></li>
									<?php }}?>
								</ul>
							</li>
							<?php } ?>
							<li class="showonmobile"><?php echo $this->html->link(__("News"), array('controller' => 'public', 'action' => 'news', 'plugin' => false));?></li>
							<li class="showonmobile"><?php echo $this->html->link(__("F.A.Q."), array('controller' => 'public', 'action' => 'faqs', 'plugin' => false));?></li>
							<li class="showonmobile"><?php echo $this->html->link(__("Support"), array('controller' => 'member', 'action' => 'support', 'plugin' => false));?></li>
							<li class="showonmobile"><?php echo $this->html->link(__("Testimonials"), array('controller' => 'public', 'action' => 'testimonials', 'plugin' => false));?></li>
						</ul>
					</div>
				  </div>
				</div>
				
			  </div>
		  </div>
		  <div id="membersubmenubg"></div>
		</div>
		
	</div>
	<div class="membermenufixheight"></div>
<?php }?>


    
			<?php // Statistics code starts here ?>
			<?php if(((!$isLogin && isset($PublicMenu["Statistics"]) && $PublicMenu["Statistics"]==1) || ($isLogin && isset($MemberMenu["Statistics"]) && $MemberMenu["Statistics"]==1)) && (($isLogin && substr($SITECONFIG["memberstatistics"],0,1)==1) || (!$isLogin && substr($SITECONFIG["publicstatistics"],0,1)==1))) { ?>
			<div class="statbg" style="<?php if(@$_COOKIE['stat']==1)echo 'display:none;';?>">
				<div class="<?php if($isLogin){echo 'statistic-membersarea';}else{echo 'pagewidth';}?>">
			
					<div class="statisticboxmain">
						<div class="statisticbox" style="<?php if(@$_COOKIE['stat']==1)echo 'display:none;';?>">
							<div class="sta-menu">			
								<ul>
									<li class="sta-left"><?php echo __('Statistics'); ?></li>
									<?php if(($isLogin && (strpos($SITECONFIG["memberstatistics"],'AlexaRank|1') !== false || strpos($SITECONFIG["memberstatistics"],'LaunchDate|1') !== false || strpos($SITECONFIG["memberstatistics"],'TotalMember|1') !== false)) || (!$isLogin && (strpos($SITECONFIG["publicstatistics"],'LaunchDate|1') !== false || strpos($SITECONFIG["publicstatistics"],'AlexaRank|1') !== false || strpos($SITECONFIG["publicstatistics"],'TotalMember|1') !== false))){ ?>
									<li>
										<?php
											echo $this->Js->link(__('Site Stats'), array('controller'=>'statistic', "action"=>"index", 'plugin' => false), array(
												'update'=>'#statecontant',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false
											));
										?>
									</li>
									<?php } if($isLogin && (strpos($SITECONFIG["memberstatistics"],'MyCash|1') !== false || strpos($SITECONFIG["memberstatistics"],'RepurchaseBalance|1') !== false || strpos($SITECONFIG["memberstatistics"],'EarningBalance|1') !== false || strpos($SITECONFIG["memberstatistics"],'CommissionBalance|1') !== false || strpos($SITECONFIG["memberstatistics"],'BannerAdCredits|1') !== false || strpos($SITECONFIG["memberstatistics"],'TextAdCredits|1') !== false || strpos($SITECONFIG["memberstatistics"],'SoloAdCredits|1') !== false)){ ?>
									<li> 
										<?php
											echo $this->Js->link(__('My Statistics'), array('controller'=>'statistic', "action"=>"mystats", 'plugin' => false), array(
												'update'=>'#statecontant',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false
											));
										?>
									</li>
									<?php } if(($isLogin && strpos($SITECONFIG["memberstatistics"],'Deposits|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'Deposits|1') !== false) || ($isLogin && strpos($SITECONFIG["memberstatistics"],'Payouts|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'Payouts|1') !== false) || ($isLogin && strpos($SITECONFIG["memberstatistics"],'Commissions|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'Commissions|1') !== false) || ($isLogin && strpos($SITECONFIG["memberstatistics"],'LastPayout|1') !== false) || (!$isLogin && strpos($SITECONFIG["publicstatistics"],'LastPayout|1') !== false)){ ?>
									<li>
										<?php
											echo $this->Js->link(__('Pay Stats'), array('controller'=>'statistic', "action"=>"paystats", 'plugin' => false), array(
												'update'=>'#statecontant',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false
											));
										?>
									</li>
									<?php } if(($isLogin && (strpos($SITECONFIG["memberstatistics"],'OnlineMembers|1') !== false || strpos($SITECONFIG["memberstatistics"],'OnlineGuests|1') !== false || strpos($SITECONFIG["memberstatistics"],'RecentlyJoined|1') !== false || strpos($SITECONFIG["memberstatistics"],'TopCommissionEarners|1') !== false)) || (!$isLogin && (strpos($SITECONFIG["publicstatistics"],'OnlineMembers|1') !== false || strpos($SITECONFIG["publicstatistics"],'OnlineGuests|1') !== false || strpos($SITECONFIG["publicstatistics"],'RecentlyJoined|1') !== false || strpos($SITECONFIG["publicstatistics"],'TopCommissionEarners|1') !== false))){ ?>
									<li>
										<?php
											echo $this->Js->link(__('Member Stats'), array('controller'=>'statistic', "action"=>"memberstats", 'plugin' => false), array(
												'update'=>'#statecontant',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false
											));
										?>
									</li>
									<?php }  /*
											  Add Stats Code Remove  
											 if(($isLogin && (strpos($SITECONFIG["memberstatistics"],'TextAds|1') !== false || strpos($SITECONFIG["memberstatistics"],'BannerAds|1') !== false)) || (!$isLogin && (strpos($SITECONFIG["publicstatistics"],'TextAds|1') !== false || strpos($SITECONFIG["publicstatistics"],'BannerAds|1') !== false))){ ?>
									<li>
										<?php 
											echo $this->Js->link(__('Ads Stats'), array('controller'=>'statistic', "action"=>"adsstats", 'plugin' => false), array(
												'update'=>'#statecontant',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false
											));
										?>
									</li>
									<?php } */?>
									<li style=" border-right:none;">&nbsp;</li>
								</ul>
							</div>
							<div class="clear-both"></div>
							<div id="statecontant"></div>
						</div>
					</div>
				</div>
			</div>
			<?php if(@$_COOKIE['stat']==1){$shstattxt= __('Show Statistics');$shstattitle= __('[+]');}else{ $shstattxt= __('Hide Statistics');$shstattitle= __('[-]');}?>
			<div class="height10"></div>
			<div class="textright statisticshbutton <?php if(!$isLogin){echo 'pagewidth';} ?>"><a class="shbutton" data-tooltip="<?php echo $shstattxt;?>" href="javascript:void(0)" onclick="statisticbox('.statisticbox, .statbg', this,'<?php echo __('[+]'); ?>','<?php echo __('[-]');?>', '<?php echo __('Show Statistics')?>', '<?php echo __('Hide Statistics')?>');"><?php echo $shstattitle;?></a></div>
			<?php } ?>
			<?php // Statistics code ends here ?>
		
	
			
			
			
	<?php if($isLogin){?>
		
		<div id="memberallcontent">
		
		<?php if((!$isLogin && isset($PublicMenu["Banner Ads"]) && $PublicMenu["Banner Ads"]==1) || ($isLogin && isset($MemberMenu["Banner Ads"]) && $MemberMenu["Banner Ads"]==1))
		{ ?>
			<?php // Code to show 728x90 banner from banner ad plans ?>
				<div class="topbannersh textcenter" style="<?php if(@$_COOKIE['bannertop']==1)echo 'display:none;';?>"></div>
				
				<div class="topbannersh" style="<?php if(@$_COOKIE['bannertop']==1)echo 'display:none;';?>">
					<div class="pagewidth">
					  <div class="bannermain topbannermain">
						<?php // Code to show 125x125 advertisement banner ?>
						<div class="banner468main">
							<?php // Code to show 468x60 banner from banner ad plans ?>
							<div class="banner468"><span id="static_468_1"></span></div>
							<?php // Code to show 468x60 advertisement banner ?>
							<div class="banner468"><span id="advertisement_468_1"></span></div>
						</div>
						<?php // Code to show 125x125 banner from banner ad plans ?>
					  </div>
					</div>
				</div>
				<?php if(@$_COOKIE['bannertop']==1){$shtbtxt= __('Show Banners');$shbtitle= __('[+]');}else{ $shtbtxt= __('Hide Banners');$shbtitle= __('[-]');}?>
				<div class="textright"><a class="topbannerbtn shbutton" data-tooltip="<?php echo $shtbtxt;?>" href="javascript:void(0)" onclick="topbannerbox('.topbannersh', this,'<?php echo __('[+]'); ?>','<?php echo __('[-]');?>', '<?php echo __('Show Banners')?>', '<?php echo __('Hide Banners')?>');");"><?php echo $shbtitle;?></a></div>
				<div class="height10"></div>
			<?php // Code to show banners before page content ends here ?>
		<?php
		}?>
			
	<?php }
	else{?><div id="allcontentbg"><div id="allcontent"><?php }?>
		<div id="right-bar">
			<div id="pagecontent">
				
				<?php // Code to show launch date widget starts here ?>
				<?php if($LaunchDateWidget!="")
				{
					echo $this->Javascript->link('launchdate');
					echo $LaunchDateWidget;
				} ?>
				<?php // Code to show launch date widget ends here ?>
				
				<?php // Code to show admin message(s) starts here ?>
				<?php $mmdata=$this->Session->read('membermessage_data');
				if(count($mmdata)){?>
				<div id="membermessagemain">
					<div class="j-success-box">
						<div class="success-icon"></div>
						<div class="j-success-text"><a href="javascript:void(0)" onclick="if($('.membermessage_box').css('display')=='none'){$('.membermessage_box').show(800);}else{$('.membermessage_box').hide(500);}"><?php echo count($mmdata).' '.__('Unread Message(s) From Admin');?></a></div>
						<div class="clear-both"></div>
					</div>
					<?php for($mm=0;$mm<count($mmdata);$mm++){?>
					<div class="membermessage_box" style="display:none;">
						<div class="box">
							<div id="updatemsg<?php echo $mm;?>">
							<div class="date">
								<div class="divtable textcenter">
									<div class="divtr">
										<div class="divtd textleft vat">
											<?php echo stripslashes($mmdata[$mm]['Membermessage_history']['description']); ?> | <?php echo $this->Time->format($SITECONFIG["timeformate"], $mmdata[$mm]['Membermessage_history']['messagedate']);?>
										</div>
										<div class="divtd textright vat">
											<?php echo $this->Form->create('Membermessage_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'messageread'.$mmdata[$mm]['Membermessage_history']['messagetype'])));
											echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$mmdata[$mm]['Membermessage_history']["id"], 'label' => false));
											echo $this->Js->submit(__('Mark as read'), array(
											  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
											  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											  'update'=>'#updatemsg'.$mm,
											  'class'=>'large white button',
											  'controller'=>'member',
											  'div'=>false,
											  'action'=>'messageread'.$mmdata[$mm]['Membermessage_history']['messagetype'],
											  'url' => array('controller' => 'member', 'action' => 'messageread'.$mmdata[$mm]['Membermessage_history']['messagetype'], 'plugin' => false)
											));
											echo $this->Form->end();?>
										</div>
									</div>
								</div>
							</div>
							<div class="comment"><?php echo nl2br(stripslashes($mmdata[$mm]['Membermessage_history']['message'.$this->Session->read('Config.language')]));?></div>
							<?php if(count($mmdata)== $mm+1){ echo "<div class='admin_messag_url';>".$this->html->link(__("...Read More"), array('controller' => 'member', 'action' => 'messageread', 'plugin' => false)); echo "</div><div class='clear-both'></div>"; } ?>	
						</div>
						</div>
					</div>
					<?php }?>
					<div class="height10"></div>
				</div>
				<?php }?>
				<?php // Code to show admin message(s) ends here ?>
				
				<?php // Code to show page wise content ?>
				<?php echo $this->fetch('content');?>
			</div>
        </div>
	<?php if($isLogin){?></div><?php }else{?></div></div><?php }?>
	
	<?php // Code to show text ads starts here. ?>
	<?php if((!$isLogin && isset($PublicMenu["Text Ads"]) && $PublicMenu["Text Ads"]==1) || ($isLogin && isset($MemberMenu["Text Ads"]) && $MemberMenu["Text Ads"]==1)) { ?>
		<?php if($isLogin){?><div id="membergrey"><?php }else{?><div id="textadbg"><div class="pagewidth"><?php }?>
	
          <div class="textadmain">
				<?php // Code to show advertisement text ads ?>
				<?php for($i=0; $i<count($TextAdsList); $i++){?>
					<?php if(!is_array($TextAdsList[$i])){ ?><div class="textadbox"><?php echo $TextAdsList[$i]; ?></div><?php }else{ ?>
						<div class="textadbox">
							<div class="text-add-title">
								<?php echo $this->Html->link($TextAdsList[$i]["title"], array('controller' => 'public', 'action' => 'counter/textad/'.$TextAdsList[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["title_color"].';')); ?>
							</div>
							<div class="textadcont">
								<?php 
								echo '<span style="color:'.$SITECONFIG["desc1_color"].'">'.$TextAdsList[$i]["desc1"].'</span>';
								echo '<br />';
								echo '<span style="color:'.$SITECONFIG["desc2_color"].'">'.$TextAdsList[$i]["desc2"].'</span>';
								if($TextAdsList[$i]["showurl"])
									echo '<br />'.$this->Html->link(__('Click here'), array('controller' => 'public', 'action' => 'counter/textad/'.$TextAdsList[$i]["textadd_id"]), array('target'=>'_blank', 'style' => 'color:'.$SITECONFIG["url_color"].';text-decoration:none;'));
								?>
							</div>
						</div>
					<?php } ?>
				<?php }?>
				<?php // Code to show text ads from text ad plans. ?>
				<div class="textadbox"><span id="static_textad_1"></span></div>
				<div class="textadbox"><span id="rotating_textad_1"></span></div>
          </div>
		  
		  <?php if($isLogin){?></div><?php }else{?></div></div><?php }?>
    <?php } ?>
	<?php // Code to show text ads ends here. ?>
	
	<?php // Code to show latest news starts here. ?>
	<?php if($this->params['controller']=='ref' && $this->params['action']=='index'){if((!$isLogin && isset($PublicMenu["Latest News"]) && $PublicMenu["Latest News"]==1) || ($isLogin && isset($MemberMenu["Latest News"]) && $MemberMenu["Latest News"]==1)){if(count($LatestNews)){ ?>
    <div class="newsbg">
		<?php if($isLogin){?><div class="statistic-membersarea"><?php }else{?><div class="pagewidth"><div class="paddingonmobile"><?php }?>
      
		  <div class="newsboxmain">
				<div class="newstitle"><?php echo __('Latest News'); ?></div>
				<?php foreach($LatestNews as $lnews){?>
					<div class="newstitleinner"><?php echo $lnews["title"];?></div>
					<div class="newsinner">
						<?php $fdescarray=@explode(' ', strip_tags($lnews["fdesc"]));
						echo @implode(' ', array_slice($fdescarray, 0, $lnews["sdesc"]));echo '&nbsp;';
						if(count($fdescarray)>$lnews["sdesc"])
						{
							echo $this->html->link(__("Read More"), array('controller'=>'public', 'action'=>'newsdetail/'.$lnews["news_id"]));
						}?>
					</div>
				<?php }?>
		  </div>
		  
		  <?php if($isLogin){?></div><?php }else{?></div></div><?php }?>
    </div>
    <?php }}}?>
	<?php // Code to show latest news ends here. ?>
	

	
	<?php if((!$isLogin && isset($PublicMenu["Banner Ads"]) && $PublicMenu["Banner Ads"]==1) || ($isLogin && isset($MemberMenu["Banner Ads"]) && $MemberMenu["Banner Ads"]==1))
	{ ?>
		<?php // Code to show 728x90 banner from banner ad plans ?>
		<div class="bottombannersh textcenter"  style="<?php if(@$_COOKIE['bannerbottom']==1)echo 'display:none;';?>"></div>
		<?php if(@$_COOKIE['bannerbottom']==1){$shbbtxt= __('Show Banners');$shbtitle= __('[+]');}else{ $shbbtxt= __('Hide Banners');$shbtitle= __('[-]');}?>
		<div class="textright showhidebannerbottom"><div class="height10"></div><div class="<?php if(!$isLogin){echo "pagewidth";} ?>"><a class="bottombannerbtn shbutton" data-tooltip="<?php echo $shbbtxt;?>" href="javascript:void(0)" onclick="bottombannerbox('.bottombannersh', this,'<?php echo __('[+]'); ?>','<?php echo __('[-]');?>', '<?php echo __('Show Banners')?>', '<?php echo __('Hide Banners')?>');"><?php if(@$_COOKIE['bannerbottom']==1)echo __('[+]');else echo __('[-]');?></a><div class="height10"></div></div></div>
	
		<div id="bluebg" class="bottombannersh" style="<?php if(@$_COOKIE['bannerbottom']==1)echo 'display:none;';?>">
			<div class="pagewidth">
			  <div class="bannermain">
				<?php // Code to show 125x125 advertisement banner ?>
				<div class="banner125"><span id="advertisement_125_1"></span></div>
				<div class="banner125"><span id="advertisement_125_2"></span></div>
				<div class="banner468main">
					<?php // Code to show 468x60 advertisement banner ?>
					<div class="banner468">
						<?php if($isLogin){?><span id="advertisement_468_2"></span><?php }else{?><span id="advertisement_468_1"></span><?php }?>
					</div>
					<?php // Code to show 468x60 banner from banner ad plans ?>
					<div class="banner468"><span id="rotating_468_1"></span></div>
				</div>
				<?php // Code to show 125x125 banner from banner ad plans ?>
				<div class="banner125"><span id="static_125_1"></span></div>
				<div class="banner125"><span id="rotating_125_1"></span></div>
				
				<div class="height10"></div>
				<div class="textcenter"><span id="static_728_1"></span></div>
				<div class="height10"></div>
				<div class="textcenter"><span id="rotating_728_1"></span></div>
			  </div>
			</div>
		</div>
	<?php // Code to show banners after page content ends here ?>
	
	<?php
	}?>
	
	<?php // Do not edit/remove this code line ?>
	<?php echo $BannerAdsList.$PPCBanner.$PPCtextad;?>
	
	<?php // Footer code starts here ?>			
    <div class="footerbg">
	  <div class="footer">
		<?php // Footer links ?>
		<div class="footermenu"><?php echo $this->html->link(__("Terms & Conditions"), array('controller' => 'public', 'action' => 'terms', 'plugin' => false));?></div>
		<div class="footermenu"><?php echo $this->html->link(__("Privacy Policy"), array('controller' => 'public', 'action' => 'privcypolicy', 'plugin' => false));?></div>
		<div class="footermenu"><?php echo $this->html->link(__("Earning Disclaimer"), array('controller' => 'public', 'action' => 'earningdisclaimer', 'plugin' => false));?></div>
		<div class="footertext">
			<?php echo __('Copyright')." &copy; ".date("Y")." ". $SITECONFIG["sitetitle"]." ".__('All Rights Reserved');if($powered_by!='')echo ' | '.__("Powered by").' ';?>
			<?php
			// Do not remove below code
			echo $powered_by;
			// Do not remove above code
			?>
		</div>
	  </div>
    </div>
	<?php // Footer code ends here ?>




<?php // Scroll to top button ?>
<a class="scrollup" href="#" >^</a>

<?php // Do not edit below code. Code to show message ads ?>
<div style='display:none'>
<?php if(@$_COOKIE['popup1']<date("Ydm") && $MessageAdsPublic!=''){?><div id='colorboxpopup1' class="MeaasageAdContent"><?php echo nl2br(stripslashes($MessageAdsPublic));?><div id="MeaasageAdtimer">5</div></div><?php }?>
<?php if(@$_COOKIE['popup2']<date("Ydm") && $MessageAdsActive!=''){?><div id='colorboxpopup2' class="MeaasageAdContent"><?php echo nl2br(stripslashes($MessageAdsActive));?><div id="MeaasageAdtimer">5</div></div><?php }?>
<?php if(@$_COOKIE['popup3']<date("Ydm") && $MessageAdsPaid!=''){?><div id='colorboxpopup3' class="MeaasageAdContent"><?php echo nl2br(stripslashes($MessageAdsPaid));?><div id="MeaasageAdtimer">5</div></div><?php }?>
<?php if(@$_COOKIE['popup4']<date("Ydm") && $MessageAdsUnpaid!=''){?><div id='colorboxpopup4' class="MeaasageAdContent"><?php echo nl2br(stripslashes($MessageAdsUnpaid));?><div id="MeaasageAdtimer">5</div></div><?php }?>
<?php if(@$_COOKIE['popup5']<date("Ydm") && $MessageAdsMembership!=''){?><div id='colorboxpopup5' class="MeaasageAdContent"><?php echo nl2br(stripslashes($MessageAdsMembership));?><div id="MeaasageAdtimer">5</div></div><?php }?>
</div>

<?php // Do not edit above code ?>
<?php if($isLogin){?>
<script type="text/javascript">
	$(document).ready (function () {
		RV();FooterBottom();
	});
</script>
<?php }?>
<?php // Do not edit above code ?>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>
</body>
</html>