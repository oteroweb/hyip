<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<script type="text/javascript"> 
(function() {
$('#<?php echo $ReplyModelname; ?>').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();
</script>
<?php if(!$ajax){
$publicact='';$memberact='';
if($ticketarea=='public')
	$publicact='active';
else
	$memberact='active';
?>
<div class="whitetitlebox">Support Tickets</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="<?php echo $publicact; ?>">
				<?php echo $this->Js->link("Public Tickets", array('controller'=>'support', "action"=>"index/public"), array(
					'update'=>'#supportpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>''
				));?>
			</li>
			<li class="<?php echo $memberact; ?>">
				<?php echo $this->Js->link("Member Tickets", array('controller'=>'support', "action"=>"index/member"), array(
					'update'=>'#supportpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>''
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="supportpage">
<?php }?>
<div id="UpdateMessage"></div>

<?php
if($ticketarea=='public')
{
	$primaryKey='pt_id';
}
elseif($ticketarea=='member')
{
	$primaryKey='mt_id';
	?>
	<div class="whitenoteboxinner"><span class="color-red"><b>*Note :</b></span> <?php echo "Attachments : Only .jpg, .jpeg, .gif, .png, .docx, .doc, .pdf Files Are Allowed to Upload. Max File Size is 5 MB."?></div>
    <?php
}
?>
	<div id="ticketboxleft" class="mobilewidth100">
		<div class="serchmainbox">
			<div class="serchgreybox">
			    Ticket Details
			</div>
			<div class="from-box">
			       <div>Ticket Id : <span><?php echo $ticketdata[$Modelname][$primaryKey];?></span></div> 
			       <div>Creator :
					<span>
						<?php
						if($ticketarea=='public')
						{
							echo $ticketdata[$Modelname]["f_name"]." ".$ticketdata[$Modelname]["l_name"]." (".$ticketdata[$Modelname]['email'].")";
						}
						elseif($ticketarea=='member')
						{
							echo $this->Js->link($ticketdata[$Modelname]["m_name"]." (ID : ".$ticketdata[$Modelname]['m_id'].")", array('controller'=>'member', "action"=>"memberadd/".$ticketdata[$Modelname]['m_id']."/top/support/index~public~0~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));
						}
						?>
					</span>
			       </div>
			       <div>Subject : <span><?php echo $ticketdata[$Modelname]["subject"];?></span></div>
			       <div>Category : <span><?php echo $ticketdata[$Modelname]["category"];?></span></div>	    
			</div>
			<div class="from-box2">
				<div>Date :  <span><?php echo $ticketdata[$Modelname]["dt_create"];?></span></div> 
				<div>Last Update Date : <span><?php echo $ticketdata[$Modelname]["dt_lastupdate"];?></span></div>
				<div>IP Address : <span><?php echo $ticketdata[$Modelname]["ip"];?></span></div>
				<div>Status :
					<span>
						<?php if($ticketdata[$Modelname]["status"]==1)
						{
						    echo 'Open ';
						    $text="(Click to Close)";
						    $statusint=0;
						}
						else
						{
						    echo 'Close ';
						    $text="(Click to Open)";
						    $statusint=1;
						}
						if(!isset($SubadminAccessArray) || in_array('support',$SubadminAccessArray) || in_array('support/adminpanel_ticketstatus',$SubadminAccessArray))
						{
							echo $this->Js->link($text, array('controller'=>'support', "action"=>"ticketstatusinside/".$ticketarea."/".$statusint."/".$ticketdata[$Modelname][$primaryKey]."/"), array(
									'update'=>'#supportpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));
						} ?>
					</span>
				</div>
			</div>
		</div>
	</div>
	
	<div id="ticketboxright" class="mobilewidth100">
		<div class="serchmainbox">
		    
		    <div class="serchgreybox">
			Enter Your Message
		    </div>
		    <div class="from-box">
			<div class="fromborderdropedown3">	
			    <div class="select-main">
			    <label>
				    <?php 
					    $supportemplate[0]="Select Template";
					    ksort($supportemplate);
					    echo $this->Form->input('templateid', array(
						    'type' => 'select',
						    'options' => $supportemplate,
						    'selected' => '',
						    'class'=>'',
						    'label' => false,
						    'div' => false,
						    'style' => '',
						    'onchange'=>'GetPreTemplateSuport(this.value,"#message", "'.$ADMINURL.'");'
					    ));
				    ?>
			    </label>
			    </div>
			</div>
			<div class="height10"></div>
			<?php echo $this->Form->create($ReplyModelname,array('id' => $ReplyModelname, 'type' => 'post', 'type' => 'file', 'onsubmit' => 'return true;', 'url'=>array('controller'=>'support','action'=>'ticketeditaction')));
				echo $this->Form->input($primaryKey, array('type'=>'hidden', 'value'=>$ticketdata[$Modelname][$primaryKey], 'label' => false));
				echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
			?>
			<input type="hidden" name="ticketarea" value="<?php echo $ticketarea; ?>" />
			<input type="hidden" name="primaryKey" value="<?php echo $primaryKey; ?>" />
			<div><?php echo $this->Form->input('message', array('type'=>'textarea', 'id'=>'message', 'label' => false, 'class'=>'from-textarea', 'style'=>'', 'value'=>$SITECONFIG["ticket_signature"]));?></div>
			
			<?php if($ticketarea=='member'){ ?>
			<div class="tablegrid">
				<div class="tablegridrow">
					<div>Attachment 1</div>
					<div>
						<div class="browsebuttonmaindiv marginleft9">
							<div class="btnorange browsebutton">Browse<?php echo $this->Form->input('photo1', array('type' => 'file','label' => false, 'div' => false,'class'=>'browserbuttonlink','onchange'=>'$(".browval1").html(this.value);')); ?></div>
						</div>
						<div class="clear-both"></div>
					</div>
					<div>Attachment 2</div>
					<div>
						<div class="browsebuttonmaindiv marginleft9">
							<div class="btnorange browsebutton">Browse<?php echo $this->Form->input('photo2', array('type' => 'file','label' => false, 'div' => false,'class'=>'browserbuttonlink','onchange'=>'$(".browval2").html(this.value);')); ?></div>
						</div>
						<div class="clear-both"></div>
					</div>
				</div>
				<div class="tablegridrow">
					<div>Attachment 3</div>
					<div>
						<div class="browsebuttonmaindiv marginleft9">
							<div class="btnorange browsebutton">Browse<?php echo $this->Form->input('photo3', array('type' => 'file','label' => false, 'div' => false,'class'=>'browserbuttonlink','onchange'=>'$(".browval3").html(this.value);')); ?></div>
						</div>
						<div class="clear-both"></div>
					</div>
					<div>Attachment 4</div>
					<div>
						<div class="browsebuttonmaindiv marginleft9">
							<div class="btnorange browsebutton">Browse<?php echo $this->Form->input('photo4', array('type' => 'file','label' => false, 'div' => false,'class'=>'browserbuttonlink','onchange'=>'$(".browval4").html(this.value);')); ?></div>
						</div>
						<div class="clear-both"></div>
					</div>
				</div>
			</div>
			<div class="height10"></div>
			<?php } ?>
			
			<?php echo $this->Js->submit('Save', array(
			    'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			    'update'=>'#UpdateMessage',
			    'class'=>'btnorange',
			    'controller'=>'support',
			    'div'=>false,
			    'action'=>'savepretemplate',
			    'url'   => array('controller' => 'support', 'action' => 'savepretemplate')
			  ));?>
			  
			<?php echo $this->Form->submit('Reply', array(
				'class'=>'btnorange',
				'div'=>false
			));?>
					
			<?php echo $this->Js->link("Back", array('controller'=>'support', "action"=>"index/".$ticketarea), array(
				'update'=>'#supportpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'btngray'
			));?>
			
			<?php echo $this->Form->end();?>
		    </div>
		</div>
	</div>
	<div class="clearboth"></div>
	
<div class="height10"></div>

<div class="tablegrid marginnone nomobilecss">
	<div class="tablegridheader">
		<div class="textleft">&nbsp;&nbsp;&nbsp;Conversation History / Time</div>
	</div>
	<?php
	$i = 0;
	foreach ($ticketreplydata as $ticketreply):
		$class = 'class="blue-color"';if ($i++ % 2 == 0){$class = 'class="white-color"';}
		?>
			<div class="tablegridrow">
				<div>
					<div class="ticketbox">
						<?php echo "<b>Message From</b> : ";//.$ticketreply[$ReplyModelname]['replyer_name']." (Id : ".$ticketreply[$ReplyModelname]['replyer_id'].")";
							if($ticketarea=='member' || $ticketreply[$ReplyModelname]['replyer_id']!=0)
							{
							echo $this->Js->link($ticketreply[$ReplyModelname]['replyer_name']." (ID : ".$ticketreply[$ReplyModelname]['replyer_id'].")", array('controller'=>'member', "action"=>"memberadd/".$ticketreply[$ReplyModelname]['replyer_id']."/top/support/index~public~0~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));
							}
							else
							{
								echo $ticketreply[$ReplyModelname]['replyer_name'];
							}
						?>
						<span class="marginleft9"></span>
						<?php echo "<b>Message Time</b> : ".$this->Time->format($SITECONFIG["timeformate"], $ticketreply[$ReplyModelname]['dt_reply']);?>
					</div>
					<div class="ticketmessagebox">
						<?php echo nl2br(stripslashes($ticketreply[$ReplyModelname]['message']));?>
					</div>
						<?php if(@$ticketreply[$ReplyModelname]['attachments']!=''){
							?><div class="ticketbox"><?php
							$attachments=explode(',',$ticketreply[$ReplyModelname]['attachments']); ?>
							<strong><?php echo 'Attachment(s) : ';?>&nbsp;</strong>
							<?php foreach($attachments as $attachment){ 
								$filetype = end(@explode('.', strtolower($attachment)));
								if(in_array(strtolower($filetype), array('jpg', 'jpeg', 'png', 'gif')))
								{
									?>&nbsp;&nbsp;<a href="<?php echo $SITEURL."img/support/".$attachment; ?>" rel="lightbox" title="<?php echo $attachment; ?>"><?php echo $attachment; ?></a><?php
								}
								elseif(in_array(strtolower($filetype), array('pdf', 'doc', 'docx')))
								{
									echo '&nbsp;&nbsp;'.$this->Html->link($attachment, array('controller'=>'support', "action"=>"downloadattachment/".$attachment), array(
										'escape'=>false,
										'target'=>'_blank'
									)); ?>
							<?php }} ?>
							</div>
						<?php } ?>
					
				</div>
			</div>
	<?php endforeach; ?>
	
	<div class="tablegridrow">
		<div>
			<div class="ticketbox">
				<?php echo "<b>Message From</b> : ";
				if($Modelname=="Public_ticket"){
					echo $ticketdata[$Modelname]['f_name']." ".$ticketdata[$Modelname]['l_name'];
				}else{
					echo $this->Js->link($ticketdata[$Modelname]['m_name']." (Id : ".$ticketdata[$Modelname]['m_id'].")", array('controller'=>'member', "action"=>"memberadd/".$ticketdata[$Modelname]['m_id']."/top/support/index~public~0~top"), array(
						'update'=>'#pagecontent',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'View Member'
					));
				}?>
				<span class="marginleft9"></span>
				<?php echo "<b>Message Time</b> : ".$this->Time->format($SITECONFIG["timeformate"], $ticketdata[$Modelname]['dt_create']);?>
			</div>
			<div class="ticketmessagebox">
				<?php echo nl2br(stripslashes($ticketdata[$Modelname]['message']));?>
			</div>
			
				<?php if(@$ticketdata[$Modelname]['attachments']!=''){
					?><div class="ticketbox"><?php
					$attachments=explode(',',$ticketdata[$Modelname]['attachments']); ?>
					<strong>Attachment(s) : &nbsp;</strong>
					<?php foreach($attachments as $attachment){ 
						$filetype = end(@explode('.', strtolower($attachment)));
						if(in_array(strtolower($filetype), array('jpg', 'jpeg', 'png', 'gif')))
						{
							?>&nbsp;&nbsp;<a href="<?php echo $SITEURL."img/support/".$attachment; ?>" rel="lightbox" title="<?php echo $attachment; ?>"><?php echo $attachment; ?></a><?php
						}
						elseif(in_array(strtolower($filetype), array('pdf', 'doc', 'docx')))
						{
							echo '&nbsp;&nbsp;'.$this->Html->link($attachment, array('controller'=>'support', "action"=>"downloadattachment/".$attachment), array(
								'escape'=>false,
								'target'=>'_blank'
							)); ?>
					<?php }} ?>
					</div>
				<?php } ?>
		</div>
	</div>
	
</div>	


<?php if(!$ajax){?>
</div><!--#supportpage over-->
				<div class="clear-both"></div>
			</div>
			<div class="clear-both"></div>
			</div>
	   </div>
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>