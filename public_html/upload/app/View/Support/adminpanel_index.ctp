<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php if(!$ajax){
$publicact='';$memberact='';
if($ticketarea=='public')
	$publicact='active';
else
	$memberact='active';
?>
<div class="whitetitlebox">Support Tickets</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="<?php echo $publicact; ?>">
				<?php echo $this->Js->link("Public Tickets", array('controller'=>'support', "action"=>"index/public"), array(
					'update'=>'#supportpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>''
				));?>
			</li>
			<li class="<?php echo $memberact; ?>">
				<?php echo $this->Js->link("Member Tickets", array('controller'=>'support', "action"=>"index/member"), array(
					'update'=>'#supportpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>''
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Predefined Templates", array('controller'=>'support', "action"=>"pretemplate"), array(
					'update'=>'#supportpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>''
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="supportpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div id="UpdateMessage"></div>
<?php if($ticketarea=='public')
{
	?><div class="helpicon"><a href="https://www.proxscripts.com/docs/Support#Public_Tickets" target="_blank">Help</a></div><?php
}
else
{
	?><div class="helpicon"><a href="https://www.proxscripts.com/docs/Support#Member_Tickets" target="_blank">Help</a></div><?php
}?>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php if($ticketarea=='public')
		{
                    $primaryKey='pt_id';
		    $searchparalabel='Visitor First Name';
		}
        elseif($ticketarea=='member')
		{
                    $primaryKey='mt_id';
		    $searchparalabel='Member Name';
		}
    echo $this->Form->create($Modelname,array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'support','action'=>'index')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('searchby', array(
						'type' => 'select',
						'options' => array('active'=>'Open Tickets', 'inactive'=>'Close Tickets', 'awaitingtoreply'=>'Awaiting for Reply Tickets', 'answered'=>'Answered Tickets', 'all'=>'All Tickets', $primaryKey=>'Ticket Id', 'name'=>$searchparalabel,'category'=>'Category'),
						'selected' => $searchby,
						'class'=>'',
						'label' => false,
						'style' => '',
						'onchange'=>'if($(this).val()=="awaitingtoreply" || $(this).val()=="answered" || $(this).val()=="active" || $(this).val()=="inactive" || $(this).val()=="all"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);} if($(this).val()=="category"){$(".searchfor_all").hide();$(".searchfor_category").show(500);}else{$(".searchfor_category").hide();$(".searchfor_all").show(500);}'
						));?>
						<input type="hidden" name="ticketarea" value="<?php echo $ticketarea; ?>" />
					</label>
				</div>
			</div>
		  </span>
		</div>
		<?php if($searchby=="awaitingtoreply" || $searchby=="answered" || $searchby=="inactive" || $searchby=="active" || $searchby=="all" || $searchby==""){ $display="none";}else{$display="";} ?>
		<div class="fromboxmain" id="SearchFor" style="display:<?php echo $display; ?>" >
			<span>Search For :</span>
			<span class="searchforfields_s searchfor_all" style='display:<?php if($searchby=="category"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class='searchfor_category' style='display:<?php if($searchby!="category"){ echo "none";} ?>'>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
							<?php 
							echo $this->Form->input('searchcategory', array(
							'type' => 'select',
							'options' => array('General'=>'General', 'Technical'=>'Technical'),
							'selected' => $searchcategory,
							'class'=>'',
							'label' => false,
							'style' => ''
							));?>
						</label>
					</div>
				</div>
			</span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
                  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#supportpage',
                  'class'=>'searchbtn',
                  'controller'=>'support',
                  'action'=>'index',
                  'url'=> array('controller' => 'support', 'action' => 'index')
                ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->

        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#supportpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'support', 'action'=>'index/'.$ticketarea)
        ));
        $currentpagenumber=$this->params['paging'][$Modelname]['page'];
        ?>
		
<div id="gride-bg">
    <div class="Xpadding10">
				
	<?php echo $this->Form->create($Modelname,array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'support','action'=>'ticketremove')));?>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php 
			echo $this->Form->checkbox('selectAllCheckboxes', array(
			  'hiddenField' => false,
			  'onclick' => 'selectAllCheckboxes("ticketIds",this.checked)',
			  'id'=>'ticketIdsAll'
			));
		?>
		<label for="ticketIdsAll"></label>
	</div>
	<div class="addnew-button">
		<?php if(!isset($SubadminAccessArray) || in_array('support',$SubadminAccessArray) || in_array('support/adminpanel_ticketremove',$SubadminAccessArray))
		{
			echo $this->Js->submit('Delete Selected Ticket(s)', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'escape'=>false,
			  'update'=>'#supportpage',
			  'class'=>'btngray',
			  'div'=>false,
			  'controller'=>'support',
			  'action'=>'ticketremove',
			  'confirm' => 'Are You Sure?',
			  'url'   => array('controller' => 'support', 'action' => 'ticketremove')
			));
		} ?>
    <?php
		if(!isset($SubadminAccessArray) || in_array('support',$SubadminAccessArray) || in_array('support/adminpanel_ticketstatus',$SubadminAccessArray)){
			echo $this->Js->submit('Close Selected Ticket(s)', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'escape'=>false,
			  'update'=>'#supportpage',
			  'class'=>'btngray',
			  'div'=>false,
			  'controller'=>'support',
			  'action'=>'ticketstatus',
			  'confirm' => 'Are You Sure?',
			  'url'   => array('controller' => 'support', 'action' => 'ticketstatus/'.$ticketarea)
			));	
		}
		?>
	</div>
	<div class="clear-both"></div>
        <input type="hidden" name="ticketarea" value="<?php echo $ticketarea; ?>" />
		
		<div class="tablegrid">
				<div class="tablegridheader">
                <?php
                    if($ticketarea=='public')
                    {
                        $label=array('Ticket Id','Date Created','Last Update Date','Visitor','Subject','Last Replier','Category','IP');
                        $sortcolumn=array('pt_id','dt_create','dt_lastupdate','f_name','subject','last_replyer','category','ip');
                    }
                    elseif($ticketarea=='member')
                    {
                        $label=array('Ticket Id','Date Created','Last Update Date','Member','Subject','Last Replier','Category','IP');
                        $sortcolumn=array('mt_id','dt_create','dt_lastupdate','m_name','subject','last_replyer','category','ip');
                    }
                ?>
                    <?php if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                    for($i=0;$i<count($label);$i++){ ?>
                        <div>
                            <?php 
                            echo $this->Js->link($label[$i], array('controller'=>'support', "action"=>"index/".$ticketarea."/0/0/".$sortcolumn[$i]."/".$sorttype."/".$currentpagenumber), array(
                                'update'=>'#supportpage',
                                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                'escape'=>false,
                                'class'=>'vtip',
                                'title'=>'Sort By '.$label[$i]
                            ));?>
                        </div>
                    <?php } ?>
                    <div><?php echo 'Action';?></div>
                    <div></div>
                </div>
                <?php $i=0; foreach ($ticketsdata as $ticket): $i++;
                    if($ticket[$ReplyModelname]['replyer_id']!=1) $class = ' highlight_tr';
                    else $class = '';
                    ?>
                    <div class="tablegridrow <?php echo $class;?>">
					    <div>
								<?php
								if(!isset($SubadminAccessArray) || in_array('support',$SubadminAccessArray) || in_array('support/adminpanel_ticketedit',$SubadminAccessArray)){
									echo $this->Js->link($ticket[$Modelname][$primaryKey], array('controller'=>'support', "action"=>"ticketedit/".$ticketarea."/".$ticket[$Modelname][$primaryKey]), array(
										'update'=>'#supportpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false,
										'class'=>'vtip',
										'title'=>'Reply Ticket'
									));
								}
								else
								{
									echo $ticket[$Modelname][$primaryKey];
								}?>
						</div>
						<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $ticket[$Modelname]['dt_create']); ?></div>
                        <div><?php echo $this->Time->format($SITECONFIG["timeformate"], $ticket[$Modelname]['dt_lastupdate']); ?></div>
                        <div>
							<?php 
							if($ticketarea=='public')
							{
								if(!isset($SubadminAccessArray) || in_array('support',$SubadminAccessArray) || in_array('support/adminpanel_ticketedit',$SubadminAccessArray)){
									echo $this->Js->link($ticket[$Modelname]['f_name']." ".$ticket[$Modelname]['l_name'], array('controller'=>'support', "action"=>"ticketedit/".$ticketarea."/".$ticket[$Modelname][$primaryKey]), array(
										'update'=>'#supportpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false,
										'class'=>'vtip',
										'title'=>'Reply Ticket'
									)).' ('.$ticket[$Modelname]['email'].')';
								}
								else
								{
									echo $ticket[$Modelname]['f_name']." ".$ticket[$Modelname]['l_name'].' ('.$ticket[$Modelname]['email'].')';
								}
							}
							else
							{
								echo $this->Js->link($ticket[$Modelname]['m_name'], array('controller'=>'member', "action"=>"memberadd/".$ticket[$Modelname]['m_id']."/top/support/index~public~0~top"), array(
									'update'=>'#pagecontent',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>'View Member'
								));
							}
							?>
						</div>
                        <div>
								<?php
								if(!isset($SubadminAccessArray) || in_array('support',$SubadminAccessArray) || in_array('support/adminpanel_ticketedit',$SubadminAccessArray)){
									echo $this->Js->link($ticket[$Modelname]['subject'], array('controller'=>'support', "action"=>"ticketedit/".$ticketarea."/".$ticket[$Modelname][$primaryKey]), array(
										'update'=>'#supportpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false,
										'class'=>'vtip',
										'title'=>'Reply Ticket'
									));
								}
								else
								{
									echo $ticket[$Modelname]['subject'];
								}?>
						</div>
                            <div>
								<?php
								if(!isset($SubadminAccessArray) || in_array('support',$SubadminAccessArray) || in_array('support/adminpanel_ticketedit',$SubadminAccessArray)){
									echo $this->Js->link($ticket[$Modelname]['last_replyer'], array('controller'=>'support', "action"=>"ticketedit/".$ticketarea."/".$ticket[$Modelname][$primaryKey]), array(
										'update'=>'#supportpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false,
										'class'=>'vtip',
										'title'=>'Reply Ticket'
									));
								}
								else
								{
									echo $ticket[$Modelname]['last_replyer'];
								}?>
							</div>
                        <div>
								<?php
								if(!isset($SubadminAccessArray) || in_array('support',$SubadminAccessArray) || in_array('support/adminpanel_ticketedit',$SubadminAccessArray)){
									echo $this->Js->link($ticket[$Modelname]['category'], array('controller'=>'support', "action"=>"ticketedit/".$ticketarea."/".$ticket[$Modelname][$primaryKey]), array(
										'update'=>'#supportpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false,
										'class'=>'vtip',
										'title'=>'Reply Ticket'
									));
								}
								else
								{
									echo $ticket[$Modelname]['category'];
								} ?>
						</div>
						<div><?php echo $ticket[$Modelname]['ip']; ?></div>
                        <div>
                            <table  border="0" cellspacing="0" cellpadding="0" id="gride-icon" width="100%">
                              <tr>
                                <td  class="icon-class" align="center" valign="middle">
                                    <?php 
                                    if(!isset($SubadminAccessArray) || in_array('support',$SubadminAccessArray) || in_array('support/adminpanel_ticketstatus',$SubadminAccessArray)){
										if($ticket[$Modelname]['status']==0)
										{
											$field_reqaction='1';
											$field_reqicon='red-icon.png';
											$field_reqtext='Open Ticket';
										}
										else
										{
											$field_reqaction='0';
											$field_reqicon='blue-icon.png';
											$field_reqtext='Close Ticket';
										}
										echo $this->Js->link($this->html->image($field_reqicon, array('alt'=>$field_reqtext)), array('controller'=>'support', "action"=>"ticketstatus/".$ticketarea."/".$field_reqaction."/".$ticket[$Modelname][$primaryKey]."/".$currentpagenumber), array(
											'update'=>'#supportpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'class'=>'vtip',
											'title'=>$field_reqtext
										));
									}
									else
									{
										echo '&nbsp;';
									}?>
                                </td>
                                <td class="icon-class" align="center" valign="middle">
                                    <?php
                                    if(!isset($SubadminAccessArray) || in_array('support',$SubadminAccessArray) || in_array('support/adminpanel_ticketedit',$SubadminAccessArray)){
										echo $this->Js->link($this->html->image('reply.png', array('alt'=>'Reply Ticket')), array('controller'=>'support', "action"=>"ticketedit/".$ticketarea."/".$ticket[$Modelname][$primaryKey]), array(
											'update'=>'#supportpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'class'=>'vtip',
											'title'=>'Reply Ticket'
										));
									}
									else
									{
										echo '&nbsp;';
									}?>
                                </td>
                              </tr>
                            </table>
                        </div>
                        <div>
							<span class="checkbox">
								<?php
								echo $this->Form->checkbox('ticketIds.', array(
								  'value' => $ticket[$Modelname][$primaryKey],
								  'class' => 'ticketIds',
								  'hiddenField' => false,
								  'id'=>'ticketIds'.$i
								));
								?>
								<label for="ticketIds<?php echo $i;?>"></label>
							</span>	
                        </div>
                    </div>
                <?php endforeach; ?>
		</div>
		
		<?php if(count($ticketsdata)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
		
        <?php echo $this->Form->end();
        if($this->params['paging'][$Modelname]['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create($Modelname,array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'support','action'=>'index/'.$ticketarea.'/rpp')));?>
			<div class="resultperpage">
				<label>
					<?php 
					echo $this->Form->input('resultperpage', array(
					  'type' => 'select',
					  'options' => $resultperpage,
					  'selected' => $this->Session->read('pagerecord'),
					  'class'=>'',
					  'label' => false,
					  'div'=>false,
					  'style' => '',
					  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
					));
					?>
				</label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#supportpage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'support',
                  'action'=>'index/'.$ticketarea.'/rpp',
                  'url'   => array('controller' => 'support', 'action' => 'index/'.$ticketarea.'/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>

     </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#supportpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>