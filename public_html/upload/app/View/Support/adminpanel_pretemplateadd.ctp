<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){ ?>
<div class="whitetitlebox">Support Tickets</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Public Tickets", array('controller'=>'support', "action"=>"index/public"), array(
					'update'=>'#supportpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>''
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Member Tickets", array('controller'=>'support', "action"=>"index/member"), array(
					'update'=>'#supportpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>''
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Predefined Templates", array('controller'=>'support', "action"=>"pretemplate"), array(
					'update'=>'#supportpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>''
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="supportpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Support#Predefined_Templates" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
	
<div class="backgroundwhite">
				
		<?php echo $this->Form->create('Supportemplate',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'support','action'=>'pretemplateaddaction')));?>
		
		<?php if(isset($supportemplatedata['Supportemplate']["id"])){
			echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$supportemplatedata['Supportemplate']["id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}?>
		<div class="frommain">
			
			<div class="fromnewtext">Status :</div>
			<div class="fromborderdropedown3">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('status', array(
							'type' => 'select',
							'options' => array('1'=>'Active', '0'=>'Inactive'),
							'selected' => $supportemplatedata['Supportemplate']["status"],
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));
						?>
					</label>
			  </div>  
			</div>
			
			<div class="fromnewtext">Description :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('description', array('type'=>'text', 'value'=>stripslashes($supportemplatedata['Supportemplate']["description"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			
			<div class="fromnewtext">Subject :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('subject', array('type'=>'text', 'value'=>stripslashes($supportemplatedata['Supportemplate']["subject"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			
			<div class="fromnewtext">Message :<span class="red-color">*</span></div>
			<div class="frombordermain">
				<?php echo $this->Form->input('message', array('type'=>'textarea', 'value'=>stripslashes($supportemplatedata['Supportemplate']["message"]),'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
			</div>
			
			<div class="formbutton">
				<?php echo $this->Js->submit('Update', array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'class'=>'btnorange',
				  'controller'=>'support',
				  'div'=>false,
				  'action'=>'pretemplateaddaction',
				  'url'   => array('controller' => 'support', 'action' => 'pretemplateaddaction')
				));?>
				<?php echo $this->Js->link("Back", array('controller'=>'support', "action"=>"pretemplate"), array(
					'update'=>'#supportpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btngray'
				));?>
			</div>
			
		</div>  
		<?php echo $this->Form->end();?>
	
	
</div>
<?php if(!$ajax){?>
</div><!--#designpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>