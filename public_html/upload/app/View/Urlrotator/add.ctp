<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($Enableurlrotator==1) { ?>
<?php if(!$ajax){ ?>
<div id="urlrotatorrpage">
<?php } ?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("URL Rotator");?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
		
		<?php // URL Rotator add form starts here ?>
		<?php echo $this->Form->create('Rotator',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'urlrotator','action'=>'addaction')));?>
		<div class="form-box">
			<?php if(isset($urlrotatordata['Rotator']["id"])){
				echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$urlrotatordata['Rotator']["id"], 'label' => false));
				echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
			}?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Title");?> : <span class="required">*</span></div>
				<div class="form-col-2">
				<?php echo $this->Form->input('title', array('type'=>'text', 'label' => false, 'value'=>@$urlrotatordata['Rotator']['title'], 'class'=>'formtextbox','div'=>false));?>
				
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?>"></span>
				</div>
			</div>
		</div>
		<div class="formbutton">
			<?php echo $this->Js->link(__('Back'), array('controller'=>'urlrotator', "action"=>"index"), array(
				'update'=>'#urlrotatorrpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button',
				'style'=>'float:left'
			));?>
			<?php echo $this->Js->submit(__('Submit'), array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'button',
				'div'=>false,
				'controller'=>'urlrotator',
				'action'=>'addaction',
				'url'   => array('controller' => 'urlrotator', 'action' => 'addaction')
			));?>
		</div>
		<div class="clear-both"></div>
		<?php echo $this->Form->end();?>
		<?php // URL Rotator add form ends here ?>
		
	</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>