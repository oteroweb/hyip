<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">URL Rotator</div>
<div id="urlrotatorrpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/URL_Rotator" target="_blank">Help</a></div>
<div id="UpdateMessage" class="UpdateMessage"></div>
<div class="backgroundwhite">
    
<?php echo $this->Form->create('Rotatorurl', array('type' => 'post', 'onsubmit' => 'return false;', 'url'=>array('controller'=>'urlrotator', 'action'=>'urladdaction')));?>
	<?php if(isset($urlrotatordata['Rotatorurl']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$urlrotatordata['Rotatorurl']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
		<div class="frommain">
			<div class="fromnewtext">Title :  </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('target', array('type'=>'text', 'value'=>$urlrotatordata['Rotatorurl']["target"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>

			<div class="formbutton">
				<?php echo $this->Js->submit('Submit', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'btnorange',
				'div'=>false,
				'controller'=>'urlrotator',
				'action'=>'urladdaction',
				'url'   => array('controller' => 'urlrotator', 'action' => 'urladdaction')
			  ));?>
			  
			  <?php echo $this->Js->link("Back", array('controller'=>'urlrotator', "action"=>"url/".$urlrotatordata['Rotatorurl']["rid"]), array(
				  'update'=>'#urlrotatorrpage',
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'escape'=>false,
				  'class'=>'btngray'
			  ));?>
			</div>
		</div>
<?php echo $this->Form->end();?>
	
</div>

	<?php if(!$ajax){?>
</div><!--#balancetransferpage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>