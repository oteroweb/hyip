<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">URL Rotator</div>
<div id="urlrotatorrpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/URL_Rotator" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="serchmainbox widht100onallscreen">
    <div class="serchgreybox">Search Option</div>
    <?php echo $this->Form->create('Rotatorurl',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'urlrotator','action'=>'url/'.$planid)));?>
	<div class="from-box">
       <div class="fromboxmain">
            <span>Search By :</span>
            <span>                     
				<div class="searchoptionselect">
				    <div class="select-main">
					<label>
					    <?php 
					    echo $this->Form->input('searchby', array(
						  'type' => 'select',
						  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'clicks'=>'Click'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => ''
					    ));
					    ?>
					</label>
				    </div>
				</div>
             </span>
		</div>
        <div class="fromboxmain">
            <span>Search For :</span>
            <span>
				<?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?>
			</span>
		</div>
     </div>
     <div class="from-box">
         <div class="fromboxmain width480">
             <span>From :</span>
              <span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
         </div>
         <div class="fromboxmain">
             <span>To :</span>
             <span><?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
             <span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#urlrotatorrpage',
					'class'=>'searchbtn',
					'controller'=>'urlrotator',
					'action'=>'url/'.$planid,
					'url'=> array('controller' => 'urlrotator', 'action' => 'url/'.$planid)
				));?>
			 </span>
         </div>
    </div>
	 <?php echo $this->Form->end();?>

</div>
<div class='height10'></div>
<div class="serchmainbox widht100onallscreen">
    <div class="serchgreybox">Rotator Details</div>
		<?php echo $this->Form->create('Rotator',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'urlrotator','action'=>'addaction')));?>
		<?php if(isset($urlrotatordata['Rotator']["id"])){
			echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$urlrotatordata['Rotator']["id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}?>
		
		<div class="from-box">
			<div class="fromboxmain">
				 <span>Status :  </span>
				 <span>                     
					 <div class="searchoptionselect">
						<div class="select-main">
							<label>
								<?php 
								   echo $this->Form->input('status', array(
									   'type' => 'select',
									   'options' => array('1'=>'Active', '0'=>'Inactive'),
									   'selected' => $urlrotatordata['Rotator']["status"],
									   'class'=>'',
									   'label' => false,
									   'div' => false,
									   'style' => ''
								   ));
							   ?>
							</label>
						</div>
					 </div>
				  </span>
			</div>
			<div class="fromboxmain">
				 <span>Title :</span>
				 <span>
					 <?php echo $this->Form->input('title', array('type'=>'text', 'value'=>$urlrotatordata['Rotator']["title"], 'label' => false, 'div' => false, 'class'=>''));?>
				 </span>
			</div>
		</div>
		<div class="from-box">
			<div class="fromboxmain">
				 <span>URL :</span>
				 <span>
					 <a href="<?php echo $SITEURL.'x/r'.$urlrotatordata['Rotator']['code'];?>" target="_blank" style="margin-left:9px;font-weight:normal;line-height:2.5;"><?php echo $SITEURL.'x/r'.$urlrotatordata['Rotator']['code'];?></a>
				 </span>
			</div>
			<div class="fromboxmain">
				<span class="frommain">
					<span class="formbutton" style="padding-top:0px;">
						<?php echo $this->Js->submit('Submit', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#UpdateMessage',
						'class'=>'btnorange',
						'div'=>false,
						'controller'=>'urlrotator',
						'action'=>'urlrotatorurladdaction',
						'url'   => array('controller' => 'urlrotator', 'action' => 'addaction')
					  ));?>
					</span>
				</span>
			</div>	
		</div>
		<?php echo $this->Form->end();?>
</div>

	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#urlrotatorrpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'urlrotator', 'action'=>'url')
	));
	$currentpagenumber=$this->params['paging']['Rotatorurl']['page'];
	?>			
	
<div id="Xgride-bg">
	<div class="height10"></div>
    <div class="padding10 backgroundwhite">
		<div class="greenbottomborder">
			<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
			<?php echo $this->Js->link("<< Back", array('controller'=>'urlrotator', "action"=>"member"), array(
				'update'=>'#urlrotatorrpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'btnorange'
			));?>
	
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Rotatorurl',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'urlrotator','action'=>'url')));?>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'urlrotator', "action"=>"url/".$planid."/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#urlrotatorrpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Date', array('controller'=>'urlrotator', "action"=>"url/".$planid."/0/dt/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#urlrotatorrpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Date'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('URL', array('controller'=>'urlrotator', "action"=>"url/".$planid."/0/target/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#urlrotatorrpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By URL'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Click', array('controller'=>'urlrotator', "action"=>"url/".$planid."/0/clicks/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#urlrotatorrpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Click'
					));?>
				</div>
				<div><?php echo 'Action';?></div>
            </div>
			<?php foreach ($rotatorurldata as $rotatorurl): ?>
				<div class="tablegridrow">
					<div><?php echo $rotatorurl['Rotatorurl']['id']; ?></div>
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $rotatorurl['Rotatorurl']['dt']); ?></div>
					<div><?php echo $rotatorurl['Rotatorurl']['target']; ?></div>
					<div><?php echo $rotatorurl['Rotatorurl']['clicks']; ?></div>
					<div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
						  	<li>
								<?php
									echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit URL')).' Edit URL', array('controller'=>'urlrotator', "action"=>"urladd/".$rotatorurl['Rotatorurl']['id']), array(
										'update'=>'#urlrotatorrpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<li>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete URL')).' Delete URL', array('controller'=>'urlrotator', "action"=>"urlremove/".$rotatorurl['Rotatorurl']['rid']."/".$rotatorurl['Rotatorurl']['id']."/".$currentpagenumber), array(
									'update'=>'#urlrotatorrpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'confirm'=>"Do You Really Want To Delete URL?"
								));?>
							</li>
						  </ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
				
	</div>
	
	<?php if(count($rotatorurldata)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
    <?php echo $this->Form->end();
	if($this->params['paging']['Rotatorurl']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Rotatorurl',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'urlrotator','action'=>'url/'.$planid.'/rpp')));?>
		<div class="resultperpage">
                        <label>
			    <?php 
			    echo $this->Form->input('resultperpage', array(
			      'type' => 'select',
			      'options' => $resultperpage,
			      'selected' => $this->Session->read('pagerecord'),
			      'class'=>'',
			      'label' => false,
			      'div'=>false,
			      'style' => '',
			      'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			    ));
			    ?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#urlrotatorrpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'urlrotator',
			  'action'=>'url/'.$planid.'/rpp',
			  'url'   => array('controller' => 'urlrotator', 'action' => 'url/'.$planid.'/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
	</div>
</div>
<?php if(!$ajax){?>
</div><!--#urlrotatorrpage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>