<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">URL Rotator</div>
<div id="urlrotatorrpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/URL_Rotator" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="serchmainbox">
    <div class="serchgreybox">Search Option</div>
    <?php echo $this->Form->create('Rotator',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'urlrotator','action'=>'member')));?>
	<div class="from-box">
       <div class="fromboxmain">
            <span>Search By :</span>
            <span>                     
				<div class="searchoptionselect">
				    <div class="select-main">
					<label>
					    <?php 
						echo $this->Form->input('searchby', array(
						      'type' => 'select',
						      'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'member_id'=>'Member Id', 'title'=>'Title', 'active'=>'Active Records', 'inactive'=>'Inactive Records'),
						      'selected' => $searchby,
						      'class'=>'',
						      'label' => false,
						      'style' => '',
							  'onchange'=>'if($(this).val()=="active" || $(this).val()=="inactive"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500)}'
						));
					    ?>
					</label>
				    </div>
				</div>
             </span>
		 </div>
         <div class="fromboxmain" id="SearchFor" <?php if($searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>>
            <span>Search For :</span>
            <span>
					<?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?>
			</span>
		 </div>
     </div>
     <div class="from-box">
         <div class="fromboxmain width480">
             <span>From :</span>
              <span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
         </div>
         <div class="fromboxmain">
             <span>To :</span>
             <span><?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
             <span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#urlrotatorrpage',
					'class'=>'searchbtn',
					'controller'=>'urlrotator',
					'action'=>'member',
					'url'=> array('controller' => 'urlrotator', 'action' => 'member')
				));?>
			 </span>
         </div>
    </div>
	 <?php echo $this->Form->end();?>
</div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#urlrotatorrpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'urlrotator', 'action'=>'member')
	));
	$currentpagenumber=$this->params['paging']['Rotator']['page'];
	?>
<div id="gride-bg" class="noborder">
    <div class="padding10">
		<div class="greenbottomborder">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Rotator',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'urlrotator','action'=>'member')));?>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'urlrotator', "action"=>"member/0/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#urlrotatorrpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Date', array('controller'=>'urlrotator', "action"=>"member/0/0/0/purchasedate/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#urlrotatorrpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Date'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('M. Id', array('controller'=>'urlrotator', "action"=>"member/0/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#urlrotatorrpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Title', array('controller'=>'urlrotator', "action"=>"member/0/0/0/title/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#urlrotatorrpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Title'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Rotator URL', array('controller'=>'urlrotator', "action"=>"member/0/0/0/code/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#urlrotatorrpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Rotator URL'
					));?>
				</div>
				<div><?php echo 'Action';?></div>
            </div>
			<?php foreach ($urlrotatordata as $urlrotator): ?>
				<div class="tablegridrow">
					<div><?php echo $urlrotator['Rotator']['id']; ?></div>
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $urlrotator['Rotator']['purchasedate']); ?></div>
					<div>
						<?php 
						echo $this->Js->link($urlrotator['Rotator']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$urlrotator['Rotator']['member_id']."/top/urlrotator/index~top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div><?php echo $urlrotator['Rotator']['title']; ?></div>
					<div><a href="<?php echo str_replace("https://", "http://", $SITEURL).'x/r'.$urlrotator['Rotator']['code'];?>" target="_blank"><?php echo str_replace("https://", "http://", $SITEURL).'x/r'.$urlrotator['Rotator']['code'];?></a></div>
					<div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							
							<?php if(!isset($SubadminAccessArray) || in_array('urlrotator',$SubadminAccessArray) || in_array('urlrotator/adminpanel_url', $SubadminAccessArray)){ ?>
							<li>
								<?php
									echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit URLs')).' Edit URLs', array('controller'=>'urlrotator', "action"=>"url/".$urlrotator['Rotator']['id']), array(
										'update'=>'#urlrotatorrpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<?php } ?>
							
						  	<?php if(!isset($SubadminAccessArray) || in_array('urlrotator',$SubadminAccessArray) || in_array('urlrotator/adminpanel_status', $SubadminAccessArray)){ ?>
							    <li>
								    <?php
								    if($urlrotator['Rotator']['pause']==0){
									    $statusaction='1';
									    $statusicon='pause.png';
									    $statustext='Unpause Rotator';
								    }else{
									    $statusaction='0';
									    $statusicon='play.png';
									    $statustext='Pause Rotator';}
								    echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'urlrotator', "action"=>"pause/".$statusaction."/".$urlrotator['Rotator']['id']."/".$currentpagenumber), array(
									    'update'=>'#urlrotatorrpage',
									    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									    'escape'=>false
								    ));
								    ?>
							    </li>
							    <li>
								    <?php 
									    if($urlrotator['Rotator']['status']==0){
										    $statusaction='1';
										    $statusicon='red-icon.png';
										    $statustext='Activate Rotator';
									    }else{
										    $statusaction='0';
										    $statusicon='blue-icon.png';
										    $statustext='Inactivate Rotator';}
									    echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'urlrotator', "action"=>"status/".$statusaction."/".$urlrotator['Rotator']['id']."/".$currentpagenumber), array(
										    'update'=>'#urlrotatorrpage',
										    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										    'escape'=>false
									    ));
								    ?>
							    </li>
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('urlrotator',$SubadminAccessArray) || in_array('urlrotator/adminpanel_remove', $SubadminAccessArray)){ ?>
							    <li>
								    <?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Record')).' Delete Record', array('controller'=>'urlrotator', "action"=>"remove/".$urlrotator['Rotator']['id']."/".$currentpagenumber), array(
									    'update'=>'#urlrotatorrpage',
									    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									    'escape'=>false,
									    'confirm'=>"Do You Really Want To Delete This Record?"
								    ));?>
							    </li>
							<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
	</div>
	
	<?php if(count($urlrotatordata)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
    <?php echo $this->Form->end();
	if($this->params['paging']['Rotator']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Rotator',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'urlrotator','action'=>'member/0/0/rpp')));?>
		<div class="resultperpage">
                    <label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
		    </label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#urlrotatorrpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'urlrotator',
			  'action'=>'member/0/0/rpp',
			  'url'   => array('controller' => 'urlrotator', 'action' => 'member/0/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#urlrotatorrpage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>