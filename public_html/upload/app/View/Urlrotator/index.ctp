<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('framechecker');?>
<?php if($Enableurlrotator==1) { ?>
<?php if(!$ajax){ ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="urlrotatorrpage">
<?php } ?>
<div id="UpdateMessage"></div>

<?php //Frame Break Code Start ?>
<input type="hidden" id='framebreakmessage' value='<?php echo __('Site URL Not Valid'); ?>|<?php echo __('Breaking out of frames'); ?>|<?php echo __('Inspecting Website'); ?>|<?php echo __('Framebreaker test passed'); ?>|<?php echo __('Error').' : '; ?>'/>
<div id="check" class="frambreakmain">
	<div class='textleft'>
		<span id='framebreakimg'></span>
		<span class="framebreaktext"><?php echo __('Inspecting Framebreaker'); ?></span>
	</div>
	<div class='textleft'>
		<span id="check_final"><?php echo __('Inspecting Website'); ?></span>
	</div>
</div>
<?php //Frame Break Code Over ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('URL Rotator');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php $this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#urlrotatorrpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'urlrotator', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Rotatorurl']['page'];
	?>
		<div class="padding-left-serchtabal">
			<?php echo $this->Js->link(__("Add New"), array('controller'=>'urlrotator', "action"=>"add"), array(
			'update'=>'#urlrotatorrpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'button'
		));?>
		</div>
		<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			
			<?php // URL Rotator table starts here ?>
			<div class="divtable textcenter">
				<div class="divthead">
					<div class="divtr tabal-title-text" >
						<div class="divth textcenter vam"><?php echo __("Id");?></div>
						<div class="divth textcenter vam"><?php echo __("Title");?></div>
						<div class="divth textcenter vam"><?php echo __("URL");?></div>
						<div class="divth textcenter vam"></div>
						<div class="divth textcenter vam">&nbsp;</div>
					</div>
				</div>
				<div class="divtbody">
					<?php $i=1;$k=1;
					$lastid=0;
					foreach ($urlrotatordata as $urlrotator):
					?>
						<?php
						if($urlrotator['Rotator']['id']!=$lastid)
						{
							if($k%2==0){$class='white-color';}else{$class='gray-color';}
							$k++;
						?>
						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam"><?php echo $urlrotator['Rotator']['id'];?></div>
							<div class="divtd textcenter vam"><?php echo $urlrotator['Rotator']['title'];?></div>
							<div class="divtd textcenter vam">
								<a href="<?php echo str_replace("https://", "http://", $SITEURL).'x/r'.$urlrotator['Rotator']['code'];?>" target="_blank"><?php echo str_replace("https://", "http://", $SITEURL).'x/r'.$urlrotator['Rotator']['code'];?></a>
								<span class="addsiteclass<?php echo $i;?>" style="display:none; margin:5px;">
									<?php echo $this->Form->create('Rotatorurl', array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off', 'url'=>array('controller'=>'urlrotator', 'action'=>'urladdaction')));?>
										<?php
										$classtext='txtframebreaker'.$i;
										$classbtn='framebreaker'.$i;
										?>
										<?php
										echo $this->Form->input('rid', array('type'=>'hidden', 'value'=>$urlrotator['Rotator']["id"], 'label' => false));
										echo $this->Form->input('target', array('type'=>'text', 'value'=>'http://', 'label' => false, 'error' => false, 'div' => false, 'class'=>'formtextbox '.$classtext));
										?>
										<input type="submit" value="<?php echo __('Submit'); ?>" class="button" onclick="return checkframebreaker(0,'<?php echo $SITEURL; ?>','<?php echo $classbtn; ?>');" />
										<?php echo $this->Js->submit(__('Submit'), array(
											'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'update'=>'#urlrotatorrpage',
											'class'=>'large white button margin-top10 framebreaker '.$classbtn,
											'style'=>'display:none',
											'div'=>false,
											'controller'=>'urlrotator',
											'action'=>'urladdaction',
											'url'   => array('controller' => 'urlrotator', 'action' => 'urladdaction')
										));?>
									<?php echo $this->Form->end();?>
								</span>
							</div>
							<div class="divtd textcenter vam">
								<input type="button" class="button floatnone" value="<?php echo __("Add New");?>" onclick="$('.addsiteclass<?php echo $i;?>').css({'display':'block'}); $(this).hide(500);" />
							</div>
							<div class="divtd textcenter vam">
								
								<div class="actionmenu">
								      <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
									        <li>
										     <?php if($urlrotator['Rotator']['pause']==0){
												$statusaction='1';
												$statusicon='pause.png';
												$statustext='Unpause Rotator';
											}else{
												$statusaction='0';
												$statusicon='play.png';
												$statustext='Pause Rotator';}
											echo $this->Js->link($this->html->image($statusicon, array('alt'=>__($statustext)))." ".__($statustext), array('controller'=>'urlrotator', "action"=>"status/".$statusaction."/".$urlrotator['Rotator']['id']."/".$currentpagenumber), array(
												'update'=>'#urlrotatorrpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											)); ?>
									      </li>
									      <li>
										      <?php
											if($urlrotator['Rotator']['status']==0){
												$statusicon='red-icon.png';
												$statustext='Inactive Rotator';
											}else{
												$statusicon='blue-icon.png';
												$statustext='Active Rotator';}
											echo '<a>'.$this->html->image($statusicon, array('alt'=>__($statustext),'class'=>'','title'=>'','style'=>'vertical-align:top'));
											echo " ".__($statustext)."</a>"
											?>
									      </li>
									      <li>
											<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit Rotator')))." ".__('Edit Rotator'), array('controller'=>'urlrotator', "action"=>"add/".$urlrotator['Rotator']['id']), array(
												'update'=>'#urlrotatorrpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));?>
									      </li>
									      <li>
									            <?php
											echo $this->Js->link($this->html->image('delete.png', array('alt'=>__('Delete Rotator')))." ".__('Delete Rotator'), array('controller'=>'urlrotator', "action"=>"remove/".$urlrotator['Rotator']['id']."/".$currentpagenumber), array(
												'update'=>'#urlrotatorrpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>'',
												'confirm'=>__("Do you really want to delete this rotator?")
											));
											?>
									      </li>
									 </ul>
								      </div>
								</div>
							
							</div>
						</div>
						
						<?php
						$lastid=$urlrotator['Rotator']['id'];
						}
						if($urlrotator['Rotatorurl']['id']>0)
						{
						?>
						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam"></div>
							<div class="divtd textcenter vam"></div>
							<div class="divtd textcenter vam">
								<?php echo $urlrotator['Rotatorurl']['target'];?>
							</div>
							<div class="divtd textcenter vam">
								<?php echo $urlrotator['Rotatorurl']['clicks'];?> <?php echo __("views");?>
							</div>
							<div class="divtd textcenter vam">
								
								<div class="actionmenu">
								      <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
									        <li>
										    <?php
											echo $this->Js->link($this->html->image('delete.png', array('alt'=>__('Delete URL')))." ".__('Delete URL'), array('controller'=>'urlrotator', "action"=>"urlremove/".$urlrotator['Rotatorurl']['id']."/".$currentpagenumber), array(
												'update'=>'#urlrotatorrpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>'',
												'confirm'=>__("Do you really want to delete this URL?")
											));
											?>
									      </li>
									 </ul>
								      </div>
								</div>
								
							</div>
						</div>
						<?php
						}
						else{
						?>
						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam"></div>
							<div class="divtd textcenter vam"></div>
							<div class="divtd textcenter vam">
								<?php echo __("No sites added.");?>
							</div>
							<div class="divtd textcenter vam"></div>
							<div class="divtd textcenter vam"></div>
						</div>
						<?php
						}
						?>
					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($urlrotatordata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // URL Rotator table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Rotatorurl']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Rotatorurl',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'urlrotator','action'=>'index/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#urlrotatorrpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'urlrotator',
						  'action'=>'index/rpp',
						  'url'   => array('controller' => 'urlrotator', 'action' => 'index/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
			</div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		
</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>