<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 03-11-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('framechecker');?>
<?php if($Enableppc==1) { ?>
<?php if(!$ajax) { ?>
<div id="Ppcpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<script> processer=new Array();
	<?php
	$firstproceser=0;
	foreach($Processorsextrafield as $proid=>$provalue)
	{
	?>
	processer[<?php echo $proid; ?>]=<?php echo $provalue; ?>;
	<?php }?>
</script>
<?php //Frame Break Code Start ?>
<input type="hidden" id='framebreakmessage' value='<?php echo __('Destination URL Not Valid'); ?>|<?php echo __('Breaking out of frames'); ?>|<?php echo __('Inspecting Website'); ?>|<?php echo __('Framebreaker test passed'); ?>|<?php echo __('Error').' : '; ?>'/>
<div id="check" class="frambreakmain">
	<div class='textleft'>
		<span id='framebreakimg'></span>
		<span class="framebreaktext"><?php echo __('Inspecting Framebreaker'); ?></span>
	</div>
	<div class='textleft'>
		<span id="check_final"><?php echo __('Inspecting Website'); ?></span>
	</div>
</div>
<?php //Frame Break Code Over ?>

<?php // Current Balance code starts here ?>
<div class="textright"><a class="shbutton currentbalancebutton" href="javascript:void(0)" onclick="currentbalancebox('.balancebox', this,'<?php echo __("[+] Show Balance");?>','<?php echo __("[-] Hide Balance");?>');"><?php if(@$_COOKIE['curbal']==1)echo __("[+] Show Balance"); else echo __("[-] Hide Balance");?>
</a></div>
<div class="comisson-bg balancebox <?php if(@$_COOKIE['curbal']==1)echo 'hide';?>">
	<div class="text-ads-title-text"><?php echo __("Current Balance");?></div>
	<div class="text-ads-title-text-right"></div>
	<div class="clear-both"></div>
</div>
<div id="balancebox" class="main-box-eran balancebox <?php if(@$_COOKIE['curbal']==1)echo 'hide';?>">
	<?php if($SITECONFIG["balance_type"]==1){ ?>
		<div class="divtable">
                        <div class="divtr white-color">
                                <div class="divth textcente vam"><?php echo __("Cash Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCash*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
                        <div class="divtr gray-color">
                                <div class="divth textcente vam"><?php echo __("Earning Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberEarning*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php } ?>
                        <div class="divtr white-color">
                                <div class="divth textcente vam"><?php echo __("Re-purchase Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberRepurchaseCash*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
                        <div class="divtr gray-color">
                                <div class="divth textcente vam"><?php echo __("Commission Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCommission*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php } ?>
                </div>
		<?php }else{ 
			?>
			<div class="divtable">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam"><?php echo __("Payment Processor");?></div>
						<div class="divth textcenter vam"><?php echo __("Cash Balance");?></div>
                                                <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
						<div class="divth textcenter vam"><?php echo __("Earning Balance");?></div>
                                                <?php } ?>
						<div class="divth textcenter vam"><?php echo __("Re-purchase Balance");?></div>
                                                <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
						<div class="divth textcenter vam"><?php echo __("Commission Balance");?></div>
                                                <?php } ?>
					</div>
				</div>
				<?php $i=1;
				foreach($MemberCash as $proc_name=>$cash)
				{
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $proc_name;?> :</div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCash[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberEarning[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php } ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberRepurchaseCash[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCommission[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php } ?>
					</div>
					<?php
				$i++; }?>
			</div>
	<?php } ?>
</div>
<?php // Current Balance code ends here ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Purchase PPC");?></div>
	<div class="clear-both"></div>
</div>
<div class="height10"></div>
	<div class="main-box-eran">
		<?php // PPC purchase form starts here ?>
		<?php echo $this->Form->create('Ppcbanner',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'ppc','action'=>'purchasepay/'.$Ppcplandata['Ppcplan']['id'])));?>
		<div class="purchaseplanmain">
		<div class="form-box">
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Plan Name");?> : </div>
				<div class="form-col-2 form-text"><?php echo $Ppcplandata['Ppcplan']['plan_name'];?></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Banner Size");?> : </div>
				<div class="form-col-2 form-text"><?php echo $Ppcplandata['Ppcplan']['banner_size'];?></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Total Clicks");?> : </div>
				<div class="form-col-2 form-text"><div id="totalclick"><?php echo $Ppcplandata['Ppcplan']['total_click'];?></div></div>
			</div>
			
			<?php $commissionlevel=explode(",", $Ppcplandata['Ppcplan']['commissionlevel']);if($commissionlevel[0]>0){?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Referral Commission Structure");?> : </div>
				<div class="form-col-2 form-text">
					<?php $commilev="";for($i=0;$i<count($commissionlevel);$i++){if($commissionlevel[$i]>0){$commilev.="Level ".($i+1)." : ".$commissionlevel[$i]."%, ";}else{break;}}echo trim($commilev,", ")?>
				</div>
			</div>
			<?php }?>
			
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Banner Title");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('banner_title', array('type'=>'text','div'=>false, 'label' => false, 'class'=>'formtextbox'));?>
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?>"></span>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Banner URL");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('banner_url', array('type'=>'text', 'label' => false, 'div' => false, 'value' => 'http://', 
				'onblur'=>'if(this.value!==""){document.getElementById("img1").src = this.value;$("#img1").show();}else{$("#img1").hide();}',
				'class'=>'formtextbox'));?>
				<div class="textcenter"><img src="" id="img1" style="display:none;"/></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Destination URL");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('target_url', array('type'=>'text', 'value'=>'http://', 'label' => false, 'class'=>'formtextbox txtframebreaker'));?></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Display Start Date");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('start_date', array('type'=>'text', 'id'=>'fromdate', 'value'=>date('Y-m-d'), 'style'=>'max-width:185px', 'div' => false, 'label' => false, 'class'=>'formtextbox datepickerajax'));?></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Daily Clicks Limit");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('daily_budget', array('type'=>'text', 'label' => false, 'class'=>'formtextbox', 'div' => false));?>
				<span class="helptooltip vtip" title="<?php echo __('Specify the maximum possible clicks per day for this banner. It will not be shown once this number is reached.'); ?>"></span>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Display Area");?> : <span class="required">*</span></div>
				<div class="form-col-2 form-text forradioalignment">
					<?php 
						echo $this->Form->radio('display_area', array('all' => __('All Countries'), 'selected' => __('Selected Countries')), array('value'=>'all', 'legend' => false, 'separator'=>'&nbsp;&nbsp;', 'onchange'=>'if(this.value=="all"){$(".c2").show(500);$(".c1").hide(500);var amount1=($("#priceall").text()/$("#totalclick").text());$(".rate").html("'.$Currency['prefix'].'"+amount1+"'.' '.$Currency['suffix'].'");getpriceamount(0);}else if(this.value=="selected"){$(".c2").hide(500);$(".c1").show(500);var amount1=($("#pricegeo").text()/$("#totalclick").text());$(".rate").html("'.$Currency['prefix'].'"+amount1+"'.' '.$Currency['suffix'].'");getpriceamount(1);}'));
					?>
				</div>
			</div>
			<div class="tabal-content-white  c1" style="display: none;">
				<fieldset>
					<div class="line-height" style="height:250px;overflow:auto;">
						<?php
						$ccounter=0;
						foreach($countries as $id => $val)
						{ ?>
							<div class="float-left-profile line-height" style="min-width:170px;">
								<div class="profile-bot-left line-height" style="padding-top:0px;">
									<?php 
										echo $this->Form->checkbox('country.', array(
										  'value' => trim($val),
										  'class' => '',
										  'div'=>false,
										  'style'=>'display:inline',
										  'hiddenField' => false
										));
									?>
								</div>
								<div class="float-left-profile line-height"><?php echo trim($val); ?></div>
							</div>
						<?php	$ccounter++;
						if($ccounter%3==0)
						{
							echo '<div class="clear-both"></div>';
						}
					 }?>
					</div>
				</fieldset>
			</div>
			<div class="form-row  c2 contact-gray">
				<div class="form-col-1"><?php echo __("Price");?> : </div>
				<div class="form-col-2 form-text">
					<?php echo $Currency['prefix'];?><?php echo round($Ppcplandata['Ppcplan']['all_country_price']*$Currency['rate'],2)." ".$Currency['suffix'];?>
					<span id="couponspanall">
					<?php
						if($this->Session->check('coupon'))
						{
							$sessioncoupon=$this->Session->read('coupon');
							if($sessioncoupon['page']=='ppc' && $sessioncoupon['planid']==$Ppcplandata["Ppcplan"]["id"])
							{
								echo " - <span>".$Currency['prefix'].round($sessioncoupon['amount']*$Currency['rate'],4)."</span> = <span>".$Currency['prefix'].round(($Ppcplandata['Ppcplan']['all_country_price']-$sessioncoupon['amount'])*$Currency['rate'],4)." ".$Currency['suffix']."</span>";
							}
						}
					?>
					</span>
				<div class="height7"></div>
					<div id="priceall" style="display:none"><?php echo $Ppcplandata['Ppcplan']['all_country_price']*$Currency['rate'];?></div>
				</div>
			</div>
			<div class="form-row  c1 contact-gray" style="display: none;">
				<div class="form-col-1"><?php echo __("Price");?> : </div>
				<div class="form-col-2 form-text">
					<?php echo $Currency['prefix'];?><?php echo round($Ppcplandata['Ppcplan']['geo_price']*$Currency['rate'],2)." ".$Currency['suffix'];?>
					<span id="couponspan">
					<?php
						if($this->Session->check('coupon'))
						{
							$sessioncoupon=$this->Session->read('coupon');
							if($sessioncoupon['page']=='ppc' && $sessioncoupon['planid']==$bannerdata['Banneradplan']['id'])
							{
								echo " - <span>".$Currency['prefix'].round($sessioncoupon['amount']*$Currency['rate'],4)." ".$Currency['suffix']."</span> = <span>".$Currency['prefix'].round(($Ppcplandata['Ppcplan']['geo_price']-$sessioncoupon['amount'])*$Currency['rate'],4)." ".$Currency['suffix']."</span>";
							}
						}
					?>
					</span>
				<div class="height7"></div></div>
				<div id="pricegeo" style="display:none"><?php echo $Ppcplandata['Ppcplan']['geo_price']*$Currency['rate'];?></div>
			</div>
			<div class="form-row ">
				<div class="form-col-1"><?php echo __("PPC Rate");?> : </div>
				<div class="form-col-2 form-text rate"><?php echo $Currency['prefix'];?><?php echo round(($Ppcplandata['Ppcplan']['all_country_price']/$Ppcplandata['Ppcplan']['total_click'])*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("How would you like to pay");?> : </div>
				<div class="form-col-2 form-text balance-width">
					
					<?php
						$paymentmethod=array();
						$allmethod=array('cash','repurchase', 'earning', 'commission', 'processor', 'ca:re', 'ca:ea', 'ca:co', 're:ea', 're:co', 'ea:co', 'ca:re:ea', 're:ea:co', 'ea:co:ca', 'co:ca:re', 'ca:ea:re:co');
						$methodarray=@explode(",",$Ppcplandata['Ppcplan']['paymentmethod']);
						
						$find=array('ca', 'ea', 're', 'co', ':');
						$replace=array('Cash Balance', 'Earning Balance', 'Re-purchase Balance', 'Commission Balance', ' + ');
								
						foreach($methodarray as $methodnm)
						{
							if(in_array($methodnm,$allmethod))
							{
								if(!in_array($methodnm,array('cash','repurchase', 'earning', 'commission', 'processor')))
									$methodnmdisp=str_replace($find, $replace, $methodnm);
								elseif($methodnm=='processor')
									$methodnmdisp=ucfirst($methodnm);
								elseif($methodnm=='repurchase')
									$methodnmdisp="Re-purchase Balance";
								else
									$methodnmdisp=ucfirst($methodnm)." Balance";
								$paymentmethod[$methodnm]=__($methodnmdisp);
							}
						}
						//$paymentmethod=array();
						//if(strpos($Ppcplandata['Ppcplan']['paymentmethod'],'cash') !== false)
						//	$paymentmethod['cash']=__('Cash Balance');
						//if(strpos($Ppcplandata['Ppcplan']['paymentmethod'],'repurchase') !== false)
						//	$paymentmethod['repurchase']=__('Re-purchase Balance');
						//if(strpos($Ppcplandata['Ppcplan']['paymentmethod'],'earning') !== false)
						//	$paymentmethod['earning']=__('Earning Balance');
						//if(strpos($Ppcplandata['Ppcplan']['paymentmethod'],'commission') !== false)
						//	$paymentmethod['commission']=__('Commission Balance');
						//if(strpos($Ppcplandata['Ppcplan']['paymentmethod'],'processor') !== false)
						//	$paymentmethod['processor']=__('Payment Processor');
						if($SITECONFIG['balance_type']==1)
						{
							echo $this->Form->radio('paymentmethod', $paymentmethod, array('value'=>'cash', 'legend' => false, 'separator'=>'&nbsp;', 'onchange'=>'if(this.value=="processor") {$(".paymentprocessorfield").show(500);} else{$(".paymentprocessorfield").hide(500);} if(this.value=="processor"){processorextrafield($("#paymentprocessors").val(),processer,"'.$SITEURL.'app/processorextrafield","Ppcbanner",".extrafield");showprocfee(1);}else{$(".extrafield").html("");showprocfee(0);}if(this.value.indexOf("re:")>=0 || this.value.indexOf(":re")>=0) {$(".refirstfield").show(500);} else{$(".refirstfield").hide(500);}'));
						}
						elseif($SITECONFIG['balance_type']==2)
						{
							echo $this->Form->radio('paymentmethod', $paymentmethod, array('value'=>'cash', 'legend' => false, 'separator'=>'&nbsp;', 'onchange'=>'if(this.value=="processor"){processorextrafield($("#paymentprocessors").val(),processer,"'.$SITEURL.'app/processorextrafield","Ppcbanner",".extrafield");showprocfee(1);}else{$(".extrafield").html("");showprocfee(0);}if(this.value.indexOf("re:")>=0 || this.value.indexOf(":re")>=0) {$(".refirstfield").show(500);} else{$(".refirstfield").hide(500);}'));
						}
					?>
					<div class="height7"></div>
					<div class="refirstfield" style="display: none;"><?php echo $this->Form->input('refirst', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>'')); echo __('Use Re-purchase Balance first'); ?></div>
				</div>
			</div>
			
			<div class="form-row paymentprocessorfield" style="display:<?php if($SITECONFIG['balance_type']==1){echo 'none';}?>;">
				<div class="form-col-1"><?php echo __("Please select a payment processor");?> : </div>
					<?php
						$procfee_string = "var procfee_array = new Array();var procfeeamount_array = new Array();";
						foreach($processorsfee as $key=>$value)
						{
							$procfee_string.=" procfee_array[".$key."] = ".$value['fee'].";";
							$procfee_string.=" procfeeamount_array[".$key."] = ".$value['feeamount'].";";
						}
					?>
					<script type='text/javascript'><?php echo $procfee_string;?></script>
					<div class="select-dropdown">
						<label>
							<?php
							echo $this->Form->input('paymentprocessor', array(
								  'type' => 'select',
								  'options' => $paymentprocessors,
								  'selected' => '',
								  'class'=>'paymentprocessor',
								  'label' => false,
								  'div' => false,
								  'id'=>'paymentprocessors',
								  'onchange' => 'if($("#PpcbannerPaymentmethodProcessor").attr("checked")){processorextrafield(this.value,processer,"'.$SITEURL.'app/processorextrafield","Ppcbanner",".extrafield");showprocfee(1);}else{showprocfee(0);}'
							));?>
						</label>
					</div>
					
					<span class="helptooltip vtip" title="<?php echo __('Fees is added to the amount when you pay through payment processor.'); ?>"></span>
			</div>
			<div class='extrafield'></div>
			<div class="height10"></div>
			<div>
				<?php // Amount calculation box starts here ?>
				<div class="purchaseplanamountdetails floatleft" style="width:49.5%">
					<div class="form-box">
						<div class="form-row">
							<div class="form-col-1"><?php echo __("Amount");?> : </div>
							<div class="form-col-2 form-text"><?php echo $Currency['prefix'];?><span id="payableamount">0</span> <?php echo $Currency['suffix'];?></div>
						</div>
						
						<div class="form-row">
							<div class="form-col-1"><?php echo __("Fees");?> : </div>
							<div class="form-col-2 form-text"><span id="procfee">0</span>% + <?php echo $Currency['prefix'];?><span id="procfeeamount">0</span> <?php echo $Currency['suffix'];?></div>
						</div>
						
						<div class="form-row">
							<div class="form-col-1"><?php echo __("Final Amount");?> : </div>
							<div class="form-col-2 form-text"><?php echo $Currency['prefix'];?><span id="finalamount">0</span> <?php echo $Currency['suffix'];?></div>
						</div>
					</div>
					<script type="text/javascript">getpriceamount(0);</script>
				</div>
				<?php // Amount calculation box ends here ?>
				<div class="purchaseplanbuttonbox floatright" style="width:50%;">
					<div class="form-box">
						<div class="form-row captchrow">
							<div class="profile-bot-left inlineblock" style="padding-left: 0px;padding-top: 7px;">
								<?php if($Ppcplandata["Ppcplan"]["iscoupon"]==1){?>
								<div class="form-col-1 forcaptchaonly"><?php echo __('Enter Coupon Code'); ?> : &nbsp;</div>
								<?php $couponclass='formtextbox coupontextbox';
								if(isset($sessioncoupon) && $sessioncoupon['page']=='ppc' && $sessioncoupon['planid']==$Ppcplandata["Ppcplan"]["id"]) $couponclass.=' greenbg'; else $sessioncoupon['code']='';
								echo $this->Form->input('code', array('type'=>'text', 'div'=>false, 'label' => false, 'style'=>'width:45%;', 'class'=>$couponclass, 'value'=>$sessioncoupon['code']));?>
								<?php echo $this->Js->submit(__('Apply Coupon'), array(
									'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'update'=>'#UpdateMessage',
									'class'=>'button floatnone',
									'div'=>false,
									'controller'=>'ppc',
									'action'=>'coupon/'.$Ppcplandata['Ppcplan']['id'],
									'url'   => array('controller' => 'ppc', 'action' => 'coupon/'.$Ppcplandata['Ppcplan']['id'])
								));?>
								<?php }?>
							</div>
							<div class="profile-bot-left inlineblock" style="font-size: 12px;"><?php if($SITECONFIG["enableagree"]==1){echo $this->Form->input('termsandconditions', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>'')); echo ' <a href="'.$SITEURL.'public/terms" target="_blank">'.__('I agree with the terms and conditions').'</a> '.__('of').' '.$SITECONFIG["sitetitle"];} ?></div>
							<div class="clear-both"></div>
						</div>
						
						<div class="formbutton">
							
							<input type="submit" value="<?php echo __('Purchase'); ?>" class="button ml10" onclick="return checkframebreaker(0,'<?php echo $SITEURL; ?>','framebreaker');" />
							<?php echo $this->Js->submit(__('Purchase'), array(
								'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'update'=>'#UpdateMessage',
								'class'=>'button framebreaker',
								'style'=>'display:none',
								'div'=>false,
								'controller'=>'ppc',
								'action'=>'purchasepay',
								'url'   => array('controller' => 'ppc', 'action' => 'purchasepay/'.$Ppcplandata['Ppcplan']['id'])
							));?>
							<?php echo $this->Js->link(__("Back"), array('controller'=>'ppc', "action"=>"index"), array(
								'update'=>'#Ppcpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'button',
								'style'=>''
							));?>
						</div>
						<div class="clear-both"></div>
					</div>
				</div>
				<div class="clear-both"></div>
			</div>
		</div>
	</div>
		<?php echo $this->Form->end();?>
		<?php // PPC purchase form ends here ?>
		
	</div>
<?php if(!$ajax) { ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>