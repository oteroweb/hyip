<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>		
<?php if(!$ajax){?>
<div class="whitetitlebox">Pay Per Click</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("PPC Banners", array('controller'=>'ppc', "action"=>"member"), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("PPC Plans", array('controller'=>'ppc', "action"=>"plan"), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Banner Widget", array('controller'=>'ppc', "action"=>"bannerwidget"), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">		
<div id="ppcpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Pay_Per_Click_-_PPC#PPC_Plans" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="serchmainbox">
   <div class="serchgreybox">
       Search Option
   </div>
   <?php echo $this->Form->create('Ppcplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ppc','action'=>'plan')));?>
   <div class="from-box">
        <div class="fromboxmain">
             <span>Search By :</span>
             <span>                     
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
							<?php 
							echo $this->Form->input('searchby', array(
								'type' => 'select',
								'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'plan_name'=>'Plan Name', 'banner_size'=>'Banner Size', 'total_click'=>'Clicks',  'all_country_price'=>'All Countries Price', 'geo_price'=>'GEO(Selected Countries) Price'),
								'selected' => $searchby,
								'class'=>'',
								'label' => false,
								'style' => '',
								'onchange'=>'if($(this).val()=="banner_size"){$(".searchfor_all").hide();$(".searchfor_size").show(500);}else{$(".searchfor_size").hide();$(".searchfor_all").show(500);}'
							));
							?>
						</label>
					</div>
				</div>
             </span>
		</div>
        <div class="fromboxmain">
				<span>Search For :</span>
				<span class='searchfor_all' style='display:<?php if($searchby=="banner_size"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?></span>
				<span class='searchfor_size' style='display:<?php if($searchby!="banner_size"){ echo "none";} ?>'>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
						<?php 
						echo $this->Form->input('banner_size', array(
							  'type' => 'select',
							  'options' => array('125x125'=>'125x125', '468x60'=>'468x60', '728x90'=>'728x90'),
							  'selected' => $searchfor,
							  'class'=>'',
							  'label' => false,
							  'style' => ''
						));
						?>
						</label>
					</div>
				</div>
				</span>				
				<span class="padding-left">
					<?php echo $this->Js->submit('', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#ppcpage',
						'class'=>'searchbtn',
						'controller'=>'ppc',
						'action'=>'plan',
						'url'=> array('controller' => 'ppc', 'action' => 'plan')
					  ));?>
				</span>
        </div>
    </div>
    <?php echo $this->Form->end();?>
</div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#ppcpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'ppc', 'action'=>'plan')
	));
	$currentpagenumber=$this->params['paging']['Ppcplan']['page'];
	?>
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('ppc',$SubadminAccessArray) || in_array('ppc/adminpanel_planadd', $SubadminAccessArray)){ ?>
	<?php echo $this->Js->link("+ Add New", array('controller'=>'ppc', "action"=>"planadd"), array(
			'update'=>'#ppcpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btnorange'
	));?>
	<?php } ?>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Ppcplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ppc','action'=>'plan')));?>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'ppc', "action"=>"plan/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Plan Name', array('controller'=>'ppc', "action"=>"plan/0/0/plan_name/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Name'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Banner Size', array('controller'=>'ppc', "action"=>"plan/0/0/banner_size/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Banner Size'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Clicks', array('controller'=>'ppc', "action"=>"plan/0/0/total_click/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Clicks'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('All Countries Price', array('controller'=>'ppc', "action"=>"plan/0/0/all_country_price/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By All Countries Price'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('GEO Price', array('controller'=>'ppc', "action"=>"plan/0/0/geo_price/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By GEO Price'
					));?>
				</div>
				<div><?php echo 'Action';?></div>
           </div>
			<?php foreach ($ppcplandata as $ppcplan): ?>
				<div class="tablegridrow">
					<div><?php echo $ppcplan['Ppcplan']['id']; ?></div>
					<div><?php echo $ppcplan['Ppcplan']['plan_name']; ?></div>
					<div><?php echo $ppcplan['Ppcplan']['banner_size']; ?></div>
					<div><?php echo $ppcplan['Ppcplan']['total_click']; ?></div>
					<div>$<?php echo $ppcplan['Ppcplan']['all_country_price']; ?></div>
					<div>$<?php echo $ppcplan['Ppcplan']['geo_price']; ?></div>
					<div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							
							<?php if(!isset($SubadminAccessArray) || in_array('ppc',$SubadminAccessArray) || in_array('ppc/adminpanel_planadd/$', $SubadminAccessArray)){ ?>
							<li>
								<?php
									echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit PPC Plan')).' Edit PPC Plan', array('controller'=>'ppc', "action"=>"planadd/".$ppcplan['Ppcplan']['id']), array(
										'update'=>'#ppcpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('ppc',$SubadminAccessArray) || in_array('ppc/adminpanel_planstatus', $SubadminAccessArray)){ ?>
							<li>
								<?php 
									if($ppcplan['Ppcplan']['status']==0){
										$statusaction='1';
										$statusicon='red-icon.png';
										$statustext='Activate PPC Plan';
									}else{
										$statusaction='0';
										$statusicon='blue-icon.png';
										$statustext='Inactivate PPC Plan';}
									echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'ppc', "action"=>"planstatus/".$statusaction."/".$ppcplan['Ppcplan']['id']."/".$currentpagenumber), array(
										'update'=>'#ppcpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<?php } ?>
							<?php if(!isset($SubadminAccessArray) || in_array('ppc',$SubadminAccessArray) || in_array('ppc/adminpanel_member', $SubadminAccessArray)){ ?>
							<li>
								<?php 
									echo $this->Js->link($this->html->image('view-members.png', array('alt'=>'View PPC Plan Members')).' View PPC Plan Members', array('controller'=>'ppc', "action"=>"member/".$ppcplan['Ppcplan']['id']), array(
										'update'=>'#ppcpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));
								?>
							</li>
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('ppc',$SubadminAccessArray) || in_array('ppc/adminpanel_planremove', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete PPC Plan')).' Delete PPC Plan', array('controller'=>'ppc', "action"=>"planremove/".$ppcplan['Ppcplan']['id']."/".$currentpagenumber), array(
									'update'=>'#ppcpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'confirm'=>"Do You Really Want To Delete This PPC Plan?"
								));?>
							</li>
							<?php } ?>
						  </ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($ppcplandata)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Ppcplan']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Ppcplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'ppc','action'=>'plan/0/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#ppcpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'ppc',
			  'action'=>'plan/0/rpp',
			  'url'   => array('controller' => 'ppc', 'action' => 'plan/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>	
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#ppcpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>