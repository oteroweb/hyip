<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Pay Per Click</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("PPC Banners", array('controller'=>'ppc', "action"=>"member"), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC Plans", array('controller'=>'ppc', "action"=>"plan"), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Banner Widget", array('controller'=>'ppc', "action"=>"bannerwidget"), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">
<div id="ppcpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Pay_Per_Click_-_PPC#Banner_Widget" target="_blank">Help</a></div>
</div>
<div class="backgroundwhite">
	
			<div class="addnew-button">
				<?php echo $this->Js->link("Regenerate Code", array('controller'=>'ppc', "action"=>"bannerwidget"), array(
					'update'=>'#ppcpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btnorange'
				));?>
			</div>
			<div class="clear-both"></div>
			
<?php $widgetdivid=date("ihYdms");?>
<script type="text/javascript">
function showwidgetcode(chk,size,only)
{
		document.getElementById("bannerwidget").innerHTML='&lt;div id="widget_<?php echo $widgetdivid;?>"&gt;&lt;script type="text/javascript" src="<?php echo $SITEURL;?>widget/ppc/'+size+'/widget_<?php echo $widgetdivid;?>"&gt;&lt;/script>&lt;/div&gt;';
		document.getElementById("current").value=size;
		
		var SRC = document.getElementById("widgetimage").src.split('/');
		if(size==125)
		{
			var STRSRC = document.getElementById("widgetimage").src.replace(SRC[SRC.length-1], '125_widget2.jpg');
			document.getElementById("widgetimage").src=STRSRC;
		}
		else if(size==468)
		{
			var STRSRC = document.getElementById("widgetimage").src.replace(SRC[SRC.length-1], '468_widget2.jpg');
			document.getElementById("widgetimage").src=STRSRC;
		}
		else
		{
			var STRSRC = document.getElementById("widgetimage").src.replace(SRC[SRC.length-1], '728_widget.jpg');
			document.getElementById("widgetimage").src=STRSRC;
		}
}
</script>
<form name="form3" id="form3" method="post" >
	<div class="frommain">
		<div class="textmain">
			<div class="nameandbox"><?php echo 'Banner Size';?> :</div>
			<div class="rediobtn">
				<div class="retbtn">
					<div class="rediobox">
					 
						<input id="widget125" type="radio" name="btype" onclick="showwidgetcode(this.form.theme,125,0);" checked="checked" />
						<label for="widget125"> <?php echo '125x125';?></label>
						
						<input id="widget468" type="radio" name="btype" onclick="showwidgetcode(this.form.theme,468,0);" />
						<label for="widget468"> <?php echo '468x60';?></label>
						
						<input id="widget728" type="radio" name="btype" onclick="showwidgetcode(this.form.theme,728,0);" />
						<label for="widget728"> <?php echo '728x90';?></label>
						
						<input type="hidden" name="current" id="current" value="125">
						<!--&nbsp;<input type="checkbox" name="theme" onclick="showwidgetcode(this,this.form.current.value,1);" /> <?php echo 'Without powered by notice';?>-->
					</div>
				</div>
			</div>
			
		</div>
		<div class="nameandbox">
			<?php echo $this->html->image("125_widget2.jpg", array("alt"=>"", "id"=>"widgetimage"));?>
		</div>
		<div class="nameandbox">
				<textarea id="bannerwidget" class="from-textarea" style="padding: 10px 10px;">&lt;div id="widget_<?php echo $widgetdivid;?>"&gt;&lt;script type="text/javascript" src="<?php echo $SITEURL;?>widget/ppc/125/widget_<?php echo $widgetdivid;?>"&gt;&lt;/script&gt;&lt;/div&gt;</textarea>
		</div>
	</div>
</form>

</div>
<?php if(!$ajax){?>
</div><!--#balancetransferpage over-->
</div>
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>