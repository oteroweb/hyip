<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Pay Per Click</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("PPC Plans", array('controller'=>'ppc', "action"=>"plan"), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC Banners", array('controller'=>'ppc', "action"=>"member"), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Banner Widget", array('controller'=>'ppc', "action"=>"bannerwidget"), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">		
<div id="ppcpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Pay_Per_Click_-_PPC#PPC_Plans" target="_blank">Help</a></div>
<div id="UpdateMessage" class="UpdateMessage"></div>
<?php echo $this->Form->create('Ppcplan',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'ppc','action'=>'planaction')));?>
	<?php if(isset($ppcplandata['Ppcplan']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$ppcplandata['Ppcplan']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
	<div class="frommain">
	
		<div class="fromnewtext">Status : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('status', array(
						'type' => 'select',
						'options' => array('1'=>'Active', '0'=>'Inactive'),
						'selected' => $ppcplandata['Ppcplan']["status"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => '',
					));
				?>
			  </label>
		  </div>
		</div>
		
		
		<div class="fromnewtext">Hide Plan ? : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('ishide', array(
					  'type' => 'select',
					  'options' => array('0'=>'No', '1'=>'Yes'),
					  'selected' => $ppcplandata['Ppcplan']["ishide"],
					  'class'=>'',
					  'label' => false,
					  'div' => false,
					  'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		
		<div class="fromnewtext">Allow Coupons on Purchase :  </div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>	
				<?php 
				  echo $this->Form->input('iscoupon', array(
					  'type' => 'select',
					  'options' => array('1'=>'Yes', '0'=>'No'),
					  'selected' => $ppcplandata['Ppcplan']["iscoupon"],
					  'class'=>'',
					  'label' => false,
					  'div' => false,
					  'style' => ''
				  ));
				?>
				</label>
			</div>
		</div>
		
		<div class="fromnewtext">Banner Size : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('banner_size', array(
						'type' => 'select',
						'options' => array('125x125'=>'125X125', '468x60'=>'468X60', '728x90'=>'728X90'),
						'selected' => $ppcplandata['Ppcplan']["banner_size"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => '',
					));
				?>
			  </label>
		  </div>
		</div>
		
		
		<div class="fromnewtext">Plan Name :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('plan_name', array('type'=>'text', 'value'=>$ppcplandata['Ppcplan']["plan_name"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Number of Clicks :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('total_click', array('type'=>'text', 'value'=>$ppcplandata['Ppcplan']["total_click"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">All Countries Price ($) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('all_country_price', array('type'=>'text', 'value'=>$ppcplandata['Ppcplan']["all_country_price"], 'label' => false, 'div'=>false, 'onblur'=>'var amount1=(this.value/$("#PpcplanTotalClick").val());$(".allrate").html("$"+amount1);', 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">GEO(Selected Countries) Price ($) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('geo_price', array('type'=>'text', 'id'=>'startdate', 'value'=>$ppcplandata['Ppcplan']["geo_price"], 'label' => false, 'div'=>false, 'onblur'=>'var amount1=(this.value/$("#PpcplanTotalClick").val());$(".georate").html("$"+amount1);', 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Calculated PPC Rate For All Countries ($) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown4">
			<span class="allrate"><?php echo '$'.$ppcplandata['Ppcplan']["all_country_price"]/$ppcplandata['Ppcplan']["total_click"]; ?></span>
		</div>
		
		<div class="fromnewtext">Calculated PPC Rate For GEO ($) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown4">
			<span class="georate"><?php echo '$'.$ppcplandata['Ppcplan']["geo_price"]/$ppcplandata['Ppcplan']["total_click"]; ?></span>
		</div>
		
		<?php $displayrevenue=''; if(strpos($SITECONFIG['modules'], ":module3:1")===false){$displayrevenue='display:none;';}?>
		<div style="<?php echo $displayrevenue; ?>" class="fromnewtext">Revenue Share (%):<span class="red-color">*</span> </div>
		<div style="<?php echo $displayrevenue; ?>" class="fromborderdropedown3">
			<?php echo $this->Form->input('revshare_rate', array('type'=>'text', 'value'=>$ppcplandata['Ppcplan']["revshare_rate"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Pay Commission For The First Purchase Only : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('fpcommission', array(
						'type' => 'select',
						'options' => array('1'=>'Yes', '0'=>'No'),
						'selected' => $ppcplandata['Ppcplan']["fpcommission"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		
		
		<div class="fromnewtext">Referral Commission Re-purchase/Compound Strategy (%) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('commrepurchase', array('type'=>'text', 'value'=>$ppcplandata['Ppcplan']["commrepurchase"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<?php @$commissionlevel=explode(',',$ppcplandata['Ppcplan']["commissionlevel"]); ?>
		<div class="fromnewtext">Referral Commission(%) :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<div class="fromborder4left">
				<div class="levalwhitfromtext">Level 1 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 2 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 3 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 4 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 5 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 6 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 7 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[6], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 8 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[7], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 9 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[8], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
				<div class="levalwhitfromtext">Level 10 :</div>
				<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[9], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
			</div>
			<div class="tooltipfronright">
				
			</div>
			<div class="clearboth"></div>
		</div>
		
		<div <?php if($SITECONFIG['balance_type']==1){echo 'style="display: none"';}?>>
		<div class="fromnewtext">Earnings Processor Preference : </div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
				<?php 
					echo $this->Form->input('earning_on', array(
						'type' => 'select',
						'options' => array('0'=>'Purchase Processor', '1'=>"Proirity Processor"),
						'selected' => $ppcplandata['Ppcplan']["earning_on"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
				</label>
			</div>
		</div>
		
		</div>

		<div class="fromnewtext">Payment Method :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php
			$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning', 'commission'=>'Commission', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase', 'ca:ea'=>'Cash + Earning', 'ca:co'=>'Cash + Commission', 're:ea'=>'Re-purchase + Earning', 're:co'=>'Re-purchase + Commission', 'ea:co'=>'Earning + Commission', 'ca:re:ea'=>'Cash + Re-purchase + Earning', 're:ea:co'=>'Re-purchase + Earning + Commission', 'ea:co:ca'=>'Earning + Commission + Cash', 'co:ca:re'=>'Commission + Cash + Re-purchase', 'ca:ea:re:co'=>'Cash + Re-purchase + Earning + Commission');
			if($SITECONFIG["wallet_for_commission"] == 'cash' && $SITECONFIG["wallet_for_earning"] == 'cash')
			{
				$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase');
			}
			elseif($SITECONFIG["wallet_for_earning"] == 'cash')
			{
				$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'commission'=>'Commission', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase', 'ca:co'=>'Cash + Commission', 're:co'=>'Re-purchase + Commission', 'co:ca:re'=>'Commission + Cash + Re-purchase');
			}
			elseif($SITECONFIG["wallet_for_commission"] == 'cash')
			{
				$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase', 'ca:ea'=>'Cash + Earning', 're:ea'=>'Re-purchase + Earning', 'ca:re:ea'=>'Cash + Re-purchase + Earning');
			}
			$selected = @explode(",",$ppcplandata['Ppcplan']["paymentmethod"]);
			echo $this->Form->input('paymentmethod', array('type' => 'select', 'div'=>false, 'label'=>false, 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $pmethod));?>
		</div>
		
		
		<div class="fromnewtext">Allowed Payment Processors :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php 
			$selected = @explode(",",$ppcplandata['Ppcplan']["paymentprocessors"]);
			echo $this->Form->input('paymentprocessors', array('type' => 'select', 'div'=>false, 'label'=>false, 'class'=>'', 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $paymentprocessors));?>
		</div>
		
		
		<?php if(isset($ppcplandata['Ppcplan']["id"])){?>
		<div class="fromnewtext">Link : </div>
		<div class="fromborderdropedown3">
			<input type="text" class="fromboxbg" value="<?php echo $SITEURL;?>ppc/purchase/<?php echo $ppcplandata['Ppcplan']["id"];?>" />
		</div>
		
		<?php }?>
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'btnorange',
				'div'=>false,
				'controller'=>'ppc',
				'action'=>'planaction',
				'url'   => array('controller' => 'ppc', 'action' => 'planaction')
			  ));?>
			  
			  <?php echo $this->Js->link("Back", array('controller'=>'ppc', "action"=>"plan"), array(
				  'update'=>'#ppcpage',
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'escape'=>false,
				  'class'=>'btngray'
			  ));?>
		</div>
	</div>
<?php echo $this->Form->end();?>
<?php if(!$ajax){?>
</div><!--#balancetransferpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>