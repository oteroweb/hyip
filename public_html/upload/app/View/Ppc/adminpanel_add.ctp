<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Pay Per Click</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("PPC Plans", array('controller'=>'ppc', "action"=>"plan"), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("PPC Banners", array('controller'=>'ppc', "action"=>"member"), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Banner Widget", array('controller'=>'ppc', "action"=>"bannerwidget"), array(
						'update'=>'#ppcpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
	</div>
</div>
<div class="tab-content">
<div id="ppcpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Pay_Per_Click_-_PPC#PPC_Banners" target="_blank">Help</a></div>
<div id="UpdateMessage" class="UpdateMessage"></div>
<?php echo $this->Form->create('Ppcbanner',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'ppc','action'=>'addaction')));?>
	<?php if(isset($ppcbannerdata['Ppcbanner']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$ppcbannerdata['Ppcbanner']["id"], 'label' => false));
		echo $this->Form->input('clicklimit', array('type'=>'hidden', 'value'=>$ppcbannerdata['Ppcbanner']["total_click"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
	
	<div class="frommain">
	
		<div class="fromnewtext">Status : </div>
		<div class="fromborderdropedown">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('status', array(
						'type' => 'select',
						'options' => array('1'=>'Active', '0'=>'Inactive'),
						'selected' => $ppcbannerdata['Ppcbanner']["status"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		<span data-original-title="If Set to 'Active', The Plan Will Be Displayed on Member Side When Purchasing PPC Plans." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="fromnewtext">Pause : </div>
		<div class="fromborderdropedown">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('pause', array(
						'type' => 'select',
						'options' => array('1'=>'Unpause', '0'=>'Pause'),
						'selected' => $ppcbannerdata['Ppcbanner']["pause"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		<span data-original-title="You Can Pause The Banner Temporarily At Your Will." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="fromnewtext">Banner Size : </div>
		<div class="fromborderdropedown">
		  <div class="select-main">
			  <label>
				<?php 
					echo $this->Form->input('banner_size', array(
						'type' => 'select',
						'options' => array('125x125'=>'125X125', '468x60'=>'468X60', '728x90'=>'728X90'),
						'selected' => $ppcbannerdata['Ppcbanner']["banner_size"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
				?>
			  </label>
		  </div>
		</div>
		<span data-original-title="Select The Size of The Banner." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="fromnewtext">Title :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('banner_title', array('type'=>'text', 'value'=>$ppcbannerdata['Ppcbanner']["banner_title"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Banner URL :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('banner_url', array('type'=>'text', 'value'=>$ppcbannerdata['Ppcbanner']["banner_url"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Destination URL :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('target_url', array('type'=>'text', 'value'=>$ppcbannerdata['Ppcbanner']["target_url"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Daily Click Limit :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('daily_budget', array('type'=>'text', 'id'=>'startdate', 'value'=>$ppcbannerdata['Ppcbanner']["daily_budget"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="fromnewtext">Display Area : </div>
		<div class="fromborderdropedown3">
		  <div class="select-main">
			  <label>
				<?php 
					if($ppcbannerdata['Ppcbanner']["display_area"]=='all'){$display_area='all';}else{$display_area='country';}
					echo $this->Form->input('display_area', array(
						'type' => 'select',
						'options' => array('all'=>'All Countries', 'country'=>'Selected Countries'),
						'selected' => $display_area,
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => '',
						'onchange' => 'if(this.selectedIndex!=0){$(".countryfields").show(500);}else{$(".countryfields").hide(500);}'
					));
				?>
			  </label>
		  </div>
		</div>
		
		<div class="countryfields" style="display:<?php if($ppcbannerdata['Ppcbanner']["display_area"]!='all'){echo '';}else{echo 'none';}?>;">
			<div class="fromnewtext">Country : </div>
			<div class="fromborderdropedown3 checkboxlist" style="height:250px;overflow:auto;">
				<?php
				$ccounter=0;
				$replacecountry=array('India','Dominica','Guinea','Mali','Netherlands','Niger','Oman','Samoa','Curacao');
				$country=array('India_2','Dominica_2','Guinea_2','Mali_2','Netherlands_2','Niger_2','Oman_2','Samoa_2','Netherlands Antilles');
				$display_area=str_replace($country,$replacecountry,$ppcbannerdata['Ppcbanner']["display_area"]);
				$countrynames=@explode(",", $display_area);
				
				foreach($countries as $cid=>$cname)
				{
					if(in_array(trim($cname), $countrynames)){$selected = 'checked="checked"';}else{ $selected = '';}
					echo '<div class="checkbox"><input id="chk'.$ccounter.'" type="checkbox" name="country[]" value="'.trim($cname).'" '.$selected.' /> <label for="chk'.$ccounter.'">'.trim($cname).'</label></div>';
					$ccounter++;
				}
				?>
			</div>
			
		</div>
		
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'btnorange',
				'div'=>false,
				'controller'=>'ppc',
				'action'=>'addaction',
				'url'   => array('controller' => 'ppc', 'action' => 'addaction')
			  ));?>
			  
			  <?php echo $this->Js->link("Back", array('controller'=>'ppc', "action"=>"member/".$planid), array(
				  'update'=>'#ppcpage',
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'escape'=>false,
				  'class'=>'btngray'
			  ));?>
		</div>
	</div>
<?php echo $this->Form->end();?>
<?php if(!$ajax){?>
</div><!--#balancetransferpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>