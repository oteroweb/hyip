<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 12-12-2014
  * Last Modified: 12-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Advertisement</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Banner Ads", array('controller'=>'sitesetting', "action"=>"advertisementcredit"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Text Ads", array('controller'=>'sitesetting', "action"=>"advertisementtextadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC Text Ads", array('controller'=>'sitesetting', "action"=>"ppctextadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Solo Ads", array('controller'=>'sitesetting', "action"=>"soload"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Login Ads", array('controller'=>'sitesetting', "action"=>"loginadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC", array('controller'=>'sitesetting', "action"=>"ppcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC", array('controller'=>'sitesetting', "action"=>"ptcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li >
					<?php echo $this->Js->link("Biz Directory", array('controller'=>'sitesetting', "action"=>"bizdirectorysetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Traffic Exchange", array('controller'=>'sitesetting', "action"=>"trafficsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Surfing Ads Settings", array('controller'=>'sitesetting', "action"=>"surfingsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Widgets", array('controller'=>'sitesetting', "action"=>"advertisementbannerwidget"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div id="UpdateMessage" class="UpdateMessage"></div>
<div class="backgroundwhite">
    
<?php echo $this->Form->create('Sitesetting',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'surfingsettingaction')));?>
		
			<div class="frommain">
				  
				   <div class="Trafficsurfing">
						<div class="helpicon"><a href="https://www.proxscripts.com/docs/Advertisement_Settings#Surfing_Ads_Settings" target="_blank">Help</a></div>
						<div class="fromnewtext ">Enable Surfing :  </div>
						<div class="fromborderdropedown3 ">
							<div class="select-main">
								<label>
								<?php 
									echo $this->Form->input('enablesurfing', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $enablesurfing,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => '',
										 'onchange'=>'if($(this).val()==1){$(".enadis2").show(500);}else{$(".enadis2").hide(500);}' 
									));
								?>
								</label>
							</div>
						</div>
						
						<div class="enadis2" style="<?php if($enablesurfing==0){ echo 'display:none;';} ?>">
						<div class="fromnewtext ">Select Surfing :  </div>
						<div class="fromborderdropedown3 ">
							<div class="select-main">
								<label>
								<?php 
									echo $this->Form->input('surfselect', array(
										'type' => 'select',
										'options' => array('1'=>'Biz Directory', '0'=>'Traffic Exchange'),
										'selected' => $surfselect,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => '',
									));
								?>
								</label>
							</div>
						</div>
						
						<div class="fromnewtext ">Minimum Number Of Ads To Surf To Get Earnings :<span class="red-color">*</span>  </div>
						<div class="fromborderdropedown3 ">
							<?php echo $this->Form->input('minimumsurfads', array('type'=>'text', 'value'=>$minimumsurfads, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
						</div>
						
						  <div class="fromnewtext ">Next Surf Hours :<span class="red-color">*</span>  </div>
						<div class="fromborderdropedown3 ">
							<?php echo $this->Form->input('hourssurfads', array('type'=>'text', 'value'=>$hourssurfads, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
						</div>
						
						<div class="fromnewtext ">Surf Time Reset : </div>
							    <div class="fromborderdropedown3 ">
							    <div class="select-main">
								    <label>
								    <?php 
									      echo $this->Form->input('resetsurftime', array(
										      'type' => 'select',
										      'options' => array('0'=>'Add Next Surf Hours After Current Date', '1'=>'Add Next Surf Hours After Last Surf Date'),
										      'selected' => $resetsurftime,
										      'class'=>'',
										      'label' => false,
										      'div' => false,
										      'style' => ''
									      ));
								    ?>
								    </label>
							    </div>
						    </div>
						
					</div>
				  </div>
						<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_surfingsettingaction', $SubadminAccessArray)){ ?>
							<div class="formbutton">
								<!--<input type="button" onclick="$('#manuDirectory, #manuSurfFreePlan, #manuEnableSurfing').removeClass('active');$('#manuSurfFreePlan').addClass('active'); $('.SurfFreePlan').show(500);$('.BizDirectory,.EnableSurfing').hide();" value="< Back" class="btngray"/> -->
								<?php echo $this->Js->submit('Submit', array(
									'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'update'=>'#UpdateMessage',
									'class'=>'btnorange',
									'div'=>false,
									'controller'=>'sitesetting',
									'action'=>'surfingsettingaction',
									'url'   => array('controller' => 'sitesetting', 'action' => 'surfingsettingaction')
								));?>
							</div>
						<?php } ?>
					
			</div>
		
<?php echo $this->Form->end();?>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>