<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 12-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Advertisement</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Banner Ads", array('controller'=>'sitesetting', "action"=>"advertisementcredit"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Text Ads", array('controller'=>'sitesetting', "action"=>"advertisementtextadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC Text Ads", array('controller'=>'sitesetting', "action"=>"ppctextadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Solo Ads", array('controller'=>'sitesetting', "action"=>"soload"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Login Ads", array('controller'=>'sitesetting', "action"=>"loginadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC", array('controller'=>'sitesetting', "action"=>"ppcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC", array('controller'=>'sitesetting', "action"=>"ptcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Biz Directory", array('controller'=>'sitesetting', "action"=>"bizdirectorysetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Traffic Exchange", array('controller'=>'sitesetting', "action"=>"trafficsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Surfing Ads Settings", array('controller'=>'sitesetting', "action"=>"surfingsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Widgets", array('controller'=>'sitesetting', "action"=>"advertisementbannerwidget"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Advertisement_Settings#Login_Ads" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="backgroundwhite">
    
	<?php echo $this->Form->create('Sitesetting',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'loginadsettingaction')));?>
		
			<div class="frommain">
				<div class="fromnewtext">Enable Login Ads :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
						  echo $this->Form->input('isenable', array(
							  'type' => 'select',
							  'options' => array('1'=>'Yes', '0'=>'No'),
							  'selected' => $isenable,
							  'class'=>'',
							  'label' => false,
							  'div' => false,
							  'style' => '',
							  'onchange'=>'if($(this).val()==1){$(".enadis").show(500);}else{$(".enadis").hide(500);}'
						  ));
						?>
						</label>
					</div>
				</div>
				
				  <div class="enadis" style="<?php if($isenable==0){ echo 'display:none;';} ?>">
				<div class="fromnewtext">Auto Approval :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
						  echo $this->Form->input('isapprove', array(
							  'type' => 'select',
							  'options' => array('0'=>'Yes', '1'=>'No'),
							  'selected' => $isapprove,
							  'class'=>'',
							  'label' => false,
							  'div' => false,
							  'style' => ''
						  ));
						?>
						</label>
					</div>
				</div>
				
				<div class="fromnewtext">Auto Approval on Edit :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
						  echo $this->Form->input('iseditapprove', array(
							  'type' => 'select',
							  'options' => array('0'=>'Yes', '1'=>'No'),
							  'selected' => $iseditapprove,
							  'class'=>'',
							  'label' => false,
							  'div' => false,
							  'style' => ''
						  ));
						?>
						</label>
					</div>
				</div>
				
                                
                <div class="fromnewtext">Maximum Login Ad Purchase Per Day : <span class="red-color">*</span> </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('maxlimitparday', array('type'=>'text', 'value'=>$maxlimitparday, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
				                                
				<div class="fromnewtext">Seconds to Wait Before Login Ad Close Button Appears : <span class="red-color">*</span> </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('secondswait', array('type'=>'text', 'value'=>$secondswait, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
				

				<div class="fromnewtext">Login Ads Counter Starting Time :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
						  echo $this->Form->input('counterstarting', array(
							  'type' => 'select',
							  'options' => array('1'=>'Site load', '0'=>'Page load'),
							  'selected' => $counterstarting,
							  'class'=>'',
							  'label' => false,
							  'div' => false,
							  'style' => '',
							  'onchange' => 'if(this.selectedIndex==0){$(".c1").show(500);}else{$(".c1").hide(500);}'
						  ));
						?>
						</label>
					</div>
				</div>
				

				<div <?php echo ($counterstarting==0)?'style="display: none"':"" ?> class="c1">
					<div class="fromnewtext">Max Seconds to Wait For Site Load : <span class="red-color">*</span> </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('sitelodesecondswait', array('type'=>'text', 'value'=>$sitelodesecondswait, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					
				</div>
			</div>
				<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_loginadsettingaction',$SubadminAccessArray)){ ?>
					<div class="formbutton">
						<?php echo $this->Js->submit('Submit', array(
						  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage',
						  'class'=>'btnorange',
						  'div'=>false,
						  'controller'=>'sitesetting',
						  'action'=>'loginadsettingaction',
						  'url'   => array('controller' => 'sitesetting', 'action' => 'loginadsettingaction')
						));?>
					</div>
				<?php } ?>
			</div>
		
<?php echo $this->Form->end();?>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>