<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Email</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("SMTP Settings", array('controller'=>'sitesetting', "action"=>"smtp"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Auto Responder", array('controller'=>'sitesetting', "action"=>"autoresponder"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Account Activation Mail", array('controller'=>'sitesetting', "action"=>"activationautomail"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Unpaid Member Notification", array('controller'=>'sitesetting', "action"=>"unpaidmembernotification"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Inactive Member Mail", array('controller'=>'sitesetting', "action"=>"inactivemembermail"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Membership Template Emails", array('controller'=>'sitesetting', "action"=>"membershipemailtemplate"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
	

<div class="helpicon"><a href="https://www.proxscripts.com/docs/Email_Settings#Account_Activation_Mail" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
    	<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'activationautomailupdate')));?>
		<?php echo $this->form->input('id', array('type'=>'hidden','value'=>'1'));?>
		<?php echo $this->Form->input('template_type', array('type'=>'hidden', 'value'=>'email_confirmation', 'label' => false));?>

				<div class="frommain">
					<div class="fromnewtext">Status :</div>
					<div class="fromborderdropedown3">
					  <div class="select-main">
						  <label>
						  <?php 
						  echo $this->Form->input('status', array(
							  'type' => 'select',
							  'options' => array('1'=>'Active', '0'=>'Inactive'),
							  'selected' => $inactiveemaildata['Inactiveemail']["status"],
							  'class'=>'',
							  'label' => false,
							  'style' => ''
						  ));
						  ?>
						  </label>
					  </div>
					</div>
					<div class="fromnewtext">Day :<span class="red-color">*</span></div>
					<div class="fromborderdropedown3">
					  <?php echo $this->Form->input('days', array('type'=>'text', 'value'=>$inactiveemaildata['Inactiveemail']["days"], 'label' => false, 'div'=>false, 'class'=>'fromboxbg'));?>
					</div>
					<div class="fromnewtext">Subject : <span class="red-color">*</span> </div>
					<div class="fromborderdropedown3">
						  <?php echo $this->Form->input('subject', array('type'=>'text', 'value'=>stripslashes($inactiveemaildata['Inactiveemail']["subject"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					<div class="fromnewtext">Message : <span class="red-color">*</span></div>
					<div class="fromborderdropedown3">
						   <?php echo $this->Form->input('message', array('type'=>'textarea', 'value'=>stripslashes($inactiveemaildata['Inactiveemail']["message"]),'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
					</div>
					<div class="fromnewtext">Mail Template Fields :</div>
					<div class="fromborderdropedown3">
						  <?php foreach($tagdata as $tag){?>
								<div class="linktotext"><a onClick="AddTags(document.getElementById('SitesettingMessage'),'[<?php echo $tag['Template_tag']['tag_name'];?>]');" title="click to paste" href="javascript:void(0);"><?php echo '['.$tag['Template_tag']['tag_name'].']';?></a><?php echo ' - '.$tag['Template_tag']['tag_desc'];?></div>
						  <?php }?>
					</div>
					<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_activationautomailupdate', $SubadminAccessArray)){ ?>
						  <div class="formbutton">
						  <?php echo $this->Js->submit('Update', array(
								'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'update'=>'#UpdateMessage',
								'class'=>'btnorange',
								'controller'=>'sitesetting',
								'div'=>false,
								'action'=>'activationautomailupdate',
								'url'   => array('controller' => 'sitesetting', 'action' => 'activationautomailupdate')
						  ));?>
						  </div>
					<?php } ?>
				</div>

		<?php echo $this->Form->end();?>

<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>