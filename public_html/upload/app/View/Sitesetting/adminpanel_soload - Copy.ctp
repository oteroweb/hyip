<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 12-12-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Advertisement</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Banner Ads", array('controller'=>'sitesetting', "action"=>"advertisementcredit"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Text Ads", array('controller'=>'sitesetting', "action"=>"advertisementtextadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Solo Ads", array('controller'=>'sitesetting', "action"=>"soload"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Login Ads", array('controller'=>'sitesetting', "action"=>"loginadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC", array('controller'=>'sitesetting', "action"=>"ppcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC", array('controller'=>'sitesetting', "action"=>"ptcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Biz Directory", array('controller'=>'sitesetting', "action"=>"bizdirectorysetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Widgets", array('controller'=>'sitesetting', "action"=>"advertisementbannerwidget"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<div class="tab-innar">
	<ul>
		<li>
			<?php echo $this->Js->link("Credit Solo Ads Settings", array('controller'=>'sitesetting', "action"=>"soload"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>'active'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Plan Solo Ads Settings", array('controller'=>'sitesetting', "action"=>"soloadplan"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				
			));?>
		</li>
	</ul>
</div>
<?php 
if($textarea=='advanced')
{
	echo $this->Javascript->link('tiny_mce/tiny_mce.js');
	echo $this->Javascript->link('tinymce');
	$textareaclass='advancededitor';
	$submitbuttonclass='tinyMCEtriggerSavetk';
}
elseif($textarea=='simple')
{
	$textareaclass='';
	$submitbuttonclass='';
}
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="whitenoteboxinner marginnone"><span class="color-red"><b>*Note : </b></span><?php echo "Credit Solo Ads And Plan Solo Ads <b>Work in Sync</b>. So, Keep This Enabled if You Want Any/Both of Them."?></div><div class="height10"></div>

<div class="helpicon"><a href="https://www.proxscripts.com/docs/Advertisement_Settings#Solo_Ads" target="_blank">Help</a></div>

    <div id="UpdateMessage"></div>
	<div class="backgroundwhite">
    
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'soloadupdate')));?>
			<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
				
					<div class="frommain">

					<div class="fromnewtext">Enable Credit Solo Ad System :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown">
						<div class="select-main">
						  <label>
						  <?php 
							echo $this->Form->input('enable_soloads', array(
								'type' => 'select',
								'options' => array('1'=>'Yes', '0'=>'No'),
								'selected' => $SITECONFIG["enable_soloads"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => '',
								'onchange'=>'if($(this).val()==1){$(".enadis").show(500);}else{$(".enadis").hide(500);}'
							));
						  ?>
						  </label>
						</div>
					</div>
					<span class="tooltipmain glyphicon-question-sign "data-toggle="tooltip" data-original-title="Take a Note That This Ad System Works on Credits And Credits That You Provide to The Members Via Plans, Memberships, Signup Bonus, etc. Won't Be of Use if You Disable Ad System(s)."></span>	
				  <div class="enadis" style="<?php if($SITECONFIG["enable_soloads"]==0){ echo 'display:none;';} ?>">
					<div class="fromnewtext">Auto Approval :  </div>
				  <div class="fromborderdropedown">
					  <div class="select-main">
						  <label>
							  <?php 
								echo $this->Form->input('addapprove', array(
									'type' => 'select',
									'options' => array('0'=>'Yes', '1'=>'No'),
									'selected' => $addapprove,
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => ''
								));
							  ?>
						  </label>
					  </div>
				  </div>
				  <span class="tooltipmain glyphicon-question-sign"data-toggle="tooltip" data-original-title="New Solo Ad Will Be Automatically Approved When This is Set to 'Yes'. You Will Have to <b>Manually Approve</b> New Ads in Other Case."></span>
				  <div class="fromnewtext">Auto Approval on Edit :  </div>
				  <div class="fromborderdropedown">
					  <div class="select-main">
						  <label>
							  <?php 
								echo $this->Form->input('iseditapprove', array(
									'type' => 'select',
									'options' => array('0'=>'Yes', '1'=>'No'),
									'selected' => $iseditapprove,
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => ''
								));
							  ?>
						  </label>
					  </div>
				  </div>
				  <span class="tooltipmain glyphicon-question-sign"data-toggle="tooltip" data-original-title="Members Can Edit The Solo Ad From Front Side. Choose Whether Those Edited Solo Ad Will Be <b>Automatically Approved</b> or Not."></span>
				  
				  <div class="fromnewtext">Number of Solo Ad Mails to be Sent At a Time :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('mail_counter', array('type'=>'text', 'value'=>$soloads_settings["Soloads_setting"]["mail_counter"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>

					<div class="fromnewtext">Number of Solo Ad Mails to be Sent Per Day :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('soloads_counter', array('type'=>'text', 'value'=>$soloads_settings["Soloads_setting"]["soloads_counter"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>

					<div class="fromnewtext">Solo Ad Mails Subject Prefix :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('subjet_prefix', array('type'=>'text', 'value'=>$soloads_settings["Soloads_setting"]["subjet_prefix"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					
					<div class="fromnewtext">Number of Text Ad Credits to be Rewarded For a Click in Solo Ad Mail :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('textads_credit', array('type'=>'text', 'value'=>$soloads_settings["Soloads_setting"]["textads_credit"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>

					<div class="fromnewtext">Number of Banner Ad Credits to be Rewarded For a Click in Solo Ad Mail :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('bannerads_credit', array('type'=>'text', 'value'=>$soloads_settings["Soloads_setting"]["bannerads_credit"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>

					<div class="fromnewtext">Number of Solo Ads That Can Be Saved By a Member :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('soloads_save', array('type'=>'text', 'value'=>$soloads_settings["Soloads_setting"]["soloads_save"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>

					<div class="fromnewtext">Expiration Term (Days) :<span class="red-color">*</span>  </div>
					<div class="fromborder">
						<?php echo $this->Form->input('expire_day', array('type'=>'text', 'value'=>$soloads_settings["Soloads_setting"]["expire_day"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					<span class="tooltipmain glyphicon-question-sign" data-toggle="tooltip" data-original-title="Specify Number Of Days Post Approval After Which Solo Ad Will Expire Whether it's Sent or Not."></span>

					
					<div class="formbutton">
						<?php 
							if($textarea=='simple'){$showtextarea='advanced';}
							if($textarea=='advanced'){$showtextarea='simple';}
							$link='soload/0/'.$showtextarea;
							
							echo $this->Js->link("Switch To ".ucfirst($showtextarea)." Mode", array('controller'=>'sitesetting', "action"=>$link), array(
								'update'=>'#settingpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'div'=>false,
								'class'=>'btngray'
						));?>
					</div>
					
					<div class="fromnewtext">Solo Ad Mails Header Space Prefix : </div>
					<div class="fromborder4">
						<?php echo $this->Form->input('header_prifix', array('type'=>'textarea', 'value'=>stripslashes($soloads_settings["Soloads_setting"]["header_prifix"]),'label' => false,'div'=>false, 'class'=>'from-textarea '.$textareaclass, 'style'=>'width:98%;height:300px;margin-left:0px;'));?>
					</div>
					
					<div class="fromnewtext">Solo Ad Mails Footer Space Appendix : </div>
					
					<div class="fromborder4">
						<?php echo $this->Form->input('footer_appendix', array('type'=>'textarea', 'value'=>stripslashes($soloads_settings["Soloads_setting"]["footer_appendix"]),'label' => false,'div'=>false, 'class'=>'from-textarea '.$textareaclass, 'style'=>'width:98%;height:300px;margin-left:0px;'));?>
					</div>
				  </div>
					<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_soloadupdate',$SubadminAccessArray)){ ?>
						<div class="formbutton">
								<?php echo $this->Js->submit('Submit', array(
								  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								  'update'=>'#UpdateMessage',
								  'class'=>'btnorange '.$submitbuttonclass,
								  'controller'=>'sitesetting',
								  'div'=>false,
								  'action'=>'soloadupdate',
								  'url'   => array('controller' => 'sitesetting', 'action' => 'soloadupdate')
								));?>
						</div>
					<?php } ?>
					</div>
				
		<?php echo $this->Form->end();?>
	
	</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>