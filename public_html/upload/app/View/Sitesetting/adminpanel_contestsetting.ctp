<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 30-03-2015
  * Last Modified: 30-03-2015
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Website</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Main Details", array('controller'=>'sitesetting', "action"=>"index"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'class'=>'satting-menu-active'
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Member Tool Settings", array('controller'=>'sitesetting', "action"=>"shortenersetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Database Operations", array('controller'=>'sitesetting', "action"=>"maintenance"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>	
				</li>
				<li>
					<?php echo $this->Js->link("Logs Settings", array('controller'=>'sitesetting', "action"=>"logs"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Launch Date Settings", array('controller'=>'sitesetting', "action"=>"launch"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Currency Settings", array('controller'=>'sitesetting', "action"=>"currency"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Cronjob Settings", array('controller'=>'sitesetting', "action"=>"cronjob"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Notification", array('controller'=>'sitesetting', "action"=>"notification"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<div class="tab-innar">
	<ul>
		<li>
			<?php echo $this->Js->link("Shortener Settings", array('controller'=>'sitesetting', "action"=>"shortenersetting"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Rotator Settings", array('controller'=>'sitesetting', "action"=>"rotatorsetting"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Dynamic Banner Ad Settings", array('controller'=>'sitesetting', "action"=>"dynamicbanner"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Contest Settings", array('controller'=>'sitesetting', "action"=>"contestsetting"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>'active'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Jackpot Settings", array('controller'=>'sitesetting', "action"=>"jackpotsetting"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			));?>
		</li>
	</ul>
</div>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Website_Settings#Contest_Settings" target="_blank">Help</a></div>
<div id="UpdateMessage" class="UpdateMessage"></div>

<?php echo $this->Form->create('Sitesetting',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'contestsettingaction')));?>
		<div class="frommain">
			<div class="fromnewtext">Enable Referral Contests :  </div>
			<div class="fromborderdropedown3">
				<div class="select-main">
				<label>
				<?php 
					  echo $this->Form->input('isenable', array(
						  'type' => 'select',
						  'options' => array('1'=>'Yes', '0'=>'No'),
						  'selected' => $isenable,
						  'class'=>'',
						  'label' => false,
						  'div' => false,
						  'style' => '',
						  'onchange'=>'if($(this).val()==1){$(".enadis").show(500);}else{$(".enadis").hide(500);}'
					  ));
				?>
				</label>
				</div>
			</div>
			
			<div class="fromnewtext">Number of Leaders display on the Leader Page :<span class="red-color">*</span>  </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('leaderbord', array('type'=>'text', 'value'=>$leaderbord, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>	
	<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_contestsettingaction', $SubadminAccessArray)){ ?>
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'class'=>'btnorange',
				  'div'=>false,
				  'controller'=>'sitesetting',
				  'action'=>'contestsettingaction',
				  'url'   => array('controller' => 'sitesetting', 'action' => 'contestsettingaction')
				));?>
		</div>
	<?php } ?>
		</div>
	
<?php echo $this->Form->end();?>
<?php }else{ ?><div class="accessdenied"><?php echo "Access Denied";?></div><?php }?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>