<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Security</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Common Settings", array('controller'=>'sitesetting', "action"=>"antibrute"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("2-Step Verification Settings", array('controller'=>'sitesetting', "action"=>"googleauthentication"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Password Settings", array('controller'=>'sitesetting', "action"=>"passwordsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Website Security", array('controller'=>'sitesetting', "action"=>"websitesecurity"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Captcha Settings", array('controller'=>'sitesetting', "action"=>"captcha"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Security_Settings#Common_Settings" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
				<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'antibruteupdate')));?>
				<?php echo $this->form->input('id', array('type'=>'hidden','value'=>'1'));?>
				<div class="tab-pane" id="profile">
					<div class="frommain">
						<div class="fromnewtext">Use Static IP :  </div>
						<div class="fromborderdropedown3">
							<div class="select-main">
								<label>
								<?php 
									 echo $this->Form->input('usestaticip', array(
									  'type' => 'select',
									  'options' => array('1'=>'Yes', '0'=>'No'),
									  'selected' => $SITECONFIG["usestaticip"],
									  'class'=>'',
									  'label' => false,
									  'div' => false,
									  'style' => ''
								  ));
								?>
								</label>
							</div>
						</div>
						

						<div class="fromnewtext">Allow multiple IPs in one session :  </div>
						<div class="fromborderdropedown3">
							<div class="select-main">
								<label>
								<?php 
									 echo $this->Form->input('iploginexpir', array(
									  'type' => 'select',
									  'options' => array('1'=>'No', '0'=>'Yes'),
									  'selected' => $SITECONFIG["iploginexpir"],
									  'class'=>'',
									  'label' => false,
									  'div' => false,
									  'style' => ''
								  ));
								?>
								</label>
							</div>
						</div>
						
						<div class="fromnewtext">Verification Mail Expiration Time (Hours) :<span class="red-color">*</span>  </div>
						<div class="fromborderdropedown3">
							<?php echo $this->Form->input('usestatictime', array('type'=>'text', 'value'=>$SITECONFIG["usestatictime"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
						</div>
						

						<div class="fromnewtext">Anti Brute Force : <span class="red-color">*</span> </div>
						<div class="mainborderbox fromborderdropedown3 extracssfortab">
							  <div class="paidetext2">Lock After </div>
							  <div class="paidfrombox"><?php echo $this->Form->input('antibrutecounter', array('type'=>'text', 'value'=>$SITECONFIG["antibrutecounter"], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
							  <div class="paidetext2">Failed Login Attempts For </div>
							  <div class="paidfrombox"><?php echo $this->Form->input('antibrutehours', array('type'=>'text', 'value'=>$SITECONFIG["antibrutehours"], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
							  <div class="paidetext2" style="width: 152px;">Hours </div>
						  </div>

						
						<div class="fromnewtext">Anti Brute Force : <span class="red-color">*</span> </div>
						<div class="mainborderbox extracssfortab secondfortab fromborderdropedown3">
							  <div class="paidetext2">Permanent Lock After </div>
							  <div class="paidfrombox"><?php echo $this->Form->input('antibrutepermanentcounter', array('type'=>'text', 'value'=>$SITECONFIG["antibrutepermanentcounter"], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
							  <div class="paidetext2">Failed Login Attempts in </div>
							  <div class="paidfrombox"><?php echo $this->Form->input('antibrutepermanentminute', array('type'=>'text', 'value'=>$SITECONFIG["antibrutepermanentminute"], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>'')); ?></div>
							  <div class="paidetext2" style="width: 152px;">Minutes </div>
						
						</div>

						<div class="formbutton">
							 <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_antibruteupdate',$SubadminAccessArray)){ ?>
							 <?php echo $this->Js->submit('Update', array(
								  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								  'update'=>'#UpdateMessage',
								  'class'=>'btnorange',
								  'controller'=>'Sitesetting',
								  'action'=>'antibruteupdate',
								  'div'=>'false',
								  'url'   => array('controller' => 'sitesetting', 'action' => 'antibruteupdate')
								));?>
							<?php } ?>	
				
					</div>
				</div>
				<?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>