<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>

<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Email</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("SMTP Settings", array('controller'=>'sitesetting', "action"=>"smtp"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Auto Responder", array('controller'=>'sitesetting', "action"=>"autoresponder"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Account Activation Mail", array('controller'=>'sitesetting', "action"=>"activationautomail"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Unpaid Member Notification", array('controller'=>'sitesetting', "action"=>"unpaidmembernotification"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Inactive Member Mail", array('controller'=>'sitesetting', "action"=>"inactivemembermail"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Membership Template Emails", array('controller'=>'sitesetting', "action"=>"membershipemailtemplate"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>

<div class="height10"></div>

<div class="helpicon"><a href="https://www.proxscripts.com/docs/Email_Settings#Unpaid_Member_Notification" target="_blank">Help</a></div>

<div id="UpdateMessage"></div>
<div class="serchmainbox">
	<div class="serchgreybox"><?php echo "Search Option";?></div>
	<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'unpaidmembernotification')));?>
	<div class="from-box">
		<div class="fromboxmain">
			<span>Search By :</span>
			<span>                     
				<div class="searchoptionselect">
					<div class="select-main">
						  <label>
							    <?php 
							    echo $this->Form->input('searchby', array(
								    'type' => 'select',
								    'options' => array('all'=>'Select Parameter', 'template_id'=>'Id', 'subject'=>'Subject', 'days'=>'Day(s) after sign up', 'active'=>'Active', 'inactive'=>'Inactive'),
								    'selected' => $searchby,
								    'class'=>'',
								    'label' => false,
								    'style' => '',
									'onchange'=>'if($(this).val()=="active" || $(this).val()=="inactive"){ $(".SearchFor").hide(500)}else{$(".SearchFor").show(500)}'
							    ));
							    ?>
						  </label>
					</div>
				</div>
			</span>
		</div>
		<div class="fromboxmain">
			<span class="SearchFor" <?php if($searchby=="active" || $searchby=="inactive"){ echo "style='display:none'"; } ?>>Search For :</span>
			<span class="SearchFor" <?php if($searchby=="active" || $searchby=="inactive"){ echo "style='display:none'"; } ?>><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#settingpage',
					'class'=>'searchbtn',
					'controller'=>'Sitesetting',
					'action'=>'unpaidmembernotification',
					'url'   => array('controller' => 'sitesetting', 'action' => 'unpaidmembernotification')
				));?>
			</span>
		</div>
	</div>
	<?php echo $this->Form->end();?>
</div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#settingpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'sitesetting', 'action'=>'unpaidmembernotification')
	));
	?>
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
    <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_unpaidmembernotificationadd',$SubadminAccessArray)){ ?>
		<?php echo $this->Js->link("+ Add New", array('controller'=>'sitesetting', "action"=>"unpaidmembernotificationadd"), array(
			'update'=>'#settingpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btnorange'
		));?>
    <?php } ?>
	</div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
		<div class="tablegridheader">
			<div>ID</div>
			<div>Subject</div>
			<div>Day(s) after sign up</div>
			<div>Action</div>
		</div>
		<?php foreach ($inactive_template_emails as $inactive_template_email): ?>
			<div class="tablegridrow">
				<div><?php echo $inactive_template_email['Inactive_template_email']['template_id']; ?></div>
				<div><?php echo stripslashes($inactive_template_email['Inactive_template_email']['subject']); ?></div>
				<div><?php echo $inactive_template_email['Inactive_template_email']['days']; ?></div>
				<div class="textcenter">
					<div class="actionmenu">
						<div class="btn-group">
							<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								
								<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_unpaidmembernotificationadd/$',$SubadminAccessArray)){ ?>
								<li>
									<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Notification')).' Edit Notification', array('controller'=>'sitesetting', "action"=>"unpaidmembernotificationadd/".$inactive_template_email['Inactive_template_email']['template_id']), array(
										'update'=>'#settingpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));?>
								</li>
								<?php } ?>
								
								<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_unpaidmembernotificationstatus',$SubadminAccessArray)){ ?>
								<li>
									<?php 
									if($inactive_template_email['Inactive_template_email']['status']==0){
										$statusaction='1';
										$statusicon='red-icon.png';
										$statustext='Activate Notification';
									}else{
										$statusaction='0';
										$statusicon='blue-icon.png';
										$statustext='Inactivate Notification';}
									echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'sitesetting', "action"=>"unpaidmembernotificationstatus/".$statusaction."/".$inactive_template_email['Inactive_template_email']['template_id']."/".$this->params['paging']['Inactive_template_email']['page']), array(
										'update'=>'#settingpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));?>
								</li>
								<?php } ?>
								
								<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_unpaidmembernotificationremove',$SubadminAccessArray)){ ?>
								<li>
									<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Notification')).' Delete Notification', array('controller'=>'sitesetting', "action"=>"unpaidmembernotificationremove/".$inactive_template_email['Inactive_template_email']['template_id']."/".$this->params['paging']['Inactive_template_email']['page']), array(
										'update'=>'#settingpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false,
										'confirm'=>'Are You Sure?'
									));?>
								</li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>	
		<?php endforeach; ?>
	</div>
	<?php if(count($inactive_template_emails)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	<?php 
	if($this->params['paging']['Inactive_template_email']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'unpaidmembernotification/rpp')));?>
		
		<div class="resultperpage">
                    <label>
			      <?php 
			      echo $this->Form->input('resultperpage', array(
				'type' => 'select',
				'options' => $resultperpage,
				'selected' => $this->Session->read('pagerecord'),
				'class'=>'searchcombobox',
				'label' => false,
				'div'=>false,
				'style' => '',
				'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			      ));
			      ?>
		    </label>
		</div>
		    
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#settingpage',
			  'class'=>'large white button',
			  'div'=>false,
			  'controller'=>'Sitesetting',
			  'action'=>'unpaidmembernotification/rpp',
			  'url'   => array('controller' => 'sitesetting', 'action' => 'unpaidmembernotification/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '...', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
	<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>