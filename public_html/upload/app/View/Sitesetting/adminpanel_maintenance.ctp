<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 13-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<div id="LoadingDiv" style="display: none;">
	<?php echo $this->Html->image('wait.gif', array('alt' => 'Please Wait!!'));?>
</div> 
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Website</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Main Details", array('controller'=>'sitesetting', "action"=>"index"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'class'=>'satting-menu-active'
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Member Tool Settings", array('controller'=>'sitesetting', "action"=>"shortenersetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Database Operations", array('controller'=>'sitesetting', "action"=>"maintenance"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>	
				</li>
				<li>
					<?php echo $this->Js->link("Logs Settings", array('controller'=>'sitesetting', "action"=>"logs"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Launch Date Settings", array('controller'=>'sitesetting', "action"=>"launch"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Currency Settings", array('controller'=>'sitesetting', "action"=>"currency"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Cronjob Settings", array('controller'=>'sitesetting', "action"=>"cronjob"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Notification", array('controller'=>'sitesetting', "action"=>"notification"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Website_Settings#Database_Operations" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

   
<script>
function SetBackupDayHidden(bdcmb)
{
	if(bdcmb=='no' || bdcmb=='daily')
		$("#hiddenbackupday").val(0);
	else
		$("#hiddenbackupday").val(bdcmb);
}
function SetBackupTimeCombo(btcmb)
{
	if(btcmb=='no' || btcmb=='daily')
	{
		$("#monthcmb").css("display","none");
		$("#weekcmb").css("display","none");
		$("#hiddenbackupday").val(0);
	}
	else if(btcmb=='weekly')
	{
		$("#weekcmb").css("display","inline-block");
		$("#monthcmb").css("display","none");
		$("#hiddenbackupday").val($("#backup_day_week").val());
	}
	else if(btcmb=='monthly')
	{
		$("#weekcmb").css("display","none");
		$("#monthcmb").css("display","inline-block");
		$("#hiddenbackupday").val($("#backup_day_month").val());
	}
}
</script>

    <center>
		<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_autobackupsetting',$SubadminAccessArray)){ ?>
		<div class="serchmainbox width100ontab" style="width:49.5%;float: left;">
			<div class="serchgreybox">Auto Backup Settings</div>
			<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'autobackupsetting')));?>
			<div class="from-box" style="min-height:65px;">
				<div class="fromboxmain" style="width:auto;">
					<span style="width:auto;">Select Automatic Backup Time Cycle</span>
					<span>                     
						<div class="searchoptionselect" style="width: 100px;display: inline-block;vertical-align: top;">
							<div class="select-main">
								<label>
									<?php 
										echo $this->Form->input('backup_time', array(
										    'type' => 'select',
										    'options' => array('no'=>'No', 'daily'=>'Daily', 'weekly'=>'Weekly', 'monthly'=>'Monthly'),
										    'selected' => $SITECONFIG["backup_time"],
										    'class'=>'',
										    'label' => false,
										    'style' => '',
										    'onchange' => 'SetBackupTimeCombo(this.value);'
										));
										echo $this->Form->input('backup_day', array(
										    'type' => 'hidden',
										    'id' => 'hiddenbackupday',
										    'value' => $SITECONFIG["backup_day"]
										));
									?>
								</label>
							</div>	
						</div>
						<?php
							if($SITECONFIG["backup_time"]=='weekly')
								$weekstyle='display:inline-block';
							else
								$weekstyle='display:none';
							
							if($SITECONFIG["backup_time"]=='monthly')
								$monthstyle='display:inline-block';
							else
								$monthstyle='display:none';
				
							echo '<div id="weekcmb" class="searchoptionselect" style="width:100px;'.$weekstyle.'"><div class="select-main"><label>';
								echo $this->Form->input('backup_day_week', array(
									  'type' => 'select',
									  'options' => array('7'=>'Sunday', '1'=>'Monday', '2'=>'Tuesday', '3'=>'Wednesday', '4'=>'Thursday', '5'=>'Friday', '6'=>'Saturday'),
									  'selected' => $SITECONFIG["backup_day"],
									  'class'=>'',
									  'label' => false,
									  'style' => '',
									  'id' => 'backup_day_week',
									  'onchange' => 'SetBackupDayHidden(this.value);'
								  ));
							echo '</label></div></div>';
							echo '<div id="monthcmb" class="searchoptionselect" style="width:100px;'.$monthstyle.'"><div class="select-main"><label>';
								echo $this->Form->input('backup_day_month', array(
									  'type' => 'select',
									  'options' => array('1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'9', '10'=>'10', '11'=>'11', '12'=>'12', '13'=>'13', '14'=>'14', '15'=>'15', '16'=>'16', '17'=>'17', '18'=>'18', '19'=>'19', '20'=>'20', '21'=>'21', '22'=>'22', '23'=>'23', '24'=>'24', '25'=>'25', '26'=>'26', '27'=>'27', '28'=>'28', '29'=>'29', '30'=>'30', '31'=>'31'),
									  'selected' => $SITECONFIG["backup_day"],
									  'class'=>'',
									  'label' => false,
									  'style' => '',
									  'id' => 'backup_day_month',
									  'onchange' => 'SetBackupDayHidden(this.value);'
								  ));
							echo '</label></div></div>';
							?>
					</span>
					<span>
						<?php echo $this->Js->submit('Update', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#UpdateMessage',
						'class'=>'btnorange',
						'controller'=>'Sitesetting',
						'action'=>'memberupdate',
						'url'   => array('controller' => 'sitesetting', 'action' => 'autobackupsetting')
					  ));?>
					</span>
				</div>
			</div>
			<?php echo $this->Form->end();?>
		</div>
		<?php $displaysmallsearch=1; } ?>
		
		<div class="serchmainbox width100ontab" <?php if($displaysmallsearch) echo 'style="float:right;width:49.5%;"';?>>
			<div class="serchgreybox">Search Option</div>
			<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'maintenance')));?>
				<div class="from-box" style="min-height:65px;">
					<div class="fromboxmain" style="width:auto;">
						<span style="width:auto;">Select Automatic Backup Time Cycle</span>
						<span>
							<div class="searchoptionselect" style="width: 100px;">
								<div class="select-main">
									<label>
										<?php 
											echo $this->Form->input('searchbackup', array(
												'type' => 'select',
												'options' => array('all'=>'All', 'month'=>'Month', '3month'=>'3 Months', 'week'=>'Week'),
												'selected' => $searchby,
												'class'=>'',
												'label' => false,
												'style' => ''
											));
										?>
									</label>
								</div>
							</div>	
						</span>
						<span>
							<?php echo $this->Js->submit('Search', array(
								'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'update'=>'#settingpage',
								'class'=>'btnorange',
								'controller'=>'Sitesetting',
								'action'=>'maintenance',
								'url'   => array('controller' => 'sitesetting', 'action' => 'maintenance')
							  ));?>
						</span>
					</div>
				</div>
			<?php echo $this->Form->end();?>
		</div>	
</center>
<div class="clear-both"></div>
<div class="height21"></div>

<?php
if($repair)
{
	?>
	<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;', 'class'=>'displayinline','url'=>array('controller'=>'sitesetting','action'=>'backupdatabase')));?>
	
	<div class="paginator-text"></div>
	<div class="addnew-button">
	<?php echo $this->Js->submit('Backup', array(
	  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
	  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
	  'update'=>'#settingpage',
	  'class'=>'btnorange',
	  'div'=>false,
	  'controller'=>'Sitesetting',
	  'action'=>'backupdatabase',
	  'url'   => array('controller' => 'sitesetting', 'action' => 'backupdatabase')
	));?>
	<?php echo $this->Form->end();?>
	<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;', 'class'=>'displayinline','url'=>array('controller'=>'sitesetting','action'=>'backupdatabase')));?>
	<?php echo $this->Js->submit('Repair', array(
	  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
	  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
	  'update'=>'#settingpage',
	  'class'=>'btngray',
	  'div'=>false,
	  'controller'=>'Sitesetting',
	  'action'=>'dbrepair',
	  'url'   => array('controller' => 'sitesetting', 'action' => 'dbrepair')
	));?>
	<?php echo $this->Form->end();?>
	</div>
	<div class="clear-both"></div>
	<div class="tablegrid">
		<div class="tablegridheader">
			<div></div>
			<div>Summary</div>
			<div></div>
		</div>
		<div class="tablegridrow">
				<div>
					<?php echo $this->html->image("errors.png");?>
				</div>
				<div>
					Error
				</div>
				<div>
					<?php echo $totalEIW[0];?>
				</div>
		</div>
		<div class="tablegridrow">
				<div>
					<?php echo $this->html->image("information.png");?>
				</div>
				<div>
					Information
				</div>
				<div>
					<?php echo $totalEIW[1];?>
				</div>
		</div>
		<div class="tablegridrow">
				<div>
					<?php echo $this->html->image("warning.png");?>
				</div>
				<div>
					Warning
				</div>
				<div>
					<?php echo $totalEIW[2];?>
				</div>
		</div>
	</div>
	<div class="height7"></div>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>Result From Repair Tables</div>
		</div>
	</div>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>Table Name</div>
				<div>Status</div>
				<div>Message</div>
		</div>
		<?php	foreach ($repaircontents as $repaircontent): ?>
		<div class="tablegridrow">
				<div><?php echo $repaircontent['tablename']; ?></div>
				<div>
					<?php 
					  if($repaircontent['msgtype']=="error") 
						echo $this->html->image("errors.png");
					  elseif($repaircontent['msgtype']=="info") 
						echo $this->html->image("information.png");
					  elseif($repaircontent['msgtype']=="warning") 
						echo $this->html->image("warning.png");
					  else
					  	echo $this->html->image("blue-icon.png");
					?>
				</div>
				<div><?php echo $repaircontent['msg']; ?></div>
		</div>
			<?php endforeach; ?>
	</div>
				<?php if(count($repaircontents)==0){ echo '<div class="norecordfound">
No records available</div>';} ?>
	<div class="height7"></div>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>Result From Analyze Tables</div>
		</div>
	</div>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>Table Name</div>
				<div>Status</div>
				<div>Message</div>
		</div>
			<?php foreach ($analyzecontents as $analyzecontent): ?>
				<div class="tablegridrow">
					<div><?php echo $analyzecontent['tablename']; ?></div>
					<div>
					<?php 
					  if($analyzecontent['msgtype']=="error") 
						echo $this->html->image("errors.png");
					  elseif($analyzecontent['msgtype']=="info") 
						echo $this->html->image("information.png");
					  elseif($analyzecontent['msgtype']=="warning") 
						echo $this->html->image("warning.png");
					  else
					  	echo $this->html->image("blue-icon.png");
					?>
					</div>
					<div><?php echo $analyzecontent['msg']; ?></div>
				</div>
			<?php endforeach; ?>
		</div>
				<?php if(count($analyzecontents)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	<div class="height7"></div>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>Result From Check Tables</div>
		</div>
	</div>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>Table Name</div>
				<div>Status</div>
				<div>Message</div>
		</div>
			<?php foreach ($checkcontents as $checkcontent): ?>
			<div class="tablegridrow">
					<div><?php echo $checkcontent['tablename']; ?></div>
					<div>
					<?php 
					  if($checkcontent['msgtype']=="error") 
						echo $this->html->image("errors.png");
					  elseif($checkcontent['msgtype']=="info") 
						echo $this->html->image("information.png");
					  elseif($checkcontent['msgtype']=="warning") 
						echo $this->html->image("warning.png");
					  else
					  	echo $this->html->image("blue-icon.png");
					?>
					</div>
					<div><?php echo $checkcontent['msg']; ?></div>
			</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($checkcontents)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	<div class="height7"></div>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>Result From Optimize Tables</div>
		</div>
	</div>
	<div class="greenbottomborder">
		<div class="tablegrid">
			<div class="tablegridheader">
					<div>Table Name</div>
					<div>Status</div>
					<div>Message</div>
			</div>
				<?php foreach ($optimizecontents as $optimizecontent): ?>
					<div class="tablegridrow">
						<div><?php echo $optimizecontent['tablename']; ?></div>
						<div>
						<?php 
						  if($optimizecontent['msgtype']=="error") 
							echo $this->html->image("errors.png");
						  elseif($optimizecontent['msgtype']=="info") 
							echo $this->html->image("information.png");
						  elseif($optimizecontent['msgtype']=="warning") 
							echo $this->html->image("warning.png");
						  else
							echo $this->html->image("blue-icon.png");
						?>
						</div>
						<div><?php echo $optimizecontent['msg']; ?></div>
					</div>
				<?php endforeach; ?>
		</div>
		<?php if(count($optimizecontents)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
		<div class="height10"></div>
	</div>
	<?php
}
else
{
?>

	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#settingpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'sitesetting', 'action'=>'maintenance')
	));
	?>
	
	<div class="paginator-text floatleft"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
    <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_backupdatabase',$SubadminAccessArray)){ ?>
        
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;', 'class'=>'displayinline','url'=>array('controller'=>'sitesetting','action'=>'backupdatabase')));?>
		<?php echo $this->Js->submit('Backup', array(
		  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
		  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		  'update'=>'#settingpage',
		  'class'=>'btnorange',
		  'div'=>false,
		  'controller'=>'Sitesetting',
		  'action'=>'backupdatabase',
		  'url'   => array('controller' => 'sitesetting', 'action' => 'backupdatabase')
		));?>
		<?php echo $this->Form->end();?>
    <?php } ?>
    <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_dbrepair',$SubadminAccessArray)){ ?>
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;', 'class'=>'displayinline','url'=>array('controller'=>'sitesetting','action'=>'backupdatabase')));?>
		<?php echo $this->Js->submit('Repair', array(
		  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
		  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		  'update'=>'#settingpage',
		  'class'=>'btngray',
		  'div'=>false,
		  'controller'=>'Sitesetting',
		  'action'=>'dbrepair',
		  'url'   => array('controller' => 'sitesetting', 'action' => 'dbrepair')
		));?>
		<?php echo $this->Form->end();?>
    <?php } ?>
	</div>
	<div class="clear-both"></div>
	<div class="greenbottomborder">
		<div class="tablegrid">
				<div class="tablegridheader">
					<div>Date</div>
					<div>Path</div>
					<div>Action</div>
				</div>
				<?php foreach ($backups as $backup):?>
					<div class="tablegridrow">
						<div><?php echo $backup['Backup']['backupdate']; ?></div>
						<div><?php echo $backup['Backup']['path']; ?></div>
						<div class="textcenter">
						  <div class="actionmenu">
							<div class="btn-group">
							  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
								  Action <span class="caret"></span>
							  </button>
							  <ul class="dropdown-menu" role="menu">
								<li>
									<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_backuprestore',$SubadminAccessArray)){ ?>
										<?php echo $this->Js->link($this->html->image('restore.png', array('alt'=>'Restore Backup'))." Restore Backup", array('controller'=>'sitesetting', "action"=>"backuprestore/".$backup['Backup']['id']), array(
											'update'=>'#settingpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false
										));?>
									<?php } ?>
								</li>
								<li>
									<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_backupremove',$SubadminAccessArray)){ ?>
										<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Backup'))." Delete", array('controller'=>'sitesetting', "action"=>"backupremove/".$backup['Backup']['id']), array(
											'update'=>'#settingpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'confirm'=>'Are You Sure?'
										));?>
									<?php } ?>
								</li>
							  </ul>
							</div>
						  </div>
						</div>
					</div>
				<?php endforeach; ?>
					
		</div>
		<?php if(count($backups)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
		<?php 
		if($this->params['paging']['Backup']['count']>$this->Session->read('pagerecord'))
		{?>
		<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
		<div class="floatleft margintop19">
			<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'maintenance/rpp')));?>
			<div class="resultperpage">
				<label>
					<?php 
					echo $this->Form->input('resultperpage', array(
					  'type' => 'select',
					  'options' => $resultperpage,
					  'selected' => $this->Session->read('pagerecord'),
					  'class'=>'',
					  'label' => false,
					  'div'=>false,
					  'style' => '',
					  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
					));
					?>
				</label>
			</div>
			<span id="resultperpageapply" style="display:none;">
				<?php echo $this->Js->submit('Apply', array(
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#settingpage',
				  'class'=>'',
				  'div'=>false,
				  'controller'=>'sitesetting',
				  'action'=>'maintenance/rpp',
				  'url'   => array('controller' => 'sitesetting', 'action' => 'maintenance/rpp')
				));?>
			</span>
			<?php echo $this->Form->end();?>
		</div>
		
		<?php }?>
		<div class="floatright">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
		</ul>
		</div>
		<div class="clear-both"></div>
		<div class="height10"></div>
	</div>
	
<?php } ?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>