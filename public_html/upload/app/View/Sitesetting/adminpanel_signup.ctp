<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Signup Settings</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Registration Form Settings", array('controller'=>'sitesetting', "action"=>"registrationform"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Profile Form Settings", array('controller'=>'sitesetting', "action"=>"profileform"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Free Credit & Bonus", array('controller'=>'sitesetting', "action"=>"freecreditandbonus"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Signup Settings", array('controller'=>'sitesetting', "action"=>"signup"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Facebook & Google Settings", array('controller'=>'sitesetting', "action"=>"facebookgoogle"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Signup_Settings#Signup_Settings" target="_blank">Help</a></div>

    <div id="UpdateMessage"></div>
	    <?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'signpupdate')));?>
        <?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
			<div class="tab-pane" id="profile">
				<div class="frommain">
					<div class="fromnewtext">Character Limitation For Username : <span class="red-color">*</span></div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('usernamelimit', array('type'=>'text', 'value'=>$SITECONFIG["usernamelimit"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					
					<div class="fromnewtext">Reserved Uuser Name:</div>
					<div class="frombordermain">
						<?php echo $this->Form->input('reservedusername', array('type'=>'textarea', 'value'=>stripslashes($SITECONFIG["reservedusername"]),'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
					 </div>
					<div class="fromnewtext">Allow New Registrations(Signups) : </div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
							<?php 
								echo $this->Form->input('allow_member_registration', array(
									'type' => 'select',
									'options' => array('1'=>'Yes', '0'=>'No'),
									'selected' => $SITECONFIG["allow_member_registration"],
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => ''
								));
							?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Allow New Registrations Same Ip(Signups) : </div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
							<?php 
								echo $this->Form->input('memberregistrationsameip', array(
									'type' => 'select',
									'options' => array('0'=>'Yes', '1'=>'No'),
									'selected' => $SITECONFIG["memberregistrationsameip"],
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => ''
								));
							?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Allow New Registrations Same Payment Processor Id : </div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
							<?php 
								echo $this->Form->input('membersameproid', array(
									'type' => 'select',
									'options' => array('0'=>'Yes', '1'=>'No'),
									'selected' => $SITECONFIG["membersameproid"],
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => ''
								));
							?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Email Confirmation Required For Members : </div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
							<?php 
								  echo $this->Form->input('emailconfirmation', array(
									  'type' => 'select',
									  'options' => array('1'=>'Yes', '0'=>'No'),
									  'selected' => $SITECONFIG["emailconfirmation"],
									  'class'=>'',
									  'label' => false,
									  'div' => false,
									  'style' => ''
								  ));
							?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Processor Email Confirmation Required For Members : </div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
							<?php 
								  echo $this->Form->input('processorconfirmation', array(
									  'type' => 'select',
									  'options' => array('1'=>'Yes', '0'=>'No'),
									  'selected' => $SITECONFIG["processorconfirmation"],
									  'class'=>'',
									  'label' => false,
									  'div' => false,
									  'style' => ''
								  ));
							?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Force an Upline During The Signup : </div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
							<?php 
								echo $this->Form->input('uplinereferral', array(
									'type' => 'select',
									'options' => array('1'=>'Yes', '0'=>'No'),
									'selected' => $SITECONFIG["uplinereferral"],
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => '',
									'onchange' => 'if(this.selectedIndex!=0){$(".randomreferral").show(500);}else{$(".randomreferral").hide(500);}'
								));
							?>
							</label>
						</div>
					</div>
					
				<div class="randomreferral" <?php if($SITECONFIG["uplinereferral"]==1) echo 'style="display:none;"'; ?>>
					<div class="fromnewtext">Default Sponsor : </div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
							<?php 
							  echo $this->Form->input('randomreferral', array(
                                                                'type' => 'select',
                                                                'options' => array('0'=>'No', '1'=>'Admin', '2'=>'Select Randomly', '3'=>'Set Default'),
                                                                'selected' => $SITECONFIG["randomreferral"],
                                                                'class'=>'',
                                                                'label' => false,
                                                                'div' => false,
                                                                'style' => '',
                                                                'onchange' => 'if(this.selectedIndex==3){$(".defaultsponsor").show(500);}else{$(".defaultsponsor").hide(500);}'
							  ));
							?>
							</label>
						</div>
					</div>
					
				</div>
                                <div class="defaultsponsor" <?php if($SITECONFIG["randomreferral"]!=3) echo 'style="display:none;"'; ?>>        
                                <div class="fromnewtext">Default Sponsor : <span class="red-color">*</span></div>
                                <div class="fromborder">
                                        <?php echo $this->Form->input('defaultsponsor', array('type'=>'text', 'value'=>$SITECONFIG["defaultsponsor"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
                                </div>
                                <span class="tooltipmain glyphicon-question-sign"data-toggle="tooltip" data-original-title="Set The Default sponsor id at the time of new registration."></span>       
                                </div>        
                                <div class="fromnewtext">Display Change Sponsor Option : </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
						  echo $this->Form->input('issponsorchange', array(
							  'type' => 'select',
							  'options' => array('1'=>'Yes', '0'=>'No'),
							  'selected' => $SITECONFIG["issponsorchange"],
							  'class'=>'',
							  'label' => false,
							  'div' => false,
							  'style' => ''
						  ));
						?>
						</label>
					</div>
				</div>
				
				<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_signpupdate',$SubadminAccessArray)){ ?>
					  <div class="formbutton">
							<?php echo $this->Js->submit('Update', array(
							  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
							  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							  'update'=>'#UpdateMessage',
							  'class'=>'btnorange',
							  'div'=>false,
							  'controller'=>'Sitesetting',
							  'action'=>'signpupdate',
							  'url'   => array('controller' => 'sitesetting', 'action' => 'signpupdate')
							));?>
					  </div>
				  <?php } ?>
				</div>
			</div>
		<?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>