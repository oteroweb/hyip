<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage'); ?>
<?php if(!$ajax){ ?>
<div class="whitetitlebox">Settings / Finance</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Payment Processor", array('controller'=>'sitesetting', "action"=>"paymentprocessor"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Add Fund Settings", array('controller'=>'sitesetting', "action"=>"addfund"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Withdrawal Settings", array('controller'=>'sitesetting', "action"=>"withdrawal"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }
$processorId=$processordata["Processor"]["id"];
$instructionsNote='See Instructions Below to Know How to Get This';
?>

<div class="helpicon"><a href="https://www.proxscripts.com/docs/Payment_Processor#<?php echo $processordata["Processor"]["directoryname"] ?>" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'paymentprocessorupdategeneral')));?>
		<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$processorId, 'label' => false));?>
     	        <div class="subbar">Add Fund Settings</div>
                  <div class="tab-pane" id="profile">
		             <div class="frommain">
						<?php echo $getform[0];?>
						<div class="formbutton">
							<?php echo $this->Js->submit('Update', array(
							  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
							  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							  'update'=>'#UpdateMessage',
							  'class'=>'btnorange',
							  'controller'=>'sitesetting',
							  'div'=>false,
							  'action'=>'paymentprocessorupdategeneral',
							  'url'   => array('controller' => 'sitesetting', 'action' => 'paymentprocessorupdategeneral')
							));?>
							<?php echo $this->Js->link("Back", array('controller'=>'sitesetting', "action"=>"paymentprocessor"), array(
								'update'=>'#settingpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'div'=>false,
								'class'=>'btngray'
							));?>
						</div>
				  </div>
			</div>
        <?php echo $this->Form->end();?>
		<?php if($getform[1]!=''){ ?>
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'paymentprocessorupdatemasspay')));?>
		<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$processorId, 'label' => false));?>
				<div class="subbar">Automated Masspay Settings</div>
                  <div class="tab-pane" id="profile">
		             <div class="frommain">
						<?php echo $getform[1];?>
							  <div class="formbutton">
							  <?php echo $this->Js->submit('Update', array(
								'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'update'=>'#UpdateMessage',
								'class'=>'btnorange',
								'controller'=>'sitesetting',
								'div'=>false,
								'action'=>'paymentprocessorupdatemasspay',
								'url'   => array('controller' => 'sitesetting', 'action' => 'paymentprocessorupdatemasspay')
							  ));?>
							  <?php echo $this->Js->link("Back", array('controller'=>'sitesetting', "action"=>"paymentprocessor"), array(
								  'update'=>'#settingpage',
								  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								  'escape'=>false,
								  'div'=>false,
								  'class'=>'btngray'
							  ));?>
							  </div>
						</div>
					  </div>
    	<?php echo $this->Form->end();?>
		<?php }  if($getform[2]!=''){  ?>
			<center>
			<div class="instructions-page-width">
				<div class="instructions-for-payza"><?php echo "Instructions For";?> <?php echo $processordata["Processor"]["proc_name"];?></div>
					 <?php echo $getform[2];?>
				</div>
			</div>
			</center>
		<?php } ?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>		
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>