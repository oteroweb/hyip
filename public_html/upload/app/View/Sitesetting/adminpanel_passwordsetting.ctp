<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Security</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Common Settings", array('controller'=>'sitesetting', "action"=>"antibrute"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("2-Step Verification Settings", array('controller'=>'sitesetting', "action"=>"googleauthentication"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Password Settings", array('controller'=>'sitesetting', "action"=>"passwordsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Website Security", array('controller'=>'sitesetting', "action"=>"websitesecurity"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Captcha Settings", array('controller'=>'sitesetting', "action"=>"captcha"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
    
</div>
	
	<div class="helpicon"><a href="https://www.proxscripts.com/docs/Security_Settings#Password_Settings" target="_blank">Help</a></div>
	
	<div id="UpdateMessage"></div>
        <?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'passwordsettingupdate')));?>
        <?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
			<div class="tab-pane" id="profile">
				<div class="frommain">
					<div class="fromnewtext">Minimum Password Length :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('pass_min_length', array('type'=>'text', 'value'=>$SITECONFIG["pass_min_length"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
  
					<div class="fromnewtext">Maximum Password Length :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('pass_max_length', array('type'=>'text', 'value'=>$SITECONFIG["pass_max_length"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
  
					<div class="fromnewtext">Minimum Numeric Characters in Passwords :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('pass_min_numeric_chars', array('type'=>'text', 'value'=>$SITECONFIG["pass_min_numeric_chars"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
  
					<div class="fromnewtext">Maximum Numeric Characters in Passwords :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('pass_max_numeric_chars', array('type'=>'text', 'value'=>$SITECONFIG["pass_max_numeric_chars"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					<div class="fromnewtext">Allow Non-Alphanumeric Characters in Passwords :  </div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
							<?php 
							echo $this->Form->input('pass_disallow_nonalphanumeric_chars', array(
								  'type' => 'select',
								  'options' => array('1'=>'Yes', '0'=>'No'),
								  'selected' => $SITECONFIG["pass_disallow_nonalphanumeric_chars"],
								  'class'=>'',
								  'label' => false,
								  'div' => false,
								  'style' => '',
								  'onchange' => 'if(this.selectedIndex==0){$(".passFieldDisp").show(500);}else{$(".passFieldDisp").hide(500);}'
							));
							?>
							</label>
						</div>
					</div>
					<div class="passFieldDisp" <?php echo ($SITECONFIG["pass_disallow_nonalphanumeric_chars"]==0)?'style="display: none;"':''; ?>>
						  <div class="fromnewtext">Minimum Number of Non-Alphanumeric Characters in Passwords :<span class="red-color">*</span>  </div>
						  <div class="fromborderdropedown3">
							  <?php echo $this->Form->input('pass_min_nonalphanumeric_chars', array('type'=>'text', 'value'=>$SITECONFIG["pass_min_nonalphanumeric_chars"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
						  </div>
						  
					</div>
					
					<div class="formbutton">
					<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_passwordsettingupdate',$SubadminAccessArray)){ ?>
						  <?php echo $this->Js->submit('Update', array(
								  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								  'update'=>'#UpdateMessage',
								  'class'=>'btnorange',
								  'div'=>false,
								  'controller'=>'Sitesetting',
								  'action'=>'passwordsettingupdate',
								  'url'   => array('controller' => 'sitesetting', 'action' => 'passwordsettingupdate')
								));?>
					<?php } ?>
					</div>
				</div>
			</div>
			
		<?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
			
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>