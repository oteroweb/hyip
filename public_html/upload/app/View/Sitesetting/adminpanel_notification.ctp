<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 12-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Website</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Main Details", array('controller'=>'sitesetting', "action"=>"index"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'class'=>'satting-menu-active'
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Member Tool Settings", array('controller'=>'sitesetting', "action"=>"shortenersetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Database Operations", array('controller'=>'sitesetting', "action"=>"maintenance"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>	
				</li>
				<li>
					<?php echo $this->Js->link("Logs Settings", array('controller'=>'sitesetting', "action"=>"logs"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Launch Date Settings", array('controller'=>'sitesetting', "action"=>"launch"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Currency Settings", array('controller'=>'sitesetting', "action"=>"currency"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Cronjob Settings", array('controller'=>'sitesetting', "action"=>"cronjob"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Notification", array('controller'=>'sitesetting', "action"=>"notification"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>

<?php if($IsAdminAccess){?>
<script type="text/javascript">$(function() {$('.colorpicker').wheelColorPicker({ sliders: "whsvp", preview: true, format: "css" });});</script>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Website_Settings#Notifications" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>

	  <?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'notificationupdate')));?>
	  
			<div class="tab-pane" id="profile">
				<div class="frommain">	  
					<div class="fromnewtext">Pending Text Ads :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
								<?php 
									echo $this->Form->input('textad', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $textad,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
									));
								?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Pending Banner Ads :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
								<?php 
									echo $this->Form->input('bannerad', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $bannerad,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
									));
								?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Pending Solo Ads :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
								<?php 
									echo $this->Form->input('soload', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $soload,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
									));
								?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Pending PPC Ads :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
								<?php 
									echo $this->Form->input('ppc', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $ppc,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
									));
								?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Pending PTC Ads :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
								<?php 
									echo $this->Form->input('ptc', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $ptc,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
									));
								?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Pending Login Ads :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
								<?php 
									echo $this->Form->input('loginad', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $loginad,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
									));
								?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Pending Biz Directory :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
								<?php 
									echo $this->Form->input('bizdir', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $bizdir,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
									));
								?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Pending Traffic Website :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
								<?php 
									echo $this->Form->input('website', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $website,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
									));
								?>
							</label>
						</div>
					</div>

					<div class="fromnewtext">Pending Withdrawal Requests :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
								<?php 
									echo $this->Form->input('withdrawal', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $withdrawal,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
									));
								?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Pending Add Fund Requests :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
								<?php 
									echo $this->Form->input('addfund', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $addfund,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
									));
								?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Member Tickets Awaiting Reply :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
								<?php 
									echo $this->Form->input('memberticket', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $memberticket,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
									));
								?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Public Tickets Awaiting Reply :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
								<?php 
									echo $this->Form->input('publicticket', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $publicticket,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
									));
								?>
							</label>
						</div>
					</div>
					
					<div class="fromnewtext">Pending Testimonials :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
								<?php 
									echo $this->Form->input('testimonial', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $testimonial,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
									));
								?>
							</label>
						</div>
					</div>
					
					<div class="formbutton">
					<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_advertisementtextadcreditupdate',$SubadminAccessArray)){ ?>
						  <?php echo $this->Js->submit('Update', array(
							'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'update'=>'#UpdateMessage',
							'class'=>'btnorange',
							'div'=>false,
							'controller'=>'sitesetting',
							'action'=>'notificationupdate',
							'url'   => array('controller' => 'sitesetting', 'action' => 'notificationupdate')
						  ));?>
					<?php } ?>
					</div>
				</div>
			</div>
	  <?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>