<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 13-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>

<script type="text/javascript">
function changesubmenu(submenu)
{
	var submenuclass = $(submenu).attr("class");
	$('div.submenu').hide(1,function() {
	   $('div.'+submenuclass).show();
	   $('li a').removeClass('active');
	   $('li.'+submenuclass+' a').addClass('active');
	});
	
	//$('.tablegrid.'+submenuclass+' .tablegridheader').removeClass('headfixedposition');
}
function checkall(submenu)
{
	var submenuclass = $(submenu).attr("class");
	if ($('input.'+submenuclass).attr("checked") == "checked"){
	  $('div.'+submenuclass+' input:checkbox').removeAttr('disabled').attr('checked','checked');
    } else {
      $('div.'+submenuclass+' input:checkbox').removeAttr('checked');
    }
}
</script>

<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'subadminaddaction')));?>
	  <?php if(isset($subadmindata['Subadmin']["id"]) && $copysubadmin==0){
	  		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$subadmindata['Subadmin']["id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	  }?>
	  
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Admin</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
		<ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Account Settings", array('controller'=>'sitesetting', "action"=>"account"), array(
					'update'=>'#settingpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Sub Admin", array('controller'=>'sitesetting', "action"=>"subadmin"), array(
					'update'=>'#settingpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
		</ul>
	</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Admin_Settings#Sub_Admin" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
	<div class="tab-pane" id="profile">
		<div class="frommain">
			<div class="fromnewtext">Status :<span class="red-color">*</span> </div>
			<div class="fromborderdropedown3">
				<div class="select-main">
				<label>
				<?php 
				  echo $this->Form->input('status', array(
					  'type' => 'select',
					  'options' => array('1'=>'Active', '0'=>'Inactive'),
					  'selected' => $subadmindata['Subadmin']["status"],
					  'class'=>'',
					  'label' => false,
					  'div' => false,
					  'style' => ''
				  ));
				?>
				</label>
				</div>
			</div>
			<div class="fromnewtext">First Name :<span class="red-color">*</span>  </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('f_name', array('type'=>'text', 'value'=>$subadmindata['Subadmin']["f_name"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<div class="fromnewtext">Last Name :<span class="red-color">*</span>  </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('l_name', array('type'=>'text', 'value'=>$subadmindata['Subadmin']["l_name"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<div class="fromnewtext">Email :<span class="red-color">*</span>  </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('email', array('type'=>'text', 'value'=>$subadmindata['Subadmin']["email"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<div class="fromnewtext">Usename :<span class="red-color">*</span>  </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('user_name', array('type'=>'text', 'value'=>$subadmindata['Subadmin']["user_name"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<div class="fromnewtext">Password :<span class="red-color">*</span>  </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('password', array('type'=>'password', 'value'=>$subadmindata['Subadmin']["password"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<div class="fromnewtext">Confirm Password :<span class="red-color">*</span>  </div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('confirm_pwd', array('type'=>'password', 'value'=>$subadmindata['Subadmin']["password"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<div class="fromnewtext">Access Rights : Select Rights Below</div>
			<div class="formbutton">
				<?php echo $this->Js->submit('Submit', array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'class'=>'btnorange',
				  'div'=>false,
				  'controller'=>'Sitesetting',
				  'action'=>'subadminaddaction',
				  'url'   => array('controller' => 'sitesetting', 'action' => 'subadminaddaction')
				));?>
				<?php echo $this->Js->link("Back", array('controller'=>'sitesetting', "action"=>"subadmin"), array(
					'update'=>'#settingpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'btngray',
					'div'=>false
				));?>
			</div>
		</div>
	</div>
<center>
	<div class="tab-innar">
		<ul>
			<li class="settings" onclick="changesubmenu(this);"><a class="active">Settings</a></li>
			<li class="member" onclick="changesubmenu(this);"><a>Member</a></li>
			<li class="finance" onclick="changesubmenu(this);"><a>Finance</a></li>
			<li class="design" onclick="changesubmenu(this);"><a>Design & CMS</a></li>
			<!--<li class="contentmanagement" onclick="changesubmenu(this);"><a>Content Management</a></li>-->
			<li class="promotools" onclick="changesubmenu(this);"><a>Promo Tools</a></li>
			<li class="advertisement" onclick="changesubmenu(this);"><a>Advertisement</a></li>
			<li class="logs" onclick="changesubmenu(this);"><a>Logs</a></li>
			<li class="managemodules" onclick="changesubmenu(this);"><a>Modules</a></li>
			<li class="support" onclick="changesubmenu(this);"><a>Support</a></li>
		</ul>
	</div>
</center>
<?php $selected = @explode(",",$subadmindata['Subadmin']["adminaccess"]); ?>
	<div class="submenu settings">
		<div class="addnew-button checkbox">
				<?php echo $this->Form->input('subaction.', array('type' => 'checkbox', 'id' => 'settingsall', 'class' => 'settings', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting', 'label'=>false, 'checked' => in_array('sitesetting',$selected), 'onclick'=>"checkall(this);")); ?><label for="settingsall"></label>
		</div>
		<div class="clearboth"></div>
    <div class="tablegrid">
        <div class="tablegridheader">
			<div>
				<?php echo "Submenu"; ?>&nbsp;&nbsp;
			</div>
			<div>All</div>
			<div>View</div>
			<div>Add</div>
			<div>Edit</div>
			<div>Status</div>
			<div>Delete</div>
			<div style="width:300px;">Extra</div>
		</div>
	<div class="tablegridrow">
		<div>Website/Main Details</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
				'id' => 'maindetails1',
			      'onclick' => 'selectMenuCheckboxes("maindetailIds",this.checked)'
			    ));?>
			<label for="maindetails1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'maindetails2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_index', 'label'=>false, 'class'=>'maindetailIds', 'onclick' => 'disableMenuCheckboxes("maindetailIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_index',$selected))); ?>
			<label for="maindetails2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'maindetails3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_maindetail', 'label'=>false, 'class'=>'maindetailIds', 'checked' => in_array('sitesetting/adminpanel_maindetail',$selected), 'disabled' => !in_array('sitesetting/adminpanel_index',$selected))); ?>
			<label for="maindetails3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Website/Member Tool/Shortener Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'shortenersett1',
			      'onclick' => 'selectMenuCheckboxes("shortenerIds",this.checked)'
			    ));?>
			<label for="shortenersett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'shortenersett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_shortenersetting', 'label'=>false, 'class'=>'shortenerIds', 'onclick' => 'disableMenuCheckboxes("shortenerIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_shortenersetting',$selected))); ?>
			<label for="shortenersett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'shortenersett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_shortenersettingaction', 'label'=>false, 'class'=>'shortenerIds', 'checked' => in_array('sitesetting/adminpanel_shortenersettingaction',$selected), 'disabled' => !in_array('sitesetting/adminpanel_shortenersetting',$selected))); ?>
			<label for="shortenersett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Website/Member Tool/Rotator Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'rotatorsett1',
			      'onclick' => 'selectMenuCheckboxes("rotatorIds",this.checked)'
			    ));?>
			<label for="rotatorsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'rotatorsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_rotatorsetting', 'label'=>false, 'class'=>'rotatorIds', 'onclick' => 'disableMenuCheckboxes("rotatorIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_rotatorsetting',$selected))); ?>
			<label for="rotatorsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'rotatorsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_rotatorsettingaction', 'label'=>false, 'class'=>'rotatorIds', 'checked' => in_array('sitesetting/adminpanel_rotatorsettingaction',$selected), 'disabled' => !in_array('sitesetting/adminpanel_rotatorsetting',$selected))); ?>
			<label for="rotatorsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Website/Member Tool/Dynamic Banner Ad Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'banneradsett1',
			      'onclick' => 'selectMenuCheckboxes("mtdynamicbannerIds",this.checked)'
			    ));?>
			<label for="banneradsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'banneradsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_dynamicbanner', 'label'=>false, 'class'=>'mtdynamicbannerIds', 'onclick' => 'disableMenuCheckboxes("mtdynamicbannerIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_dynamicbanner',$selected))); ?>
			<label for="banneradsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'banneradsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_dynamicbanneraction', 'label'=>false, 'class'=>'mtdynamicbannerIds', 'checked' => in_array('sitesetting/adminpanel_dynamicbanneraction',$selected), 'disabled' => !in_array('sitesetting/adminpanel_dynamicbanner',$selected))); ?>
			<label for="banneradsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Website/Database Operations</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'dbopsett1',
			      'onclick' => 'selectMenuCheckboxes("maintenanceIds",this.checked)'
			    ));?>
			<label for="dbopsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'dbopsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_maintenance', 'label'=>false, 'class'=>'maintenanceIds', 'onclick' => 'disableMenuCheckboxes("maintenanceIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_maintenance',$selected))); ?>
			<label for="dbopsett2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'dbopsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_backupdatabase', 'label'=>false, 'class'=>'maintenanceIds', 'checked' => in_array('sitesetting/adminpanel_backupdatabase',$selected), 'disabled' => !in_array('sitesetting/adminpanel_maintenance',$selected))); ?>
				<label for="dbopsett3">Backup</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'dbopsett4', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_backuprestore', 'label'=>false, 'class'=>'maintenanceIds', 'checked' => in_array('sitesetting/adminpanel_backuprestore',$selected), 'disabled' => !in_array('sitesetting/adminpanel_maintenance',$selected))); ?>
				<label for="dbopsett4">Restore</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'dbopsett5', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_dbrepair', 'label'=>false, 'class'=>'maintenanceIds', 'checked' => in_array('sitesetting/adminpanel_dbrepair',$selected), 'disabled' => !in_array('sitesetting/adminpanel_maintenance',$selected))); ?>
				<label for="dbopsett5">Repair</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'dbopsett6', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_backupremove', 'label'=>false, 'class'=>'maintenanceIds', 'checked' => in_array('sitesetting/adminpanel_backupremove',$selected), 'disabled' => !in_array('sitesetting/adminpanel_maintenance',$selected))); ?>
				<label for="dbopsett6">Remove Backup</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'dbopsett7', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_autobackupsetting', 'label'=>false, 'class'=>'maintenanceIds', 'checked' => in_array('sitesetting/adminpanel_autobackupsetting',$selected), 'disabled' => !in_array('sitesetting/adminpanel_maintenance',$selected))); ?>
				<label for="dbopsett7">Auto Backup Setting</label>
			</div>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Website/Logs Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'logssett1',
			      'onclick' => 'selectMenuCheckboxes("logIds",this.checked)'
			    ));?>
			<label for="logssett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'logssett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_logs', 'label'=>false, 'class'=>'logIds', 'onclick' => 'disableMenuCheckboxes("logIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_logs',$selected))); ?>
			<label for="logssett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'logssett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_logsupdate', 'label'=>false, 'class'=>'logIds', 'checked' => in_array('sitesetting/adminpanel_logsupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_logs',$selected))); ?>
			<label for="logssett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Website/Launch Date Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'launchsett1',
			      'onclick' => 'selectMenuCheckboxes("launchIds",this.checked)'
			    ));?>
			<label for="launchsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'launchsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_launch', 'label'=>false, 'class'=>'launchIds', 'onclick' => 'disableMenuCheckboxes("launchIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_launch',$selected))); ?>
			<label for="launchsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'launchsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_launchupdate', 'label'=>false, 'class'=>'launchIds', 'checked' => in_array('sitesetting/adminpanel_launchupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_launch',$selected))); ?>
			<label for="launchsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Website/Currency Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'currencysett1',
			      'onclick' => 'selectMenuCheckboxes("currencyIds",this.checked)'
			    ));?>
			<label for="currencysett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'currencysett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_currency', 'label'=>false, 'class'=>'currencyIds', 'onclick' => 'disableMenuCheckboxes("currencyIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_currency',$selected))); ?>
			<label for="currencysett2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'currencysett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_currencyadd', 'label'=>false, 'class'=>'currencyIds', 'checked' => in_array('sitesetting/adminpanel_currencyadd',$selected), 'disabled' => !in_array('sitesetting/adminpanel_currency',$selected))); ?>
			<label for="currencysett3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'currencysett4', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_currencyadd/$', 'label'=>false, 'class'=>'currencyIds', 'checked' => in_array('sitesetting/adminpanel_currencyadd/$',$selected), 'disabled' => !in_array('sitesetting/adminpanel_currency',$selected))); ?>
			<label for="currencysett4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'currencysett5', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_currencystatus', 'label'=>false, 'class'=>'currencyIds', 'checked' => in_array('sitesetting/adminpanel_currencystatus',$selected), 'disabled' => !in_array('sitesetting/adminpanel_currency',$selected))); ?>
			<label for="currencysett5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'currencysett6', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_currencyremove', 'label'=>false, 'class'=>'currencyIds', 'checked' => in_array('sitesetting/adminpanel_currencyremove',$selected), 'disabled' => !in_array('sitesetting/adminpanel_currency',$selected))); ?>
			<label for="currencysett6"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'currencysett7', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_currencyrate', 'label'=>false, 'class'=>'currencyIds', 'checked' => in_array('sitesetting/adminpanel_currencyrate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_currency',$selected))); ?>
			<label for="currencysett7">Update Exchanges Rates</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Website/Cronjob Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'cronjobsett1',
			      'onclick' => 'selectMenuCheckboxes("cronjobsettIds",this.checked)'
			    ));?>
			<label for="cronjobsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'cronjobsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_cronjob', 'label'=>false, 'class'=>'cronjobsettIds', 'onclick' => 'disableMenuCheckboxes("cronjobsettIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_cronjob',$selected))); ?>
			<label for="cronjobsett2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'cronjobsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_cronjobrun', 'label'=>false, 'class'=>'cronjobsettIds', 'checked' => in_array('sitesetting/adminpanel_cronjobrun',$selected), 'disabled' => !in_array('sitesetting/adminpanel_cronjob',$selected))); ?>
			<label for="cronjobsett3">Run Manually</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Admin/Account Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'accountsett1',
			      'onclick' => 'selectMenuCheckboxes("accountIds",this.checked)'
			    ));?>
			<label for="accountsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'accountsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_account', 'label'=>false, 'class'=>'accountIds', 'onclick' => 'disableMenuCheckboxes("accountIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_account',$selected))); ?>
			<label for="accountsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'accountsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_accountupdate', 'label'=>false, 'class'=>'accountIds', 'checked' => in_array('sitesetting/adminpanel_accountupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_account',$selected))); ?>
			<label for="accountsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Finance/Payment Processor</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'processorsett1',
			      'onclick' => 'selectMenuCheckboxes("paymentprocessorIds",this.checked)'
			    ));?>
			<label for="processorsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'processorsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_paymentprocessor', 'label'=>false, 'class'=>'paymentprocessorIds', 'onclick' => 'disableMenuCheckboxes("paymentprocessorIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_paymentprocessor',$selected))); ?>
			<label for="processorsett2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'processorsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_paymentprocessoradd', 'label'=>false, 'class'=>'paymentprocessorIds', 'checked' => in_array('sitesetting/adminpanel_paymentprocessoradd',$selected), 'disabled' => !in_array('sitesetting/adminpanel_paymentprocessor',$selected))); ?>
			<label for="processorsett3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'processorsett4', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_paymentprocessoradd/$', 'label'=>false, 'class'=>'paymentprocessorIds', 'checked' => in_array('sitesetting/adminpanel_paymentprocessoradd/$',$selected), 'disabled' => !in_array('sitesetting/adminpanel_paymentprocessor',$selected))); ?>
			<label for="processorsett4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'processorsett5', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_paymentprocessorstatus', 'label'=>false, 'class'=>'paymentprocessorIds', 'checked' => in_array('sitesetting/adminpanel_paymentprocessorstatus',$selected), 'disabled' => !in_array('sitesetting/adminpanel_paymentprocessor',$selected))); ?>
			<label for="processorsett5"></label>
		</div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Finance/Add Fund Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'addfundsett1',
			      'onclick' => 'selectMenuCheckboxes("addfundIds",this.checked)'
			    ));?>
			<label for="addfundsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'addfundsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_addfund', 'label'=>false, 'class'=>'addfundIds', 'onclick' => 'disableMenuCheckboxes("addfundIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_addfund',$selected))); ?>
			<label for="addfundsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'addfundsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_addfundupdate', 'label'=>false, 'class'=>'addfundIds', 'checked' => in_array('sitesetting/adminpanel_addfundupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_addfund',$selected))); ?>
			<label for="addfundsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Finance/Withdrawal Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'withdrawalsett1',
			      'onclick' => 'selectMenuCheckboxes("withdrawalIds",this.checked)'
			    ));?>
			<label for="withdrawalsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'withdrawalsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_withdrawal', 'label'=>false, 'class'=>'withdrawalIds', 'onclick' => 'disableMenuCheckboxes("withdrawalIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_withdrawal',$selected))); ?>
			<label for="withdrawalsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'withdrawalsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_withdrawalupdate', 'label'=>false, 'class'=>'withdrawalIds', 'checked' => in_array('sitesetting/adminpanel_withdrawalupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_withdrawal',$selected))); ?>
			<label for="withdrawalsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Advertisement/Credit Banner Ads Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'crbannersett1',
			      'onclick' => 'selectMenuCheckboxes("creditbanneradsIds",this.checked)'
			    ));?>
			<label for="crbannersett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'crbannersett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_advertisementcredit', 'label'=>false, 'class'=>'creditbanneradsIds', 'onclick' => 'disableMenuCheckboxes("creditbanneradsIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_advertisementcredit',$selected))); ?>
			<label for="crbannersett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'crbannersett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_advertisementcreditupdate', 'label'=>false, 'class'=>'creditbanneradsIds', 'checked' => in_array('sitesetting/adminpanel_advertisementcreditupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_advertisementcredit',$selected))); ?>
			<label for="crbannersett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Advertisement/Plan Banner Ads Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'planbannersett1',
			      'onclick' => 'selectMenuCheckboxes("planbanneradsIds",this.checked)'
			    ));?>
			<label for="planbannersett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'planbannersett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_banneradplan', 'label'=>false, 'class'=>'planbanneradsIds', 'onclick' => 'disableMenuCheckboxes("planbanneradsIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_banneradplan',$selected))); ?>
			<label for="planbannersett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'planbannersett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_banneradplanaction', 'label'=>false, 'class'=>'planbanneradsIds', 'checked' => in_array('sitesetting/adminpanel_banneradplanaction',$selected), 'disabled' => !in_array('sitesetting/adminpanel_banneradplan',$selected))); ?>
			<label for="planbannersett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Advertisement/Text Ad/Common Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'textadcommonsett1',
			      'onclick' => 'selectMenuCheckboxes("advtextadcommonIds",this.checked)'
			    ));?>
			<label for="textadcommonsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'textadcommonsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_advertisementtextadsetting', 'label'=>false, 'class'=>'advtextadcommonIds', 'onclick' => 'disableMenuCheckboxes("advtextadcommonIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_advertisementtextadsetting',$selected))); ?>
			<label for="textadcommonsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'textadcommonsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_advertisementtextadsettingupdate', 'label'=>false, 'class'=>'advtextadcommonIds', 'checked' => in_array('sitesetting/adminpanel_advertisementtextadsettingupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_advertisementtextadsetting',$selected))); ?>
			<label for="textadcommonsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Advertisement/Credit Text Ads Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'crtextadsett1',
			      'onclick' => 'selectMenuCheckboxes("advcredittextadIds",this.checked)'
			    ));?>
			<label for="crtextadsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'crtextadsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_advertisementtextadcredit', 'label'=>false, 'class'=>'advcredittextadIds', 'onclick' => 'disableMenuCheckboxes("advcredittextadIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_advertisementtextadcredit',$selected))); ?>
			<label for="crtextadsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'crtextadsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_advertisementtextadcreditupdate', 'label'=>false, 'class'=>'advcredittextadIds', 'checked' => in_array('sitesetting/adminpanel_advertisementtextadcreditupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_advertisementtextadcredit',$selected))); ?>
			<label for="crtextadsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Advertisement/Plan Text Ads Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'plantextadsett1',
			      'onclick' => 'selectMenuCheckboxes("advplantextadIds",this.checked)'
			    ));?>
			<label for="plantextadsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'plantextadsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_textadplan', 'label'=>false, 'class'=>'advplantextadIds', 'onclick' => 'disableMenuCheckboxes("advplantextadIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_textadplan',$selected))); ?>
			<label for="plantextadsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'plantextadsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_textadplanaction', 'label'=>false, 'class'=>'advplantextadIds', 'checked' => in_array('sitesetting/adminpanel_textadplanaction',$selected), 'disabled' => !in_array('sitesetting/adminpanel_textadplan',$selected))); ?>
			<label for="plantextadsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Advertisement/Credit Solo Ads Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'crsoloadsett1',
			      'onclick' => 'selectMenuCheckboxes("advcreditsoloadIds",this.checked)'
			    ));?>
			<label for="crsoloadsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'crsoloadsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_soload', 'label'=>false, 'class'=>'advcreditsoloadIds', 'onclick' => 'disableMenuCheckboxes("advcreditsoloadIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_soload',$selected))); ?>
			<label for="crsoloadsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'crsoloadsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_soloadupdate', 'label'=>false, 'class'=>'advcreditsoloadIds', 'checked' => in_array('sitesetting/adminpanel_soloadupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_soload',$selected))); ?>
			<label for="crsoloadsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Advertisement/Plan Solo Ads Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'plansoloadsett1',
			      'onclick' => 'selectMenuCheckboxes("advplansoloadIds",this.checked)'
			    ));?>
			<label for="plansoloadsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'plansoloadsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_soloadplan', 'label'=>false, 'class'=>'advplansoloadIds', 'onclick' => 'disableMenuCheckboxes("advplansoloadIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_soloadplan',$selected))); ?>
			<label for="plansoloadsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'plansoloadsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_soloadplanaction', 'label'=>false, 'class'=>'advplansoloadIds', 'checked' => in_array('sitesetting/adminpanel_soloadplanaction',$selected), 'disabled' => !in_array('sitesetting/adminpanel_soloadplan',$selected))); ?>
			<label for="plansoloadsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Advertisement/Login Ads Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'loginadsett1',
			      'onclick' => 'selectMenuCheckboxes("advloginadIds",this.checked)'
			    ));?>
			<label for="loginadsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'loginadsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_loginadsetting', 'label'=>false, 'class'=>'advloginadIds', 'onclick' => 'disableMenuCheckboxes("advloginadIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_loginadsetting',$selected))); ?>
			<label for="loginadsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'loginadsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_loginadsettingaction', 'label'=>false, 'class'=>'advloginadIds', 'checked' => in_array('sitesetting/adminpanel_loginadsettingaction',$selected), 'disabled' => !in_array('sitesetting/adminpanel_loginadsetting',$selected))); ?>
			<label for="loginadsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Advertisement/PPC Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'ppcsett1',
			      'onclick' => 'selectMenuCheckboxes("advppcIds",this.checked)'
			    ));?>
			<label for="ppcsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'ppcsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_ppcsetting', 'label'=>false, 'class'=>'advppcIds', 'onclick' => 'disableMenuCheckboxes("advppcIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_ppcsetting',$selected))); ?>
			<label for="ppcsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'ppcsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_ppcsettingaction', 'label'=>false, 'class'=>'advppcIds', 'checked' => in_array('sitesetting/adminpanel_ppcsettingaction',$selected), 'disabled' => !in_array('sitesetting/adminpanel_ppcsetting',$selected))); ?>
			<label for="ppcsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Advertisement/PTC Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'ptcsett1',
			      'onclick' => 'selectMenuCheckboxes("advptcIds",this.checked)'
			    ));?>
			<label for="ptcsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'ptcsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_ptcsetting', 'label'=>false, 'class'=>'advptcIds', 'onclick' => 'disableMenuCheckboxes("advptcIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_ptcsetting',$selected))); ?>
			<label for="ptcsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'ptcsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_ptcsettingaction', 'label'=>false, 'class'=>'advptcIds', 'checked' => in_array('sitesetting/adminpanel_ptcsettingaction',$selected), 'disabled' => !in_array('sitesetting/adminpanel_ptcsetting',$selected))); ?>
			<label for="ptcsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Membership Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'membershipsett1',
			      'onclick' => 'selectMenuCheckboxes("membershipIds",this.checked)'
			    ));?>
			<label for="membershipsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'membershipsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_membership', 'label'=>false, 'class'=>'membershipIds', 'onclick' => 'disableMenuCheckboxes("membershipIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_membership',$selected))); ?>
			<label for="membershipsett2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'membershipsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_membershipplanadd', 'label'=>false, 'class'=>'membershipIds', 'checked' => in_array('sitesetting/adminpanel_membershipplanadd',$selected), 'disabled' => !in_array('sitesetting/adminpanel_membership',$selected))); ?>
			<label for="membershipsett3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'membershipsett4', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_membershipplanadd/$', 'label'=>false, 'class'=>'membershipIds', 'checked' => in_array('sitesetting/adminpanel_membershipplanadd/$',$selected), 'disabled' => !in_array('sitesetting/adminpanel_membership',$selected))); ?>
			<label for="membershipsett4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'membershipsett5', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_membershipstatus', 'label'=>false, 'class'=>'membershipIds', 'checked' => in_array('sitesetting/adminpanel_membershipstatus',$selected), 'disabled' => !in_array('sitesetting/adminpanel_membership',$selected))); ?>
			<label for="membershipsett5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'membershipsett6', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_membershipremove', 'label'=>false, 'class'=>'membershipIds', 'checked' => in_array('sitesetting/adminpanel_membershipremove',$selected), 'disabled' => !in_array('sitesetting/adminpanel_membership',$selected))); ?>
			<label for="membershipsett6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Balance Transfer/Member to Member</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'btmtomsett1',
			      'onclick' => 'selectMenuCheckboxes("balancetransferIds",this.checked)'
			    ));?>
			<label for="btmtomsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'btmtomsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_balancetransfer', 'label'=>false, 'class'=>'balancetransferIds', 'onclick' => 'disableMenuCheckboxes("balancetransferIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_balancetransfer',$selected))); ?>
			<label for="btmtomsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'btmtomsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_balancetransferaction', 'label'=>false, 'class'=>'balancetransferIds', 'checked' => in_array('sitesetting/adminpanel_balancetransferaction',$selected), 'disabled' => !in_array('sitesetting/adminpanel_balancetransfer',$selected))); ?>
			<label for="btmtomsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Balance Transfer/Internal Transfer</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'btitsett1',
			      'onclick' => 'selectMenuCheckboxes("balancetransinternalIds",this.checked)'
			    ));?>
			<label for="btitsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'btitsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_balancetransferprocessor', 'label'=>false, 'class'=>'balancetransinternalIds', 'onclick' => 'disableMenuCheckboxes("balancetransinternalIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_balancetransferprocessor',$selected))); ?>
			<label for="btitsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'btitsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_balancetransferaction', 'label'=>false, 'class'=>'balancetransinternalIds', 'checked' => in_array('sitesetting/adminpanel_balancetransferaction',$selected), 'disabled' => !in_array('sitesetting/adminpanel_balancetransferprocessor',$selected))); ?>
			<label for="btitsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Email/SMTP Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'smtpsett1',
			      'onclick' => 'selectMenuCheckboxes("smtpIds",this.checked)'
			    ));?>
			<label for="smtpsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'smtpsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_smtp', 'label'=>false, 'class'=>'smtpIds', 'onclick' => 'disableMenuCheckboxes("smtpIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_smtp',$selected))); ?>
			<label for="smtpsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'smtpsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_smtpupdate', 'label'=>false, 'class'=>'smtpIds', 'checked' => in_array('sitesetting/adminpanel_smtpupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_smtp',$selected))); ?>
			<label for="smtpsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Email/Autoresponder</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'autorespondersett1',
			      'onclick' => 'selectMenuCheckboxes("autoresponderIds",this.checked)'
			    ));?>
			<label for="autorespondersett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'autorespondersett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_autoresponder', 'label'=>false, 'class'=>'autoresponderIds', 'onclick' => 'disableMenuCheckboxes("autoresponderIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_autoresponder',$selected))); ?>
			<label for="autorespondersett2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'autorespondersett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_autoresponderadd', 'label'=>false, 'class'=>'autoresponderIds', 'checked' => in_array('sitesetting/adminpanel_autoresponderadd',$selected), 'disabled' => !in_array('sitesetting/adminpanel_autoresponder',$selected))); ?>
			<label for="autorespondersett3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'autorespondersett4', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_autoresponderadd/$', 'label'=>false, 'class'=>'autoresponderIds', 'checked' => in_array('sitesetting/adminpanel_autoresponderadd/$',$selected), 'disabled' => !in_array('sitesetting/adminpanel_autoresponder',$selected))); ?>
			<label for="autorespondersett4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'autorespondersett5', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_autoresponderstatus', 'label'=>false, 'class'=>'autoresponderIds', 'checked' => in_array('sitesetting/adminpanel_autoresponderstatus',$selected), 'disabled' => !in_array('sitesetting/adminpanel_autoresponder',$selected))); ?>
			<label for="autorespondersett5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'autorespondersett6', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_autoresponderremove', 'label'=>false, 'class'=>'autoresponderIds', 'checked' => in_array('sitesetting/adminpanel_autoresponderremove',$selected), 'disabled' => !in_array('sitesetting/adminpanel_autoresponder',$selected))); ?>
			<label for="autorespondersett6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Email/Account Activation Mail</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'accactivationsett1',
			      'onclick' => 'selectMenuCheckboxes("activationautomailIds",this.checked)'
			    ));?>
			<label for="accactivationsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'accactivationsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_activationautomail', 'label'=>false, 'class'=>'activationautomailIds', 'onclick' => 'disableMenuCheckboxes("activationautomailIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_activationautomail',$selected))); ?>
			<label for="accactivationsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'accactivationsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_activationautomailupdate', 'label'=>false, 'class'=>'activationautomailIds', 'checked' => in_array('sitesetting/adminpanel_activationautomailupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_activationautomail',$selected))); ?>
			<label for="accactivationsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Email/Unpaid Member Notifications</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'unpaidmembersett1',
			      'onclick' => 'selectMenuCheckboxes("unpaidmembernotificationIds",this.checked)'
			    ));?>
			<label for="unpaidmembersett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'unpaidmembersett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_unpaidmembernotification', 'label'=>false, 'class'=>'unpaidmembernotificationIds', 'onclick' => 'disableMenuCheckboxes("unpaidmembernotificationIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_unpaidmembernotification',$selected))); ?>
			<label for="unpaidmembersett2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'unpaidmembersett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_unpaidmembernotificationadd', 'label'=>false, 'class'=>'unpaidmembernotificationIds', 'checked' => in_array('sitesetting/adminpanel_unpaidmembernotificationadd',$selected), 'disabled' => !in_array('sitesetting/adminpanel_unpaidmembernotification',$selected))); ?>
			<label for="unpaidmembersett3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'unpaidmembersett4', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_unpaidmembernotificationadd/$', 'label'=>false, 'class'=>'unpaidmembernotificationIds', 'checked' => in_array('sitesetting/adminpanel_unpaidmembernotificationadd/$',$selected), 'disabled' => !in_array('sitesetting/adminpanel_unpaidmembernotification',$selected))); ?>
			<label for="unpaidmembersett4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'unpaidmembersett5', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_unpaidmembernotificationstatus', 'label'=>false, 'class'=>'unpaidmembernotificationIds', 'checked' => in_array('sitesetting/adminpanel_unpaidmembernotificationstatus',$selected), 'disabled' => !in_array('sitesetting/adminpanel_unpaidmembernotification',$selected))); ?>
			<label for="unpaidmembersett5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'unpaidmembersett6', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_unpaidmembernotificationremove', 'label'=>false, 'class'=>'unpaidmembernotificationIds', 'checked' => in_array('sitesetting/adminpanel_unpaidmembernotificationremove',$selected), 'disabled' => !in_array('sitesetting/adminpanel_unpaidmembernotification',$selected))); ?>
			<label for="unpaidmembersett6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Email/Inactive Member Mail</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'inactivemembersett1',
			      'onclick' => 'selectMenuCheckboxes("inactivemembermailIds",this.checked)'
			    ));?>
			<label for="inactivemembersett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'inactivemembersett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_inactivemembermail', 'label'=>false, 'class'=>'inactivemembermailIds', 'onclick' => 'disableMenuCheckboxes("inactivemembermailIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_inactivemembermail',$selected))); ?>
			<label for="inactivemembersett2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'inactivemembersett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_inactivemembermailadd', 'label'=>false, 'class'=>'inactivemembermailIds', 'checked' => in_array('sitesetting/adminpanel_inactivemembermailadd',$selected), 'disabled' => !in_array('sitesetting/adminpanel_inactivemembermail',$selected))); ?>
			<label for="inactivemembersett3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'inactivemembersett4', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_inactivemembermailadd/$', 'label'=>false, 'class'=>'inactivemembermailIds', 'checked' => in_array('sitesetting/adminpanel_inactivemembermailadd/$',$selected), 'disabled' => !in_array('sitesetting/adminpanel_inactivemembermail',$selected))); ?>
			<label for="inactivemembersett4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'inactivemembersett5', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_inactivemembermailstatus', 'label'=>false, 'class'=>'inactivemembermailIds', 'checked' => in_array('sitesetting/adminpanel_inactivemembermailstatus',$selected), 'disabled' => !in_array('sitesetting/adminpanel_inactivemembermail',$selected))); ?>
			<label for="inactivemembersett5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'inactivemembersett6', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_inactivemembermailremove', 'label'=>false, 'class'=>'inactivemembermailIds', 'checked' => in_array('sitesetting/adminpanel_inactivemembermailremove',$selected), 'disabled' => !in_array('sitesetting/adminpanel_inactivemembermail',$selected))); ?>
			<label for="inactivemembersett6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Email/Membership Template Emails</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'membershipemailsett1',
			      'onclick' => 'selectMenuCheckboxes("membershiptmplmailIds",this.checked)'
			    ));?>
			<label for="membershipemailsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'membershipemailsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_membershipemailtemplate', 'label'=>false, 'class'=>'membershiptmplmailIds', 'onclick' => 'disableMenuCheckboxes("membershiptmplmailIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_membershipemailtemplate',$selected))); ?>
			<label for="membershipemailsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'membershipemailsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_membershipemailtemplateedit', 'label'=>false, 'class'=>'membershiptmplmailIds', 'checked' => in_array('sitesetting/adminpanel_membershipemailtemplateedit',$selected), 'disabled' => !in_array('sitesetting/adminpanel_membershipemailtemplate',$selected))); ?>
			<label for="membershipemailsett3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'membershipemailsett4', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_membershipemailtemplatestatus', 'label'=>false, 'class'=>'membershiptmplmailIds', 'checked' => in_array('sitesetting/adminpanel_membershipemailtemplatestatus',$selected), 'disabled' => !in_array('sitesetting/adminpanel_membershipemailtemplate',$selected))); ?>
			<label for="membershipemailsett4"></label>
		</div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Security/Common Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'securitycommonsett1',
			      'onclick' => 'selectMenuCheckboxes("securitycommonIds",this.checked)'
			    ));?>
			<label for="securitycommonsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'securitycommonsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_antibrute', 'label'=>false, 'class'=>'securitycommonIds', 'onclick' => 'disableMenuCheckboxes("securitycommonIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_antibrute',$selected))); ?>
			<label for="securitycommonsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'securitycommonsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_antibruteupdate', 'label'=>false, 'class'=>'securitycommonIds', 'checked' => in_array('sitesetting/adminpanel_antibruteupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_antibrute',$selected))); ?>
			<label for="securitycommonsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Security/Password Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'securitypasssett1',
			      'onclick' => 'selectMenuCheckboxes("securitypasswordIds",this.checked)'
			    ));?>
			<label for="securitypasssett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'securitypasssett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_passwordsetting', 'label'=>false, 'class'=>'securitypasswordIds', 'onclick' => 'disableMenuCheckboxes("securitypasswordIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_passwordsetting',$selected))); ?>
			<label for="securitypasssett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'securitypasssett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_passwordsettingupdate', 'label'=>false, 'class'=>'securitypasswordIds', 'checked' => in_array('sitesetting/adminpanel_passwordsettingupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_passwordsetting',$selected))); ?>
			<label for="securitypasssett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Security/Website Security</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'securitywebsett1',
			      'onclick' => 'selectMenuCheckboxes("websitesecurityIds",this.checked)'
			    ));?>
			<label for="securitywebsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'securitywebsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_websitesecurity', 'label'=>false, 'class'=>'websitesecurityIds', 'onclick' => 'disableMenuCheckboxes("websitesecurityIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_websitesecurity',$selected))); ?>
			<label for="securitywebsett2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'securitywebsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_websitesecurityadd', 'label'=>false, 'class'=>'websitesecurityIds', 'checked' => in_array('sitesetting/adminpanel_websitesecurityadd',$selected), 'disabled' => !in_array('sitesetting/adminpanel_websitesecurity',$selected))); ?>
			<label for="securitywebsett3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'securitywebsett4', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_websitesecurityadd/$', 'label'=>false, 'class'=>'websitesecurityIds', 'checked' => in_array('sitesetting/adminpanel_websitesecurityadd/$',$selected), 'disabled' => !in_array('sitesetting/adminpanel_websitesecurity',$selected))); ?>
			<label for="securitywebsett4"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'securitywebsett5', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_websitesecurityremove', 'label'=>false, 'class'=>'websitesecurityIds', 'checked' => in_array('sitesetting/adminpanel_websitesecurityremove',$selected), 'disabled' => !in_array('sitesetting/adminpanel_websitesecurity',$selected))); ?>
			<label for="securitywebsett5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Security/Captcha Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'securitycaptchasett1',
			      'onclick' => 'selectMenuCheckboxes("captchaIds",this.checked)'
			    ));?>
			<label for="securitycaptchasett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'securitycaptchasett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_captcha', 'label'=>false, 'class'=>'captchaIds', 'onclick' => 'disableMenuCheckboxes("captchaIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_captcha',$selected))); ?>
			<label for="securitycaptchasett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'securitycaptchasett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_captchaupdate', 'label'=>false, 'class'=>'captchaIds', 'checked' => in_array('sitesetting/adminpanel_captchaupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_captcha',$selected))); ?>
			<label for="securitycaptchasett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Signup/Registration Form</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'signupregformsett1',
			      'onclick' => 'selectMenuCheckboxes("registrationformIds",this.checked)'
			    ));?>
			<label for="signupregformsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'signupregformsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_registrationform', 'label'=>false, 'class'=>'registrationformIds', 'onclick' => 'disableMenuCheckboxes("registrationformIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_registrationform',$selected))); ?>
			<label for="signupregformsett2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'signupregformsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_registrationformadd', 'label'=>false, 'class'=>'registrationformIds', 'checked' => in_array('sitesetting/adminpanel_registrationformadd',$selected), 'disabled' => !in_array('sitesetting/adminpanel_registrationform',$selected))); ?>
			<label for="signupregformsett3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'signupregformsett4', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_registrationformadd/$', 'label'=>false, 'class'=>'registrationformIds', 'checked' => in_array('sitesetting/adminpanel_registrationformadd/$',$selected), 'disabled' => !in_array('sitesetting/adminpanel_registrationform',$selected))); ?>
			<label for="signupregformsett4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'signupregformsett5', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_registrationformaction', 'label'=>false, 'class'=>'registrationformIds', 'checked' => in_array('sitesetting/adminpanel_registrationformaction',$selected), 'disabled' => !in_array('sitesetting/adminpanel_registrationform',$selected))); ?>
			<label for="signupregformsett5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'signupregformsett6', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_registrationformremove', 'label'=>false, 'class'=>'registrationformIds', 'checked' => in_array('sitesetting/adminpanel_registrationformremove',$selected), 'disabled' => !in_array('sitesetting/adminpanel_registrationform',$selected))); ?>
			<label for="signupregformsett6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Signup/Profile Form Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'signupprofilesett1',
			      'onclick' => 'selectMenuCheckboxes("profileformIds",this.checked)'
			    ));?>
			<label for="signupprofilesett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'signupprofilesett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_profileform', 'label'=>false, 'class'=>'profileformIds', 'onclick' => 'disableMenuCheckboxes("profileformIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_profileform',$selected))); ?>
			<label for="signupprofilesett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'signupprofilesett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_profileformaction', 'label'=>false, 'class'=>'profileformIds', 'checked' => in_array('sitesetting/adminpanel_profileformaction',$selected), 'disabled' => !in_array('sitesetting/adminpanel_profileform',$selected))); ?>
			<label for="signupprofilesett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Signup/Free Credit & Bonus</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'signupcrsett1',
			      'onclick' => 'selectMenuCheckboxes("freecreditandbonusIds",this.checked)'
			    ));?>
			<label for="signupcrsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'signupcrsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_freecreditandbonus', 'label'=>false, 'class'=>'freecreditandbonusIds', 'onclick' => 'disableMenuCheckboxes("freecreditandbonusIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_freecreditandbonus',$selected))); ?>
			<label for="signupcrsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'signupcrsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_freecreditandbonusupdate', 'label'=>false, 'class'=>'freecreditandbonusIds', 'checked' => in_array('sitesetting/adminpanel_freecreditandbonusupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_freecreditandbonus',$selected))); ?>
			<label for="signupcrsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Signup/Signup Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'signupsignupsett1',
			      'onclick' => 'selectMenuCheckboxes("signupsettIds",this.checked)'
			    ));?>
			<label for="signupsignupsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'signupsignupsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_signup', 'label'=>false, 'class'=>'signupsettIds', 'onclick' => 'disableMenuCheckboxes("signupsettIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_signup',$selected))); ?>
			<label for="signupsignupsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'signupsignupsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_signpupdate', 'label'=>false, 'class'=>'signupsettIds', 'checked' => in_array('sitesetting/adminpanel_signpupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_signup',$selected))); ?>
			<label for="signupsignupsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Signup/Facebook & Google</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'signupfbgsett1',
			      'onclick' => 'selectMenuCheckboxes("fbgoogleIds",this.checked)'
			    ));?>
			<label for="signupfbgsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'signupfbgsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_facebookgoogle', 'label'=>false, 'class'=>'fbgoogleIds', 'onclick' => 'disableMenuCheckboxes("fbgoogleIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_facebookgoogle',$selected))); ?>
			<label for="signupfbgsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'signupfbgsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_facebookgoogleupdate', 'label'=>false, 'class'=>'fbgoogleIds', 'checked' => in_array('sitesetting/adminpanel_facebookgoogleupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_facebookgoogle',$selected))); ?>
			<label for="signupfbgsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Referral/Referral Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'referralsett1',
			      'onclick' => 'selectMenuCheckboxes("referralsettIds",this.checked)'
			    ));?>
			<label for="referralsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'referralsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_referral', 'label'=>false, 'class'=>'referralsettIds', 'onclick' => 'disableMenuCheckboxes("referralsettIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_referral',$selected))); ?>
			<label for="referralsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'referralsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_referralupdate', 'label'=>false, 'class'=>'referralsettIds', 'checked' => in_array('sitesetting/adminpanel_referralupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_referral',$selected))); ?>
			<label for="referralsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Referral/Add. Referral Comm. Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'referraladdsett1',
			      'onclick' => 'selectMenuCheckboxes("referralcommissionIds",this.checked)'
			    ));?>
			<label for="referraladdsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'referraladdsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_referralcommission', 'label'=>false, 'class'=>'referralcommissionIds', 'onclick' => 'disableMenuCheckboxes("referralcommissionIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_referralcommission',$selected))); ?>
			<label for="referraladdsett2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'referraladdsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_referralcommissionplanadd', 'label'=>false, 'class'=>'referralcommissionIds', 'checked' => in_array('sitesetting/adminpanel_referralcommissionplanadd',$selected), 'disabled' => !in_array('sitesetting/adminpanel_referralcommission',$selected))); ?>
			<label for="referraladdsett3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'referraladdsett4', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_referralcommissionplanadd/$', 'label'=>false, 'class'=>'referralcommissionIds', 'checked' => in_array('sitesetting/adminpanel_referralcommissionplanadd/$',$selected), 'disabled' => !in_array('sitesetting/adminpanel_referralcommission',$selected))); ?>
			<label for="referraladdsett4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'referraladdsett5', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_referralcommissionplanstatus', 'label'=>false, 'class'=>'referralcommissionIds', 'checked' => in_array('sitesetting/adminpanel_referralcommissionplanstatus',$selected), 'disabled' => !in_array('sitesetting/adminpanel_referralcommission',$selected))); ?>
			<label for="referraladdsett5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'referraladdsett6', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_referralcommissionplanremove', 'label'=>false, 'class'=>'referralcommissionIds', 'checked' => in_array('sitesetting/adminpanel_referralcommissionplanremove',$selected), 'disabled' => !in_array('sitesetting/adminpanel_referralcommission',$selected))); ?>
			<label for="referraladdsett6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Statistics/Public</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'statpublicsett1',
			      'onclick' => 'selectMenuCheckboxes("publicstatisticIds",this.checked)'
			    ));?>
			<label for="statpublicsett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'statpublicsett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_publicstatistics', 'label'=>false, 'class'=>'publicstatisticIds', 'onclick' => 'disableMenuCheckboxes("publicstatisticIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_publicstatistics',$selected))); ?>
			<label for="statpublicsett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'statpublicsett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_publicstatisticsupdate', 'label'=>false, 'class'=>'publicstatisticIds', 'checked' => in_array('sitesetting/adminpanel_publicstatisticsupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_publicstatistics',$selected))); ?>
			<label for="statpublicsett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Statistics/Member</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'statmembersett1',
			      'onclick' => 'selectMenuCheckboxes("memberstatisticIds",this.checked)'
			    ));?>
			<label for="statmembersett1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'statmembersett2', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_memberstatistics', 'label'=>false, 'class'=>'memberstatisticIds', 'onclick' => 'disableMenuCheckboxes("memberstatisticIds",this.checked)', 'checked' => in_array('sitesetting/adminpanel_memberstatistics',$selected))); ?>
			<label for="statmembersett2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsitesetting.', array('type' => 'checkbox', 'id' => 'statmembersett3', 'hiddenField'=>false, 'div'=>false, 'value'=>'sitesetting/adminpanel_memberstatisticsupdate', 'label'=>false, 'class'=>'memberstatisticIds', 'checked' => in_array('sitesetting/adminpanel_memberstatisticsupdate',$selected), 'disabled' => !in_array('sitesetting/adminpanel_memberstatistics',$selected))); ?>
			<label for="statmembersett3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
    </div>
	</div>
    
	<div class="submenu member" style="display:none;">
		<div class="addnew-button checkbox">
			<?php echo $this->Form->input('subaction.', array('type' => 'checkbox', 'id' => 'memberall', 'class' => 'member', 'hiddenField'=>false, 'div'=>false, 'value'=>'member', 'label'=>false, 'checked' => in_array('member',$selected), 'onclick'=>"checkall(this);")); ?><label for="memberall"></label>
		</div>
		<div class="clearboth"></div>
    <div class="tablegrid">
        <div class="tablegridheader nofixheader">
		<div>
			<?php echo "Submenu"; ?>&nbsp;&nbsp;
		</div>
		<div>All</div>
		<div>View</div>
		<div>Add</div>
		<div>Edit</div>
		<div>Status</div>
		<div>Delete</div>
		<div>Extra</div>
	</div>
	<div class="tablegridrow">
		<div>Member List</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'memberlist1',
			      'onclick' => 'selectMenuCheckboxes("memberviewIds",this.checked)'
			    ));?>
			<label for="memberlist1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberlist2', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_index', 'label'=>false, 'class'=>'memberviewIds', 'onclick' => 'disableMenuCheckboxes("memberviewIds",this.checked)', 'checked' => in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberlist2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberlist3', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_memberadd', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_memberadd',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberlist3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberlist4', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_memberadd/$', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_memberadd/$',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberlist4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberlist5', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_memberstatus', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_memberstatus',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberlist5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberlist6', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_memberremove', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_memberremove',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberlist6"></label>
		</div>
		<div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberlist7', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_membercsv', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_membercsv',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
				<label for="memberlist7">CSV</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberlist8', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_login', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_login',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
				<label for="memberlist8">Login</label>
			</div>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Member Edit</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberedit1', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_memberupdate', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_memberupdate',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberedit1">Details</label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberedit2', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_mdposition', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_mdposition',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberedit2">Positions</label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberedit3', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_withdrawhistory', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_withdrawhistory',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberedit3">Withdrawals</label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberedit4', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_cash_by_admin', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_cash_by_admin',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberedit4">Balance Updates</label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberedit5', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_mdreferral', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_mdreferral',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberedit5">Referrals</label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberedit6', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_commission', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_commission',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberedit6">Commissions/Bonus</label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Member Edit</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberedit7', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_mdstat', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_mdstat',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberedit7">Stats</label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberedit8', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_memberactivity', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_memberactivity',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberedit8">Activity Log</label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberedit9', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_mdemaillog', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_mdemaillog',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberedit9">Sent Emails</label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberedit10', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_mdsendmail', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_mdsendmail',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberedit10">Send Message</label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'memberedit11', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_mdmakeorder', 'label'=>false, 'class'=>'memberviewIds', 'checked' => in_array('member/adminpanel_mdmakeorder',$selected), 'disabled' => !in_array('member/adminpanel_index',$selected))); ?>
			<label for="memberedit11">Add Order</label>
		</div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Leader Board/Top Position Earners</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'topposearners1', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_leaderboard', 'label'=>false, 'checked' => in_array('member/adminpanel_leaderboard',$selected))); ?>
			<label for="topposearners1"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Leader Board/Top Commission Earners</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'topcommearn1', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_topcommissionearners', 'label'=>false, 'checked' => in_array('member/adminpanel_topcommissionearners',$selected))); ?>
			<label for="topcommearn1"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Leader Board/Top Referrals</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'topreferrals1', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_toprefferals', 'label'=>false, 'checked' => in_array('member/adminpanel_toprefferals',$selected))); ?>
			<label for="topreferrals1"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Leader Board/Referral Tracking</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'referraltrck1', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_urltracking', 'label'=>false, 'checked' => in_array('member/adminpanel_urltracking',$selected))); ?>
			<label for="referraltrck1"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Mass Mailing</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'massmailing1', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_massmail,member/adminpanel_massmailaddemail', 'label'=>false, 'checked' => in_array('member/adminpanel_massmail',$selected))); ?>
			<label for="massmailing1"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Mass Mailing History</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
				'id' => 'massmailhistory1',
			      'onclick' => 'selectMenuCheckboxes("massmaillisthistoryIds",this.checked)'
			    ));?>
			<label for="massmailhistory1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'massmailhistory2', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_massmaillisthistory', 'label'=>false, 'class'=>'massmaillisthistoryIds', 'onclick' => 'disableMenuCheckboxes("massmaillisthistoryIds",this.checked)', 'checked' => in_array('member/adminpanel_massmaillisthistory',$selected))); ?>
			<label for="massmailhistory2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'massmailhistory3', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_massmailhistoryremove', 'label'=>false, 'class'=>'massmaillisthistoryIds', 'checked' => in_array('member/adminpanel_massmailhistoryremove',$selected), 'disabled' => !in_array('member/adminpanel_massmaillisthistory',$selected))); ?>
			<label for="massmailhistory3"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Admin Message</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
				'id' => 'adminmsg1',
			      'onclick' => 'selectMenuCheckboxes("adminmessageIds",this.checked)'
			    ));?>
			<label for="adminmsg1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'adminmsg2', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_message', 'label'=>false, 'class'=>'adminmessageIds', 'onclick' => 'disableMenuCheckboxes("adminmessageIds",this.checked)', 'checked' => in_array('member/adminpanel_message',$selected))); ?>
			<label for="adminmsg2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'adminmsg3', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_messageadd', 'label'=>false, 'class'=>'adminmessageIds', 'checked' => in_array('member/adminpanel_messageadd',$selected), 'disabled' => !in_array('member/adminpanel_message',$selected))); ?>
			<label for="adminmsg3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'adminmsg4', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_messageadd/$', 'label'=>false, 'class'=>'adminmessageIds', 'checked' => in_array('member/adminpanel_messageadd/$',$selected), 'disabled' => !in_array('member/adminpanel_message',$selected))); ?>
			<label for="adminmsg4"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmember.', array('type' => 'checkbox', 'id' => 'adminmsg5', 'hiddenField'=>false, 'div'=>false, 'value'=>'member/adminpanel_messageremove', 'label'=>false, 'class'=>'adminmessageIds', 'checked' => in_array('member/adminpanel_messageremove',$selected), 'disabled' => !in_array('member/adminpanel_message',$selected))); ?>
			<label for="adminmsg5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>URL Rotator</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
				'id' => 'urlrot1',
			      'onclick' => 'selectMenuCheckboxes("urlrotatorIds",this.checked)'
			    ));?>
			<label for="urlrot1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionurlrotator.', array('type' => 'checkbox', 'id' => 'urlrot2', 'hiddenField'=>false, 'div'=>false, 'value'=>'urlrotator/adminpanel_member', 'label'=>false, 'class'=>'urlrotatorIds', 'onclick' => 'disableMenuCheckboxes("urlrotatorIds",this.checked)', 'checked' => in_array('urlrotator/adminpanel_member',$selected))); ?>
			<label for="urlrot2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionurlrotator.', array('type' => 'checkbox', 'id' => 'urlrot3', 'hiddenField'=>false, 'div'=>false, 'value'=>'urlrotator/adminpanel_url', 'label'=>false, 'class'=>'urlrotatorIds', 'checked' => in_array('urlrotator/adminpanel_url',$selected), 'disabled' => !in_array('urlrotator/adminpanel_member',$selected))); ?>
			<label for="urlrot3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionurlrotator.', array('type' => 'checkbox', 'id' => 'urlrot4', 'hiddenField'=>false, 'div'=>false, 'value'=>'urlrotator/adminpanel_status', 'label'=>false, 'class'=>'urlrotatorIds', 'checked' => in_array('urlrotator/adminpanel_status',$selected), 'disabled' => !in_array('urlrotator/adminpanel_member',$selected))); ?>
			<label for="urlrot4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionurlrotator.', array('type' => 'checkbox', 'id' => 'urlrot5', 'hiddenField'=>false, 'div'=>false, 'value'=>'urlrotator/adminpanel_remove', 'label'=>false, 'class'=>'urlrotatorIds', 'checked' => in_array('urlrotator/adminpanel_remove',$selected), 'disabled' => !in_array('urlrotator/adminpanel_member',$selected))); ?>
			<label for="urlrot5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>URL Shortener</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
				'id' => 'urlshort1',
			      'onclick' => 'selectMenuCheckboxes("urlshortenerIds",this.checked)'
			    ));?>
			<label for="urlshort1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionurlshortener.', array('type' => 'checkbox', 'id' => 'urlshort2', 'hiddenField'=>false, 'div'=>false, 'value'=>'urlshortener/adminpanel_url', 'label'=>false, 'class'=>'urlshortenerIds', 'onclick' => 'disableMenuCheckboxes("urlshortenerIds",this.checked)', 'checked' => in_array('urlshortener/adminpanel_url',$selected))); ?>
			<label for="urlshort2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionurlshortener.', array('type' => 'checkbox', 'id' => 'urlshort3', 'hiddenField'=>false, 'div'=>false, 'value'=>'urlshortener/adminpanel_add/$', 'label'=>false, 'class'=>'urlshortenerIds', 'checked' => in_array('urlshortener/adminpanel_add/$',$selected), 'disabled' => !in_array('urlshortener/adminpanel_url',$selected))); ?>
			<label for="urlshort3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionurlshortener.', array('type' => 'checkbox', 'id' => 'urlshort4', 'hiddenField'=>false, 'div'=>false, 'value'=>'urlshortener/adminpanel_status', 'label'=>false, 'class'=>'urlshortenerIds', 'checked' => in_array('urlshortener/adminpanel_status',$selected), 'disabled' => !in_array('urlshortener/adminpanel_url',$selected))); ?>
			<label for="urlshort4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionurlshortener.', array('type' => 'checkbox', 'id' => 'urlshort5', 'hiddenField'=>false, 'div'=>false, 'value'=>'urlshortener/adminpanel_remove', 'label'=>false, 'class'=>'urlshortenerIds', 'checked' => in_array('urlshortener/adminpanel_remove',$selected), 'disabled' => !in_array('urlshortener/adminpanel_url',$selected))); ?>
			<label for="urlshort5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Member Group</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
				'id' => 'membergrp1',
			      'onclick' => 'selectMenuCheckboxes("membergrpIds",this.checked)'
			    ));?>
			<label for="membergrp1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmembergroup.', array('type' => 'checkbox', 'id' => 'membergrp2', 'hiddenField'=>false, 'div'=>false, 'value'=>'membergroup/adminpanel_index', 'label'=>false, 'class'=>'membergrpIds', 'onclick' => 'disableMenuCheckboxes("membergrpIds",this.checked)', 'checked' => in_array('membergroup/adminpanel_index',$selected))); ?>
			<label for="membergrp2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmembergroup.', array('type' => 'checkbox', 'id' => 'membergrp3', 'hiddenField'=>false, 'div'=>false, 'value'=>'membergroup/adminpanel_add', 'label'=>false, 'class'=>'membergrpIds', 'checked' => in_array('membergroup/adminpanel_add',$selected), 'disabled' => !in_array('membergroup/adminpanel_index',$selected))); ?>
			<label for="membergrp3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmembergroup.', array('type' => 'checkbox', 'id' => 'membergrp4', 'hiddenField'=>false, 'div'=>false, 'value'=>'membergroup/adminpanel_add/$', 'label'=>false, 'class'=>'membergrpIds', 'checked' => in_array('membergroup/adminpanel_add/$',$selected), 'disabled' => !in_array('membergroup/adminpanel_index',$selected))); ?>
			<label for="membergrp4"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmembergroup.', array('type' => 'checkbox', 'id' => 'membergrp5', 'hiddenField'=>false, 'div'=>false, 'value'=>'membergroup/adminpanel_remove', 'label'=>false, 'class'=>'membergrpIds', 'checked' => in_array('membergroup/adminpanel_remove',$selected), 'disabled' => !in_array('membergroup/adminpanel_index',$selected))); ?>
			<label for="membergrp5"></label>
		</div>
		<div></div>
	</div>
    </div>
    
	</div>
	
	<div class="submenu finance" style="display:none;">
		<div class="addnew-button checkbox">
			<?php echo $this->Form->input('subaction.', array('type' => 'checkbox', 'id' => 'financeall', 'class' => 'finance', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance', 'label'=>false, 'checked' => in_array('finance',$selected), 'onclick'=>"checkall(this);")); ?><label for="financeall"></label>
		</div>
		<div class="clearboth"></div>
	<div class="tablegrid">
        <div class="tablegridheader nofixheader">
		<div>
			<?php echo "Submenu"; ?>&nbsp;&nbsp;
		</div>
		<div>All</div>
		<div>View</div>
		<div>Add</div>
		<div>Edit</div>
		<div>Status</div>
		<div>Delete</div>
		<div style="width: 300px;">Extra</div>
	</div>
	<div class="tablegridrow">
		<div>Payment History</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'paymenthistory1',
			      'onclick' => 'selectMenuCheckboxes("paymenthistoryIds",this.checked)'
			    ));?>
			<label for="paymenthistory1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'paymenthistory2', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_index', 'label'=>false, 'class'=>'paymenthistoryIds', 'onclick' => 'disableMenuCheckboxes("paymenthistoryIds",this.checked)', 'checked' => in_array('finance/adminpanel_index',$selected))); ?>
			<label for="paymenthistory2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'paymenthistory3', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_paymenthistoryremove', 'label'=>false, 'class'=>'paymenthistoryIds', 'checked' => in_array('finance/adminpanel_paymenthistoryremove',$selected), 'disabled' => !in_array('finance/adminpanel_index',$selected))); ?>
			<label for="paymenthistory3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'paymenthistory4', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_paymenthistorycsv', 'label'=>false, 'class'=>'paymenthistoryIds', 'checked' => in_array('finance/adminpanel_paymenthistorycsv',$selected), 'disabled' => !in_array('finance/adminpanel_index',$selected))); ?>
			<label for="paymenthistory4">CSV</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Pending Deposits</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'pendingdeposit1',
			      'onclick' => 'selectMenuCheckboxes("pendingdepositIds",this.checked)'
			    ));?>
			<label for="pendingdeposit1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingdeposit2', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingdeposit', 'label'=>false, 'class'=>'pendingdepositIds', 'onclick' => 'disableMenuCheckboxes("pendingdepositIds",this.checked)', 'checked' => in_array('finance/adminpanel_pendingdeposit',$selected))); ?>
			<label for="pendingdeposit2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingdeposit3', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingdepositremove', 'label'=>false, 'class'=>'pendingdepositIds', 'checked' => in_array('finance/adminpanel_pendingdepositremove',$selected), 'disabled' => !in_array('finance/adminpanel_pendingdeposit',$selected))); ?>
			<label for="pendingdeposit3"></label>
		</div>
		<div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingdeposit4', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingdepositcsv', 'label'=>false, 'class'=>'pendingdepositIds', 'checked' => in_array('finance/adminpanel_pendingdepositcsv',$selected), 'disabled' => !in_array('finance/adminpanel_pendingdeposit',$selected))); ?>
				<label for="pendingdeposit4">CSV</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingdeposit5', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingdepositpay', 'label'=>false, 'class'=>'pendingdepositIds', 'checked' => in_array('finance/adminpanel_pendingdepositpay',$selected), 'disabled' => !in_array('finance/adminpanel_pendingdeposit',$selected))); ?>
				<label for="pendingdeposit5">Mark</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingdeposit6', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingdepositadd', 'label'=>false, 'class'=>'pendingdepositIds', 'checked' => in_array('finance/adminpanel_pendingdepositadd',$selected), 'disabled' => !in_array('finance/adminpanel_pendingdeposit',$selected))); ?>
				<label for="pendingdeposit6">Edit Bank Wire</label>
			</div>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Pending Positions</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'pendingpos1',
			      'onclick' => 'selectMenuCheckboxes("pendingpositionIds",this.checked)'
			    ));?>
			<label for="pendingpos1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingpos2', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingposition', 'label'=>false, 'class'=>'pendingpositionIds', 'onclick' => 'disableMenuCheckboxes("pendingpositionIds",this.checked)', 'checked' => in_array('finance/adminpanel_pendingposition',$selected))); ?>
			<label for="pendingpos2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingpos3', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingpositionremove', 'label'=>false, 'class'=>'pendingpositionIds', 'checked' => in_array('finance/adminpanel_pendingpositionremove',$selected), 'disabled' => !in_array('finance/adminpanel_pendingposition',$selected))); ?>
			<label for="pendingpos3"></label>
		</div>
		<div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingpos4', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingpositioncsv', 'label'=>false, 'class'=>'pendingpositionIds', 'checked' => in_array('finance/adminpanel_pendingpositioncsv',$selected), 'disabled' => !in_array('finance/adminpanel_pendingposition',$selected))); ?>
				<label for="pendingpos4">CSV</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingpos5', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingpositionpay', 'label'=>false, 'class'=>'pendingpositionIds', 'checked' => in_array('finance/adminpanel_pendingpositionpay',$selected), 'disabled' => !in_array('finance/adminpanel_pendingposition',$selected))); ?>
				<label for="pendingpos5">Mark</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingpos6', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingpositionadd', 'label'=>false, 'class'=>'pendingpositionIds', 'checked' => in_array('finance/adminpanel_pendingpositionadd',$selected), 'disabled' => !in_array('finance/adminpanel_pendingposition',$selected))); ?>
				<label for="pendingpos6">Edit Bank Wire</label>
			</div>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Pending Purchases</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'pendingpurchases1',
			      'onclick' => 'selectMenuCheckboxes("pendingpurchaseIds",this.checked)'
			    ));?>
			<label for="pendingpurchases1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingpurchases2', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingplan', 'label'=>false, 'class'=>'pendingpurchaseIds', 'onclick' => 'disableMenuCheckboxes("pendingpurchaseIds",this.checked)', 'checked' => in_array('finance/adminpanel_pendingplan',$selected))); ?>
			<label for="pendingpurchases2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingpurchases3', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingplanremove', 'label'=>false, 'class'=>'pendingpurchaseIds', 'checked' => in_array('finance/adminpanel_pendingplanremove',$selected), 'disabled' => !in_array('finance/adminpanel_pendingplan',$selected))); ?>
			<label for="pendingpurchases3"></label>
		</div>
		<div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingpurchases4', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingplancsv', 'label'=>false, 'class'=>'pendingpurchaseIds', 'checked' => in_array('finance/adminpanel_pendingplancsv',$selected), 'disabled' => !in_array('finance/adminpanel_pendingplan',$selected))); ?>
				<label for="pendingpurchases4">CSV</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingpurchases5', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingplanpay', 'label'=>false, 'class'=>'pendingpurchaseIds', 'checked' => in_array('finance/adminpanel_pendingplanpay',$selected), 'disabled' => !in_array('finance/adminpanel_pendingplan',$selected))); ?>
				<label for="pendingpurchases5">Mark</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingpurchases6', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingplanadd', 'label'=>false, 'class'=>'pendingpurchaseIds', 'checked' => in_array('finance/adminpanel_pendingplanadd',$selected), 'disabled' => !in_array('finance/adminpanel_pendingplan',$selected))); ?>
				<label for="pendingpurchases6">Edit Bank Wire</label>
			</div>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Pending Free Plans</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'pendingfreeplans1',
			      'onclick' => 'selectMenuCheckboxes("pendingfreeplanIds",this.checked)'
			    ));?>
			<label for="pendingfreeplans1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingfreeplans2', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingfreeplan', 'label'=>false, 'class'=>'pendingfreeplanIds', 'onclick' => 'disableMenuCheckboxes("pendingfreeplanIds",this.checked)', 'checked' => in_array('finance/adminpanel_pendingfreeplan',$selected))); ?>
			<label for="pendingfreeplans2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'pendingfreeplans3', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_pendingfreeplanremove', 'label'=>false, 'class'=>'pendingfreeplanIds', 'checked' => in_array('finance/adminpanel_pendingfreeplanremove',$selected), 'disabled' => !in_array('finance/adminpanel_pendingfreeplan',$selected))); ?>
			<label for="pendingfreeplans3"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Withdrawal Requests</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'withdrawreq1',
			      'onclick' => 'selectMenuCheckboxes("withdrawIds",this.checked)'
			    ));?>
			<label for="withdrawreq1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'withdrawreq2', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_withdrawrequest', 'label'=>false, 'class'=>'withdrawIds', 'onclick' => 'disableMenuCheckboxes("withdrawIds",this.checked)', 'checked' => in_array('finance/adminpanel_withdrawrequest',$selected))); ?>
			<label for="withdrawreq2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'withdrawreq3', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_viewcomment', 'label'=>false, 'class'=>'withdrawIds', 'checked' => in_array('finance/adminpanel_viewcomment',$selected), 'disabled' => !in_array('finance/adminpanel_withdrawrequest',$selected))); ?>
				<label for="withdrawreq3">View Comment</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'withdrawreq4', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_paytoall', 'label'=>false, 'class'=>'withdrawIds', 'checked' => in_array('finance/adminpanel_paytoall',$selected), 'disabled' => !in_array('finance/adminpanel_withdrawrequest',$selected))); ?>
				<label for="withdrawreq4">Pay To All</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'withdrawreq5', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_paytoselected', 'label'=>false, 'class'=>'withdrawIds', 'checked' => in_array('finance/adminpanel_paytoselected',$selected), 'disabled' => !in_array('finance/adminpanel_withdrawrequest',$selected))); ?>
				<label for="withdrawreq5">Pay To Selected</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'withdrawreq6', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_cancelselected', 'label'=>false, 'class'=>'withdrawIds', 'checked' => in_array('finance/adminpanel_cancelselected',$selected), 'disabled' => !in_array('finance/adminpanel_withdrawrequest',$selected))); ?>
				<label for="withdrawreq6">Cancel Selected</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'withdrawreq7', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_markselected', 'label'=>false, 'class'=>'withdrawIds', 'checked' => in_array('finance/adminpanel_markselected',$selected), 'disabled' => !in_array('finance/adminpanel_withdrawrequest',$selected))); ?>
				<label for="withdrawreq7">Mark Selected</label>
			</div>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Withdrawal Requests/Traditional Masspay</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'tradipay1',
			      'onclick' => 'selectMenuCheckboxes("traditionalmasspayIds",this.checked)'
			    ));?>
			<label for="tradipay1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'tradipay2', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_traditionalmasspay', 'label'=>false, 'class'=>'traditionalmasspayIds', 'onclick' => 'disableMenuCheckboxes("traditionalmasspayIds",this.checked)', 'checked' => in_array('finance/adminpanel_traditionalmasspay',$selected))); ?>
			<label for="tradipay2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'tradipay3', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_traditionalmasspaycsv', 'label'=>false, 'class'=>'traditionalmasspayIds', 'checked' => in_array('finance/adminpanel_traditionalmasspaycsv',$selected), 'disabled' => !in_array('finance/adminpanel_traditionalmasspay',$selected))); ?>
				<label for="tradipay3">CSV</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'tradipay4', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_cancelselectedtraditional', 'label'=>false, 'class'=>'traditionalmasspayIds', 'checked' => in_array('finance/adminpanel_cancelselectedtraditional',$selected), 'disabled' => !in_array('finance/adminpanel_traditionalmasspay',$selected))); ?>
				<label for="tradipay4">Cancel Selected</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'tradipay5', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_markselectedtraditional', 'label'=>false, 'class'=>'traditionalmasspayIds', 'checked' => in_array('finance/adminpanel_markselectedtraditional',$selected), 'disabled' => !in_array('finance/adminpanel_traditionalmasspay',$selected))); ?>
				<label for="tradipay5">Mark Selected</label>
			</div>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Withdrawal Requests/Automatic Payments</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'autopay1',
			      'onclick' => 'selectMenuCheckboxes("automaticpaymentIds",this.checked)'
			    ));?>
			<label for="autopay1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'autopay2', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_automaticmasspay', 'label'=>false, 'class'=>'automaticpaymentIds', 'onclick' => 'disableMenuCheckboxes("automaticpaymentIds",this.checked)', 'checked' => in_array('finance/adminpanel_automaticmasspay',$selected))); ?>
			<label for="autopay2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'autopay3', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_cancelselectedautomatic', 'label'=>false, 'class'=>'automaticpaymentIds', 'checked' => in_array('finance/adminpanel_cancelselectedautomatic',$selected), 'disabled' => !in_array('finance/adminpanel_automaticmasspay',$selected))); ?>
				<label for="autopay3">Cancel Selected</label>
			</div>
			<div class="checkbox">
				<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'autopay4', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_markselectedautomatic', 'label'=>false, 'class'=>'automaticpaymentIds', 'checked' => in_array('finance/adminpanel_markselectedautomatic',$selected), 'disabled' => !in_array('finance/adminpanel_automaticmasspay',$selected))); ?>
				<label for="autopay4">Mark Selected</label>
			</div>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Withdrawal Requests/Total Stats</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'totalstats1', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_totalstats', 'label'=>false, 'checked' => in_array('finance/adminpanel_totalstats',$selected))); ?>
			<label for="totalstats1"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Withdrawal History</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'withdrawhistory1',
			      'onclick' => 'selectMenuCheckboxes("withdrawhistoryIds",this.checked)'
			    ));?>
			<label for="withdrawhistory1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'withdrawhistory2', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_withdrawhistory', 'label'=>false, 'class'=>'withdrawhistoryIds', 'onclick' => 'disableMenuCheckboxes("withdrawhistoryIds",this.checked)', 'checked' => in_array('finance/adminpanel_withdrawhistory',$selected))); ?>
			<label for="withdrawhistory2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'withdrawhistory3', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_withdrawhistoryremove', 'label'=>false, 'class'=>'withdrawhistoryIds', 'checked' => in_array('finance/adminpanel_withdrawhistoryremove',$selected), 'disabled' => !in_array('finance/adminpanel_withdrawhistory',$selected))); ?>
			<label for="withdrawhistory3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'withdrawhistory4', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_withdrawhistorycsv', 'label'=>false, 'class'=>'withdrawhistoryIds', 'checked' => in_array('finance/adminpanel_withdrawhistorycsv',$selected), 'disabled' => !in_array('finance/adminpanel_withdrawhistory',$selected))); ?>
			<label for="withdrawhistory4">CSV</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Commission</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'commission1',
			      'onclick' => 'selectMenuCheckboxes("commissionIds",this.checked)'
			    ));?>
			<label for="commission1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'commission2', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_commission', 'label'=>false, 'class'=>'commissionIds', 'onclick' => 'disableMenuCheckboxes("commissionIds",this.checked)', 'checked' => in_array('finance/adminpanel_commission',$selected))); ?>
			<label for="commission2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'commission3', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_commissionremove', 'label'=>false, 'class'=>'commissionIds', 'checked' => in_array('finance/adminpanel_commissionremove',$selected), 'disabled' => !in_array('finance/adminpanel_commission',$selected))); ?>
			<label for="commission3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'commission4', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_commissioncsv', 'label'=>false, 'class'=>'commissionIds', 'checked' => in_array('finance/adminpanel_commissioncsv',$selected), 'disabled' => !in_array('finance/adminpanel_commission',$selected))); ?>
			<label for="commission4">CSV</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Cash By Admin</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'cashbyadmin1',
			      'onclick' => 'selectMenuCheckboxes("cashbyadminIds",this.checked)'
			    ));?>
			<label for="cashbyadmin1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'cashbyadmin2', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_cash_by_admin', 'label'=>false, 'class'=>'cashbyadminIds', 'onclick' => 'disableMenuCheckboxes("cashbyadminIds",this.checked)', 'checked' => in_array('finance/adminpanel_withdrawhistory',$selected))); ?>
			<label for="cashbyadmin2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'cashbyadmin3', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_cash_by_adminadd', 'label'=>false, 'class'=>'cashbyadminIds', 'checked' => in_array('finance/adminpanel_cash_by_adminadd',$selected), 'disabled' => !in_array('finance/adminpanel_cash_by_admin',$selected))); ?>
			<label for="cashbyadmin3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'cashbyadmin4', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_cashbyadmincsv', 'label'=>false, 'class'=>'cashbyadminIds', 'checked' => in_array('finance/adminpanel_cashbyadmincsv',$selected), 'disabled' => !in_array('finance/adminpanel_cash_by_admin',$selected))); ?>
			<label for="cashbyadmin4">CSV</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Balance Transfer/Member to Member Pending Requests</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'btmtompr1',
			      'onclick' => 'selectMenuCheckboxes("mtompendingIds",this.checked)'
			    ));?>
			<label for="btmtompr1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbalancetransfer.', array('type' => 'checkbox', 'id' => 'btmtompr2', 'hiddenField'=>false, 'div'=>false, 'value'=>'balancetransfer/adminpanel_balancetransferpanding', 'label'=>false, 'class'=>'mtompendingIds', 'onclick' => 'disableMenuCheckboxes("mtompendingIds",this.checked)', 'checked' => in_array('balancetransfer/adminpanel_balancetransferpanding',$selected))); ?>
			<label for="btmtompr2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbalancetransfer.', array('type' => 'checkbox', 'id' => 'btmtompr3', 'hiddenField'=>false, 'div'=>false, 'value'=>'balancetransfer/adminpanel_balancetransferpandingremove', 'label'=>false, 'class'=>'mtompendingIds', 'checked' => in_array('balancetransfer/adminpanel_balancetransferpandingremove',$selected), 'disabled' => !in_array('balancetransfer/adminpanel_balancetransferpanding',$selected))); ?>
			<label for="btmtompr3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbalancetransfer.', array('type' => 'checkbox', 'id' => 'btmtompr4', 'hiddenField'=>false, 'div'=>false, 'value'=>'balancetransfer/adminpanel_balancetransferpandingpay', 'label'=>false, 'class'=>'mtompendingIds', 'checked' => in_array('balancetransfer/adminpanel_balancetransferpandingpay',$selected), 'disabled' => !in_array('balancetransfer/adminpanel_balancetransferpanding',$selected))); ?>
			<label for="btmtompr4">Allow</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Balance Transfer/Internal Transfer Pending Requests</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'btitpr1',
			      'onclick' => 'selectMenuCheckboxes("itpendingIds",this.checked)'
			    ));?>
			<label for="btitpr1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbalancetransfer.', array('type' => 'checkbox', 'id' => 'btitpr2', 'hiddenField'=>false, 'div'=>false, 'value'=>'balancetransfer/adminpanel_processorpanding', 'label'=>false, 'class'=>'itpendingIds', 'onclick' => 'disableMenuCheckboxes("itpendingIds",this.checked)', 'checked' => in_array('balancetransfer/adminpanel_processorpanding',$selected))); ?>
			<label for="btitpr2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbalancetransfer.', array('type' => 'checkbox', 'id' => 'btitpr3', 'hiddenField'=>false, 'div'=>false, 'value'=>'balancetransfer/adminpanel_processorpandingremove', 'label'=>false, 'class'=>'itpendingIds', 'checked' => in_array('balancetransfer/adminpanel_processorpandingremove',$selected), 'disabled' => !in_array('balancetransfer/adminpanel_processorpanding',$selected))); ?>
			<label for="btitpr3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbalancetransfer.', array('type' => 'checkbox', 'id' => 'btitpr4', 'hiddenField'=>false, 'div'=>false, 'value'=>'balancetransfer/adminpanel_processorpandingpay', 'label'=>false, 'class'=>'itpendingIds', 'checked' => in_array('balancetransfer/adminpanel_processorpandingpay',$selected), 'disabled' => !in_array('balancetransfer/adminpanel_processorpanding',$selected))); ?>
			<label for="btitpr4">Allow</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Balance Transfer/Member to Member Transfer History</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'btmtomth1',
			      'onclick' => 'selectMenuCheckboxes("mtomtransferIds",this.checked)'
			    ));?>
			<label for="btmtomth1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbalancetransfer.', array('type' => 'checkbox', 'id' => 'btmtomth2', 'hiddenField'=>false, 'div'=>false, 'value'=>'balancetransfer/adminpanel_balancetransferhistory', 'label'=>false, 'class'=>'mtomtransferIds', 'onclick' => 'disableMenuCheckboxes("mtomtransferIds",this.checked)', 'checked' => in_array('balancetransfer/adminpanel_balancetransferhistory',$selected))); ?>
			<label for="btmtomth2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbalancetransfer.', array('type' => 'checkbox', 'id' => 'btmtomth3', 'hiddenField'=>false, 'div'=>false, 'value'=>'balancetransfer/adminpanel_balancetransferhistoryremove', 'label'=>false, 'class'=>'mtomtransferIds', 'checked' => in_array('balancetransfer/adminpanel_balancetransferhistoryremove',$selected), 'disabled' => !in_array('balancetransfer/adminpanel_balancetransferhistory',$selected))); ?>
			<label for="btmtomth3"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Balance Transfer/Internal Transfer History</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'btitth1',
			      'onclick' => 'selectMenuCheckboxes("ittransferIds",this.checked)'
			    ));?>
			<label for="btitth1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbalancetransfer.', array('type' => 'checkbox', 'id' => 'btitth2', 'hiddenField'=>false, 'div'=>false, 'value'=>'balancetransfer/adminpanel_processorhistory', 'label'=>false, 'class'=>'ittransferIds', 'onclick' => 'disableMenuCheckboxes("ittransferIds",this.checked)', 'checked' => in_array('balancetransfer/adminpanel_processorhistory',$selected))); ?>
			<label for="btitth2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbalancetransfer.', array('type' => 'checkbox', 'id' => 'btitth3', 'hiddenField'=>false, 'div'=>false, 'value'=>'balancetransfer/adminpanel_processorhistoryremove', 'label'=>false, 'class'=>'ittransferIds', 'checked' => in_array('balancetransfer/adminpanel_processorhistoryremove',$selected), 'disabled' => !in_array('balancetransfer/adminpanel_processorhistory',$selected))); ?>
			<label for="btitth3"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Coupons</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'coupons1',
			      'onclick' => 'selectMenuCheckboxes("couponIds",this.checked)'
			    ));?>
			<label for="coupons1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'coupons2', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_coupons', 'label'=>false, 'class'=>'couponIds', 'onclick' => 'disableMenuCheckboxes("couponIds",this.checked)', 'checked' => in_array('finance/adminpanel_coupons',$selected))); ?>
			<label for="coupons2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'coupons3', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_couponsadd', 'label'=>false, 'class'=>'couponIds', 'checked' => in_array('finance/adminpanel_couponsadd',$selected), 'disabled' => !in_array('finance/adminpanel_coupons',$selected))); ?>
			<label for="coupons3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'coupons4', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_couponsadd/$', 'label'=>false, 'class'=>'couponIds', 'checked' => in_array('finance/adminpanel_couponsadd/$',$selected), 'disabled' => !in_array('finance/adminpanel_coupons',$selected))); ?>
			<label for="coupons4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'coupons5', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_couponsstatus', 'label'=>false, 'class'=>'couponIds', 'checked' => in_array('finance/adminpanel_couponsstatus',$selected), 'disabled' => !in_array('finance/adminpanel_coupons',$selected))); ?>
			<label for="coupons5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'coupons6', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_couponsremove', 'label'=>false, 'class'=>'couponIds', 'checked' => in_array('finance/adminpanel_couponsremove',$selected), 'disabled' => !in_array('finance/adminpanel_coupons',$selected))); ?>
			<label for="coupons6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Coupons/Coupons History</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'couponshis1',
			      'onclick' => 'selectMenuCheckboxes("couponhistoryIds",this.checked)'
			    ));?>
			<label for="couponshis1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'couponshis2', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/adminpanel_couponshistory', 'label'=>false, 'class'=>'couponhistoryIds', 'onclick' => 'disableMenuCheckboxes("couponhistoryIds",this.checked)', 'checked' => in_array('finance/adminpanel_couponshistory',$selected))); ?>
			<label for="couponshis2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionfinance.', array('type' => 'checkbox', 'id' => 'couponshis3', 'hiddenField'=>false, 'div'=>false, 'value'=>'finance/couponshistoryremove', 'label'=>false, 'class'=>'couponhistoryIds', 'checked' => in_array('finance/couponshistoryremove',$selected), 'disabled' => !in_array('finance/adminpanel_couponshistory',$selected))); ?>
			<label for="couponshis3"></label>
		</div>
		<div></div>
	</div>
    </div>
    
	</div>
	
	<div class="submenu design" style="display:none;">
		<div class="addnew-button checkbox">
			<?php echo $this->Form->input('subaction.', array('type' => 'checkbox', 'id' => 'designall', 'class' => 'design', 'hiddenField'=>false, 'div'=>false, 'value'=>'design', 'label'=>false, 'checked' => in_array('design',$selected), 'onclick'=>"checkall(this);")); ?><label for="designall"></label>
		</div>
		<div class="clearboth"></div>
	<div class="tablegrid">
        <div class="tablegridheader nofixheader">
		<div>
			<?php echo "Submenu"; ?>&nbsp;&nbsp;
		</div>
		<div>All</div>
		<div>View</div>
		<div>Add</div>
		<div>Edit</div>
		<div>Status</div>
		<div>Delete</div>
		<div>Extra</div>
	</div>
	<div class="tablegridrow">
		<div>Themes</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'themes1',
			      'onclick' => 'selectMenuCheckboxes("themeIds",this.checked)'
			    ));?>
			<label for="themes1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'themes2', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_index', 'label'=>false, 'class'=>'themeIds', 'onclick' => 'disableMenuCheckboxes("themeIds",this.checked)', 'checked' => in_array('design/adminpanel_index',$selected))); ?>
			<label for="themes2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'themes3', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_themeadd', 'label'=>false, 'class'=>'themeIds', 'checked' => in_array('design/adminpanel_themeadd',$selected), 'disabled' => !in_array('design/adminpanel_index',$selected))); ?>
			<label for="themes3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'themes4', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_themeadd/$', 'label'=>false, 'class'=>'themeIds', 'checked' => in_array('design/adminpanel_themeadd/$',$selected), 'disabled' => !in_array('design/adminpanel_index',$selected))); ?>
			<label for="themes4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'themes5', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_themestatus', 'label'=>false, 'class'=>'themeIds', 'checked' => in_array('design/adminpanel_themestatus',$selected), 'disabled' => !in_array('design/adminpanel_index',$selected))); ?>
			<label for="themes5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'themes6', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_themeremove', 'label'=>false, 'class'=>'themeIds', 'checked' => in_array('design/adminpanel_themeremove',$selected), 'disabled' => !in_array('design/adminpanel_index',$selected))); ?>
			<label for="themes6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Themes/Member Menu</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'thememember1',
			      'onclick' => 'selectMenuCheckboxes("membermenuIds",this.checked)'
			    ));?>
			<label for="thememember1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'thememember2', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_thememembermenu', 'label'=>false, 'class'=>'membermenuIds', 'onclick' => 'disableMenuCheckboxes("membermenuIds",this.checked)', 'checked' => in_array('design/adminpanel_thememembermenu',$selected))); ?>
			<label for="thememember2"></label>
		</div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'thememember3', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_thememembersubmenu', 'label'=>false, 'class'=>'membermenuIds', 'checked' => in_array('design/adminpanel_thememembersubmenu',$selected), 'disabled' => !in_array('design/adminpanel_thememembermenu',$selected))); ?>
			<label for="thememember3"></label>
		</div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Themes/Public Menu</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'themepublic1',
			      'onclick' => 'selectMenuCheckboxes("publicmenuIds",this.checked)'
			    ));?>
			<label for="themepublic1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'themepublic2', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_themepublicmenu', 'label'=>false, 'class'=>'publicmenuIds', 'onclick' => 'disableMenuCheckboxes("publicmenuIds",this.checked)', 'checked' => in_array('design/adminpanel_themepublicmenu',$selected))); ?>
			<label for="themepublic2"></label>
		</div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'themepublic3', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_menuformaction', 'label'=>false, 'class'=>'publicmenuIds', 'checked' => in_array('design/adminpanel_menuformaction',$selected), 'disabled' => !in_array('design/adminpanel_themepublicmenu',$selected))); ?>
			<label for="themepublic3"></label>
		</div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Themes/Advertisement Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'themeadv1',
			      'onclick' => 'selectMenuCheckboxes("themeadIds",this.checked)'
			    ));?>
			<label for="themeadv1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'themeadv2', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_advertisement', 'label'=>false, 'class'=>'themeadIds', 'onclick' => 'disableMenuCheckboxes("themeadIds",this.checked)', 'checked' => in_array('design/adminpanel_advertisement',$selected))); ?>
			<label for="themeadv2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'themeadv3', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_advertisementaction', 'label'=>false, 'class'=>'themeadIds', 'checked' => in_array('design/adminpanel_advertisementaction',$selected), 'disabled' => !in_array('design/adminpanel_advertisement',$selected))); ?>
			<label for="themeadv3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Themes/Settings</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'themesetting1',
			      'onclick' => 'selectMenuCheckboxes("themesettingIds",this.checked)'
			    ));?>
			<label for="themesetting1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'themesetting2', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_themesetting', 'label'=>false, 'class'=>'themesettingIds', 'onclick' => 'disableMenuCheckboxes("themesettingIds",this.checked)', 'checked' => in_array('design/adminpanel_themesetting',$selected))); ?>
			<label for="themesetting2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'themesetting3', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_themesettingupdate', 'label'=>false, 'class'=>'themesettingIds', 'checked' => in_array('design/adminpanel_themesettingupdate',$selected), 'disabled' => !in_array('design/adminpanel_themesetting',$selected))); ?>
			<label for="themesetting3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Admin Themes</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'adminthemes1',
			      'onclick' => 'selectMenuCheckboxes("adminthemeIds",this.checked)'
			    ));?>
			<label for="adminthemes1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'adminthemes2', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_admintheme', 'label'=>false, 'class'=>'adminthemeIds', 'onclick' => 'disableMenuCheckboxes("adminthemeIds",this.checked)', 'checked' => in_array('design/adminpanel_admintheme',$selected))); ?>
			<label for="adminthemes2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'adminthemes3', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_adminthemeadd', 'label'=>false, 'class'=>'adminthemeIds', 'checked' => in_array('design/adminpanel_adminthemeadd',$selected), 'disabled' => !in_array('design/adminpanel_admintheme',$selected))); ?>
			<label for="adminthemes3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'adminthemes4', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_adminthemeadd/$', 'label'=>false, 'class'=>'adminthemeIds', 'checked' => in_array('design/adminpanel_adminthemeadd/$',$selected), 'disabled' => !in_array('design/adminpanel_admintheme',$selected))); ?>
			<label for="adminthemes4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'adminthemes5', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_adminthemestatus', 'label'=>false, 'class'=>'adminthemeIds', 'checked' => in_array('design/adminpanel_adminthemestatus',$selected), 'disabled' => !in_array('design/adminpanel_admintheme',$selected))); ?>
			<label for="adminthemes5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'adminthemes6', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_adminthemeremove', 'label'=>false, 'class'=>'adminthemeIds', 'checked' => in_array('design/adminpanel_adminthemeremove',$selected), 'disabled' => !in_array('design/adminpanel_admintheme',$selected))); ?>
			<label for="adminthemes6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Template Directory</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'tempdir1',
			      'onclick' => 'selectMenuCheckboxes("templateeditIds",this.checked)'
			    ));?>
			<label for="tempdir1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'tempdir2', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_template', 'label'=>false, 'class'=>'templateeditIds', 'onclick' => 'disableMenuCheckboxes("templateeditIds",this.checked)', 'checked' => in_array('design/adminpanel_template',$selected))); ?>
			<label for="tempdir2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'tempdir3', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_templateupdate', 'label'=>false, 'class'=>'templateeditIds', 'checked' => in_array('design/adminpanel_templateupdate',$selected), 'disabled' => !in_array('design/adminpanel_template',$selected))); ?>
			<label for="tempdir3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Layout Directory</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'layoutdir1',
			      'onclick' => 'selectMenuCheckboxes("layouteditIds",this.checked)'
			    ));?>
			<label for="layoutdir1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'layoutdir2', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_layout', 'label'=>false, 'class'=>'layouteditIds', 'onclick' => 'disableMenuCheckboxes("layouteditIds",this.checked)', 'checked' => in_array('design/adminpanel_layout',$selected))); ?>
			<label for="layoutdir2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'layoutdir3', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_layoutupdate', 'label'=>false, 'class'=>'layouteditIds', 'checked' => in_array('design/adminpanel_layoutupdate',$selected), 'disabled' => !in_array('design/adminpanel_layout',$selected))); ?>
			<label for="layoutdir3"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Email Templates/Site Email Templates</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'siteemailtemp1',
			      'onclick' => 'selectMenuCheckboxes("siteemailtemplateIds",this.checked)'
			    ));?>
			<label for="siteemailtemp1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'siteemailtemp2', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_siteemailtemplate', 'label'=>false, 'class'=>'siteemailtemplateIds', 'onclick' => 'disableMenuCheckboxes("siteemailtemplateIds",this.checked)', 'checked' => in_array('design/adminpanel_siteemailtemplate',$selected))); ?>
			<label for="siteemailtemp2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'siteemailtemp3', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_siteemailtemplateadd/$', 'label'=>false, 'class'=>'siteemailtemplateIds', 'checked' => in_array('design/adminpanel_siteemailtemplateadd/$',$selected), 'disabled' => !in_array('design/adminpanel_siteemailtemplate',$selected))); ?>
			<label for="siteemailtemp3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'siteemailtemp4', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_siteemailtemplatestatus', 'label'=>false, 'class'=>'siteemailtemplateIds', 'checked' => in_array('design/adminpanel_siteemailtemplatestatus',$selected), 'disabled' => !in_array('design/adminpanel_siteemailtemplate',$selected))); ?>
			<label for="siteemailtemp4"></label>
		</div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Email Templates/Mass Mail Templates</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'massmailtemp1',
			      'onclick' => 'selectMenuCheckboxes("massemailtemplateIds",this.checked)'
			    ));?>
			<label for="massmailtemp1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'massmailtemp2', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_massemailtemplate', 'label'=>false, 'class'=>'massemailtemplateIds', 'onclick' => 'disableMenuCheckboxes("massemailtemplateIds",this.checked)', 'checked' => in_array('design/adminpanel_massemailtemplate',$selected))); ?>
			<label for="massmailtemp2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'massmailtemp3', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_massemailtemplateadd', 'label'=>false, 'class'=>'massemailtemplateIds', 'checked' => in_array('design/adminpanel_massemailtemplateadd',$selected), 'disabled' => !in_array('design/adminpanel_massemailtemplate',$selected))); ?>
			<label for="massmailtemp3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'massmailtemp4', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_massemailtemplateadd/$', 'label'=>false, 'class'=>'massemailtemplateIds', 'checked' => in_array('design/adminpanel_massemailtemplateadd/$',$selected), 'disabled' => !in_array('design/adminpanel_massemailtemplate',$selected))); ?>
			<label for="massmailtemp4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'massmailtemp5', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_massemailtemplatestatus', 'label'=>false, 'class'=>'massemailtemplateIds', 'checked' => in_array('design/adminpanel_massemailtemplatestatus',$selected), 'disabled' => !in_array('design/adminpanel_massemailtemplate',$selected))); ?>
			<label for="massmailtemp5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'massmailtemp6', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_massemailtemplateremove', 'label'=>false, 'class'=>'massemailtemplateIds', 'checked' => in_array('design/adminpanel_massemailtemplateremove',$selected), 'disabled' => !in_array('design/adminpanel_massemailtemplate',$selected))); ?>
			<label for="massmailtemp6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Email Templates/Site Email Templates Categories</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'siteemailtempcat1',
			      'onclick' => 'selectMenuCheckboxes("siteemailtemplatecategoryIds",this.checked)'
			    ));?>
			<label for="siteemailtempcat1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'siteemailtempcat2', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_emailcategory', 'label'=>false, 'class'=>'siteemailtemplatecategoryIds', 'onclick' => 'disableMenuCheckboxes("siteemailtemplatecategoryIds",this.checked)', 'checked' => in_array('design/adminpanel_emailcategory',$selected))); ?>
			<label for="siteemailtempcat2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'siteemailtempcat3', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_emailcategoryadd', 'label'=>false, 'class'=>'siteemailtemplatecategoryIds', 'checked' => in_array('design/adminpanel_emailcategoryadd',$selected), 'disabled' => !in_array('design/adminpanel_emailcategory',$selected))); ?>
			<label for="siteemailtempcat3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'siteemailtempcat4', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_emailcategoryadd/$', 'label'=>false, 'class'=>'siteemailtemplatecategoryIds', 'checked' => in_array('design/adminpanel_emailcategoryadd/$',$selected), 'disabled' => !in_array('design/adminpanel_emailcategory',$selected))); ?>
			<label for="siteemailtempcat4"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'siteemailtempcat5', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_emailcategoryremove', 'label'=>false, 'class'=>'siteemailtemplatecategoryIds', 'checked' => in_array('design/adminpanel_emailcategoryremove',$selected), 'disabled' => !in_array('design/adminpanel_emailcategory',$selected))); ?>
			<label for="siteemailtempcat5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Languages</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'languages1',
			      'onclick' => 'selectMenuCheckboxes("languageIds",this.checked)'
			    ));?>
			<label for="languages1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'languages2', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_languageeditphase', 'label'=>false, 'class'=>'languageIds', 'onclick' => 'disableMenuCheckboxes("languageIds",this.checked)', 'checked' => in_array('design/adminpanel_languageeditphase',$selected))); ?>
			<label for="languages2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'languages3', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_languageeditphaseadd', 'label'=>false, 'class'=>'languageIds', 'checked' => in_array('design/adminpanel_languageeditphaseadd',$selected), 'disabled' => !in_array('design/adminpanel_languageeditphase',$selected))); ?>
			<label for="languages3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'languages4', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_languageeditphaseadd/$', 'label'=>false, 'class'=>'languageIds', 'checked' => in_array('design/adminpanel_languageeditphaseadd/$',$selected), 'disabled' => !in_array('design/adminpanel_languageeditphase',$selected))); ?>
			<label for="languages4"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'languages5', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_languageeditphaseremove', 'label'=>false, 'class'=>'languageIds', 'checked' => in_array('design/adminpanel_languageeditphaseremove',$selected), 'disabled' => !in_array('design/adminpanel_languageeditphase',$selected))); ?>
			<label for="languages5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'languages6', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_languagephase', 'label'=>false, 'class'=>'languageIds', 'checked' => in_array('design/adminpanel_languagephase',$selected), 'disabled' => !in_array('design/adminpanel_languageeditphase',$selected))); ?>
			<label for="languages6">Edit Phase</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Language Phases</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'languagephases1',
			      'onclick' => 'selectMenuCheckboxes("languagephaseIds",this.checked)'
			    ));?>
			<label for="languagephases1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'languagephases2', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_languagephase', 'label'=>false, 'class'=>'languagephaseIds', 'onclick' => 'disableMenuCheckboxes("languagephaseIds",this.checked)', 'checked' => in_array('design/adminpanel_languagephase',$selected))); ?>
			<label for="languagephases2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'languagephases3', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_languagephaseadd', 'label'=>false, 'class'=>'languagephaseIds', 'checked' => in_array('design/adminpanel_languagephaseadd',$selected), 'disabled' => !in_array('design/adminpanel_languagephase',$selected))); ?>
			<label for="languagephases3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'languagephases4', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_languagephaseadd/$', 'label'=>false, 'class'=>'languagephaseIds', 'checked' => in_array('design/adminpanel_languagephaseadd/$',$selected), 'disabled' => !in_array('design/adminpanel_languagephase',$selected))); ?>
			<label for="languagephases4"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiondesign.', array('type' => 'checkbox', 'id' => 'languagephases5', 'hiddenField'=>false, 'div'=>false, 'value'=>'design/adminpanel_languagephaseremove', 'label'=>false, 'class'=>'languagephaseIds', 'checked' => in_array('design/adminpanel_languagephaseremove',$selected), 'disabled' => !in_array('design/adminpanel_languagephase',$selected))); ?>
			<label for="languagephases5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Website Pages</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'webpages1',
			      'onclick' => 'selectMenuCheckboxes("websitepageIds",this.checked)'
			    ));?>
			<label for="webpages1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'webpages2', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_index', 'label'=>false, 'class'=>'websitepageIds', 'onclick' => 'disableMenuCheckboxes("websitepageIds",this.checked)', 'checked' => in_array('contentmanagement/adminpanel_index',$selected))); ?>
			<label for="webpages2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'webpages3', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_websitepageedit', 'label'=>false, 'class'=>'websitepageIds', 'checked' => in_array('contentmanagement/adminpanel_websitepageedit',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_index',$selected))); ?>
			<label for="webpages3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'webpages4', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_websitepageedit/$', 'label'=>false, 'class'=>'websitepageIds', 'checked' => in_array('contentmanagement/adminpanel_websitepageedit/$',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_index',$selected))); ?>
			<label for="webpages4"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'webpages5', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_websitepageremove', 'label'=>false, 'class'=>'websitepageIds', 'checked' => in_array('contentmanagement/adminpanel_websitepageremove',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_index',$selected))); ?>
			<label for="webpages5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Website Page Categories</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'webpagecategories1',
			      'onclick' => 'selectMenuCheckboxes("websitepagecategoryIds",this.checked)'
			    ));?>
			<label for="webpagecategories1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'webpagecategories2', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_pagecategory', 'label'=>false, 'class'=>'websitepagecategoryIds', 'onclick' => 'disableMenuCheckboxes("websitepagecategoryIds",this.checked)', 'checked' => in_array('contentmanagement/adminpanel_pagecategory',$selected))); ?>
			<label for="webpagecategories2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'webpagecategories3', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_pagecategoryadd', 'label'=>false, 'class'=>'websitepagecategoryIds', 'checked' => in_array('contentmanagement/adminpanel_pagecategoryadd',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_pagecategory',$selected))); ?>
			<label for="webpagecategories3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'webpagecategories4', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_pagecategoryadd/$', 'label'=>false, 'class'=>'websitepagecategoryIds', 'checked' => in_array('contentmanagement/adminpanel_pagecategoryadd/$',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_pagecategory',$selected))); ?>
			<label for="webpagecategories4"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'webpagecategories5', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_pagecategoryremove', 'label'=>false, 'class'=>'websitepagecategoryIds', 'checked' => in_array('contentmanagement/adminpanel_pagecategoryremove',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_pagecategory',$selected))); ?>
			<label for="webpagecategories5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Virtual Pages</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'virtualpages1',
			      'onclick' => 'selectMenuCheckboxes("sitepageIds",this.checked)'
			    ));?>
			<label for="virtualpages1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'virtualpages2', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_sitepages', 'label'=>false, 'class'=>'sitepageIds', 'onclick' => 'disableMenuCheckboxes("sitepageIds",this.checked)', 'checked' => in_array('contentmanagement/adminpanel_sitepages',$selected))); ?>
			<label for="virtualpages2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'virtualpages3', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_sitepageadd', 'label'=>false, 'class'=>'sitepageIds', 'checked' => in_array('contentmanagement/adminpanel_sitepageadd',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_sitepages',$selected))); ?>
			<label for="virtualpages3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'virtualpages4', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_sitepageadd/$', 'label'=>false, 'class'=>'sitepageIds', 'checked' => in_array('contentmanagement/adminpanel_sitepageadd/$',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_sitepages',$selected))); ?>
			<label for="virtualpages4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'virtualpages5', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_sitepageaction', 'label'=>false, 'class'=>'sitepageIds', 'checked' => in_array('contentmanagement/adminpanel_sitepageaction',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_sitepages',$selected))); ?>
			<label for="virtualpages5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'virtualpages6', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_sitepageremove', 'label'=>false, 'class'=>'sitepageIds', 'checked' => in_array('contentmanagement/adminpanel_sitepageremove',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_sitepages',$selected))); ?>
			<label for="virtualpages6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>News</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'news1',
			      'onclick' => 'selectMenuCheckboxes("newsIds",this.checked)'
			    ));?>
			<label for="news1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'news2', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_news', 'label'=>false, 'class'=>'newsIds', 'onclick' => 'disableMenuCheckboxes("newsIds",this.checked)', 'checked' => in_array('contentmanagement/adminpanel_news',$selected))); ?>
			<label for="news2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'news3', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_newsadd', 'label'=>false, 'class'=>'newsIds', 'checked' => in_array('contentmanagement/adminpanel_newsadd',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_news',$selected))); ?>
			<label for="news3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'news4', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_newsadd/$', 'label'=>false, 'class'=>'newsIds', 'checked' => in_array('contentmanagement/adminpanel_newsadd/$',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_news',$selected))); ?>
			<label for="news4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'news5', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_newsstatus', 'label'=>false, 'class'=>'newsIds', 'checked' => in_array('contentmanagement/adminpanel_newsstatus',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_news',$selected))); ?>
			<label for="news5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'news6', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_newsremove', 'label'=>false, 'class'=>'newsIds', 'checked' => in_array('contentmanagement/adminpanel_newsremove',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_news',$selected))); ?>
			<label for="news6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>FAQs</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'faqs1',
			      'onclick' => 'selectMenuCheckboxes("faqIds",this.checked)'
			    ));?>
			<label for="faqs1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'faqs2', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_faqs', 'label'=>false, 'class'=>'faqIds', 'onclick' => 'disableMenuCheckboxes("faqIds",this.checked)', 'checked' => in_array('contentmanagement/adminpanel_faqs',$selected))); ?>
			<label for="faqs2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'faqs3', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_faqadd', 'label'=>false, 'class'=>'faqIds', 'checked' => in_array('contentmanagement/adminpanel_faqadd',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_faqs',$selected))); ?>
			<label for="faqs3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'faqs4', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_faqadd/$', 'label'=>false, 'class'=>'faqIds', 'checked' => in_array('contentmanagement/adminpanel_faqadd/$',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_faqs',$selected))); ?>
			<label for="faqs4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'faqs5', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_faqstatus', 'label'=>false, 'class'=>'faqIds', 'checked' => in_array('contentmanagement/adminpanel_faqstatus',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_faqs',$selected))); ?>
			<label for="faqs5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'faqs6', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_faqremove', 'label'=>false, 'class'=>'faqIds', 'checked' => in_array('contentmanagement/adminpanel_faqremove',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_faqs',$selected))); ?>
			<label for="faqs6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>FAQ Categories</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'faqcategories1',
			      'onclick' => 'selectMenuCheckboxes("faqcategoryIds",this.checked)'
			    ));?>
			<label for="faqcategories1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'faqcategories2', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_faqcategories', 'label'=>false, 'class'=>'faqcategoryIds', 'onclick' => 'disableMenuCheckboxes("faqcategoryIds",this.checked)', 'checked' => in_array('contentmanagement/adminpanel_faqcategories',$selected))); ?>
			<label for="faqcategories2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'faqcategories3', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_faqcategoryadd', 'label'=>false, 'class'=>'faqcategoryIds', 'checked' => in_array('contentmanagement/adminpanel_faqcategoryadd',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_faqcategories',$selected))); ?>
			<label for="faqcategories3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'faqcategories4', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_faqcategoryadd/$', 'label'=>false, 'class'=>'faqcategoryIds', 'checked' => in_array('contentmanagement/adminpanel_faqcategoryadd/$',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_faqcategories',$selected))); ?>
			<label for="faqcategories4"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'faqcategories5', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_faqcategoryremove', 'label'=>false, 'class'=>'faqcategoryIds', 'checked' => in_array('contentmanagement/adminpanel_faqcategoryremove',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_faqcategories',$selected))); ?>
			<label for="faqcategories5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Testimonials</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'testimonials1',
			      'onclick' => 'selectMenuCheckboxes("testimonialIds",this.checked)'
			    ));?>
			<label for="testimonials1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'testimonials2', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_testimonials', 'label'=>false, 'class'=>'testimonialIds', 'onclick' => 'disableMenuCheckboxes("testimonialIds",this.checked)', 'checked' => in_array('contentmanagement/adminpanel_testimonials',$selected))); ?>
			<label for="testimonials2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'testimonials3', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_testimonialadd', 'label'=>false, 'class'=>'testimonialIds', 'checked' => in_array('contentmanagement/adminpanel_testimonialadd',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_testimonials',$selected))); ?>
			<label for="testimonials3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'testimonials4', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_testimonialadd/$', 'label'=>false, 'class'=>'testimonialIds', 'checked' => in_array('contentmanagement/adminpanel_testimonialadd/$',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_testimonials',$selected))); ?>
			<label for="testimonials4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'testimonials5', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_testimonialstatus', 'label'=>false, 'class'=>'testimonialIds', 'checked' => in_array('contentmanagement/adminpanel_testimonialstatus',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_testimonials',$selected))); ?>
			<label for="testimonials5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactioncontentmanagement.', array('type' => 'checkbox', 'id' => 'testimonials6', 'hiddenField'=>false, 'div'=>false, 'value'=>'contentmanagement/adminpanel_testimonialremove', 'label'=>false, 'class'=>'testimonialIds', 'checked' => in_array('contentmanagement/adminpanel_testimonialremove',$selected), 'disabled' => !in_array('contentmanagement/adminpanel_testimonials',$selected))); ?>
			<label for="testimonials6"></label>
		</div>
		<div></div>
	</div>
    </div>
	</div>
	
	<div class="submenu promotools" style="display:none;">
		<div class="addnew-button checkbox">
			<?php echo $this->Form->input('subaction.', array('type' => 'checkbox', 'id' => 'promotoolsall', 'class' => 'promotools', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools', 'label'=>false, 'checked' => in_array('promotools',$selected), 'onclick'=>"checkall(this);")); ?><label for="promotoolsall"></label>
		</div>
		<div class="clearboth"></div>
	<div class="tablegrid">
        <div class="tablegridheader nofixheader">
		<div>
			<?php echo "Submenu"; ?>&nbsp;&nbsp;
		</div>
		<div>All</div>
		<div>View</div>
		<div>Add</div>
		<div>Edit</div>
		<div>Status</div>
		<div>Delete</div>
		<div>Extra</div>
	</div>
	<div class="tablegridrow">
		<div>Banners</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'userbanners1',
			      'onclick' => 'selectMenuCheckboxes("userbannerIds",this.checked)'
			    ));?>
			<label for="userbanners1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'userbanners2', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_index', 'label'=>false, 'class'=>'userbannerIds', 'onclick' => 'disableMenuCheckboxes("userbannerIds",this.checked)', 'checked' => in_array('promotools/adminpanel_index',$selected))); ?>
			<label for="userbanners2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'userbanners3', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_userbanneradd', 'label'=>false, 'class'=>'userbannerIds', 'checked' => in_array('promotools/adminpanel_userbanneradd',$selected), 'disabled' => !in_array('promotools/adminpanel_index',$selected))); ?>
			<label for="userbanners3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'userbanners4', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_userbanneradd/$', 'label'=>false, 'class'=>'userbannerIds', 'checked' => in_array('promotools/adminpanel_userbanneradd/$',$selected), 'disabled' => !in_array('promotools/adminpanel_index',$selected))); ?>
			<label for="userbanners4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'userbanners5', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_userbannerstatus', 'label'=>false, 'class'=>'userbannerIds', 'checked' => in_array('promotools/adminpanel_userbannerstatus',$selected), 'disabled' => !in_array('promotools/adminpanel_index',$selected))); ?>
			<label for="userbanners5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'userbanners6', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_userbannerremove', 'label'=>false, 'class'=>'userbannerIds', 'checked' => in_array('promotools/adminpanel_userbannerremove',$selected), 'disabled' => !in_array('promotools/adminpanel_index',$selected))); ?>
			<label for="userbanners6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Dynamic Banners</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'dynamicbanner1',
			      'onclick' => 'selectMenuCheckboxes("dynamicbannerIds",this.checked)'
			    ));?>
			<label for="dynamicbanner1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionstatbanner.', array('type' => 'checkbox', 'id' => 'dynamicbanner2', 'hiddenField'=>false, 'div'=>false, 'value'=>'statbanner/adminpanel_banner', 'label'=>false, 'class'=>'dynamicbannerIds', 'onclick' => 'disableMenuCheckboxes("dynamicbannerIds",this.checked)', 'checked' => in_array('statbanner/adminpanel_banner',$selected))); ?>
			<label for="dynamicbanner2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionstatbanner.', array('type' => 'checkbox', 'id' => 'dynamicbanner3', 'hiddenField'=>false, 'div'=>false, 'value'=>'statbanner/adminpanel_add', 'label'=>false, 'class'=>'dynamicbannerIds', 'checked' => in_array('statbanner/adminpanel_add',$selected), 'disabled' => !in_array('statbanner/adminpanel_banner',$selected))); ?>
			<label for="dynamicbanner3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionstatbanner.', array('type' => 'checkbox', 'id' => 'dynamicbanner4', 'hiddenField'=>false, 'div'=>false, 'value'=>'statbanner/adminpanel_add/$', 'label'=>false, 'class'=>'dynamicbannerIds', 'checked' => in_array('statbanner/adminpanel_add/$',$selected), 'disabled' => !in_array('statbanner/adminpanel_banner',$selected))); ?>
			<label for="dynamicbanner4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionstatbanner.', array('type' => 'checkbox', 'id' => 'dynamicbanner5', 'hiddenField'=>false, 'div'=>false, 'value'=>'statbanner/adminpanel_status', 'label'=>false, 'class'=>'dynamicbannerIds', 'checked' => in_array('statbanner/adminpanel_status',$selected), 'disabled' => !in_array('statbanner/adminpanel_banner',$selected))); ?>
			<label for="dynamicbanner5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionstatbanner.', array('type' => 'checkbox', 'id' => 'dynamicbanner6', 'hiddenField'=>false, 'div'=>false, 'value'=>'statbanner/adminpanel_remove', 'label'=>false, 'class'=>'dynamicbannerIds', 'checked' => in_array('statbanner/adminpanel_remove',$selected), 'disabled' => !in_array('statbanner/adminpanel_banner',$selected))); ?>
			<label for="dynamicbanner5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Splash Pages</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'splashpage1',
			      'onclick' => 'selectMenuCheckboxes("splashpageIds",this.checked)'
			    ));?>
			<label for="splashpage1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'splashpage2', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_splashpages', 'label'=>false, 'class'=>'splashpageIds', 'onclick' => 'disableMenuCheckboxes("splashpageIds",this.checked)', 'checked' => in_array('promotools/adminpanel_splashpages',$selected))); ?>
			<label for="splashpage2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'splashpage3', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_splashpageadd', 'label'=>false, 'class'=>'splashpageIds', 'checked' => in_array('promotools/adminpanel_splashpageadd',$selected), 'disabled' => !in_array('promotools/adminpanel_splashpages',$selected))); ?>
			<label for="splashpage3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'splashpage4', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_splashpageadd/$', 'label'=>false, 'class'=>'splashpageIds', 'checked' => in_array('promotools/adminpanel_splashpageadd/$',$selected), 'disabled' => !in_array('promotools/adminpanel_splashpages',$selected))); ?>
			<label for="splashpage4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'splashpage5', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_splashpagestatus', 'label'=>false, 'class'=>'splashpageIds', 'checked' => in_array('promotools/adminpanel_splashpagestatus',$selected), 'disabled' => !in_array('promotools/adminpanel_splashpages',$selected))); ?>
			<label for="splashpage5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'splashpage6', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_splashpageremove', 'label'=>false, 'class'=>'splashpageIds', 'checked' => in_array('promotools/adminpanel_splashpageremove',$selected), 'disabled' => !in_array('promotools/adminpanel_splashpages',$selected))); ?>
			<label for="splashpage6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Landing Pages</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'landingpage1',
			      'onclick' => 'selectMenuCheckboxes("landingpageIds",this.checked)'
			    ));?>
			<label for="landingpage1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'landingpage2', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_landingpages', 'label'=>false, 'class'=>'landingpageIds', 'onclick' => 'disableMenuCheckboxes("landingpageIds",this.checked)', 'checked' => in_array('promotools/adminpanel_landingpages',$selected))); ?>
			<label for="landingpage2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'landingpage3', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_landingpageadd', 'label'=>false, 'class'=>'landingpageIds', 'checked' => in_array('promotools/adminpanel_landingpageadd',$selected), 'disabled' => !in_array('promotools/adminpanel_landingpages',$selected))); ?>
			<label for="landingpage3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'landingpage4', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_landingpageadd/$', 'label'=>false, 'class'=>'landingpageIds', 'checked' => in_array('promotools/adminpanel_landingpageadd/$',$selected), 'disabled' => !in_array('promotools/adminpanel_landingpages',$selected))); ?>
			<label for="landingpage4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'landingpage5', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_landingpagestatus', 'label'=>false, 'class'=>'landingpageIds', 'checked' => in_array('promotools/adminpanel_landingpagestatus',$selected), 'disabled' => !in_array('promotools/adminpanel_landingpages',$selected))); ?>
			<label for="landingpage5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'landingpage6', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_landingpageremove', 'label'=>false, 'class'=>'landingpageIds', 'checked' => in_array('promotools/adminpanel_landingpageremove',$selected), 'disabled' => !in_array('promotools/adminpanel_landingpages',$selected))); ?>
			<label for="landingpage6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Text Links</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'textlinks1',
			      'onclick' => 'selectMenuCheckboxes("textlinkIds",this.checked)'
			    ));?>
			<label for="textlinks1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'textlinks2', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_textlinks', 'label'=>false, 'class'=>'textlinkIds', 'onclick' => 'disableMenuCheckboxes("textlinkIds",this.checked)', 'checked' => in_array('promotools/adminpanel_textlinks',$selected))); ?>
			<label for="textlinks2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'textlinks3', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_textlinkadd', 'label'=>false, 'class'=>'textlinkIds', 'checked' => in_array('promotools/adminpanel_textlinkadd',$selected), 'disabled' => !in_array('promotools/adminpanel_textlinks',$selected))); ?>
			<label for="textlinks3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'textlinks4', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_textlinkadd/$', 'label'=>false, 'class'=>'textlinkIds', 'checked' => in_array('promotools/adminpanel_textlinkadd/$',$selected), 'disabled' => !in_array('promotools/adminpanel_textlinks',$selected))); ?>
			<label for="textlinks4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'textlinks5', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_textlinkstatus', 'label'=>false, 'class'=>'textlinkIds', 'checked' => in_array('promotools/adminpanel_textlinkstatus',$selected), 'disabled' => !in_array('promotools/adminpanel_textlinks',$selected))); ?>
			<label for="textlinks5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'textlinks6', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_textlinkremove', 'label'=>false, 'class'=>'textlinkIds', 'checked' => in_array('promotools/adminpanel_textlinkremove',$selected), 'disabled' => !in_array('promotools/adminpanel_textlinks',$selected))); ?>
			<label for="textlinks6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Promotional Emails</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'promoemails1',
			      'onclick' => 'selectMenuCheckboxes("emailadIds",this.checked)'
			    ));?>
			<label for="promoemails1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'promoemails2', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_emailads', 'label'=>false, 'class'=>'emailadIds', 'onclick' => 'disableMenuCheckboxes("emailadIds",this.checked)', 'checked' => in_array('promotools/adminpanel_emailads',$selected))); ?>
			<label for="promoemails2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'promoemails3', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_emailadadd', 'label'=>false, 'class'=>'emailadIds', 'checked' => in_array('promotools/adminpanel_emailadadd',$selected), 'disabled' => !in_array('promotools/adminpanel_emailads',$selected))); ?>
			<label for="promoemails3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'promoemails4', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_emailadadd/$', 'label'=>false, 'class'=>'emailadIds', 'checked' => in_array('promotools/adminpanel_emailadadd/$',$selected), 'disabled' => !in_array('promotools/adminpanel_emailads',$selected))); ?>
			<label for="promoemails4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'promoemails5', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_emailadstatus', 'label'=>false, 'class'=>'emailadIds', 'checked' => in_array('promotools/adminpanel_emailadstatus',$selected), 'disabled' => !in_array('promotools/adminpanel_emailads',$selected))); ?>
			<label for="promoemails5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionpromotools.', array('type' => 'checkbox', 'id' => 'promoemails6', 'hiddenField'=>false, 'div'=>false, 'value'=>'promotools/adminpanel_emailadremove', 'label'=>false, 'class'=>'emailadIds', 'checked' => in_array('promotools/adminpanel_emailadremove',$selected), 'disabled' => !in_array('promotools/adminpanel_emailads',$selected))); ?>
			<label for="promoemails6"></label>
		</div>
		<div></div>
	</div>
    </div>
	</div>
	
	<div class="submenu advertisement" style="display:none;">
		<div class="addnew-button checkbox">
			<?php echo $this->Form->input('subaction.', array('type' => 'checkbox', 'id' => 'advertisementall', 'class' => 'advertisement', 'hiddenField'=>false, 'div'=>false, 'value'=>'advertisement', 'label'=>false, 'checked' => in_array('advertisement',$selected), 'onclick'=>"checkall(this);")); ?><label for="advertisementall"></label>
		</div>
		<div class="clearboth"></div>	
	<div class="tablegrid">
        <div class="tablegridheader nofixheader">
		<div>
			<?php echo "Submenu"; ?>&nbsp;&nbsp;
		</div>
		<div>All</div>
		<div>View</div>
		<div>Add</div>
		<div>Edit</div>
		<div>Status</div>
		<div>Delete</div>
		<div>Extra</div>
	</div>
	<div class="tablegridrow">
		<div>Banner Ads/Credit Banner Ads</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'crbannerads1',
			      'onclick' => 'selectMenuCheckboxes("bannerIds",this.checked)'
			    ));?>
			<label for="crbannerads1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'crbannerads2', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_index', 'label'=>false, 'class'=>'bannerIds', 'onclick' => 'disableMenuCheckboxes("bannerIds",this.checked)', 'checked' => in_array('bannerad/adminpanel_index',$selected))); ?>
			<label for="crbannerads2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'crbannerads3', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_userbanneradd', 'label'=>false, 'class'=>'bannerIds', 'checked' => in_array('bannerad/adminpanel_userbanneradd',$selected), 'disabled' => !in_array('bannerad/adminpanel_index',$selected))); ?>
			<label for="crbannerads3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'crbannerads4', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_userbanneradd/$', 'label'=>false, 'class'=>'bannerIds', 'checked' => in_array('bannerad/adminpanel_userbanneradd/$',$selected), 'disabled' => !in_array('bannerad/adminpanel_index',$selected))); ?>
			<label for="crbannerads4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'crbannerads5', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_userbannerstatus', 'label'=>false, 'class'=>'bannerIds', 'checked' => in_array('bannerad/adminpanel_userbannerstatus',$selected), 'disabled' => !in_array('bannerad/adminpanel_index',$selected))); ?>
			<label for="crbannerads5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'crbannerads6', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_userbannerremove', 'label'=>false, 'class'=>'bannerIds', 'checked' => in_array('bannerad/adminpanel_userbannerremove',$selected), 'disabled' => !in_array('bannerad/adminpanel_index',$selected))); ?>
			<label for="crbannerads6"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'crbannerads7', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_bannercredit', 'label'=>false, 'class'=>'bannerIds', 'checked' => in_array('bannerad/adminpanel_bannercredit',$selected), 'disabled' => !in_array('bannerad/adminpanel_index',$selected))); ?>
			<label for="crbannerads7">Credit</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Banner Ads/Plan Banner Ads</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'planbannerads1',
			      'onclick' => 'selectMenuCheckboxes("planbannerIds",this.checked)'
			    ));?>
			<label for="planbannerads1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'planbannerads2', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_planmember', 'label'=>false, 'class'=>'planbannerIds', 'onclick' => 'disableMenuCheckboxes("planbannerIds",this.checked)', 'checked' => in_array('bannerad/adminpanel_planmember',$selected))); ?>
			<label for="planbannerads2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'planbannerads3', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_planmemberadd/$', 'label'=>false, 'class'=>'planbannerIds', 'checked' => in_array('bannerad/adminpanel_planmemberadd/$',$selected), 'disabled' => !in_array('bannerad/adminpanel_planmember',$selected))); ?>
			<label for="planbannerads3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'planbannerads4', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_planmemberstatus', 'label'=>false, 'class'=>'planbannerIds', 'checked' => in_array('bannerad/adminpanel_planmemberstatus',$selected), 'disabled' => !in_array('bannerad/adminpanel_planmember',$selected))); ?>
			<label for="planbannerads4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'planbannerads5', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_planmemberremove', 'label'=>false, 'class'=>'planbannerIds', 'checked' => in_array('bannerad/adminpanel_planmemberremove',$selected), 'disabled' => !in_array('bannerad/adminpanel_planmember',$selected))); ?>
			<label for="planbannerads5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Banner Ads/Banner Ad Plans</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'banneradplans1',
			      'onclick' => 'selectMenuCheckboxes("banneradplanIds",this.checked)'
			    ));?>
			<label for="banneradplans1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'banneradplans2', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_plan', 'label'=>false, 'class'=>'banneradplanIds', 'onclick' => 'disableMenuCheckboxes("banneradplanIds",this.checked)', 'checked' => in_array('bannerad/adminpanel_plan',$selected))); ?>
			<label for="banneradplans2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'banneradplans3', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_planadd', 'label'=>false, 'class'=>'banneradplanIds', 'checked' => in_array('bannerad/adminpanel_planadd',$selected), 'disabled' => !in_array('bannerad/adminpanel_plan',$selected))); ?>
			<label for="banneradplans3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'banneradplans4', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_planadd/$', 'label'=>false, 'class'=>'banneradplanIds', 'checked' => in_array('bannerad/adminpanel_planadd/$',$selected), 'disabled' => !in_array('bannerad/adminpanel_plan',$selected))); ?>
			<label for="banneradplans4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'banneradplans5', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_status', 'label'=>false, 'class'=>'banneradplanIds', 'checked' => in_array('bannerad/adminpanel_status',$selected), 'disabled' => !in_array('bannerad/adminpanel_plan',$selected))); ?>
			<label for="banneradplans5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'banneradplans6', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_remove', 'label'=>false, 'class'=>'banneradplanIds', 'checked' => in_array('bannerad/adminpanel_remove',$selected), 'disabled' => !in_array('bannerad/adminpanel_plan',$selected))); ?>
			<label for="banneradplans6"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionbannerad.', array('type' => 'checkbox', 'id' => 'banneradplans7', 'hiddenField'=>false, 'div'=>false, 'value'=>'bannerad/adminpanel_planmember', 'label'=>false, 'class'=>'banneradplanIds', 'checked' => in_array('bannerad/adminpanel_planmember',$selected), 'disabled' => !in_array('bannerad/adminpanel_plan',$selected))); ?>
			<label for="banneradplans7">View Members</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Text Ads/Credit Text Ads</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'crtextads1',
			      'onclick' => 'selectMenuCheckboxes("credittextIds",this.checked)'
			    ));?>
			<label for="crtextads1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'crtextads2', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_index', 'label'=>false, 'class'=>'credittextIds', 'onclick' => 'disableMenuCheckboxes("credittextIds",this.checked)', 'checked' => in_array('textad/adminpanel_index',$selected))); ?>
			<label for="crtextads2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'crtextads3', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_textadadd', 'label'=>false, 'class'=>'credittextIds', 'checked' => in_array('textad/adminpanel_textadadd',$selected), 'disabled' => !in_array('textad/adminpanel_index',$selected))); ?>
			<label for="crtextads3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'crtextads4', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_textadadd/$', 'label'=>false, 'class'=>'credittextIds', 'checked' => in_array('textad/adminpanel_textadadd/$',$selected), 'disabled' => !in_array('textad/adminpanel_index',$selected))); ?>
			<label for="crtextads4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'crtextads5', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_textadstatus', 'label'=>false, 'class'=>'credittextIds', 'checked' => in_array('textad/adminpanel_textadstatus',$selected), 'disabled' => !in_array('textad/adminpanel_index',$selected))); ?>
			<label for="crtextads5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'crtextads6', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_textadremove', 'label'=>false, 'class'=>'credittextIds', 'checked' => in_array('textad/adminpanel_textadremove',$selected), 'disabled' => !in_array('textad/adminpanel_index',$selected))); ?>
			<label for="crtextads6"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'crtextads7', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_textcredit', 'label'=>false, 'class'=>'credittextIds', 'checked' => in_array('textad/adminpanel_textcredit',$selected), 'disabled' => !in_array('textad/adminpanel_index',$selected))); ?>
			<label for="crtextads7">Credit</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Text Ads/Plan Text Ads</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'plantextads1',
			      'onclick' => 'selectMenuCheckboxes("plantextIds",this.checked)'
			    ));?>
			<label for="plantextads1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'plantextads2', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_planmember', 'label'=>false, 'class'=>'plantextIds', 'onclick' => 'disableMenuCheckboxes("plantextIds",this.checked)', 'checked' => in_array('textad/adminpanel_planmember',$selected))); ?>
			<label for="plantextads2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'plantextads3', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_planmemberadd/$', 'label'=>false, 'class'=>'plantextIds', 'checked' => in_array('textad/adminpanel_planmemberadd/$',$selected), 'disabled' => !in_array('textad/adminpanel_planmember',$selected))); ?>
			<label for="plantextads3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'plantextads4', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_planmemberstatus', 'label'=>false, 'class'=>'plantextIds', 'checked' => in_array('textad/adminpanel_planmemberstatus',$selected), 'disabled' => !in_array('textad/adminpanel_planmember',$selected))); ?>
			<label for="plantextads4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'plantextads5', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_planmemberremove', 'label'=>false, 'class'=>'plantextIds', 'checked' => in_array('textad/adminpanel_planmemberremove',$selected), 'disabled' => !in_array('textad/adminpanel_planmember',$selected))); ?>
			<label for="plantextads5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Text Ads/Text Ad Plans</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'textadplans1',
			      'onclick' => 'selectMenuCheckboxes("textadplanIds",this.checked)'
			    ));?>
			<label for="textadplans1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'textadplans2', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_plan', 'label'=>false, 'class'=>'textadplanIds', 'onclick' => 'disableMenuCheckboxes("textadplanIds",this.checked)', 'checked' => in_array('textad/adminpanel_plan',$selected))); ?>
			<label for="textadplans2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'textadplans3', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_planadd', 'label'=>false, 'class'=>'textadplanIds', 'checked' => in_array('textad/adminpanel_planadd',$selected), 'disabled' => !in_array('textad/adminpanel_plan',$selected))); ?>
			<label for="textadplans3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'textadplans4', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_planadd/$', 'label'=>false, 'class'=>'textadplanIds', 'checked' => in_array('textad/adminpanel_planadd/$',$selected), 'disabled' => !in_array('textad/adminpanel_plan',$selected))); ?>
			<label for="textadplans4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'textadplans5', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_planstatus', 'label'=>false, 'class'=>'textadplanIds', 'checked' => in_array('textad/adminpanel_planstatus',$selected), 'disabled' => !in_array('textad/adminpanel_plan',$selected))); ?>
			<label for="textadplans5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'textadplans6', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_planremove', 'label'=>false, 'class'=>'textadplanIds', 'checked' => in_array('textad/adminpanel_planremove',$selected), 'disabled' => !in_array('textad/adminpanel_plan',$selected))); ?>
			<label for="textadplans6"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactiontextad.', array('type' => 'checkbox', 'id' => 'textadplans7', 'hiddenField'=>false, 'div'=>false, 'value'=>'textad/adminpanel_planmember', 'label'=>false, 'class'=>'textadplanIds', 'checked' => in_array('textad/adminpanel_planmember',$selected), 'disabled' => !in_array('textad/adminpanel_plan',$selected))); ?>
			<label for="textadplans7">View Members</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Solo Ads</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'soloads1',
			      'onclick' => 'selectMenuCheckboxes("soloadIds",this.checked)'
			    ));?>
			<label for="soloads1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsoload.', array('type' => 'checkbox', 'id' => 'soloads2', 'hiddenField'=>false, 'div'=>false, 'value'=>'soload/adminpanel_index', 'label'=>false, 'class'=>'soloadIds', 'onclick' => 'disableMenuCheckboxes("soloadIds",this.checked)', 'checked' => in_array('soload/adminpanel_index',$selected))); ?>
			<label for="soloads2"></label>
		</div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsoload.', array('type' => 'checkbox', 'id' => 'soloads3', 'hiddenField'=>false, 'div'=>false, 'value'=>'soload/adminpanel_soloadstatus', 'label'=>false, 'class'=>'soloadIds', 'checked' => in_array('soload/adminpanel_soloadstatus',$selected), 'disabled' => !in_array('soload/adminpanel_index',$selected))); ?>
			<label for="soloads3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsoload.', array('type' => 'checkbox', 'id' => 'soloads4', 'hiddenField'=>false, 'div'=>false, 'value'=>'soload/adminpanel_soloadremove', 'label'=>false, 'class'=>'soloadIds', 'checked' => in_array('soload/adminpanel_soloadremove',$selected), 'disabled' => !in_array('soload/adminpanel_index',$selected))); ?>
			<label for="soloads4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsoload.', array('type' => 'checkbox', 'id' => 'soloads5', 'hiddenField'=>false, 'div'=>false, 'value'=>'soload/adminpanel_soloadcredit', 'label'=>false, 'class'=>'soloadIds', 'checked' => in_array('soload/adminpanel_soloadcredit',$selected), 'disabled' => !in_array('soload/adminpanel_index',$selected))); ?>
			<label for="soloads5">Credit</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Solo Ads/Solo Ad Plans</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'soloadplans1',
			      'onclick' => 'selectMenuCheckboxes("soloadplanIds",this.checked)'
			    ));?>
			<label for="soloadplans1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsoload.', array('type' => 'checkbox', 'id' => 'soloadplans2', 'hiddenField'=>false, 'div'=>false, 'value'=>'soload/adminpanel_plan', 'label'=>false, 'class'=>'soloadplanIds', 'onclick' => 'disableMenuCheckboxes("soloadplanIds",this.checked)', 'checked' => in_array('soload/adminpanel_plan',$selected))); ?>
			<label for="soloadplans2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsoload.', array('type' => 'checkbox', 'id' => 'soloadplans3', 'hiddenField'=>false, 'div'=>false, 'value'=>'soload/adminpanel_planadd', 'label'=>false, 'class'=>'soloadplanIds', 'checked' => in_array('soload/adminpanel_planadd',$selected), 'disabled' => !in_array('soload/adminpanel_plan',$selected))); ?>
			<label for="soloadplans3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsoload.', array('type' => 'checkbox', 'id' => 'soloadplans4', 'hiddenField'=>false, 'div'=>false, 'value'=>'soload/adminpanel_planadd/$', 'label'=>false, 'class'=>'soloadplanIds', 'checked' => in_array('soload/adminpanel_planadd/$',$selected), 'disabled' => !in_array('soload/adminpanel_plan',$selected))); ?>
			<label for="soloadplans4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsoload.', array('type' => 'checkbox', 'id' => 'soloadplans5', 'hiddenField'=>false, 'div'=>false, 'value'=>'soload/adminpanel_planstatus', 'label'=>false, 'class'=>'soloadplanIds', 'checked' => in_array('soload/adminpanel_planstatus',$selected), 'disabled' => !in_array('soload/adminpanel_plan',$selected))); ?>
			<label for="soloadplans5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsoload.', array('type' => 'checkbox', 'id' => 'soloadplans6', 'hiddenField'=>false, 'div'=>false, 'value'=>'soload/adminpanel_planremove', 'label'=>false, 'class'=>'soloadplanIds', 'checked' => in_array('soload/adminpanel_planremove',$selected), 'disabled' => !in_array('soload/adminpanel_plan',$selected))); ?>
			<label for="soloadplans6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Pay Per Click/PPC Plans</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'ppcplans1',
			      'onclick' => 'selectMenuCheckboxes("ppcplanIds",this.checked)'
			    ));?>
			<label for="ppcplans1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionppc.', array('type' => 'checkbox', 'id' => 'ppcplans2', 'hiddenField'=>false, 'div'=>false, 'value'=>'ppc/adminpanel_plan', 'label'=>false, 'class'=>'ppcplanIds', 'onclick' => 'disableMenuCheckboxes("ppcplanIds",this.checked)', 'checked' => in_array('ppc/adminpanel_plan',$selected))); ?>
			<label for="ppcplans2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionppc.', array('type' => 'checkbox', 'id' => 'ppcplans3', 'hiddenField'=>false, 'div'=>false, 'value'=>'ppc/adminpanel_planadd', 'label'=>false, 'class'=>'ppcplanIds', 'checked' => in_array('ppc/adminpanel_planadd',$selected), 'disabled' => !in_array('ppc/adminpanel_plan',$selected))); ?>
			<label for="ppcplans3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionppc.', array('type' => 'checkbox', 'id' => 'ppcplans4', 'hiddenField'=>false, 'div'=>false, 'value'=>'ppc/adminpanel_planadd/$', 'label'=>false, 'class'=>'ppcplanIds', 'checked' => in_array('ppc/adminpanel_planadd/$',$selected), 'disabled' => !in_array('ppc/adminpanel_plan',$selected))); ?>
			<label for="ppcplans4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionppc.', array('type' => 'checkbox', 'id' => 'ppcplans5', 'hiddenField'=>false, 'div'=>false, 'value'=>'ppc/adminpanel_planstatus', 'label'=>false, 'class'=>'ppcplanIds', 'checked' => in_array('ppc/adminpanel_planstatus',$selected), 'disabled' => !in_array('ppc/adminpanel_plan',$selected))); ?>
			<label for="ppcplans5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionppc.', array('type' => 'checkbox', 'id' => 'ppcplans6', 'hiddenField'=>false, 'div'=>false, 'value'=>'ppc/adminpanel_planremove', 'label'=>false, 'class'=>'ppcplanIds', 'checked' => in_array('ppc/adminpanel_planremove',$selected), 'disabled' => !in_array('ppc/adminpanel_plan',$selected))); ?>
			<label for="ppcplans6"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionppc.', array('type' => 'checkbox', 'id' => 'ppcplans7', 'hiddenField'=>false, 'div'=>false, 'value'=>'ppc/adminpanel_member', 'label'=>false, 'class'=>'ppcplanIds', 'checked' => in_array('ppc/adminpanel_member',$selected), 'disabled' => !in_array('ppc/adminpanel_plan',$selected))); ?>
			<label for="ppcplans7">View Plan Members</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Pay Per Click/PPC Banners</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'ppcbanners1',
			      'onclick' => 'selectMenuCheckboxes("ppcbannerIds",this.checked)'
			    ));?>
			<label for="ppcbanners1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionppc.', array('type' => 'checkbox', 'id' => 'ppcbanners2', 'hiddenField'=>false, 'div'=>false, 'value'=>'ppc/adminpanel_member', 'label'=>false, 'class'=>'ppcbannerIds', 'onclick' => 'disableMenuCheckboxes("ppcbannerIds",this.checked)', 'checked' => in_array('ppc/adminpanel_member',$selected))); ?>
			<label for="ppcbanners2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionppc.', array('type' => 'checkbox', 'id' => 'ppcbanners3', 'hiddenField'=>false, 'div'=>false, 'value'=>'ppc/adminpanel_add/$', 'label'=>false, 'class'=>'ppcbannerIds', 'checked' => in_array('ppc/adminpanel_add/$',$selected), 'disabled' => !in_array('ppc/adminpanel_member',$selected))); ?>
			<label for="ppcbanners3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionppc.', array('type' => 'checkbox', 'id' => 'ppcbanners4', 'hiddenField'=>false, 'div'=>false, 'value'=>'ppc/adminpanel_status', 'label'=>false, 'class'=>'ppcbannerIds', 'checked' => in_array('ppc/adminpanel_status',$selected), 'disabled' => !in_array('ppc/adminpanel_member',$selected))); ?>
			<label for="ppcbanners4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionppc.', array('type' => 'checkbox', 'id' => 'ppcbanners5', 'hiddenField'=>false, 'div'=>false, 'value'=>'ppc/adminpanel_remove', 'label'=>false, 'class'=>'ppcbannerIds', 'checked' => in_array('ppc/adminpanel_remove',$selected), 'disabled' => !in_array('ppc/adminpanel_member',$selected))); ?>
			<label for="ppcbanners5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Paid To Click/PTC Plans</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'ptcplans1',
			      'onclick' => 'selectMenuCheckboxes("ptcplanIds",this.checked)'
			    ));?>
			<label for="ptcplans1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcplans2', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_plan', 'label'=>false, 'class'=>'ptcplanIds', 'onclick' => 'disableMenuCheckboxes("ptcplanIds",this.checked)', 'checked' => in_array('ptc/adminpanel_plan',$selected))); ?>
			<label for="ptcplans2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcplans3', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_planadd', 'label'=>false, 'class'=>'ptcplanIds', 'checked' => in_array('ptc/adminpanel_planadd',$selected), 'disabled' => !in_array('ptc/adminpanel_plan',$selected))); ?>
			<label for="ptcplans3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcplans4', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_planadd/$', 'label'=>false, 'class'=>'ptcplanIds', 'checked' => in_array('ptc/adminpanel_planadd/$',$selected), 'disabled' => !in_array('ptc/adminpanel_plan',$selected))); ?>
			<label for="ptcplans4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcplans5', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_planstatus', 'label'=>false, 'class'=>'ptcplanIds', 'checked' => in_array('ptc/adminpanel_planstatus',$selected), 'disabled' => !in_array('ptc/adminpanel_plan',$selected))); ?>
			<label for="ptcplans5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcplans6', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_planremove', 'label'=>false, 'class'=>'ptcplanIds', 'checked' => in_array('ptc/adminpanel_planremove',$selected), 'disabled' => !in_array('ptc/adminpanel_plan',$selected))); ?>
			<label for="ptcplans6"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcplans7', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_member', 'label'=>false, 'class'=>'ptcplanIds', 'checked' => in_array('ptc/adminpanel_member',$selected), 'disabled' => !in_array('ptc/adminpanel_plan',$selected))); ?>
			<label for="ptcplans7">View Plan Members</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Paid To Click/PTC Ads</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'ptcads1',
			      'onclick' => 'selectMenuCheckboxes("ptcadIds",this.checked)'
			    ));?>
			<label for="ptcads1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcads2', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_member', 'label'=>false, 'class'=>'ptcadIds', 'onclick' => 'disableMenuCheckboxes("ptcadIds",this.checked)', 'checked' => in_array('ptc/adminpanel_member',$selected))); ?>
			<label for="ptcads2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcads3', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_add/$', 'label'=>false, 'class'=>'ptcadIds', 'checked' => in_array('ptc/adminpanel_add/$',$selected), 'disabled' => !in_array('ptc/adminpanel_member',$selected))); ?>
			<label for="ptcads3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcads4', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_status', 'label'=>false, 'class'=>'ptcadIds', 'checked' => in_array('ptc/adminpanel_status',$selected), 'disabled' => !in_array('ptc/adminpanel_member',$selected))); ?>
			<label for="ptcads4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcads5', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_remove', 'label'=>false, 'class'=>'ptcadIds', 'checked' => in_array('ptc/adminpanel_remove',$selected), 'disabled' => !in_array('ptc/adminpanel_member',$selected))); ?>
			<label for="ptcads5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Paid To Click/PTC Themes</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'ptcthemes1',
			      'onclick' => 'selectMenuCheckboxes("ptcthemeIds",this.checked)'
			    ));?>
			<label for="ptcthemes1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcthemes2', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_design', 'label'=>false, 'class'=>'ptcthemeIds', 'onclick' => 'disableMenuCheckboxes("ptcthemeIds",this.checked)', 'checked' => in_array('ptc/adminpanel_design',$selected))); ?>
			<label for="ptcthemes2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcthemes3', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_designadd', 'label'=>false, 'class'=>'ptcthemeIds', 'checked' => in_array('ptc/adminpanel_designadd',$selected), 'disabled' => !in_array('ptc/adminpanel_design',$selected))); ?>
			<label for="ptcthemes3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcthemes4', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_designadd/$', 'label'=>false, 'class'=>'ptcthemeIds', 'checked' => in_array('ptc/adminpanel_designadd/$',$selected), 'disabled' => !in_array('ptc/adminpanel_design',$selected))); ?>
			<label for="ptcthemes4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcthemes5', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_designstatus', 'label'=>false, 'class'=>'ptcthemeIds', 'checked' => in_array('ptc/adminpanel_designstatus',$selected), 'disabled' => !in_array('ptc/adminpanel_design',$selected))); ?>
			<label for="ptcthemes5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionptc.', array('type' => 'checkbox', 'id' => 'ptcthemes6', 'hiddenField'=>false, 'div'=>false, 'value'=>'ptc/adminpanel_designremove', 'label'=>false, 'class'=>'ptcthemeIds', 'checked' => in_array('ptc/adminpanel_designremove',$selected), 'disabled' => !in_array('ptc/adminpanel_design',$selected))); ?>
			<label for="ptcthemes6"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Login Ads/Login Ad Plans</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'loginadplans1',
			      'onclick' => 'selectMenuCheckboxes("loginplanIds",this.checked)'
			    ));?>
			<label for="loginadplans1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionloginad.', array('type' => 'checkbox', 'id' => 'loginadplans2', 'hiddenField'=>false, 'div'=>false, 'value'=>'loginad/adminpanel_plan', 'label'=>false, 'class'=>'loginplanIds', 'onclick' => 'disableMenuCheckboxes("loginplanIds",this.checked)', 'checked' => in_array('loginad/adminpanel_plan',$selected))); ?>
			<label for="loginadplans2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionloginad.', array('type' => 'checkbox', 'id' => 'loginadplans3', 'hiddenField'=>false, 'div'=>false, 'value'=>'loginad/adminpanel_planadd', 'label'=>false, 'class'=>'loginplanIds', 'checked' => in_array('loginad/adminpanel_planadd',$selected), 'disabled' => !in_array('loginad/adminpanel_plan',$selected))); ?>
			<label for="loginadplans3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionloginad.', array('type' => 'checkbox', 'id' => 'loginadplans4', 'hiddenField'=>false, 'div'=>false, 'value'=>'loginad/adminpanel_planadd/$', 'label'=>false, 'class'=>'loginplanIds', 'checked' => in_array('loginad/adminpanel_planadd/$',$selected), 'disabled' => !in_array('loginad/adminpanel_plan',$selected))); ?>
			<label for="loginadplans4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionloginad.', array('type' => 'checkbox', 'id' => 'loginadplans5', 'hiddenField'=>false, 'div'=>false, 'value'=>'loginad/adminpanel_loginadstatus', 'label'=>false, 'class'=>'loginplanIds', 'checked' => in_array('loginad/adminpanel_loginadstatus',$selected), 'disabled' => !in_array('loginad/adminpanel_plan',$selected))); ?>
			<label for="loginadplans5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionloginad.', array('type' => 'checkbox', 'id' => 'loginadplans6', 'hiddenField'=>false, 'div'=>false, 'value'=>'loginad/adminpanel_loginadremove', 'label'=>false, 'class'=>'loginplanIds', 'checked' => in_array('loginad/adminpanel_loginadremove',$selected), 'disabled' => !in_array('loginad/adminpanel_plan',$selected))); ?>
			<label for="loginadplans6"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionloginad.', array('type' => 'checkbox', 'id' => 'loginadplans7', 'hiddenField'=>false, 'div'=>false, 'value'=>'loginad/adminpanel_loginad', 'label'=>false, 'class'=>'loginplanIds', 'checked' => in_array('loginad/adminpanel_loginad',$selected), 'disabled' => !in_array('loginad/adminpanel_plan',$selected))); ?>
			<label for="loginadplans7">View Plan Members</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Login Ads/Login Ads</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'loginads1',
			      'onclick' => 'selectMenuCheckboxes("loginadIds",this.checked)'
			    ));?>
			<label for="loginads1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionloginad.', array('type' => 'checkbox', 'id' => 'loginads2', 'hiddenField'=>false, 'div'=>false, 'value'=>'loginad/adminpanel_loginad', 'label'=>false, 'class'=>'loginadIds', 'onclick' => 'disableMenuCheckboxes("loginadIds",this.checked)', 'checked' => in_array('loginad/adminpanel_loginad',$selected))); ?>
			<label for="loginads2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionloginad.', array('type' => 'checkbox', 'id' => 'loginads3', 'hiddenField'=>false, 'div'=>false, 'value'=>'loginad/adminpanel_loginadadd/$', 'label'=>false, 'class'=>'loginadIds', 'checked' => in_array('loginad/adminpanel_loginadadd/$',$selected), 'disabled' => !in_array('loginad/adminpanel_loginad',$selected))); ?>
			<label for="loginads3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionloginad.', array('type' => 'checkbox', 'id' => 'loginads4', 'hiddenField'=>false, 'div'=>false, 'value'=>'loginad/adminpanel_loginadaddstatus', 'label'=>false, 'class'=>'loginadIds', 'checked' => in_array('loginad/adminpanel_loginadaddstatus',$selected), 'disabled' => !in_array('loginad/adminpanel_loginad',$selected))); ?>
			<label for="loginads4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionloginad.', array('type' => 'checkbox', 'id' => 'loginads5', 'hiddenField'=>false, 'div'=>false, 'value'=>'loginad/adminpanel_loginadaddremove', 'label'=>false, 'class'=>'loginadIds', 'checked' => in_array('loginad/adminpanel_loginadaddremove',$selected), 'disabled' => !in_array('loginad/adminpanel_loginad',$selected))); ?>
			<label for="loginads5"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Message Ads</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'messageads1',
			      'onclick' => 'selectMenuCheckboxes("messageadIds",this.checked)'
			    ));?>
			<label for="messageads1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionadvertisement.', array('type' => 'checkbox', 'id' => 'messageads2', 'hiddenField'=>false, 'div'=>false, 'value'=>'advertisement/adminpanel_messageads', 'label'=>false, 'class'=>'messageadIds', 'onclick' => 'disableMenuCheckboxes("messageadIds",this.checked)', 'checked' => in_array('advertisement/adminpanel_messageads',$selected))); ?>
			<label for="messageads2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionadvertisement.', array('type' => 'checkbox', 'id' => 'messageads3', 'hiddenField'=>false, 'div'=>false, 'value'=>'advertisement/adminpanel_messageadupdate', 'label'=>false, 'class'=>'messageadIds', 'checked' => in_array('advertisement/adminpanel_messageadupdate',$selected), 'disabled' => !in_array('advertisement/adminpanel_messageads',$selected))); ?>
			<label for="messageads3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionadvertisement.', array('type' => 'checkbox', 'id' => 'messageads4', 'hiddenField'=>false, 'div'=>false, 'value'=>'advertisement/adminpanel_messageadstatus', 'label'=>false, 'class'=>'messageadIds', 'checked' => in_array('advertisement/adminpanel_messageadstatus',$selected), 'disabled' => !in_array('advertisement/adminpanel_messageads',$selected))); ?>
			<label for="messageads4"></label>
		</div>
		<div></div>
		<div></div>
	</div>
    </div>
	</div>
	
	<div class="submenu logs" style="display:none;">
		<div class="addnew-button checkbox">
			<?php echo $this->Form->input('subaction.', array('type' => 'checkbox', 'id' => 'logsall', 'class' => 'logs', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs', 'label'=>false, 'checked' => in_array('logs',$selected), 'onclick'=>"checkall(this);")); ?><label for="logsall"></label>
		</div>
		<div class="clearboth"></div>	
	<div class="tablegrid">
        <div class="tablegridheader nofixheader">
		<div>
			<?php echo "Submenu"; ?>&nbsp;&nbsp;
		</div>
		<div>All</div>
		<div>View</div>
		<div>Add</div>
		<div>Edit</div>
		<div>Status</div>
		<div>Delete</div>
		<div>Extra</div>
	</div>
	<div class="tablegridrow">
		<div>Admin Activity</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'adminact1',
			      'onclick' => 'selectMenuCheckboxes("adminactivityIds",this.checked)'
			    ));?>
			<label for="adminact1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'adminact2', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_index', 'label'=>false, 'class'=>'adminactivityIds', 'onclick' => 'disableMenuCheckboxes("adminactivityIds",this.checked)', 'checked' => in_array('logs/adminpanel_index',$selected))); ?>
			<label for="adminact2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'adminact3', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_adminactivityremove', 'label'=>false, 'class'=>'adminactivityIds', 'checked' => in_array('logs/adminpanel_adminactivityremove',$selected), 'disabled' => !in_array('logs/adminpanel_index',$selected))); ?>
			<label for="adminact3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'adminact4', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_adminactivitycsv', 'label'=>false, 'class'=>'adminactivityIds', 'checked' => in_array('logs/adminpanel_adminactivitycsv',$selected), 'disabled' => !in_array('logs/adminpanel_index',$selected))); ?>
			<label for="adminact4">CSV</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Member Activity</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'memberact1',
			      'onclick' => 'selectMenuCheckboxes("memberactivityIds",this.checked)'
			    ));?>
			<label for="memberact1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'memberact2', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_memberactivity', 'label'=>false, 'class'=>'memberactivityIds', 'onclick' => 'disableMenuCheckboxes("memberactivityIds",this.checked)', 'checked' => in_array('logs/adminpanel_memberactivity',$selected))); ?>
			<label for="memberact2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'memberact3', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_memberactivityremove', 'label'=>false, 'class'=>'memberactivityIds', 'checked' => in_array('logs/adminpanel_memberactivityremove',$selected), 'disabled' => !in_array('logs/adminpanel_memberactivity',$selected))); ?>
			<label for="memberact3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'memberact4', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_memberactivitycsv', 'label'=>false, 'class'=>'memberactivityIds', 'checked' => in_array('logs/adminpanel_memberactivitycsv',$selected), 'disabled' => !in_array('logs/adminpanel_memberactivity',$selected))); ?>
			<label for="memberact4">CSV</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Admin Logs</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'adminlogs1',
			      'onclick' => 'selectMenuCheckboxes("adminIds",this.checked)'
			    ));?>
			<label for="adminlogs1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'adminlogs2', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_admin', 'label'=>false, 'class'=>'adminIds', 'onclick' => 'disableMenuCheckboxes("adminIds",this.checked)', 'checked' => in_array('logs/adminpanel_admin',$selected))); ?>
			<label for="adminlogs2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'adminlogs3', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_adminremove', 'label'=>false, 'class'=>'adminIds', 'checked' => in_array('logs/adminpanel_adminremove',$selected), 'disabled' => !in_array('logs/adminpanel_admin',$selected))); ?>
			<label for="adminlogs3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'adminlogs4', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_admincsv', 'label'=>false, 'class'=>'adminIds', 'checked' => in_array('logs/adminpanel_admincsv',$selected), 'disabled' => !in_array('logs/adminpanel_admin',$selected))); ?>
			<label for="adminlogs4">CSV</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Member Logs</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'memberlogs1',
			      'onclick' => 'selectMenuCheckboxes("memberIds",this.checked)'
			    ));?>
			<label for="memberlogs1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'memberlogs2', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_member', 'label'=>false, 'class'=>'memberIds', 'onclick' => 'disableMenuCheckboxes("memberIds",this.checked)', 'checked' => in_array('logs/adminpanel_member',$selected))); ?>
			<label for="memberlogs2"></label>
		</div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'memberlogs3', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_memberstatus', 'label'=>false, 'class'=>'memberIds', 'checked' => in_array('logs/adminpanel_memberstatus',$selected), 'disabled' => !in_array('logs/adminpanel_member',$selected))); ?>
			<label for="memberlogs3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'memberlogs4', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_memberremove', 'label'=>false, 'class'=>'memberIds', 'checked' => in_array('logs/adminpanel_memberremove',$selected), 'disabled' => !in_array('logs/adminpanel_member',$selected))); ?>
			<label for="memberlogs4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'memberlogs5', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_membercsv', 'label'=>false, 'class'=>'memberIds', 'checked' => in_array('logs/adminpanel_membercsv',$selected), 'disabled' => !in_array('logs/adminpanel_member',$selected))); ?>
			<label for="memberlogs5">CSV</label>
		</div>
	</div>
	<div class="tablegridrow">
		<div>Cronjob Logs</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'cronjoblogs1',
			      'onclick' => 'selectMenuCheckboxes("cronjobIds",this.checked)'
			    ));?>
			<label for="cronjoblogs1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'cronjoblogs2', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_cronjob', 'label'=>false, 'class'=>'cronjobIds', 'onclick' => 'disableMenuCheckboxes("cronjobIds",this.checked)', 'checked' => in_array('logs/adminpanel_cronjob',$selected))); ?>
			<label for="cronjoblogs2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'cronjoblogs3', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_cronjobremove', 'label'=>false, 'class'=>'cronjobIds', 'checked' => in_array('logs/adminpanel_cronjobremove',$selected), 'disabled' => !in_array('logs/adminpanel_cronjob',$selected))); ?>
			<label for="cronjoblogs3"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>IP Logs</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'iplogs1', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_iplog', 'label'=>false, 'checked' => in_array('logs/adminpanel_iplog',$selected))); ?>
			<label for="iplogs1"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Registration IP Logs</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'regiplogs1', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_iplogregistration', 'label'=>false, 'checked' => in_array('logs/adminpanel_iplogregistration',$selected))); ?>
			<label for="regiplogs1"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Processor Trace Logs</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'protracelogs1',
			      'onclick' => 'selectMenuCheckboxes("traceIds",this.checked)'
			    ));?>
			<label for="protracelogs1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'protracelogs2', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_posttrace', 'label'=>false, 'class'=>'traceIds', 'onclick' => 'disableMenuCheckboxes("traceIds",this.checked)', 'checked' => in_array('logs/adminpanel_posttrace',$selected))); ?>
			<label for="protracelogs2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'protracelogs3', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_posttraceremove', 'label'=>false, 'class'=>'traceIds', 'checked' => in_array('logs/adminpanel_posttraceremove',$selected), 'disabled' => !in_array('logs/adminpanel_posttrace',$selected))); ?>
			<label for="protracelogs3"></label>
		</div>
		<div></div>
	</div>
	<div class="tablegridrow">
		<div>Sent Emails</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'sentemail1',
			      'onclick' => 'selectMenuCheckboxes("emailtraceIds",this.checked)'
			    ));?>
			<label for="sentemail1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'sentemail2', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_emailtrace', 'label'=>false, 'class'=>'emailtraceIds', 'onclick' => 'disableMenuCheckboxes("emailtraceIds",this.checked)', 'checked' => in_array('logs/adminpanel_emailtrace',$selected))); ?>
			<label for="sentemail2"></label>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionlogs.', array('type' => 'checkbox', 'id' => 'sentemail3', 'hiddenField'=>false, 'div'=>false, 'value'=>'logs/adminpanel_emailtraceremove', 'label'=>false, 'class'=>'emailtraceIds', 'checked' => in_array('logs/adminpanel_emailtraceremove',$selected), 'disabled' => !in_array('logs/adminpanel_emailtrace',$selected))); ?>
			<label for="sentemail3"></label>
		</div>
		<div></div>
	</div>
    </div>
	</div>
	
	<div class="submenu managemodules" style="display:none;">
		<div class="addnew-button checkbox">
			<?php echo $this->Form->input('subaction.', array('type' => 'checkbox', 'id' => 'managemodulesall', 'class' => 'managemodules', 'hiddenField'=>false, 'div'=>false, 'value'=>'managemodule', 'label'=>false, 'checked' => in_array('managemodule',$selected), 'onclick'=>"checkall(this);")); ?><label for="managemodulesall"></label>
		</div>
		<div class="clearboth"></div>
	<div class="tablegrid">
        <div class="tablegridheader nofixheader">
		<div>
			<?php echo "Submenu"; ?>&nbsp;&nbsp;
		</div>
		<div>All</div>
		<div>View</div>
		<div>Add</div>
		<div>Edit</div>
		<div>Status</div>
		<div>Delete</div>
		<div>Extra</div>
	</div>
	<div class="tablegridrow">
		<div>Modules</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'modules1',
			      'onclick' => 'selectMenuCheckboxes("moduleIds",this.checked)'
			    ));?>
			<label for="modules1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmanagemodule.', array('type' => 'checkbox', 'id' => 'modules2', 'hiddenField'=>false, 'div'=>false, 'value'=>'managemodule/adminpanel_index', 'label'=>false, 'class'=>'moduleIds', 'onclick' => 'disableMenuCheckboxes("moduleIds",this.checked)', 'checked' => in_array('managemodule/adminpanel_index',$selected))); ?>
			<label for="modules2"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmanagemodule.', array('type' => 'checkbox', 'id' => 'modules3', 'hiddenField'=>false, 'div'=>false, 'value'=>'managemodule/adminpanel_patch', 'label'=>false, 'class'=>'moduleIds', 'checked' => in_array('managemodule/adminpanel_patch',$selected), 'disabled' => !in_array('managemodule/adminpanel_index',$selected))); ?>
			<label for="modules3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmanagemodule.', array('type' => 'checkbox', 'id' => 'modules4', 'hiddenField'=>false, 'div'=>false, 'value'=>'managemodule/adminpanel_patch/$', 'label'=>false, 'class'=>'moduleIds', 'checked' => in_array('managemodule/adminpanel_patch/$',$selected), 'disabled' => !in_array('managemodule/adminpanel_index',$selected))); ?>
			<label for="modules4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmanagemodule.', array('type' => 'checkbox', 'id' => 'modules5', 'hiddenField'=>false, 'div'=>false, 'value'=>'managemodule/adminpanel_moduleaction', 'label'=>false, 'class'=>'moduleIds', 'checked' => in_array('managemodule/adminpanel_moduleaction',$selected), 'disabled' => !in_array('managemodule/adminpanel_index',$selected))); ?>
			<label for="modules5"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionmanagemodule.', array('type' => 'checkbox', 'id' => 'modules6', 'hiddenField'=>false, 'div'=>false, 'value'=>'managemodule/adminpanel_unpatch', 'label'=>false, 'class'=>'moduleIds', 'checked' => in_array('managemodule/adminpanel_unpatch',$selected), 'disabled' => !in_array('managemodule/adminpanel_index',$selected))); ?>
			<label for="modules6"></label>
		</div>
		<div></div>
	</div>
    </div>
	</div>
	
	<div class="submenu support" style="display:none;">
		<div class="addnew-button checkbox">
			<?php echo $this->Form->input('subaction.', array('type' => 'checkbox', 'id' => 'supportall', 'class' => 'support', 'hiddenField'=>false, 'div'=>false, 'value'=>'support', 'label'=>false, 'checked' => in_array('support',$selected), 'onclick'=>"checkall(this);")); ?><label for="supportall"></label>
		</div>
		<div class="clearboth"></div>
	<div class="tablegrid">
        <div class="tablegridheader nofixheader">
		<div>
			<?php echo "Submenu"; ?>&nbsp;&nbsp;
		</div>
		<div>All</div>
		<div>View</div>
		<div>Add</div>
		<div>Edit</div>
		<div>Status</div>
		<div>Delete</div>
		<div>Extra</div>
	</div>
	<div class="tablegridrow">
		<div>Tickets</div>
		<div class="checkbox">
			<?php 
			    echo $this->Form->checkbox('selectMenuCheckboxes', array(
			      'hiddenField' => false,
			      'id' => 'tickets1',
			      'onclick' => 'selectMenuCheckboxes("ticketIds",this.checked)'
			    ));?>
			<label for="tickets1"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsupport.', array('type' => 'checkbox', 'id' => 'tickets2', 'hiddenField'=>false, 'div'=>false, 'value'=>'support/adminpanel_index', 'label'=>false, 'class'=>'ticketIds', 'onclick' => 'disableMenuCheckboxes("ticketIds",this.checked)', 'checked' => in_array('support/adminpanel_index',$selected))); ?>
			<label for="tickets2"></label>
		</div>
		<div></div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsupport.', array('type' => 'checkbox', 'id' => 'tickets3', 'hiddenField'=>false, 'div'=>false, 'value'=>'support/adminpanel_ticketedit', 'label'=>false, 'class'=>'ticketIds', 'checked' => in_array('support/adminpanel_ticketedit',$selected), 'disabled' => !in_array('support/adminpanel_index',$selected))); ?>
			<label for="tickets3"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsupport.', array('type' => 'checkbox', 'id' => 'tickets4', 'hiddenField'=>false, 'div'=>false, 'value'=>'support/adminpanel_ticketstatus', 'label'=>false, 'class'=>'ticketIds', 'checked' => in_array('support/adminpanel_ticketstatus',$selected), 'disabled' => !in_array('support/adminpanel_index',$selected))); ?>
			<label for="tickets4"></label>
		</div>
		<div class="checkbox">
			<?php echo $this->Form->input('subactionsupport.', array('type' => 'checkbox', 'id' => 'tickets5', 'hiddenField'=>false, 'div'=>false, 'value'=>'support/adminpanel_ticketremove', 'label'=>false, 'class'=>'ticketIds', 'checked' => in_array('support/adminpanel_ticketremove',$selected), 'disabled' => !in_array('support/adminpanel_index',$selected))); ?>
			<label for="tickets5"></label>
		</div>
		<div></div>
	</div>
    </div>
	</div>
	
<?php echo $this->Form->end();?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>