<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-11-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Balance Transfer Settings</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Member to Member Transfer", array('controller'=>'sitesetting', "action"=>"balancetransfer"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Internal Transfer", array('controller'=>'sitesetting', "action"=>"balancetransferprocessor"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
	  </div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Balance_Transfer_Settings#Member_to_Member_Transfer" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<?php echo $this->Form->create('Sitesetting',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'balancetransferaction')));?>
	<?php
	echo $this->Form->input('transfertype', array('type'=>'hidden', 'value'=>1, 'label' => false));
	echo $this->Form->input('defaultstring', array('type'=>'hidden', 'value'=>$defaultstring, 'label' => false));
	?>
	
		<div class="frommain">
			<div class="fromnewtext">Enable Member to Member Balance Transfer :  </div>
			<div class="fromborderdropedown3">
				<div class="select-main">
					<label>
					<?php 
					  echo $this->Form->input('isenable', array(
						  'type' => 'select',
						  'options' => array('1'=>'Yes', '0'=>'No'),
						  'selected' => $isenable,
						  'class'=>'',
						  'label' => false,
						  'div' => false,
						  'style' => '',
						  'onchange'=>'if($(this).val()==1){$(".enadis").show(500);}else{$(".enadis").hide(500);}'
					  ));
					?>
					</label>
				</div>
			</div>
			
			<div class="enadis" style="<?php if($isenable==0){ echo 'display:none;';} ?>">
			<div class="fromnewtext">Auto Approval :  </div>
			<div class="fromborderdropedown3">
				<div class="select-main">
					<label>
					<?php 
					  echo $this->Form->input('memberapprove', array(
						  'type' => 'select',
						  'options' => array('0'=>'Yes', '1'=>'No'),
						  'selected' => $memberapprove,
						  'class'=>'',
						  'label' => false,
						  'div' => false,
						  'style' => ''
					  ));
					?>
					</label>
				</div>
			</div>
			
	
			<div class="fromnewtext">Minimum Balance Transfer Limit($) :<span class="red-color">*</span></div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('minbalancelimit', array('type'=>'text', 'value'=>$minbalancelimit, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>			
			</div>
			
	
			<div class="fromnewtext">Maximum Balance Transfer Limit($) :<span class="red-color">*</span></div>
			<div class="fromborderdropedown3">
				<?php echo $this->Form->input('balancelimit', array('type'=>'text', 'value'=>$balancelimit, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			

			<div class="fromnewtext">Admin Fees :<span class="red-color">*</span></div>
			<div class="fromborderdropedown5">
				<div class="select-main">
					<label>
					<?php 
						  echo $this->Form->input('feestype', array(
							  'type' => 'select',
							  'options' => array('1'=>'Percentage (%)', '0'=>'Dollar ($)'),
							  'selected' => $feestype,
							  'class'=>'',
							  'label' => false,
							  'div' => false,
							  'style' => ''
						  ));
					?>
					</label>
				</div>
			</div>
			<div class="fromborderdropedown5">
				<?php echo $this->Form->input('adminfees', array('type'=>'text', 'value'=>$adminfees, 'label' => false, 'div' => false, 'class'=>'fromboxbghalf borderwidth','style' => ''));?>
			</div>
			<!--    Customization by tushar m start   -->	
			<div class="fromnewtext">From Balance : </div>
			<div class="fromborderdropedown3" style="padding: 4px;">
				  <?php echo $this->Html->image('check.jpg', array('alt' => 'Cash'));?>&nbsp;<span class="red-color">Cash </span> &nbsp;
				<?php
					$pmethod=array('repurchase'=>'Re-purchase', 'earning'=>'Earning', 'commission'=>'Commission');
					if($SITECONFIG["wallet_for_commission"] == 'cash' && $SITECONFIG["wallet_for_earning"] == 'cash')
					{
						$pmethod=array('repurchase'=>'Re-purchase');
					}
					elseif($SITECONFIG["wallet_for_earning"] == 'cash')
					{
						$pmethod=array('repurchase'=>'Re-purchase', 'commission'=>'Commission');
					}
					elseif($SITECONFIG["wallet_for_commission"] == 'cash')
					{
						$pmethod=array('repurchase'=>'Re-purchase', 'earning'=>'Earning');
					}
				  $selected = @explode(",",$frompaymentmethod_member);
				  echo $this->Form->input('frompaymentmethod_member', array('type' => 'select', 'div'=>false, 'label'=>false, 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $pmethod));?>
			</div>
			
			<div class="fromnewtext">To Balance : </div>
			<div class="fromborderdropedown3" style="padding: 4px;">
				  <?php echo $this->Html->image('check.jpg', array('alt' => 'Cash'));?>&nbsp;<span class="red-color">Cash </span> &nbsp;
				<?php
					$pmethod=array('repurchase'=>'Re-purchase', 'earning'=>'Earning', 'commission'=>'Commission');
					if($SITECONFIG["wallet_for_commission"] == 'cash' && $SITECONFIG["wallet_for_earning"] == 'cash')
					{
						$pmethod=array('repurchase'=>'Re-purchase');
					}
					elseif($SITECONFIG["wallet_for_earning"] == 'cash')
					{
						$pmethod=array('repurchase'=>'Re-purchase', 'commission'=>'Commission');
					}
					elseif($SITECONFIG["wallet_for_commission"] == 'cash')
					{
						$pmethod=array('repurchase'=>'Re-purchase', 'earning'=>'Earning');
					}
				  $selected = @explode(",",$topaymentmethod_member);
				  echo $this->Form->input('topaymentmethod_member', array('type' => 'select', 'div'=>false, 'label'=>false, 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $pmethod));?>
			</div>
			
			<!--    Customization by tushar m over   -->
			</div>
		<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_balancetransferaction', $SubadminAccessArray)){ ?>
			<div class="formbutton">
				<?php echo $this->Js->submit('Submit', array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage',
					  'class'=>'btnorange',
					  'div'=>false,
					  'controller'=>'sitesetting',
					  'action'=>'balancetransferaction',
					  'url'   => array('controller' => 'sitesetting', 'action' => 'balancetransferaction')
					));?>
			</div>
		<?php } ?>  
		</div>
	
<?php echo $this->Form->end();?>

<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>