<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 20-11-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Statistics</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Public Statistics", array('controller'=>'sitesetting', "action"=>"publicstatistics"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Member Statistics", array('controller'=>'sitesetting', "action"=>"memberstatistics"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
	<div class="height10"></div>
	
	<div class="helpicon"><a href="https://www.proxscripts.com/docs/Statistics_Settings#Public_Statistics" target="_blank">Help</a></div>
	
    <div id="UpdateMessage"></div>
        <?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'publicstatisticsupdate')));?>
        <?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
			<div class="">
			<div>
				<span class="sta-text">Website Statistics</span>
				<span class="checkbox">
					<?php 
						if($Publicstatistics==1){$checked="checked";}else{$checked="";}
						echo $this->Form->input('chkstatistics', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
				</span>
			</div>		
			<div class="sitesetting">
				<div class="sitesettingmainbox">
					<div class="bluebox">Site Stats :</div>
					<div class="greybox">
						<div class="launchdatebox">Launch Date :</div>
						<div class="launchdatebox">
							<span class="checkbox">
									<?php 
										if($LaunchDate==1){$checked="checked";}else{$checked="";}
										echo $this->Form->input('chklaunch_dt', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"1", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation1">
						<div class="checkbox">
							Launch Date Code
							<?php if($LaunchDateP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('p_launch_dt', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".p_launch_dt").show(500);}else{$(".p_launch_dt").hide(500);}'));
							?>
						</div>
						<div class="copycodetxt p_launch_dt" <?php echo ($LaunchDateP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox p_launch_dt" <?php echo ($LaunchDateP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box" id="textarea">&lt;?php if($StatisticLaunchDate!=''){ echo $this->Time->format($SITECONFIG["timeformate"], $StatisticLaunchDate); }?&gt;</textarea>
						</div>
					</div>
					
					<div class="whitebox">
						<div class="launchdatebox">Alexa Rank :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
								 if($AlexaRank==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('chkalexa', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"2", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation2">
						<div class="checkbox">
							Alexa Rank Code
							<?php 
								if($AlexaRankP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('p_alexa', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".p_alexa").show(500);}else{$(".p_alexa").hide(500);}'));?>
						</div>
						<div class="copycodetxt p_alexa" <?php echo ($AlexaRankP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox p_alexa" <?php echo ($AlexaRankP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box" id="textarea">&lt;?php echo $StatisticAlexaRank;?&gt;</textarea>
						</div>
					</div>
					<div class="greybox">
						<div class="launchdatebox">Total Members :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
								 if($TotalMember==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('chktotalmember', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"3", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation3">
						<div class="checkbox">
							Total Members Code
							<?php 
								if($TotalMemberP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('p_totalmember', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".p_totalmember").show(500);}else{$(".p_totalmember").hide(500);}'));
							?>
						</div>
						<div class="copycodetxt p_totalmember" <?php echo ($TotalMemberP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox p_totalmember" <?php echo ($TotalMemberP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box" id="textarea">&lt;?php echo $StatisticTotalMember;?&gt;</textarea>
						</div>
					</div>
					
					<div class="darkbluebox">Pay Stats :</div>
					<div class="greybox">
						<div class="launchdatebox">Deposits :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($Deposits==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('chktotaldeposit', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"4", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation4">
						<div class="checkbox">
							Deposits Code
							<?php 
								if($DepositsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('p_totaldeposit', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".p_totaldeposit").show(500);}else{$(".p_totaldeposit").hide(500);}'));?>
						</div>
						<div class="copycodetxt p_totaldeposit" <?php echo ($DepositsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox p_totaldeposit" <?php echo ($DepositsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box" id="textarea">&lt;?php echo $StatisticTotalDiposit;?&gt;</textarea>
						</div>
					</div>
					
					<div class="whitebox">
						<div class="launchdatebox">Payouts :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($Payouts==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('chktotpayout', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"5", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation5">
						<div class="checkbox">
							Payouts Code
							<?php 
								if($PayoutsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('p_totpayout', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".p_totpayout").show(500);}else{$(".p_totpayout").hide(500);}'));?>
						</div>
						<div class="copycodetxt p_totpayout" <?php echo ($PayoutsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox p_totpayout" <?php echo ($PayoutsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box" id="textarea">&lt;?php echo $StatisticTotalPayouts;?&gt;</textarea>
						</div>
					</div>
					
					
					<div class="greybox">
						<div class="launchdatebox">Commissions :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($Commissions==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('chkcommission', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"6", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation6">
						<div class="checkbox">
							Commissions Code
							<?php 
								if($CommissionsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('p_commission', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".p_commission").show(500);}else{$(".p_commission").hide(500);}'));?>
						</div>
						<div class="copycodetxt p_commission" <?php echo ($CommissionsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox p_commission" <?php echo ($CommissionsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box" id="textarea">&lt;?php echo $StatisticTotalCommission;?&gt;</textarea>
						</div>
					</div>
					<div class="whitebox">
						<div class="launchdatebox">Last Payout :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($LastPayout==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('chklastpayout', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"7", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation7">
						<div class="checkbox">
							Last Payout Code
							<?php 
								if($LastPayoutP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('p_lastpayout', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".p_lastpayout").show(500);}else{$(".p_lastpayout").hide(500);}'));?>
						</div>
						<div class="copycodetxt p_lastpayout" <?php echo ($LastPayoutP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox p_lastpayout" <?php echo ($LastPayoutP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box" id="textarea">&lt;?php echo $StatisticTotalLastPayouts;?&gt;</textarea>
						</div>
					</div>
				</div>
				
				<div class="sitesettingmainbox">
					<div class="darkbluebox">Member Stats :</div>
					<div class="greybox">
						<div class="launchdatebox">Online Members :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
								if($OnlineMembers==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('chkonlinemember', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"8", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation8">
						<div class="checkbox">
							Online Members Code
							<?php
								if($OnlineMembersP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('p_onlinemember', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".p_onlinemember").show(500);}else{$(".p_onlinemember").hide(500);}'));?>
						</div>
						<div class="copycodetxt p_onlinemember" <?php echo ($OnlineMembersP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox p_onlinemember" <?php echo ($OnlineMembersP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box" id="textarea">&lt;div id="OnlineMembers"&gt;&lt;?php echo $OnlineMembers;?&gt;&lt;/div&gt;</textarea>
						</div>
						<div class="copycodetxt p_onlinemember" <?php echo ($OnlineMembersP != 1)? 'style="display:none;"' : ''; ?>>This Code Can Be Used <b>Only Once Per Page</b></div>
					</div>
					
					<div class="whitebox">
						<div class="launchdatebox">Online Guests :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($OnlineGuests==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('chkonlineguest', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"9", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation9">
						<div class="checkbox">
							Online Guests Code
							<?php 
								if($OnlineGuestsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('p_onlineguest', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".p_onlineguest").show(500);}else{$(".p_onlineguest").hide(500);}'));?>
						</div>
						<div class="copycodetxt p_onlineguest" <?php echo ($OnlineGuestsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox p_onlineguest" <?php echo ($OnlineGuestsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box" id="textarea">&lt;div id="OnlineGuests"&gt;&lt;?php echo $OnlineGuests;?&gt;&lt;/div&gt;</textarea>
						</div>
						<div class="copycodetxt p_onlineguest" <?php echo ($OnlineGuestsP != 1)? 'style="display:none;"' : ''; ?>>This Code Can Be Used <b>Only Once Per Page</b></div>
					</div>
					<div class="bluebox">Recently Joined :</div>
					<div class="greybox">
						<div class="launchdatebox">Recently Joined :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($RecentlyJoined==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('chkrecjoined', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"10", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation10">
						<div class="checkbox">
							Recently Joined Code
							<?php 
								if($RecentlyJoinedP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('p_recjoined', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".p_recjoined").show(500);}else{$(".p_recjoined").hide(500);}'));?>
						</div>
						<div class="copycodetxt p_recjoined" <?php echo ($RecentlyJoinedP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox p_recjoined" <?php echo ($RecentlyJoinedP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box" id="textarea">&lt;?php foreach($StatisticRecentlyJoined as $key=>$value){ echo $value."-".$key; } ?&gt;</textarea>
						</div>
					</div>
					<div class="darkbluebox">Top Commission Earners :</div>
					<div class="greybox">
						<div class="launchdatebox">Top Commission Earners :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									 if($TopCommissionEarners==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('chktopcommisionearner', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"11", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation11">
						<div class="checkbox">
							Top Commission Earners Code
							<?php 
								if($TopCommissionEarnersP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('p_topcommisionearner', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".p_topcommisionearner").show(500);}else{$(".p_topcommisionearner").hide(500);}'));?>
						</div>
						<div class="copycodetxt p_topcommisionearner" <?php echo ($TopCommissionEarnersP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox p_topcommisionearner" <?php echo ($TopCommissionEarnersP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box" id="textarea">&lt;?php foreach($StatisticTopCommissionEarners as $earners){ echo $earners['Member']['user_name']."-".$earners[0]['earning']; } ?&gt;</textarea>
						</div>
					</div>
					<?php /* Ads Stats Code Remove
					<div class="bluebox">Ads Stats :</div>
					<div class="greybox">
						<div class="launchdatebox">Text Ads :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($TextAds==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('chktext', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"12", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation12">
						<div class="checkbox">
							Text Ads Code
							<?php 
								if($TextAdsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('p_textads', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".p_textads").show(500);}else{$(".p_textads").hide(500);}'));?>
						</div>
						<div class="copycodetxt p_textads" <?php echo ($TextAdsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox p_textads" <?php echo ($TextAdsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box" id="textarea">&lt;?php echo $StatisticTotalTextAds;?&gt;</textarea>
						</div>
					</div>
					
					<div class="whitebox">
						<div class="launchdatebox">Banner Ads :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									 if($BannerAds==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('chkbannerads', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"13", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation13">
						<div class="checkbox">
							Banner Ads Code
							<?php 
								if($BannerAdsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('p_bannerads', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".p_bannerads").show(500);}else{$(".p_bannerads").hide(500);}'));?>
						</div>
						<div class="copycodetxt p_bannerads" <?php echo ($BannerAdsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox p_bannerads" <?php echo ($BannerAdsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box" id="textarea">&lt;?php echo $StatisticTotalBannerAds;?&gt;</textarea>
						</div>
					</div>
					*/ ?>
				</div>
			</div>
			<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_publicstatisticsupdate',$SubadminAccessArray)){ ?>
                    <div class="formbutton">
                        <?php echo $this->Js->submit('Update', array(
                          'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                          'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                          'update'=>'#UpdateMessage',
                          'class'=>'btnorange',
                          'controller'=>'Sitesetting',
                          'action'=>'publicstatisticsupdate',
                          'url'   => array('controller' => 'sitesetting', 'action' => 'publicstatisticsupdate')
                        ));?>
                    </div>
            <?php } ?>
			</div>
        <?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>