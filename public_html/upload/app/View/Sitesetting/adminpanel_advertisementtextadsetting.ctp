<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 12-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Advertisement</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Banner Ads", array('controller'=>'sitesetting', "action"=>"advertisementcredit"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Text Ads", array('controller'=>'sitesetting', "action"=>"advertisementtextadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC Text Ads", array('controller'=>'sitesetting', "action"=>"ppctextadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Solo Ads", array('controller'=>'sitesetting', "action"=>"soload"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Login Ads", array('controller'=>'sitesetting', "action"=>"loginadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC", array('controller'=>'sitesetting', "action"=>"ppcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC", array('controller'=>'sitesetting', "action"=>"ptcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Biz Directory", array('controller'=>'sitesetting', "action"=>"bizdirectorysetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Traffic Exchange", array('controller'=>'sitesetting', "action"=>"trafficsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Surfing Ads Settings", array('controller'=>'sitesetting', "action"=>"surfingsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Widgets", array('controller'=>'sitesetting', "action"=>"advertisementbannerwidget"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<div class="tab-innar">
	<ul>
		<li>
			<?php echo $this->Js->link("Common Settings", array('controller'=>'sitesetting', "action"=>"advertisementtextadsetting"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>'active'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Credit Text Ads Settings", array('controller'=>'sitesetting', "action"=>"advertisementtextadcredit"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Plan Text Ads Settings", array('controller'=>'sitesetting', "action"=>"textadplan"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				
			));?>
		</li>
	</ul>
</div>
<?php if($IsAdminAccess){?>
<script type="text/javascript">$(function() {$('.colorpicker').wheelColorPicker({ sliders: "whsvp", preview: true, format: "css" });});</script>

<div class="helpicon"><a href="https://www.proxscripts.com/docs/Advertisement_Settings#Text_Ads" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>
	  
	  <?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'advertisementtextadsettingupdate')));?>
	  <?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
				<div class="frommain">
				  <div class="fromnewtext">Maximum Characters Allowed For Title :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('max_title', array('type'=>'text', 'value'=>$SITECONFIG["max_title"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>

				  <div class="fromnewtext">Maximum Characters Allowed For Description Line 1 :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('max_desc1', array('type'=>'text', 'value'=>$SITECONFIG["max_desc1"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>

				  <div class="fromnewtext">Maximum Characters Allowed For Description Line 2 :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('max_desc2', array('type'=>'text', 'value'=>$SITECONFIG["max_desc2"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>

				  <div class="fromnewtext">Title Color : </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('title_color', array('type'=>'text', 'value'=>$SITECONFIG["title_color"], 'label' => false, 'div' => false, 'class'=>'fromboxbg colorpicker'));?>
				  </div>
			
				  <div class="fromnewtext">Description Line 1 Color : </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('desc1_color', array('type'=>'text', 'value'=>$SITECONFIG["desc1_color"], 'label' => false, 'div' => false, 'class'=>'fromboxbg colorpicker'));?>
				  </div>
			
				  <div class="fromnewtext">Description Line 2 Color : </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('desc2_color', array('type'=>'text', 'value'=>$SITECONFIG["desc2_color"], 'label' => false, 'div' => false, 'class'=>'fromboxbg colorpicker'));?>
				  </div>

				  <div class="fromnewtext">URL Color : </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('url_color', array('type'=>'text', 'value'=>$SITECONFIG["url_color"], 'label' => false, 'div' => false, 'class'=>'fromboxbg colorpicker'));?>
				  </div>
				  <div class="formbutton">
				  <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_advertisementtextadsettingupdate',$SubadminAccessArray)){ ?>
						<?php echo $this->Js->submit('Update', array(
						  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage',
						  'class'=>'btnorange',
						  'div'=>false,
						  'controller'=>'sitesetting',
						  'action'=>'advertisementtextadsettingupdate',
						  'url'   => array('controller' => 'sitesetting', 'action' => 'advertisementtextadsettingupdate')
						));?>
				  <?php } ?>
				  </div>
				</div>
			
	  <?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>