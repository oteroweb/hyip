<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 12-12-2014
  * Last Modified: 12-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Advertisement</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Banner Ads", array('controller'=>'sitesetting', "action"=>"advertisementcredit"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Text Ads", array('controller'=>'sitesetting', "action"=>"advertisementtextadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC Text Ads", array('controller'=>'sitesetting', "action"=>"ppctextadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Solo Ads", array('controller'=>'sitesetting', "action"=>"soload"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Login Ads", array('controller'=>'sitesetting', "action"=>"loginadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC", array('controller'=>'sitesetting', "action"=>"ppcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC", array('controller'=>'sitesetting', "action"=>"ptcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Biz Directory", array('controller'=>'sitesetting', "action"=>"bizdirectorysetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Traffic Exchange", array('controller'=>'sitesetting', "action"=>"trafficsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Surfing Ads Settings", array('controller'=>'sitesetting', "action"=>"surfingsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Widgets", array('controller'=>'sitesetting', "action"=>"advertisementbannerwidget"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<div class="tab-innar">
	<div id="tab-innar">  
	  <ul>
		  <li><a class="active" id="manuDirectory" href="#" onclick="$('.BizDirectory').show(500);$('.SurfFreePlan,.EnableSurfing').hide();">Biz Directory Settings</a></li>
		  <li><a href="#" id="manuSurfFreePlan" onclick="$('.SurfFreePlan').show(500);$('.BizDirectory,.EnableSurfing').hide();">Surf Free Plan Settings</a></li>
		  <li><a href="#" id="manuEnableSurfing" onclick="$('.EnableSurfing').show(500);$('.BizDirectory,.SurfFreePlan').hide();">Surfing Ads Settings</a></li>
	  </ul>
	</div>
</div>
<?php if($IsAdminAccess){?>
<div id="UpdateMessage" class="UpdateMessage"></div>
<div class="backgroundwhite">
    
<?php echo $this->Form->create('Sitesetting',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'bizdirectorysettingaction')));?>
		
			<div class="frommain">
				  <div class="BizDirectory">
						<div class="helpicon"><a href="https://www.proxscripts.com/docs/Advertisement_Settings#Biz_Directory" target="_blank">Help</a></div>
						<div class="fromnewtext">Enable Biz Directory :  </div>
						<div class="fromborderdropedown3">
							<div class="select-main">
								<label>
								<?php 
									echo $this->Form->input('enablesurfing', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $enablesurfing,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => '',
										'onchange'=>'if($(this).val()==1){$(".enadis").show(500);}else{$(".enadis").hide(500);}'
									));
								?>
								</label>
							</div>
						</div>
						
						<div class="enadis" style="<?php if($enablesurfing==0){ echo 'display:none;';} ?>">
						<div class="fromnewtext">Auto Approval : </div>
						<div class="fromborderdropedown3">
							<div class="select-main">
								<label>
								<?php 
									  echo $this->Form->input('isapprove', array(
										  'type' => 'select',
										  'options' => array('0'=>'Yes', '1'=>'No'),
										  'selected' => $isapprove,
										  'class'=>'',
										  'label' => false,
										  'div' => false,
										  'style' => ''
									  ));
								?>
								</label>
							</div>
						</div>
						
						
						<div class="fromnewtext ">Auto Approval on Edit :  </div>
						<div class="fromborderdropedown3 ">
							<div class="select-main">
								<label>
									<?php 
									  echo $this->Form->input('iseditapprove', array(
										  'type' => 'select',
										  'options' => array('0'=>'Yes', '1'=>'No'),
										  'selected' => $iseditapprove,
										  'class'=>'',
										  'label' => false,
										  'div' => false,
										  'style' => ''
									  ));
									?>
								</label>
							</div>
						</div>
						
						</div>
						<!--<div class="formbutton">
							  <input type="button" value="Next >" onclick="$('#manuDirectory, #manuSurfFreePlan, #manuEnableSurfing').removeClass('active');$('#manuSurfFreePlan').addClass('active'); $('.SurfFreePlan').show(500);$('.BizDirectory,.EnableSurfing').hide();" class="btngray"/>
						</div>-->
				  </div>
				  
				  <div class="SurfFreePlan">
						<div class="helpicon"><a href="https://www.proxscripts.com/docs/Advertisement_Settings#Surf_Free_Plan_Settings" target="_blank">Help</a></div>
						<div class="fromnewtext ">Enable Surf Free Plan :  </div>
						<div class="fromborderdropedown3 ">
							<div class="select-main">
								<label>
								<?php 
									  echo $this->Form->input('enablesurffree', array(
										  'type' => 'select',
										  'options' => array('1'=>'Yes', '0'=>'No'),
										  'selected' => $enablesurffree,
										  'class'=>'',
										  'label' => false,
										  'div' => false,
										  'style' => '',
									  ));
								?>
								</label>
							</div>
						</div>
						
						
						<!--<div class="formbutton">
							  <input type="button" onclick="$('#manuDirectory, #manuSurfFreePlan, #manuEnableSurfing').removeClass('active');$('#manuDirectory').addClass('active'); $('.BizDirectory').show(500);$('.SurfFreePlan,.EnableSurfing').hide();" value="< Back" class="btngray"/>
							  <input type="button" onclick="$('#manuDirectory, #manuSurfFreePlan, #manuEnableSurfing').removeClass('active');$('#manuEnableSurfing').addClass('active'); $('.EnableSurfing').show(500);$('.BizDirectory,.SurfFreePlan').hide();" value="Next >" class="btngray"/>
						</div>-->  
				  </div>
				  
				  <div class="EnableSurfing">
						<div class="helpicon"><a href="https://www.proxscripts.com/docs/Advertisement_Settings#Surfing_Ads_Settings" target="_blank">Help</a></div>
						<div class="fromnewtext ">Enable Surfing Ads :  </div>
						<div class="fromborderdropedown3 ">
							<div class="select-main">
								<label>
								<?php 
									echo $this->Form->input('enablesurfingads', array(
										'type' => 'select',
										'options' => array('1'=>'Yes', '0'=>'No'),
										'selected' => $enablesurfingads,
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => '',
										 'onchange'=>'if($(this).val()==1){$(".enadis1").show(500);}else{$(".enadis1").hide(500);}' 
									));
								?>
								</label>
							</div>
						</div>
						
						<div class="enadis1" style="<?php if($enablesurfingads==0){ echo 'display:none;';} ?>">
						<div class="fromnewtext ">Seconds To Wait For Successful Click Count : <span class="red-color">*</span></div>
						<div class="fromborderdropedown3 ">
							<?php echo $this->Form->input('countersecond', array('type'=>'text', 'id'=>'startdate', 'value'=>$countersecond, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
						</div>
						
						
						<div class="fromnewtext ">Seconds Countdown Starting Time : </div>
						<div class="fromborderdropedown3 ">
							<div class="select-main">
								<label>
								<?php 
									  echo $this->Form->input('counterstarton', array(
										  'type' => 'select',
										  'options' => array('0'=>'On Page Load', '1'=>'On Site Load'),
										  'selected' => $counterstarton,
										  'class'=>'',
										  'label' => false,
										  'div' => false,
										  'style' => '',
										  'onchange' => 'if(this.selectedIndex==1){$(".c1").show(500);}else{$(".c1").hide(500);}'
									  ));
								?>
								</label>
							</div>
						</div>
						
						
						<div <?php echo ($counterstarton==0)?'style="display: none"':"" ?> class="c1 ">
							<div class="fromnewtext">Max. Seconds to Wait For Site Load : <span class="red-color">*</span></div>
							<div class="fromborderdropedown3">
								<?php echo $this->Form->input('sitelodecountersecond', array('type'=>'text', 'id'=>'startdate', 'value'=>$sitelodecountersecond, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
							</div>
							
						</div>
					</div>
				  </div>
						<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_bizdirectorysettingaction', $SubadminAccessArray)){ ?>
							<div class="formbutton">
								<!--<input type="button" onclick="$('#manuDirectory, #manuSurfFreePlan, #manuEnableSurfing').removeClass('active');$('#manuSurfFreePlan').addClass('active'); $('.SurfFreePlan').show(500);$('.BizDirectory,.EnableSurfing').hide();" value="< Back" class="btngray"/> -->
								<?php echo $this->Js->submit('Submit', array(
									'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'update'=>'#UpdateMessage',
									'class'=>'btnorange',
									'div'=>false,
									'controller'=>'sitesetting',
									'action'=>'bizdirectorysettingaction',
									'url'   => array('controller' => 'sitesetting', 'action' => 'bizdirectorysettingaction')
								));?>
							</div>
						<?php } ?>
					
			</div>
		
<?php echo $this->Form->end();?>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>