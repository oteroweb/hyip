<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Referral Settings</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Referral Settings", array('controller'=>'sitesetting', "action"=>"referral"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'class'=>'satting-menu-active'
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Add. Referral Commission Settings", array('controller'=>'sitesetting', "action"=>"referralcommission"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>

<div class="helpicon"><a href="https://www.proxscripts.com/docs/Referral_Settings#Referral_Settings" target="_blank">Help</a></div>

    <div id="UpdateMessage"></div>
	    <?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'referralupdate')));?>
        <?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
			<div class="frommain">
				  <div class="fromnewtext">Free Member Can Get Commission on Referral Purchases :</div>
				  <div class="fromborderdropedown3">
						<div class="select-main">
							<label>
							<?php 
								echo $this->Form->input('freemembercommision', array(
									'type' => 'select',
									'options' => array('1'=>'Yes', '0'=>'No'),
									'selected' => $SITECONFIG["freemembercommision"],
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => ''
								));
							?>
							</label>
						</div>
				  </div>
				  
				  
				  <div class="fromnewtext">Free Member Can Get Commission on Member Fees :</div>
				  <div class="fromborderdropedown3">
						<div class="select-main">
							<label>
							<?php 
							echo $this->Form->input('freemembershipcommission', array(
								'type' => 'select',
								'options' => array('1'=>'Yes', '0'=>'No'),
								'selected' => $SITECONFIG["freemembershipcommission"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));
							?>
							</label>
						</div>
				  </div>	
				  

				<div class="fromnewtext">Minimum Cash Balance Required To Earn Refferal Commission ($) : </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('refferal_commission_required_cash', array('type'=>'text', 'value'=>$SITECONFIG["refferal_commission_required_cash"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
				
				
				<div class="fromnewtext">Referral Tracking Policy : </div>
				
				<div style="height: 40px" class="fromborderdropedown3 referralpolicy">
				  <div class="fromborderdropedown5">
					  <div class="select-main">
						  <label>
						  <?php 
							echo $this->Form->input('refferal_tracking', array(
								'type' => 'select',
								'options' => array('0'=>'Session', '1'=>'Cookie'),
								'selected' => $SITECONFIG["refferal_tracking"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => '',
								'onchange'=>'if(this.selectedIndex==1){$(".coockiedays").show(500);}else{$(".coockiedays").hide(500);}'
							));
						  ?>
						  </label>
					  </div>
				  </div>
				  <div class="fromborderdropedown5 coockiedays" <?php echo ($SITECONFIG["refferal_tracking"]) ? "" : "style='display:none'"?>>
						  <div class="daystexthaf">Days :</div>
						  <?php echo $this->Form->input('refferal_cookie_days', array('type'=>'text', 'value'=>$SITECONFIG["refferal_cookie_days"], 'label' => false, 'div' => false, 'class'=>'fromboxbghalf borderwidth', 'style'=>''));?>
				  </div>
				</div>
				
				<div class="fromnewtext">Provide Referral Commission For Purchases Using Balance  : </div>
				  <div class="fromborderdropedown3" style="padding: 4px;">
						<?php echo $this->Html->image('check.jpg', array('alt' => 'Cash'));?>&nbsp;<span class="red-color">Cash </span> &nbsp;
					<?php
					$pmethod=array('repurchase'=>'Re-purchase', 'earning'=>'Earning', 'commission'=>'Commission');
					if($SITECONFIG["wallet_for_commission"] == 'cash' && $SITECONFIG["wallet_for_earning"] == 'cash')
					{
						$pmethod=array('repurchase'=>'Re-purchase');
					}
					elseif($SITECONFIG["wallet_for_earning"] == 'cash')
					{
						$pmethod=array('repurchase'=>'Re-purchase', 'commission'=>'Commission');
					}
					elseif($SITECONFIG["wallet_for_commission"] == 'cash')
					{
						$pmethod=array('repurchase'=>'Re-purchase', 'earning'=>'Earning');
					}
						  
					$selected = @explode(",",$SITECONFIG["allowcommissionfor"]);
					echo $this->Form->input('allowcommissionfor', array('type' => 'select', 'div'=>false, 'label'=>false, 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $pmethod));?>
				  </div>
				 
				  <div class="fromnewtext">Additional Referral Commission Policy :</div>
				  <div class="fromborderdropedown3">
					<div class="select-main">
						<label>
					  <?php 
						  echo $this->Form->input('add_referral_commission_policy', array(
								'type' => 'select',
								'options' => array(0 =>'No Additional Referral Commission', 1 =>'Based on Number of Referrals', 2 =>'Based on Membership Levels'),
								'selected' => $SITECONFIG["add_referral_commission_policy"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style'=>''
						  ));
					  ?>
						</label>
					</div>
				  </div>
				  
				  
				  <div class="fromnewtext">Customize your Referral Link :</div>
				  <div class="fromborderdropedown3">
					<div class="select-main">
						<label>
					  <?php 
						  echo $this->Form->input('reflinkiduser', array(
								'type' => 'select',
								'options' => array(0 =>'User Id', 1 =>'User Name'),
								'selected' => $SITECONFIG["reflinkiduser"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style'=>''
						  ));
					  ?>
						</label>
					</div>
				  </div>
				  
			  <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_referralupdate',$SubadminAccessArray)){ ?>
					<div class="formbutton">
                        <?php echo $this->Js->submit('Update', array(
                          'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                          'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                          'update'=>'#UpdateMessage',
                          'class'=>'btnorange',
                          'controller'=>'Sitesetting',
						  'div'=>false,
                          'action'=>'referralupdate',
                          'url'   => array('controller' => 'sitesetting', 'action' => 'referralupdate')
                        ));?>
				  </div>
              <?php } ?>
			</div>
		<?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>