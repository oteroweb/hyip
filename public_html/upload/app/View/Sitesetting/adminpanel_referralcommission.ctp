<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Referral Settings</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Referral Settings", array('controller'=>'sitesetting', "action"=>"referral"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'class'=>'satting-menu-active'
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Add. Referral Commission Settings", array('controller'=>'sitesetting', "action"=>"referralcommission"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>


<div class="helpicon"><a href="https://www.proxscripts.com/docs/Referral_Settings#Add._Referral_Commission_Settings" target="_blank">Help</a></div>

<div id="UpdateMessage"></div>

<?php if($SITECONFIG["add_referral_commission_policy"]==1){ ?>
	<div class="height7"></div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#settingpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'sitesetting', 'action'=>'referralcommission')
	));
	?>
<div id="Xgride-bg" class="greenbottomborder">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
    <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_referralcommissionplanadd',$SubadminAccessArray)){
	if($SITECONFIG["add_referral_commission_policy"]==1){?>
		<?php echo $this->Js->link("+ Add New", array('controller'=>'sitesetting', "action"=>"referralcommissionplanadd"), array(
			'update'=>'#settingpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btnorange'
		));?>
    <?php } } ?>
	</div>
	<div class="clearboth"></div>
		<div class="tablegrid">
			<div class="tablegridheader">
				<div>Id</div>
				<?php if($SITECONFIG["add_referral_commission_policy"]==1){ ?>
					<div>From</div>
					<div>To</div>
				<?php }elseif($SITECONFIG["add_referral_commission_policy"]==2){ ?>
					<div>Membership Level Name</div>
					<div>Membership Level Price</div>
					<div>Membership Level Type</div>
					<div>Display Order</div>
				<?php } ?>
				<div>Add. Comm. On Position (%)</div>
				<div>Add. Comm. On Membership (%)</div>
			   <?php if($SITECONFIG["add_referral_commission_policy"]==1){ ?>
				<div>Action</div>
			<?php } ?>
			</div>
			<?php foreach ($referralcommissionplans as $referralcommissionplan): ?>
				<div class="tablegridrow">
					<div><?php echo $referralcommissionplan[$loadmodel]['id'];?></div>
                    <?php if($SITECONFIG["add_referral_commission_policy"]==1){ ?>
                        <div><?php echo $referralcommissionplan[$loadmodel]['fromreferral'];?></div>
                        <div><?php echo $referralcommissionplan[$loadmodel]['toreferral'];?></div>
                    <?php }elseif($SITECONFIG["add_referral_commission_policy"]==2){ ?>
                    	<div><?php echo $referralcommissionplan[$loadmodel]['membership_name'];?></div>
                        <div>$<?php echo $referralcommissionplan[$loadmodel]['price'];?></div>
                        <div>
							<?php echo $referralcommissionplan[$loadmodel]['membership_fee_type'];
							if($referralcommissionplan[$loadmodel]['membership_fee_type']=='Days')
								echo '('.$referralcommissionplan[$loadmodel]['membership_fee_value'].')';
							?>
                        </div>
                        <div><?php echo $referralcommissionplan[$loadmodel]['upgrade_order'];?></div>
                    <?php } ?>
					<div>
					<?php
					$commission=@explode(',0',$referralcommissionplan[$loadmodel]['commission']);
					echo trim($commission[0],',');
					?>
					</div>
                    <div>
					<?php
					$commission=@explode(',0',$referralcommissionplan[$loadmodel]['add_commission_on_fees']);
					echo trim($commission[0],',');
					?>
					</div>
				<?php if($SITECONFIG["add_referral_commission_policy"]==1){ ?>
					<div class="textcenter">
					<div class="actionmenu">
						<div class="btn-group">
							<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
                          	
                            <li>
							<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_referralcommissionplanadd/$',$SubadminAccessArray)){ ?>
								<?php 
								if($SITECONFIG["add_referral_commission_policy"]==1)
									$edittext='Edit Criteria';
								elseif($SITECONFIG["add_referral_commission_policy"]==2)
									$edittext='Edit Membership';
								echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>$edittext)).' '.$edittext, array('controller'=>'sitesetting', "action"=>"referralcommissionplanadd/".$referralcommissionplan[$loadmodel]['id']), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));?>
							<?php }
								else
								{
									echo '&nbsp;';
								} ?>
                            </li>
							
							<li>
							<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_referralcommissionplanstatus',$SubadminAccessArray)){ ?>
								<?php 
								if($referralcommissionplan[$loadmodel]['status']==0){
									$statusaction='1';
									$statusicon='red-icon.png';
									if($SITECONFIG["add_referral_commission_policy"]==1)
										$statustext='Activate Criteria';
									elseif($SITECONFIG["add_referral_commission_policy"]==2)
										$statustext='Activate Membership';
								}else{
									$statusaction='0';
									$statusicon='blue-icon.png';
									if($SITECONFIG["add_referral_commission_policy"]==1)
										$statustext='Inactivate Criteria';
									elseif($SITECONFIG["add_referral_commission_policy"]==2)
										$statustext='Inactivate Membership';
								}
								echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'sitesetting', "action"=>"referralcommissionplanstatus/".$statusaction."/".$referralcommissionplan[$loadmodel]['id']."/".$this->params['paging'][$loadmodel]['page']), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));?>
                            <?php }
								else
								{
									echo '&nbsp;';
								} ?>
                            </li>
							
                            <li>
							<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_referralcommissionplanremove',$SubadminAccessArray)){ ?>
								<?php 
								if($SITECONFIG["add_referral_commission_policy"]==1)
									$deletetext='Delete Criteria';
								elseif($SITECONFIG["add_referral_commission_policy"]==2)
									$deletetext='Delete Membership';
								echo $this->Js->link($this->html->image('delete.png', array('alt'=>$deletetext)).' '.$deletetext, array('controller'=>'sitesetting', "action"=>"referralcommissionplanremove/".$referralcommissionplan[$loadmodel]['id']."/".$this->params['paging'][$loadmodel]['page']), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'confirm'=>'Are You Sure?'
								));?>
                            <?php }
									else
									{
										echo '&nbsp;';
									} ?>
                             </li>
						  </ul>
						</div>
					</div>
				</div>
				<?php } ?>
					<td>&nbsp;</td>
			</div>
			<?php endforeach; ?>
		    </div>
		<?php if(count($referralcommissionplans)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	<?php 
	if($this->params['paging'][$loadmodel]['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'referralcommission/rpp')));?>
		<div class="resultperpage">
                        <label>
			      <?php 
			      echo $this->Form->input('resultperpage', array(
				'type' => 'select',
				'options' => $resultperpage,
				'selected' => $this->Session->read('pagerecord'),
				'class'=>'',
				'label' => false,
				'div'=>false,
				'style' => '',
				'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			      ));
			      ?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#settingpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'Sitesetting',
			  'action'=>'referralcommission/rpp',
			  'url'   => array('controller' => 'sitesetting', 'action' => 'referralcommission/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '...', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	</div>
</div>
<?php } ?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
	<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>