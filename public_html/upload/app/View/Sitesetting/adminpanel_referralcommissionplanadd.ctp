<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Referral Settings</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Referral Settings", array('controller'=>'sitesetting', "action"=>"referral"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'class'=>'satting-menu-active'
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Add. Referral Commission Settings", array('controller'=>'sitesetting', "action"=>"referralcommission"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
	
	<div class="height10"></div>
	
	<div class="helpicon"><a href="https://www.proxscripts.com/docs/Referral_Settings#Add._Referral_Commission_Settings" target="_blank">Help</a></div>
	
	<div id="UpdateMessage"></div>
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'referralcommissionplanaddaction')));?>
		
		<?php if(isset($referralcommissionplandata[$loadmodel]["id"])){
			echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$referralcommissionplandata[$loadmodel]["id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}?>
			
			<div class="frommain">
				<div class="fromnewtext">Status :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
						  echo $this->Form->input('status', array(
							  'type' => 'select',
							  'options' => array('1'=>'Active', '0'=>'Inactive'),
							  'selected' => $referralcommissionplandata[$loadmodel]["status"],
							  'class'=>'',
							  'label' => false,
							  'div' => false,
							  'style' => ''
						  ));
							?>
						</label>
					</div>
				</div>

				<div class="fromnewtext">From : <span class="red-color">*</span> </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('fromreferral', array('type'=>'text', 'value'=>$referralcommissionplandata[$loadmodel]["fromreferral"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
				
				<div class="fromnewtext">To : <span class="red-color">*</span> </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('toreferral', array('type'=>'text', 'value'=>$referralcommissionplandata[$loadmodel]["toreferral"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
			
				<?php $addcommissionlevel=@explode(',',$referralcommissionplandata[$loadmodel]["commission"]); ?>
				<div class="fromnewtext">Additional Commission on Purchases (%) :<span class="red-color">*</span> </div>
				<div class="fromborderdropedown3">
					<div class="fromborder4left">
						<div class="levalwhitfromtext">Level 1 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 2 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 3 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 4 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 5 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 6 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 7 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[6], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 8 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[7], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 9 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[8], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 10 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[9], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
					</div>
					<div class="tooltipfronright">
				   </div>
				   <div class="clearboth"></div>
				</div>
				
				
				<?php $add_commission_on_fees=@explode(',',$referralcommissionplandata[$loadmodel]["add_commission_on_fees"]); ?>
				<div class="fromnewtext">Additional Commission on Membership Fees (%):<span class="red-color">*</span> </div>
				<div class="fromborderdropedown3">
					<div class="fromborder4left">
						<div class="levalwhitfromtext">Level 1 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissiononfees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'style'=>'width:57px;'));?></div>
						<div class="levalwhitfromtext">Level 2 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissiononfees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'style'=>'width:57px;'));?></div>
						<div class="levalwhitfromtext">Level 3 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissiononfees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'style'=>'width:57px;'));?></div>
						<div class="levalwhitfromtext">Level 4 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissiononfees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'style'=>'width:57px;'));?></div>
						<div class="levalwhitfromtext">Level 5 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissiononfees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'style'=>'width:57px;'));?></div>
						<div class="levalwhitfromtext">Level 6 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissiononfees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'style'=>'width:57px;'));?></div>
						<div class="levalwhitfromtext">Level 7 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissiononfees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[6], 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'style'=>'width:57px;'));?></div>
						<div class="levalwhitfromtext">Level 8 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissiononfees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[7], 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'style'=>'width:57px;'));?></div>
						<div class="levalwhitfromtext">Level 9 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissiononfees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[8], 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'style'=>'width:57px;'));?></div>
						<div class="levalwhitfromtext">Level 10 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissiononfees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[9], 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'style'=>'width:57px;'));?></div>
					</div>
					<div class="tooltipfronright">
				   </div>
				   <div class="clearboth"></div>
				</div>
				<div class="formbutton">
						<?php echo $this->Js->submit('Submit', array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage',
					  'class'=>'btnorange',
					  'div'=>false,
					  'controller'=>'sitesetting',
					  'action'=>'referralcommissionplanaddaction',
					  'url'   => array('controller' => 'sitesetting', 'action' => 'referralcommissionplanaddaction')
					));?>
					<?php echo $this->Js->link("Back", array('controller'=>'sitesetting', "action"=>"referralcommission"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'div'=>false,
						'class'=>'btngray'
					));?>
				</div>
		
			</div>
		<?php echo $this->Form->end();?>
	<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>