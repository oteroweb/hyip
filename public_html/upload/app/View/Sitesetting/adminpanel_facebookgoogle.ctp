<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 10-01-2015
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Signup Settings</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Registration Form Settings", array('controller'=>'sitesetting', "action"=>"registrationform"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Profile Form Settings", array('controller'=>'sitesetting', "action"=>"profileform"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Free Credit & Bonus", array('controller'=>'sitesetting', "action"=>"freecreditandbonus"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Signup Settings", array('controller'=>'sitesetting', "action"=>"signup"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Facebook & Google Settings", array('controller'=>'sitesetting', "action"=>"facebookgoogle"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Signup_Settings#Facebook_.26_Google_Settings" target="_blank">Help</a></div>
    <div id="UpdateMessage"></div>
	     <?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'facebookgoogleupdate')));?>
        <?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
			<div class="frommain">
				  
				  <div class="fromnewtext">Enable Login With Facebook :  </div>
				  <div class="fromborderdropedown3">
					  <div class="select-main">
						  <label>
						  <?php
						  if($SITECONFIG["chkfb"]){$style="";}else{$style="style='display:none'";}
							echo $this->Form->input('chkfb', array(
								'type' => 'select',
								'options' => array('1'=>'Enable', '0'=>'Disable'),
								'selected' => $SITECONFIG["chkfb"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => '',
								'onchange'=>'if($(this).val()==1){$(".facebookfields").show(500);}else{$(".facebookfields").hide(500);}'
							)); ?>
						  </label>
					  </div>
				  </div>
			
			<div class="facebookfields" <?php echo $style;?>>
				<div class="fromnewtext">Facebook App Id : </div>
				<div class="fromborderdropedown3">
				  <?php echo $this->Form->input('fbapi_id', array('type'=>'text', 'value'=>$SITECONFIG["fbapi_id"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>	
				</div>
			</div>
			<div class="nameandbox facebookfields" <?php echo $style;?>>
				<div class="fromnewtext">Facebook App Secret : </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('fbapi_sec', array('type'=>'text', 'value'=>$SITECONFIG["fbapi_sec"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
			</div>
			
			<div class="fromnewtext">Enable Login With Google :</div>
			<div class="fromborderdropedown3">
				<div class="select-main">
					<label>
						<?php
						if($SITECONFIG["chkgoogle"]){$stylegoogle="";}else{$stylegoogle="style='display:none'";}
						echo $this->Form->input('chkgoogle', array(
							'type' => 'select',
							'options' => array('1'=>'Enable', '0'=>'Disable'),
							'selected' => $SITECONFIG["chkgoogle"],
							'class'=>'',
							'label' => false,
							'style' => '',
							'onchange'=>'if($(this).val()==1){$(".googlefields").show(500);}else{$(".googlefields").hide(500);}'
						)); ?>
					</label>
				</div>
			</div>
			
			<div class="googlefields" <?php echo $stylegoogle; ?>>
				<div class="fromnewtext">Google Client ID : </div>
				<div class="fromborderdropedown3">
				  <?php echo $this->Form->input('googleclientid', array('type'=>'text', 'value'=>$SITECONFIG["googleclientid"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>	
				</div>
			</div>
			<div class="nameandbox googlefields" <?php echo $stylegoogle;?>>
				<div class="fromnewtext">Google Client Secret : </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('googleclientsec', array('type'=>'text', 'value'=>$SITECONFIG["googleclientsec"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
			</div>
			<div class="nameandbox googlefields" <?php echo $stylegoogle;?>>
				<div class="fromnewtext">Google Redirect URIS : </div>
				<div class="fromborderdropedown3 extralineheight">
					<?php echo $SITEURL.'login/with/google'; ?>
				</div>
			</div>
			<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_facebookgoogleupdate',$SubadminAccessArray)){ ?>
						<div class="formbutton">
                        <?php echo $this->Js->submit('Update', array(
                          'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                          'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                          'update'=>'#UpdateMessage',
                          'class'=>'btnorange',
						  'div'=>false,
                          'controller'=>'Sitesetting',
                          'action'=>'facebookgoogleupdate',
                          'url'   => array('controller' => 'sitesetting', 'action' => 'facebookgoogleupdate')
                        ));?>
						</div>
              <?php } ?>
			</div>
        <?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>