<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Membership Settings</div>
<div id="settingpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Membership_Settings" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

   
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#settingpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'sitesetting', 'action'=>'membership')
	));
	$currentpagenumber=$this->params['paging']['Membership']['page'];
	?>
	
<div id="Xgride-bg">
	<div class="height10"></div>
    <div class="padding10 backgroundwhite">
		<div class="greenbottomborder">
			<div class="height10"></div>
			<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
			<div class="addnew-button">	
			<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_membershipplanadd',$SubadminAccessArray)){ ?>
					<?php echo $this->Js->link("+ Add New", array('controller'=>'sitesetting', "action"=>"membershipplanadd"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'btnorange'
					));?>
			<?php } ?>
			</div>
			<div class="clear-both"></div>
			
				<div class="tablegrid">
						<div class="tablegridheader">
						  <div>Id</div>
						  <div>Membership Name</div>
						  <div>Membership Price</div>
						  <div>Membership Type</div>
						  <div>Add. Comm. on Purchases</div>
						  <div>Add. Comm. on Membership</div>
						  <div>Action</div>
						</div>
						
					<?php foreach ($referralcommissionplans as $referralcommissionplan):?>
						<div class="tablegridrow">
						<div><?php echo $referralcommissionplan['Membership']['id'];?></div>
						<div><?php echo $referralcommissionplan['Membership']['membership_name'];?></div>
						<div>$<?php echo $referralcommissionplan['Membership']['price'];?></div>
						<div>
								<?php
								if($referralcommissionplan['Membership']['membership_fee_type']=='Days')
									echo 'Every '.$referralcommissionplan['Membership']['membership_fee_value'].' Days';
								else
									echo $referralcommissionplan['Membership']['membership_fee_type'];
								?>
						</div>
						
						<div>
						<?php
						$commission=@explode(',0',$referralcommissionplan['Membership']['addcommissionlevel']);
						echo trim($commission[0],',');
						?>
						</div>
						<div>
						<?php
						$commission=@explode(',0',$referralcommissionplan['Membership']['add_commission_on_fees']);
						echo trim($commission[0],',');
						?>
						</div>
						<div class="textcenter">
							  <div class="actionmenu">
								<div class="btn-group">
								  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
									  Action <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu">
										
										<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_membershipplanadd/$',$SubadminAccessArray)){ ?>
										<li>
										<?php 
										echo $this->Js->link($this->html->image('men-icon2.png', array('alt'=>'Edit Membership'))." Edit", array('controller'=>'sitesetting', "action"=>"membershipplanadd/".$referralcommissionplan['Membership']['id']), array(
											'update'=>'#settingpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false
										));?>
										</li>
										<?php }?>
										<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_membershipremove',$SubadminAccessArray)){ ?>
										<li>
										<?php 
										echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Membership'))." Delete", array('controller'=>'sitesetting', "action"=>"membershipremove/".$referralcommissionplan['Membership']['id']."/".$currentpagenumber), array(
											'update'=>'#settingpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'confirm'=>'Are You Sure You Want to Delete This Membership?'
										));?>
										</li>
										<?php }?>
										
										<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_membershipstatus',$SubadminAccessArray)){ ?>
										<li>
											 <?php 
											 if($referralcommissionplan['Membership']['status']==0){
												 $statusaction='1';
												 $statusicon='red-icon.png';
													 $statustext='Activate';
											 }else{
												 $statusaction='0';
												 $statusicon='blue-icon.png';
													 $statustext='Inactivate';
											 }
											 echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'sitesetting', "action"=>"membershipstatus/".$statusaction."/".$referralcommissionplan['Membership']['id']."/".$currentpagenumber), array(
												 'update'=>'#settingpage',
												 'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												 'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												 'escape'=>false
											 ));?>
										</li>
										<?php }?>
								  </ul>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>	
				</div>
			<?php if(count($referralcommissionplans)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
				
			<?php 
			if($this->params['paging']['Membership']['count']>$this->Session->read('pagerecord'))
			{?>
			<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
			<div class="floatleft margintop19">
				<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'membership/0/rpp')));?>
				<div class="resultperpage">
							<label>
					<?php 
					echo $this->Form->input('resultperpage', array(
					  'type' => 'select',
					  'options' => $resultperpage,
					  'selected' => $this->Session->read('pagerecord'),
					  'class'=>'',
					  'label' => false,
					  'div'=>false,
					  'style' => '',
					  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
					));
					?>
					</label>
				</div>
				<span id="resultperpageapply" style="display:none;">
					<?php echo $this->Js->submit('Apply', array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#settingpage',
					  'class'=>'',
					  'div'=>false,
					  'controller'=>'Sitesetting',
					  'action'=>'membership/0/rpp',
					  'url'   => array('controller' => 'sitesetting', 'action' => 'membership/0/rpp')
					));?>
				</span>
				<?php echo $this->Form->end();?>
			</div>
			<?php }?>
			<div class="floatright">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '...', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
			</ul>
			</div>
			<div class="clear-both"></div>
			<div class="height10"></div>
		</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>