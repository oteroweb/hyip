<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Security</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Common Settings", array('controller'=>'sitesetting', "action"=>"antibrute"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Two Step Authentication Settings", array('controller'=>'sitesetting', "action"=>"googleauthentication"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Password Settings", array('controller'=>'sitesetting', "action"=>"passwordsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Website Security", array('controller'=>'sitesetting', "action"=>"websitesecurity"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Captcha Settings", array('controller'=>'sitesetting', "action"=>"captcha"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<div class="whitenoteboxinner"><b><span class="red-color">*<?php echo "Note";?> :</span></b> <?php echo "Admin Can Add a Record to <b>Block New Registrations</b> From Any Particular Email, Domain, IP Address or Country. Specific Username Can Also be Banned. Delete a Record Any Time to Again Allow New Sign Ups From Those Particular Emails, IP Addresses, Domains or Countries."?></div>
<div class="height10"></div>

<div class="helpicon"><a href="https://www.proxscripts.com/docs/Security_Settings#IP_Address" target="_blank">Help</a></div>

<div id="UpdateMessage"></div>
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'websitesecurityipaddress')));?>
	<div class="from-box">
		<div class="fromboxmain">
			<span>Search For :</span>
			<span class="searchforfields_s"><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
                  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#settingpage',
                  'class'=>'searchbtn',
                  'controller'=>'sitesetting',
                  'action'=>'websitesecurityipaddress',
                  'url'=> array('controller' => 'sitesetting', 'action' => 'websitesecurityipaddress')
                ));?>
			</span>
		</div>
	 </div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#settingpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'sitesetting', 'action'=>'websitesecurityipaddress')
	));
	$currentpagenumber=$this->params['paging']['Member_block']['page'];
	?>
<div id="gride-bg">
    <div class="Xpadding10">
	
<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'websitesecuritydelete')));?>
	  <div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_websitesecurityadd',$SubadminAccessArray)){ ?>
            <?php echo $this->Js->link("+ Add New", array('controller'=>'sitesetting', "action"=>"websitesecurityadd"), array(
                'update'=>'#settingpage',
                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                'escape'=>false,
                'class'=>'btnorange'
            ));?>
    <?php } ?>
	</div>
	<div class="clear-both"></div>
	 
			<div class="tab-innar">
				<ul class="floatleft nofloatonmobile">
				  <li>
						<?php echo $this->Js->link('Domain Name', array('controller'=>'sitesetting', "action"=>"websitesecurity"), array(
							'update'=>'#settingpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'',
						));?>
				  </li>
				  <li>
				  		<?php echo $this->Js->link('Email', array('controller'=>'sitesetting', "action"=>"websitesecurityemail"), array(
							'update'=>'#settingpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'',
						));?>
				  </li>
				  <li>
				  		<?php echo $this->Js->link('IP Address', array('controller'=>'sitesetting', "action"=>"websitesecurityipaddress"), array(
							'update'=>'#settingpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'active',
						));?>
				  </li>
				  <li>
				  		<?php echo $this->Js->link('Country', array('controller'=>'sitesetting', "action"=>"websitesecuritycountry"), array(
							'update'=>'#settingpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'',
						));?>
				  </li>
				  <li>
				  		<?php echo $this->Js->link('Username', array('controller'=>'sitesetting', "action"=>"websitesecurityusername"), array(
							'update'=>'#settingpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'',
						));?>
				  </li>
				</ul>
				<div class="massactionbox floatright textright forsmallviewmassactionbox" style='margin-top: 24px;'>
						<div class="actionmenu">
							  <div class="btn-group">
								  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
									Action <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu">
								  <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_websitesecurityadd/$',$SubadminAccessArray)){ ?>
								  <li>
									<?php echo $this->Js->submit('Delete All', array(
										  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
										  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										  'update'=>'#settingpage',
										  'class'=>'btndeleteall',
										  'div'=>false,
										  'controller'=>'sitesetting',
										  'action'=>'websitesecuritydelete/all',
										  'url'   => array('controller' => 'sitesetting', 'action' => 'websitesecuritydelete/all'."/i/".$currentpagenumber),
										  'confirm'=>'Are You Sure?'
									));?>
								  </li>
								  <?php } ?>
								  <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_websitesecurityremove',$SubadminAccessArray)){ ?>
								  <li>
									  <?php echo $this->Js->submit('Delete Selected', array(
										  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
										  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										  'update'=>'#settingpage',
										  'class'=>'btndeleteselected',
										  'div'=>false,
										  'controller'=>'sitesetting',
										  'action'=>'websitesecuritydelete',
										  'url'   => array('controller' => 'sitesetting', 'action' => 'websitesecuritydelete'."/0/i/".$currentpagenumber),
										  'confirm'=>'Are You Sure?'
									   ));?>
								  </li>
								  <?php } ?>
								</ul>
							  </div>
						</div>
				  </div>
				  <div class="clear-both"></div>
			</div> 
			
	<div class="tablegrid marginnone">
		    <div class="tablegridheader">
			      <div style="width: 90%;">IP Address</div>
			      <div>Action</div>
				  <div class="checkbox">
					<?php 
					echo $this->Form->checkbox('selectAllCheckboxes', array(
					  'hiddenField' => false,
					  'onclick' => 'selectAllCheckboxes("memberblockIds",this.checked)'
					));
					?>
					<label for="SitesettingSelectAllCheckboxes"></label>
                </div>
		    </div>
			<?php foreach ($member_blocks as $member_block): ?>
				<div class="tablegridrow">
					<div><?php echo $member_block['Member_block']['value']; ?></div>
					<div class="textcenter">
						<?php /* if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_websitesecurityadd/$',$SubadminAccessArray)){ ?>
								<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Record')).' Edit Record', array('controller'=>'sitesetting', "action"=>"websitesecurityadd/".$member_block['Member_block']['id']), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));?>
							<?php }*/ ?>
                            <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_websitesecurityremove',$SubadminAccessArray)){ ?>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Record')), array('controller'=>'sitesetting', "action"=>"websitesecurityremove/".$member_block['Member_block']['id']."/i/".$currentpagenumber), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
								'title'=>'Delete Record',
									'confirm'=>'Are You Sure?'
								));?>
							<?php } ?>
				    </div>
					<div class="checkbox">
						<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_websitesecurityremove',$SubadminAccessArray)){ ?>
							  <?php
							  echo $this->Form->checkbox('memberblockIds.', array(
								'value' => $member_block['Member_block']['id'],
								'class' => 'memberblockIds',
								'id'=>'memberblockIds'.$member_block['Member_block']['id'],
								'hiddenField' => false
							  ));
							  ?>
							  <label for="<?php echo 'memberblockIds'.$member_block['Member_block']['id'] ?>"></label>
						<?php } ?>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($member_blocks)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	<?php 
	if($this->params['paging']['Member_block']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'websitesecurityipaddress/rpp')));?>
		<div class="resultperpage">
                        <label>
			      <?php 
			      echo $this->Form->input('resultperpage', array(
				'type' => 'select',
				'options' => $resultperpage,
				'selected' => $this->Session->read('pagerecord'),
				'class'=>'searchcombobox',
				'label' => false,
				'div'=>false,
				'style' => '',
				'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			      ));
			      ?>
			</label>
		</div>
		
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#settingpage',
			  'class'=>'large white button',
			  'div'=>false,
			  'controller'=>'Sitesetting',
			  'action'=>'websitesecurityipaddress/rpp',
			  'url'   => array('controller' => 'sitesetting', 'action' => 'websitesecurityipaddress/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '...', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	</div>
</div>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>