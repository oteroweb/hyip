<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Admin</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Account Settings", array('controller'=>'sitesetting', "action"=>"account"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Sub Admin", array('controller'=>'sitesetting', "action"=>"subadmin"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
	  </div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Admin_Settings#Account_Settings" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>
<div class="tab-pane" id="profile">
	  <div class="frommain">
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'accountupdate')));?>
		<?php echo $this->form->input('member_id', array('type'=>'hidden','value'=>'1'));?>           
			
				  <div class="fromnewtext">Admin Username :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('user_name', array('type'=>'text', 'value'=>($this->Session->read('ADMINTYPE')=='Main')?$this->Session->read('adminusername'):'', 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
				  <div class="fromnewtext">Admin Email :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('email', array('type'=>'text', 'value'=>$memberdata['Member']['email'], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
				  <div class="fromnewtext">New Password :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('new_pwd', array('type'=>'password', 'value'=>'', 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				 
				  <div class="fromnewtext">Confirm Password :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('confirm_pwd', array('type'=>'password', 'value'=>'', 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
				  <div class="fromnewtext">Current Password :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('current_pwd', array('type'=>'password', 'value'=>'', 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
			<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_accountupdate',$SubadminAccessArray)){ ?>
				  <div class="formbutton">
						<?php echo $this->Js->submit('Update', array(
						  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage',
						  'class'=>'btnorange',
						  'div'=>false,
						  'controller'=>'sitesetting',
						  'action'=>'accountupdate',
						  'url'   => array('controller' => 'sitesetting', 'action' => 'accountupdate')
						));?>
				  </div>
			<?php } ?>
		<?php echo $this->Form->end();?>
		
		<?php if($this->Session->read('ADMINTYPE')=='Main'){?>
		
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'answerupdate')));?>
		<?php echo $this->form->input('member_id', array('type'=>'hidden','value'=>'1'));?>
			<div id="UpdateMessage"></div>
				  <div class="fromnewtext">Security Question :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('security_question', array('type'=>'text', 'value'=>$security_question, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
				  <div class="fromnewtext">Security Answer :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('security_answer', array('type'=>'password', 'value'=>'', 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				 
				  <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_accountupdate',$SubadminAccessArray)){ ?>
				  <div class="formbutton">
						<?php echo $this->Js->submit('Update', array(
						  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage',
						  'class'=>'btnorange',
						  'div'=>false,
						  'controller'=>'sitesetting',
						  'action'=>'answerupdate',
						  'url'   => array('controller' => 'sitesetting', 'action' => 'answerupdate')
						));?>
				  </div>
				  <?php } ?>
		<?php echo $this->Form->end();?>
		<?php }?>
	  </div>
</div>

<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>