<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Security</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Common Settings", array('controller'=>'sitesetting', "action"=>"antibrute"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Two Step Authentication Settings", array('controller'=>'sitesetting', "action"=>"googleauthentication"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Password Settings", array('controller'=>'sitesetting', "action"=>"passwordsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Website Security", array('controller'=>'sitesetting', "action"=>"websitesecurity"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Captcha Settings", array('controller'=>'sitesetting', "action"=>"captcha"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Security_Settings#Website_Security" target="_blank">Help</a></div>

	<div id="UpdateMessage"></div>
	
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'websitesecurityaddaction')));?>
		
		<?php if(isset($member_blockdata['Member_block']["id"])){
			echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$member_blockdata['Member_block']["id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}?>
		<?php echo $this->form->input('dt', array('type'=>'hidden','value'=>date("Y-m-d H:i:s")));?>
		<?php echo $this->form->input('modify_dt', array('type'=>'hidden','value'=>'0000-00-00 00:00:00'));?>
			<div class="tab-pane" id="profile">
				<div class="frommain">
					<div class="fromnewtext">Block Signups By :  </div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
							<?php 
							echo $this->Form->input('name', array(
							  'type' => 'select',
							  'options' => array('Domain Name'=>'Domain Name', 'Ip Address'=>'IP Address', 'Email'=>'Email', 'Username'=>'Username', 'Country'=>'Country'),
							  'selected' => $member_blockdata['Member_block']["name"],
							  'class'=>'',
							  'label' => false,
							  'div' => false,
							  'style' => ''
						  ));
							?>
							</label>
						</div>
					</div>
					<div class="fromnewtext">Enter Value :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('value', array('type'=>'text', 'value'=>$member_blockdata['Member_block']["value"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					<div class="formbutton">
					  <?php echo $this->Js->submit('Submit', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#UpdateMessage',
						'class'=>'btnorange',
						'controller'=>'sitesetting',
						'div'=>false,
						'action'=>'websitesecurityaddaction',
						'url'   => array('controller' => 'sitesetting', 'action' => 'websitesecurityaddaction')
					  ));?>
					  <?php echo $this->Js->link("Back", array('controller'=>'sitesetting', "action"=>"websitesecurity"), array(
						  'update'=>'#settingpage',
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'escape'=>false,
						  'div'=>false,
						  'class'=>'btngray'
					  ));?>
				   </div>
				</div>
			</div>
		<?php echo $this->Form->end();?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
			
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>