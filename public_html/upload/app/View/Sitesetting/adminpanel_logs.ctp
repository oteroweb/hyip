<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Website</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Main Details", array('controller'=>'sitesetting', "action"=>"index"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'class'=>'satting-menu-active'
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Member Tool Settings", array('controller'=>'sitesetting', "action"=>"shortenersetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Database Operations", array('controller'=>'sitesetting', "action"=>"maintenance"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>	
				</li>
				<li class="active">
					<?php echo $this->Js->link("Logs Settings", array('controller'=>'sitesetting', "action"=>"logs"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Launch Date Settings", array('controller'=>'sitesetting', "action"=>"launch"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Currency Settings", array('controller'=>'sitesetting', "action"=>"currency"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Cronjob Settings", array('controller'=>'sitesetting', "action"=>"cronjob"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Notification", array('controller'=>'sitesetting', "action"=>"notification"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Website_Settings#Logs_Settings" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>



	<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'logsupdate')));?>
	<?php echo $this->form->input('id', array('type'=>'hidden','value'=>'1'));?>
	<div class="tab-pane" id="profile">
		<div class="frommain">
			<div class="fromnewtext">Admin Activity : </div>
			<div class="fromborder">
				<?php echo $this->Form->input('logsaactivity', array('type'=>'text', 'value'=>$AdminActivitydays, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<div class="chekckbox fromchekspadding">
				<?php echo $this->Form->input('islogsaactivity', array('type'=>'checkbox', 'checked'=>($AdminActivity==1)?true:false, 'label' => '', 'div' => true, 'class'=>''));?>
			</div>
			<div class="fromnewtext">Member Activity : </div>
			<div class="fromborder">
				<?php echo $this->Form->input('logsmactivity', array('type'=>'text', 'value'=>$MemberActivitydays, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<div class="chekckbox fromchekspadding">
				<?php echo $this->Form->input('islogsmactivity', array('type'=>'checkbox', 'checked'=>($MemberActivity==1)?true:false, 'label' => '', 'div' => true, 'class'=>''));?>
			</div>
			<div class="fromnewtext">Admin Logs : </div>
			<div class="fromborder">
				<?php echo $this->Form->input('logsadmin', array('type'=>'text', 'value'=>$AdminLogsdays, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<div class="chekckbox fromchekspadding">
				<?php echo $this->Form->input('islogsadmin', array('type'=>'checkbox', 'checked'=>($AdminLogs==1)?true:false, 'label' => '', 'div' => true, 'class'=>''));?>
			</div>
			<div class="fromnewtext">Member & IP Logs : </div>
			<div class="fromborder">
				<?php echo $this->Form->input('logsmember', array('type'=>'text', 'value'=>$MemberIPLogsdays, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<div class="chekckbox fromchekspadding">
				<?php echo $this->Form->input('islogsmember', array('type'=>'checkbox', 'checked'=>($MemberIPLogs==1)?true:false, 'label' => '', 'div' => true, 'class'=>''));?>
			</div>
			<div class="fromnewtext">Cronjob Logs : </div>
			<div class="fromborder">
				<?php echo $this->Form->input('logscronjob', array('type'=>'text', 'value'=>$CronjobLogsdays, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<div class="chekckbox fromchekspadding">
				<?php echo $this->Form->input('islogscronjob', array('type'=>'checkbox', 'checked'=>($CronjobLogs==1)?true:false, 'label' => '', 'div' => true, 'class'=>''));?>
			</div>
			<div class="fromnewtext">Processor Trace Logs : </div>
			<div class="fromborder">
				<?php echo $this->Form->input('logsprocessortrace', array('type'=>'text', 'value'=>$ProcessorTraceLogsdays, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<div class="chekckbox fromchekspadding">
				<?php echo $this->Form->input('islogsprocessortrace', array('type'=>'checkbox', 'checked'=>($ProcessorTraceLogs==1)?true:false, 'label' => '', 'div' => true, 'class'=>''));?>
			</div>
			<div class="fromnewtext">Sent Emails : </div>
			<div class="fromborder">
				<?php echo $this->Form->input('logsemail', array('type'=>'text', 'value'=>$EmailLogsdays, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			</div>
			<div class="chekckbox fromchekspadding">
				<?php echo $this->Form->input('islogsemail', array('type'=>'checkbox', 'checked'=>($EmailLogs==1)?true:false, 'label' => '', 'div' => true, 'class'=>''));?>
			</div>
		<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_logsupdate',$SubadminAccessArray)){ ?>
			<div class="formbutton">
				<?php echo $this->Js->submit('Update', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#UpdateMessage',
					'class'=>'btnorange',
					'div'=>false,
					'controller'=>'Sitesetting',
					'action'=>'logsupdate',
					'url'   => array('controller' => 'sitesetting', 'action' => 'logsupdate')
				));?>
			</div>
		<?php } ?>
		</div>
	</div>
	<?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>