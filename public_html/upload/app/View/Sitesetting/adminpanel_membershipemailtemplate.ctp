<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Email</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("SMTP Settings", array('controller'=>'sitesetting', "action"=>"smtp"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Auto Responder", array('controller'=>'sitesetting', "action"=>"autoresponder"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Account Activation Mail", array('controller'=>'sitesetting', "action"=>"activationautomail"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Unpaid Member Notification", array('controller'=>'sitesetting', "action"=>"unpaidmembernotification"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Inactive Member Mail", array('controller'=>'sitesetting', "action"=>"inactivemembermail"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Membership Template Emails", array('controller'=>'sitesetting', "action"=>"membershipemailtemplate"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>

<div class="helpicon"><a href="https://www.proxscripts.com/docs/Email_Settings#Membership_Template_Emails" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#settingpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'sitesetting', 'action'=>'membershipemailtemplate')
	));
	?>
<div id="Xgride-bg">
	  <div class="Xpadding10">
			<div class="greenbottomborder">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
		<div class="tablegridheader">
			<div>Id</div>
			<div>Subject</div>
			<div>Day(s) Before Expiry</div>
			<div>Action</div>
		</div>
		<?php foreach ($membership_template_emails as $membership_template_email): ?>
		<div class="tablegridrow">
			<div><?php echo $membership_template_email['Membership_template_email']['template_id']; ?></div>
			<div><?php echo stripslashes($membership_template_email['Membership_template_email']['subject']); ?></div>
			<div><?php echo $membership_template_email['Membership_template_email']['days']; ?></div>
			<div class="textcenter">
				<div class="actionmenu">
					<div class="btn-group">
						<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
						  Action <span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
							
							<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_membershipemailtemplateedit',$SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Email')).' Edit Email', array('controller'=>'sitesetting', "action"=>"membershipemailtemplateedit/".$membership_template_email['Membership_template_email']['template_id']), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));?>
							</li>
							<?php } ?>
							
							<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_membershipemailtemplatestatus',$SubadminAccessArray)){ ?>
							<li>
								<?php 
								if($membership_template_email['Membership_template_email']['status']==0){
									$statusaction='1';
									$statusicon='red-icon.png';
									$statustext='Activate Email';
								}else{
									$statusaction='0';
									$statusicon='blue-icon.png';
									$statustext='Inactivate Email';}
								echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'sitesetting', "action"=>"membershipemailtemplatestatus/".$statusaction."/".$membership_template_email['Membership_template_email']['template_id']."/".$this->params['paging']['Membership_template_email']['page']), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));?>
							</li>
							<?php } ?>
							
						</ul>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
	<?php if(count($membership_template_emails)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	<?php 
	if($this->params['paging']['Membership_template_email']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'membershipemailtemplate/rpp')));?>
		<div class="resultperpage">
                        <label>
			      <?php 
			      echo $this->Form->input('resultperpage', array(
				'type' => 'select',
				'options' => $resultperpage,
				'selected' => $this->Session->read('pagerecord'),
				'class'=>'',
				'label' => false,
				'div'=>false,
				'style' => '',
				'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			      ));
			      ?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#settingpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'Sitesetting',
			  'action'=>'membershipemailtemplate/rpp',
			  'url'   => array('controller' => 'sitesetting', 'action' => 'membershipemailtemplate/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '...', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
	<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>