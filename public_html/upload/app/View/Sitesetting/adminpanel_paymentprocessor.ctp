<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 19-11-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Finance</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Payment Processors", array('controller'=>'sitesetting', "action"=>"paymentprocessor"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Add Fund Settings", array('controller'=>'sitesetting', "action"=>"addfund"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Withdrawal Settings", array('controller'=>'sitesetting', "action"=>"withdrawal"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Finance_Settings#Payment_Processors" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div id="Xgride-bg" class="bottomgreenborder">
    <div class="Xpadding10">
	<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_paymentprocessoradd',$SubadminAccessArray)){ ?>
	  <?php echo $this->Js->link("Add Processor", array('controller'=>'sitesetting', "action"=>"paymentprocessoraddnew"), array(
				  'update'=>'#settingpage',
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'escape'=>false,
				  'class'=>'btnorange'
	  ));?>
	  <?php } ?>
    
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>Id</div>
				<div>Processor Name</div>
				<div>Add Fund</div>
				<div>Purchase</div>
				<div>Withdraw</div>
				<div>Auto Withdraw</div>
				<div>Max With. Req. Amount($)</div>
				<div>Fees(%)</div>
				<div>Action</div>
			</div>
			<?php foreach ($processors as $processor): ?>
				<div class="tablegridrow <?php if($processor['Processor']['proc_status']==0){ echo 'red'; } ?>">
					<div><?php echo $processor['Processor']['id']; ?></div>
					<div><?php echo $processor['Processor']['proc_name']; ?></div>
                    <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_paymentprocessorstatus',$SubadminAccessArray)){ ?>
					<div>
						<?php 
						if($processor['Processor']['isaddfund']==0){
							$receivefundaction='1';
							$receivefundicon='off.jpg';
							$receivefundtext='Enable Add Funds';
						}else{
							$receivefundaction='0';
							$receivefundicon='on.jpg';
							$receivefundtext='Disable Add Funds';}
						echo $this->Js->link($this->html->image($receivefundicon, array('alt'=>$receivefundtext)), array('controller'=>'sitesetting', "action"=>"paymentprocessorstatus/fund/".$receivefundaction."/".$processor['Processor']['id']), array(
							'update'=>'#settingpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>$receivefundtext
						));?>
					</div>
                    <?php }else{echo '<div>&nbsp;</div>';} ?>
					<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_paymentprocessorstatus',$SubadminAccessArray)){ ?>
					<div>
						<?php 
						if($processor['Processor']['receivefund']==0){
							$receivefundaction='1';
							$receivefundicon='off.jpg';
							$receivefundtext='Enable Purchase';
						}else{
							$receivefundaction='0';
							$receivefundicon='on.jpg';
							$receivefundtext='Disable Purchase';}
						echo $this->Js->link($this->html->image($receivefundicon, array('alt'=>$receivefundtext)), array('controller'=>'sitesetting', "action"=>"paymentprocessorstatus/purchase/".$receivefundaction."/".$processor['Processor']['id']), array(
							'update'=>'#settingpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>$receivefundtext
						));?>
					</div>
                    <?php }else{echo '<div>&nbsp;</div>';} ?>
                    <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_paymentprocessorstatus',$SubadminAccessArray)){ ?>
					<div>
						<?php 
						if($processor['Processor']['is_masspay']==0){
							$is_masspayaction='1';
							$is_masspayicon='off.jpg';
							$is_masspaytext='Enable Withdrawals';
						}else{
							$is_masspayaction='0';
							$is_masspayicon='on.jpg';
							$is_masspaytext='Disable Withdrawals';}
						echo $this->Js->link($this->html->image($is_masspayicon, array('alt'=>$is_masspaytext)), array('controller'=>'sitesetting', "action"=>"paymentprocessorstatus/withdrow/".$is_masspayaction."/".$processor['Processor']['id']), array(
							'update'=>'#settingpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>$is_masspaytext
						));?>
					</div>
                    <?php }else{echo '<div>&nbsp;</div>';} ?>
                    <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_paymentprocessorstatus',$SubadminAccessArray)){ ?>
					<div>
						<?php 
						if($processor['Processor']['autowithdrow']==0){
							$autowithdrowaction='1';
							$autowithdrowicon='off.jpg';
							$autowithdrowtext='Enable Auto Withdrawals';
						}else{
							$autowithdrowaction='0';
							$autowithdrowicon='on.jpg';
							$autowithdrowtext='Disable Auto Withdrawals';}
							echo $this->Js->link($this->html->image($autowithdrowicon, array('alt'=>$autowithdrowtext)), array('controller'=>'sitesetting', "action"=>"paymentprocessorstatus/autowithdrow/".$autowithdrowaction."/".$processor['Processor']['id']), array(
								'update'=>'#settingpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>$autowithdrowtext
							));
						?>
					</div>
                    <?php }else{echo '<div>&nbsp;</div>';} ?>
					<div><?php echo $processor['Processor']['max_req_amt']; ?></div>
					<div><?php echo $processor['Processor']['proc_fee']; ?></div>
                    <div class="textcenter">
					<div class="actionmenu">
						<div class="btn-group">
							<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
	                        <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_paymentprocessorstatus',$SubadminAccessArray)){ ?>
								
								<?php } ?>
							<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_paymentprocessoradd/$',$SubadminAccessArray)){ ?>
                                <li>
                                    <?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Details')).' Edit Details', array('controller'=>'sitesetting', "action"=>"paymentprocessoradd/".$processor['Processor']['id']), array(
                                        'update'=>'#settingpage',
                                        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                        'escape'=>false
                                    ));?>
                                </li>
                                <li>
                                    <?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Processor Name/Directory')).' Edit Processor Directory', array('controller'=>'sitesetting', "action"=>"paymentprocessoraddnew/".$processor['Processor']['id']), array(
                                        'update'=>'#settingpage',
                                        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                        'escape'=>false
                                    ));?>
                                </li>
								<li>
									<?php 
									if($processor['Processor']['proc_status']==0){
										$statusaction='1';
										$statusicon='red-icon.png';
										$statustext='Activate Processor';
									}else{
										$statusaction='0';
										$statusicon='blue-icon.png';
										$statustext='Inactivate Processor';}
									echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'sitesetting', "action"=>"paymentprocessorstatus/status/".$statusaction."/".$processor['Processor']['id']), array(
										'update'=>'#settingpage',
										'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'escape'=>false
									));?>
								</li>
				<?php } ?>
                          </ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($processors)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>