<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Signup Settings</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Registration Form Settings", array('controller'=>'sitesetting', "action"=>"registrationform"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Profile Form Settings", array('controller'=>'sitesetting', "action"=>"profileform"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Free Credit & Bonus", array('controller'=>'sitesetting', "action"=>"freecreditandbonus"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Signup Settings", array('controller'=>'sitesetting', "action"=>"signup"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Facebook & Google Settings", array('controller'=>'sitesetting', "action"=>"facebookgoogle"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>

<div class="height10"></div>

<div class="helpicon"><a href="https://www.proxscripts.com/docs/Signup_Settings#Registration_Form_Settings" target="_blank">Help</a></div>

<div id="UpdateMessage"></div>		
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#settingpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'sitesetting', 'action'=>'registrationform')
	));
	$currentpagenumber=$this->params['paging']['Regform_manage']['page'];
	?>
<div id="Xgride-bg">
    <div class="Xpadding10">
	  <div class="greenbottomborder">
	  <div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_registrationformadd',$SubadminAccessArray)){ ?>
            <?php echo $this->Js->link("+ Add New", array('controller'=>'sitesetting', "action"=>"registrationformadd"), array(
                'update'=>'#settingpage',
                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                'escape'=>false,
                'class'=>'btnorange'
            ));?>
    <?php } ?>
	</div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'sitesetting', "action"=>"registrationform/0/0/field_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Field Title', array('controller'=>'sitesetting', "action"=>"registrationform/0/0/field_title/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Field Title'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Field Type', array('controller'=>'sitesetting', "action"=>"registrationform/0/0/field_type/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Field Type'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Display Order', array('controller'=>'sitesetting', "action"=>"registrationform/0/0/display_order/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Display Order'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Required Field', array('controller'=>'sitesetting', "action"=>"registrationform/0/0/field_req/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Required Field'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Status', array('controller'=>'sitesetting', "action"=>"registrationform/0/0/status/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Status'
					));?>
				</div>
				<div>Action</div>
			</div>
			<?php foreach ($regform_manages as $regform_manage): ?>
				<div class="tablegridrow">
					<div><?php echo $regform_manage['Regform_manage']['field_id']; ?></div>
					<div><?php echo $regform_manage['Regform_manage']['field_title']; ?></div>
					<div><?php echo ucfirst($regform_manage['Regform_manage']['field_type']); ?></div>
					<div>
						<?php 
						//if($regform_manage['Regform_manage']['field_recommend']==1){echo "N/A";}
						//else
						if(true)
						{
							if($regform_manage['Regform_manage']['display_order']==$mino && $maxo>1)
							{
								echo $this->Js->link($this->html->image('down.png', array('alt'=>'')), array('controller'=>'sitesetting', "action"=>"registrationformaction/down/".$regform_manage['Regform_manage']['display_order']."/".$regform_manage['Regform_manage']['field_id']."/".$currentpagenumber), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>'Move Down'
								));
							}
							elseif($regform_manage['Regform_manage']['display_order']==$maxo && $maxo>1)
							{
								echo $this->Js->link($this->html->image('up.png', array('alt'=>'')), array('controller'=>'sitesetting', "action"=>"registrationformaction/up/".$regform_manage['Regform_manage']['display_order']."/".$regform_manage['Regform_manage']['field_id']."/".$currentpagenumber), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>'Move Up'
								));
							}
							elseif($regform_manage['Regform_manage']['display_order']<$maxo && $regform_manage['Regform_manage']['display_order']>$mino)
							{
								echo $this->Js->link($this->html->image('down.png', array('alt'=>'')), array('controller'=>'sitesetting', "action"=>"registrationformaction/down/".$regform_manage['Regform_manage']['display_order']."/".$regform_manage['Regform_manage']['field_id']."/".$currentpagenumber), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>'Move Down'
								));
								echo '&nbsp;';
								echo $this->Js->link($this->html->image('up.png', array('alt'=>'')), array('controller'=>'sitesetting', "action"=>"registrationformaction/up/".$regform_manage['Regform_manage']['display_order']."/".$regform_manage['Regform_manage']['field_id']."/".$currentpagenumber), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>'Move Up'
								));
							}
						}
						?>
					</div>
                    <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_registrationformaction',$SubadminAccessArray)){ ?>
					<div>
						<?php 
						if($regform_manage['Regform_manage']['field_req']==0){
							$field_reqaction='1';
							$field_reqicon='red-icon.png';
							$field_reqtext='Make This Required';
						}else{
							$field_reqaction='0';
							$field_reqicon='blue-icon.png';
							$field_reqtext='Make This Not Required';}
						
						if($regform_manage['Regform_manage']['field_recommend']==0)
						{
							
							echo $this->Js->link($this->html->image($field_reqicon, array('alt'=>$field_reqtext)), array('controller'=>'sitesetting', "action"=>"registrationformaction/required/".$field_reqaction."/".$regform_manage['Regform_manage']['field_id']."/".$currentpagenumber), array(
								'update'=>'#settingpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>$field_reqtext
							));
						}
						else
						{
							echo "N/A";
						}
						?>
					</div>
                    <?php }else{echo '<div>&nbsp;</div>';} ?>
                    <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_registrationformaction',$SubadminAccessArray)){ ?>
					<div>
						<?php 
						if($regform_manage['Regform_manage']['status']==0){
							$statusaction='1';
							$statusicon='red-icon.png';
							$statustext='Activate Field';
						}else{
							$statusaction='0';
							$statusicon='blue-icon.png';
							$statustext='Inactivate Field';}
						
						if($regform_manage['Regform_manage']['field_recommend']==0)
						{
							
							echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)), array('controller'=>'sitesetting', "action"=>"registrationformaction/status/".$statusaction."/".$regform_manage['Regform_manage']['field_id']."/".$currentpagenumber), array(
								'update'=>'#settingpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>$statustext
							));
						}
						else
						{
							echo "N/A";
						}
						?>
					</div>
                    <?php }else{echo '<div>&nbsp;</div>';} ?>
					<div class="textcenter">
					<div class="actionmenu">
						<div class="btn-group">
							<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
							<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_registrationformadd/$',$SubadminAccessArray)){ ?>
							<li>
								<?php 
								if($regform_manage['Regform_manage']['field_recommend']==0)
								{
									if(!in_array($regform_manage['Regform_manage']['field_title'],array("Address", "Country","Contact No", "First Name", "Last Name", "Payment Processor", "Processor Acc Id")))
									{
										echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Field')).' Edit Field', array('controller'=>'sitesetting', "action"=>"registrationformadd/".$regform_manage['Regform_manage']['field_id']), array(
											'update'=>'#settingpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false
										));
									}
								}
								?>
							</li>
                            <?php } ?>
                            <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_registrationformremove',$SubadminAccessArray)){ ?>
							<li>
								<?php 
								if($regform_manage['Regform_manage']['field_recommend']==0)
								{
									if(!in_array($regform_manage['Regform_manage']['field_title'],array("Address", "Country","Contact No", "First Name", "Last Name", "Payment Processor", "Processor Acc Id")))
									{
										echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Field')).' Delete Field', array('controller'=>'sitesetting', "action"=>"registrationformremove/".$regform_manage['Regform_manage']['field_id']."/".$currentpagenumber), array(
											'update'=>'#settingpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'confirm'=>'Are You Sure?'
										));
									}
								}
								?>
							</li>
                            <?php } ?>
						 </ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
				<?php if(count($regform_manages)==0){ echo '<tr><td colspan="9" class="padding-tabal"></td></tr><tr class="blue-color"><td>&nbsp;</td><td  align="center" valign="middle" colspan="7">No records available</td><td>&nbsp;</td></tr>';} ?>
	</div>
	<?php 
	if($this->params['paging']['Regform_manage']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'registrationform/rpp')));?>
		<div class="combobox1">
		<?php 
		echo $this->Form->input('resultperpage', array(
		  'type' => 'select',
		  'options' => $resultperpage,
		  'selected' => $this->Session->read('pagerecord'),
		  'class'=>'',
		  'label' => false,
		  'div'=>false,
		  'style' => '',
		  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
		));
		?>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#settingpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'Sitesetting',
			  'action'=>'registrationform/rpp',
			  'url'   => array('controller' => 'sitesetting', 'action' => 'registrationform/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	  </div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>