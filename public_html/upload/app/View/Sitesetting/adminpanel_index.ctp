<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Website</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Main Details", array('controller'=>'sitesetting', "action"=>"index"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'class'=>'satting-menu-active'
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Member Tools", array('controller'=>'sitesetting', "action"=>"shortenersetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Database Operations", array('controller'=>'sitesetting', "action"=>"maintenance"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>	
				</li>
				<li>
					<?php echo $this->Js->link("Logs Settings", array('controller'=>'sitesetting', "action"=>"logs"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Launch Date Settings", array('controller'=>'sitesetting', "action"=>"launch"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Currency Settings", array('controller'=>'sitesetting', "action"=>"currency"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Cronjob Settings", array('controller'=>'sitesetting', "action"=>"cronjob"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Notifications", array('controller'=>'sitesetting', "action"=>"notification"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<script type="text/javascript"> 
(function() {
$('#SiteSettingForm').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();
</script>
<?php if($IsAdminAccess){?>
				<div class="helpicon"><a href="https://www.proxscripts.com/docs/Website_Settings#Main_Details" target="_blank">Help</a></div>
				<div id="UpdateMessage"></div>
				
				
				<?php echo $this->Form->create('Sitesetting',array('id'=>'SiteSettingForm', 'type' => 'file', 'onsubmit' => 'return true;', 'url'=>array('controller'=>'sitesetting','action'=>'maindetail')));?>
				<?php echo $this->form->input('id', array('type'=>'hidden','value'=>'1'));?>
				<div class="tab-pane" id="profile">
					<div class="frommain">
						<div class="nameandbox">
							<?php echo $this->Html->image('logo.png', array('alt' => 'Site Logo Not Set'))?>
						</div>
						<div class="fromnewtext">Site Logo : </div>
						<div class="fromborderdropedown3">
							<div class="btnorange browsebutton">Browse<?php echo $this->Form->input('logo', array('type' => 'file','label' => false, 'div' => false,'class'=>'browserbuttonlink','onchange'=>'$(".logoimgname").html(this.value);')); ?></div>
						</div>
						
						<div class="fromnewtext">Site Title :<span class="red-color">*</span>  </div>
						<div class="fromborderdropedown3">
							<?php echo $this->Form->input('sitetitle', array('type'=>'text', 'value'=>stripslashes($SITECONFIG["sitetitle"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
						</div>
						
						
						<div class="fromnewtext">Site Email :<span class="red-color">*</span>  </div>
						<div class="fromborderdropedown3">
							<?php echo $this->Form->input('site_email', array('type'=>'text', 'value'=>$SITECONFIG["site_email"],'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
						</div>
						
				    
						<div class="fromnewtext">Email Signature :  </div>
						<div class="fromborderdropedown3">
							<?php echo $this->Form->input('signature', array('type'=>'textarea', 'value'=>stripslashes($SITECONFIG["signature"]),'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
						    
						</div>
						<div class="fromnewtext">Ticket Signature :  </div>
						<div class="fromborderdropedown3">
							<?php echo $this->Form->input('ticket_signature', array('type'=>'textarea', 'value'=>stripslashes($SITECONFIG["ticket_signature"]),'label' => false,'div' => false, 'class'=>'from-textarea'));?>
						    
						</div>
						<div class="fromnewtext">Maintenance Mode :<span class="red-color">*</span>  </div>
						<div class="fromborderdropedown3">
							<div class="select-main">
							<label>
							<?php 
								echo $this->Form->input('maintenancemode', array(
									'type' => 'select',
									'options' => array('1'=>'Yes', '0'=>'No'),
									'selected' => $SITECONFIG["maintenancemode"],
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => '',
									'onchange' => 'if(this.selectedIndex==1){$(".maintenancemode_fields").hide(500);}else{$(".maintenancemode_fields").show(500);}'
								));
							?>
							</label>
							</div>
						</div>
						
						<div class="fromnewtext maintenancemode_fields" style="display:<?php if($SITECONFIG["maintenancemode"]==1){echo '';}else{echo 'none';}?>;"></div>
						<div class="frombordermain maintenancemode_fields" style="display:<?php if($SITECONFIG["maintenancemode"]==1){echo '';}else{echo 'none';}?>;">
							<?php echo $this->Form->input('maintenancemodetext', array('type'=>'textarea', 'value'=>stripslashes($SITECONFIG["maintenancemodetext"]), 'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
						</div>
						<div class="fromnewtext">Allow SSL (https://) :  </div>
						<div class="fromborderdropedown3">
							<div class="select-main">
							<label>
							<?php 
                                                            echo $this->Form->input('allowssl', array(
								  'type' => 'select',
								  'options' => array('1'=>'Yes', '0'=>'No'),
								  'selected' => $SITECONFIG["allowssl"],
								  'class'=>'',
								  'label' => false,
								  'div' => false,
								  'style' => '',
                                                                  'onchange' => 'if(!confirm("Warning: If SSL is not installed then please do not enable the SSL from Admin area. As it will stops site from working.")){$(this).val('.$SITECONFIG["allowssl"].');}'
							  ));
							?>
							</label>
							</div>
						</div>
						
					
					
					
					<div class="fromnewtext">Affiliate Id :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('adminid', array('type'=>'text', 'value'=>$SITECONFIG["adminid"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					
					<div class="fromnewtext">Powered By Link :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
						<label>
						<?php 
							  echo $this->Form->input('poweredby_link', array(
								  'type' => 'select',
								  'options' => array('ProxCore.Com'=>'ProxCore.Com', 'ProxScripts.Com'=>'ProxScripts.Com'),
								  'selected' => $SITECONFIG["poweredby_link"],
								  'class'=>'',
								  'label' => false,
								  'div' => false,
								  'style' => ''
							  ));
						?>
						</label>
						</div>
					</div>
					
					<div class="fromnewtext">Default Language :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
						<label>
						<?php 
							  echo $this->Form->input('defaultlanguage', array(
								  'type' => 'select',
								  'options' => $languagedata,
								  'selected' => $SITECONFIG["defaultlanguage"],
								  'class'=>'',
								  'label' => false,
								  'div' => false,
								  'style' => ''
							  ));
						?>
						</label>
						</div>
					</div>
					
					<div class="fromnewtext">Display Language Selection on Front Side :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
						<label>
						<?php 
							  echo $this->Form->input('languageonfront', array(
								  'type' => 'select',
								  'options' => array('1'=>'Yes', '0'=>'No'),
								  'selected' => $SITECONFIG["languageonfront"],
								  'class'=>'',
								  'label' => false,
								  'div' => false,
								  'style' => ''
							  ));
						?>
						</label>
						</div>
					</div>
					
					<div class="fromnewtext">Set your Time Zone : </div>
					<div class="fromborderdropedown3">
						<div class="select-main">
						<label>
						<?php echo $this->timezone->select('timezone', false, $SITECONFIG["timezone"]);?>
						</label>
						</div>
					</div>
					
					<div class="fromnewtext">Set Time and Date Format :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('timeformate', array('type'=>'text', 'value'=>$SITECONFIG["timeformate"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					
					<div class="fromnewtext">Custom Logout URL :</div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('custom_logout_url', array('type'=>'text', 'value'=>$SITECONFIG["custom_logout_url"], 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'placeholder'=>'http://'));?>
					</div>
					
					<div class="fromnewtext">Import Contacts Feature from Tell Friends Page :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
						<label>
						<?php 
							  echo $this->Form->input('importcontact', array(
								  'type' => 'select',
								  'options' => array('1'=>'Yes', '0'=>'No'),
								  'selected' => $SITECONFIG["importcontact"],
								  'class'=>'',
								  'label' => false,
								  'div' => false,
								  'style' => ''
							  ));
						?>
						</label>
						</div>
					</div>
					
					<div class="fromnewtext">Enable Member Tickets :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
						<label>
						<?php 
							  echo $this->Form->input('mticket_chk', array(
								  'type' => 'select',
								  'options' => array('1'=>'Yes', '0'=>'No'),
								  'selected' => $SITECONFIG["mticket_chk"],
								  'class'=>'',
								  'label' => false,
								  'div' => false,
								  'style' => ''
							  ));
						?>
						</label>
						</div>
					</div>
					
					<div class="fromnewtext">Enable Public Tickets :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
						<label>
						<?php 
							  echo $this->Form->input('pticket_chk', array(
								  'type' => 'select',
								  'options' => array('1'=>'Yes', '0'=>'No'),
								  'selected' => $SITECONFIG["pticket_chk"],
								  'class'=>'',
								  'label' => false,
								  'div' => false,
								  'style' => ''
							  ));
						?>
						</label>
						</div>
					</div>
					
					<div class="fromnewtext">Enable Agree With Terms And Conditions :</div>
					<div class="fromborderdropedown3">
						<div class="select-main">
						<label>
						<?php 
							  echo $this->Form->input('enableagree', array(
								  'type' => 'select',
								  'options' => array('1'=>'Yes', '0'=>'No'),
								  'selected' => $SITECONFIG["enableagree"],
								  'class'=>'',
								  'label' => false,
								  'div' => false,
								  'style' => ''
							  ));
						?>
						</label>
						</div>
					</div>
					
					
					
					<div class="fromnewtext">Cache Time :</div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('cacheduration', array('type'=>'text', 'value'=>$SITECONFIG["cacheduration"], 'label' => false, 'div' => false,'class'=>'fromboxbghalf','style'=>''));?>
						<div class="select-main2 select-main-different">
							<div class="select-main">
							<label>
							<?php echo $this->Form->input('cachetype', array(
								'type' => 'select',
								'options' => array('minutes'=>'Minutes', 'hours'=>'Hours', 'week'=>'Weeks'),
								'selected' => $SITECONFIG["cachetype"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));?>
							</label>
							</div>
						</div>
					</div>
					
					<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_maindetail',$SubadminAccessArray)){ ?>
					<div class='formbutton'>
						<?php echo $this->Form->submit('Update', array(
							'class'=>'btnorange',
							'div'=>false
						));?>
					</div>
					<?php } ?>
				</div>
			</div>
				<?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>