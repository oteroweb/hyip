<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 31-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Finance</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Payment Processor", array('controller'=>'sitesetting', "action"=>"paymentprocessor"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Add Fund Settings", array('controller'=>'sitesetting', "action"=>"addfund"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Withdrawal Settings", array('controller'=>'sitesetting', "action"=>"withdrawal"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Finance_Settings#Withdrawal_Settings" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'withdrawalupdate')));?>
		<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
		<div class="tab-pane" id="profile">
			<div class="frommain">
				  <div class="fromnewtext">Free Member Can Request a Withdrawal :</div>
				  <div class="fromborderdropedown3">
						<div class="select-main">
						<label>
					  <?php 
							echo $this->Form->input('freememberwithdroval', array(
								'type' => 'select',
								'options' => array('1'=>'Yes', '0'=>'No'),
								'selected' => $SITECONFIG["freememberwithdroval"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));
					  ?>
						</label>
						</div>
				  </div>
				  
			    <div class="fromnewtext">Minimum Sum Of Withdrawal ($) : <span class="red-color">*</span> </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('minsumofwidth', array('type'=>'text', 'value'=>$SITECONFIG["minsumofwidth"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
				  <div class="fromnewtext">Maximum Sum Of Withdrawal ($) : <span class="red-color">*</span> </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('maxsumofwidth', array('type'=>'text', 'value'=>$SITECONFIG["maxsumofwidth"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
				  <div class="fromnewtext">Maximum Requests Allowed Per Day : <span class="red-color">*</span> </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('memberwithdrawlimit', array('type'=>'text', 'value'=>$SITECONFIG["memberwithdrawlimit"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
				  <div class="fromnewtext">Maximum Request Amount Allowed Per Day ($) : <span class="red-color">*</span> </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('withdrawamountlimitperday', array('type'=>'text', 'value'=>$SITECONFIG["withdrawamountlimitperday"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
				  <div class="fromnewtext">Maximum Request Amount Allowed Per Week ($) : <span class="red-color">*</span> </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('withdrawamountlimitperweek', array('type'=>'text', 'value'=>$SITECONFIG["withdrawamountlimitperweek"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
				  <div class="fromnewtext">Balance : </div>
				  <div class="fromborderdropedown3" style="padding: 4px;">
					  <?php
						  $pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning', 'commission'=>'Commission');
						  if($SITECONFIG["wallet_for_commission"] == 'cash' && $SITECONFIG["wallet_for_earning"] == 'cash')
						  {
							  $pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase');
						  }
						  elseif($SITECONFIG["wallet_for_earning"] == 'cash')
						  {
							  $pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'commission'=>'Commission');
						  }
						  elseif($SITECONFIG["wallet_for_commission"] == 'cash')
						  {
							  $pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning');
						  }
						$selected = @explode(",",$SITECONFIG["withdrawbalance"]);
						echo $this->Form->input('withdrawbalance', array('type' => 'select', 'div'=>false, 'label'=>false, 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $pmethod));?>
				  </div>
				  
				  <div class="fromnewtext">Withdrawal Days : </div>
				  <div class="fromborderdropedown3" style="padding: 4px;">
					  <?php
					$selected = @explode(",",$SITECONFIG["withdraw_days"]);
					echo $this->Form->input('withdraw_days', array('type' => 'select', 'div'=>false, 'label'=>false, 'multiple' => 'checkbox', 'selected' => $selected, 'options' => array(
							'7' => 'Sun',
							'1' => 'Mon',
							'2' => 'Tue',
							'3' => 'Wed',
							'4' => 'Thu',
							'5' => 'Fri',
							'6' => 'Sat'
							)
					));?>
				  </div>
				  
			<?php if($SITECONFIG["balance_type"]==1){ ?>
					<div class="fromnewtext">Put Restriction On Withdrawals (Prevent Exchangers) :  </div>
					<div class="fromborderdropedown3">
						<div class="select-main">
						<label>
						<?php 
                          echo $this->Form->input('withdraw_restriction', array(
                              'type' => 'select',
                              'options' => array('1'=>'Yes', '0'=>'No'),
                              'selected' => $SITECONFIG["withdraw_restriction"],
                              'class'=>'',
                              'label' => false,
                              'div' => false,
                              'style' => ''
                          ));
						?>
						</label>
						</div>
					</div>
					
			<?php } ?>
			 	  <div class="formbutton"> 
			  <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_withdrawalupdate',$SubadminAccessArray)){ ?>
                    <?php echo $this->Js->submit('Update', array(
                          'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                          'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                          'update'=>'#UpdateMessage',
                          'class'=>'btnorange',
                          'controller'=>'sitesetting',
                          'action'=>'withdrawalupdate',
                          'url'   => array('controller' => 'sitesetting', 'action' => 'withdrawalupdate')
                    ));?>
              <?php } ?>
				</div>
			 </div>
		</div>
		<?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>