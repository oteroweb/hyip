<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Website</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Common Settings", array('controller'=>'sitesetting', "action"=>"antibrute"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("2-Step Verification Settings", array('controller'=>'sitesetting', "action"=>"googleauthentication"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Password Settings", array('controller'=>'sitesetting', "action"=>"passwordsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Website Security", array('controller'=>'sitesetting', "action"=>"websitesecurity"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Captcha Settings", array('controller'=>'sitesetting', "action"=>"captcha"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Security_Settings#Captcha_Settings" target="_blank">Help</a></div>
    <div id="UpdateMessage"></div>
        <?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'captchaupdate')));?>
        <?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
			<div class="tab-pane" id="profile">
				<div class="frommain">
					<div class="fromnewtext">Captcha Characters :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('captchacharacters', array('type'=>'text', 'value'=>$SITECONFIG["captchacharacters"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					
	
					<div class="fromnewtext">Captcha Character Length :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('captchacharlenth', array('type'=>'text', 'value'=>$SITECONFIG["captchacharlenth"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					
	
					<div class="fromnewtext">Captcha Text Color :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('captchatextcolor', array('type'=>'text', 'value'=>$SITECONFIG["captchatextcolor"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					
					
					<div class="fromnewtext">Captcha Background Image :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('captchaimage', array('type'=>'text', 'value'=>$SITECONFIG["captchaimage"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					
	
	
					<div class="fromnewtext">Captcha Font File :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('captchafontfile', array('type'=>'text', 'value'=>$SITECONFIG["captchafontfile"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					
	
					<div class="fromnewtext">Enable Captcha For : </div>
					<div class="fromborderdropedown3" style="padding: 4px;">
						<?php $selected = @explode(",",$SITECONFIG["captchaenablefor"]);
						echo $this->Form->input('captchaenablefor', array('type' => 'select', 'div'=>true, 'label'=>false, 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $enablecaptchafor));?>
					</div>
					
	
				<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_captchaupdate',$SubadminAccessArray)){ ?>
					<div class="formbutton">
						<?php echo $this->Js->submit('Update', array(
							'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'update'=>'#UpdateMessage',
							'class'=>'btnorange',
							'div'=>false,
							'controller'=>'Sitesetting',
							'action'=>'captchaupdate',
							'url'   => array('controller' => 'sitesetting', 'action' => 'captchaupdate')
						));?>
					</div>
				<?php } ?>
				</div>
			</div>
		<?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>