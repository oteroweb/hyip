<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 20-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Membership Settings</div>
<div id="settingpage">
<?php }?>

<div class="helpicon"><a href="https://www.proxscripts.com/docs/Membership_Settings" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>


<div class="backgroundwhite">
				
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'membershipplanaddaction')));?>
		<?php if(isset($referralcommissionplandata['Membership']["id"])){
			echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$referralcommissionplandata['Membership']["id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}?>
		
			<div class="frommain">
				<div class="fromnewtext">Status :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
							echo $this->Form->input('status', array(
								'type' => 'select',
								'options' => array('1'=>'Active', '0'=>'Inactive'),
								'selected' => $referralcommissionplandata['Membership']["status"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));
						?>
						</label>
					</div>
				</div>
				

				<div class="fromnewtext">Purchase Status :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
							echo $this->Form->input('allow_new_purchase', array(
								'type' => 'select',
								'options' => array('1'=>'Active', '0'=>'Inactive'),
								'selected' => $referralcommissionplandata['Membership']["allow_new_purchase"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));
						?>
						</label>
					</div>
				</div>
				

				<div class="fromnewtext">Hide? :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
							echo $this->Form->input('ishide', array(
								'type' => 'select',
								'options' => array('0'=>'No', '1'=>'Yes'),
								'selected' => $referralcommissionplandata['Membership']["ishide"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));
						?>
						</label>
					</div>
				</div>
				
				<div class="fromnewtext">Allow Coupons on Purchase :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>	
						<?php 
						echo $this->Form->input('iscoupon', array(
							'type' => 'select',
							'options' => array('1'=>'Yes', '0'=>'No'),
							'selected' => $referralcommissionplandata['Membership']["iscoupon"],
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));
					      ?>
					      </label>
				        </div>
				</div>
				

				<div class="fromnewtext">Membership Name : <span class="red-color">*</span> </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('membership_name', array('type'=>'text', 'value'=>$referralcommissionplandata['Membership']["membership_name"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>

				<div class="fromnewtext">Membership Price ($) :<span class="red-color">*</span>  </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('price', array('type'=>'text', 'value'=>$referralcommissionplandata['Membership']["price"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
				

				<div class="fromnewtext">Membership Payment Type :<span class="red-color">*</span>  </div>
				
				<div class="textleft">
						<div class="fromborderdropedown5 ">
							<div class="select-main">
								<label>
								<?php 
									echo $this->Form->input('membership_fee_type', array(
										'type' => 'select',
										'options' => array('One Time'=>'One Time', 'Weekly'=>'Weekly', 'Monthly'=>'Monthly', 'Yearly'=>'Yearly', 'Days'=>'Days'),
										'selected' => $referralcommissionplandata['Membership']["membership_fee_type"],
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => '',
										'onchange'=>'if(this.value=="Days"){$(".membership_fee_value").show(500);}else{$(".membership_fee_value").hide(500);}if(this.value=="One Time"){$(".mprenew").hide(500);}else{$(".mprenew").show(500);}'
									));
								?>
								</label>
							</div>
						</div>
						<div class="fromborderdropedown5 membership_fee_value" style="display:<?php if($referralcommissionplandata['Membership']["membership_fee_type"]=='Days'){echo '';}else{echo 'none';}?>;">
							<div class="daystexthaf">Days :</div>
							<?php echo $this->Form->input('membership_fee_value', array('type'=>'text', 'value'=>$referralcommissionplandata['Membership']["membership_fee_value"], 'label' => false, 'div' => false, 'class'=>'fromboxbghalf borderwidth', 'style'=>''));?>
						</div>
						
				</div>
				<div class="fromnewtext mprenew" style="display:<?php if($referralcommissionplandata['Membership']["membership_fee_type"]!='One Time'){echo '';}else{echo 'none';}?>;">Allow plan(s) when Membership Renewe : </div>
				<div class="fromborderdropedown3 mprenew" style="display:<?php if($referralcommissionplandata['Membership']["membership_fee_type"]!='One Time'){echo '';}else{echo 'none';}?>;">
					<div class="select-main">
						<label>
						<?php 
							echo $this->Form->input('fpfreeplan', array(
								'type' => 'select',
								'options' => array('1'=>'Yes', '0'=>'No'),
								'selected' => $referralcommissionplandata['Membership']["fpfreeplan"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));
						?>
						</label>
					</div>
				</div>
				
			<?php if(strpos($SITECONFIG["banneradplansetting"],'isenable|1') !== false){ ?>
				<div class="fromnewtext">Banner Ad Plan :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
							<?php
							$bannerads[0]="Select Banner Ad Plan";
							ksort($bannerads);
							echo $this->Form->input('banner_credit', array(
										'type' => 'select',
										'options' => $bannerads,
										'selected' => $referralcommissionplandata['Membership']["banner_credit"],
										'class'=>'',
										'label' => false,
										'div' => false,
										'style' => ''
							));?>
						</label>
					</div>
				</div>
				
			<?php } else {
				echo $this->Form->input('banner_credit', array('type'=>'hidden', 'value'=>0, 'label' => false));
			} ?>
			<?php if(strpos($SITECONFIG["textadplansetting"],'isenable|1') !== false){ ?>
				<div class="fromnewtext">Text Ad Plan :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
							<?php
							$ptextadplans[0]="Select Text Ad Plan";
							ksort($ptextadplans);
							echo $this->Form->input('txtadd_credit', array(
								'type' => 'select',
								'options' => $ptextadplans,
								'selected' => $referralcommissionplandata['Membership']["txtadd_credit"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));?>
						</label>
					</div>
				</div>
				

			<?php } else {
				echo $this->Form->input('txtadd_credit', array('type'=>'hidden', 'value'=>0, 'label' => false));
			} ?>
			<?php if(strpos($SITECONFIG["soloadplansetting"],'isenable|1') !== false && $SITECONFIG["enable_soloads"]==1){ ?>

				<div class="fromnewtext">Solo Ad Plan :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php
						$soloadplans[0]="Select Solo Ad Plan";
						ksort($soloadplans);
						echo $this->Form->input('soloads_credit', array(
									'type' => 'select',
									'options' => $soloadplans,
									'selected' => $referralcommissionplandata['Membership']["soloads_credit"],
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => ''
						));?>
						</label>
					</div>
				</div>
				

			<?php } else {
				echo $this->Form->input('soloads_credit', array('type'=>'hidden', 'value'=>0, 'label' => false));
			} ?>
			<?php if(strpos($SITECONFIG["ppcsetting"],'isenable|1') !== false){ ?>
				<div class="fromnewtext">PPC Plan :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php
						$ppcplans[0]="Select PPC Plan";
						ksort($ppcplans);
						echo $this->Form->input('ppc', array(
									'type' => 'select',
									'options' => $ppcplans,
									'selected' => @$referralcommissionplandata['Membership']["ppc"],
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => ''
						));?>
						</label>
					</div>
				</div>
				

			<?php } else {
				echo $this->Form->input('ppc', array('type'=>'hidden', 'value'=>0, 'label' => false));
			} ?>
			<?php if(strpos($SITECONFIG["ptcsetting"],'isenable|1') !== false){ ?>
				<div class="fromnewtext">PTC Plan :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php
						$ptcplans[0]="Select PTC Plan";
						ksort($ptcplans);
						echo $this->Form->input('ptc', array(
									'type' => 'select',
									'options' => $ptcplans,
									'selected' => @$referralcommissionplandata['Membership']["ptc"],
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => ''
						));?>
						</label>
					</div>
				</div>
				
			<?php } else {
				echo $this->Form->input('ptc', array('type'=>'hidden', 'value'=>0, 'label' => false));
			} ?>
			<?php if(strpos($SITECONFIG["loginadsetting"],'isenable|1') !== false){ ?>

				<div class="fromnewtext">Login Ad Plan :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php
						$loginads[0]="Select Login Ad Plan";
						ksort($loginads);
						echo $this->Form->input('loginad', array(
									'type' => 'select',
									'options' => $loginads,
									'selected' => @$referralcommissionplandata['Membership']["loginad"],
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => ''
						));?>
						</label>
					</div>
				</div>
			
			<?php } else {
				echo $this->Form->input('loginad', array('type'=>'hidden', 'value'=>0, 'label' => false));
			} ?>
			<?php if(strpos($SITECONFIG["bizdirectorysetting"],'enablesurfing|1') !== false){ ?>

				<div class="fromnewtext">Biz Directory Plan : </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php
						$directorys[0]="Select Biz Directory Plan";
						ksort($directorys);
						echo $this->Form->input('bizdirectory', array(
							'type' => 'select',
							'options' => $directorys,
							'selected' => @$referralcommissionplandata['Membership']["bizdirectory"],
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));?>
						</label>
					</div>
				</div>
			<?php } else {
				echo $this->Form->input('bizdirectory', array('type'=>'hidden', 'value'=>0, 'label' => false));
			} ?>
			<?php if(strpos($SITECONFIG["trafficsetting"],'enablewebcredit|1') !== false){ ?>
				<div class="fromnewtext">Website Credit Plan : </div>
				<div class="fromborderdropedown3">
				  <div class="select-main">
					  <label>
						<?php
						$Webcredits[0]="Select Website Credit Plan";
						ksort($Webcredits);
						echo $this->Form->input('webcredits', array(
							'type' => 'select',
							'options' => $Webcredits,
							'selected' => @$referralcommissionplandata['Membership']["webcredits"],
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));?>
					  </label>
				  </div>
				</div>
				
				<?php } else {
					echo $this->Form->input('webcredits', array('type'=>'hidden', 'value'=>0, 'label' => false));
				} ?>
				<div class="fromnewtext">Pay Commission For The First Membership Fee Payment Only : </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
							  echo $this->Form->input('refferal_commission_first_memfee', array(
								  'type' => 'select',
								  'options' => array('1'=>'Yes', '0'=>'No'),
								  'selected' => @$referralcommissionplandata['Membership']["refferal_commission_first_memfee"],
								  'class'=>'',
								  'label' => false,
								  'div' => false,
								  'style' => ''
							  ));
						?>
						</label>
					</div>
				</div>
				
				
				<div class="fromnewtext">Referral Commission Re-purchase/Compound Strategy (%) :<span class="red-color">*</span> </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('commrepurchase', array('type'=>'text', 'value'=>$referralcommissionplandata['Membership']["commrepurchase"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>

			<?php $commissionlevel=@explode(',',$referralcommissionplandata['Membership']["commissionlevel"]); ?>
				<div class="fromnewtext">Referral Commission on Membership Fees (%) :<span class="red-color">*</span> </div>
				<div class="fromborderdropedown3">
					<div class="fromborder4left">
						<div class="levalwhitfromtext">Level 1 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 2 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 3 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 4 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 5 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 6 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 7 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[6], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 8 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[7], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 9 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[8], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 10 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('commissionlevel.', array('type'=>'text', 'value'=>@$commissionlevel[9], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
					</div>
					<div class="tooltipfronright">
						
				   </div>
				   <div class="clearboth"></div>
				</div>
				
			<?php $addcommissionlevel=@explode(',',$referralcommissionplandata['Membership']["addcommissionlevel"]); ?>
				<div class="fromnewtext">Additional Commission on Purchases (%) :<span class="red-color">*</span> </div>
				<div class="fromborderdropedown3">
					<div class="fromborder4left">
						<div class="levalwhitfromtext">Level 1 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 2 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 3 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 4 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 5 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 6 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 7 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[6], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 8 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[7], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 9 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[8], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 10 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('addcommissionlevel.', array('type'=>'text', 'value'=>@$addcommissionlevel[9], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
					</div>
					<div class="tooltipfronright">
						
				   </div>
				   <div class="clearboth"></div>
				</div>
			<?php $add_commission_on_fees=@explode(',',$referralcommissionplandata['Membership']["add_commission_on_fees"]); ?>
				<div class="fromnewtext">Additional Commission on Membership Fees (%):<span class="red-color">*</span> </div>
				<div class="fromborderdropedown3">
					<div class="fromborder4left">
						<div class="levalwhitfromtext">Level 1 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('add_commission_on_fees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 2 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('add_commission_on_fees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 3 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('add_commission_on_fees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 4 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('add_commission_on_fees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 5 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('add_commission_on_fees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 6 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('add_commission_on_fees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 7 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('add_commission_on_fees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[6], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 8 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('add_commission_on_fees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[7], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 9 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('add_commission_on_fees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[8], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
						<div class="levalwhitfromtext">Level 10 : </div>
						<div class="levalwhitfrombox"><?php echo $this->Form->input('add_commission_on_fees.', array('type'=>'text', 'value'=>@$add_commission_on_fees[9], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''));?></div>
					</div>
					<div class="tooltipfronright">
						
				   </div>
				   <div class="clearboth"></div>
				</div>
				
				<div <?php if($SITECONFIG['balance_type']==1){echo 'style="display: none"';}?>>
				<div class="fromnewtext">Earnings Processor Preference : </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
							echo $this->Form->input('earning_on', array(
								'type' => 'select',
								'options' => array('0'=>'Purchase Processor', '1'=>"Proirity Processor"),
								'selected' => $referralcommissionplandata['Membership']["earning_on"],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));
						?>
						</label>
					</div>
				</div>
				</div>
				
				<div class="fromnewtext">Payment Method :<span class="red-color">*</span>  </div>
				<div class="fromborderdropedown3" style="padding: 4px;">
					<?php
					$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning', 'commission'=>'Commission', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase', 'ca:ea'=>'Cash + Earning', 'ca:co'=>'Cash + Commission', 're:ea'=>'Re-purchase + Earning', 're:co'=>'Re-purchase + Commission', 'ea:co'=>'Earning + Commission', 'ca:re:ea'=>'Cash + Re-purchase + Earning', 're:ea:co'=>'Re-purchase + Earning + Commission', 'ea:co:ca'=>'Earning + Commission + Cash', 'co:ca:re'=>'Commission + Cash + Re-purchase', 'ca:ea:re:co'=>'Cash + Re-purchase + Earning + Commission');
						if($SITECONFIG["wallet_for_commission"] == 'cash' && $SITECONFIG["wallet_for_earning"] == 'cash')
						{
							$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase');
						}
						elseif($SITECONFIG["wallet_for_earning"] == 'cash')
						{
							$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'commission'=>'Commission', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase', 'ca:co'=>'Cash + Commission', 're:co'=>'Re-purchase + Commission', 'co:ca:re'=>'Commission + Cash + Re-purchase');
						}
						elseif($SITECONFIG["wallet_for_commission"] == 'cash')
						{
							$pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning', 'processor'=>'Processor', 'ca:re'=>'Cash + Re-Purchase', 'ca:ea'=>'Cash + Earning', 're:ea'=>'Re-purchase + Earning', 'ca:re:ea'=>'Cash + Re-purchase + Earning');
						}
						$selected = @explode(",",$referralcommissionplandata['Membership']["paymentmethod"]);
						echo $this->Form->input('paymentmethod', array('type' => 'select', 'div'=>false, 'label'=>false, 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $pmethod));?>
				</div>
				

				<div class="fromnewtext">Allowed Payment Processors :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3" style="padding: 4px;">
					<?php 
					$selected = @explode(",",$referralcommissionplandata['Membership']["paymentprocessors"]);
					echo $this->Form->input('paymentprocessors', array('type' => 'select', 'div'=>false, 'label'=>false, 'class'=>'', 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $paymentprocessors));?>
				</div>
				

			<?php if(isset($referralcommissionplandata['Membership']["id"])){?>
				<div class="fromnewtext">Link :</div>
				<div class="fromborderdropedown3">
					<input type="text" class="fromboxbg" value="<?php echo $SITEURL;?>member/purchasemembership/<?php echo $referralcommissionplandata['Membership']["id"];?>" />
				</div>
				

			<?php }?>
				<div class="formbutton">
					<?php echo $this->Js->submit('Submit', array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage',
					  'class'=>'btnorange',
					  'div'=>false,
					  'controller'=>'sitesetting',
					  'action'=>'membershipplanaddaction',
					  'url'   => array('controller' => 'sitesetting', 'action' => 'membershipplanaddaction')
					));?>
					<?php echo $this->Js->link("Back", array('controller'=>'sitesetting', "action"=>"membership"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'btngray',
						'div'=>false
					));?>
				</div>
			</div>
		
		<?php echo $this->Form->end();?>
</div>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>