<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 31-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Balance Transfer Settings</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Member to Member Transfer", array('controller'=>'sitesetting', "action"=>"balancetransfer"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Internal Transfer", array('controller'=>'sitesetting', "action"=>"balancetransferprocessor"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
	  </div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Balance_Transfer_Settings#Internal_Transfer" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<?php echo $this->Form->create('Sitesetting',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'balancetransferaction')));?>
	<?php
	echo $this->Form->input('transfertype', array('type'=>'hidden', 'value'=>2, 'label' => false));
	echo $this->Form->input('defaultstring', array('type'=>'hidden', 'value'=>$defaultstring, 'label' => false));
	?>

	
	  <div class="frommain">
	  <?php //processer to processer start ?>	

		<div class="fromnewtext">Enable Internal Balance Transfer :  </div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
				<?php 
				  echo $this->Form->input('isenablepro', array(
					  'type' => 'select',
					  'options' => array('1'=>'Yes', '0'=>'No'),
					  'selected' => $isenablepro,
					  'class'=>'',
					  'label' => false,
					  'div' => false,
					  'style' => '',
					  'onchange'=>'if($(this).val()==1){$(".enadis").show(500);}else{$(".enadis").hide(500);}$(".tablegridheader").each(function(index, element){ if(!$(this).hasClass("nofixheader")) { fixhead[index]=[755.683349609375, $(this)]; } });'
				  ));
				?>
				</label>
			</div>
		</div>
		
		<div class="enadis" style="<?php if($isenablepro==0){ echo 'display:none;';} ?>">
		<div class="fromnewtext">Auto Approval :  </div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
				<?php 
				  echo $this->Form->input('proapprove', array(
					  'type' => 'select',
					  'options' => array('0'=>'Yes', '1'=>'No'),
					  'selected' => $proapprove,
					  'class'=>'',
					  'label' => false,
					  'div' => false,
					  'style' => ''
				  ));
				?>
				</label>
			</div>
		</div>
		

		<div class="fromnewtext" style="display:<?php echo ($SITECONFIG['balance_type']==1)? 'none':""; ?>">To Processor : </div>
		<div class="fromborderdropedown3" style="display:<?php echo ($SITECONFIG['balance_type']==1)? 'none':""; ?>">
			<div class="select-main">
				<label>
				<?php 
				  echo $this->Form->input('pymenttoprocessor', array(
					  'type' => 'select',
					  'options' => array('1'=>'Yes', '0'=>'No'),
					  'selected' => $pymenttoprocessor,
					  'class'=>'',
					  'label' => false,
					  'div' => false,
					  'style' => ''
				  ));
				?>
				</label>
			</div>
		</div>
		
		    <div class="fromnewtext">Admin Fees :<span class="red-color">*</span></div>
		    <div class="fromborderdropedown5">
			    <div class="select-main">
				    <label>
				    <?php 
				      echo $this->Form->input('profeestype', array(
					      'type' => 'select',
					      'options' => array('1'=>'Percentage (%)', '0'=>'Dollar ($)'),
					      'selected' => $profeestype,
					      'class'=>'',
					      'label' => false,
					      'div' => false,
					      'style' => ''
				      ));
				    ?>
				    </label>
			    </div>
		    </div>
		<div class="fromborderdropedown5">
			<?php echo $this->Form->input('proadminfees', array('type'=>'text', 'value'=>$proadminfees, 'label' => false, 'div' => false, 'class'=>'fromboxbghalf borderwidth','style' => ''));?>
		</div>
		    
		    
		    <div class="fromnewtext" style="display:<?php echo ($SITECONFIG['balance_type']==2)? 'none':""; ?>">Minimum Balance Transfer Limit($) :<span class="red-color">*</span></div>
		    <div class="fromborder" style="display:<?php echo ($SITECONFIG['balance_type']==2)? 'none':""; ?>">
			    <?php echo $this->Form->input('minbalancelimitpro', array('type'=>'text', 'value'=>$minbalancelimitpro, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>			
		    </div>
		 
		    <div class="fromnewtext" style="display:<?php echo ($SITECONFIG['balance_type']==2)? 'none':""; ?>">Maximum Balance Transfer Limit($) :<span class="red-color">*</span></div>
		    <div class="fromborder" style="display:<?php echo ($SITECONFIG['balance_type']==2)? 'none':""; ?>">
			    <?php echo $this->Form->input('balancelimitpro', array('type'=>'text', 'value'=>$balancelimitpro, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		    </div>
		    
		<div class="fromnewtext">From Balance Type :<span class="red-color">*</span>  </div>
		<div class="fromborderdropedown3" style="padding: 4px;">
			<?php
			      $pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning', 'commission'=>'Commission');
			      if($SITECONFIG["wallet_for_commission"] == 'cash' && $SITECONFIG["wallet_for_earning"] == 'cash')
			      {
				      $pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase');
			      }
			      elseif($SITECONFIG["wallet_for_earning"] == 'cash')
			      {
				      $pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'commission'=>'Commission');
			      }
			      elseif($SITECONFIG["wallet_for_commission"] == 'cash')
			      {
				      $pmethod=array('cash'=>'Cash', 'repurchase'=>'Re-purchase', 'earning'=>'Earning');
			      }
				$selected = @explode(",",$frompaymentmethod);
				echo $this->Form->input('frompaymentmethod', array('type' => 'select', 'div'=>false, 'label'=>false, 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $pmethod));?>
		</div>
		
		
		<div class="fromnewtext">To Balance Type :<span class="red-color">*</span>  </div>
		<div class="fromborderdropedown3" style="padding: 4px;">
			<?php
				$selected = @explode(",",$topaymentmethod);
				echo $this->Form->input('topaymentmethod', array('type' => 'select', 'div'=>false, 'label'=>false, 'multiple' => 'checkbox', 'selected' => $selected, 'options' => $pmethod));?>
		</div>
		
		
		<div class="fromnewtext" style="display:<?php echo ($SITECONFIG['balance_type']==1)? 'none':""; ?>">Various Options :<span class="red-color">*</span> </div>
	    <div class="fromborderdropedown3" style="display:<?php echo ($SITECONFIG['balance_type']==1)? 'none':""; ?>">
	    <input type="text" class="fromboxbg" readonly="readonly" value="Check The Instructions in The Next Help Icon Before Following Doing Settings." /></div>
	    
	  </div>
	  </div>
	  
	  <div class="enadis" style="<?php if($isenablepro==0){ echo 'display:none;';} ?>">
	  <div class="tablegrid tablegriddifferent tableforremoveheader" style="display:<?php echo ($SITECONFIG['balance_type']==1)? 'none':""; ?>">
		  <div class="tablegridheader" style="font-size:12px;">
			  <div>From\To</div>
			  <div>Reserve</div>
			  <div>Min</div>
			  <div>Max</div>
			  <?php
			  $toprocessorarray = @explode(",",$toprocessor);
			  foreach($paymentprocessors as $pid=>$pnm){
					  echo '<div><span class="checkbox">';
					  echo $pnm." ";
					  echo $this->Form->input('toprocessor.', array('type' => 'checkbox', 'id'=>'topp'.$pid, 'div'=>false, 'label'=>false, 'hiddenField'=>false, 'checked' => (in_array($pid, $toprocessorarray)?'checked':''), 'value' => $pid));
					  echo '<label for="topp'.$pid.'"></label></span></div>';
			  }
			  ?>
			</div>
		  <?php
		  $fromprocessorarray = @explode(",",$fromprocessor);
		  $processorfee=@explode("|", $processorfee);
		  $processorlimit=@explode("|", $processorlimit);
		  $minlimit=@explode("|", $minlimit);
		  $maxlimit=@explode("|", $maxlimit);
		  $pcounter=0;
		  $i=0;
		  foreach($paymentprocessors as $pid=>$pnm){
		  ?>
			  <div class="tablegridrow">
				  <div class="checkbox">
					  <?php
					  echo $this->Form->input('fromprocessor.', array('type' => 'checkbox', 'id'=>'leftp'.$pid, 'div'=>false, 'label'=>false, 'hiddenField'=>false, 'checked' => (in_array($pid, $fromprocessorarray)?'checked':''), 'value' => $pid));
					  echo ' '.$pnm;
					  echo '<label for="leftp'.$pid.'"></label>';
					  ?>
				  </div>
				  <div>
					  <?php @$processorlimitdata=@explode(":",$processorlimit[$i]); ?>
					  <?php echo '<input class="fromboxbg" type="text" value="'.@$processorlimitdata[1].'" name="processorlimit['.$pid.']" style="width:50px" />'; ?>
				  </div>
				  <div>
					  <?php @$minlimitdata=@explode(":",$minlimit[$i]); ?>
					  <?php echo '<input class="fromboxbg" type="text" value="'.@$minlimitdata[1].'" name="minlimit['.$pid.']" style="width:50px" />'; ?>
				  </div>
				  <div>
					  <?php @$maxlimitdata=@explode(":",$maxlimit[$i]); ?>
					  <?php echo '<input class="fromboxbg" type="text" value="'.@$maxlimitdata[1].'" name="maxlimit['.$pid.']" style="width:50px" />'; ?>
				  </div>
			  <?php $ii=0; foreach($paymentprocessors as $ppid=>$ppnm){
				  $processorfeedata=@explode("[-]", $processorfee[$pcounter]);
				  	echo '<div><input class="fromboxbg" type="text" value="'.@$processorfeedata[1].'" name="processorfee['.$pid.'_'.$ppid.']" style="width:50px" /></div>';
					$pcounter++;
			  $ii++;} ?>
			  </div>
		  <?php $i++;}?>
	  </div>
	  </div>
	  <div class="frommain">
			
		<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_balancetransferaction', $SubadminAccessArray)){ ?>
			<div class="formbutton">
				<?php echo $this->Js->submit('Submit', array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage',
					  'class'=>'btnorange',
					  'div'=>false,
					  'controller'=>'sitesetting',
					  'action'=>'balancetransferaction',
					  'url'   => array('controller' => 'sitesetting', 'action' => 'balancetransferaction')
					));?>
			</div>
		<?php } ?>
		</div>
	
<?php echo $this->Form->end();?>

<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>