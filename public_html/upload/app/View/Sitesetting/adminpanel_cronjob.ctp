<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Website</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Main Details", array('controller'=>'sitesetting', "action"=>"index"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'class'=>'satting-menu-active'
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Member Tool Settings", array('controller'=>'sitesetting', "action"=>"shortenersetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Database Operations", array('controller'=>'sitesetting', "action"=>"maintenance"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>	
				</li>
				<li>
					<?php echo $this->Js->link("Logs Settings", array('controller'=>'sitesetting', "action"=>"logs"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Launch Date Settings", array('controller'=>'sitesetting', "action"=>"launch"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Currency Settings", array('controller'=>'sitesetting', "action"=>"currency"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Cronjob Settings", array('controller'=>'sitesetting', "action"=>"cronjob"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Notification", array('controller'=>'sitesetting', "action"=>"notification"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
	<div class="helpicon"><a href="https://www.proxscripts.com/docs/Website_Settings#Cronjob_Settings" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>
	
<div class="backgroundwhite">
			<div class="frommain">	
				<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_cronjobrun',$SubadminAccessArray)){ ?>
				<div class="formbutton">
					<div class="height10"></div>
					<input type="button" onclick="$('#cronjobsetting').fadeOut(500);$('#cronjobmenualy').delay(500).fadeIn(500);" value="Run Cronjob Manually" class="btnorange" />
				</div>
				<?php } ?>
			
						<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'cronjobupdate')));?>
						<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
							<div id="cronjobsetting">
								<div class="fromnewtext">Receive Cronjob Run Notifications :<span class="red-color">*</span>  </div>
								<div class="fromborderdropedown3">
									<div class="select-main">
									<label>
									<?php 
									  echo $this->Form->input('cron_email', array(
										  'type' => 'select',
										  'options' => array('1'=>'Yes', '0'=>'No'),
										  'selected' => $SITECONFIG["cron_email"],
										  'class'=>'',
										  'label' => false,
										  'div' => false,
										  'style' => ''
									  ));
									?>
									</label>
									</div>
								</div>
								
								<div class="formbutton">
									<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_cronjobupdate',$SubadminAccessArray)){ ?>
										<?php echo $this->Js->submit('Update', array(
											'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'update'=>'#UpdateMessage',
											'class'=>'btnorange',
											'controller'=>'sitesetting',
											'action'=>'cronjobupdate',
											'div'=>false,
											'url'   => array('controller' => 'sitesetting', 'action' => 'cronjobupdate')
											));?>
										<?php } ?>
								  </div>
								
						<?php echo $this->Form->end();?>
					</div>	
					<div id="cronjobmenualy" style="display:none;">
						<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'cronjobrun')));?>
						<?php 
						//echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));
						echo $this->Form->input('member_id', array('type'=>'hidden', 'value'=>1, 'label' => false));
						?>
							<div class="fromnewtext">Select Cronjob :</div>
							<div class="fromborderdropedown3">
								<div class="select-main">
								<label>
								<?php 
									  echo $this->Form->input('cronjobfile', array(
										  'type' => 'select',
										  'options' => $cronjobs,
										  'selected' => '',
										  'class'=>'',
										  'label' => false,
										  'div' => false,
										  'style' => ''
									  ));
								?>
								</label>
								</div>
							</div>
							<div class="fromnewtext">Admin Password : <span class="red-color">*</span> </div>
							<div class="fromborderdropedown3">
								<?php echo $this->Form->input('current_pwd', array('type'=>'password', 'value'=>'', 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
							</div>
							<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_cronjobrun',$SubadminAccessArray)){ ?>
							<div class="formbutton">
								<?php echo $this->Js->submit('Submit', array(
									  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
									  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									  'update'=>'#UpdateMessage',
									  'class'=>'btnorange',
									  'controller'=>'sitesetting',
									  'div'=>false,
									  'action'=>'cronjobrun',
									  'url'   => array('controller' => 'sitesetting', 'action' => 'cronjobrun')
									));?>
									<input type="button" onclick="$('#cronjobmenualy').fadeOut(500);$('#cronjobsetting').delay(500).fadeIn(500);" value="Back" class="btngray" />
							</div>
							<?php } ?>
							<?php echo $this->Form->end();?>
				  </div>
			</div>
</div>	
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>