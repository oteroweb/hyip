<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Signup Settings</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	    <ul class="nav nav-tabs" id="myTab">
            <li>
                <?php echo $this->Js->link("Registration Form Settings", array('controller'=>'sitesetting', "action"=>"registrationform"), array(
                    'update'=>'#settingpage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                ));?>
            </li>
            <li>
                <?php echo $this->Js->link("Profile Form Settings", array('controller'=>'sitesetting', "action"=>"profileform"), array(
                    'update'=>'#settingpage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
                ));?>
            </li>
            <li class="active">
                <?php echo $this->Js->link("Free Credit & Bonus", array('controller'=>'sitesetting', "action"=>"freecreditandbonus"), array(
                    'update'=>'#settingpage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
                ));?>
            </li>
            <li>
                <?php echo $this->Js->link("Signup Settings", array('controller'=>'sitesetting', "action"=>"signup"), array(
                    'update'=>'#settingpage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
                ));?>
            </li>
            <li>
                <?php echo $this->Js->link("Facebook & Google Settings", array('controller'=>'sitesetting', "action"=>"facebookgoogle"), array(
                    'update'=>'#settingpage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
                ));?>
            </li>
        </ul>
	</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>

<div class="helpicon"><a href="https://www.proxscripts.com/docs/Signup_Settings#Free_Credit_.26_Bonus" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

        <?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'freecreditandbonusupdate')));?>
        <?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
		
			<div class="frommain">
				<?php if(strpos($SITECONFIG["banneradplansetting"],'isenable|1') !== false){ ?>
					<div class="fromnewtext">Banner Ad Plan : </div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
							<?php
							$bannerads[0]="Select Banner Ad Plan";
							ksort($bannerads);
							echo $this->Form->input('banner', array(
								'type' => 'select',
								'options' => $bannerads,
								'selected' => $BannerAd,
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));?>
							</label>
						</div>
					</div>
				<?php } else {
					echo $this->Form->input('banner_credit', array('type'=>'hidden', 'value'=>0, 'label' => false));
				} ?>
			<?php if(strpos($SITECONFIG["textadplansetting"],'isenable|1') !== false){ ?>

				<div class="fromnewtext">Text Ad Plan : </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php
						$ptextadplans[0]="Select Text Ad Plan";
						ksort($ptextadplans);
						echo $this->Form->input('txtadd', array(
							'type' => 'select',
							'options' => $ptextadplans,
							'selected' => $TextAd,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));?>
						</label>
					</div>
				</div>
			<?php } else {
				echo $this->Form->input('txtadd_credit', array('type'=>'hidden', 'value'=>0, 'label' => false));
			} ?>
			<?php if(strpos($SITECONFIG["soloadplansetting"],'isenable|1') !== false && $SITECONFIG["enable_soloads"]==1){ ?>

				<div class="fromnewtext">Solo Ad Plan : </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php
						$soloadplans[0]="Select Solo Ad Plan";
						ksort($soloadplans);
						echo $this->Form->input('solo', array(
							'type' => 'select',
							'options' => $soloadplans,
							'selected' => $SoloAd,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));?>
						</label>
					</div>
				</div>
			<?php } else {
				echo $this->Form->input('soloads_credit', array('type'=>'hidden', 'value'=>0, 'label' => false));
			} ?>
			<?php if(strpos($SITECONFIG["ppcsetting"],'isenable|1') !== false){ ?>

				<div class="fromnewtext">PPC Plan : </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php
						$ppcplans[0]="Select PPC Plan";
						ksort($ppcplans);
						echo $this->Form->input('ppc', array(
							'type' => 'select',
							'options' => $ppcplans,
							'selected' => $Ppc,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));?>
						</label>
					</div>
				</div>
			<?php } else {
				echo $this->Form->input('ppc', array('type'=>'hidden', 'value'=>0, 'label' => false));
			} ?>
			<?php if(strpos($SITECONFIG["ptcsetting"],'isenable|1') !== false){ ?>
				<div class="fromnewtext">PTC Plan : </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php
						$ptcplans[0]="Select PTC Plan";
						ksort($ptcplans);
						echo $this->Form->input('ptc', array(
							'type' => 'select',
							'options' => $ptcplans,
							'selected' => $Ptc,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));?>
						</label>
					</div>
				</div>
			<?php } else {
				echo $this->Form->input('ptc', array('type'=>'hidden', 'value'=>0, 'label' => false));
			} ?>
			<?php if(strpos($SITECONFIG["loginadsetting"],'isenable|1') !== false){ ?>

				<div class="fromnewtext">Login Ad Plan : </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php
						$loginads[0]="Select Login Ad Plan";
						ksort($loginads);
						echo $this->Form->input('loginad', array(
							'type' => 'select',
							'options' => $loginads,
							'selected' => $LoginAd,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));?>
						</label>
					</div>
				</div>
			<?php } else {
				echo $this->Form->input('loginad', array('type'=>'hidden', 'value'=>0, 'label' => false));
			} ?>
			<?php if(strpos($SITECONFIG["bizdirectorysetting"],'enablesurfing|1') !== false){ ?>

				<div class="fromnewtext">Biz Directory Plan : </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php
						$directorys[0]="Select Biz Directory Plan";
						ksort($directorys);
						echo $this->Form->input('bizdirectory', array(
							'type' => 'select',
							'options' => $directorys,
							'selected' => $BizDirectory,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));?>
						</label>
					</div>
				</div>
			<?php } else {
				echo $this->Form->input('bizdirectory', array('type'=>'hidden', 'value'=>0, 'label' => false));
			} ?>
				<?php if(strpos($SITECONFIG["trafficsetting"],'enablewebcredit|1') !== false){ ?>
				<div class="fromnewtext">Website Credit Plan : </div>
				<div class="fromborderdropedown3">
				  <div class="select-main">
					  <label>
						<?php
						$Webcredits[0]="Select Website Credit Plan";
						ksort($Webcredits);
						echo $this->Form->input('webcredits', array(
							'type' => 'select',
							'options' => $Webcredits,
							'selected' => $Webcredit,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));?>
					  </label>
				  </div>
				</div>
				
				<?php } else {
					echo $this->Form->input('webcredits', array('type'=>'hidden', 'value'=>0, 'label' => false));
				} ?>
				<div class="fromnewtext">Start Up Bonus ($) : </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('startupbonus', array('type'=>'text', 'value'=>$StartUpBonus, 'label' => false, 'div' => false, 'class'=>'fromboxbghalf','style' => ''));?>
					<div class="select-main2">
						<div class="select-main">
							<label>	
							<?php
							$pmethod=array(0=>'Cash Balance', 1=>'Repurchase Balance', 2=>'Earning Balance');
							if($SITECONFIG["wallet_for_earning"] == 'cash')
							{
								$pmethod=array(0=>'Cash Balance', 1=>'Repurchase Balance');
							}
							echo $this->Form->input('startupbonusbalancetype', array(
								'type' => 'select',
								'options' => $pmethod,
								'selected' => $StartUpBonusBalanceType,
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));?>
							</label>
						</div>
					</div>
				</div>
				

				<div class="fromgreybox">
					<div class="paidetext">Bonus Criteria 1 - Get</div>
					<div class="paidfrombox"><?php echo $this->Form->input('startupbonusreferral', array('type'=>'text', 'value'=>$Referrals, 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>'')); ?></div>
					<div class="paiddrop">
						<div class="select-main4">
							<div class="select-main">
								<label>
								<?php
								echo $this->Form->input('startupbonusreferraltype', array(
									  'type' => 'select',
									  'options' => array('0'=>'Select Referral Type', '1'=>'Paid', '2'=>'Unpaid'),
									  'selected' => $ReferralsType,
									  'class'=>'',
									  'label' => false,
									  'div' => false,
									  'style'=>''
								));?>
								</label>
							</div>
						</div>
					</div>
					<div class="paidfrombox" style="padding-top:10px;height:27px"><span class="fromboxbg4">Referrals</span></div>
				</div>
				<div class="fromgreybox">
					<div class="paidetext">Bonus Criteria 2 - Get</div>
					<div class="paidfrombox"><?php echo $this->Form->input('startupbonusposition', array('type'=>'text', 'value'=>$Positions, 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>'')); ?></div>
					<div class="paiddrop">
						<div class="select-main4">
							<div class="select-main">
								<label>
								<?php
								echo $this->Form->input('startupbonuspositiontype', array(
									'type' => 'select',
									'options' => array('0'=>'Select Position Type', '1'=>'Completed', '2'=>'New'),
									'selected' => $PositionsType,
									'class'=>'',
									'label' => false,
									'div' => false,
									'style'=>''
								));?>
								</label>
							</div>
						</div>
					</div>
					<div class="paidfrombox" style="padding-top:10px;height:27px"><span class="fromboxbg4">Referrals</span></div>
				</div>
            
				<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_freecreditandbonusupdate',$SubadminAccessArray)){ ?>
					<div class="formbutton">
						<?php echo $this->Js->submit('Update', array(
						  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage',
						  'class'=>'btnorange',
						  'div'=>false,	
						  'controller'=>'Sitesetting',
						  'action'=>'freecreditandbonusupdate',
						  'url'   => array('controller' => 'sitesetting', 'action' => 'freecreditandbonusupdate')
						));?>
					</div>
				<?php } ?>
			</div>
		
        <?php echo $this->Form->end();?>
<?php }else{ ?><div class="accessdenied"><?php echo "Access Denied";?></div><?php }?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>