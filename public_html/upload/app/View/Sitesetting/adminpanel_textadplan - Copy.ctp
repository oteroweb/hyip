<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 12-12-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Advertisement</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Banner Ads", array('controller'=>'sitesetting', "action"=>"advertisementcredit"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Text Ads", array('controller'=>'sitesetting', "action"=>"advertisementtextadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Solo Ads", array('controller'=>'sitesetting', "action"=>"soload"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Login Ads", array('controller'=>'sitesetting', "action"=>"loginadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC", array('controller'=>'sitesetting', "action"=>"ppcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC", array('controller'=>'sitesetting', "action"=>"ptcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Biz Directory", array('controller'=>'sitesetting', "action"=>"bizdirectorysetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Widgets", array('controller'=>'sitesetting', "action"=>"advertisementbannerwidget"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<div class="tab-innar">
	<ul>
		<li>
			<?php echo $this->Js->link("Common Settings", array('controller'=>'sitesetting', "action"=>"advertisementtextadsetting"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Credit Text Ads Settings", array('controller'=>'sitesetting', "action"=>"advertisementtextadcredit"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Plan Text Ads Settings", array('controller'=>'sitesetting', "action"=>"textadplan"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>'active'
			));?>
		</li>
	</ul>
</div>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Advertisement_Settings#Plan_Text_Ads_Settings" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="backgroundwhite">
    
<?php echo $this->Form->create('Sitesetting',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'textadplanaction')));?>
	
		<div class="frommain">			
			<div class="fromnewtext">Enable Plan Text Ads :  </div>
			<div class="fromborderdropedown">
				<div class="select-main">
					<label>
					<?php 
					  echo $this->Form->input('isenable', array(
						  'type' => 'select',
						  'options' => array('1'=>'Yes', '0'=>'No'),
						  'selected' => $isenable,
						  'class'=>'',
						  'label' => false,
						  'div' => false,
						  'style' => '',
						  'onchange'=>'if($(this).val()==1){$(".enadis").show(500);}else{$(".enadis").hide(500);}'
					  ));
					?>
					</label>
				</div>
			</div>
			<span class="tooltipmain glyphicon-question-sign"data-toggle="tooltip" data-original-title="You Can Enable/Disable The Text Ad Plans Anytime. The Text Ad Plans Can Be Purchased Only if You Have Enabled This. Plan Text Ads Will Not Be Shown on Front Side When You Disable This."></span>
			<div class="enadis" style="<?php if($isenable==0){ echo 'display:none;';} ?>">
			<div class="fromnewtext">Auto Approval :  </div>
			<div class="fromborderdropedown">
				<div class="select-main">
					<label>
					<?php 
					  echo $this->Form->input('isapprove', array(
						  'type' => 'select',
						  'options' => array('0'=>'Yes', '1'=>'No'),
						  'selected' => $isapprove,
						  'class'=>'',
						  'label' => false,
						  'div' => false,
						  'style' => ''
					  ));
					?>
					</label>
				</div>
			</div>
			<span class="tooltipmain glyphicon-question-sign"data-toggle="tooltip" data-original-title="New Text Ads Will Be Automatically Approved When This is Set to 'Yes'. You Will Have to <b>Manually Approve</b> New Ads Otherwise."></span>
			<div class="fromnewtext">Auto Approval on Edit :  </div>
			<div class="fromborderdropedown">
				<div class="select-main">
					<label>
					<?php 
					  echo $this->Form->input('iseditapprove', array(
						  'type' => 'select',
						  'options' => array('0'=>'Yes', '1'=>'No'),
						  'selected' => $iseditapprove,
						  'class'=>'',
						  'label' => false,
						  'div' => false,
						  'style' => ''
					  ));
				   ?>
					</label>
				</div>
			</div>
			<span class="tooltipmain glyphicon-question-sign"data-toggle="tooltip" data-original-title="Members Can Edit The Text Ads From Front Side. Choose Whether Those Edited Text Ads Will Be <b>Automatically Approved</b> or Not."></span>	
			</div>
			<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_textadplanaction',$SubadminAccessArray)){ ?>
				<div class="formbutton">
					<?php echo $this->Js->submit('Submit', array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage',
					  'class'=>'btnorange',
					  'div'=>false,
					  'controller'=>'sitesetting',
					  'action'=>'textadplanaction',
					  'url'   => array('controller' => 'sitesetting', 'action' => 'textadplanaction')
					));?>
				</div>
			<?php } ?>
		</div>
	
<?php echo $this->Form->end();?>
	
</div>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
<?php }?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>