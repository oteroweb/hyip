<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 20-11-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Statistics</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Public Statistics", array('controller'=>'sitesetting', "action"=>"publicstatistics"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Member Statistics", array('controller'=>'sitesetting', "action"=>"memberstatistics"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
    <div class="height10"></div>
	
	<div class="helpicon"><a href="https://www.proxscripts.com/docs/Statistics_Settings#Member_Statistics" target="_blank">Help</a></div>
	
    <div id="UpdateMessage"></div>
        <?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'memberstatisticsupdate')));?>
        <?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
		<div class="">
			<div>
				<span class="sta-text">Website Statistics</span>
				<span class="checkbox">
					<?php 
						if($memberstatistics==1){$checked="checked";}else{$checked="";}
                    	echo $this->Form->input('mchkstatistics', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
				</span>
			</div>		
			<div class="sitesetting">
				<div class="sitesettingmainbox">
					<div class="bluebox">Site Stats :</div>
					<div class="greybox">
						<div class="launchdatebox">Launch Date :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($LaunchDate==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchklaunch_dt', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>	
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"1", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation1">
						<div class="checkbox">
							Launch Date Code
							<?php 
								if($LaunchDateP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_launch_dt', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_launch_dt").show(500);}else{$(".m_launch_dt").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_launch_dt" <?php echo ($LaunchDateP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_launch_dt" <?php echo ($LaunchDateP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php if($StatisticLaunchDate!=''){ echo $this->Time->format($SITECONFIG["timeformate"], $StatisticLaunchDate); }?&gt;</textarea>
						</div>
					</div>
					
					<div class="whitebox">
						<div class="launchdatebox">Alexa Rank :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									 if($AlexaRank==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchkalexa', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"2", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation2">
						<div class="checkbox">
							Alexa Rank Code
							<?php 
								if($AlexaRankP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_alexa', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_alexa").show(500);}else{$(".m_alexa").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_alexa" <?php echo ($AlexaRankP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_alexa" <?php echo ($AlexaRankP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticAlexaRank;?&gt;</textarea>
						</div>
					</div>
					<div class="greybox">
						<div class="launchdatebox">Total Members :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($TotalMember==1){$checked="checked";}else{$checked="";}
								   echo $this->Form->input('mchktotalmember', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"3", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation3">
						<div class="checkbox">
							Total Members Code
							<?php 
								if($TotalMemberP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_totalmember', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_totalmember").show(500);}else{$(".m_totalmember").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_totalmember" <?php echo ($TotalMemberP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_totalmember" <?php echo ($TotalMemberP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticTotalMember;?&gt;</textarea>
						</div>
					</div>
					
					<div class="darkbluebox">Pay Stats :</div>
					<div class="greybox">
						<div class="launchdatebox">Deposits :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
								if($Deposits==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('mchktotaldeposit', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"4", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation4">
						<div class="checkbox">
							Deposits Code
							<?php 
								if($DepositsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_totaldeposit', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_totaldeposit").show(500);}else{$(".m_totaldeposit").hide(500);}'));?>	
						</div>
						<div class="copycodetxt m_totaldeposit" <?php echo ($DepositsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_totaldeposit" <?php echo ($DepositsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticTotalDiposit;?&gt;</textarea>
						</div>
					</div>
					
					<div class="whitebox">
						<div class="launchdatebox">Payouts :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($Payouts==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchktotpayout', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>	
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"5", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation5">
						<div class="checkbox">
							Payouts Code
							<?php 
								if($PayoutsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_totpayout', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_totpayout").show(500);}else{$(".m_totpayout").hide(500);}'));?>	
						</div>
						<div class="copycodetxt m_totpayout" <?php echo ($PayoutsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_totpayout" <?php echo ($PayoutsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticTotalPayouts;?&gt;</textarea>
						</div>
					</div>
					
					
					<div class="greybox">
						<div class="launchdatebox">Commissions :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($Commissions==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchkcommission', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>	
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"6", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation6">
						<div class="checkbox">
							Commissions Code
							<?php 
								if($CommissionsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_commission', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_commission").show(500);}else{$(".m_commission").hide(500);}'));?>	
						</div>
						<div class="copycodetxt m_commission" <?php echo ($CommissionsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_commission" <?php echo ($CommissionsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticTotalCommission;?&gt;</textarea>
						</div>
					</div>
					<div class="whitebox">
						<div class="launchdatebox">Last Payout :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($LastPayout==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchklastpayout', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>	
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"7", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation7">
						<div class="checkbox">
							Last Payout Code
							<?php 
								if($LastPayoutP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_lastpayout', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_lastpayout").show(500);}else{$(".m_lastpayout").hide(500);}'));?>	
						</div>
						<div class="copycodetxt m_lastpayout" <?php echo ($LastPayoutP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_lastpayout" <?php echo ($LastPayoutP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticTotalLastPayouts;?&gt;</textarea>
						</div>
					</div>
					<div class="bluebox">Member Stats :</div>
					<div class="greybox">
						<div class="launchdatebox">Online Members :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($OnlineMembers==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchkonlinemember', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"8", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation8">
						<div class="checkbox">
							Online Members Code
							<?php 
								if($OnlineMembersP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_onlinemember', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_onlinemember").show(500);}else{$(".m_onlinemember").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_onlinemember" <?php echo ($OnlineMembersP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_onlinemember" <?php echo ($OnlineMembersP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;div id="OnlineMembers"&gt;&lt;?php echo $OnlineMembers;?&gt;&lt;/div&gt;</textarea>
						</div>
						<div class="copycodetxt m_onlinemember" <?php echo ($OnlineMembersP != 1)? 'style="display:none;"' : ''; ?>>This Code Can Be Used <b>Only Once Per Page</b></div>
					</div>
					
					<div class="whitebox">
						<div class="launchdatebox">Online Guests :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($OnlineGuests==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchkonlineguest', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"9", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation9">
						<div class="checkbox">
							Online Guests Code
							<?php 
								if($OnlineGuestsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_onlineguest', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_onlineguest").show(500);}else{$(".m_onlineguest").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_onlineguest" <?php echo ($OnlineGuestsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_onlineguest" <?php echo ($OnlineGuestsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;div id="OnlineGuests"&gt;&lt;?php echo $OnlineGuests;?&gt;&lt;/div&gt;</textarea>
						</div>
						<div class="copycodetxt m_onlineguest" <?php echo ($OnlineGuestsP != 1)? 'style="display:none;"' : ''; ?>>This Code Can Be Used <b>Only Once Per Page</b></div>
					</div>
					
				</div>
				
				<div class="sitesettingmainbox">
					<div class="darkbluebox">My Statistics :</div>
					<div class="greybox">
						<div class="launchdatebox">Cash Balance :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
								if($MyCash==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('mchktot_cash', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"14", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation14">
						<div class="checkbox">
							My Cash Code
							<?php 
								if($MyCashP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_totalcash', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_totalcash").show(500);}else{$(".m_totalcash").hide(500);}'));?>	
						</div>
						<div class="copycodetxt m_totalcash" <?php echo ($MyCashP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_totalcash" <?php echo ($MyCashP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticMemberCash;?&gt;</textarea>
						</div>
					</div>
					
					<div class="whitebox">
						<div class="launchdatebox">Re-purchase Balance :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($RepurchaseBalance==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchktot_rpbal', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"15", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation15">
						<div class="checkbox">
							Re-purchase Balance Code
							<?php 
								if($RepurchaseBalanceP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_totalrpbal', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_totalrpbal").show(500);}else{$(".m_totalrpbal").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_totalrpbal" <?php echo ($RepurchaseBalanceP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_totalrpbal" <?php echo ($RepurchaseBalanceP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticMemberRepurchaseCash;?&gt;</textarea>
						</div>
					</div>
					
					<div class="greybox">
						<div class="launchdatebox">Earnings Balance :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($EarningBalance==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchktot_earningbal', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"19", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation19">
						<div class="checkbox">
							Earnings Balance Code
							<?php 
								if($EarningBalanceP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_totalearningbal', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_totalearningbal").show(500);}else{$(".m_totalearningbal").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_totalearningbal" <?php echo ($EarningBalanceP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_totalearningbal" <?php echo ($EarningBalanceP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticMemberEarning;?&gt;</textarea>
						</div>
					</div>
					
					<div class="whitebox">
						<div class="launchdatebox">Commission Balance :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($CommissionBalance==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchktot_commissionbal', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"20", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation20">
						<div class="checkbox">
							Commission Balance Code
							<?php 
								if($CommissionBalanceP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_totalcommissionbal', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_totalcommissionbal").show(500);}else{$(".m_totalcommissionbal").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_totalcommissionbal" <?php echo ($CommissionBalanceP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_totalcommissionbal" <?php echo ($CommissionBalanceP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticMemberCommission;?&gt;</textarea>
						</div>
					</div>
					
					<div class="greybox">
						<div class="launchdatebox">Banner Ad Credits :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($BannerAdCredits==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchktot_banner', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"16", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation16">
						<div class="checkbox">
							Banner Ad Credits Code
							<?php 
								if($BannerAdCreditsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_totalbanner', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_totalbanner").show(500);}else{$(".m_totalbanner").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_totalbanner" <?php echo ($BannerAdCreditsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_totalbanner" <?php echo ($BannerAdCreditsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticMemberBannerCredits;?&gt;</textarea>
						</div>
					</div>
					
					<div class="whitebox">
						<div class="launchdatebox">Text Ad Credits :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									 if($TextAdCredits==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchktot_text', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>	
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"17", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation17">
						<div class="checkbox">
							Text Ad Credits Code
							<?php 
								if($TextAdCreditsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_totaltext', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_totaltext").show(500);}else{$(".m_totaltext").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_totaltext" <?php echo ($TextAdCreditsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_totaltext" <?php echo ($TextAdCreditsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticMemberTextCredits;?&gt;</textarea>
						</div>
					</div>
					<div class="greybox">
						<div class="launchdatebox">Solo Ad Credits :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($SoloAdCredits==1){$checked="checked";}else{$checked="";}
								   echo $this->Form->input('mchktot_solo', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"18", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation18">
						<div class="checkbox">
							Solo Ad Credits Code
							<?php 
								if($SoloAdCreditsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_totalsolo', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_totalsolo").show(500);}else{$(".m_totalsolo").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_totalsolo" <?php echo ($SoloAdCreditsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_totalsolo" <?php echo ($SoloAdCreditsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticMemberSoloCredits;?&gt;</textarea>
						</div>
					</div>
					
					
					
					<div class="bluebox">Recently Joined :</div>
					<div class="greybox">
						<div class="launchdatebox">Recently Joined :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($RecentlyJoined==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchkrecjoined', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>	
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"10", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation10">
						<div class="checkbox">
							Recently Joined Code
							<?php 
								if($RecentlyJoinedP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_recjoined', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_recjoined").show(500);}else{$(".m_recjoined").hide(500);}'));?>	
						</div>
						<div class="copycodetxt m_recjoined" <?php echo ($RecentlyJoinedP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_recjoined" <?php echo ($RecentlyJoinedP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php foreach($StatisticRecentlyJoined as $key=>$value){ echo $value."-".$key; } ?&gt;</textarea>
						</div>
					</div>
					<div class="darkbluebox">Top Commission Earners :</div>
					<div class="greybox">
						<div class="launchdatebox">Top Commission Earners :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($TopCommissionEarners==1){$checked="checked";}else{$checked="";}
								   echo $this->Form->input('mchktopcommisionearner', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"11", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation11">
						<div class="checkbox">
							Top Commission Earners Code
							<?php 
								if($TopCommissionEarnersP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_topcommisionearner', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_topcommisionearner").show(500);}else{$(".m_topcommisionearner").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_topcommisionearner" <?php echo ($TopCommissionEarnersP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_topcommisionearner" <?php echo ($TopCommissionEarnersP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php foreach($StatisticTopCommissionEarners as $earners){ echo $earners['Member']['user_name']."-".$earners[0]['earning']; } ?&gt;</textarea>
						</div>
					</div>
					<?php  /* Ads Stats Code Remove
					<div class="bluebox">Ads Stats :</div>
					<div class="greybox">
						<div class="launchdatebox">Text Ads :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($TextAds==1){$checked="checked";}else{$checked="";}
									echo $this->Form->input('mchktext', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"12", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation12">
						<div class="checkbox">
							Text Ads Code
							<?php 
								if($TextAdsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_textads', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_textads").show(500);}else{$(".m_textads").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_textads" <?php echo ($TextAdsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_textads" <?php echo ($TextAdsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticTotalTextAds;?&gt;</textarea>
						</div>
					</div>
					
					<div class="whitebox">
						<div class="launchdatebox">Banner Ads :</div>
						<div class="launchdatebox">
							<span class="checkbox">
								<?php 
									if($BannerAds==1){$checked="checked";}else{$checked="";}
								   echo $this->Form->input('mchkbannerads', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));?>
							</span>
							<span><?php echo $this->html->image("dblarrow.png", array("alt"=>"13", 'div'=>false, 'class'=>'helpicon'));?></span>
						</div>
					</div>
					<div class="launchpopup informationboxstatistic helpinformation13">
						<div class="checkbox">
							Banner Ads Code
							<?php 
								if($BannerAdsP==1){$checked="checked";}else{$checked="";}
								echo $this->Form->input('m_bannerads', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked, 'onchange'=>'if($(this).is(":checked")){$(".m_bannerads").show(500);}else{$(".m_bannerads").hide(500);}'));?>
						</div>
						<div class="copycodetxt m_bannerads" <?php echo ($BannerAdsP != 1)? 'style="display:none;"' : ''; ?>>Copy The Code Below And Put it Where You Want This to Appear</div>
						<div class="siteformbox m_bannerads" <?php echo ($BannerAdsP != 1)? 'style="display:none;"' : ''; ?>>
							<textarea name="textarea" readonly="readonly" cols="45" rows="5" class="sta-from-box">&lt;?php echo $StatisticTotalBannerAds;?&gt;</textarea>
						</div>
					</div>
					*/ ?>
				</div>
			</div>
			<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_memberstatisticsupdate',$SubadminAccessArray)){ ?>
                    <div class="formbutton">
                        <?php echo $this->Js->submit('Update', array(
                          'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                          'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                          'update'=>'#UpdateMessage',
                          'class'=>'btnorange',
                          'controller'=>'Sitesetting',
                          'action'=>'memberstatisticsupdate',
                          'url'   => array('controller' => 'sitesetting', 'action' => 'memberstatisticsupdate')
                        ));?>
                    </div>
              <?php } ?>
			</div>
        <?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>