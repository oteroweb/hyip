<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Website</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Main Details", array('controller'=>'sitesetting', "action"=>"index"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'class'=>'satting-menu-active'
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Common Settings", array('controller'=>'sitesetting', "action"=>"common"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Database Operations", array('controller'=>'sitesetting', "action"=>"maintenance"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>	
				</li>
				<li>
					<?php echo $this->Js->link("Logs Settings", array('controller'=>'sitesetting', "action"=>"logs"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Launch Date Settings", array('controller'=>'sitesetting', "action"=>"launch"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Currency Settings", array('controller'=>'sitesetting', "action"=>"currency"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Cronjob Settings", array('controller'=>'sitesetting', "action"=>"cronjob"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Website_Settings#Currency_Settings" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="backgroundwhite">
	
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'currencyaddaction')));?>
		<?php if(isset($currencydata['Currency']["id"])){
			echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$currencydata['Currency']["id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}?>
		
			<div class="frommain">
				<div class="fromnewtext">Currency Code :<span class="red-color">*</span>  </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('code', array('type'=>'text', 'value'=>$currencydata['Currency']["code"], 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'readonly'=>($currencydata['Currency']["id"]==1)?true:false));?>
				</div>
				
				<div class="fromnewtext">Prefix :<span class="red-color">*</span>  </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('prefix', array('type'=>'text', 'value'=>$currencydata['Currency']["prefix"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
				
				<div class="fromnewtext">Suffix :<span class="red-color">*</span>  </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('suffix', array('type'=>'text', 'value'=>$currencydata['Currency']["suffix"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
				
				<div class="fromnewtext">Base Conversion Rate :<span class="red-color">*</span>  </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('rate', array('type'=>'text', 'value'=>$currencydata['Currency']["rate"], 'label' => false, 'div' => false, 'class'=>'fromboxbg', 'readonly'=>($currencydata['Currency']["id"]==1)?true:false));?>
				</div>
				
				<div class="formbutton">
					<?php echo $this->Js->submit('Submit', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#UpdateMessage',
					'class'=>'btnorange',
					'div'=>false,
					'controller'=>'sitesetting',
					'action'=>'currencyaddaction',
					'url'   => array('controller' => 'sitesetting', 'action' => 'currencyaddaction')
				  ));?>
				  <?php echo $this->Js->link("Back", array('controller'=>'sitesetting', "action"=>"currency"), array(
					  'update'=>'#settingpage',
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'escape'=>false,
					  'div'=>false,
					  'class'=>'btngray'
				  ));?>
				</div>
			</div>
		
		<?php echo $this->Form->end();?>

</div>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>