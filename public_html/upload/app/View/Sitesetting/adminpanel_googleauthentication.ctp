<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Security</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Common Settings", array('controller'=>'sitesetting', "action"=>"antibrute"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("2-Step Verification Settings", array('controller'=>'sitesetting', "action"=>"googleauthentication"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Password Settings", array('controller'=>'sitesetting', "action"=>"passwordsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Website Security", array('controller'=>'sitesetting', "action"=>"websitesecurity"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Captcha Settings", array('controller'=>'sitesetting', "action"=>"captcha"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Security_Settings#Two_Step_Authentication_Settings" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
	  <div class="subbar">Admin Area</div>
	  <?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'googleauthenticationaction')));?>
	  <?php echo $this->form->input('id', array('type'=>'hidden','value'=>'1'));?>
	  <div class="tab-pane" id="profile">
		  <div class="frommain" id="divalldetail">
				
			  <div class="fromnewtext">Enable Google 2-Step Verification :  </div>
			  <div class="fromborderdropedown3">
				  <div class="select-main">
					  <label>
					  <?php 
						   echo $this->Form->input('enable', array(
							'type' => 'select',
							'options' => array('1'=>'Yes', '0'=>'No'),
							'selected' => $enable,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => '',
							'onchange'=>'if($(this).val()==1){$(".GoogleAuthi").show(500);}else{$(".GoogleAuthi").hide(500);}'
						)); ?>
					  </label>
				  </div>
			  </div>
			 
			  <?php $style=''; if($enable==0){ $style="display:none";} ?>
			  <div class="fromnewtext GoogleAuthi" style="<?php echo $style; ?>">Select A Means To Set up Google 2-Step Verification : </div>
			  <div class="fromborderdropedown3 GoogleAuthi" style="<?php echo $style; ?>">
				  <div class="select-main">
					  <label>
					  <?php 
						   echo $this->Form->input('authentication', array(
							'type' => 'select',
							'options' => array('0'=>'QR Code', '1'=>'Secret Key'),
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));
					  ?>
					  </label>
				  </div>
			  </div>
			  
			  
			  <div class="fromnewtext GoogleAuthi" style="<?php echo $style; ?>">Admin Password : <span class="red-color">*</span> </div>
			  <div class="fromborderdropedown3 GoogleAuthi" style="<?php echo $style; ?>">
				  <?php echo $this->Form->input('current_pwd', array('type'=>'password', 'value'=>'', 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
			  </div>
			  
			  
			  <div class="formbutton">
				   <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_googleauthenticationaction',$SubadminAccessArray)){ ?>
				   <?php echo $this->Js->submit('Update', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#Googleauthidata',
						'class'=>'btnorange',
						'controller'=>'Sitesetting',
						'action'=>'googleauthenticationaction',
						'div'=>'false',
						'url'   => array('controller' => 'sitesetting', 'action' => 'googleauthenticationaction')
					  ));?>
				  <?php } ?>	
	  
		  </div>
	  </div>
	  <?php echo $this->Form->end();?>
	  
	  <?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'googleauthicodecheck')));?>
	  <?php echo $this->form->input('id', array('type'=>'hidden','value'=>'1'));?>
	  <div class="tab-pane" id="profile">
			<div class="frommain" id="divshowcode" style="display:none;">
				  
				  <div class="fromnewtext">Scan QR Code With Your Phone : </div>
				  
				  <div class="fromnewtext" id="Googleauthidata"></div>
				  
				  <div class="fromnewtext">Enter The Code Generated Here : <span class="red-color">*</span> </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('code', array('type'=>'text', 'value'=>'', 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
				  
				 <div class="formbutton">
					   <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_googleauthenticationaction',$SubadminAccessArray)){ ?>
					   <?php echo $this->Js->submit('Update', array(
							'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'update'=>'#UpdateMessage',
							'class'=>'btnorange',
							'controller'=>'Sitesetting',
							'action'=>'googleauthicodecheck',
							'div'=>'false',
							'url'   => array('controller' => 'sitesetting', 'action' => 'googleauthicodecheck')
						  ));?>
					  <?php } ?>	
		  
				  </div>
			</div>
		 </div>
	  <?php echo $this->Form->end();?>
	  
	  <div class="subbar">Member Area</div>
	  <?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'googleauthimember')));?>
	  <?php echo $this->form->input('id', array('type'=>'hidden','value'=>'1'));?>
	  <div class="tab-pane" id="profile">
		  <div class="frommain" id="divalldetail">
				
				  <div class="fromnewtext">Enable Google 2-Step Verification :  </div>
				  <div class="fromborderdropedown3">
					  <div class="select-main">
						  <label>
						  <?php 
							   echo $this->Form->input('allowgoogleauthi', array(
								'type' => 'select',
								'options' => array('1'=>'Yes', '0'=>'No'),
								'selected' => $SITECONFIG['allowgoogleauthi'],
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							)); ?>
						  </label>
					  </div>
				  </div>
				  
			  
				  <div class="fromnewtext">Enable Secondary Password :</div>
				  <div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
							  echo $this->Form->input('allowsecondary', array(
								  'type' => 'select',
								  'options' => array('1'=>'Yes', '0'=>'No'),
								  'selected' => $SITECONFIG["allowsecondary"],
								  'class'=>'',
								  'label' => false,
								  'div' => false,
								  'style' => ''
							  ));
						?>
						</label>
					</div>
				  </div>
				  
				
				  <div class="formbutton">
					   <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_googleauthenticationaction',$SubadminAccessArray)){ ?>
					   <?php echo $this->Js->submit('Update', array(
							'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'update'=>'#UpdateMessage',
							'class'=>'btnorange',
							'controller'=>'Sitesetting',
							'action'=>'googleauthimember',
							'div'=>'false',
							'url'   => array('controller' => 'sitesetting', 'action' => 'googleauthimember')
						  ));?>
					  <?php } ?>	
		  
				  </div>
	  </div>
	  <?php echo $this->Form->end();?>
	  
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>