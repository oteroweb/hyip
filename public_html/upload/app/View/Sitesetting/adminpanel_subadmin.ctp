<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Admin</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
		<ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Account Settings", array('controller'=>'sitesetting', "action"=>"account"), array(
					'update'=>'#settingpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Sub Admin", array('controller'=>'sitesetting', "action"=>"subadmin"), array(
					'update'=>'#settingpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
		</ul>
	</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Admin_Settings#Sub_Admin" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<div class="serchmainbox">
	<div class="serchgreybox"><?php echo "Search Option";?></div>
	<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'subadmin')));?>
	<div class="from-box">
		<div class="fromboxmain">
			<span>Search By :</span>
			<span>                     
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
							<?php 
							echo $this->Form->input('searchby', array(
								'type' => 'select',
								'options' => array('all'=>'Select Parameter', 'id'=>'Member Id', 'user_name'=>'Username', 'email'=>'Email', 'f_name'=>'First Name', 'l_name'=>'Last Name', 'register_ip'=>'Register IP'),
								'selected' => $searchby,
								'class'=>'',
								'label' => false,
								'style' => ''
							));
							?>
						</label>
					</div>
				</div>
			</span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?></span>
			
			<span class="padding-left">
				<?php echo $this->Js->submit('',array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#settingpage',
				'class'=>'searchbtn',
				'div'=>false,
				'controller'=>'Sitesetting',
				'action'=>'subadmin',
				'url'   => array('controller' => 'sitesetting', 'action' => 'subadmin')
			));?>
			</span>
		</div>
	</div>
	<?php echo $this->Form->end();?>
</div>

	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#settingpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'sitesetting', 'action'=>'subadmin')
	));
	?>
<div id="gride-bg" class='paddingbottom10'>
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
    	<?php echo $this->Js->link("+ Add New", array('controller'=>'sitesetting', "action"=>"subadminadd"), array(
            'update'=>'#settingpage',
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'escape'=>false,
            'class'=>'btnorange'
        ));?>
	</div>
	<div class="clear-both"></div>
	
		<div class="tablegrid">
			<div class="tablegridheader">
				<div>Id</div>
				<div>Reg. Date</div>
				<div>Username</div>
				<div>Full Name</div>
				<div>Email</div>
				<div>Registration IP</div>
				<div>Action</div>
			</div>
			<?php foreach ($subadmins as $subadmin):?>
				<div class="tablegridrow">
					<div><?php echo $subadmin['Subadmin']['id']; ?></div>
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $subadmin['Subadmin']['reg_dt']);?></div>
					<div><?php echo $subadmin['Subadmin']['user_name']; ?></div>
					<div><?php echo $subadmin['Subadmin']['f_name']." ".$subadmin['Subadmin']['l_name'];?></div>
					<div><?php echo $subadmin['Subadmin']['email']; ?></div>
					<div><?php echo $subadmin['Subadmin']['register_ip']; ?></div>
					<div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							<li>
								<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Sub Admin')).' Edit Sub Admin', array('controller'=>'sitesetting', "action"=>"subadminadd/".$subadmin['Subadmin']['id']), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));?>
							</li>
							<li>
								<?php echo $this->Js->link($this->html->image('copy-icon.png', array('alt'=>'Copy Sub Admin')).' Copy Sub Admin', array('controller'=>'sitesetting', "action"=>"subadminadd/".$subadmin['Subadmin']['id']."/yes"), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));?>
							</li>
							<li>
								<?php 
								if($subadmin['Subadmin']['status']==0){
									$statusaction='1';
									$statusicon='red-icon.png';
									$statustext='Activate Sub Admin';
								}else{
									$statusaction='0';
									$statusicon='blue-icon.png';
									$statustext='Inactivate Sub Admin';}
								echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'sitesetting', "action"=>"subadminstatus/".$statusaction."/".$subadmin['Subadmin']['id']."/".$this->params['paging']['Subadmin']['page']), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));?>
							</li>
							<li>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Sub Admin')).' Delete Sub Admin', array('controller'=>'sitesetting', "action"=>"subadminremove/".$subadmin['Subadmin']['id']."/".$this->params['paging']['Subadmin']['page']), array(
									'update'=>'#settingpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'confirm'=>'Are You Sure?'
								));?>
							</ul>
						</div>
					  </div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<?php if(count($subadmins)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	<?php 
	if($this->params['paging']['Subadmin']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'subadmin/rpp')));?>
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#settingpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'Sitesetting',
			  'action'=>'subadmin/rpp',
			  'url'   => array('controller' => 'sitesetting', 'action' => 'subadmin/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '...', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
<div class="clear-both"></div>
	
	</div>
</div>
    <?php }else{
		?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
	}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>