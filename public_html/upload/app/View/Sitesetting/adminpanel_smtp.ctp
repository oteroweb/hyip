<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 17-11-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Email</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("SMTP Settings", array('controller'=>'sitesetting', "action"=>"smtp"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Email Piping Settings", array('controller'=>'sitesetting', "action"=>"emailpipe"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Auto Responder", array('controller'=>'sitesetting', "action"=>"autoresponder"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Account Activation Mail", array('controller'=>'sitesetting', "action"=>"activationautomail"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Unpaid Member Notification", array('controller'=>'sitesetting', "action"=>"unpaidmembernotification"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Inactive Member Mail", array('controller'=>'sitesetting', "action"=>"inactivemembermail"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Membership Template Emails", array('controller'=>'sitesetting', "action"=>"membershipemailtemplate"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Email_Settings#SMTP_Settings" target="_blank">Help</a></div>

	  <div id="UpdateMessage"></div>
			<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'smtpupdate')));?>
			<?php echo $this->form->input('id', array('type'=>'hidden','value'=>'1'));?>
			
				<div class="frommain">
						
				  <div class="fromnewtext">Enable SMTP Authorization :</div>
				  <div class="fromborderdropedown3">
					  <div class="select-main">
						  <label>
						  <?php 
							echo $this->Form->input('smtp_authorize', array(
								'type' => 'select',
								'options' => array('1'=>'Enable', '0'=>'Disable'),
								'selected' => $smtp_authorize,
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => '',
								'onchange'=>'if($(this).val()==1){$(".enadis").show(500);}else{$(".enadis").hide(500); $(".testsmtp").hide(500);}'
							));
						  ?>
						  </label>
					  </div>
				  </div>
				  
				  <div class="enadis" style="<?php if($smtp_authorize==0){ echo 'display:none;';} ?>">
					<div class="fromnewtext">SMTP Server :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('smtp_server', array('type'=>'text', 'value'=>$SMTPServer, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
  
					<div class="fromnewtext">SMTP Username :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('smtp_uname', array('type'=>'text', 'value'=>$SMTPUsername, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
  
					<div class="fromnewtext">SMTP Password :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('smtp_pwd', array('type'=>'password', 'value'=>$SMTPPassword, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
  
					<div class="fromnewtext">SMTP Port :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('smtp_port', array('type'=>'text', 'value'=>$SMTPPort, 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
				    
					<div class="fromnewtext">SMTP SSL Type :</div>
				    <div class="fromborderdropedown3">
					  <div class="select-main">
						  <label>
						  <?php 
							echo $this->Form->input('smtp_type', array(
								'type' => 'select',
								'options' => array(''=>'None', 'ssl'=>'SSL', 'tls'=>'TLS'),
								'selected' => $SMTPType,
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
							));
						  ?>
						  </label>
					  </div>
				    </div>
				    
				</div>
					<div class="formbutton">
					<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_smtpupdate',$SubadminAccessArray)){ ?>
					<?php echo $this->Js->submit('Update', array(
						  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage',
						  'class'=>'btnorange',
						  'div'=>false,
						  'controller'=>'sitesetting',
						  'action'=>'smtpupdate',
						  'url'   => array('controller' => 'sitesetting', 'action' => 'smtpupdate')
					));?>
					 <?php } ?>
					<input type="button" class="btngray floatright enadis" style="<?php if($smtp_authorize==0){ echo 'display:none;';} ?>" value="Test SMTP Setting" onclick="$('.testsmtp').show(500);"/>
					</div>
					
					
				</div>
			
			<?php echo $this->Form->end();?>
			
			
			
			<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'smtptest')));?>
			
				<div class="frommain testsmtp" style="display: none;">
						
				  	<div class="fromnewtext">Email :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('smtp_testemail', array('type'=>'text',  'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
  
					<div class="fromnewtext">Subject :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('smtp_testsubject', array('type'=>'text',  'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
					</div>
					
					<div class="fromnewtext">Message  :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<?php echo $this->Form->input('smtp_testmessage', array('type'=>'textarea', 'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
					</div>
				    
				
					<div class="formbutton">
					<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_smtpupdate',$SubadminAccessArray)){ ?>
					<?php echo $this->Js->submit('Send', array(
						  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage',
						  'class'=>'btnorange',
						  'div'=>false,
						  'controller'=>'sitesetting',
						  'action'=>'smtpupdate',
						  'url'   => array('controller' => 'sitesetting', 'action' => 'smtptest')
					));?>
					 <?php } ?>
					</div>
				</div>
			
			<?php echo $this->Form->end();?>

<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>