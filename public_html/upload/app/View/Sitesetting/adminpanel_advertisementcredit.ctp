<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 12-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Advertisement</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Banner Ads", array('controller'=>'sitesetting', "action"=>"advertisementcredit"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Text Ads", array('controller'=>'sitesetting', "action"=>"advertisementtextadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC Text Ads", array('controller'=>'sitesetting', "action"=>"ppctextadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Solo Ads", array('controller'=>'sitesetting', "action"=>"soload"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Login Ads", array('controller'=>'sitesetting', "action"=>"loginadsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PPC", array('controller'=>'sitesetting', "action"=>"ppcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("PTC", array('controller'=>'sitesetting', "action"=>"ptcsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Biz Directory", array('controller'=>'sitesetting', "action"=>"bizdirectorysetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Traffic Exchange", array('controller'=>'sitesetting', "action"=>"trafficsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Surfing Ads Settings", array('controller'=>'sitesetting', "action"=>"surfingsetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Widgets", array('controller'=>'sitesetting', "action"=>"advertisementbannerwidget"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<div class="tab-innar">
	<ul>
		<li>
			<?php echo $this->Js->link("Credit Banner Ads Settings", array('controller'=>'sitesetting', "action"=>"advertisementcredit"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>'active'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Plan Banner Ads Settings", array('controller'=>'sitesetting', "action"=>"banneradplan"), array(
				'update'=>'#settingpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				
			));?>
		</li>
	</ul>
</div>
<?php if($IsAdminAccess){?>
	
	
	<div class="helpicon"><a href="https://www.proxscripts.com/docs/Advertisement_Settings#Banner_Ads" target="_blank">Help</a></div>
	
	<div id="UpdateMessage"></div>
	<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'advertisementcreditupdate')));?>
	<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
		<div class="tab-pane" id="profile">
			<div class="frommain">
				<div class="fromnewtext">Enable Credit Banner Ad System :<span class="red-color">*</span>  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
					  <label>
					  <?php 
						echo $this->Form->input('enable_bannerads', array(
							'type' => 'select',
							'options' => array('1'=>'Yes', '0'=>'No'),
							'selected' => $SITECONFIG["enable_bannerads"],
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => '',
							'onchange'=>'if($(this).val()==1){$(".enadis").show(500);}else{$(".enadis").hide(500);}'
						));
					  ?>
					  </label>
					</div>
				</div>
				
				<div class="enadis" style="<?php if($SITECONFIG["enable_bannerads"]==0){ echo 'display:none;';} ?>">
				<div class="fromnewtext">Auto Approval :  </div>
				  <div class="fromborderdropedown3">
					  <div class="select-main">
						  <label>
							  <?php 
								echo $this->Form->input('addapprove', array(
									'type' => 'select',
									'options' => array('0'=>'Yes', '1'=>'No'),
									'selected' => $addapprove,
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => ''
								));
							  ?>
						  </label>
					  </div>
				  </div>
				  
				  
				  <div class="fromnewtext">Auto Approval on Edit :  </div>
				  <div class="fromborderdropedown3">
					  <div class="select-main">
						  <label>
							  <?php 
								echo $this->Form->input('iseditapprove', array(
									'type' => 'select',
									'options' => array('0'=>'Yes', '1'=>'No'),
									'selected' => $iseditapprove,
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => ''
								));
							  ?>
						  </label>
					  </div>
				  </div>
				 
				
				<div class="fromnewtext">Credits :<span class="red-color">*</span>  </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('memberearncredit', array('type'=>'text', 'value'=>$SITECONFIG["memberearncredit"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
				<div class="fromnewtext">Seconds to Wait to Get Credits :<span class="red-color">*</span>  </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('membercreditseconds', array('type'=>'text', 'value'=>$SITECONFIG["membercreditseconds"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
				</div>
				<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_advertisementcreditupdate',$SubadminAccessArray)){ ?>
				<div class='formbutton'>
					<?php echo $this->Js->submit('Update', array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage',
					  'class'=>'btnorange',
					  'div'=>false,
					  'controller'=>'sitesetting',
					  'action'=>'advertisementcreditupdate',
					  'url'   => array('controller' => 'sitesetting', 'action' => 'advertisementcreditupdate')
					));?>
				</div>
			  <?php } ?>
			</div>
		</div>
	<?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<br />
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>