<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Website</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Main Details", array('controller'=>'sitesetting', "action"=>"index"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'class'=>'satting-menu-active'
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Member Tool Settings", array('controller'=>'sitesetting', "action"=>"shortenersetting"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Database Operations", array('controller'=>'sitesetting', "action"=>"maintenance"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>	
				</li>
				<li>
					<?php echo $this->Js->link("Logs Settings", array('controller'=>'sitesetting', "action"=>"logs"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Launch Date Settings", array('controller'=>'sitesetting', "action"=>"launch"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Currency Settings", array('controller'=>'sitesetting', "action"=>"currency"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Cronjob Settings", array('controller'=>'sitesetting', "action"=>"cronjob"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Notification", array('controller'=>'sitesetting', "action"=>"notification"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Website_Settings#Launch_Date_Settings" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'launchupdate')));?>
		<?php echo $this->form->input('id', array('type'=>'hidden','value'=>'1'));?>
		<div class="tab-pane" id="profile">
			<div class="frommain">
				<div class="fromnewtext">Launch Theme Title : <span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('launch_text', array('type'=>'text', 'value'=>stripslashes($SITECONFIG["launch_text"]), 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
				
				<div class="fromnewtext">Site Launch Date : </div>
				<?php echo $this->Form->input('launch_dt', array('type'=>'text', 'value'=>$SITECONFIG["launch_dt"], 'label' => false, 'div' => false, 'class'=>'fromboxbgcal width93 datetimepicker', 'style'=>''));?>
				<div class="fromnewtext">Site Launch Time Zone :  </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
					<label>
					<?php echo $this->timezone->select('launchzone', false, $SITECONFIG["launchzone"]); ?>			
					</label>
					</div>
				</div>
				
				<div class="fromnewtext">Launch Date Theme :  </div>
				<div class="fromborderdropedown3">
					<div class="tooltipfronright">
						
					</div>
					<div class="rediobtn padding28 ">
						<div class="retbtn">
							<div class="rediobox">
							<?php 
							echo $this->Form->radio('launchtheme', array(
									1=>$this->html->image("l1.png", array("alt"=>"Theme 1", 'align'=>'absmiddle', 'class'=>'')), 
									2=>$this->html->image("l2.png", array("alt"=>"Theme 2", 'align'=>'absmiddle', 'class'=>'')), 
									3=>$this->html->image("l3.png", array("alt"=>"Theme 3", 'align'=>'absmiddle', 'class'=>''))
								), 
								array('legend' => false,'separator'=>'<br>', 'label' => true, 'div' => true,  'default' => $SITECONFIG["launchtheme"]));
							?>	
							</div>
						</div>
					</div>
				</div>
			<?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_launchupdate',$SubadminAccessArray)){ ?>
				<div class="formbutton">
					<?php echo $this->Js->submit('Update', array(
						  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage',
						  'class'=>'btnorange',
						  'div'=>false,
						  'controller'=>'Sitesetting',
						  'action'=>'launchupdate',
						  'url'   => array('controller' => 'sitesetting', 'action' => 'launchupdate')
					));?>
				</div>
			<?php } ?>
		</div>
</div>
		<?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>