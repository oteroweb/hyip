<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Finance</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Payment Processor", array('controller'=>'sitesetting', "action"=>"paymentprocessor"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Add Fund Settings", array('controller'=>'sitesetting', "action"=>"addfund"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Withdrawal Settings", array('controller'=>'sitesetting', "action"=>"withdrawal"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Finance_Settings#Add_Fund_Settings" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'addfundupdate')));?>
		<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>1, 'label' => false));?>
		<div class="tab-pane" id="profile">
			<div class="frommain">
				  <div class="fromnewtext">Enable Add Fund Limit :</div>
				  <div class="fromborderdropedown3">
					  <div class="select-main">
					  <label>
					  <?php 
						  echo $this->Form->input('enable_addfund', array(
							  'type' => 'select',
							  'options' => array('1'=>'Yes', '0'=>'No'),
							  'selected' => $SITECONFIG["enable_addfund"],
							  'class'=>'',
							  'label' => false,
							  'div' => false,
							  'style' => '',
							  'onchange'=>'if($(this).val()==1){$(".enadis").show(500);}else{$(".enadis").hide(500);}'
						  ));
					  ?>
					  </label>
					  </div>
				  </div>
				  
				  <div class="enadis" style="<?php if($SITECONFIG["enable_addfund"]==0){ echo 'display:none;';} ?>">
				  <div class="fromnewtext">Minimum Sum Of Add Fund ($) :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('addfundminlimit', array('type'=>'text', 'value'=>$SITECONFIG["addfundminlimit"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
				  <div class="fromnewtext">Maximum Sum Of Add Fund ($) :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('addfundmaxlimit', array('type'=>'text', 'value'=>$SITECONFIG["addfundmaxlimit"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
				  <div class="fromnewtext">Life Time Add Fund Limit ($) :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
					  <?php echo $this->Form->input('addfundlifetimelimit', array('type'=>'text', 'value'=>$SITECONFIG["addfundlifetimelimit"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				  </div>
				  
				  
				  <div class="fromnewtext">Customised Add Fund Limit :<span class="red-color">*</span>  </div>
				  <div class="fromborderdropedown3">
						<?php echo "<div class='paidetext2'>Allow Maximum $ </div><div class='paidfrombox'>".$this->Form->input('addfundamountlimit', array('type'=>'text', 'value'=>$SITECONFIG["addfundamountlimit"], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>'')). "</div><div class='paidetext2'> every </div><div class='paidfrombox'>".$this->Form->input('addfunddaylimit', array('type'=>'text', 'value'=>$SITECONFIG["addfunddaylimit"], 'label' => false, 'div' => false, 'class'=>'fromboxbg4', 'style'=>''))."</div><div class='paidetext2'> Days</div>";?>
						 
				  </div>
				  </div>
				  <div class="formbutton">
			  <?php if(!isset($SubadminAccessArray) || in_array('sitesetting',$SubadminAccessArray) || in_array('sitesetting/adminpanel_addfundupdate',$SubadminAccessArray)){ ?>
				  	<?php echo $this->Js->submit('Update', array(
						  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage',
						  'class'=>'btnorange',
						  'div'=>false,
						  'controller'=>'sitesetting',
						  'action'=>'addfundupdate',
						  'url'   => array('controller' => 'sitesetting', 'action' => 'addfundupdate')
						));?>
			  <?php } ?>
		
				  </div>
			</div>
		</div>
			<?php echo $this->Form->end();?>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>