<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Settings / Signup Settings</div>
<div class="height10"></div>
<div class="tab-blue-box">
	  <div id="tab">
	     <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Registration Form Settings", array('controller'=>'sitesetting', "action"=>"registrationform"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Profile Form Settings", array('controller'=>'sitesetting', "action"=>"profileform"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Free Credit & Bonus", array('controller'=>'sitesetting', "action"=>"freecreditandbonus"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Signup Settings", array('controller'=>'sitesetting', "action"=>"signup"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Facebook & Google Settings", array('controller'=>'sitesetting', "action"=>"facebookgoogle"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
			</ul>
		</div>
</div>
<div class="tab-content">
<div id="settingpage">
<?php }?>

<div class="helpicon"><a href="https://www.proxscripts.com/docs/Signup_Settings#Registration_Form_Settings" target="_blank">Help</a></div>

	<div id="UpdateMessage"></div>
		<?php echo $this->Form->create('Sitesetting',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'sitesetting','action'=>'registrationformaddaction')));?>
		
		<?php if(isset($regform_managedata['Regform_manage']["field_id"])){
			echo $this->Form->input('field_id', array('type'=>'hidden', 'value'=>$regform_managedata['Regform_manage']["field_id"], 'label' => false));
			echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		}else{
			echo $this->Form->input('display_order', array('type'=>'hidden', 'value'=>$this->Session->read('max_order')+1, 'label' => false));
			echo $this->Form->input('profileorder', array('type'=>'hidden', 'value'=>$this->Session->read('max_order_profile')+1, 'label' => false));
		}?>
		<div class="tab-pane" id="profile">
			<div class="frommain">
				<div class="fromnewtext">Field Title : <span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					  <?php echo $this->Form->input('field_title', array('type'=>'text', 'value'=>$regform_managedata['Regform_manage']["field_title"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>

				<div class="fromnewtext">Field Type :</div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
						echo $this->Form->input('field_type', array(
							'type' => 'select',
							'options' => array('textbox'=>'Textbox', 'textarea'=>'Textarea'),
							'selected' => $regform_managedata['Regform_manage']["field_type"],
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));
						?>
						</label>
					</div>
				</div>
				
				  <div class="fromnewtext">Field Required :</div>
				  <div class="fromborderdropedown3">
					<div class="select-main">
						<label>
						<?php 
						echo $this->Form->input('field_req', array(
							'type' => 'select',
							'options' => array('0'=>'Not Required', '1'=>'Required'),
							'selected' => $regform_managedata['Regform_manage']["field_req"],
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));
						?>
						</label>
					</div>
				  </div>
				  <div class="formbutton">
						<?php echo $this->Js->submit('Submit', array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage',
					  'class'=>'btnorange',
					  'controller'=>'sitesetting',
					  'div'=>false,
					  'action'=>'registrationformaddaction',
					  'url'   => array('controller' => 'sitesetting', 'action' => 'registrationformaddaction')
					));?>
                    <?php echo $this->Js->link("Back", array('controller'=>'sitesetting', "action"=>"registrationform"), array(
						'update'=>'#settingpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'btngray',
						'div'=>false
					));?>
				  </div>
			</div>
		</div>
		<?php echo $this->Form->end();?>
<?php if(!$ajax){?>
</div><!--#settingpage over-->
</div>
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>