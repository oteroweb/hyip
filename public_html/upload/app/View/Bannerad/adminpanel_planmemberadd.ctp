<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 14-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Banner Ads</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Banner Ads", array('controller'=>'bannerad', "action"=>"index"), array(
					'update'=>'#banneradpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("View Credits", array('controller'=>'bannerad', "action"=>"bannercredit"), array(
					'update'=>'#banneradpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Banner Ad Plans", array('controller'=>'bannerad', "action"=>"plan"), array(
					'update'=>'#banneradpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="banneradpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Banner_Ads#Plan_Banner_Ads" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<?php echo $this->Form->create('Bannerad',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'bannerad','action'=>'planmemberaddaction')));?>
	<?php if(isset($banneraddata['Bannerad']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$banneraddata['Bannerad']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		echo $this->Form->input('banner_size', array('type'=>'hidden', 'value'=>$banneraddata['Bannerad']["banner_size"], 'label' => false));
	}?>
	<div class="frommain">
		
		<div class="fromnewtext">Title :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('title', array('type'=>'text', 'value'=>$banneraddata['Bannerad']["title"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Banner URL :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('banner_url', array('type'=>'text', 'value'=>$banneraddata['Bannerad']["banner_url"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Destination URL :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('site_url', array('type'=>'text', 'value'=>$banneraddata['Bannerad']["site_url"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Description :<span class="red-color">*</span> </div>
		<div class="frombordermain">
			<?php echo $this->Form->input('description', array('type'=>'textarea', 'value'=>stripslashes($banneraddata['Bannerad']["description"]),'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
		</div>
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'class'=>'btnorange',
			  'div'=>false,
			  'controller'=>'bannerad',
			  'action'=>'planmemberaddaction',
			  'url'   => array('controller' => 'bannerad', 'action' => 'planmemberaddaction')
			));?>
			
			<?php echo $this->Js->link("Back", array('controller'=>'bannerad', "action"=>"planmember/".$planid), array(
				'update'=>'#banneradpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'div'=>false,
				'class'=>'btngray'
			));?>
		</div>
		
	</div>
<?php echo $this->Form->end();?>
<?php if(!$ajax){?>
</div><!--#banneradpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>