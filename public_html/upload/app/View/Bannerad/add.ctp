<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 24-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('framechecker');?>
<?php if(!$ajax){?>
<div id="banneradpage">
<?php } ?>
<div id="UpdateMessage"></div>

<?php //Frame Break Code Start ?>
<input type="hidden" id='framebreakmessage' value='<?php echo __('Destination URL Not Valid'); ?>|<?php echo __('Breaking out of frames'); ?>|<?php echo __('Inspecting Website'); ?>|<?php echo __('Framebreaker test passed'); ?>|<?php echo __('Error').' : '; ?>'/>
<div id="check" class="frambreakmain">
	<div class='textleft'>
		<span id='framebreakimg'></span>
		<span class="framebreaktext"><?php echo __('Inspecting Framebreaker'); ?></span>
	</div>
	<div class='textleft'>
		<span id="check_final"><?php echo __('Inspecting Website'); ?></span>
	</div>
</div>
<?php //Frame Break Code Over ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Banner Ad Details');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	
	<?php // Banner ad add/edit form starts here ?>
	<?php echo $this->Form->create('Userbanner',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'bannerad','action'=>'addaction')));?>
				<?php if(isset($userbannerdata['Userbanner']["banner_id"])){
					$action='addaction/'.$userbannerdata['Userbanner']["banner_id"];
					echo $this->Form->input('current_style', array('type'=>'hidden', 'value'=>$userbannerdata['Userbanner']["style"], 'label' => false));
				}else{
					$action='addaction';
				}?>
					<?php if(isset($userbannerdata['Userbanner']['photo'])){?>
					<div class="formgridrow color-black">
						<div class="textcenter vam"><div class="height5"></div><img src="<?php echo $userbannerdata['Userbanner']['photo'];?>" alt="" /><div class="height5"></div></div>
					</div>
					<?php }?>
				<div class="form-box">
					<div class="form-row">
						<div class="form-col-1"><?php echo __('Available Credits')?> :</div>
						<div class="form-col-2 form-text"><?php echo $addbanner_banner_credit;?></div>
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __('Title')?> : <span class="required">*</span></div>
						<div class="form-col-2"><?php echo $this->Form->input('title' ,array('type'=>'text', "class"=>"formtextbox", 'label'=>false, 'value'=>$userbannerdata['Userbanner']["title"],'div'=>false));?>
						<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)') ?>"></span>
						
						</div>
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __('Allocate Credits')?> : <span class="required">*</span>	</div>
						<div class="form-col-2"><?php echo $this->Form->input('credit' ,array('type'=>'text', "class"=>"formtextbox", 'label'=>false, 'value'=>$userbannerdata['Userbanner']["credit"]));?></div>
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __('Banner URL')?> : <span class="required">*</span>	</div>
						<div class="form-col-2"><?php echo $this->Form->input('photo' ,array('type'=>'text', "class"=>"formtextbox", 'label'=>false, 'value'=>$userbannerdata['Userbanner']["photo"]));?></div>
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __('Destination URL')?> : <span class="required">*</span>	</div>
						<?php if($userbannerdata['Userbanner']["siteurl"]=='') $userbannerdata['Userbanner']["siteurl"]='http://' ?>
						<div class="form-col-2"><?php echo $this->Form->input('siteurl' ,array('type'=>'text', "class"=>"formtextbox txtframebreaker", 'label'=>false, 'value'=>$userbannerdata['Userbanner']["siteurl"]));?></div>
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __('Banner Style')?> : </div>
						
						<div class="select-dropdown">
							<label>
								<?php echo $this->Form->input('style', array(
									  'type' => 'select',
									  'options' => array('468x60'=>'468X60', '125x125'=>'125X125'),
									  'selected' => $userbannerdata['Userbanner']["style"],
									  'class'=>'searchcomboboxwidth',
									  'label' => false,
									  'div' => false
								));?>
							</label>
						</div>
						
					</div>
					<?php if($AdvertisementBannerAddCaptcha){ ?>
					    <div class="form-row captchrow">
						<div class="form-col-1"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
							<?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode' ,'div'=>false, "class"=>"formcapthcatextbox", 'label'=>false));?>
							<span><?php echo __('Code')?> :</span>
							<?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'AdvertisementBannerAddCaptcha','vspace'=>2,"style"=>"vertical-align: middle")); ?> 
							<a href="javascript:void(0);" onclick="javascript:document.images.AdvertisementBannerAddCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "style"=>"vertical-align: middle"));?></a>
					  </div>
					<?php }?>
				</div>
		<div class="formbutton">
			<?php echo $this->Js->link(__("Back"), array('controller'=>'bannerad', "action"=>"index"), array(
				'update'=>'#banneradpage',
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button',
				'style'=>'float:left',
				'controller'=>'bannerad',
				'action'=>'index',
				'url'=> array('controller' => 'bannerad', 'action' => 'index')
			));?>
			<input type="button" value="<?php echo __('Submit'); ?>" class="button" onclick="return checkframebreaker(0,'<?php echo $SITEURL; ?>','framebreaker');" />
			<?php echo $this->Js->submit(__('Submit'), array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'div'=>false,
			  'class'=>'button framebreaker',
			  'style'=>'display:none',
			  'controller'=>'bannerad',
			  'action'=>$action,
			  'url'=> array('controller'=>'bannerad', 'action'=>$action)
			));?>
		</div>
		<div class="clear-both"></div>
	<?php echo $this->Form->end();?>
	<?php // Banner ad add/edit form ends here ?>
	
</div>
<?php if(!$ajax){?>
</div>
<?php } ?>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>