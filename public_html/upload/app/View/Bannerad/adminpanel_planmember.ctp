<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Banner Ads</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Banner Ads", array('controller'=>'bannerad', "action"=>"index"), array(
					'update'=>'#banneradpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("View Credits", array('controller'=>'bannerad', "action"=>"bannercredit"), array(
					'update'=>'#banneradpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Banner Ad Plans", array('controller'=>'bannerad', "action"=>"plan"), array(
					'update'=>'#banneradpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="banneradpage">
<?php }?>

    <div class="tab-innar">
		<ul>
		    <li>
				<?php echo $this->Js->link("Credit Banner Ads", array('controller'=>'bannerad', "action"=>"index"), array(
					'update'=>'#banneradpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
		    </li>
		    <li>
				<?php echo $this->Js->link("Plan Banner Ads", array('controller'=>'bannerad', "action"=>"planmember"), array(
					'update'=>'#banneradpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'active'
				));?>
			</li>
		</ul>
	</div>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Banner_Ads#Plan_Banner_Ads" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Bannerad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'bannerad','action'=>'planmember')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php echo $this->Form->input('searchby', array(
							  'type' => 'select',
							  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'member_id'=>'Member Id', 'title'=>'Banner Title',  'display_counter'=>'Displayed', 'click_counter'=>'Clicked',  'active'=>'Active Banners', 'inactive'=>'Inactive Banners', 'expire'=>'Expire Banners', 'running'=>'Running Banners', '125x125'=>'125x125 Banners', '468x60'=>'468x60 Banners', '728x90'=>'728x90 Banners'),
							  'selected' => $searchby,
							  'class'=>'',
							  'label' => false,
							  'style' => '',
							  'onchange'=>'if($(this).val()=="id" || $(this).val()=="member_id" || $(this).val()=="all" || $(this).val()=="title" || $(this).val()=="display_counter" || $(this).val()=="click_counter"){$("#SearchFor").show(500);}else{$("#SearchFor").hide(500);}'
						));?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=="active" || $searchby=="inactive" || $searchby=="125x125" || $searchby=="468x60" || $searchby=="728x90" || $searchby=='expire' || $searchby=='running'){ echo "style='display:none'";} ?>>
			<span>Search For :</span>
			<span class="searchforfields_s"><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#banneradpage',
				'class'=>'searchbtn',
				'controller'=>'bannerad',
				'action'=>'planmember/'.$planid,
				'url'=> array('controller' => 'bannerad', 'action' => 'planmember/'.$planid)
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->

	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#banneradpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'bannerad', 'action'=>'planmember/'.$planid)
	));
	$currentpagenumber=$this->params['paging']['Bannerad']['page'];
	?>
	
<div id="gride-bg">
    <div class="Xpadding10">
				
	<div class="records records-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Bannerad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'bannerad','action'=>'planmember')));?>
	
	<div class="tablegrid">
		<div class="tablegridheader">
			   <div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'bannerad', "action"=>"planmember/".$planid."/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('M. Id', array('controller'=>'bannerad', "action"=>"planmember/".$planid."/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Plan', array('controller'=>'bannerad', "action"=>"planmember/".$planid."/0/plan_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Purchase Date', array('controller'=>'bannerad', "action"=>"planmember/".$planid."/0/pruchase_date/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Purchase Date'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Displayed', array('controller'=>'bannerad', "action"=>"planmember/".$planid."/0/display_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Displayed'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Clicked', array('controller'=>'bannerad', "action"=>"planmember/".$planid."/0/click_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Clicked'
					));?>
				</div>
				<div><?php echo 'Action';?></div>
			</div>
			<?php foreach ($bannerads as $bannerad): ?>
			<div class="tablegridrow">
				    <div><?php echo $bannerad['Bannerad']['id']; ?></div>
					<div>
						<?php 
						echo $this->Js->link($bannerad['Bannerad']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$bannerad['Bannerad']['member_id']."/top/bannerad/index~top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>
						<a href="#" class="vtip" title="<b>Plan Name : </b><?php echo $bannerad['Banneradplan']['plan_name']; ?><br><b>Banner Size : </b><?php echo $bannerad['Bannerad']['banner_size']; ?>"> <?php echo $bannerad['Bannerad']['plan_id']; ?></a>
				    </div>
					<div class="textcenter" style="width: 60%;">
						<p><b>Purchase Date : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $bannerad['Bannerad']['pruchase_date']); ?></p>
						<a href="<?php echo $bannerad['Bannerad']['site_url'];?>" target="_blank" class="vtip" title="<b>Title : </b><?php echo $bannerad['Bannerad']['title']; ?>">
							<?php
								if($bannerad['Bannerad']['banner_size']=='125x125'){$width="50px";$maxwidth='125px';}
								elseif($bannerad['Bannerad']['banner_size']=='468x60'){$width="90%";$maxwidth="468px";}
								else{$width="98%";$maxwidth="728px";}
								echo '<img src="'.$bannerad['Bannerad']['banner_url'].'" alt="'.$bannerad['Bannerad']['title'].'" width="'.$width.'" style="max-width:'.$maxwidth.';" />';
							?>
						</a>
						<p><b>Approved Date : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $bannerad['Bannerad']['approve_date']); ?></p>
						<p><b>Expiry Date : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $bannerad['Bannerad']['expire_date']); ?></p>
					</div>
					<div><?php echo $bannerad['Bannerad']['display_counter']; ?></div>
					<div><?php echo $bannerad['Bannerad']['click_counter']; ?></div>
					<div class="textcenter">
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
												
												<?php if(!isset($SubadminAccessArray) || in_array('bannerad',$SubadminAccessArray) || in_array('bannerad/adminpanel_planmemberadd/$', $SubadminAccessArray)){ ?>
												<li>
												<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Banner'))." Edit", array('controller'=>'bannerad', "action"=>"planmemberadd/".$planid."/".$bannerad['Bannerad']['id']), array(
												'update'=>'#banneradpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>''
												));?>
												</li>
												<?php } ?>
										
												<?php if(!isset($SubadminAccessArray) || in_array('bannerad',$SubadminAccessArray) || in_array('bannerad/adminpanel_planmemberstatus', $SubadminAccessArray)){ ?>
												<li>
												<?php
												if($bannerad['Bannerad']['status']==0){
													$statusaction='1';
													$statusicon='red-icon.png';
													$statustext='Approve';
												}else{
													$statusaction='0';
													$statusicon='blue-icon.png';
													$statustext='Disapprove';}
												echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'bannerad', "action"=>"planmemberstatus/".$planid."/".$statusaction."/".$bannerad['Bannerad']['id']."/".$currentpagenumber), array(
													'update'=>'#banneradpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>''
												));?>
												</li>
												<?php } ?>
												
												<?php if(!isset($SubadminAccessArray) || in_array('bannerad',$SubadminAccessArray) || in_array('bannerad/adminpanel_planmemberremove', $SubadminAccessArray)){ ?>
												<li>
												<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Banner'))." Delete", array('controller'=>'bannerad', "action"=>"planmemberremove/".$planid."/".$bannerad['Bannerad']['id']."/".$currentpagenumber), array(
												'update'=>'#banneradpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'confirm'=>"Do You Really Want to Delete This Banner?"
												));?>
												</li>
												<?php } ?>
									</ul>
								  </div>
								</div>
                    </div>
				</div>
			<?php endforeach; ?>				
	</div>
	<?php if(count($bannerads)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Bannerad']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Bannerad',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'planmember','action'=>'bannerad/'.$planid.'/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#banneradpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'bannerad',
			  'action'=>'planmember/'.$planid.'/rpp',
			  'url'   => array('controller' => 'bannerad', 'action' => 'planmember/'.$planid.'/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#banneradpage over-->
</div>
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>