<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('framechecker');?>
<?php if($EnableBannerad==1) { ?>
<?php if(!$ajax){ ?>
<div id="banneradpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>

<?php //Frame Break Code Start ?>
<input type="hidden" id='framebreakmessage' value='<?php echo __('Destination URL Not Valid'); ?>|<?php echo __('Breaking out of frames'); ?>|<?php echo __('Inspecting Website'); ?>|<?php echo __('Framebreaker test passed'); ?>|<?php echo __('Error').' : '; ?>'/>
<div id="check" class="frambreakmain">
	<div class='textleft'>
		<span id='framebreakimg'></span>
		<span class="framebreaktext"><?php echo __('Inspecting Framebreaker'); ?></span>
	</div>
	<div class='textleft'>
		<span id="check_final"><?php echo __('Inspecting Website'); ?></span>
	</div>
</div>
<?php //Frame Break Code Over ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Get Free Banner Ad");?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
		<?php // Banner ad plan purchase form starts here ?>
		<?php echo $this->Form->create('Bannerad',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'bannerad','action'=>'getfreebanneradaction/'.$pending_free_plan_id)));?>
			
		<div class="form-box">
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Plan Name");?> : </div>
				<div class="form-col-2 form-text"><?php echo $bannerdata['Banneradplan']['plan_name'];?></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Days");?> : </div>
				<div class="form-col-2 form-text"><?php echo $bannerdata['Banneradplan']['days'];?> days</div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Advertisement Type");?> : </div>
				<div class="form-col-2 form-text"><?php echo ($bannerdata['Banneradplan']['isstatic']==1)? __('Static') : __('Rotating') ;?></div>
			</div>
			<?php if($bannerdata['Banneradplan']['isstatic']==1){ ?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Display Area");?> : </div>
				<div class="form-col-2 form-text"><?php echo ($bannerdata['Banneradplan']['displayarea']==1)? __('Member Area') : __('Public Area') ;?></div>
			</div>
			<?php } ?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Banner Size");?> : </div>
				<div class="form-col-2 form-text"><?php echo $bannerdata['Banneradplan']['banner_size'];?></div>
			</div>
			
			<?php if($bannerdata['Banneradplan']['fpcommission']==1){?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Pay Commission For The First Position Only");?> : </div>
				<div class="form-col-2 form-text"><?php echo __("Yes");?></div>
			</div>
			<?php }?>
			
			<?php $commissionlevel=explode(",", $bannerdata['Banneradplan']['commissionlevel']);if($commissionlevel[0]>0){?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Referral Commission Structure");?> : </div>
				<div class="form-col-2 form-text">
					<?php $commilev="";for($i=0;$i<count($commissionlevel);$i++){if($commissionlevel[$i]>0){$commilev.="Level ".($i+1)." : ".$commissionlevel[$i]."%, ";}else{break;}}echo trim($commilev,", ")?>
				</div>
			</div>
			<?php }?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Next Available Date");?> : </div>
				<div class="form-col-2 form-text"><?php echo $this->Time->format($SITECONFIG["timeformate"], $approvedate);?></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Title");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('title', array('type'=>'text', 'label' => false, 'div'=>false, 'class'=>'formtextbox'));?>
					<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)') ?>"></span>
					
				</div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Banner URL");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('banner_url', array('type'=>'text', 'label' => false,'onblur'=>'if(this.value!=""){document.getElementById("img1").src = this.value;$("#img1").show();}else{$("#img1").hide();}', 'class'=>'formtextbox'));?><img src="" id="img1" class="hide" /></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Destination URL");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('site_url', array('type'=>'text', 'label' => false, 'class'=>'formtextbox txtframebreaker', 'value' => 'http://'));?></div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Description");?> : <span class="required">*</span></div>
				<div class="form-col-2"><?php echo $this->Form->input('description', array('type'=>'textarea', 'label'=>false,'title'=>__('Description'), 'class'=>'formtextarea '));?></div>
			</div>
		</div>
	
		<div class="profile-bot-left floatright"><?php if($SITECONFIG["enableagree"]==1){echo $this->Form->input('termsandconditions', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>'')); echo ' <a href="'.$SITEURL.'public/terms" target="_blank">'.__('I agree with the terms and conditions').'</a> '.__('of').' '.$SITECONFIG["sitetitle"];} ?></div>
		<div class="clear-right"></div>
	
	<div class="clear-both"></div>
	<div class="formbutton">
		<?php echo $this->Js->link(__("Back"), array('controller'=>'bannerad', "action"=>"plans"), array(
			'update'=>'#banneradpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'button',
			'style'=>'float:left'
		));?>
		<input type="button" value="<?php echo __('Get it'); ?>" class="button" onclick="return checkframebreaker(0,'<?php echo $SITEURL; ?>','framebreaker');" />
		<?php echo $this->Js->submit(__('Get it'), array(
			'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'update'=>'#UpdateMessage',
			'class'=>'button framebreaker',
			'div'=>false,
			'controller'=>'bannerad',
			'style'=>'display:none',
			'action'=>'getfreebanneradaction/'.$pending_free_plan_id,
			'url'   => array('controller' => 'bannerad', 'action' => 'getfreebanneradaction/'.$pending_free_plan_id)
		));?>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->end();?>
	<?php // Banner ad plan purchase form ends here ?>
	
<?php if(!$ajax){ ?>
</div>
<?php } ?>	
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>