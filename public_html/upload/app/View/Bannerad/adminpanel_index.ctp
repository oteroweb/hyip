<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Banner Ads</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Banner Ads", array('controller'=>'bannerad', "action"=>"index"), array(
					'update'=>'#banneradpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("View Credits", array('controller'=>'bannerad', "action"=>"bannercredit"), array(
					'update'=>'#banneradpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Banner Ad Plans", array('controller'=>'bannerad', "action"=>"plan"), array(
					'update'=>'#banneradpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="banneradpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Banner_Ads#Banner_Ads" target="_blank">Help</a></div>
     <div class="tab-innar">
		<ul>
		    <li>
				<?php echo $this->Js->link("Credit Banner Ads", array('controller'=>'bannerad', "action"=>"index"), array(
					'update'=>'#banneradpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'active'
				));?>
		    </li>
		    <li>
				<?php echo $this->Js->link("Plan Banner Ads", array('controller'=>'bannerad', "action"=>"planmember"), array(
					'update'=>'#banneradpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
		</ul>
	</div>

    <div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Userbanner',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'bannerad','action'=>'index')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
					<?php echo $this->Form->input('searchby', array(
						  'type' => 'select',
						  'options' => array('all'=>'Select Parameter', 'banner_id'=>'Banner Id', '468x60'=>'Banner Style 468x60', '125x125'=>'Banner Style 125x125', 'running'=>'Running Banners', 'expire'=>'Expire Banners', 'active'=>'Active Banners', 'inactive'=>'Inactive / Unapproved Banners', 'user_id'=>'Member Id'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => '',
						  'onchange'=>'if($(this).val()=="banner_id" || $(this).val()=="user_id" || $(this).val()=="all"){$("#SearchFor").show(500);}else{$("#SearchFor").hide(500);}'
					));?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=='468x60' || $searchby=='125x125' || $searchby=='active' || $searchby=='inactive' || $searchby=='expire' || $searchby=='running'){ echo "style='display:none'";} ?>>
			<span>Search For :</span>
			<span class="searchforfields_s"><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
                  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#banneradpage',
                  'class'=>'searchbtn',
                  'controller'=>'bannerad',
                  'action'=>'index',
                  'url'=> array('controller' => 'bannerad', 'action' => 'index')
                ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
	
        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#banneradpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'bannerad', 'action'=>'index')
        ));
        $currentpagenumber=$this->params['paging']['Userbanner']['page'];
        ?>

<div id="gride-bg">
    <div class="Xpadding10">
	
	<?php echo $this->Form->create('Userbanner',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'bannerad','action'=>'userbannerstatus')));?>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php 
		echo $this->Form->checkbox('selectAllCheckboxes', array(
		  'hiddenField' => false,
		  'onclick' => 'selectAllCheckboxes("bannerIds",this.checked)'
		));
		?>
		<label for="UserbannerSelectAllCheckboxes"></label>
	</div>
	<div class="addnew-button">
        <?php if(!isset($SubadminAccessArray) || in_array('bannerad',$SubadminAccessArray) || in_array('bannerad/adminpanel_userbanneradd',$SubadminAccessArray)){ ?>
                <?php echo $this->Js->link("+ Add New", array('controller'=>'bannerad', "action"=>"userbanneradd"), array(
                    'update'=>'#banneradpage',
                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                    'escape'=>false,
                    'class'=>'btnorange'
                ));?>
        <?php } ?>
		<?php if(!isset($SubadminAccessArray) || in_array('bannerad',$SubadminAccessArray) || in_array('bannerad/adminpanel_userbannerstatus',$SubadminAccessArray)){
			echo $this->Js->submit('Approve Selected Record(s)', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'escape'=>false,
			  'update'=>'#banneradpage',
			  'class'=>'btngray',
			  'div'=>false,
			  'controller'=>'bannerad',
			  'action'=>'userbannerstatus',
			  'confirm' => 'Are You Sure?',
			  'url'   => array('controller' => 'bannerad', 'action' => 'userbannerstatus')
			));
		} ?>
		
	</div>
	<div class="clear-both"></div>
	
		<div class="tablegrid extramarginforbanners">
				<div class="tablegridheader">
				    <div style="width: 50px;">
						<?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('M. Id', array('controller'=>'bannerad', "action"=>"index/0/0/user_id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#banneradpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Member Id'
                        ));?>
					</div>
                    <div style="width: 100px;"><?php echo 'Username';?></div>
                    <div style="width: 30%;"><?php echo "Banner";?></div>
                    <div style="width: 100px;"><?php echo "Click(s)";?></div>
                    <div style="width: 100px;"><?php echo "CTR";?></div>
                    <div style="width: 200px;">
                        Credits (<?php echo $this->Js->link('Used', array('controller'=>'bannerad', "action"=>"index/0/0/disp_counter/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#banneradpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Used Credits'
                        ));
						?>/<?php 
						echo $this->Js->link('Allowed', array('controller'=>'bannerad', "action"=>"index/0/0/credit/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#banneradpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Allowed Credits'
                        ));?>)
                    </div>
                    <div style="width: 70px;">
                        <?php echo $this->Js->link('Action', array('controller'=>'bannerad', "action"=>"index/0/0/status/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#banneradpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Status'
                        ));?>
                    </div>
                    <div style="width: 50px;"></div>
                </div>
                <?php foreach ($userbanners as $userbanner):?>
                    <div class="tablegridrow">
					    <div>
							<?php 
							echo $this->Js->link($userbanner['Userbanner']['user_id'], array('controller'=>'member', "action"=>"memberadd/".$userbanner['Userbanner']['user_id']."/top/bannerad/index~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
						</div>
                        <div>
							<?php 
							echo $this->Js->link($userbanner['Member']['user_name'], array('controller'=>'member', "action"=>"memberadd/".$userbanner['Userbanner']['user_id']."/top/bannerad/index~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
						</div>
                        <div class="textcenter">
							<a href="<?php echo $userbanner['Userbanner']['siteurl'];?>" target="_blank">
								<?php
									if($userbanner['Userbanner']['style']=='125x125'){$height="50px";$maxwidth='';}else{$height="90%";$maxwidth="468px";}
									if($userbanner['Userbanner']['is_upload']==1)	
										echo '<img src="'.$SITEURL.'img/banners/'.$userbanner['Userbanner']['photo'].'" alt="'.$userbanner['Userbanner']['title'].'" width="'.$height.'" style="max-width:'.$maxwidth.'" />';
									else
										echo '<img src="'.$userbanner['Userbanner']['photo'].'" alt="'.$userbanner['Userbanner']['title'].'" width="'.$height.'" style="max-width:'.$maxwidth.'" />';
								?>
							</a>
							<br><span><b>Date : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $userbanner['Userbanner']['create_date']); ?></span>
                        </div>
                        <div><?php echo $userbanner['Userbanner']['click']; ?></div>
                        <div><?php echo @number_format((($userbanner['Userbanner']['click']*100)/$userbanner['Userbanner']['disp_counter']),2,'.',''); ?>%</div>
                        <div><?php echo $userbanner['Userbanner']['disp_counter'].'/'.$userbanner['Userbanner']['credit']; ?></div>
                        <div class="textcenter">
								<span class="actionmenu">
								  <span class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
												
												<?php if(!isset($SubadminAccessArray) || in_array('bannerad',$SubadminAccessArray) || in_array('bannerad/adminpanel_userbanneradd/$',$SubadminAccessArray)){?>
												<li>
												<?php
													echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Banner'))." Edit", array('controller'=>'bannerad', "action"=>"userbanneradd/".$userbanner['Userbanner']['banner_id']), array(
														'update'=>'#banneradpage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>''
													));
												?>
												</li>
												<?php }?>
										
												<?php if(!isset($SubadminAccessArray) || in_array('bannerad',$SubadminAccessArray) || in_array('bannerad/adminpanel_userbannerstatus',$SubadminAccessArray)){?>
												<li>
												<?php
													if($userbanner['Userbanner']['status']==0){
														$statusaction='1';
														$statusicon='red-icon.png';
														$statustext='Activate';
													}else{
														$statusaction='0';
														$statusicon='blue-icon.png';
														$statustext='Inactivate';}
													echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'bannerad', "action"=>"userbannerstatus/".$statusaction."/".$userbanner['Userbanner']['banner_id']."/".$userbanner['Userbanner']['user_id']."/".$currentpagenumber), array(
														'update'=>'#banneradpage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>''
													));
												?>
												</li>
												<?php }?>
												
												<?php if(!isset($SubadminAccessArray) || in_array('bannerad',$SubadminAccessArray) || in_array('bannerad/adminpanel_userbannerremove',$SubadminAccessArray)){?>
												<li>
												<?php
													echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Banner'))." Delete", array('controller'=>'bannerad', "action"=>"userbannerremove/".$userbanner['Userbanner']['banner_id']."/".$currentpagenumber), array(
														'update'=>'#banneradpage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false,
														'class'=>'',
														'confirm'=>"Do You Really Want to Delete This Banner Ad?"
													));
												?>
												</li>
												<?php }?>
									</ul>
								  </span>
								</span>
                        </div>
                        <div class="checkbox">
								<?php
								if($userbanner['Userbanner']['status']==0)
								{
									echo $this->Form->checkbox('bannerIds.', array(
									  'value' => $userbanner['Userbanner']['banner_id'],
									  'id'=>'bannerIds'.$userbanner['Userbanner']['banner_id'],
									  'class' => 'bannerIds',
									  'hiddenField' => false
									));
								echo '<label for="bannerIds'.$userbanner['Userbanner']['banner_id'].'"></label>';
								}
								?>
						</div>
                    </div>
                <?php endforeach; ?>
        </div>
		<?php if(count($userbanners)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php echo $this->Form->end();
        if($this->params['paging']['Userbanner']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Userbanner',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'bannerad','action'=>'index/0/rpp')));?>
			<div class="resultperpage">
				<label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
				</label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#banneradpage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'bannerad',
                  'action'=>'index/0/rpp',
                  'url'   => array('controller' => 'bannerad', 'action' => 'index/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#banneradpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>