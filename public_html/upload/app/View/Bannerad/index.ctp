<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 15-10-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(!$ajax){?>
<div id="banneradpage">
<?php } //if(!$ajax){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<?php // Top menu code starts here ?>
<div class="comisson-bg mobilecss">
	<div class="commison-menu">
		<ul>
			<?php if($SITECONFIG['enable_bannerads']==1){?>
			<li>
				<?php
					echo $this->Js->link(__('Banner Ads'), array('controller'=>'bannerad', "action"=>"index"), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip act',
						'title'=>__('Regular Earning History')
					));
				?>
			</li>
			<?php }
			if(strpos($SITECONFIG["banneradplansetting"],'isenable|1') !== false){ ?>
			<li>
				<?php
					echo $this->Js->link(__('Banner Ad Plans'), array('controller'=>'bannerad', "action"=>"plans"), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Banner Ad Plans')
					));
				?>
			</li>
			<?php } ?>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Top menu code ends here ?>

<div class="main-box-eran">
	
	<?php // Search box code starts here ?>
	<div class="floatleft searchlabel"><?php echo __("Advanced Search");?></div>
	<div class="floatright"><a href="javascript:void(0)" onclick="togglebox('.searchlabel', this,'<?php echo __("[+] Show Search Box");?>','<?php echo __("[-] Hide Search Box");?>');"><?php echo __("[-] Hide Search Box");?></a></div>
	<div class="clear-both"></div>
	<div class="advsearchbox searchlabel">
	<?php echo $this->Form->create('Userbanner',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'bannerad','action'=>'index')));?>
		<div class="searchboxrow">
			<span class="searchboxcol1"><?php echo __("Search By");?>  :&nbsp;</span>
			<div class="select-dropdown smallsearch">
				<label>
					<?php echo $this->Form->input('searchby', array(
							'type' => 'select',
							'options' => array('all'=>__('All'), '468x60'=>'468X60', '125x125'=>'125X125'),
							'selected' => $searchby,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
					  ));?>
				</label>
			</div>
			<div class="textcenter margint5">
				<?php echo $this->Js->submit(__('Update Results'), array(
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#banneradpage',
					'class'=>'add-new',
					'div'=>false,
					'controller'=>'bannerad',
					'action'=>'index',
					'url'   => array('controller' => 'bannerad', 'action' => 'index')
				  ));?>
			</div>
		</div>
	<?php echo $this->Form->end();?>
	</div>
	<?php // Search box code ends here ?>

<div class="clear-both"></div>
<div class="height10"></div>
<?php //}?>

<?php echo $this->Javascript->link('allpage');?>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#banneradpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'bannerad', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Userbanner']['page']; ?>
		<div class="padding-left-serchtabal">
			<?php echo $this->Js->link(__("Add New"), array('controller'=>'bannerad', "action"=>"add"), array(
				'update'=>'#banneradpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button'
			));?>
		</div>
		
		<?php // Banner ads table starts here ?>
		<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
		<div class="divtable">
			<div class="divthead">
				<div class="divtr tabal-title-text">
				  <div class="divth textcenter vam"><?php echo __("Banner");?></div>
				  <div class="divth textcenter vam"><?php echo __("CTR");?></div>
				  <div class="divth textcenter vam">
				  		<?php 
						if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
						echo $this->Js->link(__('Clicks'), array('controller'=>'bannerad', "action"=>"index/0/click/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#banneradpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Clicks')
						));?>
				  </div>
				  <div class="divth textcenter vam">
				  		<?php 
						echo $this->Js->link(__('Used Credits'), array('controller'=>'bannerad', "action"=>"index/0/disp_counter/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#banneradpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Used Credits')
						));?>
				  </div>
				  <div class="divth textcenter vam">
						<?php echo $this->Js->link(__('Allowed Credits'), array('controller'=>'bannerad', "action"=>"index/0/credit/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#banneradpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Allowed Credits')
						));?>
				  </div>
				  <div class="divth textcenter vam">
						<?php echo $this->Js->link(__('Status'), array('controller'=>'bannerad', "action"=>"index/0/status/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#banneradpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Status')
						));?>
				  </div>
				  <div class="divth textcenter vam"><?php echo __("Action");?></div>
				</div>
			</div>
			<div class="divtbody">
				<?php $i=1;
				foreach ($userbanners as $userbanner):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam">
						<?php
							if($userbanner['Userbanner']['style']=='468x60')
								$bannerimg='<img src="'.$userbanner['Userbanner']['photo'].'" alt="" style="width:90%;max-height:60px;max-width:468px;" />';
							else
								$bannerimg='<img src="'.$userbanner['Userbanner']['photo'].'" alt="" style="width:70%;max-height:125px;max-width:125px;" />';
						?>
						<?php echo $this->Js->link($bannerimg, array('controller'=>'bannerad', "action"=>"add/".$userbanner['Userbanner']['banner_id']), array(
							'update'=>'#banneradpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Edit Banner')
						));?>
						</div>
						<div class="divtd textcenter vam"><?php echo @number_format((($userbanner['Userbanner']['click']*100)/$userbanner['Userbanner']['disp_counter']),2,'.',''); ?>%</div>
						<div class="divtd textcenter vam"><?php echo $userbanner['Userbanner']['click'];?></div>
						<div class="divtd textcenter vam"><?php echo $userbanner['Userbanner']['disp_counter'];?></div>
						<div class="divtd textcenter vam"><?php echo $userbanner['Userbanner']['credit'];?></div>
						<div class="divtd textcenter vam">
							<?php 
							if($userbanner['Userbanner']['status']==0){
								$statusaction='1';
								$statusicon='red-icon.png';
								$statustext='Pending';
							}else{
								$statusaction='0';
								$statusicon='blue-icon.png';
								$statustext='Approved';}
							echo $this->html->image($statusicon, array('alt'=>__($statustext), 'title'=>__($statustext), 'class'=>'vtip'));
							?>
						</div>
						<div class="divtd textcenter vam">
							<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit Banner'))), array('controller'=>'bannerad', "action"=>"add/".$userbanner['Userbanner']['banner_id']), array(
								'update'=>'#banneradpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Edit Banner')
							));?>
							
							<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>__('Delete Banner'))), array('controller'=>'bannerad', "action"=>"remove/".$userbanner['Userbanner']['banner_id']."/".$currentpagenumber), array(
								'update'=>'#banneradpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Delete Banner'),
								'confirm'=>__("Do you really want to delete this Banner Ad?")
							));?>
						</div>
					</div>
				<?php $i++;endforeach; ?>
			</div>
		</div>
		<?php if(count($userbanners)==0) echo "<div class='tabal-content-white'>".__('No records available')."</div>"; ?>
		<?php // Banner ads table ends here ?>
		
		<?php // Paging code starts here ?>
		<?php $pagerecord=$this->Session->read('pagerecord');
		if($this->params['paging']['Userbanner']['count']>$pagerecord)
		{?>
		<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Userbanner',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'bannerad','action'=>'index/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#banneradpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'bannerad',
						  'action'=>'index/rpp',
						  'url'   => array('controller' => 'bannerad', 'action' => 'index/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
		</div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		
	</div>
<?php if(!$ajax){?>	
</div>
<?php } ?>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>