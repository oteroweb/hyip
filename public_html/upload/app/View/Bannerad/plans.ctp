<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 15-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($EnableBannerad==1) { ?>
<?php if(!$ajax){ ?>
<div id="banneradpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<?php // Code for showing free available plans start
if(isset($pending_free_plans) && count($pending_free_plans)){
?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Free Plans Available");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php // Banner Ad free Plans table starts here ?>
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam"><?php echo __('Plan Name');?></div>
				<div class="divth textcenter vam"><?php echo __("Action");?></div>
			</div>
	        </div>
	        <div class="divtbody">
		        <tr><td colspan="8" class="ovw-padding-tabal"></td></tr>
		        <?php
		        $i = 0;
		        foreach ($pending_free_plans as $pending_free_plan):
			       if($i%2==0){$class='white-color';}else{$class='gray-color';}
			       ?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $pending_free_plan['Banneradplan']['plan_name'];?></div>
					<div class="divtd textcenter vam">
					<?php
						echo $this->Js->link('<span class="purchasebutton getitbutton">Get it</span>', array('controller'=>'bannerad', "action"=>"getfreebannerad/".$pending_free_plan['Pending_free_plan']['id']), array(
							'update'=>'#banneradpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Get it')
						));
					?>
					</div>
				</div>
		        <?php $i++;endforeach; ?>
	       </div>
	</div>
	<?php // Banner Ad free Plans table ends here ?>
</div>
<div class="height5"></div>
<?php } // Code for showing free available plans end ?>

<?php // Top menu code starts here ?>
<div class="comisson-bg mobilecss">
	<div class="commison-menu">
		<ul>
			<?php if($SITECONFIG['enable_bannerads']==1){?>
			<li>
				<?php
					echo $this->Js->link(__('Banner Ads'), array('controller'=>'bannerad', "action"=>"index"), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Regular Earning History')
					));
				?>
			</li>
			<?php }
			if(strpos($SITECONFIG["banneradplansetting"],'isenable|1') !== false){ ?>
			<li>
				<?php
					echo $this->Js->link(__('Banner Ad Plans'), array('controller'=>'bannerad', "action"=>"plans"), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip act',
						'title'=>__('Banner Ad Plans')
					));
				?>
			</li>
			<?php } ?>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Top menu code ends here ?>

<div id="UpdateMessage"></div>
<div class="main-box-eran">
	<div class="clear-both"></div>
	<div class="height5"></div>
		
		<?php // Banner Ad Plans table starts here ?>
		<div class="divtable">
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam"><?php echo __('Plan Name');?></div>
					<div class="divth textcenter vam"><?php echo __('Price');?></div>
					<div class="divth textcenter vam"><?php echo __("Action");?></div>
				</div>
	       </div>
	       <div class="divtbody">
		       <tr><td colspan="8" class="ovw-padding-tabal"></td></tr>
		       <?php
		       $i = 0;
		       foreach ($Banneradplandata as $Banneradplan):
			       if($i%2==0){$class='white-color';}else{$class='gray-color';}
			       ?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $Banneradplan['Banneradplan']['plan_name'];?></div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($Banneradplan['Banneradplan']['price']*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
						<div class="divtd textcenter vam">
						<?php
							echo $this->Js->link('<span class="purchasebutton">Purchase</span>', array('controller'=>'bannerad', "action"=>"purchase/".$Banneradplan['Banneradplan']['id']), array(
								'update'=>'#banneradpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Purchase')
							));
						?>
						</div>
					</div>
		       <?php $i++;endforeach; ?>
	       </div>
	</div>
	<?php if(count($Banneradplandata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
	<?php // Banner Ad Plans table ends here ?>
</div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('My Banner Ad Plans');?></div>
	<div class="clear-both"></div>
</div>
	
<div class="main-box-eran">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#banneradpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'bannerad', 'action'=>'plans')
	));
	$currentpagenumber=$this->params['paging']['Bannerad']['page'];
	?>
	<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
	<div class="clear-both"></div>
	<div class="height5"></div>
		
	<?php // My Banner Ad Plans table starts here ?>
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam">
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link(__("Id"), array('controller'=>'bannerad', "action"=>"plans/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Id')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__("Plan"), array('controller'=>'bannerad', "action"=>"plans/0/plan_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Plan')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__("Purchase Date"), array('controller'=>'bannerad', "action"=>"plans/0/pruchase_date/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Purchase Date')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__("Displayed"), array('controller'=>'bannerad', "action"=>"plans/0/display_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Displayed')
					));?>
				</div>
				<div class="divth textcenter vam">
					<?php 
					echo $this->Js->link(__("Clicked"), array('controller'=>'bannerad', "action"=>"plans/0/click_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Sort By').' '.__('Clicked')
					));?>
				</div>
				<div class="divth textcenter vam"><?php echo __("Status"); ?></div>
				<div class="divth textcenter vam"><?php echo __("Action"); ?></div>
			</div>
		</div>
		<div class="divtbody">
			<?php $i=1;
			foreach ($bannerads as $bannerad):
			if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $bannerad['Bannerad']['id'];?></div>
					<div class="divtd textcenter vam">
						<a href="#" class="vtip" title="<b><?php echo __("Plan Name"); ?> : </b><?php echo $bannerad['Banneradplan']['plan_name']; ?><br><b><?php echo __("Banner Size"); ?> : </b><?php echo $bannerad['Bannerad']['banner_size']; ?>"> <?php echo $bannerad['Bannerad']['plan_id']; ?></a>
					</div>
					<div class="divtd textcenter vam">
						<p><b><?php echo __("Purchase Date"); ?> : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $bannerad['Bannerad']['pruchase_date']); ?></p>
							<a href="<?php echo $bannerad['Bannerad']['site_url'];?>" target="_blank" class="vtip" title="<b><?php echo __("Title"); ?> : </b><?php echo $bannerad['Bannerad']['title']; ?>">
								<?php
									if($bannerad['Bannerad']['banner_size']=='125x125'){$height="50px";$maxwidth='';}
									elseif($bannerad['Bannerad']['banner_size']=='468x60'){$height="80%";$maxwidth="468px";}
									else{$height="80%";$maxwidth="728px";}
									echo '<img src="'.$bannerad['Bannerad']['banner_url'].'" alt="'.$bannerad['Bannerad']['title'].'" width="'.$height.'" style="max-width:'.$maxwidth.';" />';
								?>
							</a>
							<div class="noborderonmobile"><b><?php echo __("Approved Date"); ?> : </b><?php if($bannerad['Bannerad']['expire_date'] != '0000-00-00'){ echo $this->Time->format($SITECONFIG["timeformate"], $bannerad['Bannerad']['approve_date']); } else { echo '-';}?></div>
							<div class="noborderonmobile"><b><?php echo __("Expiry Date"); ?> : </b><?php if($bannerad['Bannerad']['expire_date'] != '0000-00-00') { echo $this->Time->format($SITECONFIG["timeformate"], $bannerad['Bannerad']['expire_date']); }else { echo '-';} ?></div>
					</div>
					<div class="divtd textcenter vam"><?php echo $bannerad['Bannerad']['display_counter'];?></div>
					<div class="divtd textcenter vam"><?php echo $bannerad['Bannerad']['click_counter'];?></div>
					<div class="divtd textcenter vam">
						<?php if ($bannerad['Bannerad']['expire_date'] < date('Y-m-d H:i:s') && $bannerad['Bannerad']['expire_date'] != '0000-00-00' && $bannerad['Bannerad']['expire_date'] != NULL) { 
							echo __("Expired");
						}
						elseif($bannerad['Bannerad']['status']==1) {
							echo __("Running");
						}
						elseif($bannerad['Bannerad']['status']==0){
							echo __("Pending");
						}
						?>
					</div>
					<div class="divtd textcenter vam">
						<?php
							echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit Banner Ad'))), array('controller'=>'bannerad', "action"=>"myplanadd/".$bannerad['Bannerad']['id']), array(
								'update'=>'#banneradpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Edit Banner Ad')
							));
						?>
					</div>
				</div>
			<?php $i++;endforeach; ?>
		</div>
	</div>
	<?php if(count($bannerads)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
	<?php // My Banner Ad Plans table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Bannerad']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
	<div class="pag-float-left">
		<div class="ul-bg">
			<ul class="nice_paging">
			<?php 
			foreach($resultperpage as $rpp)
			{
				?>
				<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
					<?php 
					echo $this->Form->create('Banneradplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'bannerad','action'=>'plans/rpp')));
					echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
				
					echo $this->Js->submit($rpp, array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#banneradpage',
					  'class'=>'resultperpagebutton',
					  'div'=>false,
					  'controller'=>'bannerad',
					  'action'=>'plans/rpp',
					  'url'   => array('controller' => 'bannerad', 'action' => 'plans/rpp')
					));
					echo $this->Form->end();
					?>
				</li>
				<?php 
			}?>
			</ul>
		<div class="clear-both"></div>
		</div>
	</div>
	<?php }?>
	<div class="floatright ul-bg">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
		</ul>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>
	
</div>

<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>