<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 10-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List#Pending_Request" target="_blank">Help</a></div>
<div class="tab-innar nomargin">
	<ul>
		<li>
			<?php echo $this->Js->link("Withdrawals", array('controller'=>'member', "action"=>"withdrawhistory/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Pending Request", array('controller'=>'member', "action"=>"withdrawrequest/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'active'
			));?>
		</li>
	</ul>
</div>
<div class="clear-both"></div>
<div class="height10"></div>
<div id="UpdateMessage"></div>

	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberdetailpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'withdrawrequest/'.$member_id)
	));
	$currentpagenumber=$this->params['paging']['Withdraw']['page'];
	?>
	<div class="margin10">
	<div class="greenbottomborder">
	<div class="height10"></div>
	<div class="display-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Withdraw',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'withdrawrequest')));?>
	
	<div class="tablegrid">
		<div class="tablegridheader">
			<div>
				<?php 
				if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
				echo $this->Js->link('M. Id', array('controller'=>'member', "action"=>"withdrawrequest/".$member_id."/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Member Id'
				));?>
			</div>
			<div>
				<?php 
				echo $this->Js->link('Req. Dt.', array('controller'=>'member', "action"=>"withdrawrequest/".$member_id."/0/0/req_dt/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Request Date'
				));?>
			</div>
			<div>Username</div>
			<div>
				<?php echo $this->Js->link('Payment Processor', array('controller'=>'member', "action"=>"withdrawrequest/".$member_id."/0/0/payment_processor/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Payment Processor'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Payment Processor Id', array('controller'=>'member', "action"=>"withdrawrequest/".$member_id."/0/0/pro_acc_id/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Payment Processor Id'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Amount', array('controller'=>'member', "action"=>"withdrawrequest/".$member_id."/0/0/amount/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Amount'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Fees', array('controller'=>'member', "action"=>"withdrawrequest/".$member_id."/0/0/fee/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Fees'
				));?>
			</div>
                </div>
		
		<?php
			$i = 0;
			$currentpage=array();
			foreach ($withdraws as $withdraw):
				$processor=$withdraw['Withdraw']['payment_processor'];
				@$currentpage[$processor]+=$withdraw['Withdraw']['amount'];
				$class = 'class="blue-color"';if ($i++ % 2 == 0){$class = 'class="white-color"';}
				?>
				
				<div class="tablegridrow">
					<div><?php echo $withdraw['Withdraw']['member_id'];?></div>
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $withdraw['Withdraw']['req_dt']); ?></div>
					<div><?php echo $withdraw['Member']['user_name']; ?></div>
					<div><?php echo $withdraw['Withdraw']['payment_processor']; ?></div>
					<div><?php echo $withdraw['Withdraw']['pro_acc_id']; ?></div>
					<div>$<?php echo round($withdraw['Withdraw']['amount'],4); ?></div>
					<div>$<?php echo round($withdraw['Withdraw']['fee'],4); ?></div>
				</div>
			<?php endforeach;?>
	</div>
	<?php if(count($withdraws)==0){ echo '<div class="norecordfound">No records available</div>';}?>
    <div class="height5"></div>
	<div class="margin10">	
		<?php echo $this->Form->end(); ?>
		<?php
		if($this->params['paging']['Withdraw']['count']>$this->Session->read('pagerecord'))
		{?>
		<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
		<div class="floatleft margintop19">
			<?php echo $this->Form->create('Withdraw',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'withdrawrequest/'.$member_id.'/rpp')));?>
			<div class="resultperpage">
				<label>
					<?php 
					echo $this->Form->input('resultperpage', array(
					  'type' => 'select',
					  'options' => $resultperpage,
					  'selected' => $this->Session->read('pagerecord'),
					  'class'=>'',
					  'label' => false,
					  'div'=>false,
					  'style' => '',
					  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
					));
					?>
				</label>
			</div>
			<span id="resultperpageapply" style="display:none;">
				<?php echo $this->Js->submit('Apply', array(
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#memberdetailpage',
				  'class'=>'',
				  'div'=>false,
				  'controller'=>'member',
				  'action'=>'withdrawrequest/rpp',
				  'url'   => array('controller' => 'member', 'action' => 'withdrawrequest/'.$member_id.'/rpp')
				));?>
			</span>
			<?php echo $this->Form->end();?>
		</div>
		<?php }?>
		<div class="floatright">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
		</ul>
		</div>
		<div class="clear-both"></div>
		<div class="height10"></div>
	</div>
	</div>
	<div class="height7"></div>
</div>
<div class="height10"></div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>