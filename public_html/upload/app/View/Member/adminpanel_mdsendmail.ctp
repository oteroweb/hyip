<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List#Send_Message" target="_blank">Help</a></div>
<div class="height10"></div>

<div id="UpdateMessage"></div>

		<?php echo $this->Form->create('Template_email',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'sendmailaction')));?>
		
		<?php 
			echo $this->Form->input('member_id', array('type'=>'hidden', 'value'=>$member_id, 'label' => false));
		?>
			
			<div class="frommain">
				<div class="fromnewtext">Subject : <span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('subject', array('type'=>'text', 'value'=>'', 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
				<div class="fromnewtext">Message : <span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('message', array('type'=>'textarea', 'value'=>'','label' => false, 'div' => false, 'class'=>'form-textarea', 'style'=>'width:99%;'));?>
				</div>
				<div class="fromnewtext">Mail Template Fields :</div>
				<div class="fromborderdropedown3">
					<div class="linktotext"><a onClick="AddTags(document.getElementById('Template_emailMessage'),'[SiteTitle]');" title="click to paste" href="javascript:void(0);"><?php echo '[SiteTitle]';?></a><?php echo ' - '. 'Title of The Site';?></div>
						<div class="linktotext"><a onClick="AddTags(document.getElementById('Template_emailMessage'),'[FirstName]');" title="click to paste" href="javascript:void(0);"><?php echo '[FirstName]';?></a><?php echo ' - '. "Member's First Name";?></div>
						<div class="linktotext"><a onClick="AddTags(document.getElementById('Template_emailMessage'),'[LastName]');" title="click to paste" href="javascript:void(0);"><?php echo '[LastName]';?></a><?php echo ' - '. "Member's Last Name";?></div>
						<div class="linktotext"><a onClick="AddTags(document.getElementById('Template_emailMessage'),'[UserName]');" title="click to paste" href="javascript:void(0);"><?php echo '[UserName]';?></a><?php echo ' - '. "Member's Username";?></div>
						<div class="linktotext"><a onClick="AddTags(document.getElementById('Template_emailMessage'),'[Email]');" title="click to paste" href="javascript:void(0);"><?php echo '[Email]';?></a><?php echo ' - '. "Member's Email";?></div>
						<div class="linktotext"><a onClick="AddTags(document.getElementById('Template_emailMessage'),'[RefLink]');" title="click to paste" href="javascript:void(0);"><?php echo '[RefLink]';?></a><?php echo ' - '. ' Member Referral Link';?></div>
				</div>
				<div class="formbutton">
					<?php echo $this->Js->submit('Send', array(
						  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage',
						  'class'=>'btnorange',
						  'controller'=>'member',
						  'div'=>false,
						  'action'=>'sendmailaction',
						  'url'   => array('controller' => 'member', 'action' => 'sendmailaction')
						));?>
				</div>
			</div>
		<?php echo $this->Form->end();?>
		<div class="height-12"></div>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>