<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List#Commissions_Bonus" target="_blank">Help</a></div>
<div class="clear-both"></div>
<div class="height21"></div>
    <div id="UpdateMessage"></div>
    
	
    <div class="serchmainbox">
	<div class="serchgreybox"><?php echo "Search Option";?></div>
	
	  <?php echo $this->Form->create('Commission',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'commission')));?>
	  
	  <div class="from-box">
		<div class="fromboxmain">
			<span><?php echo "Search By";?> :</span>
			<span>                     
			    <div class="searchoptionselect">
				<div class="select-main">
					<label>
					    <?php 
						echo $this->Form->input('searchby', array(
						      'type' => 'select',
						      'options' => array('all'=>'Select Parameter', 'comm_on_member_fee'=>'Comm. on Member Fees Only', 'comm_on_position'=>'Comm. on Position Only', 'bonus'=>'Quickener', 'matching'=>'Matching Bonus'),
						      'selected' => $searchby,
						      'class'=>'',
						      'label' => false,
						      'style' => ''
						));
					    ?>
					</label>
				</div>
			    </div>
			 </span>
		</div>
		<div class="fromboxmain" style="display:none">
		    <span>Search For :</span>
		    <span><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?></span>
		    
		</div>
	</div>
	<div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
		</div>
		<div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				    'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				    'update'=>'#memberdetailpage',
				    'class'=>'searchbtn',
				    'controller'=>'member',
				    'action'=>'commission',
				    'url'=> array('controller' => 'member', 'action' => 'commission/'.$member_id)
				));?>
			</span>
		</div>
	</div>
	  
	  <?php echo $this->Form->end();?>
</div>

<div id="gride-bg">
    
        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#memberdetailpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'member', 'action'=>'commission/'.$member_id)
        ));
        $currentpagenumber=$this->params['paging']['Commission']['page'];
        ?>
        <?php echo $this->Form->create('Commission',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'commissionremove')));?>
		<div class="height10"></div>
		<div class="display-text display-text-padding">
			<?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?>
		</div>
        <div class="clear-both"></div>
	
	<div class="tablegrid">
	    <div class="tablegridheader">
		    <div>
                        <?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('Id', array('controller'=>'member', "action"=>"commission/".$member_id."/0/0/tran_id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#memberdetailpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Id'
                        ));?>
                    </div>
		    <div>
                        <?php echo $this->Js->link('Date', array('controller'=>'member', "action"=>"commission/".$member_id."/0/0/tran_dt/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#memberdetailpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Date'
                        ));?>
                    </div>
                    <div>
                        <?php echo $this->Js->link('From Member', array('controller'=>'member', "action"=>"commission/".$member_id."/0/0/from_id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#memberdetailpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By From Member'
                        ));?>
                    </div>
                    <div>
                        <?php echo $this->Js->link('Amount', array('controller'=>'member', "action"=>"commission/".$member_id."/0/0/amount/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#memberdetailpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Amount'
                        ));?>
                    </div>
                    <div>
                        <?php echo $this->Js->link('Type', array('controller'=>'member', "action"=>"commission/".$member_id."/0/0/description/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#memberdetailpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Type'
                        ));?>
                    </div>
	    </div>
	    
	    <?php
                $i = 0;
                foreach ($commissions as $commission):
                    $class = 'class="blue-color"';if ($i++ % 2 == 0){$class = 'class="white-color"';}
                    if($commission['Commission']['description']=='position') $description='Comm. on Position';
                    elseif($commission['Commission']['description']=='memberfee') $description='Comm. on Fees';
					elseif($commission['Commission']['description']=='bonus') $description='Quickener';
					elseif($commission['Commission']['description']=='matching') $description='Matching Bonus';
					else $description=ucwords($commission['Commission']['description']);
                    ?>
                    
                    <div class="tablegridrow">
                        <div><?php echo $commission['Commission']['tran_id'];?></div>
			<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $commission['Commission']['tran_dt']); ?></div>
                        <div>
			    <?php
			    if($commission['Commission']['from_id']!=0){ echo $this->Js->link($commission['From_Member']['user_name'], array('controller'=>'member', "action"=>"memberadd/".$commission['Commission']['from_id']."/top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						)); }else{echo $commission['From_Member']['user_name'];}
			    ?>
			</div>
                        <div>$<?php echo round($commission['Commission']['amount'],4); ?></div>
                        <div><?php echo $description; ?></div>
                    </div>
                <?php endforeach; ?>
		
	</div>
	<?php if(count($commissions)==0){ echo '<div class="norecordfound">No records available</div>';}?>
		<div class="margin10">
			<?php echo $this->Form->end();
			if($this->params['paging']['Commission']['count']>$this->Session->read('pagerecord'))
			{?>
			<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
			<div class="floatleft margintop19">
				<?php echo $this->Form->create('Commission',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'commission/rpp')));?>
				<div class="resultperpage">
				    <label>
					    <?php 
					    echo $this->Form->input('resultperpage', array(
					      'type' => 'select',
					      'options' => $resultperpage,
					      'selected' => $this->Session->read('pagerecord'),
					      'class'=>'',
					      'label' => false,
					      'div'=>false,
					      'style' => '',
					      'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
					    ));
					    ?>
				    </label>
				</div>
				</div>
				<span id="resultperpageapply" style="display:none;">
					<?php echo $this->Js->submit('Apply', array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#memberdetailpage',
					  'class'=>'',
					  'div'=>false,
					  'controller'=>'member',
					  'action'=>'commission/rpp',
					  'url'   => array('controller' => 'member', 'action' => 'commission/'.$member_id.'/rpp')
					));?>
				</span>
				<?php echo $this->Form->end();?>
			</div>
			<?php }?>
			<div class="floatright">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
			</ul>
			</div>
			<div class="clear-both"></div>
			<div class="height7"></div>
        </div>
	
</div>
</div>
<div class="height10"></div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>