<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 25-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<script type="text/javascript"> 
(function() {
$('#Member_ticket_reply').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();
</script>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Member Support');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">

<?php if($SITECONFIG["mticket_chk"]==1){ ?>
	
	<?php // Member Ticket view code starts here ?>
	<div class="contentboxhead color-black"><span><?php echo __("Member Support Ticket Details");?></span></div>
	<div class="divtable textcenter supportedit">
		<div class="divtbody">
			<div class="divtr gray-color">
				<div class="divtd textcenter vam"><?php echo __("Creator");?></div>
				<div class="divtd textleft vam">&nbsp;<?php echo $this->Session->read('user_fname')." ".$this->Session->read('user_lname');?></div>
			</div>
			<div class="divtr white-color">
				<div class="divtd textcenter vam"><?php echo __("Category");?></div>
				<div class="divtd textleft vam">&nbsp;<?php echo ucwords($member_ticketdata['Member_ticket']['category']);?></div>
			</div>
			<div class="divtr gray-color">
				<div class="divtd textcenter vam"><?php echo __("Subject");?></div>
				<div class="divtd textleft vam">&nbsp;<?php echo $member_ticketdata['Member_ticket']['subject'];?></div>
			</div>
			<div class="divtr white-color">
				<div class="divtd textcenter vam"><?php echo __("Date created");?></div>
				<div class="divtd textleft vam">&nbsp;<?php echo $this->Time->format($SITECONFIG["timeformate"], $member_ticketdata['Member_ticket']['dt_create']); ?></div>
			</div>
			<div class="divtr gray-color">
				<div class="divtd textcenter vam"><?php echo __("Date modified");?></div>
				<div class="divtd textleft vam">&nbsp;<?php echo $this->Time->format($SITECONFIG["timeformate"], $member_ticketdata['Member_ticket']['dt_lastupdate']); ?></div>
			</div>
			<div class="divtr white-color">
				<div class="divtd textcenter vam"><?php echo __("Status");?></div>
				<div class="divtd textleft vam">&nbsp;
					<?php
						if($member_ticketdata['Member_ticket']['status']==1){
							echo __('Open');
							echo $this->Js->link(__("Close Ticket"), array('controller'=>'member', "action"=>"supportedit/".$member_ticketdata['Member_ticket']['mt_id']."/status/0"), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'style'=>'margin-left:15px;'
							));
						}else{
							echo __('Close');
							echo $this->Js->link(__("Open ticket"), array('controller'=>'member', "action"=>"supportedit/".$member_ticketdata['Member_ticket']['mt_id']."/status/1"), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'style'=>'margin-left:15px;'
							));
						}
					?>
				</div>
			</div>
		</div>
	</div>
	<?php // Member Ticket view code ends here ?>
	
	<?php // New Message form code starts here ?>
	<?php echo $this->Form->create('Member_ticket_reply',array('id' => 'Member_ticket_reply', 'type' => 'post', 'type' => 'file', 'onsubmit' => 'return true;', 'autocomplete'=>'off', 'url'=>array('controller'=>'member','action'=>'supportreplay'))); ?>
	<?php echo $this->Form->input('mt_id', array('type'=>'hidden', 'value'=>$member_ticketdata['Member_ticket']['mt_id'], 'label' => false));?>
	<div class="form-box">
			<div class="formgridrow contact-gray">
				<div class="title-box" style="text-align: center;"><?php echo __("Enter your message");?></div>
			</div>
			<div class="form-row">
				<?php echo $this->Form->input('message', array('type'=>'textarea', 'id' => 'replymessage', 'label' => false, 'class'=>'formtextarea100', 'style'=>'width:99.4%;max-width:100%;'));?>
			</div>
			<div class="form-row">
				<div class="height7"></div>
				<div class="inlineblock">
					<span width="170" class="main-content-from-text"><?php echo __("Attachment");?> 1 :</span>
					<span><div class="browse_wrapper"><?php echo $this->Form->input('photo1', array('type' => 'file','label' => false)); ?></div></span>
				</div>
				<div class="inlineblock">
					<span class="main-content-from-text"><?php echo __("Attachment");?> 2 :</span>
					<span><div class="browse_wrapper"><?php echo $this->Form->input('photo2', array('type' => 'file','label' => false)); ?></div></span>
				</div>
				<div class="inlineblock">
					<span class="main-content-from-text"><?php echo __("Attachment");?> 3 :</span>
					<span><div class="browse_wrapper"><?php echo $this->Form->input('photo3', array('type' => 'file','label' => false)); ?></div></span>
				</div>
				<div class="inlineblock">
					<span class="main-content-from-text"><?php echo __("Attachment");?> 4 :</span>
					<span><div class="browse_wrapper"><?php echo $this->Form->input('photo4', array('type' => 'file','label' => false)); ?></div></span>
				</div>
				<div class="height7"></div>
		    </div>
			<div class="form-row">
				<div class="form-text">
					<div class="note-box textcenter"><b><span class="red-color">*<?php echo __("Note");?> :</span></b> <?php echo __("Attachments : Only .jpg, .jpeg, .gif, .png, .pdf, .doc and .docx files are allowed to upload. Max file size is 5 MB."); ?></div>
				</div>
		    </div>
			<div class="formbutton">
				<?php echo $this->html->link(__("Back"), array('controller' => 'member', 'action' => 'support'), array('class'=>'button', 'style'=>'float:left'));?>
				<?php echo $this->Form->submit(__('Reply'), array(
				  'class'=>'button',
				  'div'=>false
				));?>
			</div>	
			<div class="clear-both"></div>
	</div>
	<?php echo $this->Form->end();?>
	<?php // New Message form code ends here ?>
	
	<div class="height10"></div>
	
	<?php // Reply history code starts here ?>
	<?php if(count($member_ticket_replydata)>0){?>
		<?php $i=1;foreach ($member_ticket_replydata as $member_ticket_reply): ?>
			<div class="tabal-title-heading">
				<div class="float-left mobilefloatnone"><?php echo __("Message from");?> : <?php echo $member_ticket_reply['Member_ticket_reply']['replyer_name'];?></div>
				<div class="float-right mobilefloatnone"><?php echo $this->Time->format($SITECONFIG["timeformate"], $member_ticket_reply['Member_ticket_reply']['dt_reply']); ?></div>
				<div class="clear-both"></div>
			</div>		
				
			<div class="tabal-content-gray textleft">
				<div><?php echo nl2br($member_ticket_reply['Member_ticket_reply']['message']);?></div>
			</div>
			<?php if($member_ticket_reply['Member_ticket_reply']['attachments']!=''){ 
				$attachments=explode(',',$member_ticket_reply['Member_ticket_reply']['attachments']); ?>
				<div class="tabal-content-white textleft">	
					<div class="login-details-innar-box">
						<span class="notice-text">
							<strong><?php echo __('Attachment(s)'); ?> : &nbsp;</strong>
							<?php foreach($attachments as $key=>$attachment){ 
								$key+=1;
								echo '&nbsp;&nbsp;'.$this->Html->link(__('Attachment').' '.$key, array('controller'=>'member', "action"=>"supportattachment/R/".$member_ticket_reply['Member_ticket_reply']['ticket_id']."/".$attachment), array(
									'escape'=>false,
									'target'=>'_blank'
								)); ?>
							<?php } ?>
						</span>
					</div>
				</div>	
			<?php } ?>
			<div class="height10"></div>
		<?php $i++;endforeach; ?>
	<?php }else{?>
		<div class="title-box"><div><?php echo __("No replies found");?></div></div>
	<?php }?>
	<div class="height10"></div>

	<div class="tabal-title-heading">
		<div class="float-left mobilefloatnone"><?php echo __("Message from");?> : <?php echo $member_ticketdata['Member_ticket']['m_name'];?></div>
		<div class="float-right mobilefloatnone"><?php echo $this->Time->format($SITECONFIG["timeformate"], $member_ticketdata['Member_ticket']['dt_create']); ?></div>
		<div class="clear-both"></div>
	</div>
	<div class="tabal-content-gray textleft">
		<div class="divtd textleft vam"><?php echo stripslashes(nl2br($member_ticketdata['Member_ticket']['message']));?></div>
		<div class="divtd textcenter vam"></div>
	</div>
	<?php if($member_ticketdata['Member_ticket']['attachments']!=''){ 
		$attachments=explode(',',$member_ticketdata['Member_ticket']['attachments']); ?>
		<div class="tabal-content-white textleft">	
			<div class="login-details-innar-box"><span class="notice-text">
				<strong><?php echo __('Attachment(s)'); ?> : &nbsp;</strong>
				<?php foreach($attachments as $key=>$attachment){ 
					$key+=1;
					echo '&nbsp;&nbsp;'.$this->Html->link(__('Attachment').' '.$key, array('controller'=>'member', "action"=>"supportattachment/M/".$member_ticketdata['Member_ticket']['mt_id']."/".$attachment), array(
						'escape'=>false,
						'target'=>'_blank'
					)); ?>
				<?php } ?>
				</span>
			</div>
		</div>
	<?php } ?>
	<?php // Reply history code ends here ?>
	
	<?php }else{?>
		<div class="login-details">
			<div class="login-details-border">
				<div class="title-box"><?php echo __("Member Support");?></div>
					<div class="login-details-innar-box">
					<?php echo __("Admin has stopped new tickets currently. Please try later.");?>
				</div>
			</div>
		</div>
	<?php }?>
</div>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>