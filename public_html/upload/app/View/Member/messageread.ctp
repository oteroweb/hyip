<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 03-10-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(!$ajax){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="memberpage">
<?php } ?>
<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Admin Messages');?></div>
	<div class="clear-both"></div>
</div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'messageread')
	));
	$currentpagenumber=$this->params['paging']['Membermessage_history']['page'];
	?>
	<div class="main-box-eran">
		
		<?php // Member Message table starts here ?>
		<div class="padding-left-serchtabal"></div>
		<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
		<div class="textleft message-box-main">
			<?php $i=1;
			if(count($membermessage_historydata)>0)
			{
				foreach ($membermessage_historydata as $membermessage_history):
				$classheadding="message-heading-read";
				if(in_array($membermessage_history['Membermessage_history']['id'],$radmessage)){$classheadding="tabal-title-heading";}
				if($i%2==0){$class='tabal-content-white';}else{$class='tabal-content-gray';}
				 ?>
					<div class="<?php echo $classheadding ?>">
					<div class="message-subject" onclick="SlideInOut('<?php echo ".tabal-content".$membermessage_history['Membermessage_history']['id'];?>')"><?php echo nl2br(stripslashes($membermessage_history['Membermessage_history']['description'])); ?></div>
		
					<?php if(!in_array($membermessage_history['Membermessage_history']['id'],$radmessage)){
							echo "<div class='message-unread-button'>";
							 echo $this->Form->create('Membermessage_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'messageread/1')));
							echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$membermessage_history['Membermessage_history']["id"], 'label' => false));
							echo $this->Form->input('type', array('type'=>'hidden', 'value'=>$membermessage_history['Membermessage_history']["messagetype"], 'label' => false));
							echo $this->Js->submit(__('Mark as read'), array(
							  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
							  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							  'update'=>'#memberpage',
							  'class'=>'large white button',
							  'controller'=>'member',
							  'div'=>false,
							  'action'=>'messageread/1',
							  'url' => array('controller' => 'member', 'action' => 'messageread/1', 'plugin' => false)
							));
							echo $this->Form->end();
							echo "</div>";
					} ?>
		
					<div class="message-date">
						<?php echo __('Message').' '.__('Date'); ?> : <?php echo $this->Time->format($SITECONFIG["timeformate"], $membermessage_history['Membermessage_history']['messagedate']); ?>
					</div>
					<div class="clear-both"></div>
					</div>
					<div style="display: none;" class="<?php echo $class;?> <?php echo "tabal-content".$membermessage_history['Membermessage_history']['id'];?>">
						<?php echo nl2br(stripslashes($membermessage_history['Membermessage_history']['message'.$this->Session->read('Config.language')])); ?>
						
						<div class="clear-both"></div>
					</div>
					<div class="height10"></div>
				<?php $i++;endforeach; ?>
			<?php } else {  echo __('No records available'); } ?>
			<?php // Member Message table ends here ?>

  	   </div>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Membermessage_history']['count']>$pagerecord)
	{?>	
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Membermessage_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'messageread/0/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#memberpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'member',
						  'action'=>'messageread/0/rpp',
						  'url'   => array('controller' => 'member', 'action' => 'messageread/0/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
            </div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		
	</div>
<?php if(!$ajax){?>
</div>
<?php } ?>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>