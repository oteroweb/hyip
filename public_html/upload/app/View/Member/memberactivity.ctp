<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 25-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(!$ajax){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Activity Logs');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">

<?php // Search box code starts here ?>
<div class="floatleft searchlabel"><?php echo __("Advanced Search");?></div>
	<div class="floatright"><a href="javascript:void(0)" onclick="togglebox('.searchlabel', this,'<?php echo __("[+] Show Search Box");?>','<?php echo __("[-] Hide Search Box");?>');"><?php echo __("[-] Hide Search Box");?></a></div>
	<div class="clear-both"></div>
	<div class="advsearchbox searchlabel">
	<?php echo $this->Form->create('Memberlog',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'member','action'=>'memberactivity')));?>	
		<div class="inlineblock vat">
			<div>
				<span class="searchboxcol1"><?php echo __("Search By");?>  :&nbsp;</span>
				<div class="select-dropdown smallsearch">
					<label>
						<?php echo $this->Form->input('searchby', array(
								'type' => 'select',
								'options' => array('all'=>__('Select Parameter'), 'ipaddress'=>__('IP Address')),
								'selected' => $searchby,
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
						  ));?>
					</label>
				</div>
				  
			</div>
			<div>
				<span class="searchboxcol1"><?php echo __("Search For");?> :&nbsp;</span>
				<?php echo $this->Form->input('searchfor', array('type'=>'text', 'id'=>'searchfor', 'value'=>$searchfor, 'label' => false, 'class'=>'finece-from-box searchfortxt','div' => false));?>
			</div>
		</div>
		<div  class="inlineblock vat">
			<div class="margintb5">
				<span class="searchboxcol2"><?php echo __("From Date");?> :&nbsp;</span>
				<?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker searchdatetxt'));?>
			</div>
			<div>
				<span class="searchboxcol2"><?php echo __("To Date");?> :&nbsp;</span>
				<?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker searchdatetxt'));?>
			</div>
		</div>
		<div class="textcenter margint5">
			<?php echo $this->Js->submit(__('Update Results'), array(
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#memberpage',
				'class'=>'add-new',
				'div'=>false,
				'controller'=>'member',
				'action'=>'memberactivity',
				'url'   => array('controller' => 'member', 'action' => 'memberactivity')
			));?>
		</div>
		<div class="clear-both"></div>
	<?php echo $this->Form->end();?>
	</div>
	<?php // Search box code ends here ?>
	
	<div class="height10"></div>
<div class="clear-both"></div>
<?php }?>

<div id="memberpage">
<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'memberactivity')
	));
	$currentpagenumber=$this->params['paging']['Memberlog']['page']; ?>
		
		<?php // Activity Logs table starts here ?>
		<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			<div class="divtable textcenter">	
				<div class="divthead">
					<div class="divtr tabal-title-text">	
						<div class="divth textcenter vam">
						  <?php 
						  if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
						  echo $this->Js->link(__('Date'), array('controller'=>'member', "action"=>"memberactivity/0/logdate/".$sorttype."/".$currentpagenumber), array(
							  'update'=>'#memberpage',
							  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							  'escape'=>false,
							  'class'=>'vtip',
							  'title'=>__('Sort By').' '.__('Date')
						  ));?>
						</div>
						<div class="divth textcenter vam">
						  <?php 
						  echo $this->Js->link(__('Activity'), array('controller'=>'member', "action"=>"memberactivity/0/log/".$sorttype."/".$currentpagenumber), array(
							  'update'=>'#memberpage',
							  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							  'escape'=>false,
							  'class'=>'vtip',
							  'title'=>__('Sort By').' '.__('Activity')
						  ));?>
						</div>
						<div class="divth textcenter vam">
						  <?php echo $this->Js->link(__('IP Address'), array('controller'=>'member', "action"=>"memberactivity/0/ipaddress/".$sorttype."/".$currentpagenumber), array(
							  'update'=>'#memberpage',
							  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							  'escape'=>false,
							  'class'=>'vtip',
							  'title'=>__('Sort By').' '.__('IP Address')
						  ));?>
						</div>
					</div>
				</div>
				<div class="divtbody">
				<?php $i=1;
				foreach ($memberlogs as $memberlog):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
				  <div class="divtr  <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $memberlog['Memberlog']['logdate']); ?></div>
						<div class="divtd textcenter vam"><?php echo $memberlog['Memberlog']['log'];?></div>
						<div class="divtd textcenter vam"><?php echo $memberlog['Memberlog']['ipaddress'];?></div>
				  </div>
				<?php $i++;endforeach; ?>  
				</div>
			</div>
			<?php if(count($memberlogs)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // Activity Logs table ends here ?>
		
		<?php // Paging code starts here ?>
		<?php $pagerecord=$this->Session->read('pagerecord');
		if($this->params['paging']['Memberlog']['count']>$pagerecord)
		{?>
		<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Memberlog',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'memberactivity/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
					
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#memberpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'member',
						  'action'=>'memberactivity/rpp',
						  'url'   => array('controller' => 'member', 'action' => 'memberactivity/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
			</div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>

	</div>
</div>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>