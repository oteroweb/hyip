<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 10-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="tab-innar nomargin">
	<ul>
		<li>
			<?php echo $this->Js->link("Withdrawals", array('controller'=>'member', "action"=>"withdrawhistory/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'active'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Pending Request", array('controller'=>'member', "action"=>"withdrawrequest/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>''
			));?>
		</li>
	</ul>
</div>
<div class="clear-both"></div>
<div class="height21"></div>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List#Withdrawals" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<div class="serchmainbox">
	<div class="serchgreybox"><?php echo "Search Option";?></div>
	
	  <?php echo $this->Form->create('Withdrawal_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'withdrawhistory')));?>
	  
	  <div class="from-box">
		<div class="fromboxmain">
			<span><?php echo "Search By";?> :</span>
			<span>                     
			    <div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
							echo $this->Form->input('searchby', array(
							      'type' => 'select',
							      'options' => array('all'=>'Select Parameter', 'amount'=>'Amount', 'receiver_email'=>'Receiver Email', 'receiver_id'=>'Receiver Id', 'user_name'=>'Username', 'processor_id'=>'Payment Processor'),
							      'selected' => $searchby,
							      'class'=>'',
							      'label' => false,
							      'style' => '',
										  'onchange'=>'if($(this).val()=="processor_id"){generatecombo("data[Withdrawal_history][proc_name]","Processor","id","proc_name","'.$searchfor.'","#searchforcombo","'.$ADMINURL.'","");$("#searchall").hide();$("#searchforcombo").show(500);} else{$("#searchforcombo").hide();$("#searchall").show(500);}'
							));
						?>
					</label>
				</div>
			    </div>
			 </span>
		</div>
		<div class="fromboxmain">
		    <span >Search For :</span>
		    <span id="searchall" style='display:<?php if($searchby=="processor_id"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?></span>
		    <span id="searchforcombo" style='display:<?php if($searchby!="processor_id"){ echo "none";} ?>'></span>
		</div>
	</div>
	<div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
		</div>
		<div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#memberdetailpage',
					'class'=>'searchbtn',
					'controller'=>'member',
					'action'=>'withdrawhistory',
					'url'=> array('controller' => 'member', 'action' => 'withdrawhistory/'.$member_id)
				));?>
			</span>
		</div>
	</div>
	  
	  <?php echo $this->Form->end();?>
</div>


<div id="gride-bg">
<?php if($searchby=='processor_id'){ ?>
<script>
	generatecombo("data[Withdrawal_history][proc_name]","Processor","id","proc_name","<?php echo $searchfor; ?>","#searchforcombo","<?php echo $ADMINURL;?>","");
</script>
<?php } ?>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberdetailpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'withdrawhistory/'.$member_id)
	));
	$currentpagenumber=$this->params['paging']['Withdrawal_history']['page'];
	?>
	<div class="height10"></div>
    <div class="margin10"><div class="display-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="clear-both"></div></div>
	<?php echo $this->Form->create('Withdrawal_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'withdrawhistoryremove')));?>
	
	<div class="tablegrid">
		<div class="tablegridheader">
			<div>
				<?php 
				if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
				echo $this->Js->link('M. Id', array('controller'=>'member', "action"=>"withdrawhistory/".$member_id."/0/0/receiver_id/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Member Id'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Pay. Date', array('controller'=>'member', "action"=>"withdrawhistory/".$member_id."/0/0/dt/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Payment Date'
				));?>
			</div>
			<div>Username</div>
			<div>
				<?php echo $this->Js->link("Amount", array('controller'=>'member', "action"=>"withdrawhistory/".$member_id."/0/0/amount/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>"Sort By Amount"
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Sender', array('controller'=>'member', "action"=>"withdrawhistory/".$member_id."/0/0/sender_email/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Sender'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Receiver', array('controller'=>'member', "action"=>"withdrawhistory/".$member_id."/0/0/receiver_email/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Receiver'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Status', array('controller'=>'member', "action"=>"withdrawhistory/".$member_id."/0/0/return_desc/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Status'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Ref. No.', array('controller'=>'member', "action"=>"withdrawhistory/".$member_id."/0/0/refrence_no/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Reference No.'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Processor', array('controller'=>'member', "action"=>"withdrawhistory/".$member_id."/0/0/processor/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Processor'
				));?>
			</div>
                </div>
		
		<?php
		$i = 0;
			foreach ($withdrawal_historys as $withdrawal_history):
				$class = 'class="blue-color"';if ($i++ % 2 == 0){$class = 'class="white-color"';}
				?>
				
				<div class="tablegridrow">
					<div><?php echo $withdrawal_history['Withdrawal_history']['receiver_id'] ;?></div>
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $withdrawal_history['Withdrawal_history']['dt']); ?></div>
					<div><?php echo $withdrawal_history['Member']['user_name']; ?></div>
					<div>$<?php echo round($withdrawal_history['Withdrawal_history']['amount'],4); ?></div>
					<div><?php echo $withdrawal_history['Withdrawal_history']['sender_email']; ?></div>
					<div><?php echo $withdrawal_history['Withdrawal_history']['receiver_email']; ?></div>
					<div><?php echo $withdrawal_history['Withdrawal_history']['return_desc']; ?></div>
					<div><?php echo $withdrawal_history['Withdrawal_history']['refrence_no']; ?></div>
					<div><?php echo $withdrawal_history['Withdrawal_history']['processor']; ?></div>
				</div>
			<?php endforeach; ?>
    </div>
	<?php if(count($withdrawal_historys)==0){ echo '<div class="norecordfound">No records available</div>';}?>
	<div class="height5"></div>
	<div class="margin10">
    <?php echo $this->Form->end();
	if($this->params['paging']['Withdrawal_history']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Withdrawal_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'withdrawhistory/'.$member_id.'/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#memberdetailpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'member',
			  'action'=>'withdrawhistory/rpp',
			  'url'   => array('controller' => 'member', 'action' => 'withdrawhistory/'.$member_id.'/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
		<div class="clear-both"></div>
	</div>
	<div class="height7"></div>
</div>
</div>
<div class="height10"></div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>