<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 19-11-2014
  *********************************************************************/
?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="memberpage">
<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>
<script type="text/javascript">
(function() {
$('#MemberProfilePageForm').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();
</script>
	<?php if(!$profilesecurity) { ?>
	<?php if($themesubmenuaccess){ ?>
	<?php // Email change note starts here ?>
	<?php if(($SITECONFIG['emailconfirmation'] && $memberdata['Member']['email_verify']==0) || ($SITECONFIG['processorconfirmation'] && $memberdata['Member']['processor_verify']==0)) { ?>
		<div class="height10"></div>
		<div class="note-box j-success-box padding-innar">
			<?php echo '&nbsp; &nbsp;'.__('Note : Your email/processor id update is pending and not verified yet.'); ?> <?php echo $this->html->link(__("Cancel the update"), array('controller' => 'member', 'action' => 'profile/0/cancelchange'));?>
		</div>
	<?php } ?>
	<?php // Email change note ends here ?>
	
	<div class="comisson-bg">
        <div class="text-ads-title-text"><?php echo __("Profile");?></div>
        <div class="clear-both"></div>
	</div>
	<div class="main-box-eran">
		<?php echo $this->Form->create('Member',array('id' => 'MemberProfilePageForm', 'type' => 'post', 'type' => 'file', 'autocomplete'=>'off', 'onsubmit' => 'return true;','url'=>array('controller'=>'member','action'=>'profile')));?>
		
		<?php // Account Details start here ?>
		
		<div class="tabal-title-heading"><span><?php echo __('Account Details'); ?></span>
		<?php echo $this->html->link(__("Delete Your Account"), array('controller' => 'member', 'action' => 'delete', 'plugin' => false), array('class'=>'button'));?>
		</div>
		
		<div class="profilebox">
			<div class="profilepicbox vat">
				<?php
					if($memberdata['Member']['member_photo']==NULL || $memberdata['Member']['member_photo']=='')
					{
						echo $this->html->image("member/dummy.jpg", array('class'=>'profilepicture','alt'=>'','id'=>'memberimg','width'=>'69','height'=>'69'));
					}
					else
					{
						echo $this->html->image("member/".$memberdata['Member']['member_photo'].'?tk='.date('sHi'), array('class'=>'profilepicture','alt'=>'','id'=>'memberimg','width'=>'69','height'=>'69'));
					}
				?>
				<?php echo __('Profile Picture'); ?>
				<div class="browse_wrapper" style="margin: 0px; width: 62px;display: inline-block;"><?php echo $this->Form->input('member_photo' ,array('type'=>'file', "class"=>"", 'label'=>false));?></div>
			</div>
			<div class="form-box ml10 vat">
				<div class="form-row">
					<div class="form-text profileusername"><?php echo __("Username");?> : </div>
					<div class="form-text profileusername ml10"><?php echo $memberdata['Member']['user_name'];?></div>
				</div>
				<div class="form-row">
					<div class="form-text profileusername"><?php echo __("Sponsor");?> : </div>
					<div class="form-text profileusername ml10"><?php echo $memberdata['Member']['referrer'];?></div>
				</div>
				<div class="form-row">
					<div class="profileusername profileemail"><?php echo __("Email");?> : <span class="required">*</span></div>
					<?php echo $this->Form->input('email' ,array('type'=>'text', "class"=>"formtextbox", 'style'=>'width:50%',  'title'=>__('Email'),'div'=>false, 'label'=>false, 'value'=>$memberdata['Member']['email']));?>
				</div>
			</div>
		</div>
		<?php // Account Details end here ?>
		
		<?php $i=0; $titleset=0; $processordisplayed=0;
			foreach($customfields as $fieldarray){
			
			// Payment Processor Details start here
			if($fieldarray["display"]=='Payment Processor')
			{
				$processordisplayed=1;
				if($i>0){ $titleset=0;echo '</div>'; }
				?>
				<div class="height10"></div>
				<div class="tabal-title-heading protitlebox"><span><?php echo __('Payment Processor'); ?></span><?php if($fieldarray["required"]){echo '<span class="required">*</span>';}?></div>
				
				<div class="form-row-main">
					<?php
					if($SITECONFIG['balance_type']==1)
					{
						?>
						<div class="form-row">
							<div class="form-col-1">
								<div class="select-dropdown fullwidth">
									<label>
										<?php
										echo $this->Form->input('payment_processor', array(
											'type' => 'select',
											'options' => $paymentprocessors,
											'selected' => $memberdata['Member']['payment_processor'],
											'class'=>'',
											'label' => false,
											'div' => false,
										));?>
									</label>
								</div>
							</div>
						<?php echo $this->Form->input('pro_acc_id' ,array('type'=>'text', 'label'=>false,'div'=>false, 'title'=>__('Payment Processor Id'), 'value'=>$memberdata['Member']['pro_acc_id'], 'class'=>'formtextbox'));?>
					</div>
					<?php	
					}
					elseif($SITECONFIG['balance_type']==2)
					{
						?><div class="form-row differprofile">
							<div class="form-col-1 form-text"><b><?php echo __('Name'); ?></b></div>
							<div class="proprocesser"><b><?php echo __('Id'); ?></b></div>
							<div class="proprocesser" style="width: 16%;"><b><?php echo __('Preferred'); ?></b></div>
						</div>
						<?php
						$checked=TRUE; $n=1;
						foreach($paymentprocessors as $proid=>$proname)
						{ ?>
							<div class="form-row differprofile">
								<div class="form-col-1"><?php echo $proname;?></div>
								<div class="proprocesser"><input name="<?php echo "processor[".$proid."]"; ?>" title="<?php echo $proname;?>" class="formtextbox" type="text" style="max-width:200px; width: 100%;" value="<?php echo $memberdata['Member']['processor_'.$proid];?>" /></div>
								<div class="proprocesser" style="width: 16%;">
								<?php 
								echo $this->Form->radio('priority_processor', array($proid=>''), array('legend' => false, 'value' => false, 'label' => false, 'checked'=>$checked));
								if($memberdata['Member']['priority_processor']==$proid)
									$checked=FALSE;
								?>&nbsp;
								</div>
							</div>
						<?php $n++; }
					}
					?>
				</div><?php
			}
			// Payment Processor Details end here
			
			// Personal Details start here
			else{
				if($titleset==0)
				{
					$titleset=1;
					?>
					<div class="height10"></div>
					<div class="tabal-title-heading"><span><?php echo __('Personal Details'); ?></span></div>
					<div class="formgrid">
					<?php
				}
				?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __($fieldarray["display"]);?> : <?php if($fieldarray["required"]){echo '<span class="required">*</span>';}?></div>
					<?php if($fieldarray["required"]==1){$customrequired='customrequired';}else{$customrequired='';}?>
					<?php if($fieldarray["display"]=='First Name'){?>
						<?php echo $this->Form->input('f_name' ,array('type'=>'text', 'label'=>false, 'div'=>false, 'title'=>__('First Name'), 'value'=>$memberdata['Member']['f_name'], 'class'=>'formtextbox '.$customrequired));?>
					<?php }elseif($fieldarray["display"]=='Last Name'){?>
						<?php echo $this->Form->input('l_name' ,array('type'=>'text', 'label'=>false, 'div'=>false, 'title'=>__('Last Name'), 'value'=>$memberdata['Member']['l_name'], 'class'=>'formtextbox '.$customrequired));?>
					<?php }elseif($fieldarray["display"]=='Address'){?>
						<?php echo $this->Form->input('address', array('type'=>'textarea', 'label'=>false, 'div'=>false, 'title'=>__('Address'), 'class'=>'formtextarea '.$customrequired, 'value'=>stripslashes($memberdata['Member']['address'])));?>
					<?php }elseif($fieldarray["display"]=='Contact No'){?>
						<?php echo $this->Form->input('contact_no' ,array('type'=>'text',  'label'=>false, 'div'=>false, 'title'=>__('Contact No'), "class"=>"formtextbox ".$customrequired, 'value'=>$memberdata['Member']['contact_no']));?>
					<?php }elseif($fieldarray["display"]=='Country'){?>
					
					<div class="select-dropdown">
						<label>
							<?php
								$countries[0]="Select country";
								ksort($countries);
								echo $this->Form->input('country', array(
								  'type' => 'select',
								  'options' => $countries,
								  'selected' => $memberdata['Member']['country'],
								  'class'=>''.$customrequired,
								  'label' => false,
								  'div'=>false,
								  'title'=>__('Country'),
							));?>
						</label>
					</div>
						
						
					<?php }else{
						?>
						<?php if($fieldarray["type"]=='textarea'){ ?>
							<textarea name="<?php echo "custom[".$fieldarray["field_id"]."]"; ?>" title="<?php echo __($fieldarray["display"]);?>"  class="formtextarea <?php echo $customrequired;?>"><?php echo stripslashes(@$member_custom_values[$fieldarray["field_id"]]);?></textarea>
						<?php }elseif($fieldarray["type"]=='text'){?>
							<input name="<?php echo 'custom['.$fieldarray['field_id'].']'; ?>" title="<?php echo __($fieldarray['display']);?>" class="formtextbox  <?php echo $customrequired;?>" type="<?php echo $fieldarray['type']; ?>" value="<?php echo stripslashes(@$member_custom_values[$fieldarray["field_id"]]);?>" >
						<?php }?>
					<?php }?>
				
			</div>
			<?php }$i++;
				if($i==count($customfields))
				{
					echo '</div>';
				}
			}
			// Personal Details end here ?>
			
		<?php // Captcha Code starts here ?>
		<?php if($MemberProfileCaptcha){ ?>	
			<div class="captchrow form-row">
				<div class="form-col-1"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
					<?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formcapthcatextbox", 'div'=>false, 'label'=>false));?>
					<span><?php echo __('Code')?> :</span>
					<?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'MemberProfileCaptcha','vspace'=>2, "style"=>"vertical-align: middle", 'width'=>'118', 'height'=>'44')); ?> 
						<a href="javascript:void(0);" onclick="javascript:document.images.MemberProfileCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "style"=>"vertical-align: middle"));?></a>
			</div>
		<?php }?>
		<?php // Captcha Code ends here ?>
		
		<?php // Personalized Details start here ?>
		<div class="height10"></div>
		<div class="tabal-title-heading"><span><?php echo __('Personalized Details'); ?></span></div>
		<div class="form-row">
			<div class="form-text">
				<?php 
					if($memberdata['Member']["unsubscribeemail"]){$checked="checked";}else{$checked="";}
					echo $this->Form->input('unsubscribeemail', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked,'onchange'=>'if(this.checked){$(".solo_email").attr("checked","checked");}'));
				?>
				<?php echo __('Unsubscribe from E-mails'); ?>
				<span class="helptooltip vtip" title="<?php echo __('Mails are sent to you upon each and every major activity related to your account. You can opt out if you do not wish to receive them and resume anytime.') ?>"></span>
				
			</div>
		</div>
		<?php if($SITECONFIG["enable_soloads"]==1){ ?>
		<div class="form-row">
			<div class="form-text">
				<?php 
					if($memberdata['Member']["solo_email"]){$checked="checked";}else{$checked="";}
					echo $this->Form->input('solo_email', array('type' => 'checkbox','class'=>'solo_email', 'div'=>false, 'label' =>'', 'checked'=>$checked));
				?>
				<?php echo __('Unsubscribe from Solo E-mails'); ?>
				<span class="helptooltip vtip" title="<?php echo __('Solo ad mails are sent to all the members. You can opt out if you wish. You can join back anytime.') ?>"></span>
			</div>
		</div>
		<?php } ?>
		<?php if($SITECONFIG['allowtheme']==1){?>
		<div class="form-row">
			<div class="form-col-1"><?php echo __('Theme')?> : <span class="required">*</span></div>
			
			<div class="select-dropdown">
				<label>
					<?php
					$themeslist[0]=__("Select Theme");
					ksort($themeslist);
					echo $this->Form->input('mytheme', array(
						'type' => 'select',
						'options' => $themeslist,
						'selected' => $memberdata['Member']["mytheme"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
					?>
				</label>
			</div>
			
		</div>
		<?php }?>
		<div class="formbutton">
			<?php echo $this->Form->submit(__('Submit'), array(
			  'class'=>'button',
			  'div'=>false,
			  'onclick'=>'return ValidateRegisterForm();'
			));?>
			<?php echo $this->Form->input('ErrorMessage', array('type'=>'hidden', 'id'=>'ErrorMessage', 'label' => false));?>
			<?php echo $this->Form->end();?>
		</div>
		<?php // Personalized Details end here ?>
		
	<div class="clear-both"></div>
	</div>
	
	<?php // Coupon Code starts here ?>
	<div class="comisson-bg">
        <div class="text-ads-title-text"><?php echo __("Coupon");?></div>
        <div class="clear-both"></div>
	</div>
	<div class="main-box-eran">
		<?php echo $this->Form->create('Member',array('id' => 'MemberProfileCouponPageForm', 'type' => 'post', 'autocomplete'=>'off', 'type' => 'file', 'onsubmit' => 'return true;','url'=>array('controller'=>'member','action'=>'profilecoupon'))); ?>
		<div class="form-row-main">
			<div class="form-row couponbox">
				<div class="form-col-1 form-text" style="min-width: 170px;">
					<?php echo __("Enter Coupon Code");?> :
				</div>
				<?php
				echo $this->Form->input('code', array('type'=>'text', 'div'=>false, 'label' => false, 'class'=>'coupontextbox profilecoupon'));
				
				?>
				<span class="smallmargin helptooltip vtip" title="<?php echo __('Enter coupon code to get cash balance in your account now.') ?>"></span>
					<?php
					echo $this->Js->submit(__('Apply Coupon'), array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#UpdateMessage',
						'class'=>'button floatright',
						'div'=>false,
						'controller'=>'member',
						'action'=>'profilecoupon',
						'style'=>'',
						'url'   => array('controller' => 'member', 'action' => 'profilecoupon')
					));
					?>
				<div class="clear-both"></div>
			</div>
		</div>
		<?php echo $this->Form->end();?>
	</div>
	<?php // Coupon Code ends here ?>
	
<?php } else { echo __('This page is disabled by administrator'); } } else { 
if($themesubmenuaccesssecurity){
if($memberdata['Member']['member_id']!=1) { ?>	
	
	<?php // Change Password code starts here ?>
	<div id="UpdateMessage2"></div>
	<div class="comisson-bg">
        <div class="text-ads-title-text"><?php echo __("Change")." ".__("Password");?></div>
        <div class="clear-both"></div>
	</div>
	<div class="main-box-eran">
		<?php echo $this->Form->create('Member',array('id' => 'MemberChangePasswordPageForm', 'type' => 'post', 'autocomplete'=>'off', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'changepassword')));?>
			<div class="form-box">
				<div class="form-row">
					<div class="form-col-1 form-text"><?php echo __("Username");?> :</div>
					<?php echo $memberdata['Member']['user_name'];?>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Current")." ".__("Password");?> : <span class="required">*</span></div>
					<?php echo $this->Form->input('current_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput", 'div'=>false, 'label'=>false));?>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("New Password");?> : <span class="required">*</span></div>
					<?php echo $this->Form->input('new_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput", 'div'=>false, 'label'=>false));?>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Confirm")." ".__("New Password");?> : <span class="required">*</span></div>
					<?php echo $this->Form->input('confirm_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput", 'div'=>false, 'label'=>false));?>
				</div>				
				<div class="formbutton">
					<?php echo $this->Js->submit(__('Update'), array(
						  'before'=>$this->Js->get('#pleasewait, #UpdateMessage2')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage2',
						  'div'=>false,
						  'class'=>'button',
						  'controller'=>'member',
						  'action'=>'changepassword',
						  'url'   => array('controller' => 'member', 'action' => 'changepassword')
					));?>
				</div>
			</div>	
		<?php echo $this->Form->end();?>
	<div class="clear-both"></div>
	</div>
	<?php // Change Password code ends here ?>
	
	<?php } ?>
	
	<?php // Change Secondary Password code starts here ?>
	<?php if($SITECONFIG["allowgoogleauthi"]==1 || $SITECONFIG["allowsecondary"]==1){?>
	<div id="UpdateMessage5"></div>
	<div class="comisson-bg">
        <div class="text-ads-title-text"><?php echo __("Two Step Verification");?></div>
        <div class="clear-both"></div>
	</div>
	<div class="main-box-eran">
		<?php echo $this->Form->create('Member',array('id' => 'MemberSecondaryPageForm', 'type' => 'post', 'autocomplete'=>'off', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'changesecondary')));?>
		<div class="form-box">
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Two Step Verification");?> : <span class="required">*</span></div>
				<div class="select-dropdown">
					<label>
						<?php
						echo $this->Form->input('enabeltwostap', array(
							'type' => 'select',
							'options' => $TwoStapOption,
							'selected' => $EnableTwoStap,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => '',
							'onchange'=> 'if($(this).val()==1){$(".secondary").show();$(".googleAuthi,.disableTwoStap").hide();}else if($(this).val()==2){$(".googleAuthi").show();$(".disableTwoStap, .secondary").hide();}else{$(".disableTwoStap").show();$(".googleAuthi, .secondary").hide();}'
						));
						?>
					</label>
				</div>
			</div>
			<div class="form-row disableTwoStap" style="display:<?php if($EnableTwoStap!=0){ echo 'none'; } ?>">
				<div class="form-col-1"><?php echo __("Current")." ".__("Password");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('current_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput", 'div'=>false, 'label'=>false));?>
			</div>
			<div class="formbutton disableTwoStap" style="display:<?php if($EnableTwoStap!=0){ echo 'none'; } ?>"> 
				<?php echo $this->Js->submit(__('Update'), array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage3')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage5',
					  'div'=>false,
					  'class'=>'button',
					  'controller'=>'member',
					  'action'=>'changesecondary',
					  'url'   => array('controller' => 'member', 'action' => 'changesecondary')
				));?>
			</div>			
		</div>
		<?php echo $this->Form->end();?>
		<div class="clear-both"></div>
	</div>
	<?php } ?>
	
	<?php if($SITECONFIG["allowsecondary"]==1){?>
	<div class="main-box-eran secondary" style="display:<?php if($EnableTwoStap!=1){ echo 'none'; } ?>">
		<?php echo $this->Form->create('Member',array('id' => 'MemberSecondaryPageForm', 'type' => 'post', 'autocomplete'=>'off', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'changesecondary')));?>
		
		<?php echo $this->Form->input('enabeltwostap', array('type'=>'hidden','value'=>'1', 'label' => false));?>
		<div class="form-box">
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Current")." ".__("Password");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('current_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput", 'div'=>false, 'label'=>false));?>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("New Secondary Password");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('new_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput", 'label'=>false,'div'=>false));?>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Confirm")." ".__("New Secondary Password");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('confirm_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput",'div'=>false, 'label'=>false));?>
			</div>
			<div class="formbutton"> 
				<?php echo $this->Js->submit(__('Update'), array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage3')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage5',
					  'div'=>false,
					  'class'=>'button',
					  'controller'=>'member',
					  'action'=>'changesecondary',
					  'url'   => array('controller' => 'member', 'action' => 'changesecondary')
				));?>
			</div>			
		</div>
		<?php echo $this->Form->end();?>
		<div class="clear-both"></div>
	</div>
	<?php }?>
	
	<?php if($SITECONFIG["allowgoogleauthi"]==1){?>
	<div class="main-box-eran googleAuthi" style="display:<?php if($EnableTwoStap!=2){ echo 'none'; } ?>">
		<?php echo $this->Form->create('Member',array('id' => 'MemberSecondaryPageForm', 'type' => 'post', 'autocomplete'=>'off', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'googleauthi')));?>
		<?php echo $this->Form->input('enabeltwostap', array('type'=>'hidden','value'=>'2'));?>
		<div class="form-box" id="divalldetail">
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Google Authentication Using");?> : <span class="required">*</span></div>
				<div class="select-dropdown">
					<label>
						<?php
						echo $this->Form->input('authentication', array(
							'type' => 'select',
							'options' => array(0 =>__("QR Code"),1=>__("Secret Key")),
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));
						?>
					</label>
				</div>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Current")." ".__("Password");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('current_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput", 'div'=>false, 'label'=>false));?>
			</div>
			<div class="formbutton"> 
				<?php echo $this->Js->submit(__('Update'), array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage3')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage5',
					  'div'=>false,
					  'class'=>'button',
					  'controller'=>'member',
					  'action'=>'googleauthi',
					  'url'   => array('controller' => 'member', 'action' => 'googleauthi')
				));?>
			</div>			
		</div>
		<?php echo $this->Form->end();?>
		
		<?php echo $this->Form->create('Member',array('id' => 'MemberSecondaryPageForm', 'type' => 'post', 'autocomplete'=>'off', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'googleauthicodecheck')));?>
		<?php echo $this->Form->input('enabeltwostap', array('type'=>'hidden','value'=>'2'));?>
		<div class="form-box" id="divshowcode" style="display:none">
			<div class="form-row">
				<div class="form-col-1 form-text"></div>
				<span id="Googleauthidata"></span>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Enter ")." ".__("Code");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('code' ,array('type'=>'text', "class"=>"login-from-box-1", 'div'=>false, 'label'=>false));?>
			</div>
			<div class="formbutton"> 
				<?php echo $this->Js->submit(__('Update'), array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage3')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage5',
					  'div'=>false,
					  'class'=>'button',
					  'controller'=>'member',
					  'action'=>'googleauthicodecheck',
					  'url'   => array('controller' => 'member', 'action' => 'googleauthicodecheck')
				));?>
			</div>			
		</div>
		<?php echo $this->Form->end();?>
		<div class="clear-both"></div>
	</div>
	<?php } ?>
	<?php // Change Secondary Password code ends here ?>
	
	<?php // Change Security Details code starts here ?>
	<div id="UpdateMessage3"></div>
	<div class="comisson-bg">
        <div class="text-ads-title-text"><?php echo __("Change")." ".__("Security Details");?></div>
        <div class="clear-both"></div>
	</div>
	<div class="main-box-eran">
		<?php echo $this->Form->create('Member',array('id' => 'MemberAnswerPageForm', 'type' => 'post', 'autocomplete'=>'off', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'changeanswer')));?>
		<div class="form-box">
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Security Question");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('security_question' ,array('type'=>'text', "class"=>"login-from-box-1", 'label'=>false, 'value'=>$memberdata['Member']['security_question'],'div'=>false));?>
				
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)') ?>"></span>
				
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Current Answer");?> :</div>
					<label class="login-from-box-1 security_answer" style="max-width:314px;display: inline-block;line-height: 37px;">********</label>
					<span class="inlineblock" onclick="if($('.security_answer').text()=='********'){$('.security_answer').text('<?php echo $memberdata['Member']['security_answer'];?>');$(this).text('<?php echo __("[Hide Answer]");?>');}else{$('.security_answer').text('********');$(this).text('<?php echo __("[Show Answer]");?>');}"><?php echo __("[Show Answer]");?></span>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("New Answer");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('new_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput", 'label'=>false,'div'=>false, 'style'=>'max-width:336px;'));?>
				
				<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)') ?>"></span>
				
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Confirm")." ".__("New Answer");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('confirm_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput",'div'=>false, 'label'=>false));?>
			</div>
			<div class="formbutton"> 
				<?php echo $this->Js->submit(__('Update'), array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage3')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage3',
					  'div'=>false,
					  'class'=>'button',
					  'controller'=>'member',
					  'action'=>'changeanswer',
					  'url'   => array('controller' => 'member', 'action' => 'changeanswer')
				));?>
			</div>			
		</div>
		<?php echo $this->Form->end();?>
		<div class="clear-both"></div>
	</div>
	<?php // Change Security Details code ends here ?>
	
	<?php // Use Static IP code starts here ?>
	<div id="UpdateMessage4"></div>
	<div class="comisson-bg">
        <div class="text-ads-title-text"><?php echo __("Personalized Details");?></div>
        <div class="clear-both"></div>
	</div>
	<div class="main-box-eran">
		<?php echo $this->Form->create('Member',array('id' => 'MemberIpPageForm', 'type' => 'post', 'autocomplete'=>'off', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'changestaticip')));?>
		<div class="form-box">
			<div class="form-row">
				<div class="form-text">
					<div class="form-col-1"><?php echo __("Enable Static IP Address");?> : </div>
					<?php 
						if($memberdata['Member']["usestaticip"]){$checked="checked";}else{$checked="";}
						echo $this->Form->input('usestaticip', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>$checked));
					?>
					
					<span class="helptooltip vtip" title="<?php echo __('Static IP protects logins from other IP Addresses. When enabled, you have to verify each and every new IP Address you try to log in with to your account. So, you will receive a verification mail every time there is a login attempt from an unverified IP Address.') ?>"></span>
					
				&nbsp;&nbsp;&nbsp;( <?php echo $this->html->link(__("View")." ".__("Used IP Addresses"), array('controller' => 'member', 'action' => 'usedipaddress', 'plugin' => false), array());?> )
				</div>
			</div>
			<div class="formbutton"> 
				<?php echo $this->Js->submit(__('Update'), array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage3')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage4',
					  'div'=>false,
					  'class'=>'button',
					  'controller'=>'member',
					  'action'=>'changestaticip',
					  'url'   => array('controller' => 'member', 'action' => 'changestaticip')
				));?>
			</div>			
		</div>
		<?php echo $this->Form->end();?>
		<div class="clear-both"></div>
	</div>
	<?php // Use Static IP code ends here ?>
<?php } else { echo __('This page is disabled by administrator'); } } ?>	
</div>
 <div class="clear-both"></div>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>