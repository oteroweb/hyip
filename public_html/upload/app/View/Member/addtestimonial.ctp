<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 25-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>
<script type="text/javascript"> 
(function() {
$('#Testimonial').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
		$("html, body").animate({ scrollTop: 80 }, 'slow');
	}
});
})();
</script>
<div class="comisson-bg">
        <div class="text-ads-title-text"><?php echo __("Add Testimonial");?></div>
        <div class="clear-both"></div>
	</div>
	<div class="main-box-eran" style="text-align: center;">
		
		<?php // Rate us form starts here ?>
		<?php echo $this->Form->create('Testimonial',array('id' => 'Testimonial', 'type' => 'post', 'type' => 'file', 'onsubmit' => 'return true;', 'autocomplete'=>'off','url'=>array('controller'=>'member','action'=>'addtestimonial')));?>
		<?php echo $this->Form->input('test_id', array('type'=>'hidden', 'value'=>$testimonialdata['Testimonial']["test_id"], 'label' => false));?>
		<?php echo $this->Form->input('content' ,array('type'=>'textarea', "class"=>"formtextarea", 'div'=>false, 'label'=>false, 'value'=>stripslashes($testimonialdata['Testimonial']['content']), 'style'=>'max-width: 320px; height: 196px;'));?>
		<div class="fixcontentbox" style="vertical-align: top;margin-top: 6px;">
		    <div class="rateusrightbox">
			<div>
			    <div>
				<?php echo __('Image')?> : 
				<?php
					if($testimonialdata['Testimonial']['photo']==NULL || $testimonialdata['Testimonial']['photo']=='')
					{
						echo $this->html->image("testimonials/dummy.jpg", array('alt'=>__('Testimonials
'),'id'=>'testmonialimg','class'=>'profilepicture','alt'=>'','width'=>'69','height'=>'69','style'=>'vertical-align: middle;'));
					}
					else
					{
						echo $this->html->image("testimonials/".$testimonialdata['Testimonial']['photo'], array('alt'=>__('Testimonials
'),'id'=>'testmonialimg','class'=>'profilepicture','alt'=>'','width'=>'69','height'=>'69','style'=>'vertical-align: middle;'));
					}
				?>
			    </div>
			    <div>
				<div style="margin-top: 10px;display: inline-block;vertical-align: top;"><?php echo __('Upload Photo')?> :</div>
				<div class="browse_wrapper" style="margin: 0px; width: 62px;display: inline-block;"><?php echo $this->Form->input('photo' ,array('type'=>'file', "class"=>"", 'div'=>false, 'label'=>false));?></div>
			    </div>
			</div>
			<?php if($MemberAddtestimonialCaptcha){ ?>
				<div>
				    <div>
					<?php echo __('Code')?> : 
				    </div>
				    <div>
					<?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'MemberAddtestimonialCaptcha','vspace'=>2, "style"=>"vertical-align: middle", 'width'=>'118', 'height'=>'44')); ?>
					<a href="javascript:void(0);" onclick="javascript:document.images.MemberAddtestimonialCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "style"=>"vertical-align: middle"));?></a>
				    </div>
				</div>
				<div>
				    <div>
					<?php echo __('Enter Captcha')?> : <span class="required">*</span>&nbsp;
				    </div>
				    <div>
					<?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formtextbox", 'label'=>false));?>
				    </div>
				</div>
			<?php } ?>
		    </div>
		</div>
		<div class="clear-both"></div>
		<div class="textcenter"> 
			<div class="">
				<?php echo $this->Form->submit(__('Submit'), array(
				  'class'=>'add-new',
				  'div'=>false
				));?>
			</div>
		</div>
		<div class="clear-both"></div>
		<?php echo $this->Form->end();?>
		<?php // Rate us form ends here ?>
		
	<div class="clear-both"></div>
</div>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>