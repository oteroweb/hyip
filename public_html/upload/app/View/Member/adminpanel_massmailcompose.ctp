<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 17-11-2014
  *********************************************************************/
?>
<?php 
if($textarea=='advanced')
{
	echo $this->Javascript->link('tiny_mce/tiny_mce.js');
	echo $this->Javascript->link('tinymce');
	$textareaclass='advancededitor';
	$submitbuttonclass='tinyMCEtriggerSavetk';
}
elseif($textarea=='simple')
{
	$textareaclass='';
	$submitbuttonclass='';
}
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Member / Mass Mailing</div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Step-1", array('controller'=>'member', "action"=>"massmail"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<?php if($IsAdminAccess){?>
				<li>
					<?php echo $this->Js->link("Step-2", array('controller'=>'member', "action"=>"massmaillist"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li class="active">
					<?php echo $this->Js->link("Step-3", array('controller'=>'member', "action"=>"massmailcompose"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<?php }?>
			 </ul>
	</div>
</div>
<div class="tab-content">
<div id="memberpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Mass_Mailing#Step-3" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
	<script type="text/javascript">
	$(document).ready (function () {
		$("#emailsentarea").css({'top':($(window).height()-125)/2, 'left':($(window).width()-600)/2});
	});
	</script>
	<!-- sent email list area starts -->
	<div id="emailsentarea">
		<div class="closebutton"><a href="javascript:void(0)" onclick="closediv()"><?php echo $this->Html->image('close-button.png', array('alt'=>'X'))?></a></div>
		<div style="width: 60%; float: left;">	
			<table><tr>
			<td><div class="text">Total Mails Sent :</div></td><td class="text"><div class="totalcomplete"></div></td>
			</tr></table>
		</div>
		
		<div class="massprogressbar text" style="float: left;background-color: #e2eaf1;border: 1px solid #c3d3df;border-radius: 15px;padding: 5px 20px; height: 20px;"></div>
	</div>
	<div class="clearboth"></div>
	<!--<div id="emailsentarea">
		<div class="closebutton"><a href="javascript:void(0)" onclick="closediv()"><?php //echo $this->Html->image('close-button.png', array('alt'=>'X'))?></a></div>
		<div class="massprogressbar_bg"><div class="massprogressbar"></div></div>
		<table><tr>
		<td><div class="text">Total Mails Sent :</div></td><td class="text"><div class="totalcomplete"></div></td>
		</tr></table>
	</div> -->
	<!-- sent email list area over -->
	
	<script type="text/javascript">
	function closediv()
	{
		document.getElementById('emailsentarea').innerHTML="";
		document.getElementById("emailsentarea").style.display="none";
		
		$.ajax({
			type: "POST",
			url: "<?php echo $ADMINURL;?>/member/massmailcomplete",
			data: { memberid: 0}
			}).done(function( html ) {
				$("#memberpage").html(html);
		});
	}
	function sendmassmail()
	{
		<?php $memberids=$this->Session->read('MemberIds');?>
		var error=0;
		var msgerror='';
		if($('#subjecthtml').val()=='')
		{
			error=1;
			msgerror="<li>Please Enter Subject</li>";
			$("input[name*=\'data[Member][subject]\']").parent("div").addClass("validationerror").next("span").addClass("validationerror");
		}
		if($('#messagehtml').val()=='')
		{
			error=1;
			msgerror+="<li>Please Enter Message</li>";
			$("input[name*=\'data[Member][message]\']").parent("div").addClass("validationerror").next("span").addClass("validationerror");
		}
		if(<?php echo count($memberids);?>==0)
		{
			error=1;
			msgerror+="<li>Please Select Atleast One Member to Send Mail</li>";
			
		}
		if (error==1) 
		{
			$("#UpdateMessage").removeClass('formsuccess').addClass('formerror').html("Please Enter Valid Data in Following Fields<ul>"+msgerror+"</ul>").show();
			$('#subjecthtml').focus();
			return false;
		}
		else
		{
			$('#pleasewait').fadeIn();
			<?php
			$totalmails=count($memberids);
			$percentage=100/$totalmails;
			for($i=0;$i<$totalmails;$i++)
			{
				?>
				var splitted=null;
				$.ajax({
					type: "POST",
					url: "<?php echo $ADMINURL;?>/member/massmailsend",
					data: { memberid: <?php echo $memberids[$i];?>, subject: $('#subjecthtml').val(), message: $('#messagehtml').val()}
					}).done(function( html ) {
						//$("#emailsentarea").append(<?php echo ($i+1);?>+". "+html);$("#emailsentarea").show();
						
						$("#emailsentarea").show();
						splitted=html.split("|");
						
						$("#emailsentarea .totalcomplete").html(splitted[0]+"/"+<?php echo count($memberids); ?>);
						//$("#emailsentarea .massprogressbar").css('width', Math.round(splitted[1])+'%');
						$("#emailsentarea .massprogressbar").html(Math.round(splitted[1])+'%');
						
						<?php if((count($memberids)-1)==$i){?>$("#pleasewait").fadeOut();$(".closebutton").css('visibility','visible');<?php }?>
				});
				<?php
			}
			?>
			$.ajax({
				type: "POST",
				url: "<?php echo $ADMINURL;?>/member/massmailhistoryadd",
				data: { memberids: <?php echo count($memberids);?>, subject: $('#subjecthtml').val(), message: $('#messagehtml').val()}
				}).done(function( html ) {
				
			});
		}
	}
	</script>
		<?php //echo $this->element('massmail/progress', array('percentagecomplete'=>$percentagecomplete)); ?>
		
		<?php echo $this->Form->create('Member',array('type' => 'post', 'id'=>'SiteSettingForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'massmailsend')));?>
			<div class="frommain">
				<div class="fromnewtext">Total Members :</div>
				<div class="fromborderdropedown4"><?php echo count($this->Session->read('MemberIds'));?></div>
				
				<div class="fromnewtext">Select Mass Mail Template  :</div>
				<div class="fromborderdropedown3">
					<div class="select-main">
					<label>
						<?php 
							$admin_template_emaildata[0]="Select Template";
							ksort($admin_template_emaildata);
							echo $this->Form->input('templateid', array(
								'type' => 'select',
								'options' => $admin_template_emaildata,
								'selected' => '',
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => '',
								'onchange'=>'GetAdminTemplateEmailMS(this.value,"#subjecthtml","#messagehtml", "'.$ADMINURL.'", "'.$textarea.'");'
							));
						?>
					</label>
					</div>
				</div>
					
				<div class="fromnewtext">Subject :<span class="red-color">*</span>  </div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('subject', array('type'=>'text', 'id'=>'subjecthtml', 'value'=>'', 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
				</div>
				
				<div class="fromnewtext floatleft">Message :<span class="red-color">*</span>  </div>
				<div class="formbutton">
					<?php 
						if($textarea=='simple'){$showtextarea='advanced';}
						if($textarea=='advanced'){$showtextarea='simple';}
						$link='massmailcompose/'.$showtextarea;
						
						echo $this->Js->link("Switch To ".ucfirst($showtextarea)." Mode", array('controller'=>'member', "action"=>$link), array(
							'update'=>'#memberpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'btngray floatright'
						));
					?>
				</div>
				<div class="clearboth"></div>
				<div class="height10"></div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('message', array('type'=>'textarea', 'id'=>'messagehtml', 'value'=>'','label' => false, 'div'=>false, 'class'=>'from-textarea '.$textareaclass));?>
				</div>
				
				<div class="fromnewtext">Mail Template Fields :</div>
				<div class="fromborderdropedown3">
					  <?php foreach($tagdata as $tag){?>
						<div class="linktotext"><a onClick="AddTags(document.getElementById('messagehtml'),'[<?php echo $tag['Template_tag']['tag_name'];?>]');" title="click to paste" href="javascript:void(0);"><?php echo '['.$tag['Template_tag']['tag_name'].']';?></a><?php echo ' - '.$tag['Template_tag']['tag_desc'];?></div>
					<?php }?>
				</div>
			
				<div class="formbutton">
					<?php echo $this->Form->submit('Submit', array(
					  'class'=>'btnorange '.$submitbuttonclass,
					  'div'=>false,
					  'onclick' => 'sendmassmail();'
					));?>
					
					<?php echo $this->Js->submit('Cronjob', array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage',
					  'class'=>'btnorange '.$submitbuttonclass,
					  'div'=>false,
					  'controller'=>'member',
					  'action'=>'massmailsendcronjob',
					  'url'   => array('controller' => 'member', 'action' => 'massmailsendcronjob')
					)); ?>
				</div>
			</div>
		<?php echo $this->Form->end();?>
		<div class="height-12"></div>
	<?php if(!$ajax){?>
</div><!--#memberpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>