<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List#Add_Order" target="_blank">Help</a></div>
<?php echo $this->Javascript->link('allpage');?>
<div class="height10"></div>
<div id="UpdateMessage"></div>
		<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'mdmakeorderaction')));?>
		<?php echo $this->Form->input('member_id', array('type'=>'hidden', 'value'=>$member_id, 'label' => false));?>
			<div class="frommain">
				<div class="fromnewtext">Select Product : </div>
				<div class="fromborderdropedown3">
					<div class="select-main">
						<label>
					<?php
					$searchbyoptions=array('0'=>'Select Parameter', 'Ppc1'=>'PPC Ad', 'Ptc1'=>'PTC Ad', 'Loginad1'=>'Login Ad', 'Member1'=>'Membership', 'Textad1'=>'Text Ad','Bannerad1'=>'Banner Ad','Soload1'=>'Solo Ad','Directory1'=>'Biz Directory','Traffic1'=>'Web Credits');
					
					$onchangestr="";
					$modules=@explode(",",trim($SITECONFIG["modules"],","));
					foreach($modules as $module)
					{
						$modulearray=@explode(":", $module);
						if($modulearray[2]==1 && $modulearray[3]!="" && $modulearray[4]!="" && $modulearray[14]==1)
						{
							$subplanarray=explode("-",$modulearray[3]);
							$subpositionarray=explode("-",$modulearray[4]);
							$subplanname=explode("-",$modulearray[13]);
							$subcounter=1;
							foreach($subplanarray as $subplan)
							{
								if(count($subplanarray)==1)
								{
									$searchbyoptions['MODULE'.ucfirst($modulearray[1]).$subcounter]=$modulearray[0];
								}
								else
								{
									$searchbyoptions['MODULE'.ucfirst($modulearray[1]).$subcounter]=$subplanname[$subcounter-1]." ".$modulearray[0];
								}
								$onchangestr.='else if($(this).val()=="MODULE'.ucfirst($modulearray[1]).$subcounter.'"){generatecombo2("data[Member]['.ucfirst($modulearray[1]).$subcounter.']","'.$subplan.'","id","plan_name","'.$searchfor.'",".searchforcombo","","","'.$ADMINURL.'","isactive#1,allow_new_purchase#1");$(".planlist").show(500);generateorderform("'.$modulearray[1].$subcounter.'", "'.$ADMINURL.'");}';
								
								//if($searchby==ucfirst($modulearray[1]).$subcounter)
									//echo '<script type="text/javascript">generatecombo2("data[Member][MODULE'.ucfirst($modulearray[1]).$subcounter.']","'.$subplan.'","id","plan_name","'.$searchfor.'",".searchforcombo","width:562px;","","'.$ADMINURL.'");</script>';
								$subcounter++;
							}
						}
					}
					?>
					<?php 
						echo $this->Form->input('plantype', array(
							'type' => 'select',
							'options' => $searchbyoptions,
							'selected' => '',
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => '',
							'onchange' => 'if($(this).val()=="Ppc1"){generatecombo2("data[Member][Ppc1]","Ppcplan","id","plan_name","'.$searchfor.'",".searchforcombo","","","'.$ADMINURL.'","status#1");$(".planlist").show(500);$("#GenerateForm").html("");}else if($(this).val()=="Ptc1"){generatecombo2("data[Member][Ptc1]","Ptcplan","id","plan_name","'.$searchfor.'",".searchforcombo","","","'.$ADMINURL.'","status#1");$(".planlist").show(500);$("#GenerateForm").html("");}else if($(this).val()=="Loginad1"){generatecombo2("data[Member][Loginad1]","Loginad","id","plan_name","'.$searchfor.'",".searchforcombo","","","'.$ADMINURL.'","active_status#1");$(".planlist").show(500);$("#GenerateForm").html("");}else if($(this).val()=="Member1"){generatecombo2("data[Member][Member1]","Membership","id","membership_name","'.$searchfor.'",".searchforcombo","","","'.$ADMINURL.'","status#1,allow_new_purchase#1");$(".planlist").show(500);generateorderform("member1", "'.$ADMINURL.'");}else if($(this).val()=="Textad1"){generatecombo2("data[Member][Textad1]","Ptextadplan","id","plan_name","'.$searchfor.'",".searchforcombo","","","'.$ADMINURL.'","status#1");$(".planlist").show(500);$("#GenerateForm").html("");}else if($(this).val()=="Bannerad1"){generatecombo2("data[Member][Bannerad1]","Banneradplan","id","plan_name","'.$searchfor.'",".searchforcombo","","","'.$ADMINURL.'","status#1");$(".planlist").show(500);$("#GenerateForm").html("");}else if($(this).val()=="Soload1"){generatecombo2("data[Member][Soload1]","Soloadplan","id","plan_name","'.$searchfor.'",".searchforcombo","","","'.$ADMINURL.'","status#1");}else if($(this).val()=="Directory1"){generatecombo2("data[Member][Directory1]","Directoryplan","id","plan_name","'.$searchfor.'",".searchforcombo","","","'.$ADMINURL.'","status#1");$(".planlist").show(500);$("#GenerateForm").html("");}else if($(this).val()=="Traffic1"){generatecombo2("data[Member][Traffic1]","Webcreditplan","id","plan_name","'.$searchfor.'",".searchforcombo","","","'.$ADMINURL.'","status#1");$(".planlist").show(500);$("#GenerateForm").html("");} '.$onchangestr.' else{$(".planlist").hide(500);$("#GenerateForm").html("");}'
						));
					?>
						</label>
					</div>
				</div>
				
				
				<div class="planlist" style="display: none;">
					<div class="fromnewtext">Select Plan : </div>
						<div class="fromborderdropedown3">
							<div class="select-main">
								<label>
									<span class="searchforcombo"></span>
								</label>
							</div>
						</div>
					
				</div>
				<div id="GenerateForm"></div>
				<div class="formbutton">
					<?php echo $this->Js->submit('Purchase', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#UpdateMessage',
						'class'=>'btnorange',
						'controller'=>'member',
						'div'=>false,
						'action'=>'mdmakeorderaction',
						'url'   => array('controller' => 'member', 'action' => 'mdmakeorderaction')
					));?>
				</div>
			</div>
		<?php echo $this->Form->end();?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>