<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 10-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List#Stats" target="_blank">Help</a></div>
	<div class="statistictablesmain">
	  <div class="statistictables">
	    <div class="financialstatbox mobilewidth100">
	      <div class="planstats">
		<div class="planstathead">Active Membership(s)</div>
		  <div class="divtable">
		    <div class="divtr">
		      <div class="divtd"><b>Membership</b></div>
		      <div class="divtd"><b>Purchase Date</b></div>
		    </div>
		    <?php if(count($membermemberships)>0)
		    { ?>
		      <?php foreach ($membermemberships as $membermembership): ?>
			<div class="divtr">
			  <div class="divtd"><?php echo $membermembership['Membership']['membership_name']; ?> :</div>
			  <div class="divtd"><?php echo $this->Time->format($SITECONFIG["timeformate"], $membermembership['Membermembership']['purchasedate']); ?></div>
			</div>
		      <?php endforeach;
		    }
		    else
		    { ?>
			<div class="divtr">
			  <div class="divtd">No Membership Found.</div>
			</div>
		    <?php
		    } ?>
		    
		  </div>
	      </div>
	    </div>
	    
	    <div class="financialstatbox mobilewidth100">
	      <div class="planstats">
		<div class="planstathead">Referrals</div>
		  <div class="divtable">
		    <div class="divtr">
		      <div class="divtd">Total Referrals :</div>
		      <div class="divtd"><?php echo $MemberData['Member']['total_referrer']; ?></div>
		    </div>
		    <div class="divtr">
		      <div class="divtd">Total Ref. Comm. on Position :</div>
		      <div class="divtd">$<?php echo isset($PositionCommission) ? round($PositionCommission,4) : 0; ?></div>
		    </div>
		    <div class="divtr">
		      <div class="divtd">Total Ref. Comm. on Membership Fees :</div>
		      <div class="divtd">$<?php echo isset($MembershipCommission) ? round($MembershipCommission,4) : 0; ?></div>
		    </div>
		  </div>
	      </div>
	    </div>
	    
	    <div class="financialstatbox mobilewidth100">
	      <div class="planstats">
		<div class="planstathead">Amount</div>
		  <div class="divtable">
		    <div class="divtr">
		      <div class="divtd">Pending Withdrawal Amount :</div>
		      <div class="divtd">$<?php echo isset($Withdrawaldata[0][0]['amount']) ? round($Withdrawaldata[0][0]['amount'],4) :0; ?></div>
		    </div>
		    <div class="divtr">
		      <div class="divtd">Complete Withdrawal Amount :</div>
		      <div class="divtd">$<?php echo isset($Withdrawal[0][0]['amount']) ? round($Withdrawal[0][0]['amount'],4) :0; ?></div>
		    </div>
		    
		  </div>
	      </div>
	    </div>
	    
	    <?php if(count($moduledata)>0)
	    {
		  foreach($moduledata as $module)
		  { ?>
		    <div class="financialstatbox mobilewidth100">
		      <div class="planstats">
			<div class="planstathead"><?php echo $module['planname']; ?> Positions</div>
			  <div class="divtable">
			    <div class="divtr">
			      <div class="divtd">Total Active :</div>
			      <div class="divtd"><?php echo $module['ActivePosition']; ?></div>
			    </div>
			    <div class="divtr">
			      <div class="divtd">Total Inactive :</div>
			      <div class="divtd"><?php echo $module['InActivePosition']; ?></div>
			    </div>
			    <div class="divtr">
			      <div class="divtd">Total Purchased :</div>
			      <div class="divtd"><?php echo $module['TotalPosition']; ?></div>
			    </div>
			    <div class="divtr">
			      <div class="divtd">Total Earning :</div>
			      <div class="divtd">$<?php echo round($module['PositionEarning'],4); ?></div>
			    </div>
			  </div>
		      </div>
		    </div>
		    <?php
		  }
	     } ?>
	    <div class="financialstatbox mobilewidth100">
	      <div class="planstats">
		<div class="planstathead">IP</div>
		  <div class="divtable">
		    <div class="divtr">
		      <div class="divtd">Registration IP :</div>
		      <div class="divtd"><?php echo $MemberData['Member']['register_ip'];?> (<?php echo ($MemberData['Member']['guicountry']!="") ? $MemberData['Member']['guicountry'] : 'N/A' ?>)</div>
		    </div>
		    <div class="divtr">
		      <div class="divtd">Last Login IP :</div>
		      <div class="divtd"><?php echo $MemberData['Member']['lastip'];?> (<?php echo ($MemberData['Member']['lastcountry']!="") ? $MemberData['Member']['lastcountry'] : 'N/A' ?>)</div>
		    </div>
		    <div class="divtr">
		      <div class="divtd">Last Activity Date :</div>
		      <div class="divtd"><?php echo $this->Time->format($SITECONFIG["timeformate"], $LastActivityData['Memberlog']['logdate']); ?></div>
		    </div>
		    <div class="divtr">
		      <div class="divtd">Registration Date :</div>
		      <div class="divtd"><?php echo $this->Time->format($SITECONFIG["timeformate"], $MemberData['Member']['reg_dt']); ?></div>
		    </div>
		  </div>
	      </div>
	    </div>
		<div class="financialstatbox mobilewidth100">
	      <div class="planstats">
		<div class="planstathead">Advertisement</div>
		  <div class="divtable">
			<?php if($TotalBannerAd!='N/A'){?>
		    <div class="divtr">
		      <div class="divtd">Banner Ad :</div>
		      <div class="divtd"><?php echo $TotalBannerAd;?></div>
		    </div>
			<?php }if($TotalTextAd!='N/A'){?>
		    <div class="divtr">
		      <div class="divtd">Text Ad :</div>
		      <div class="divtd"><?php echo $TotalTextAd;?></div>
		    </div>
			<?php }if($TotalSoloAd!='N/A'){?>
		    <div class="divtr">
		      <div class="divtd">Solo Ad :</div>
		      <div class="divtd"><?php echo $TotalSoloAd;?></div>
		    </div>
			<?php }if($TotalPPCAd!='N/A'){?>
		    <div class="divtr">
		      <div class="divtd">PPC :</div>
		      <div class="divtd"><?php echo $TotalPPCAd;?></div>
		    </div>
			<?php }if($TotalPTCAd!='N/A'){?>
			<div class="divtr">
		      <div class="divtd">PTC :</div>
		      <div class="divtd"><?php echo $TotalPTCAd;?></div>
		    </div>
			<?php }if($TotalLoginAd!='N/A'){?>
			<div class="divtr">
		      <div class="divtd">Login Ad :</div>
		      <div class="divtd"><?php echo $TotalLoginAd;?></div>
		    </div>
			<?php }if($TotalDirectoryAd!='N/A'){?>
			<div class="divtr">
		      <div class="divtd">Biz Directory :</div>
		      <div class="divtd"><?php echo $TotalDirectoryAd;?></div>
		    </div>
			<?php }if($TotalSurffreeAd!='N/A'){?>
			<div class="divtr">
		      <div class="divtd">Surf Free :</div>
		      <div class="divtd"><?php echo $TotalSurffreeAd;?></div>
		    </div>
			<?php }?>
		  </div>
	      </div>
	    </div>
	    
	    <div class="financialstatbox mobilewidth100">
	      <div class="planstats">
		<div class="planstathead">Miscellaneous Bonus</div>
		  <div class="divtable">
		    <div class="divtr">
		      <div class="divtd">Bonus :</div>
		      <div class="divtd">$<?php echo round($Bonus,4); ?></div>
		    </div>
		    <div class="divtr">
		      <div class="divtd">Matrix Completion Bonus :</div>
		      <div class="divtd">$<?php echo round($MatrixBonus,4); ?></div>
		    </div>
		  </div>
	      </div>
	    </div>
	    
	    <div class="financialstatbox mobilewidth100">
	      <div class="planstats">
		<div class="planstathead">Geo</div>
		  <div class="divtable">
		    <div class="divtr">
		      <div class="divtd">Country :</div>
		      <div class="divtd"><?php echo $MemberData['Country']['country_name'] ?></div>
		    </div>
		    <div class="divtr">
		      <div class="divtd">IP Country :</div>
		      <div class="divtd"><?php echo ($MemberData['Member']['guicountry']!="") ? $MemberData['Member']['guicountry'] : 'N/A' ?></div>
		    </div>
		  </div>
	      </div>
	    </div>
	    
	  </div>
	</div>
      
      
    
<div class="height-12"></div>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>