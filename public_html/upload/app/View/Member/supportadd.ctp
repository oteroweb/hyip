<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 25-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<script type="text/javascript"> 
(function() {
$('#Member_ticket').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();
</script>
<div class="height5"></div><div id="UpdateMessage"></div>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Member Support Ticket Details');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	
		<?php if($SITECONFIG["mticket_chk"]==1){ ?>
			
			<?php // Support ticket add form starts here ?>
			<?php echo $this->Form->create('Member_ticket',array('id' => 'Member_ticket', 'type' => 'post', 'type' => 'file', 'onsubmit' => 'return true;', 'autocomplete'=>'off', 'url'=>array('controller'=>'member','action'=>'supportadd'))); ?>
				<div class="form-box">
					<div class="form-row">
						<div class="form-col-1"><?php echo __('Category')?> : </div>
						
						<div class="select-dropdown">
							<label>
								<?php echo $this->Form->input('category', array(
									  'type' => 'select',
									  'options' => array('General'=>__('General'), 'Technical'=>__('Technical')),
									  'selected' => '',
									  'class'=>'',
									  'label' => false,
									  'div' => false
								));?>
							</label>
						</div>
						
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __('Subject')?> : <span class="required">*</span></div>
						<?php echo $this->Form->input('subject' ,array('type'=>'text', "class"=>"formtextbox", 'label'=>false, 'div'=>false));?>
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __('Message')?> : <span class="required">*</span></div>
						<?php echo $this->Form->input('message' ,array('type'=>'textarea', "class"=>"formtextarea", 'label'=>false, 'div'=>false));?>
					</div>
					<?php if($MemberSupportCaptcha){ ?>
					  <div class="form-row captchrow">
						<div class="form-col-1"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
							<?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formcapthcatextbox","div"=>false, 'label'=>false));?>
							<span><?php echo __('Code')?> :</span>
							<?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'MemberSupportCaptcha','vspace'=>2,"style"=>"vertical-align: middle", 'width'=>'118', 'height'=>'44')); ?> 
							<a href="javascript:void(0);" onclick="javascript:document.images.MemberSupportCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"","style"=>"vertical-align: middle"));?></a>
					  </div>
					    <?php }?>
					<div class="form-row">
						<div class="height7"></div>
						<div class="inlineblock">
                        	<span><?php echo __("Attachment");?> 1 :</span>
							<div class="browse_wrapper vam inline"><?php echo $this->Form->input('photo1', array('type' => 'file','label' => false)); ?></div>
						</div>
						<div class="inlineblock">
                            <span><?php echo __("Attachment");?> 2 :</span>
                            <div class="browse_wrapper vam inline" ><?php echo $this->Form->input('photo2', array('type' => 'file','label' => false)); ?></div>
						</div>
						<div class="inlineblock">
							<span><?php echo __("Attachment");?> 3 :</span>
                            <div class="browse_wrapper vam inline"><?php echo $this->Form->input('photo3', array('type' => 'file','label' => false)); ?></div>
						</div>
						<div class="inlineblock">
                            <span><?php echo __("Attachment");?> 4 :</span>
                            <div class="browse_wrapper vam inline"><?php echo $this->Form->input('photo4', array('type' => 'file','label' => false)); ?></div>
						</div>
						<div class="height7"></div>
                    </div>
                    <div class="form-row">
						<div class="form-text">
						    <div class="note-box"><b><span class="red-color">*<?php echo __("Note");?> :</span></b> <?php echo __("Attachments : Only .jpg, .jpeg, .gif, .png, .pdf, .doc and .docx files are allowed to upload. Max file size is 5 MB."); ?></div><div class="height10"></div>
						</div>
					</div>
					<div class="formbutton">
						<?php echo $this->html->link(__("Back"), array('controller' => 'member', 'action' => 'support'), array('class'=>'button', 'style'=>'float:left'));?>
						<?php echo $this->Form->submit(__('Submit'), array(
						  'class'=>'button',
						  'div'=>false
						));?>
					</div>
					<div class="clear-both"></div>	
                </div>
			<?php echo $this->Form->end();?>
			<?php // Support ticket add form ends here ?>
			
		<?php }else{?>
			<?php echo __("Admin has stopped new tickets currently. Please try later.");?>
		<?php }?>
</div>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>