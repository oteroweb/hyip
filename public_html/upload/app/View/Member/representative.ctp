<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 17-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php ///tempcode start ?>
<script>
	$(function(){
		$(".recentrightbox").click(function(){
			$(this).next(".recentbluebox").toggle(500);
			$(this).toggleClass("activediv");
		});
	});
</script>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>	
<div class="comisson-bg-new">
	<div class="text-ads-title-text"><?php echo __("REPRESENTATIVES LIST"); ?></div>
	<div class="clear-both"></div>
</div>

	<?php foreach($members as $countryname => $member){  ?>
	<div class="left-part repleft-part mobilewidth100">
			<div class="recentrightbox reprecentright" style="cursor:pointer;">
				<div class="reprecentwhite"><?php echo $this->html->image("countryflag/".strtolower($member[0]['country']['iso']).".png", array('alt'=>'')); ?></div>
				<div class="recentright recrecentright"><?php echo $countryname; ?> (<?php echo count($member); ?>)</div>
				<div class="recentplus"></div>
			</div>
			
			<div class="recentbluebox reprecentbluebox" style="display: none;">
				<?php foreach($member as $memberdata){  ?>
						<div>
						<b>Name:</b> <a href="mailto:<?php echo $memberdata['Member']['email']; ?>"><?php echo $memberdata['Member']['f_name']." ".$memberdata['Member']['l_name']; ?></a><br/>
						<b>Phone:</b> <?php echo $memberdata['Member']['contact_no']; ?><br/>
						<b>Address:</b> <?php echo $memberdata['Member']['address']; ?><br/>
						<?php if(isset($custom_fields) && count($custom_fields)>0)
						{  
							$tmp_arr = @explode("[#]",$memberdata['Member']["custom_value"]);
							$i=0;
							foreach ($custom_fields as $custom_field):
								  $custom_value=@explode("[@]",$tmp_arr[$i]);
								    echo '<b>'.$custom_field["Regform_manage"]["field_title"].'</b> :'; 
								    echo stripslashes(@$custom_value[1])."<br/>"; 
								    $i++;
							endforeach;
						}?>
						<b>Register under :</b> <a href="<?php echo $SITEURL."ref/".$memberdata['Member']['user_name']; ?>" ><?php echo $memberdata['Member']['user_name']; ?></a></div>
						
				<?php } ?>
			</div>
		
	</div>	
	<?php } ?>
<div class="clearboth"></div>