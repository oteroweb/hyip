<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List#Login_IP_Logs" target="_blank">Help</a></div>
<div class="tab-innar nomargin">
		<ul>
			<li>
				<?php echo $this->Js->link("Activity Log", array('controller'=>'member', "action"=>"memberactivity/".$member_id), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>''
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Login IP Logs", array('controller'=>'member', "action"=>"iplog/".$member_id), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'active'
				));?>
			</li>
		</ul>
	</div>
<div class="clear-both"></div>
<div class="height21"></div>
<div id="UpdateMessage"></div>

<div class="serchmainbox">
	<div class="serchgreybox"><?php echo "Search Option";?></div>
	
	  <?php echo $this->Form->create('Iplog',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'iplog')));?>
	  
	  <div class="from-box">
		<div class="fromboxmain">
			<span><?php echo "Search By";?> :</span>
			<span>                     
			    <div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
						echo $this->Form->input('searchby', array(
						      'type' => 'select',
						      'options' => array('all'=>'Select Parameter', 'ip'=>'IP Address'),
						      'selected' => $searchby,
						      'class'=>'',
						      'label' => false,
						      'style' => ''
						));
						?>
					</label>
				</div>
			    </div>
			 </span>
		</div>
		<div class="fromboxmain">
		    <span>Search For :</span>
		    <span><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?></span>
		</div>
	</div>
	<div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
		</div>
		<div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#memberdetailpage',
						'class'=>'searchbtn',
						'controller'=>'member',
						'action'=>'memberactivity',
						'url'=> array('controller' => 'member', 'action' => 'iplog/'.$member_id)
				));?>
			</span>
		</div>
	</div>
	  
	  <?php echo $this->Form->end();?>
</div>



<div id="gride-bg">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberdetailpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'iplog/'.$member_id)
	));
	$currentpagenumber=$this->params['paging']['User_ip_log']['page'];
	?>
	<div class="padding-left-serchtabal">
	</div>
		<div class="height10"></div>
		 <div class="margin10">
		 <div class="display-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
		<div class="clear-both"></div></div>
		 
		<div class="tablegrid">
				<div class="tablegridheader">
					<div>Login Date</div>
					<div>IP Address</div>
					<div>Country</div>
					<div>Action</div>
				</div>
				<?php
				$i = 0;
				foreach ($user_ip_logs as $user_ip_log):
					$class = 'class="blue-color"';if ($i++ % 2 == 0){$class = 'class="white-color"';}
					?>
					
					<div class="tablegridrow">
						<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $user_ip_log['User_ip_log']['login_date']);?></div>
						<div><?php echo $user_ip_log['User_ip_log']['ip']; ?></div>
						<div><?php echo $user_ip_log['User_ip_log']['guicountry']; ?></div>
						<div>
                            <?php 
							if(!isset($SubadminAccessArray) || in_array('logs',$SubadminAccessArray) || in_array('logs/adminpanel_memberstatus',$SubadminAccessArray)){ 
                                if($user_ip_log['User_ip_log']['ip_status']==0){
                                    $statusaction='1';
                                    $statusicon='red-icon.png';
                                    $statustext='Unblock IP Address';
                                }else{
                                    $statusaction='0';
                                    $statusicon='blue-icon.png';
                                    $statustext='Block IP Address';}
                                echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)), array('controller'=>'member', "action"=>"iplogstatus/".$member_id."/".$user_ip_log['User_ip_log']['id']."/".$statusaction."/".$currentpagenumber), array(
                                    'update'=>'#memberdetailpage',
                                    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                    'escape'=>false,
                                    'class'=>'vtip',
                                    'title'=>$statustext
	                            ));
							}
							else
							{
								echo '&nbsp;';
							}?>
                        </div>
					</div>
				<?php endforeach; ?>
				
		</div>
		 <?php if(count($user_ip_logs)==0){ echo '<div class="norecordfound">No records available</div>';}?>
	 <div class="margin10">
	<?php 
	if($this->params['paging']['User_ip_log']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'iplog/0/rpp')));?>
		<div class="resultperpage">
				<label>
						<?php 
						echo $this->Form->input('resultperpage', array(
						  'type' => 'select',
						  'options' => $resultperpage,
						  'selected' => $this->Session->read('pagerecord'),
						  'class'=>'',
						  'label' => false,
						  'div'=>false,
						  'style' => '',
						  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
						));
						?>
				</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#memberdetailpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'member',
			  'action'=>'iplog/'.$member_id.'/0/rpp/',
			  'url'   => array('controller' => 'member', 'action' => 'iplog/'.$member_id.'/0/rpp/')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	</div>
	<div class="height7"></div>
	</div>
</div>
<div class="height10"></div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>