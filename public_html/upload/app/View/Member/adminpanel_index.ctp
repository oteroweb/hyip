<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-12-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Member / List</div>
<div id="memberpageTK">
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<script type="text/javascript">
	<?php if($searchby=='membergroup'){ ?>
	generatecombo("data[Member][groupname]","Membergroup","id","groupname","<?php echo $searchfor; ?>",'.searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } elseif($searchby=='country'){ ?>
	generatecombo("data[Member][country_name]","Country","id","country_name","<?php echo $searchfor; ?>",'.searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } elseif($searchby=='guicountry'){ ?>
	generatecombo("data[Member][guicountry]","Member","guicountry","guicountry","<?php echo $searchfor; ?>",'.searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } elseif($searchby=='membership'){ ?>
	generatecombo("data[Member][membership_name]","Membership","id","membership_name","<?php echo $searchfor; ?>",'.searchforcombo',"<?php echo $ADMINURL;?>","");
	<?php } ?>
</script>
<div class="serchmainbox">
    <div class="serchgreybox">Search Option</div>
    <?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'index')));?>
	<div class="from-box">
       <div class="fromboxmain">
            <span>Search By :</span>
            <span>
		
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
					<?php
					$searchbyoptions=array('all'=>'Select Parameter', 'member_id'=>'Member Id', 'inactive'=>'Inactive Members', 'active'=>'Active Members', 'delete'=>'Deleted Members', 'unpaid'=>'Unpaid Members', 'paid'=>'Paid Members', 'user_name'=>'Username', 'email'=>'Email', 'f_name'=>'First Name', 'l_name'=>'Last Name','membergroup'=>'Member Group', 'country'=>'Country', 'guicountry'=>'IP Country', 'membership'=>'Membership', 'referrer'=>'Referrer Id','register_ip'=>'IP Address');
					
					$onchangestr="";
					$modules=@explode(",",trim($SITECONFIG["modules"],","));
					foreach($modules as $module)
					{
						$modulearray=@explode(":", $module);
						if($modulearray[2]==1 && $modulearray[3]!="" && $modulearray[4]!="" && $modulearray[5]==1)
						{
							$subplanarray=explode("-",$modulearray[3]);
							$subpositionarray=explode("-",$modulearray[4]);
							$subplanname=explode("-",$modulearray[13]);
							$subcounter=1;
							foreach($subplanarray as $subplan)
							{
								if(count($subplanarray)==1)
									$searchbyoptions["MODULE".$subpositionarray[$subcounter-1]]=$modulearray[0];
								else
									$searchbyoptions["MODULE".$subpositionarray[$subcounter-1]]=$subplanname[$subcounter-1]." ".$modulearray[0];
								$onchangestr.='else if($(this).val()=="MODULE'.$subpositionarray[$subcounter-1].'"){generatecombo("data[Member][MODULE'.$subpositionarray[$subcounter-1].']","'.$subplan.'","id","plan_name","'.$searchfor.'",".searchforcombo","'.$ADMINURL.'","");$(".searchfor_all").hide(500);$(".searchforcombo").show(500);}';
								
								if($searchby=='MODULE'.$subpositionarray[$subcounter-1])
									echo '<script type="text/javascript">generatecombo("data[Member][MODULE'.$subpositionarray[$subcounter-1].']","'.$subplan.'","id","plan_name","'.$searchfor.'",".searchforcombo","'.$ADMINURL.'","");</script>';
								$subcounter++;
							}
						}
					}
					?>
					<?php
					echo $this->Form->input('searchby', array(
						'type' => 'select',
						'options' => $searchbyoptions,
						'selected' => $searchby,
						'class'=>'',
						'label' => false,
						'style' => '',
						'onchange' => 'if($(this).val()=="delete" || $(this).val()=="inactive" || $(this).val()=="active" || $(this).val()=="unpaid" || $(this).val()=="paid"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);}if($(this).val()=="membergroup"){generatecombo("data[Member][groupname]","Membergroup","id","groupname","'.$searchfor.'",".searchforcombo","'.$ADMINURL.'","");$(".searchfor_all").hide(500);$(".searchforcombo").show(500);}else if($(this).val()=="country"){generatecombo("data[Member][country_name]","Country","id","country_name","'.$searchfor.'",".searchforcombo","'.$ADMINURL.'","");$(".searchfor_all").hide(500);$(".searchforcombo").show(500);}else if($(this).val()=="guicountry"){generatecombo("data[Member][guicountry]","Member","guicountry","guicountry","'.$searchfor.'",".searchforcombo","'.$ADMINURL.'","");$(".searchfor_all").hide(500);$(".searchforcombo").show(500);}else if($(this).val()=="membership"){generatecombo("data[Member][membership_name]","Membership","id","membership_name","'.$searchfor.'",".searchforcombo","'.$ADMINURL.'","");$(".searchfor_all").hide(500);$(".searchforcombo").show(500);} '.$onchangestr.' else{$(".searchforcombo").hide(500);$(".searchfor_all").show(500);}'
					));
					?>
					</label>
				</div>
			</div>
             </span>
		 </div>
         <div class="fromboxmain" id="SearchFor" <?php if($searchby=="delete" || $searchby=="inactive" || $searchby=="active" || $searchby=="unpaid" || $searchby=="paid"){ echo 'style="display:none"';} ?>>
			<span>Search For :</span>
				<span class="searchfor_all" style="display:<?php if($searchby=='country' || $searchby=='guicountry' || $searchby=='membership' || $searchby=='membergroup' || substr($searchby, 0, 6)=='MODULE'){echo 'none';}?>;">
					<?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?>
				</span>
				<span class="searchforcombo" style="display:<?php if($searchby=='country' && $searchby=='guicountry' && $searchby=='membership' && $searchby=='membergroup' && substr($searchby, 0, 6)=='MODULE'){echo 'none';}?>;"></span>
		 </div>
     </div>
	 <div class="from-box">
         <div class="fromboxmain width480">
             <span>From :</span>
              <span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
         </div>
         <div class="fromboxmain">
             <span>To :</span>
             <span><?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
             <span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#memberpage',
					'class'=>'searchbtn',
					'controller'=>'member',
					'action'=>'index',
					'url'   => array('controller' => 'member', 'action' => 'index')
				));?>
			 </span>
         </div>
    </div>
	 <?php echo $this->Form->end();?>
</div>
<?php }?>
<span id="memberpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Member']['page'];
	?>

<div id="gride-bg" class="noborder">
    <div class="padding10">
		<div class="greenbottomborder">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">		
    	<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_memberadd',$SubadminAccessArray)){ ?>
			<?php echo $this->Js->link("+ Add New", array('controller'=>'member', "action"=>"memberadd"), array(
                'update'=>'#memberpageTK',
                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                'escape'=>false,
                'class'=>'btnorange'
            ));?>
        <?php } ?>
        <?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_membercsv',$SubadminAccessArray)){ ?>
			<?php echo $this->Html->link("Download CSV File", array('controller'=>'member', "action"=>"membercsv"), array(
                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                'escape'=>false,
                'class'=>'btngray',
                'target'=>'_blank'
            ));?>
        <?php } ?>
	</div>
	<div class="clear-both"></div>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('M. Id', array('controller'=>'member', "action"=>"index/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Reg. Date', array('controller'=>'member', "action"=>"index/0/0/reg_dt/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Registration Date'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Username', array('controller'=>'member', "action"=>"index/0/0/user_name/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Username'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Full Name', array('controller'=>'member', "action"=>"index/0/0/f_name/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By First Name'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Email', array('controller'=>'member', "action"=>"index/0/0/email/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Email'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Sponsor', array('controller'=>'member', "action"=>"index/0/0/referrer/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Sponsor Id'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Tot Ref.', array('controller'=>'member', "action"=>"index/0/0/total_referrer/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Total Referrals'
					));?>
				</div>
				<div>Action</div>
		</div>
		
			<?php foreach ($members as $member): ?>
				<div class="tablegridrow">
					<div style='background-color:<?php echo $member['Membergroup']['groupcolor'];?>'>
						<?php echo $this->Js->link($member['Member']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top"), array(
							'update'=>'#memberpageTK',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $member['Member']['reg_dt']);?></div>
					<div>
				         <?php echo $this->Js->link($member['Member']['user_name'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top"), array(
							'update'=>'#memberpageTK',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>
							<?php echo $this->Js->link($member['Member']['f_name']." ".$member['Member']['l_name'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top"), array(
								'update'=>'#memberpageTK',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
					</div>
					<div>
						<?php echo $this->Js->link($member['Member']['email'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top"), array(
							'update'=>'#memberpageTK',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>
						<?php if($member['Member']['referrer']!=0){ echo $this->Js->link($member['Member']['referrer'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['referrer']."/top"), array(
							'update'=>'#memberpageTK',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Sponsor'
						)); }else{echo '-';}?>
					</div>
					<div><?php echo $member['Member']['total_referrer']; ?></div>
					<div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							
							<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_memberadd/$',$SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('men-icon2.png', array('alt'=>'Edit Member')).' Edit Member', array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top"), array(
									'update'=>'#memberpageTK',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));?>
							</li>
                            <?php } ?>
							
                          	<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_memberstatus',$SubadminAccessArray)){ ?>
							<li>
								<?php /*echo $this->Js->link($this->html->image('lock_opened.png', array('alt'=>'Login In Member Area')), array('controller'=>'member', "action"=>"login/".$member['Member']['member_id']), array(
									'update'=>'#memberpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>'Login In Member Area'
								));*/?>
								<a class="" onclick="return confirm('Take a Note That Member Logs Will Not Be Created When You Log in From Here. Continue?');" title="" target="_blank" href="<?php echo $ADMINURL;?>/member/login/<?php echo $member['Member']['member_id'];?>"><?php echo $this->html->image('lock_opened.png', array('alt'=>'Login to '.$member['Member']['user_name'].'\'s Member Area')).' Login to '.$member['Member']['user_name'].'\'s Member Area';?></a>
							</li>
							<li>
								<?php 
								if($member['Member']['member_id']!=1){
								if($member['Member']['active_status']==0){
									$statusaction='1';
									$statusicon='red-icon.png';
									$statustext='Activate Member';
								}else{
									$statusaction='0';
									$statusicon='blue-icon.png';
									$statustext='Inactivate Member';}
								echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'member', "action"=>"memberstatus/".$statusaction."/".$member['Member']['member_id']."/".$currentpagenumber), array(
									'update'=>'#memberpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));
								}?>
							</li>
			    <?php } ?>
							
                            <?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_memberremove',$SubadminAccessArray)){ ?>
							<li>
								<?php if($member['Member']['member_id']!=1){?>
									<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Member')).' Delete Member', array('controller'=>'member', "action"=>"memberremove/".$member['Member']['member_id']."/".$currentpagenumber), array(
                                        'update'=>'#memberpage',
                                        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                        'escape'=>false,
                                        'confirm'=>"Do you really want to delete this member? This action is irreversible.\nAll the data related to this member will be removed.\nLike banners, text ads, solo ads, and all the statistics?"
                                    ));?>
								<?php }?>
							</li>
                            <?php } ?>
						   </ul>
						</div>
					  </div>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	
	<?php if(count($members)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    
	<?php 
	if($this->params['paging']['Member']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'index/0/rpp')));?>
		<div class="resultperpage">
			<label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#memberpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'Member',
			  'action'=>'index/0/rpp',
			  'url'   => array('controller' => 'member', 'action' => 'index/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</span><!--#memberpage over-->
</div><!--#memberpageTK over-->				
<?php }?>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>