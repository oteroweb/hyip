<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 17-12-2014
  *********************************************************************/
?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php ///tempcode start ?>
<script>
$(document).ready( function() {
	$("#tab ul li a").click(function() {
		$("#tab ul li a").removeClass('active');
		$(this).addClass('active');
	});
});
</script>
<script>
function countdown_start22(pending_seconds)
{
	dday=Math.floor(pending_seconds/(60*60*24)*1)
	dhour=Math.floor((pending_seconds%(60*60*24))/(60*60)*1)
	dmin=Math.floor(((pending_seconds%(60*60*24))%(60*60))/(60)*1)
	dsec=Math.floor((((pending_seconds%(60*60*24))%(60*60))%(60))/1)
	
	if(dday==0&&dhour==0&&dmin==0&&dsec==0)
	{
		window.location="<?php echo $SITEURL.'/member';?>";
		return
	}
	else
	{
		if(dday>0)
		{
			dhour = dhour+dday*24;
			//document.getElementById("kk123").innerHTML = dday+ " days, "+dhour+" hrs, "+dmin+" mnts,"+dsec+" sec ";
			document.getElementById("kk123").innerHTML = "<b>"+dhour+"</b> hrs, <b>"+dmin+"</b> mnts,<b>"+dsec+"</b> sec ";
		}
		else
		{
			document.getElementById("kk123").innerHTML = "<b>"+dhour+"</b> hrs, <b>"+dmin+"</b> mnts,<b>"+dsec+"</b> sec ";
			//document.forms.count.count2.value=dhour+" hrs, "+dmin+" mnts,"+dsec+" sec ";	
		}
		pending_seconds--;
		setTimeout("countdown_start22("+pending_seconds+")",1000);
	}	
}
</script>  
<div class="comisson-bg-new">
	<div class="text-ads-title-text"><?php echo __("Member Overview"); ?></div>
	<div class="clear-both"></div>
</div>
<div class="left-part mobilewidth100">
	<?php if(strpos($SITECONFIG["bizdirectorysetting"],'enablesurffree|1')!==false || strpos($SITECONFIG["surfingsetting"],'enablesurfing|1')!==false){?>
	<div class="profilegreymain surfbarmain">
	<?php if(strpos($SITECONFIG["surfingsetting"],'enablesurfing|1')!==false){?>	
		<div class="greybox">
		  <div class="greyboxpadding">
				<span class="glyphicon glyphicon-calendar"></span>
				<div class="surftext">
					<?php if($nextclicktime>date('Y-m-d H:i:s')){ ?>
					<div style="">
						<?php __("Surf"); echo $memberclicklimit-$clicktoday; echo __("Ads Again After"); ?>
						<?php echo $nextclicktime; ?>
						<div id="kk123"  class="counter_display" style="padding-top:5px;"></div>
						<?php $to_seconds =  strtotime($nextclicktime);
						$pending_secnods = $to_seconds-strtotime(date("Y-m-d H:i:s")); ?>
						<script type="text/javascript">countdown_start22(<?php echo $pending_secnods; ?>);</script>
					</div>
					<?php } else { ?>
					<div style="">
						<?php if((strpos($SITECONFIG["surfingsetting"],'surfselect|0') !== false) && (strpos($SITECONFIG["trafficsetting"],'enablesurfing|1') !== false)) { ?>
						<?php $memberremaining=$memberclicklimit-$clicktoday; ?>
						<a href="<?php echo str_replace("https://", "http://", $SITEURL);?>traffic/surf"><?php echo __("You Are Yet To Surf <b>".$memberremaining."</b> Ads Today!"); ?></a>
						<?php /* $memberremaining=$memberclicklimit-$clicktoday; echo $this->html->link(__("You Are Yet To Surf")."<b> ".$memberremaining."</b> ".__("Ads Today!"), array('controller' => 'directory', 'action' => 'view', 'plugin' => false),array('escape'=>false));*/ ?>
						<?php } if((strpos($SITECONFIG["surfingsetting"],'surfselect|1') !== false) && (strpos($SITECONFIG["bizdirectorysetting"],'enablesurfing|1') !== false)) { ?>
						<?php $memberremaining=$memberclicklimit-$clicktoday; ?>
						<a href="<?php echo str_replace("https://", "http://", $SITEURL);?>directory/view"><?php echo __("You Are Yet To Surf <b>".$memberremaining."</b> Ads Today!"); ?></a>
						<?php } ?>
					</div>
					<?php } ?>
				</div>
		  </div>
		</div>
		<?php }?>
		
		<div class="greybox">
		<?php if(strpos($SITECONFIG["bizdirectorysetting"],'enablesurffree|1')!==false){?>
		  <div class="greyboxpadding">
			<?php echo $this->html->link(__("Buy Surf Free Units"), array('controller' => 'directory', 'action' => 'surffreeplan', 'plugin' => false));?>
			
		  </div>
		<?php }?>
		</div>
		
	</div>
	<?php } ?>
    <div class="profile-completeness">
		<div class="profile-completeness-text"><?php echo __("Profile Completeness") ; ?></div>
		<div class="progressbarbox">
			<div id="profilemeter"  class="progressbar" style="position:relative;"><div></div></div>
			<script>progressBar(<?php echo $ProfileCompletedRate;?>, $('#profilemeter'));</script>
		</div>
		<div class="profile-complet-button"><?php echo $this->html->link(__("Complete Profile"), array('controller' => 'member', 'action' => 'profile', 'plugin' => false), array('class' => 'profilebutton'));?></div>
		<div class="clearboth"></div>
    </div>
    <div class="memberprofile">
		<div class="oviprofile">
			<?php
				if($MemberPhoto==NULL || $MemberPhoto=='')
				{
					echo $this->html->image("member/dummy.jpg", array('alt'=>''));
				}
				else
				{
					echo $this->html->image("member/".$MemberPhoto.'?tk='.date('sHi'), array('alt'=>''));
				}
			?>
		</div>
		<div class="oviprofileright">
			<?php $username=$this->Session->read("username");?>
			<div class="memberprofilewhitebox"><span class="oviusernametextblue"><b><?php echo __("Username"); ?> :</b> <?php echo $username;?> </span>  | <span class="oviusernametextyellow"> <b><?php echo __("Member Id"); ?> :</b> <?php echo $UID;?>  </span>|  <b><?php echo __("Sponsor Id"); ?> :</b> <?php echo $MemberReferrer;?></div>
			<div class="memberprofilewhitebox"><span class="oviusernametextred"><b><?php echo __("Registration Date"); ?> :</b> <?php echo $this->Time->format($SITECONFIG["timeformate"], $this->Session->read("reg_dt"));?> </span> | <span class="oviusernametextdarkblue"> <b><?php echo __("Last Login Ip"); ?> :</b> <?php echo $this->Session->read('lastip');?></span></div>
		</div>
		<div class="clearboth"></div>
    </div>
    <div class="refferallinktext"><?php echo __("Referral Link"); ?></div>
	<div class="refferalbox">
		<div class="reffericon">
		  <?php echo $this->html->image("referrallink.png", array('alt'=>'')); ?>
		</div>
		<div class="refferright">
			<div class="memberprofilewhitebox"> 
                            <?php if($SITECONFIG["reflinkiduser"]==0)
				{
					?><a href="<?php echo $SITEURL;?>ref/<?php echo $UID;?>"><?php echo $SITEURL;?>ref/<?php echo $UID;?></a><?php
				}
				else
				{
					?><a href="<?php echo $SITEURL;?>ref/<?php echo $username;?>"><?php echo $SITEURL;?>ref/<?php echo $username;?></a><?php
				}?>
                        </div>
			<div class="totalmembertext"><?php echo __("Total Referrals"); ?> : <?php echo $TotalReferrer; ?></div>
		</div>
		<div class="clearboth"></div>
	</div>
    <div class="profilegreymain">
		<div class="greybox <?php if($membermembership==''){echo 'width100';}?>">
			<div class="greyboxpadding">
				<div class="currunticonmain">
				  <?php echo $this->html->image("current-icon.png", array('alt'=>'')); ?>
				</div>
				<div class="currunticonright">
				  <span class="currenttext"><?php echo __("Current Server Time");?> :</span><br/>
				  <span class="curruntbluetext">
					<span id="servertime"></span>
					<!-- For Server Time -->
					<script type="text/javascript">
						var currenttime = '<?php print $PHPTime;?>';
						var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
						var serverdate=new Date(currenttime);
						function padlength(what){
							var output=(what.toString().length==1)? "0"+what : what;
							return output;
						}
						function displaytime(){
							serverdate.setSeconds(serverdate.getSeconds()+1);
							var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear();
							var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds());
							document.getElementById("servertime").innerHTML=datestring+" "+timestring;
						}
						window.onload=function(){
						setInterval("displaytime()", 1000);
						}
					</script>
				  </span>
				</div>
			</div>
		</div>
		<?php if($membermembership!=''){?>
		<div class="greybox">
			<div class="greyboxpadding">
				<div class="currunticonmain2"><?php echo $this->html->image("membership.png", array('alt'=>'')); ?></div>
				<div class="currunticonright">
					<span class="currenttext"><?php echo __("Membership Status"); ?> :<span class="curruntbluetext"><?php echo ($membermembership=='')?__('Unpaid'):__('Paid'); ?></span></span><br/>
					<span class="currenttext"><?php echo __("Membership Name"); ?> :<span class="curruntbluetext"><?php echo $membermembership ; ?></span></span>
				</div>
			</div>
		</div>
		<?php }?>
		<?php if($membermembership!='' || $CommissionPosition>0){ ?>
		<div class="greybox">
		  <div class="greyboxpadding">
			<div class="currunticonmain2"><?php echo $this->html->image("commissionicon.png", array('alt'=>'')); ?></div>
			<div class="currunticonright">
				<?php if($membermembership!=''){?>
				<span class="currenttext"><?php echo __("Commission on Fees"); ?> : <span class="curruntbluetext"><?php echo $Currency['prefix'];?><?php echo round($CommissionMemberfee*$Currency['rate'],2);?></span></span><br/>
				<?php } ?>
				<?php if($CommissionPosition>0){?>
				<span class="currenttext"><?php echo __("Commission on Purchases"); ?> : <span class="curruntbluetext"><?php echo $Currency['prefix'];?><?php echo round($CommissionPosition*$Currency['rate'],2);?></span></span>
				<?php } ?>
			</div>
		  </div>
		</div>
		<?php } ?>
		<?php if((($CommissionBonus>0 && strpos($SITECONFIG['modules'], ":module1:1")!==false)) || ($CommissionMatrix>0 && (strpos($SITECONFIG['modules'], ":module2:1")!==false || strpos($SITECONFIG['modules'], ":module4:1")!==false))){ ?>
		<div class="greybox">
		  <div class="greyboxpadding">
			<div class="currunticonmain"><?php echo $this->html->image("miscell.png", array('alt'=>'')); ?></div>
			<div class="currunticonright">
				<?php if($CommissionBonus>0 && strpos($SITECONFIG['modules'], ":module1:1")!==false){ ?>
				<span class="currenttext"><?php echo __("Total Miscellaneous Bonus");?> : <span class="curruntbluetext"><?php echo $Currency['prefix'];?><?php echo round(($CommissionBonus)*$Currency['rate'],2);?></span></span><br/>
				<?php } if($CommissionMatrix>0 && (strpos($SITECONFIG['modules'], ":module2:1")!==false || strpos($SITECONFIG['modules'], ":module4:1")!==false)){ ?>
				<span class="currenttext"><?php echo __("Total Matching Bonus");?> : <span class="curruntbluetext"> <?php echo $Currency['prefix'];?><?php echo round(($CommissionMatrix)*$Currency['rate'],2);?></span></span>
				<?php } ?>
			</div>
		  </div>
		</div>
		<?php } ?>
	</div>
	
	<?php if($SITECONFIG["enable_textads"]==1 || $SITECONFIG["enable_bannerads"]==1 || $SITECONFIG["enable_soloads"]==1 || strpos($SITECONFIG["surfingsetting"],'enablesurfing|1')!==false ){?>
	<div class="addcreditmain">
		<div class="addcredittitle"><?php echo __("Ad Credits"); ?></div>
		<?php if($SITECONFIG["enable_textads"]==1){?>
		<div class="adcreditsbox">
			<div class="adcredittype"><b><?php echo __("Text");?> :</b> <?php echo $MemberTextCredits;?></div>
		    <div class="assignlink"><?php echo $this->html->link("[".__("Assign")."]", array('controller' => 'textad', 'action' => 'index', 'plugin' => false), array('class' => ''));?></div>
		</div>
		<?php } ?>
		<?php if($SITECONFIG["enable_bannerads"]==1){?>
		<div class="adcreditsbox">
			<div class="adcredittype"><b><?php echo __("Banner");?> :</b> <?php echo $MemberBannerCredits;?></div>
			<div class="assignlink"><?php echo $this->html->link("[".__("Assign")."]", array('controller' => 'bannerad', 'action' => 'index', 'plugin' => false), array('class' => ''));?></div>
		</div>
		<?php } ?>
		<?php
		if($SITECONFIG["enable_soloads"]==1){?>
		<div class="adcreditsbox">
			<div class="adcredittype"><b><?php echo __("Solo");?> :</b> <?php echo $MemberSoloCredits;?></div>
			<div class="assignlink"><?php echo $this->html->link("[".__("Assign")."]", array('controller' => 'soload', 'action' => 'index', 'plugin' => false), array('class' => ''));?></div>
		</div>
		<?php } ?>
		<?php
		if(strpos($SITECONFIG["surfingsetting"],'enablesurfing|1')!==false){
		if((strpos($SITECONFIG["surfingsetting"],'surfselect|0') !== false) && (strpos($SITECONFIG["trafficsetting"],'enablewebsites|1') !== false)) { ;
		?>
		<div class="adcreditsbox">
			<div class="adcredittype"><b><?php echo __("Website");?> :</b> <?php echo $MemberWebsiteCredits;?></div>
			<div class="assignlink"><?php echo $this->html->link("[".__("Assign")."]", array('controller' => 'traffic', 'action' => 'website', 'plugin' => false), array('class' => ''));?></div>
		</div>
		<?php }} ?>
		
    </div>
	<?php } ?>
    <div class="refferallinktext"><?php echo __("Plan Earnings"); ?></div>
    <div class="refferaldarkblue">
		<div class="planearningmain notmobile">
			<div class="planearningleft">&nbsp;</div>
			<div class="planearningmiddle"><div class="planearningheadtop">Total Position Earnings</div></div>
			<div class="planearningright"><div class="planearningheadtop">Total Positions Spot</div></div>
			<div class="clearboth"></div>
		</div>
		
		<?php if(count($moduledata)>0){foreach($moduledata as $module){ ?>
			<div class="planearningmain">
				<div class="planearningleft mobilewidth100"><div class="planearninglefthead"><?php echo $module['planname'];?></div></div>
				
				<div class="planearningmiddle"><div class="planearningheadtop showonmobile">Total Position Earnings</div></div>
				<div class="planearningright"><div class="planearningheadtop showonmobile">Total Positions Spot</div></div>
				<div class="planearningmiddle"><div class="planearningdata"><?php echo $Currency['prefix'];?><?php echo round($module['PositionEarning']*$Currency['rate'],2)." ".$Currency['suffix'];?></div></div>
				<div class="planearningright"><div class="planearningdata"><?php echo $module['ActivePosition']; ?> (<?php echo $module['TotalPosition'];?>)</div></div>
				<div class="clearboth"></div>
			</div>
		<?php }} ?>
    </div>
	<a href="<?php echo $SITEURL ?>member/commission" class="view-all">[<?php echo __("View All"); ?>]</a>
	<div class="clearboth"></div>
	<?php if(isset($_COOKIE['rv'])){?>
    <div class="recentbox">
		<div class="recentwhite"><?php echo $this->html->image("recenticon.png", array('alt'=>'')); ?></div>
		<div class="recentright"><?php echo __("Recent Viewed Pages"); ?></div>
		<div class="recentbluebox">
	      <ul class="recenticon">
			<?php $recentviewdpages=@explode("~" , trim($_COOKIE['rv'],"~"));
			if(count($recentviewdpages)>0){
			for($i=0; $i<count($recentviewdpages); $i++){$rvpages=@explode("^",$recentviewdpages[$i]);?>
				<li><a href="<?php echo @$rvpages[1];?>"><?php echo @$rvpages[0];?></a></li>
			<?php } ?>
			
			<?php }else{ echo "<li>".__('No records available')."</li>"; } ?>
	      </ul>
		</div>
    </div>
	<?php } ?>
</div>
<div class="right-part mobilewidth100">
    <div class="mo-title"><?php echo __("Financial Info"); ?></div>
	<?php if($SITECONFIG["balance_type"]!=1){ ?><div class="clicktext"><?php echo __("Click on the Payment Processor icon to view balance"); ?></div><?php } ?>
	<div class="clearboth"></div>
    <div class="financial-Info-main">
		<?php if($SITECONFIG["balance_type"]==1){ ?>	
		<div class="refferaldarkblue" style="padding-top: 10px;">
			<div class="planearningmain">
				<div class="planearningleft mobilewidth100"><div class="planearninglefthead"><?php echo __("Cash Balance"); ?></div></div>
				<div class="textcenter"><div class="planearningdata"><?php echo $Currency['prefix'];?><?php echo round($MemberCash*$Currency['rate'],2)." ".$Currency['suffix'];?></div></div>
				<div class="clearboth"></div>
			</div>
			<?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
			<div class="planearningmain">
				<div class="planearningleft mobilewidth100"><div class="planearninglefthead"><?php echo __("Earning Balance"); ?></div></div>
				<div class="textcenter"><div class="planearningdata"><?php echo $Currency['prefix'];?><?php echo round($MemberEarningCash*$Currency['rate'],2)." ".$Currency['suffix'];?></div></div>
				<div class="clearboth"></div>
			</div>
			<?php } ?>
			<div class="planearningmain">
				<div class="planearningleft mobilewidth100"><div class="planearninglefthead"><?php echo __("Re-purchase Balance"); ?></div></div>
				<div class="textcenter"><div class="planearningdata"><?php echo $Currency['prefix'].round($MemberRepurchaseCash*$Currency['rate'],2)." ".$Currency['suffix'];?></div></div>
				<div class="clearboth"></div>
			</div>
			<?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
			<div class="planearningmain">
				<div class="planearningleft mobilewidth100"><div class="planearninglefthead"><?php echo __("Commission Balance"); ?></div></div>
				<div class="textcenter"><div class="planearningdata"><?php echo $Currency['prefix'];?><?php echo round($MemberCommissionCash*$Currency['rate'],2)." ".$Currency['suffix'];?></div></div>
				<div class="clearboth"></div>
			</div>
			<?php } ?>
		</div>
		<?php }else{ ?>
		
			<?php $i=1;
				foreach($MemberCash as $proc_name=>$cash)
				{
					if($i%2==0){$class='gray-color';}else{$class='white-color';}?>
					
					<div class="financial-Info-box">
						<a href="#" onclick="return false" tabindex="<?php echo $i; ?>"><?php echo $this->html->image($proc_name.".png", array('alt'=>'')); ?></a>
						<div class="popupbox">
							<div class="arrow"></div>
							<div class="popupbox-content">
								<div class="processor-left"><?php echo __("Payment Processor");?> :</div>
								<div class="processor-right"><?php echo $proc_name;?></div>
								<div class="processor-left"><?php echo __("Cash Balance"); ?> :</div>
								<div class="processor-right"><?php echo $Currency['prefix'].round($cash*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
								<div class="processor-left"><?php echo __("Re-purchase Balance"); ?> :</div>
								<div class="processor-right"><?php echo $Currency['prefix'].round($MemberRepurchaseCash[$proc_name]*$Currency['rate'],2)." ".$Currency['suffix']; ?></div>
                                                                <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
								<div class="processor-left"><?php echo __("Commission Balance"); ?> :</div>
								<div class="processor-right"><?php echo $Currency['prefix'].round($MemberCommissionCash[$proc_name]*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
                                                                <?php } ?>
                                                                <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
								<div class="processor-left"><?php echo __("Earning Balance"); ?> :</div>
								<div class="processor-right"><?php echo $Currency['prefix'].round($MemberEarningCash[$proc_name]*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
                                                                <?php } ?>
								<div class="processor-left"><?php echo __("Payment Processor Id");?> :</div>
								<div class="processor-right"><?php echo $MemberProcessors[$proc_name];?></div>
							</div>
						</div>
					</div>
					
			<?php $i++; } ?>
		<div class="preferred-processor"><?php echo __("Preferred Processor"); ?> :
		<?php
		if($MemberPriorityProcessor=="N/A")
		{
			echo "N/A";
		}
		else
		{?>
			<?php echo $this->html->image($MemberPriorityProcessor.".png", array('alt'=>'')); 
		}?>
		<?php echo $this->html->link("[".__("Change It")."]", array('controller' => 'member', 'action' => 'profile', 'plugin' => false), array('class' => 'change-It'));?></div>
		<?php } ?>
    </div>
    <div class="mo-title"><?php echo __("Activity Logs"); ?></div>
	<div class="divtable2 overview">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam"><?php echo __("Activity"); ?></div>
				<div class="divth textcenter vam" style="width: 100px;"><?php echo __("IP Address"); ?></div>
				<div class="divth textcenter vam"><?php echo __("Date"); ?></div>
			</div>
		</div>
		<div class="divtbody">
			<?php $i=1;
			foreach ($Memberlogs as $Memberlog):
				if($i%2==0){$class='gray-color';}else{$class='white-color';}?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $Memberlog['Memberlog']['log']; ?></div>
					<div class="divtd textcenter vam"><?php echo $Memberlog['Memberlog']['ipaddress']; ?></div>
					<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $Memberlog['Memberlog']['logdate']); ?></div>
				</div>
			<?php $i++;endforeach; ?>
		</div>
	</div>
	<?php echo $this->html->link("[".__("View All")."]", array('controller' => 'member', 'action' => 'memberactivity', 'plugin' => false), array('class' => 'view-all'));?>
	<div class="tab-blue-box">
		<div id="tab">
			  <ul id="myTab" class="nav nav-tabs">
				<li class="">
					<?php
					echo $this->Js->link(__('Last 5 Withdrawals'), array('controller'=>'member', "action"=>"lastwithdrowrequest"), array(
						'update'=>'#lasttenrecored',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'active'
					));
					?>
				</li>
				<li>
					<?php
					echo $this->Js->link(__('Last 5 Add Funds'), array('controller'=>'member', "action"=>"lastaddfundrequest"), array(
						'update'=>'#lasttenrecored',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
					));
					?>
				</li>
				<li>
					<?php
					echo $this->Js->link(__('Other Payments'), array('controller'=>'member', "action"=>"lastotherpayment"), array(
						'update'=>'#lasttenrecored',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
					));
					?>
				</li>
			  </ul>
			</div>
		</div>
		<div id="lasttenrecored" class="tab-content">
			<div class="tab-pane active" id="last5withdrawals">
				<div class="divtable2 overview">
					<div class="divthead">
						<div class="divtr mo-tabal-title-text">
							<div class="divth textcenter vam"><?php echo __("Amount"); ?> </div>
							<div class="divth textcenter vam"><?php echo __("Fees"); ?></div>
							<div class="divth textcenter vam"><?php echo __("Request Date"); ?></div>
							<div class="divth textcenter vam"><?php echo __("Status"); ?></div>
						</div>
					</div>
					<div class="divtbody">
						<?php $i=1;
						foreach ($withdrowrequests as $withdrowrequest):
							if($withdrowrequest['Withdraw']['status']=="Paid Manually") $withdrowrequest['Withdraw']['status']='Done';
							if($i%2==0){$class='gray-color';}else{$class='white-color';}?>
							<div class="divtr <?php echo $class;?>">
								<div class="divtd textcenter vam">
									<?php echo $Currency['prefix'].round($withdrowrequest['Withdraw']['amount']*$Currency['rate'],2)." ".$Currency['suffix'];?>
								</div>
								<div class="divtd textcenter vam">
									<?php echo $Currency['prefix'].round($withdrowrequest['Withdraw']['fee']*$Currency['rate'],2)." ".$Currency['suffix'];?>
								</div>
								<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $withdrowrequest['Withdraw']['req_dt']); ?></div>
								<div class="divtd textcenter vam"><?php echo ucwords(__($withdrowrequest['Withdraw']['status']));?></div>
							</div>
						<?php $i++;endforeach; ?>
					</div>
				</div>
			</div>
			<a href="<?php echo $SITEURL ?>member/withdrawal_history" class="view-all">[<?php echo __("View All"); ?>]</a>
			</div>
			<div class="recent-news-box">
				<div class="recentwhite"><?php echo $this->html->image("recentnewsicon.png", array('alt'=>'')); ?></div>
				<div class="recentright"><?php echo __("Recent News");?></div>
				<div class="recent-news-bluebox">
					<ul class="recenticon">
						<?php foreach ($RecentNews as $RecentNew):?>
						<li><a href="<?php echo $SITEURL;?>public/newsdetail/<?php echo $RecentNew['News']['news_id'];?>"><?php echo $RecentNew['News']['title'];?></a></li>
					<?php endforeach; ?>
					<?php if(count($RecentNews)==0) echo "<li>".__('No records available')."</li>"; ?>
						
					</ul>
				</div>	
			</div>
		</div>
	<div class="clearboth"></div>