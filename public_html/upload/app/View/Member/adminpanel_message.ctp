<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Member / Admin Message</div>
<div id="memberpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Admin_Message" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<div class="serchmainbox">
   <div class="serchgreybox">
      Search Option
   </div>
   <?php echo $this->Form->create('Membermessage_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'message')));?>
   <div class="from-box">
        <div class="fromboxmain width480">
           <span>From :</span>
           <span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
        </div>
        <div class="fromboxmain">
            <span>To :</span>
            <span><?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
            <span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#memberpage',
					'class'=>'searchbtn',
					'controller'=>'member',
					'action'=>'message',
					'url'   => array('controller' => 'member', 'action' => 'message')
				));?>
			</span>
        </div>
    </div>
   <?php echo $this->Form->end();?>
</div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'message')
	));
	$currentpagenumber=$this->params['paging']['Membermessage_history']['page'];
	?>
<div id="gride-bg" class="noborder">
    <div class="padding10">
	  <div class="greenbottomborder">
	  <div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
		<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_messageadd',$SubadminAccessArray)){ ?>
			<?php echo $this->Js->link("+ Add New", array('controller'=>'member', "action"=>"messageadd"), array(
                'update'=>'#memberpage',
                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                'escape'=>false,
                'class'=>'btnorange'
            ));?>
        <?php } ?>
		<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_messageremove',$SubadminAccessArray)){ ?>
			<?php echo $this->Js->link("Delete All Messages", array('controller'=>'member', "action"=>"messageremoveall"), array(
                'update'=>'#memberpage',
                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                'escape'=>false,
                'class'=>'btngray',
				'confirm'=>"Do You Really Want to Delete All Messages?"
            ));?>
        <?php } ?>
	</div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'member', "action"=>"message/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Date', array('controller'=>'member', "action"=>"message/0/0/messagedate/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Date'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Subject', array('controller'=>'member', "action"=>"message/0/0/description/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Subject'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Sent to', array('controller'=>'member', "action"=>"message/0/0/messagetype/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Sent to'
					));?>
				</div>
				<div>Action</div>
			</div>
			<?php foreach ($messages as $message): ?>
				<div class="tablegridrow">
					<div><?php echo $message['Membermessage_history']['id']; ?></div>
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $message['Membermessage_history']['messagedate']);?></div>
					<div>
								<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_messageadd/$',$SubadminAccessArray)){ ?>
								<?php echo $this->Js->link(stripslashes($message['Membermessage_history']['description']), array('controller'=>'member', "action"=>"messageadd/simple/".$message['Membermessage_history']['id']), array(
									'update'=>'#memberpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>'Edit Message'
								));?>
						        <?php }else{ echo stripslashes($message['Membermessage_history']['description']); }  ?>
					</div>
					<div><?php
								if($message['Membermessage_history']['messagetype']==1)
									echo "<a class='vtip' href='#' title='Member Id(s) : ".$message['Membermessage_history']['memberlist']."'>Members</a>";
								elseif($message['Membermessage_history']['messagetype']==2)
									echo "Paid Members";
								elseif($message['Membermessage_history']['messagetype']==3)
									echo "Unpaid Members";
								elseif($message['Membermessage_history']['messagetype']==4)
									echo "<a class='vtip' href='#' title='Membership Id(s) : ".$message['Membermessage_history']['memberlist']."'>Memberships</a>";
								elseif($message['Membermessage_history']['messagetype']==5)
									echo "<a class='vtip' href='#' title='Regular Earning Plan Id(s) : ".$message['Membermessage_history']['memberlist']."'>Regular Earning Plans</a>";			
								elseif($message['Membermessage_history']['messagetype']==6)
									echo "<a class='vtip' href='#' title='Dynamic Earning Plan Id(s) : ".$message['Membermessage_history']['memberlist']."'>Dynamic Earning Plans</a>";
								elseif($message['Membermessage_history']['messagetype']==7)
									echo "<a class='vtip' href='#' title='Revenue Sharing Plan Id(s) : ".$message['Membermessage_history']['memberlist']."'>Revenue Sharing Plans</a>";
								else
								    echo "All";
								?></div>
					<div class="textcenter">
					  <div class="actionmenu">
						<div class="btn-group">
						  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
							  Action <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
                          	<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_messageadd/$',$SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Message')).' Edit Message', array('controller'=>'member', "action"=>"messageadd/simple/".$message['Membermessage_history']['id']), array(
									'update'=>'#memberpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false
								));?>
							</li>
                            <?php } ?>
							<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_messageremove',$SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Message History')).' Delete Message History', array('controller'=>'member', "action"=>"messageremove/".$message['Membermessage_history']['id']."/".$currentpagenumber), array(
									'update'=>'#memberpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'confirm'=>"Do You Really Want to Delete This Message?"
								));?>
							</li>
                            <?php } ?>
						  </ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($messages)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
	<?php 
	if($this->params['paging']['Membermessage_history']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Membermessage_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'message/0/rpp')));?>
		
		<div class="resultperpage">
                        <label>
			   <?php 
			   echo $this->Form->input('resultperpage', array(
			     'type' => 'select',
			     'options' => $resultperpage,
			     'selected' => $this->Session->read('pagerecord'),
			     'class'=>'',
			     'label' => false,
			     'div'=>false,
			     'style' => '',
			     'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			   ));
			   ?>
			</label>
		</div>
		
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#memberpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'Membermessage_history',
			  'action'=>'index/0/rpp',
			  'url'   => array('controller' => 'member', 'action' => 'message/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#memberpage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>