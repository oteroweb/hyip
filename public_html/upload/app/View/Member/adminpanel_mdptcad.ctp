<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<script>
$(document).ready(function(){
	//$("a[rel='lightboxtext']").colorbox({width:"80%", height:"80%", inline:true, href:"#"+atag.title});
	$(".iframeclass").colorbox({width:"80%", height:"80%", iframe:true});
});
</script>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Paid_To_Click_-_PTC#PTC_Ads" target="_blank">Help</a></div>
<div class="tab-innar nomargin">
	<ul>
		<li>
			<?php echo $this->Js->link("Modules", array('controller'=>'member', "action"=>"mdposition/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Advertisement Plan", array('controller'=>'member', "action"=>"mdadvertisementplan/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'active'
			));?>
		</li>
	</ul>
</div>
<div class="positions-menu-drowp">
	<ul>
		<li>
			<?php echo $this->Js->link('Banner Ad', array('controller'=>'member', "action"=>"mdadvertisementplan/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('Text Ad', array('controller'=>'member', "action"=>"mdtextad/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('Solo Ad', array('controller'=>'member', "action"=>"mdsoload/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('Login Ad', array('controller'=>'member', "action"=>"mdloginad/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('PPC', array('controller'=>'member', "action"=>"mdppcad/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('PTC', array('controller'=>'member', "action"=>"mdptcad/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>'act'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('Biz Directory', array('controller'=>'member', "action"=>"mddirectory/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('Surf Free', array('controller'=>'member', "action"=>"mdsurffree/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
	</ul>
</div>
<div class="clear-both"></div>
<div class="height21"></div>
<div id="UpdateMessage"></div>
<?php if($IsAdminAccess){?>
<div class="serchmainbox">
	<?php echo $this->Form->create('Ptcbanner',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'mdptcad')));?>			
   <div class="serchgreybox">
      Search Option
   </div>
   <div class="from-box">
      <div class="fromboxmain">
      <span>Search By :</span>
      <span>                     
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
					<?php 
					echo $this->Form->input('searchby', array(
						  'type' => 'select',
						  'options' => array('all'=>'Select Parameter', 'id'=>'Banner Id', 'banner_size'=>'Banner Size', 'display_counter'=>'Displayed', 'click_counter'=>'Clicked', 'running'=>'Running Banners', 'expire'=>'Expire Banners', 'approve'=>'Approved Ads', 'unapprove'=>'Disapproved Ads', 'pause'=>'Paused Ads', 'unpause'=>'Active(Unpaused) Ads'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => '',
						  'onchange'=>'if($(this).val()=="approve" || $(this).val()=="unapprove" || $(this).val()=="pause" || $(this).val()=="unpause" || $(this).val()=="expire" || $(this).val()=="running"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);}						  if($(this).val()=="banner_size"){$(".searchfor_all").hide();$(".searchfor_size").show(500);}else{$(".searchfor_size").hide();$(".searchfor_all").show(500);}'
					));
					?>
					</label>
				</div>
			</div>
        </span>
	    </div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=="approve" || $searchby=="unapprove" || $searchby=="pause" || $searchby=="unpause" || $searchby=='expire' || $searchby=='running'){ echo 'style="display:none"';} ?>>
			<span>Search For :</span>
			<span class='searchfor_all' style='display:<?php if($searchby=="banner_size"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?></span>
			<span class='searchfor_size' style='display:<?php if($searchby!="banner_size"){ echo "none";} ?>'>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
						<?php 
						echo $this->Form->input('banner_size', array(
							  'type' => 'select',
							  'options' => array('125x125'=>'125x125', '468x60'=>'468x60', '728x90'=>'728x90'),
							  'selected' => $searchfor,
							  'class'=>'',
							  'label' => false,
							  'style' => ''
						));
						?>
						</label>
					</div>
				</div>	
			</span>
		</div>
   </div>
   <div class="from-box">
        <div class="fromboxmain width480">
				<span>From :</span>
				<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
		</div>
        <div class="fromboxmain">
				<span>To :</span>
				<span><?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
				<span class="padding-left">
					<?php echo $this->Js->submit('', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#memberdetailpage',
						'class'=>'searchbtn',
						'controller'=>'member',
						'action'=>'mdptcad/'.$member_id,
						'url'=> array('controller' => 'member', 'action' => 'mdptcad/'.$member_id)
					));?>			
				</span>
        </div>
    </div>
</div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberdetailpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'mdptcad/'.$member_id)
	));
	$currentpagenumber=$this->params['paging']['Ptcbanner']['page'];
	?>
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Ptcbanner',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'mdptcad')));?>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'member', "action"=>"mdptcad/".$member_id."/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div><?php echo 'Plan';?></div>
                <div>
					<?php 
					echo $this->Js->link('Banner Size', array('controller'=>'member', "action"=>"mdptcad/".$member_id."/0/banner_size/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Banner Size'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Banner', array('controller'=>'member', "action"=>"mdptcad/".$member_id."/0/purchasedate/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Banner'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Displayed', array('controller'=>'member', "action"=>"mdptcad/".$member_id."/0/display_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Displayed'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Clicked', array('controller'=>'member', "action"=>"mdptcad/".$member_id."/0/click_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Clicked'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Likes', array('controller'=>'member', "action"=>"mdptcad/".$member_id."/0/like_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Likes'
					));?>
				</div>
            </div>
			<?php foreach ($ptcbannerdata as $ptcbanner): ?>
				<div class="tablegridrow">
					<div><?php echo $ptcbanner['Ptcbanner']['id']; ?></div>
					<div>
						<a href="#" class="vtip" title="<b>Plan Name : </b><?php echo $ptcbanner['Ptcplan']['plan_name']; ?>"><?php echo $ptcbanner['Ptcbanner']['ptc_id'];?></a>
					</div>
					<div><?php echo ($ptcbanner['Ptcbanner']['banner_size']=="") ? "N/A" : $ptcbanner['Ptcbanner']['banner_size'] ; ?></div>
					<div>
						<br/>
						<b>Purchase Date : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $ptcbanner['Ptcbanner']['purchasedate']); ?>
						<br/>
						<?php if($ptcbanner['Ptcbanner']['banner_size'] != "") { ?>
						<a href="<?php echo $ptcbanner['Ptcbanner']['target_url'];?>" target="_blank" class="vtip" title="<b>Title : </b><?php echo $ptcbanner['Ptcbanner']['banner_title']; ?>">
							<?php
								if($ptcbanner['Ptcbanner']['banner_url'] != '')
								{
									if($ptcbanner['Ptcbanner']['banner_size']=='125x125'){$height="50px";$maxwidth='';}
									elseif($ptcbanner['Ptcbanner']['banner_size']=='468x60'){$height="90%";$maxwidth="468px";}
									else{$height="100%";$maxwidth="728px";}
									
									echo '<img src="'.$ptcbanner['Ptcbanner']['banner_url'].'" alt="'.$ptcbanner['Ptcbanner']['banner_title'].'" width="'.$height.'" style="max-width:'.$maxwidth.';" />';
								}
								else
								{
									echo $ptcbanner['Ptcbanner']['banner_title'];
								}
							?>
						</a>
						<?php } else { ?>
						<a href="<?php echo $ptcbanner['Ptcbanner']['target_url'];?>" target="_blank" class="vtip" title="<b>Title : </b><?php echo $ptcbanner['Ptcbanner']['banner_title']; ?>"><b>Title : </b><?php echo $ptcbanner['Ptcbanner']['banner_title']; ?></a>
						<?php } ?>
						<br/>
						<b>Display Start Date : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $ptcbanner['Ptcbanner']['start_date']); ?>
						<br/><br/>
					</div>
					<div><?php echo $ptcbanner['Ptcbanner']['display_counter']; ?></div>
					<div>
						<?php if($ptcbanner['Ptcbanner']['display_area']!='all'){?>
							<a href="<?php echo $ADMINURL;?>/ptc/geo/<?php echo $ptcbanner['Ptcbanner']['id'];?>" class="iframeclass">
								<span class="vtip" title="Click here to view country wise clicks"><?php echo $ptcbanner['Ptcbanner']['click_counter'];?></span>
							</a>
							<div style="display:none;"><div id="inlinebox<?php echo $ptcbanner['Ptcbanner']['id'];?>"><?php echo $ptcbanner['Ptcbanner']['click_counter'];?></div></div>
						<?php }else{
							echo $ptcbanner['Ptcbanner']['click_counter'];
						}?>
					</div>
					<div>
						<a href="#" class="vtip" title="<b>Members : </b><?php echo str_replace(",", ", ", $ptcbanner['Ptcbanner']['like_members']); ?>"><?php echo $ptcbanner['Ptcbanner']['like_counter'];?></a>
					</div>
			</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($ptcbannerdata)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Ptcbanner']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Ptcbanner',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'mdptcad/'.$member_id.'/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#memberdetailpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'member',
			  'action'=>'mdptcad/'.$member_id.'/rpp',
			  'url'   => array('controller' => 'member', 'action' => 'mdptcad/'.$member_id.'/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>