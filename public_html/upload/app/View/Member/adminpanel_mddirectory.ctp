<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 11-12-2014
  * Last Modified: 11-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Biz_Directory#Plan_Member" target="_blank">Help</a></div>
<div class="tab-innar nomargin">
	<ul>
		<li>
			<?php echo $this->Js->link("Modules", array('controller'=>'member', "action"=>"mdposition/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Advertisement Plan", array('controller'=>'member', "action"=>"mdadvertisementplan/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'active'
			));?>
		</li>
	</ul>
</div>
<div class="positions-menu-drowp">
	<ul>
		<li>
			<?php echo $this->Js->link('Banner Ad', array('controller'=>'member', "action"=>"mdadvertisementplan/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('Text Ad', array('controller'=>'member', "action"=>"mdtextad/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('Solo Ad', array('controller'=>'member', "action"=>"mdsoload/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('Login Ad', array('controller'=>'member', "action"=>"mdloginad/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('PPC', array('controller'=>'member', "action"=>"mdppcad/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('PTC', array('controller'=>'member', "action"=>"mdptcad/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('Biz Directory', array('controller'=>'member', "action"=>"mddirectory/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>'act'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link('Surf Free', array('controller'=>'member', "action"=>"mdsurffree/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>''
			));?>
		</li>
	</ul>
</div>
<div class="clear-both"></div>
<div class="height21"></div>
<div id="UpdateMessage"></div>
<?php if($IsAdminAccess){?>
<script type="text/javascript">
	<?php if($searchby=='category'){ ?>
	generatecombo("data[Directorymember][category]","Directorycategory","id","category","<?php echo $searchfor; ?>",".searchforcategory","<?php echo $ADMINURL;?>","");
	<?php } ?>
</script>
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Directorymember',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'mddirectory')));?>
	
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
					<?php 
					echo $this->Form->input('searchby', array(
						  'type' => 'select',
						  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'plan_id'=>'Plan Id', 'member_id'=>'Member Id', 'title'=>'Title',  'display_counter'=>'Displayed', 'click_counter'=>'Clicked', 'category'=>'Category', 'running'=>'Running Biz Directory', 'expire'=>'Expire Biz Directory', 'active'=>'Active Biz Directory', 'inactive'=>'Inactive Biz Directory', 'unpause'=>'Unpause Biz Directory', 'pause'=>'Pause Biz Directory'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => '',
						  'onchange'=>'if($(this).val()=="unpause" || $(this).val()=="pause" || $(this).val()=="inactive" || $(this).val()=="active" || $(this).val()=="expire" || $(this).val()=="running"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);} if($(this).val()=="category"){ generatecombo("data[Directorymember][category]","Directorycategory","id","category","<?php echo $searchfor; ?>",".searchforcategory","'.$ADMINURL.'","");$(".searchforall").hide(); $(".searchforcategory").show(500);}else{ $(".searchforcategory").hide(); $(".searchforall").show(500);}'
					));
					?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=="unpause" || $searchby=="pause" || $searchby=="inactive" || $searchby=="active" || $searchby=='expire' || $searchby=='running'){ echo 'style="display:none"';} ?>>
			<span>Search For :</span>
			<span class="searchforall" <?php if($searchby=="category"){ echo "style='display:none'";}?>><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class="searchforcategory" <?php if($searchby!="category"){ echo "style='display:none'";}?>></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#memberdetailpage',
				'class'=>'searchbtn',
				'controller'=>'member',
				'action'=>'mddirectory/'.$member_id,
				'url'=> array('controller' => 'member', 'action' => 'mddirectory/'.$member_id)
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->


	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberdetailpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'mddirectory/'.$member_id)
	));
	$currentpagenumber=$this->params['paging']['Directorymember']['page'];
	?>

<div id="gride-bg">
    <div class="Xpadding10">
	<?php echo $this->Form->create('Directorymember',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'planmember')));?>		
    <div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="clear-both"></div>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'member', "action"=>"mddirectory/".$member_id."/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Plan Id', array('controller'=>'member', "action"=>"mddirectory/".$member_id."/0/plan_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Category Id', array('controller'=>'member', "action"=>"mddirectory/".$member_id."/0/category/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Category Id'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Purchase Date', array('controller'=>'member', "action"=>"mddirectory/".$member_id."/0/create_date/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Purchase Date'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Displayed', array('controller'=>'member', "action"=>"mddirectory/".$member_id."/0/display_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Displayed'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Clicked', array('controller'=>'member', "action"=>"mddirectory/".$member_id."/0/click_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Clicked'
					));?>
				</div>
				<div></div>
			</div>
			<?php foreach ($directorymembers as $directorymember): ?>
				<div class="tablegridrow">
					<div><?php echo $directorymember['Directorymember']['id']; ?></div>
					<div>
						<a href="#" class="vtip" title="<b>Plan Name : </b><?php echo $directorymember['Directoryplan']['plan_name']; ?>"> <?php echo $directorymember['Directorymember']['plan_id']; ?></a>
				    </div>
					<div><?php echo $directorymember['Directorymember']['category']; ?></div>
					<div class="textcenter" style="width: 40%;">
						<p><b>Purchase Date : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $directorymember['Directorymember']['create_date']); ?></p>
						<a href="<?php echo $directorymember['Directorymember']['site_url'];?>" target="_blank" class="vtip" title="<b>Title : </b><?php echo $directorymember['Directorymember']['title']; ?>">
							<?php
								if($directorymember['Directoryplan']['banner_size']=='125x125'){$width="50px";$maxwidth='125px';}
								elseif($directorymember['Directoryplan']['banner_size']=='468x60'){$width="90%";$maxwidth="468px";}
								else{$width="98%";$maxwidth="728px";}
								echo '<img src="'.$directorymember['Directorymember']['banner_url'].'" alt="'.$directorymember['Directorymember']['title'].'" width="'.$width.'" style="max-width:'.$maxwidth.';" />';
							?>
						</a>
						<p><b>Approved Date : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $directorymember['Directorymember']['approve_date']); ?></p>
					</div>
					<div><?php echo $directorymember['Directorymember']['display_counter']; ?></div>
					<div><?php echo $directorymember['Directorymember']['click_counter']; ?></div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($directorymembers)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Directorymember']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Directorymember',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'mddirectory/'.$member_id.'/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#memberdetailpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'member',
			  'action'=>'planmember/'.$member_id.'/rpp',
			  'url'   => array('controller' => 'member', 'action' => 'mddirectory/'.$member_id.'/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>