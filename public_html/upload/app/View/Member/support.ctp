<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 25-09-2014
  *********************************************************************/
?>
<?php if(!$ajax){
	if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }
} ?>	
<?php if($SITECONFIG["mticket_chk"]==1){ ?>
<?php if(!$ajax){?>
<div id="memberpage">
<?php }?>

<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Member Support');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	
	<?php // Search box code starts here ?>
	<div class="floatleft searchlabel"><?php echo __("Advanced Search");?></div>
	<div class="floatright"><a href="javascript:void(0)" onclick="togglebox('.searchlabel', this,'<?php echo __("[+] Show Search Box");?>','<?php echo __("[-] Hide Search Box");?>');"><?php echo __("[-] Hide Search Box");?></a></div>
	<div class="clear-both"></div>
	<div class="advsearchbox searchlabel">
	<?php echo $this->Form->create('Member_ticket',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'member','action'=>'support')));?>
		<div class="searchboxrow" style="margin-bottom: 0px;">
			<span class="searchboxcol1"><?php echo __("Search By");?>  :&nbsp;</span>
			
			<div class="select-dropdown smallsearch">
				<label>
					<?php echo $this->Form->input('searchby', array(
						'type' => 'select',
						'options' => array('all'=>__('All Tickets'), '1'=>__('Open Tickets'), '0'=>__('Close Tickets')),
						'selected' => $searchby,
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
						));
					?>
				</label>
			</div>
			
			<div class="textcenter margint5">
				<?php echo $this->Js->submit(__('Update Results'), array(
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#memberpage',
					'class'=>'button floatnone',
					'div'=>false,
					'controller'=>'member',
					'action'=>'support',
					'url'   => array('controller' => 'member', 'action' => 'support')
				  ));?>
			</div>
		</div>
	<?php echo $this->Form->end();?>
	</div>
	<?php // Search box code ends here ?>
	
<div class="height10"></div>

<?php echo $this->Javascript->link('allpage');?>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'support')
	));
	$currentpagenumber=$this->params['paging']['Member_ticket']['page']; ?>
		
		<?php // Support tickets table starts here ?>
		<div class="padding-left-serchtabal">
			<?php echo $this->Js->link(__("Add New"), array('controller'=>'member', "action"=>"supportadd"), array(
				'update'=>'#memberpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button'
			));?>
		</div>
		<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			<div class="divtable textcenter">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam">
							<?php 
							if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
							echo $this->Js->link(__('Category'), array('controller'=>'member', "action"=>"support/0/category/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Category')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Subject'), array('controller'=>'member', "action"=>"support/0/subject/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Subject')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Date created'), array('controller'=>'member', "action"=>"support/0/dt_create/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Date created')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Date modified'), array('controller'=>'member', "action"=>"support/0/dt_lastupdate/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Date modified')
							));?>
						</div>
						<div class="divth textcenter vam"><?php echo __("Action");?></div>
					</div>
				</div>
				<div class="divtbody">
					<?php $i=1;
					foreach ($member_ticketdata as $member_ticket):
					if($i%2==0){$class='gray-color';}else{$class='white-color';}
					if($member_ticket['Member_ticket_replie']['replyer_id']!=$member_ticket['Member_ticket']['m_id']){ $class .= ' highlight_tr';}?>
						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam">
								<?php echo $this->Js->link(ucwords($member_ticket['Member_ticket']['category']), array('controller'=>'member', "action"=>"supportedit/".$member_ticket['Member_ticket']['mt_id']), array(
									'update'=>'#memberpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>__('Edit ticket')
								));?>
							</div>	
							<div class="divtd textcenter vam">
								<?php echo $this->Js->link($member_ticket['Member_ticket']['subject'], array('controller'=>'member', "action"=>"supportedit/".$member_ticket['Member_ticket']['mt_id']), array(
									'update'=>'#memberpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>__('Edit ticket')
								));?>
							</div>
							<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $member_ticket['Member_ticket']['dt_create']); ?></div>
							<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $member_ticket['Member_ticket']['dt_lastupdate']); ?></div>
							<div class="divtd textcenter vam">
								
								<div class="actionmenu">
									<div class="btn-group">
									  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										  Action <span class="caret"></span>
									  </button>
									  <ul class="dropdown-menu" role="menu">
										  <li>
										      <?php 
											if($member_ticket['Member_ticket']['status']==0){
												$statusaction='1';
												$statusicon='red-icon.png';
												$statustext='Open ticket';
											}else{
												$statusaction='0';
												$statusicon='blue-icon.png';
												$statustext='Close ticket';}
											echo $this->Js->link($this->html->image($statusicon, array('alt'=>__($statustext)))." ".__($statustext), array('controller'=>'member', "action"=>"supportstatus/".$statusaction."/".$member_ticket['Member_ticket']['mt_id']."/".$currentpagenumber), array(
												'update'=>'#memberpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));
											?>
										</li>
										 <li>
											<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit ticket'))).' '.__('Edit ticket'), array('controller'=>'member', "action"=>"supportedit/".$member_ticket['Member_ticket']['mt_id']), array(
												'update'=>'#memberpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));?>
										 </li>
									   </ul>
									</div>
								</div>
									
								
							
								
							</div>
						</div>

					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($member_ticketdata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // Support tickets table ends here ?>

	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Member_ticket']['count']>$pagerecord)
	{?>	
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Member_ticket',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'support/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#memberpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'member',
						  'action'=>'support/rpp',
						  'url'   => array('controller' => 'member', 'action' => 'support/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
            </div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		
	</div>
<?php if(!$ajax){?>	
</div>
<?php } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>

<?php }else{?>
<div class="comisson-bg">
	<div class="float-left"><?php echo $this->Html->image('commison-left.jpg', array('alt' => __('Member Support')))?></div>
	<div class="text-ads-title-text"><?php echo __('Member Support');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php echo __("Admin has stopped new tickets currently. Please try later.");?>
</div>
<?php }?>