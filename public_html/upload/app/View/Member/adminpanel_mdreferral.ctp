<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List#Referrals" target="_blank">Help</a></div>
<div class="tab-innar nomargin">
	<ul>
		<li>
			<?php echo $this->Js->link("Referrals", array('controller'=>'member', "action"=>"mdreferral/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'active'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Referral Tracking", array('controller'=>'member', "action"=>"mdreferraltracking/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>''
			));?>
		</li>
	</ul>
</div>

<div class="clear-both"></div>
<div class="height21"></div>
<div id="UpdateMessage"></div>

<div class="serchmainbox">
	<div class="serchgreybox"><?php echo "Search Option";?></div>
	
	  <?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'mdreferral')));?>
	  
	  <div class="from-box">
		<div class="fromboxmain">
			<span><?php echo "Search By";?> :</span>
			<span>                     
			    <div class="searchoptionselect">
				<div class="select-main">
				    <label>
					<?php 
					  echo $this->Form->input('searchby', array(
						  'type' => 'select',
						  'options' => array('all'=>'Select Parameter', 'inactive'=>'Inactive Members', 'active'=>'Active Members', 'unpaid'=>'Unpaid Members', 'paid'=>'Paid Members', 'user_name'=>'Username', 'email'=>'Email', 'f_name'=>'First Name', 'l_name'=>'Last Name'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => '',
						  'onchange'=>'if($(this).val()=="inactive" || $(this).val()=="active" || $(this).val()=="unpaid" || $(this).val()=="paid"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);}'
					  ));
					?>
				    </label>
				</div>
			    </div>
			 </span>
		</div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=="inactive" || $searchby=="active" || $searchby=="unpaid" || $searchby=="paid"){ echo 'style="display:none"';} ?>>
		    <span>Search For :</span>
		    <span><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?></span>
		    
		</div>
	</div>
	<div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
		</div>
		<div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#memberdetailpage',
					'class'=>'searchbtn',
					'controller'=>'member',
					'action'=>'mdreferral',
					'url'   => array('controller' => 'member', 'action' => 'mdreferral/'.$member_id)
				));?>
			</span>
		</div>
	</div>
	  
	  <?php echo $this->Form->end();?>
</div>	
	
<div id="gride-bg">

	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberdetailpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'mdreferral/'.$member_id)
	));
	$currentpagenumber=$this->params['paging']['Member']['page'];
	?>
	<div class="margin10">
		<div class="height10"></div>
		<div class="display-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div></div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('M. Id', array('controller'=>'member', "action"=>"mdreferral/".$member_id."/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Reg.Date', array('controller'=>'member', "action"=>"mdreferral/".$member_id."/0/0/reg_dt/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Registration Date'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Username', array('controller'=>'member', "action"=>"mdreferral/".$member_id."/0/0/user_name/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Username'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Full Name', array('controller'=>'member', "action"=>"mdreferral/".$member_id."/0/0/f_name/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By First Name'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Email', array('controller'=>'member', "action"=>"mdreferral/".$member_id."/0/0/email/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Email'
					));?>
				</div>
				<div>
					<?php echo $this->Js->link('Tot Ref.', array('controller'=>'member', "action"=>"mdreferral/".$member_id."/0/0/total_referrer/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Total Referrals'
					));?>
				</div>
                </div>
		
		<?php
			$i = 0;
			foreach ($members as $member):
				$class = 'class="blue-color"';if ($i++ % 2 == 0){$class = 'class="white-color"';}
				?>
				
				<div class="tablegridrow">
					<div><?php if($member['Member']['member_id']!=0){ echo $this->Js->link($member['Member']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top"), array(
							'update'=>'#memberpageTK',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						)); }else{echo $member['Member']['member_id'];}?></div>
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $member['Member']['reg_dt']);?></div>
					<div><?php if($member['Member']['member_id']!=0){ echo $this->Js->link($member['Member']['user_name'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top"), array(
							'update'=>'#memberpageTK',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						)); }else{echo $member['Member']['user_name'];}?></div>
					<div><?php echo $member['Member']['f_name']."<br/>".$member['Member']['l_name'];?></div>
					<div><?php echo $member['Member']['email']; ?></div>
					<div><?php echo $member['Member']['total_referrer']; ?></div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($members)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
	
	
	
	<div class="margin10">
	<?php 
	if($this->params['paging']['Member']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'mdreferral/0/rpp')));?>
		
		<div class="resultperpage">
			<label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#memberdetailpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'Member',
			  'action'=>'mdreferral/0/rpp',
			  'url'   => array('controller' => 'member', 'action' => 'mdreferral/'.$member_id.'/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	</div>
	<div class="height7"></div>
</div>
</div>
<div class="height10"></div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>