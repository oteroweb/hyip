<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>		
<?php if($IsAdminAccess){?>

<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List#Balance_Updates" target="_blank">Help</a></div>
<div class="clear-both"></div>
<div class="height21"></div>
<div id="UpdateMessage"></div>

<div class="serchmainbox">
	<div class="serchgreybox"><?php echo "Search Option";?></div>
	
	  <?php echo $this->Form->create('Cash_by_admin',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'cash_by_admin')));?>
	  
	  <div class="from-box">
		<div class="fromboxmain">
			<span><?php echo "Search By";?> :</span>
			<span>                     
			    <div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php if($SITECONFIG["balance_type"]==1)
							$options=array('all'=>'Select Parameter', 'added_type'=>'Type', 'added_only'=>'Added Only', 'deducted_only'=>'Deducted Only');
						else
							$options=array('all'=>'Select Parameter', 'added_type'=>'Type', 'processor'=>'Payment Processor', 'added_only'=>'Added Only', 'deducted_only'=>'Deducted Only');
						
						echo $this->Form->input('searchby', array(
						      'type' => 'select',
						      'options' => $options,
						      'selected' => $searchby,
						      'class'=>'',
						      'label' => false,
						      'style' => '','onchange'=>'if($(this).val()=="added_only" || $(this).val()=="deducted_only"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);} if($(this).val()=="processor"){generatecombo("data[Cash_by_admin][proc_name]","Processor","proc_name","proc_name","'.$searchfor.'","#searchforcombo","'.$ADMINURL.'","");$("#searchall").hide();$("#searchforcombo").show(500);$("#searchfortype").hide();}else if($(this).val()=="added_type"){$("#searchforcombo").hide();$("#searchall").hide();$("#searchfortype").show(500);} else{$("#searchforcombo").hide();$("#searchall").show(500);$("#searchfortype").hide();}'
						));
						?>
					</label>
				</div>
			    </div>
			 </span>
		</div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=="added_only" || $searchby=="deducted_only"){ echo 'style="display:none"';} ?>>
		    <span>Search For :</span>
		    <span id="searchall" style='display:<?php if($searchby=="processor" || $searchby=="added_type"){ echo "none";} ?>'>
			<?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?>
		    </span>
		    <span id='searchfortype' style='display:<?php if($searchby!="added_type"){ echo "none";} ?>'>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
						<?php 
							echo $this->Form->input('added_type', array(
								'type' => 'select',
								'options' => array('cash'=>'Cash', 'repurchase'=>'Purchase', 'earning'=>'Earning', 'commission'=>'Commission'),
								'class'=>'',
								'selected' => $searchfor,
								'label' => false,
								'div' => false,
								'style' => ''
							));
						?>
						</label>
					</div>
				</div>
		    </span>
		    <span  id="searchforcombo" style='display:<?php if($searchby!="processor"){ echo "none";} ?>'></span>
		</div>
	</div>
	<div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
		</div>
		<div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#memberdetailpage',
					'class'=>'searchbtn',
					'controller'=>'member',
					'action'=>'cash_by_admin',
					'url'=> array('controller' => 'member', 'action' => 'cash_by_admin/'.$member_id)
				));?>
			</span>
		</div>
	</div>
	  <?php echo $this->Form->end();?>
</div>	
	
<div id="gride-bg">
<?php if($searchby=='processor'){ ?>
<script>
	generatecombo("data[Cash_by_admin][proc_name]","Processor","proc_name","proc_name","<?php echo $searchfor; ?>","#searchforcombo","<?php echo $ADMINURL;?>","");
</script>
<?php } ?>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberdetailpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'cash_by_admin/'.$member_id)
	));
	$currentpagenumber=$this->params['paging']['Cash_by_admin']['page'];
	?>
	<div class="margin10">
	<div class="height10"></div>
	<div class="display-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="clear-both"></div> </div>
	
	
	<div class="tablegrid">
		<div class="tablegridheader">
			<div>
				<?php 
				if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
				echo $this->Js->link('Id', array('controller'=>'member', "action"=>"cash_by_admin/".$member_id."/0/0/id/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Id'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Date', array('controller'=>'member', "action"=>"cash_by_admin/".$member_id."/0/0/added_date/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Date'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link("Amount", array('controller'=>'member', "action"=>"cash_by_admin/".$member_id."/0/0/added_balance/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>"Sort By Amount"
				));?>
			</div>
			<?php if($SITECONFIG["balance_type"]==2){ ?>
			    <div>
				<?php echo $this->Js->link('Payment Processor', array('controller'=>'member', "action"=>"cash_by_admin/".$member_id."/0/0/processor/".$sorttype."/".$currentpagenumber), array(
				    'update'=>'#memberdetailpage',
				    'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				    'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				    'escape'=>false,
				    'class'=>'vtip',
				    'title'=>'Sort By Payment Processor'
				));?>
			    </div>
			<?php } ?>
			<div>
				<?php echo $this->Js->link('Balance Type', array('controller'=>'member', "action"=>"cash_by_admin/".$member_id."/0/0/added_type/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Balance Type'
				));?>
			</div>
			<div><?php echo 'Description';?></div>
                </div>
		
		<?php
			$i = 0;
			foreach ($cash_by_admins as $cash_by_admin):
				$class = 'class="blue-color"';if ($i++ % 2 == 0){$class = 'class="white-color"';}
				if($cash_by_admin['Cash_by_admin']['description']=='') $cash_by_admin['Cash_by_admin']['description']='N/A';
				?>
				
				<div class="tablegridrow">
					<div><?php echo $cash_by_admin['Cash_by_admin']['id']; ?></div>
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $cash_by_admin['Cash_by_admin']['added_date']); ?></div>
					<div>$<?php echo round($cash_by_admin['Cash_by_admin']['added_balance'],4); ?></div>
					<?php if($SITECONFIG["balance_type"]==2){ ?>
					<div><?php echo $cash_by_admin['Cash_by_admin']['processor']; ?></div>
					<?php } ?>
					<div><?php if($cash_by_admin['Cash_by_admin']['added_type']=='repurchase') echo 'Purchase'; else echo ucwords($cash_by_admin['Cash_by_admin']['added_type']); ?></div>
					<div><?php echo $this->html->image('information.png', array('alt'=>'Description', 'class'=>'vtip', 'title'=>nl2br(stripslashes($cash_by_admin['Cash_by_admin']['description']))));?></div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($cash_by_admins)==0){ echo '<div class="norecordfound">No records available</div>';}?>
	
	<div class="margin10">	
		<?php
		if($this->params['paging']['Cash_by_admin']['count']>$this->Session->read('pagerecord'))
		{?>
		<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
		<div class="floatleft margintop19">
			<?php echo $this->Form->create('Cash_by_admin',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'cash_by_admin/rpp')));?>
			<div class="resultperpage">
				<label>
					<?php 
					echo $this->Form->input('resultperpage', array(
					  'type' => 'select',
					  'options' => $resultperpage,
					  'selected' => $this->Session->read('pagerecord'),
					  'class'=>'',
					  'label' => false,
					  'div'=>false,
					  'style' => '',
					  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
					));
					?>
				</label>
			</div>
			<span id="resultperpageapply" style="display:none;">
				<?php echo $this->Js->submit('Apply', array(
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#memberdetailpage',
				  'class'=>'',
				  'div'=>false,
				  'controller'=>'member',
				  'action'=>'cash_by_admin/rpp',
				  'url'   => array('controller' => 'member', 'action' => 'cash_by_admin/'.$member_id.'/rpp')
				));?>
			</span>
			<?php echo $this->Form->end();?>
		</div>
		<?php }?>
		<div class="floatright">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
		</ul>
		</div>
		<div class="clear-both"></div>
	</div>
	<div class="height7"></div>
</div>
</div>
<div class="height10"></div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>