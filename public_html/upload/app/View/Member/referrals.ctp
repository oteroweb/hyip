<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 25-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>

<?php if(!$ajax) { ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="memberpage">
<?php } ?>
<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>

<?php // Top Menu starts here ?>
<div class="comisson-bg">
	<div class="commison-menu">
		<ul>
			<li>
				<?php
					echo $this->Js->link(__('My Referrals'), array('controller'=>'member', "action"=>"referrals"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip act',
						'title'=>__('My Referrals')
					));
				?>
			</li>
			<li>
				<?php
					echo $this->Js->link(__('Referral Tracking'), array('controller'=>'member', "action"=>"referraltracking"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('Referral Tracking')
					));
				?>
			</li>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Top Menu ends here ?>

	<?php
	$actionlink='referrals';
	if(isset($referrer) && $referrer!='0')
		$actionlink.='/'.$referrer;
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>$actionlink)
	));
	$currentpagenumber=$this->params['paging']['Member']['page'];
	?>
	<div class="main-box-eran">
		
		<?php // My Referrals table starts here ?>
		<div class="padding-left-serchtabal">
			<?php if($referrer!='0' || $referrer!=''){
			echo $this->Js->link(__("Back"), array('controller'=>'member', "action"=>"referrals"), array(
				'update'=>'#memberpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button ml10'
			));
			echo '<span class="ml10 vab"><b>'.__('Member Id').' : '.$referrer.'</b></span>';
		}?>
		</div>
		<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			<div class="divtable refferal_table">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<?php if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
						if($SITECONFIG["reflinkiduser"]==0){ ?>
						<div class="divth textcenter vam">
							<?php 
							echo $this->Js->link(__('Member Id'), array('controller'=>'member', "action"=>"referrals/".$referrer."/0/member_id/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Member Id')
							));?>
						</div>
						<?php } ?>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Full Name'), array('controller'=>'member', "action"=>"referrals/".$referrer."/0/f_name/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('First Name')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Username'), array('controller'=>'member', "action"=>"referrals/".$referrer."/0/user_name/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Username')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Email'), array('controller'=>'member', "action"=>"referrals/".$referrer."/0/email/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Email')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Total Referrals'), array('controller'=>'member', "action"=>"referrals/".$referrer."/0/total_referrer/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Total Referrals')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Status'), array('controller'=>'member', "action"=>"referrals/".$referrer."/0/ispaid/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Status')
							));?>
						</div>
					</div>
				</div>
				<div class="divtbody">
					<?php $i=1;
					foreach ($referraldata as $referral):
					if($i%2==0){$class='gray-color';}else{$class='white-color';}?>

						<div class="divtr <?php echo $class;?>">
							<?php if($SITECONFIG["reflinkiduser"]==0){ ?>
							<div class="divtd textcenter vam"><?php echo $referral['Member']['member_id'];?></div>
							<?php } ?>
							<div class="divtd textcenter vam"><?php echo $referral['Member']['f_name']."&nbsp;".$referral['Member']['l_name'];?></div>
							<div class="divtd textcenter vam"><?php echo $referral['Member']['user_name'];?></div>
							<div class="divtd textcenter vam"><?php echo $referral['Member']['email'];?></div>
							<div class="divtd textcenter vam">
								<?php 
								if($referral['Member']['total_referrer']>0)
								{
								if($SITECONFIG["reflinkiduser"]==1){$refid=$referral['Member']['user_name'];}else{$refid=$referral['Member']['member_id'];}
								echo $this->Js->link($referral['Member']['total_referrer'], array('controller'=>'member', "action"=>"referrals/".$refid), array(
									'update'=>'#memberpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>__('View referrals of').' #'.$refid
								));
								}
								else
								{
									echo $referral['Member']['total_referrer'];
								}
								?>
							</div>
							<div class="divtd textcenter vam">
								<?php echo ($referral['Member']['ispaid']==1) ? __("Paid") : __("Free");?>
							</div>
						</div>
					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($referraldata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // My Referrals table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Member']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member', 'action'=>'referrals/0/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#memberpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'member',
						  'action'=>'referrals/0/rpp',
						  'url'   => array('controller' => 'member', 'action' => 'referrals/0/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
            </div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
	</div>
<?php if(!$ajax) { ?>		
</div>
<?php } ?>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>