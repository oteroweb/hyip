<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 19-11-2014
  *********************************************************************/
?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="memberpage">
<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>

<?php if($themesubmenuaccess){ ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Delete Your Account');?></div>
	<div class="clear-both"></div>
</div>	
<?php if($secondary==1){ ?>
<div class="main-box-eran">
	<div class="form-box">
		
		<?php // Secondary Password form starts here ?>
		<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off')); ?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Secondary Password");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('secondarypassword' ,array('type'=>'password',"div"=>false, "class"=>"login-from-box-1 keyboardInput", 'label'=>false));?>
			</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Current")." ".__("Password");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('current_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput", 'div'=>false, 'label'=>false));?>
			</div>
			<?php if($MemberProfileCaptcha){ ?>
				<div class="form-row captchrow">
				  <div class="form-col-1"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
					  <?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formcapthcatextbox", 'div'=>false, 'label'=>false));?>
					  <span><?php echo __('Code')?> :</span>
					  <?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'MemberProfileCaptcha','vspace'=>2, 'align'=>'absmiddle', 'width'=>'118', 'height'=>'44')); ?> 
					  <a href="javascript:void(0);" onclick="javascript:document.images.MemberProfileCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "width"=>"", "height"=>"", "align"=>"absmiddle"));?></a>
					  <div class="clear-both"></div>
				</div>
			<?php }?>
		
		<div class="formbutton">
			<?php echo $this->Js->submit(__('Submit'), array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'div'=>false,
			  'class'=>'button',
			  'controller'=>'Member',
			  'action'=>'delete'
			));?>
		</div>
		
		<div class="clear-both"></div>
		<?php echo $this->Form->end();?>
		<?php // Secondary Password form ends here ?>
		
	</div>	
<div class="clear-both"></div>
</div>
<?php }elseif($secondary==2){ ?>
<div class="main-box-eran">
	<div class="form-box">
		
		<?php // Secondary Password form starts here ?>
		<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off')); ?>
		<div class="form-row">
			<div class="form-col-1"><?php echo __("Verification Code");?> : <span class="required">*</span></div>
			<?php echo $this->Form->input('code' ,array('type'=>'text',"div"=>false, "class"=>"login-from-box-1", 'label'=>false));?>
		</div>
		<div class="form-row">
				<div class="form-col-1"><?php echo __("Current")." ".__("Password");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('current_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput", 'div'=>false, 'label'=>false));?>
			</div>
		<div class="formbutton">
			<?php echo $this->Js->submit(__('Submit'), array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'div'=>false,
			  'class'=>'button',
			  'controller'=>'Member',
			  'action'=>'delete'
			));?>
		</div>
		
		<div class="clear-both"></div>
		<?php echo $this->Form->end();?>
		<?php // Secondary Password form ends here ?>
		
	</div>	
<div class="clear-both"></div>
</div>
<?php } elseif($secondary==3){ ?>
<div class="main-box-eran">
	<div class="form-box">
		
		<?php // Secondary Password form starts here ?>
		<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off')); ?>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Current")." ".__("Password");?> : <span class="required">*</span></div>
				<?php echo $this->Form->input('current_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput", 'div'=>false, 'label'=>false));?>
			</div>
			<?php if($MemberProfileCaptcha){ ?>
				<div class="form-row captchrow">
				  <div class="form-col-1"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
					  <?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formcapthcatextbox", 'div'=>false, 'label'=>false));?>
					  <span><?php echo __('Code')?> :</span>
					  <?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'MemberProfileCaptcha','vspace'=>2, 'align'=>'absmiddle', 'width'=>'118', 'height'=>'44')); ?> 
					  <a href="javascript:void(0);" onclick="javascript:document.images.MemberProfileCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "width"=>"", "height"=>"", "align"=>"absmiddle"));?></a>
					  <div class="clear-both"></div>
				</div>
			<?php }?>
		
		<div class="formbutton">
			<?php echo $this->Js->submit(__('Submit'), array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'div'=>false,
			  'class'=>'button',
			  'controller'=>'Member',
			  'action'=>'delete'
			));?>
		</div>
		
		<div class="clear-both"></div>
		<?php echo $this->Form->end();?>
		<?php // Secondary Password form ends here ?>
		
	</div>	
<div class="clear-both"></div>
</div>
<?php }?>
	
<?php } else { echo __('This page is disabled by administrator'); }?>	
</div>
 <div class="clear-both"></div>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>