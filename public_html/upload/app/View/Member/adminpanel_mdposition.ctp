<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 10-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List#Positions" target="_blank">Help</a></div>
<div class="tab-innar nomargin">
	<ul>
		<li>
			<?php echo $this->Js->link("Modules", array('controller'=>'member', "action"=>"mdposition/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'active'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Advertisement Plan", array('controller'=>'member', "action"=>"mdadvertisementplan/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>''
			));?>
		</li>
	</ul>
</div>
<div class="positions-menu-drowp">
	<ul>
		<?php foreach($ActivePositionModules as $key=>$value){ if($key==$PositionModel){$class="act";}else{$class="";}?>
		<li>
			<?php echo $this->Js->link($value, array('controller'=>'member', "action"=>"mdposition/".$member_id."/".$key), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>$class
			));?>
		</li>
		<?php }?>
	</ul>
</div>
<div class="clear-both"></div>
<div class="height21"></div>
<div id="memberpagecont">
<div id="UpdateMessage"></div>

	
	
<div class="serchmainbox">
	<div class="serchgreybox"><?php echo "Search Option";?></div>
	  <?php echo $this->Form->create('Position',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'mdposition/'.$member_id.'/'.$PositionModel)));?>
		<div class="from-box">
			<div class="fromboxmain width480">
				<span>From :</span>
				<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
			</div>
			<div class="fromboxmain">
				<span>To :</span>
				<span><?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
				<span class="padding-left">
					<?php echo $this->Js->submit('', array(
						'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'update'=>'#memberdetailpage',
						'class'=>'searchbtn',
						'controller'=>'member',
						'action'=>'mdposition/'.$member_id.'/'.$PositionModel,
						'url'   => array('controller' => 'member', 'action' => 'mdposition/'.$member_id.'/'.$PositionModel)
					));?>
				</span>
			</div>
		</div>
	  <?php echo $this->Form->end();?>
</div>
<div id="gride-bg">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberdetailpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'mdposition/'.$member_id.'/'.$PositionModel)
	));
	$currentpagenumber=$this->params['paging'][$PositionModel]['page'];
	?>
	<div class="padding-left-serchtabal">
		
	</div>
	<div class="height10"></div>
	<div class="display-text display-text-padding"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?>
	
	<?php foreach($PendingPositionModules as $key=>$value){ if($key==$PositionModel){?>
		<?php echo $this->Js->link('Pending Positions', array('controller'=>'member', "action"=>"pendingposition/".$member_id."/".ucfirst($value)), array(
			'update'=>'#memberdetailpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'class'=>'btngray floatright'
		));?>
	<?php } }?>
	</div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
		<div class="tablegridheader">
			<div>
				<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Position Id', array('controller'=>'member', "action"=>"mdposition/".$member_id."/".$PositionModel."/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Position Id'
					));
				?>
			</div>
			<div>
				<?php 
					echo $this->Js->link('Plan Id', array('controller'=>'member', "action"=>"mdposition/".$member_id."/".$PositionModel."/0/0/planid/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Id'
					));
				?>
			</div>
			<div>
				<?php echo $this->Js->link('Purchase Date', array('controller'=>'member', "action"=>"mdposition/".$member_id."/".$PositionModel."/0/0/purchasedate/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Purchase Date'
					));
				?>
			</div>
			<div>
				<?php echo $this->Js->link('Amount', array('controller'=>'member', "action"=>"mdposition/".$member_id."/".$PositionModel."/0/0/amount/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Amount'
					));
				?>
			</div>
			<div><?php echo 'Total Earning';?></div>
			<div>
				<?php echo $this->Js->link('NP/RP', array('controller'=>'member', "action"=>"mdposition/".$member_id."/".$PositionModel."/0/0/cycle_complete/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberdetailpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By New Purchage or Re-purchage'
					));
				?>
			</div>
			<div><?php echo 'Status';?></div>
		</div>
		
		<?php
		$i = 0;
		foreach ($positions as $position):
			$class = 'class="blue-color"';if ($i++ % 2 == 0){$class = 'class="white-color"';}
		?>
			<div class="tablegridrow">
				<div>#<?php echo $position[$PositionModel]['id']; ?></div>
				<div><?php echo $position[$PositionModel]['planid']; ?></div>
				<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $position[$PositionModel]['purchasedate']);?></div>
				<div>$<?php echo round($position[$PositionModel]['amount'],4);?></div>
				<?php $tk='<b>Earning :</b> $'.round($position[$PositionModel]['earning'],4).'<br /><b>Re-purchase :</b> $'.round($position[$PositionModel]['repurchase'],4);$total=round($position[$PositionModel]['earning']+$position[$PositionModel]['repurchase'],4);?>
				<div><a href="#" class="vtip" title='<?php echo $tk;?>'><?php echo '$'.$total;?></a></div>
				<div><?php echo $position[$PositionModel]['nprp'];?></div>
				<div>
					<?php 
						if($position[$PositionModel]['status']==0)
							echo 'Inactive/Completed';
						else
							echo 'Active';
					?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	
	<?php if(count($positions)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
	<div class="margin10">	
		<?php
		if($this->params['paging'][$PositionModel]['count']>$this->Session->read('pagerecord'))
		{?>
		<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
		<div class="floatleft margintop19">
			<?php echo $this->Form->create('Position',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'mdposition/'.$member_id.'/'.$PositionModel.'/0/rpp')));?>
			<div class="resultperpage">
				<label>
					<?php 
					echo $this->Form->input('resultperpage', array(
					  'type' => 'select',
					  'options' => $resultperpage,
					  'selected' => $this->Session->read('pagerecord'),
					  'class'=>'',
					  'label' => false,
					  'div'=>false,
					  'style' => '',
					  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
					));
					?>
				</label>
			</div>
			<span id="resultperpageapply" style="display:none;">
				<?php echo $this->Js->submit('Apply', array(
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#memberdetailpage',
				  'class'=>'',
				  'div'=>false,
				  'controller'=>'member',
				  'action'=>'mdposition/'.$member_id.'/'.$PositionModel.'/0/rpp',
				  'url'   => array('controller' => 'member', 'action' => 'mdposition/'.$member_id.'/'.$PositionModel.'/0/rpp')
				));?>
			</span>
			<?php echo $this->Form->end();?>
		</div>
		<?php }?>
		<div class="floatright">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
		</ul>
		</div>
		<div class="clear-both"></div>
	</div>
	<div class="height7"></div>
</div>
</div>

<div class="height10"></div>
<div><?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>