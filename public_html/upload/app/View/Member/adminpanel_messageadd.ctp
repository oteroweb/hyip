<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php 
if($textarea=='advanced')
{
	echo $this->Javascript->link('tiny_mce/tiny_mce.js');
	echo $this->Javascript->link('tinymce');
	$textareaclass='advancededitor';
	$submitbuttonclass='tinyMCEtriggerSavetk';
}
elseif($textarea=='simple')
{
	$textareaclass='';
	$submitbuttonclass='';
}
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Member / Admin Message</div>
<div id="memberpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Admin_Message" target="_blank">Help</a></div>
<?php echo $this->Javascript->link('allpage');?>
<script>
$(function() {
	var availableTags = [
		<?php echo $this->Session->read('messagememberlist');?>
	];
	
function splitTK( val ) {
	return val.split( /,\s*/ );
}
function extractLast( term ) {
	return splitTK( term ).pop();
}
$( "#memberidlist" )
	// don't navigate away from the field on tab when selecting an item
	.bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).data( "ui-autocomplete" ).menu.active ) {
			event.preventDefault();
		}
	})
	.autocomplete({
			minLength: 0,
			source: function( request, response ) {
			// delegate back to autocomplete, but extract the last term
			response( $.ui.autocomplete.filter(
			availableTags, extractLast( request.term ) ) );
		},
		focus: function() {
			// prevent value inserted on focus
			return false;
		},
		select: function( event, ui ) {
			var terms = splitTK( this.value );
			// remove the current input
			terms.pop();
			// add the selected item
			mid=ui.item.value.split(" (");
			terms.push( mid[0] );
			//terms.push( ui.item.value );
			// add placeholder to get the comma-and-space at the end
			terms.push( "" );
			this.value = terms.join( ", " );
			return false;
		}
	});
});
</script>
	<div id="UpdateMessage"></div>
	
<div class="backgroundwhite">
		<?php echo $this->Form->create('Membermessage_history',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'advertisement','action'=>'messageadsaddaction')));?>
			<?php if(isset($massagedata['Membermessage_history']["id"])){
				echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$massagedata['Membermessage_history']["id"], 'label' => false));
			}?>
				<div class="frommain">
					<div class="fromnewtext">Send to :<span class="red-color">*</span>  </div>
					<div class="fromborderdropedown3">
						<div class="select-main">
							<label>
							<?php
								$messagetype=array('0'=>'All', '1'=>'Member Ids', '2'=>'Paid Members', '3'=>'Unpaid Members', '4'=>'Membership Wise');
								echo $this->Form->input('messagetype', array(
									'type' => 'select',
									'options' => $messagetype,
									'selected' => $massagedata['Membermessage_history']['messagetype'],
									'class'=>'',
									'label' => false,
									'div' => false,
									'style' => '',
									'onchange' => 'if($(this).val()==1){$(".messagetypemembership,.messagetyperegplan,.messagetypedynamicplan,.messagetyperevplan").hide();$(".messagetypefields").show();} else if($(this).val()==4){$(".messagetypefields,.messagetyperegplan,.messagetypedynamicplan,.messagetyperevplan").hide();$(".messagetypemembership").show();}else if($(this).val()==5){$(".messagetypefields,.messagetypemembership,.messagetypedynamicplan,.messagetyperevplan").hide();$(".messagetyperegplan").show();}else if($(this).val()==6){$(".messagetypefields,.messagetypemembership,.messagetyperegplan,.messagetyperevplan").hide();$(".messagetypedynamicplan").show();}else if($(this).val()==7){$(".messagetypefields,.messagetypemembership,.messagetyperegplan,.messagetypedynamicplan").hide();$(".messagetyperevplan").show();}else{$(".messagetypefields,.messagetypemembership,.messagetyperegplan,.messagetypedynamicplan,.messagetyperevplan").hide();}'
							));?>
							</label>
						</div>
					</div>
				
				<div class="fromborderdropedown3 messagetypefields" style="display:<?php echo ($massagedata['Membermessage_history']['messagetype']==1)?'':'none'; ?>;">
					<?php echo $this->Form->input('memberlist', array('type'=>'text', 'label' => false, 'div'=>false, 'id'=>'memberidlist', 'class'=>'fromboxbg', 'value'=>$massagedata['Membermessage_history']['memberlist'], 'style'=>''));?></span>
			    </div>	
			
				<div class="messagetypemembership" style="display:<?php echo ($massagedata['Membermessage_history']['messagetype']==4)?'':'none'; ?>;">
					<div class="fromborderdropedown3 checkboxlist" style="border-top:0;">
						<?php
							if($massagedata['Membermessage_history']['messagetype']==4)
								$selectmembership=explode(',',$massagedata['Membermessage_history']['memberlist']);
							else
								$selectmembership=array();
						
							echo $this->Form->input('membership', array(
								'type'=>'select',
								'multiple'=>'checkbox',
								'label' => false,
								'class'=>'',
								'div'=>true,
								'selected' => $selectmembership,
								'options'=> $Membership
							));
						?>
					</div>
					
				</div>
				<div class="messagetyperegplan" style="display:<?php echo ($massagedata['Membermessage_history']['messagetype']==5)?'':'none'; ?>;">
					<div class="fromborder" style="padding: 4px;">
						<?php 
							if($massagedata['Membermessage_history']['messagetype']==5)
								$selectDailyearningplan=explode(',',$massagedata['Membermessage_history']['memberlist']);
							else
								$selectDailyearningplan=array();
							
							echo $this->Form->input('regularearningplan', array(
								'type'=>'select',
								'multiple'=>'checkbox',
								'label' => false,
								'class'=>'marginleft9',
								'div'=>false,
								'selected' => $selectDailyearningplan,
								'options'=> $Dailyearningplan
							));
						?>
					</div>
				</div>
				<div class="messagetypedynamicplan" style="display:<?php echo ($massagedata['Membermessage_history']['messagetype']==6)?'':'none'; ?>;">
					<div class="fromborder" style="padding: 4px;">
						<?php
							if($massagedata['Membermessage_history']['messagetype']==4)
								$selectDynamicearningplan=explode(',',$massagedata['Membermessage_history']['memberlist']);
							else
								$selectDynamicearningplan=array();
								
							echo $this->Form->input('dynamicearningplan', array(
								'type'=>'select',
								'multiple'=>'checkbox',
								'label' => false,
								'class'=>'marginleft9',
								'div'=>false,
								'selected' => $selectDynamicearningplan,
								'options'=> $Dynamicearningplan
							));
						?>
					</div>
				</div>
				<div class="messagetyperevplan" style="display:<?php echo ($massagedata['Membermessage_history']['messagetype']==7)?'':'none'; ?>;">
					<div class="fromborder" style="padding: 4px;">
						<?php
							if($massagedata['Membermessage_history']['messagetype']==4)
								$selectRevenueplan=explode(',',$massagedata['Membermessage_history']['memberlist']);
							else
								$selectRevenueplan=array();
								
							echo $this->Form->input('revenuesharingplan', array(
								'type'=>'select',
								'multiple'=>'checkbox',
								'label' => false,
								'class'=>'marginleft9',
								'div'=>false,
								'selected' => $selectRevenueplan,
								'options'=> $Revenueplan
							));
						?>
					</div>
				</div>
				<div class="fromnewtext">Subject :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<?php echo $this->Form->input('description', array('type'=>'text', 'label' => false, 'div'=>false,'value'=>stripslashes($massagedata['Membermessage_history']['description']), 'class'=>'fromboxbg', 'style'=>''));?>
				</div>

				<div class="formbutton">
					<?php 
					if($textarea=='simple'){$showtextarea='advanced';}elseif($textarea=='advanced'){$showtextarea='simple';}
					$link='messageadd/'.$showtextarea.'/'.$massagedata['Membermessage_history']['id'];
					echo $this->Js->link("Switch To ".ucfirst($showtextarea)." Mode", array('controller'=>'member', "action"=>$link), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'btngray'
					));?>
					<?php if($textarea=='simple'){ ?>
					<input type="button" value="Copy Same Content To All Language" onclick="if(confirm('Copy English Content Into All Languages')){$('.alllanguagesame').val($('#Membermessage_historyMessageeng').val()); }" class="btnorange floatright" style="margin-right: 5px;" />
					<?php }else{ ?>
					<input type="button" value="Copy Same Content To All Language" onclick="if(confirm('Copy English Content Into All Languages')){for (i=0; i < tinyMCE.editors.length; i++){ tinyMCE.editors[i].setContent(tinyMCE.editors[0].getContent()); }}" class="btnorange floatright" style="margin-right: 5px;" />
					<?php } ?>
				</div>
				<div class="clearboth"></div>
				<div class="newgreytabs">

					<ul class="nav nav-tabs" role="tablist">
					     <?php foreach($languagedata as $code=>$displayname){ $activeclass='';if($code=='eng'){ $activeclass='active'; } ?>
						<li role="presentation" class="<?php echo $activeclass; ?>">
							<a onclick="$('.allactive').hide();$('.<?php echo $code; ?>').show();" class="active" role="tab" data-toggle="tab"><?php echo $displayname ?></a>
						</li>
						<?php } ?>
					</ul>
			      
				      <div class="tab-content">
					  <?php foreach($languagedata as $code=>$displayname){ $activeclass='';if($code!='eng'){ $activeclass='style="display: none" '; } ?>
					  <div <?php echo $activeclass; ?> role="tabpanel" class="tab-pane active allactive <?php echo $code; ?>">
					      <div class="masstextnew">Message (<?php echo $displayname;?>) :<span class="red-color">*</span></div>
					      <div class="greyboxnewtabsin"><?php echo $this->Form->input('message'.$code, array('type'=>'textarea', 'label' => false, 'div' => false, 'class'=>'from-textarea alllanguagesame '.$textareaclass,'value'=>stripslashes($massagedata['Membermessage_history']['message'.$code]), 'style'=>'height: 400px;'));?></div>
					  </div>
					<?php }?>
				      </div>
			      </div>
				
				<?php if(!isset($SubadminAccessArray) || in_array('advertisement',$SubadminAccessArray) || in_array('advertisement/adminpanel_messageads',$SubadminAccessArray)){ ?>
						<div class="formbutton">
							<?php echo $this->Js->submit('Send', array(
								'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'update'=>'#UpdateMessage',
								'class'=>'btnorange '.$submitbuttonclass,
								'controller'=>'member',
								'div'=>false,
								'action'=>'messageaddaction',
								'url'   => array('controller' => 'member', 'action' => 'messageaddaction')
							  ));?>
							  <?php echo $this->Js->link("Back", array('controller'=>'member', "action"=>"message"), array(
								  'update'=>'#memberpage',
								  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								  'escape'=>false,
								  'class'=>'btngray'
							  ));?>
						</div>
				<?php } ?>
				</div>
		<?php echo $this->Form->end();?>
		
</div>
<?php if(!$ajax){?>
</div><!--#memberpage over-->
<?php }?>	
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>