<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 17-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Member / Mass Mailing</div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li>
					<?php echo $this->Js->link("Step-1", array('controller'=>'member', "action"=>"massmail"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<?php if($IsAdminAccess){?>
				<li class="active">
					<?php echo $this->Js->link("Step-2", array('controller'=>'member', "action"=>"massmaillist"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Step-3", array('controller'=>'member', "action"=>"massmailcompose"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<?php }?>
			 </ul>
	</div>
</div>
<div class="tab-content">
<div id="memberpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Mass_Mailing#Step-2" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'massmaillist')
	));
	$currentpagenumber=$this->params['paging']['Member']['page'];
	?>
	<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'massmailremoveemail')));?>
	<div class="greenbottomborder">
	<div class="paginator-text paddingtop0"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php echo $this->Form->checkbox('selectAllCheckboxes', array(
			'hiddenField' => false,
			'onclick' => 'selectAllCheckboxes("MemberIds",this.checked);'
		));?>
		<label for="MemberSelectAllCheckboxes"></label>
	</div>
	<div class="addnew-button massactionbox">
		<div class="actionmenu">
			<div class="btn-group">
				<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
				  Action <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
				<li>
					<?php echo $this->Js->link("Remove All (Record(s) : ".$this->params['paging']['Member']['count'].")", array('controller'=>'member', "action"=>"massmailremoveemail/all"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>''
					));?>
				</li>
				<li>
					<?php echo $this->Js->submit('Remove Selected', array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'massactionbtn',
						'div'=>false,
						'controller'=>'member',
						'action'=>'massmailremoveemail',
						'url'   => array('controller' => 'member', 'action' => 'massmailremoveemail')
					));?>
				</li>
			  </ul>
			</div>
		</div>
	</div>
	<div class="clear-both"></div>
	
	<div class="tablegrid">
		<div class="tablegridheader">
			<div>
				<?php 
				if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
				echo $this->Js->link('M. Id', array('controller'=>'member', "action"=>"massmaillist/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Member Id'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Reg. Date', array('controller'=>'member', "action"=>"massmaillist/0/0/reg_dt/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Registration Date'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Username', array('controller'=>'member', "action"=>"massmaillist/0/0/user_name/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Username'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Full Name', array('controller'=>'member', "action"=>"massmaillist/0/0/f_name/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By First Name'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Email', array('controller'=>'member', "action"=>"massmaillist/0/0/email/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Email'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Sponsor', array('controller'=>'member', "action"=>"massmaillist/0/0/referrer/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Sponsor Id'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Tot Ref.', array('controller'=>'member', "action"=>"massmail/0/0/total_referrer/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Total Referrals'
				));?>
			</div>
			<div></div>
		</div>
			<?php foreach ($members as $member): ?>
				<div class="tablegridrow">
					<div>
						<?php 
						echo $this->Js->link($member['Member']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/member/massmail~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $member['Member']['reg_dt']);?></div>
					<div>
						<?php 
						echo $this->Js->link($member['Member']['user_name'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/member/massmail~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>
						<?php 
						echo $this->Js->link($member['Member']['f_name']." ".$member['Member']['l_name'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/member/massmail~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>
						<?php 
						echo $this->Js->link($member['Member']['email'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/member/massmail~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>
						<?php if($member['Member']['referrer']!=0){ echo $this->Js->link($member['Member']['referrer'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['referrer']."/top/member/leaderboard~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Sponsor'
						)); }else{echo $member['Member']['referrer'];}?>
					</div>
					<div><?php echo $member['Member']['total_referrer']; ?></div>
					<div class="checkbox">
						<?php
						echo $this->Form->checkbox('MemberIds.', array(
						  'value' => $member['Member']['member_id'],
						  'class' => 'MemberIds',
						  'id'=>'checkMemberIds'.$member['Member']['member_id'],
						  'hiddenField' => false
						));
						?>
						<label for="<?php echo 'checkMemberIds'.$member['Member']['member_id']; ?>"></label>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($members)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	<?php echo $this->Form->end();?>
	<?php 
	if($this->params['paging']['Member']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'massmaillist/0/rpp')));?>
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#memberpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'Member',
			  'action'=>'massmaillist/0/rpp',
			  'url'   => array('controller' => 'member', 'action' => 'massmaillist/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
</div>
</div>
<?php if(!$ajax){?>
</div><!--#memberpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>