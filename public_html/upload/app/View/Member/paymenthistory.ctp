<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 25-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(!$ajax){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Payment History');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">

<?php // Search box code starts here ?>
<div class="floatleft searchlabel"><?php echo __("Advanced Search");?></div>
	<div class="floatright"><a href="javascript:void(0)" onclick="togglebox('.searchlabel', this,'<?php echo __("[+] Show Search Box");?>','<?php echo __("[-] Hide Search Box");?>');"><?php echo __("[-] Hide Search Box");?></a></div>
	<div class="clear-both"></div>
	<div class="advsearchbox searchlabel">
	<?php echo $this->Form->create('Member_fee_payment',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'member','action'=>'paymenthistory')));?>
		<div class="inlineblock vat">
			<div>
				<span class="searchboxcol1"><?php echo __("Search By");?>  :&nbsp;</span>
				<div class="select-dropdown smallsearch">
					<label>
						<?php echo $this->Form->input('searchby', array(
							'type' => 'select',
							'options' => array('all'=>__('All'), 'transaction_no'=>__('Transaction No'), 'amount'=>__('Amount'), 'fees'=>__('Fees'),'discount'=>__('Discount'), 'receiver_email'=>__('Receiver Email'), 'processor'=>__('Payment Processor'), 'ptype'=>__('Payment For'), 'payment_status'=>__('Status')),
							'selected' => $searchby,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => ''
						));?>
					</label>
				</div>
			</div>
			<div>
				<span class="searchboxcol1"><?php echo __("Search For");?> :&nbsp;</span>
				<?php echo $this->Form->input('searchfor', array('type'=>'text', 'id'=>'searchfor', 'value'=>$searchfor, 'label' => false, 'class'=>'finece-from-box searchfortxt','div' => false));?>
			</div>
		</div>
		<div  class="inlineblock vat">
			<div class="margintb5">
				<span class="searchboxcol2"><?php echo __("From Date");?> :&nbsp;</span>
				<?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker searchdatetxt'));?>
			</div>
			<div>
				<span class="searchboxcol2"><?php echo __("To Date");?> :&nbsp;</span>
				<?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker searchdatetxt'));?>
			</div>
		</div>
		<div class="textcenter margint5">
			<?php echo $this->Js->submit(__('Update Results'), array(
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#memberpage',
				'class'=>'button floatnone',
				'div'=>false,
				'controller'=>'member',
				'action'=>'paymenthistory',
				'url'   => array('controller' => 'member', 'action' => 'paymenthistory')
			  ));?>
		</div>
		<div class="clear-both"></div>
		
	<?php echo $this->Form->end();?>
	</div>
	<?php // Search box code ends here ?>
	
	<div class="height10"></div>
<?php }?>
<div id="memberpage">
<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'paymenthistory')
	));
	$currentpagenumber=$this->params['paging']['Member_fee_payment']['page'];?>
		
		<?php // Payment History table starts here ?>
		<div class="padding-left-serchtabal"></div>
		<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			<div class="divtable">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam">
							<?php 
							if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
							echo $this->Js->link(__('Date'), array('controller'=>'member', "action"=>"paymenthistory/0/pay_dt/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Date')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Transaction No'), array('controller'=>'member', "action"=>"paymenthistory/0/transaction_no/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Transaction No')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__("Amount"), array('controller'=>'member', "action"=>"paymenthistory/0/amount/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Amount')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Fees'), array('controller'=>'member', "action"=>"paymenthistory/0/fees/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Fees')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Discount'), array('controller'=>'member', "action"=>"paymenthistory/0/discount/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Discount')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Receiver Id'), array('controller'=>'member', "action"=>"paymenthistory/0/receiver_email/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Receiver Id')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Payment Processor'), array('controller'=>'member', "action"=>"paymenthistory/0/processor/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Payment Processor')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Payment For'), array('controller'=>'member', "action"=>"paymenthistory/0/ptype/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Payment For')
							));?>
						</div>
                        <div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Status'), array('controller'=>'member', "action"=>"paymenthistory/0/payment_status/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Status')
							));?>
						</div>
					</div>
				</div>
				<div class="divtbody">
					<?php
					$i = 0;
					foreach ($paymenthistories as $paymenthistory):
						if($i%2==0){$class='gray-color';}else{$class='white-color';}
						if($paymenthistory['Member_fee_payment']['ptype']=='fund') $description='Add Funds';
						elseif($paymenthistory['Member_fee_payment']['ptype']=='position') $description='Position';
						elseif($paymenthistory['Member_fee_payment']['ptype']=='memberfee') $description='Membership';
						else $description=ucwords($paymenthistory['Member_fee_payment']['ptype']);
						
						if(in_array($paymenthistory['Member_fee_payment']['payment_status'],array('Success','success', 'COMPLETE', 'Done', 'Completed'))){ $payment_status='Completed'; }
						elseif(in_array($paymenthistory['Member_fee_payment']['payment_status'],array('CANCELED', 'Fail'))){ $payment_status='Failed'; }
						
						?>
						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $paymenthistory['Member_fee_payment']['pay_dt']); ?></div>
							<div class="divtd textcenter vam">
								<?php if($paymenthistory['Member_fee_payment']['processor']=='Blockchain')
								{
									echo '<a href="" class="vtip" title="'.$paymenthistory['Member_fee_payment']['transaction_no'].'" ></a>';
								}
								else
								{
									echo $paymenthistory['Member_fee_payment']['transaction_no'];
								}
								?>
							
							</div>
							<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($paymenthistory['Member_fee_payment']['amount']*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
							<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($paymenthistory['Member_fee_payment']['fees']*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
							<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($paymenthistory['Member_fee_payment']['discount']*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
							<div class="divtd textcenter vam"><?php echo $paymenthistory['Member_fee_payment']['receiver_email']; ?></div>
							<div class="divtd textcenter vam"><?php echo $paymenthistory['Member_fee_payment']['processor']; ?></div>
							<div class="divtd textcenter vam"><?php echo __($description); ?></div>
                            <div class="divtd textcenter vam"><?php echo __($payment_status); ?></div>
						</div>
					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($paymenthistories)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // Payment History table ends here ?>

	<?php // Paging code starts here ?>
	<?php
	$pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Member_fee_payment']['count']>$pagerecord)
	{?>	
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Member_fee_payment',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'paymenthistory/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#memberpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'member',
						  'action'=>'paymenthistory/rpp',
						  'url'   => array('controller' => 'member', 'action' => 'paymenthistory/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
            </div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
	
	</div>
	
</div>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>