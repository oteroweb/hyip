<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 17-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Member / Leader Board</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<?php echo $this->Js->link("Top Position Earners", array('controller'=>'member', "action"=>"leaderboard"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Top Commission Earners", array('controller'=>'member', "action"=>"topcommissionearners/top"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<li>
					<?php echo $this->Js->link("Top Referrals", array('controller'=>'member', "action"=>"toprefferals/top"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<?php /* ?>
				<li>
					<?php echo $this->Js->link("Referral Tracking", array('controller'=>'member', "action"=>"urltracking"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
					));?>
				</li>
				<?php */ ?>
			</ul>
	</div>
</div>
<div class="tab-content">
<div id="memberpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Leader_Board#Top_Position_Earners" target="_blank">Help</a></div>
	<div class="tab-innar">
		<ul>
			<?php foreach($LeaderBordModules as $key=>$value){ if($key==$PositionModel){$class="active";}else{$class="";}?>
		    <li>
				<?php echo $this->Js->link($value, array('controller'=>'member', "action"=>"leaderboard/0/".$key), array(
					'update'=>'#memberpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>$class
				));?>
		    </li>
			<?php }?>
		</ul>
	</div>
	
	<div id="UpdateMessage"></div>
	
<div class="serchmainbox">
    <div class="serchgreybox">Search Option</div>
    <?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'leaderboard')));?>
	 <div class="from-box">
         <div class="fromboxmain width480">
             <span>From :</span>
              <span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
         </div>
         <div class="fromboxmain">
             <span>To :</span>
             <span><?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
             <span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#memberpage',
					'class'=>'searchbtn',
					'controller'=>'member',
					'action'=>'leaderboard',
					'url'   => array('controller' => 'member', 'action' => 'leaderboard/0/'.$PositionModel)
				));?>
			 </span>
         </div>
    </div>
	 <?php echo $this->Form->end();?>
</div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'leaderboard')
	));
	$currentpagenumber=$this->params['paging']['Member']['page'];
	?>
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="height10"></div>
	<div class="records records-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="clear-both"></div>
	<div class="tablegrid">
		<div class="tablegridheader">
				<div>M. Id</div>
				<div>Reg. Date</div>
				<div>Username</div>
				<div>Full Name</div>
				<div>Email</div>
				<div>Position Earnings</div>
				<div>Sponsor</div>
				<div>Tot Ref.</div>
				<div>Action</div>
		</div>
			<?php foreach ($members as $member): ?>
				<div class="tablegridrow">
					<div>
							<?php 
							echo $this->Js->link($member['Member']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/member/leaderboard~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
					</div>
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $member['Member']['reg_dt']);?></div>
					<div>
						<?php 
							echo $this->Js->link($member['Member']['user_name'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/member/leaderboard~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
					</div>
					<div>
						<?php 
							echo $this->Js->link($member['Member']['f_name']." ".$member['Member']['l_name'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/member/leaderboard~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
					</div>
					<div>
						<?php 
							echo $this->Js->link($member['Member']['email'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/member/leaderboard~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
					</div>
					<div>$<?php echo round($member[0]['total_position'],4); ?></div>
					<div>
						<?php if($member['Member']['referrer']!=0){ echo $this->Js->link($member['Member']['referrer'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['referrer']."/top/member/leaderboard~top"), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Sponsor'
						)); }else{echo $member['Member']['referrer'];}?>
					</div>
					<div><?php echo $member['Member']['total_referrer']; ?></div>
					<div>
								<?php 
								echo $this->Js->link($this->html->image('search.png', array('alt'=>'View Member')), array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/member/leaderboard~top"), array(
									'update'=>'#pagecontent',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>'View Member'
								));?>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($members)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
	<?php
	if($this->params['paging']['Member']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'leaderboard/rpp')));?>
		<div class="resultperpage">
			<label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#memberpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'member',
			  'action'=>'leaderboard/rpp',
			  'url'   => array('controller' => 'member', 'action' => 'leaderboard/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php } if(false){ ?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<?php } ?>
	<div class="clear-both"></div>
	<div class="height10"></div>
</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#memberpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>