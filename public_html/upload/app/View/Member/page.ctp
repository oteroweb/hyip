<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 25-09-2014
  *********************************************************************/
?>
<?php if($virtual_page_content!="404 Page Not Found") { ?>
	<div class="comisson-bg">
		<div class="text-ads-title-text"><?php echo stripslashes($virtual_page_name); ?></div>
		<div class="clear-both"></div>
	</div>
	
	<?php // Virtual page content starts here ?>
	<div class="main-box-eran">
	  <?php if(trim($virtual_page_content)!=''){ echo stripslashes($virtual_page_content); } ?>
	</div>
	<?php // Virtual page content ends here ?>
	
<?php }else {
	echo '<div class="main-box-eran">';
	if(trim($virtual_page_content)!=''){ echo stripslashes($virtual_page_content); }
	echo '</div>';	
} ?>