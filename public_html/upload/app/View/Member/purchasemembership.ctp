<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 03-11-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($themesubmenuaccess){?>
<?php if(!$ajax){ ?>
<div id="memberpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<script> processer=new Array();
	<?php
	$firstproceser=0;
	foreach($Processorsextrafield as $proid=>$provalue)
	{
	?>
	processer[<?php echo $proid; ?>]=<?php echo $provalue; ?>;
	<?php }?>
</script>
<?php // Current Balance code starts here ?>

<script type="text/javascript">
	$(".included-menu ul li a").click(function()
	{
		$(".includedoverlay").addClass("addoverlay");
	});
	$(".includedoverlay").click(function()
	{
		$(".included-menu ul li span").hide();
		$(this).removeClass("addoverlay");
	});
</script>

<?php // Current Balance code starts here ?>
<div class="textright"><a class="shbutton currentbalancebutton" href="javascript:void(0)" onclick="currentbalancebox('.balancebox', this,'<?php echo __("[+] Show Balance");?>','<?php echo __("[-] Hide Balance");?>');"><?php if(@$_COOKIE['curbal']==1)echo __("[+] Show Balance"); else echo __("[-] Hide Balance");?>
</a></div>
<div class="comisson-bg balancebox <?php if(@$_COOKIE['curbal']==1)echo 'hide';?>">
	<div class="text-ads-title-text"><?php echo __("Current Balance");?></div>
	<div class="text-ads-title-text-right"></div>
	<div class="clear-both"></div>
</div>
<div id="balancebox" class="main-box-eran balancebox <?php if(@$_COOKIE['curbal']==1)echo 'hide';?>">
	<?php if($SITECONFIG["balance_type"]==1){ ?>
		<div class="divtable">
                        <div class="divtr white-color">
                                <div class="divth textcente vam"><?php echo __("Cash Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCash*$Currency['rate'],3); ?><?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
                        <div class="divtr gray-color">
                                <div class="divth textcente vam"><?php echo __("Earning Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberEarning*$Currency['rate'],3); ?><?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php } ?>
                        <div class="divtr white-color">
                                <div class="divth textcente vam"><?php echo __("Re-purchase Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberRepurchaseCash*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
                        <div class="divtr gray-color">
                                <div class="divth textcente vam"><?php echo __("Commission Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCommission*$Currency['rate'],3); ?><?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php } ?>
                </div>
		<?php }else{ 
			?>
			<div class="divtable">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam"><?php echo __("Payment Processor");?></div>
						<div class="divth textcenter vam"><?php echo __("Cash Balance");?></div>
                                                <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
						<div class="divth textcenter vam"><?php echo __("Earning Balance");?></div>
                                                <?php } ?>
						<div class="divth textcenter vam"><?php echo __("Re-purchase Balance");?></div>
                                                <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
						<div class="divth textcenter vam"><?php echo __("Commission Balance");?></div>
                                                <?php } ?>
					</div>
				</div>
				<?php $i=1;
				foreach($MemberCash as $proc_name=>$cash)
				{
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $proc_name;?> :</div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCash[$proc_name]*$Currency['rate'],3); ?><?php echo " ".$Currency['suffix'];?></div>
                                                <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberEarning[$proc_name]*$Currency['rate'],3); ?><?php echo " ".$Currency['suffix'];?></div>
                                                <?php } ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberRepurchaseCash[$proc_name]*$Currency['rate'],3); ?><?php echo " ".$Currency['suffix'];?></div>
                                                <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCommission[$proc_name]*$Currency['rate'],3); ?><?php echo " ".$Currency['suffix'];?></div>
                                                <?php } ?>
					</div>
					<?php
				$i++; }?>
			</div>
	<?php } ?>
</div>
<?php // Current Balance code ends here ?>



<div class="includedoverlay"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Purchase Membership");?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
	<div class="height10"></div>
			<?php // Membership purchase form starts here ?>
			<?php echo $this->Form->create('Membership',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'member','action'=>'membershipupgrateaction/'.$membership['Membership']['id'])));?>
			<div class="purchaseplanmain">	
				<div class="form-box">
					<div class="form-row">
						<div class="form-col-1"><?php echo __("Name");?> : </div>
						<div class="form-col-2 form-text"><?php echo $membership['Membership']['membership_name'];?></div>
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __("Price");?> : </div>
						<div class="form-col-2 form-text">
							<?php echo $Currency['prefix'];?><?php echo round($membership['Membership']['price']*$Currency['rate'],2)." ".$Currency['suffix'];?>
							<span id="couponspan">
							<?php
								$payamount = round($membership['Membership']['price']*$Currency['rate'],2);
								if($this->Session->check('coupon'))
								{
									$sessioncoupon=$this->Session->read('coupon');
									if($sessioncoupon['page']=='membership' && $sessioncoupon['planid']==$membership['Membership']['id'])
									{
										echo " - <span>".$Currency['prefix'].round($sessioncoupon['amount']*$Currency['rate'],4)."</span> = <span>".$Currency['prefix'].round(($membership['Membership']-$sessioncoupon['amount'])*$Currency['rate'],4)." ".$Currency['suffix']."</span>";
									}
								}
							?>
							</span>
							<?php echo $this->Form->input('payamount', array('type'=>'hidden', 'id'=>'payamount', 'value'=>$payamount, 'div'=>false, 'label' => false, 'class'=>'payamount', 'onchange'=>''));?>
						</div>
					</div>
					<?php $commilev="";$commissionlevel=explode(",", $membership['Membership']['commissionlevel']);if($commissionlevel[0]>0){ ?>
					<div class="form-row">
						<div class="form-col-1"><?php echo __("Referral Commission Structure");?> : </div>
						<div class="form-col-2 form-text">
							<?php for($i=0;$i<count($commissionlevel);$i++){if($commissionlevel[$i]>0){$commilev.="Level ".($i+1)." : ".$commissionlevel[$i]."%, ";}else{break;}}echo trim($commilev,", ")?>	
						</div>
					</div>
					<?php }?>
					<div class="form-row">
						<div class="form-col-1"><?php echo __("How would you like to pay");?> : </div>
						<div class="form-col-2 form-text balance-width">
							
							<?php
								$paymentmethod=array();
								if(strpos($membership['Membership']['paymentmethod'],'cash') !== false)
									$paymentmethod['cash']=__('Cash Balance');
								if(strpos($membership['Membership']['paymentmethod'],'repurchase') !== false)
									$paymentmethod['repurchase']=__('Purchase Balance');
								if(strpos($membership['Membership']['paymentmethod'],'earning') !== false)
									$paymentmethod['earning']=__('Earning Balance');
								if(strpos($membership['Membership']['paymentmethod'],'commission') !== false)
									$paymentmethod['commission']=__('Commission Balance');
								if(strpos($membership['Membership']['paymentmethod'],'processor') !== false)
									$paymentmethod['processor']=__('Payment Processor');
								if($SITECONFIG['balance_type']==1)
								{
									echo $this->Form->radio('paymentmethod', $paymentmethod, array('value'=>'cash', 'legend' => false, 'separator'=>'&nbsp;', 'onchange'=>'if(this.value=="processor") {$(".paymentprocessorfield").show(500);} else{$(".paymentprocessorfield").hide(500);} if(this.value=="processor"){processorextrafield($("#paymentprocessors").val(),processer,"'.$SITEURL.'app/processorextrafield","Membership",".extrafield");showprocfee(1);}else{$(".extrafield").html("");showprocfee(0);}'));
								}
								elseif($SITECONFIG['balance_type']==2)
								{
									echo $this->Form->radio('paymentmethod', $paymentmethod, array('value'=>'cash', 'legend' => false, 'separator'=>'&nbsp;', 'onchange'=>'if(this.value=="processor"){processorextrafield($("#paymentprocessors").val(),processer,"'.$SITEURL.'app/processorextrafield","Membership",".extrafield");showprocfee(1);}else{$(".extrafield").html("");showprocfee(0);}'));
								}
							?>
							<div class="height7"></div>
						</div>
					</div>
					
					<div class="form-row paymentprocessorfield" style="display:<?php if($SITECONFIG['balance_type']==1){echo 'none';}?>;">
						<div class="form-col-1"><?php echo __("Please select a payment processor");?> : <span class="required">*</span></div>
							<?php
								$procfee_string = "var procfee_array = new Array();var procfeeamount_array = new Array();";
								
								foreach($processorsfee as $key=>$value)
								{
									$procfee_string.=" procfee_array[".$key."] = ".$value['fee'].";";
									$procfee_string.=" procfeeamount_array[".$key."] = ".$value['feeamount'].";";
								}
							?>
							<script type='text/javascript'><?php echo $procfee_string;?></script>
							
							<div class="select-dropdown">
								<label>
									<?php 
									echo $this->Form->input('paymentprocessor', array(
										  'type' => 'select',
										  'options' => $paymentprocessors,
										  'selected' => '',
										  'class'=>'paymentprocessor',
										  'label' => false,
										  'div' => false,
										  'id'=>'paymentprocessors',
										  'onchange' => 'if($("#MembershipPaymentmethodProcessor").attr("checked")){processorextrafield(this.value,processer,"'.$SITEURL.'app/processorextrafield","Membership",".extrafield");showprocfee(1);}else{showprocfee(0);}'
									));?>
								</label>
							</div>
							
							
							<span class="helptooltip vtip" title="<?php echo __('Fees is added to the amount when you pay through payment processor.') ?>"></span>
							
					</div>
					<div class="form-row">
						<div class="form-col-1"><?php echo __("What's included");?> : </div>
						<div class="form-col-2 form-text">
							<div class="included-menu">
								<ul>
									<?php 
									if($membership['Membership']['banner_credit']>0 && strpos($SITECONFIG["banneradplansetting"],'isenable|1') !== false)
									{
										echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Banneradplan\', \'.bannerplanlist\', \'id:'.$membership['Membership']['banner_credit'].'\', \''.$SITEURL.'\', \'bannerad/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("Banner Ad Plan")."</a>";
										echo '<span class="showincludecontent bannerplanlist"></span></li>';
									}
									if($membership['Membership']['txtadd_credit']>0 && strpos($SITECONFIG["textadplansetting"],'isenable|1') !== false)
									{
										echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Ptextadplan\', \'.textplanlist\', \'id:'.$membership['Membership']['txtadd_credit'].'\', \''.$SITEURL.'\', \'textad/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("Text Ad Plan")."</a>";
										echo '<span class="showincludecontent textplanlist"></span></li>';
									}
							
									if($membership['Membership']['soloads_credit']>0 && strpos($SITECONFIG["soloadplansetting"],'isenable|1') !== false)
									{
										echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Soloadplan\', \'.soloplanlist\', \'id:'.$membership['Membership']['soloads_credit'].'\', \''.$SITEURL.'\', \'soload/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("Solo Ad Plan")."</a>";
										echo '<span class="showincludecontent soloplanlist"></span></li>';
									}
									if($membership['Membership']['ppc']>0 && strpos($SITECONFIG["ppcsetting"],'isenable|1') !== false)
									{
										echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Ppcplan\', \'.ppcplanlist\', \'id:'.$membership['Membership']['ppc'].'\', \''.$SITEURL.'\', \'ppc/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("PPC Ad Plan")."</a>";
										echo '<span class="showincludecontent ppcplanlist"></span></li>';
									}
									if($membership['Membership']['ptc']>0 && strpos($SITECONFIG["ptcsetting"],'isenable|1') !== false)
									{
										echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Ptcplan\', \'.ptcplanlist\', \'id:'.$membership['Membership']['ptc'].'\', \''.$SITEURL.'\', \'ptc/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("PTC Ad Plan")."</a>";
										echo '<span class="showincludecontent ptcplanlist"></span></li>';
									}
									if($membership['Membership']['loginad']>0 && strpos($SITECONFIG["loginadsetting"],'isenable|1') !== false)
									{
										echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Loginad\', \'.loginadplanlist\', \'id:'.$membership['Membership']['loginad'].'\', \''.$SITEURL.'\', \'loginad/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("Login Ad Plan")."</a>";
										echo '<span class="showincludecontent loginadplanlist"></span></li>';
									}
									if($membership['Membership']['bizdirectory']>0 && strpos($SITECONFIG["bizdirectorysetting"],'enablesurfing|1') !== false)
									{
										echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Directoryplan\', \'.directoryplanlist\', \'id:'.$membership['Membership']['bizdirectory'].'\', \''.$SITEURL.'\', \'directory/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("Biz Directory Plan")."</a>";
										echo '<span class="showincludecontent directoryplanlist"></span></li>';
									}
									if($membership['Membership']['webcredits']>0 && strpos($SITECONFIG["trafficsetting"],'enablewebcredit|1') !== false)
									{
										echo '<li><a href="javascript:void(0)" onclick="if($(this).parent(\'li\').find(\'span\').html()==\'\'){GetPlanList(\'Webcreditplan\', \'.webcreditplanlist\', \'id:'.$membership['Membership']['webcredits'].'\', \''.$SITEURL.'\', \'traffic/purchase/ID\');$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}else{$(\'.showincludecontent\').hide();$(this).parent(\'li\').find(\'span\').show();}">'.__("Traffic Plan")."</a>";
										echo '<span class="showincludecontent webcreditplanlist"></span></li>';
									}
									?>
								</ul>
							</div>
						</div>
					</div>
					<div class='extrafield'></div>
				</div>
				<div class="height10"></div>
				<div>
					<?php // Amount calculation box starts here ?>
					<div class="purchaseplanamountdetails floatleft" style="width:49.5%">
						<div class="form-box">
							<div class="form-row">
								<div class="form-col-1"><?php echo __("Amount");?> : </div>
								<div class="form-col-2 form-text"><?php echo $Currency['prefix'];?><span id="payableamount"><?php echo $payamount;?></span> <?php echo $Currency['suffix'];?></div>
							</div>
							
							<div class="form-row">
								<div class="form-col-1"><?php echo __("Fees");?> : </div>
								<div class="form-col-2 form-text"><span id="procfee">0</span>% + <?php echo $Currency['prefix'];?><span id="procfeeamount">0</span> <?php echo $Currency['suffix'];?></div>
							</div>
							
							<div class="form-row">
								<div class="form-col-1"><?php echo __("Final Amount");?> : </div>
								<div class="form-col-2 form-text"><?php echo $Currency['prefix'];?><span id="finalamount"><?php echo $payamount;?></span> <?php echo $Currency['suffix'];?></div>
							</div>
						</div>	
					</div>
					<?php // Amount calculation box ends here ?>
					
					<div class="purchaseplanbuttonbox floatright" style="width:50%;">
						<div class="form-box">
							<div class="form-row captchrow">
								<?php if($membership['Membership']['iscoupon']==1){ ?>
								<div class="profile-bot-left inlineblock" style="padding-left: 0px;padding-top: 7px;">
									<div class="form-col-1 forcaptchaonly"><?php echo __('Enter Coupon Code'); ?> : &nbsp;</div>
									<?php $couponclass='formtextbox coupontextbox';
									if(isset($sessioncoupon) && $sessioncoupon['page']=='membership' && $sessioncoupon['planid']=$membership['Membership']['id']) $couponclass.=' greenbg'; else $sessioncoupon['code']='';
									echo $this->Form->input('code', array('type'=>'text', 'div'=>false, 'label' => false, 'style'=>'width:45%;', 'class'=>$couponclass, 'value'=>$sessioncoupon['code']));?>
									<?php echo $this->Js->submit(__('Apply Coupon'), array(
										'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
										'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
										'update'=>'#UpdateMessage',
										'class'=>'button floatnone',
										'div'=>false,
										'controller'=>'member',
										'action'=>'membershipcoupon/'.$membership['Membership']['id'],
										'url'   => array('controller' => 'member', 'action' => 'membershipcoupon/'.$membership['Membership']['id'])
									));?>
								</div>
								<?php } ?>
								<div class="profile-bot-left inlineblock" style="font-size: 12px;"><?php if($SITECONFIG["enableagree"]==1){echo $this->Form->input('termsandconditions', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>'')); echo ' <a href="'.$SITEURL.'public/terms" target="_blank">'.__('I agree with the terms and conditions').'</a> '.__('of').' '.$SITECONFIG["sitetitle"]; }?></div>
							</div>
						</div>
						<div class="form-box">
							<div class="formbutton">
								<?php echo $this->Js->submit(__('Purchase'), array(
									'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'update'=>'#UpdateMessage',
									'class'=>'button ml10',
									'div'=>false,
									'controller'=>'member',
									'action'=>'membershipupgrateaction/'.$membership['Membership']['id'],
									'url'   => array('controller' => 'member', 'action' => 'membershipupgrateaction/'.$membership['Membership']['id'])
								));?>
								<?php echo $this->Js->link(__("Back"), array('controller'=>'member', "action"=>"membership"), array(
									'update'=>'#memberpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'button',
									'style'=>''
								));?>
							</div>
							<div class="clear-both"></div>		
						</div>	
					</div>
					<div class="clear-both"></div>
				</div>	
				<div class="clear-both"></div>
			
			<?php echo $this->Form->end();?>
			<?php // Membership purchase form starts here ?>
			
		</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>