<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-09-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
	<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List#Add_Order" target="_blank">Help</a></div>
	<div class="fromnewtext">How would you like to pay</div>
	<div class="fromborderdropedown3">
		<div class="rediobtn" style="padding-left: 28px;">
			<div class="retbtn">
				<div class="rediobox">
				<?php
					$paymentmethod=array(
						'cash'=>__('Cash Balance'),
						'repurchase'=>__('Re-purchase Balance'),
						'earning'=>__('Earning Balance'),
						'commission'=>__('Commission Balance'),
						'free'=>__('Free'),
					);
						echo $this->Form->radio('paymentmethod', $paymentmethod, array('value'=>'cash','name'=>'data[Member][paymentmethod]', 'legend' => false, 'separator'=>'&nbsp;'));
					?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="fromnewtext">Referral Commission?</div>
	<div class="fromborderdropedown3">
		<div class="rediobtn" style="padding-left: 28px;">
			<div class="retbtn">
				<div class="rediobox">	
				<?php
					$commission=array(
						0=>__('No'),
						1=>__('Yes')
					);
					echo $this->Form->radio('commissionpay', $commission, array('value'=>0,'name'=>'data[Member][commissionpay]', 'legend' => false, 'separator'=>'&nbsp;'));
				?>
				</div>
			</div>
		</div>
	</div>
	
	
	<?php if($SITECONFIG['balance_type']==2){?>
	<div class="fromnewtext">Payment Processor</div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
				<?php echo $this->Form->input('paymentprocessor', array(
					'type' => 'select',
					'options' => $paymentprocessors,
					'selected' => '',
					'class'=>'paymentprocessor',
					'label' => false,
					'div' => false,
					'name'=>'data[Member][paymentprocessor]',
					'style' => ''
			  ));?>
				</label>
			</div>
		</div>
	<?php }?>