<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 30-10-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('progressbar');echo $this->Html->css('progressbar');?>
<div id="memberpage">
<div id="UpdateMessage"></div>
<?php $onetime=array(); ?>
<?php if(count($membermemberships)){ ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Current Memberships");?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
		
		<?php // Current Memberships table starts here ?>
		<?php echo $this->Form->create('Membermembership',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'membership')));?>
		<div class="divtable textcenter">		
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam"><?php echo __("Name");?></div>
					<div class="divth textcenter vam"><?php echo __("Fees Type");?></div>
					<div class="divth textcenter vam"><?php echo __("Purchase Date");?></div>
					<div class="divth textcenter vam"><?php echo __("Expiry Date");?></div>
					<div class="divth textcenter vam"><?php echo __("Auto Renewal");?></div>
				</div>
			</div>
			<div class="divtbody">
				<?php $i=1;
				$display=0;
				foreach ($membermemberships as $membermembership):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $membermembership['Membership']['membership_name'];?></div>
						<div class="divtd textcenter vam"><?php echo ($membermembership['Membership']['membership_fee_type']=='Days') ? __('Every').' '.$membermembership['Membership']['membership_fee_value'].' '.__('Days') : __($membermembership['Membership']['membership_fee_type']);?></div>
						<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], date("Y-m-d H:00:00",strtotime($membermembership['Membermembership']['purchasedate']))); ?></div>
						<div class="divtd textcenter vam">
							<?php
							if($membermembership['Membership']['membership_fee_type']!='One Time')
							{
								echo $this->Time->format($SITECONFIG["timeformate"], date("Y-m-d H:00:00",strtotime($membermembership['Membermembership']['expirydate'])));
								if($membermembership['Membermembership']['expirydate']<=date("Y-m-d H:00:00"))
								{
									echo '<div>'.__('Expired').'</div>';
								}
								else
								{
									$diff_second = strtotime(date("Y-m-d H:00:00",strtotime($membermembership['Membermembership']['expirydate'])))-strtotime(date("Y-m-d H:00:00",strtotime($membermembership['Membermembership']['purchasedate'])));
									$complete_second = strtotime(date("Y-m-d H:i:s"))-strtotime(date("Y-m-d H:00:00",strtotime($membermembership['Membermembership']['purchasedate'])));
									$mid=$membermembership['Membership']['id'];
									$totalhours=floor(($diff_second-$complete_second)/3600);
									$daysorhours='days';
									if($totalhours<24)
										$daysorhours='hours';
									?>
									<script type="text/javascript">
									var temptime<?php echo $mid;?>=<?php echo round(($diff_second-$complete_second)*100/$diff_second,4);?>;
									var timername<?php echo $mid;?>;
									function nextProgress<?php echo $mid;?>(incs<?php echo $mid;?>)
									{
										temptime<?php echo $mid;?>=temptime<?php echo $mid;?>-incs<?php echo $mid;?>;
										if (temptime<?php echo $mid;?> <= 0)
										{
											clearTimeout(timername<?php echo $mid;?>);
											$('#progress_bar<?php echo $mid;?> .ui-progress').animateProgress(0, function() {}, <?php echo $mid;?>, '<?php echo $daysorhours;?>', <?php echo $diff_second;?>, 0<?php echo $complete_second;?>);
										}
										else
										{
										  $('#progress_bar<?php echo $mid;?> .ui-progress').animateProgress(temptime<?php echo $mid;?>, function() {}, <?php echo $mid;?>, '<?php echo $daysorhours;?>', <?php echo $diff_second;?>, 0<?php echo $complete_second;?>);
										}
									}
									function startprogress<?php echo $mid;?>(incrseconds<?php echo $mid;?>)
									{
										timername<?php echo $mid;?>=setInterval(nextProgress<?php echo $mid;?>, 1000, incrseconds<?php echo $mid;?>)
									}
									</script>
									<?php echo '<script type="text/javascript">$( document ).ready(function() {startprogress'.$mid.'('.round(100/($diff_second),8).');});</script>';?>
									<!-- Progress bar -->
									<div style="margin: 0 5px;" class="membershipdiffer">
									<?php $daysofhoursstr=ucfirst($daysorhours); ?>
									<div class="ui-remain<?php echo $mid;?> ui-remain-main"><?php echo __($daysofhoursstr).' '.__('remaining');?> : <b class="value">0</b></div>
									<div id="progress_bar<?php echo $mid;?>" class="ui-progress-bar ui-container" align="left">
										<div class="ui-progress" style="width: <?php echo round(100/($complete_second),4);?>%;">&nbsp;</div>
									</div>
									<span class="ui-label<?php echo $mid;?>"><b class="value">0%</b> <?php echo __('remaining'); ?></span>
									</div>
									<!-- /Progress bar -->
						  <?php }
							}
							else
							{
								$onetime[]=$membermembership['Membership']['id'];
								echo '-';
							}
							?>
						</div>
						<div class="divtd textcenter vam">
							<?php 
								if($membermembership['Membermembership']['autorenew'] == 1 && $membermembership['Membership']['membership_fee_type'] != "One Time")
								{
									$display++;
									echo __('ON').' '.$this->Form->checkbox('Membershipdata.', array(
									  'value' => $membermembership['Membership']['id'],
									  'type' => 'checkbox',
									  'style'=>'display:inline-block',
									  'div'=>false,
									  'hiddenField' => false
									));
								}
								else
								{
									echo __('OFF');
								}
							?>
						</div>
					</div>
				<?php $i++;endforeach; ?>
			</div>
		</div>
		<?php if(count($membermemberships)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
		<div class="formbutton">
			<?php
			if($display !=0)
			{
				echo $this->Js->submit(__('Stop Selected'), array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'class'=>'button',
				  'div'=>false,
				  'controller'=>'member',
				  'action'=>'membership',
				  'url'   => array('controller' => 'member', 'action' => 'membership')
				));
			}
			?>
		</div>
		<div class="clear-both"></div>
		<?php echo $this->Form->end();?>
		<?php // Current Memberships table ends here ?>
		
	</div>
<?php } ?>	
	
<?php // Current Balance code starts here ?>
<div class="textright"><a class="shbutton currentbalancebutton" href="javascript:void(0)" onclick="currentbalancebox('.balancebox', this,'<?php echo __("[+] Show Balance");?>','<?php echo __("[-] Hide Balance");?>');"><?php if(@$_COOKIE['curbal']==1)echo __("[+] Show Balance"); else echo __("[-] Hide Balance");?>
</a></div>
<div class="comisson-bg balancebox <?php if(@$_COOKIE['curbal']==1)echo 'hide';?>">
	<div class="text-ads-title-text"><?php echo __("Current Balance");?></div>
	<div class="text-ads-title-text-right"></div>
	<div class="clear-both"></div>
</div>
<div id="balancebox" class="main-box-eran balancebox <?php if(@$_COOKIE['curbal']==1)echo 'hide';?>">
	<?php if($SITECONFIG["balance_type"]==1){ ?>
		<div class="divtable">
                        <div class="divtr white-color">
                                <div class="divth textcente vam"><?php echo __("Cash Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCash*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
                        <div class="divtr gray-color">
                                <div class="divth textcente vam"><?php echo __("Earning Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberEarning*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php } ?>
                        <div class="divtr white-color">
                                <div class="divth textcente vam"><?php echo __("Re-purchase Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberRepurchaseCash*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
                        <div class="divtr gray-color">
                                <div class="divth textcente vam"><?php echo __("Commission Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCommission*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php } ?>
                </div>
		<?php }else{ 
			?>
			<div class="divtable">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam"><?php echo __("Payment Processor");?></div>
						<div class="divth textcenter vam"><?php echo __("Cash Balance");?></div>
                                                <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
						<div class="divth textcenter vam"><?php echo __("Earning Balance");?></div>
                                                <?php } ?>
						<div class="divth textcenter vam"><?php echo __("Re-purchase Balance");?></div>
                                                <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
						<div class="divth textcenter vam"><?php echo __("Commission Balance");?></div>
                                                <?php } ?>
					</div>
				</div>
				<?php $i=1;
				foreach($MemberCash as $proc_name=>$cash)
				{
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $proc_name;?> :</div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCash[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberEarning[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php } ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberRepurchaseCash[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCommission[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php } ?>
					</div>
					<?php
				$i++; }?>
			</div>
	<?php } ?>
</div>
<?php // Current Balance code ends here ?>

	
	<div class="comisson-bg">
		<div class="text-ads-title-text"><?php echo __("Available Memberships");?></div>
		<div class="clear-both"></div>
	</div>
	<div class="main-box-eran">
		
		<?php // Available Memberships table starts here ?>
		<?php echo $this->Form->create('Membership',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'member','action'=>'membershipupgrateaction')));?>
		
		<div class="divtable textcenter">	
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam"><?php echo __("Name");?></div>
					<div class="divth textcenter vam"><?php echo __("Fees Type");?></div>
					<div class="divth textcenter vam"><?php echo __("Price");?></div>
					<div class="divth textcenter vam"><?php echo __("Additional Commission");?></div>
					<div class="divth textcenter vam" style="min-width: 100px;"><?php echo __("Action");?></div>
				</div>
			</div>
			<div class="divtbody">
				<?php $i=1;
				foreach ($membership as $memberships):
				if(!in_array($memberships['Membership']['id'],$onetime)){
				if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $memberships['Membership']['membership_name'];?></div>
						<div class="divtd textcenter vam"><?php echo ($memberships['Membership']['membership_fee_type']=='Days') ? __('Every').' '.$memberships['Membership']['membership_fee_value'].' '.__('Days') : __($memberships['Membership']['membership_fee_type']);?></div>
						<div class="divtd textcenter vam mprice<?php echo $memberships['Membership']['id']; ?>"><?php echo $Currency['prefix'];?><?php echo round($memberships['Membership']['price']*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
						<div class="divtd textcenter vam">
							<?php
							$commissionlevel = @explode(",",$memberships['Membership']['addcommissionlevel']);
							$add_commission_on_feeslevel = @explode(",",$memberships['Membership']['add_commission_on_fees']);
							if($commissionlevel[0]!=0 || $add_commission_on_feeslevel[0]!=0)
							{
								if($commissionlevel[0]!=0)
								{
									echo __('Comm. on Purchase')." : ";
									foreach($commissionlevel as $key=>$value)
									{
										if($value>0) echo $value."% ";
									}
								}
								if($add_commission_on_feeslevel[0]!=0)
								{
									if($commissionlevel[0]!=0)
										echo "<br>";
									echo __('Comm. on Membership Purchase')." : ";
									foreach($add_commission_on_feeslevel as $key=>$value)
									{
										if($value>0) echo $value."% ";
									}
								}
							}
							else
							{
								echo __("N/A");
							}
							?>
						</div>
						<div class="divtd textcenter vam">
				       <?php
					       echo $this->Js->link('<span class="purchasebutton">'.__('Purchase').'</span>', array('controller'=>'member', "action"=>"purchasemembership/".$memberships['Membership']['id']), array(
						       'update'=>'#memberpage',
						       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						       'escape'=>false,
						       'class'=>'vtip',
						       'title'=>__('Purchase')
					       ));
				       ?>
				       </div>
					</div>
				<?php } ?>
				<?php $i++;endforeach; ?>
			</div>
		</div>
		<?php if(count($membership)==0 || count($membership)==count($onetime)) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
		<div class="membershifield"></div>
		<div class="clear-both"></div>
		<?php echo $this->Form->end();?>
		<?php // Available Memberships table ends here ?>
		
	</div>
<div class="height10"></div>
</div><!--#memberpage over-->
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>