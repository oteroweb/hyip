<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<script>
function ViewDescription(atag)
{
	$("a[rel='lightboxtext']").colorbox({width:"80%", height:"80%", inline:true, href:"#"+atag.title});
}
</script>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List#Sent_Emails" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<div class="height10"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Emaillog',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'mdemaillog/'.$member_id)));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
							echo $this->Form->input('searchby', array(
							      'type' => 'select',
							      'options' => array('all'=>'Select Parameter', "email"=>'Email', "subject"=>'Subject'),
							      'selected' => $searchby,
							      'class'=>'',
							      'label' => false,
							      'style' => ''
						));?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span class="searchforfields_s"><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
                  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#memberdetailpage',
                  'class'=>'searchbtn',
                  'controller'=>'member',
                  'action'=>'mdemaillog/'.$member_id,
                  'url'=> array('controller' => 'member', 'action' => 'mdemaillog/'.$member_id)
                ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
    
        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#memberdetailpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'member', 'action'=>'mdemaillog/'.$member_id)
        ));
        $currentpagenumber=$this->params['paging']['Emaillog']['page'];
        ?>

<div id="gride-bg">
    <div class="padding10">
				
	<?php echo $this->Form->create('Emaillog',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'mdemaillog/'.$member_id)));?>
	
		<div class="records records-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
        <div class="clear-both"></div>
		<div class="tablegrid">
				<div class="tablegridheader">
				    <div>
						<?php if($ordertype=="desc")$sorttype="asc";else $sorttype="desc"; ?>
                        <?php echo $this->Js->link('Sent Date', array('controller'=>'member', "action"=>"mdemaillog/".$member_id."/0/logdate/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#memberdetailpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Sent Date'
                        ));?>
                    </div>
				    <div>
                        <?php 
                            echo $this->Js->link('Email', array('controller'=>'member', "action"=>"mdemaillog/".$member_id."/0/email/".$sorttype."/".$currentpagenumber), array(
                                'update'=>'#memberdetailpage',
                                'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                'escape'=>false,
                                'class'=>'vtip',
                                'title'=>'Sort By Email'
                            ));?>
                    </div>
				    <div>
                        <?php echo $this->Js->link('Subject', array('controller'=>'member', "action"=>"mdemaillog/".$member_id."/0/subject/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#memberdetailpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Subject'
                        ));?>
                    </div>
				    <div>Email Content</div>
                </div>
                <?php foreach ($emaillogs as $emaillog):?>
                    <div class="tablegridrow">
						<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $emaillog['Emaillog']['logdate']); ?></div>
						<div><?php echo $emaillog['Emaillog']['email']; ?></div>
						<div><?php echo $emaillog['Emaillog']['subject']; ?></div>
						<div>
				            <a href="#" rel="lightboxtext" title="ViewDescription-<?php echo $emaillog['Emaillog']['id'];?>" onclick="ViewDescription(this)">
								<?php echo $this->html->image('search.png', array('alt'=>'Message', 'align'=>'absmiddle'));?>
							</a>
							<div style="display:none;">
								<div id="ViewDescription-<?php echo $emaillog['Emaillog']['id'];?>">
									<div>To : <?php echo $emaillog['Emaillog']['email']; ?></div>
									<div>Subject : <?php echo $emaillog['Emaillog']['subject']; ?></div>
									<hr>
									<div><?php echo stripslashes(nl2br($emaillog['Emaillog']['message']));?></div>
									</br>
								</div>
							</div>
						</div>
					</div>
                <?php endforeach; ?>
		</div>
		<?php if(count($emaillogs)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php echo $this->Form->end();
        if($this->params['paging']['Emaillog']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Emaillog',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'mdemaillog/'.$member_id.'/rpp')));?>
			<div class="resultperpage">
                <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#memberdetailpage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'member',
                  'action'=>'mdemaillog/'.$member_id.'/rpp',
                  'url'   => array('controller' => 'member', 'action' => 'mdemaillog/'.$member_id.'/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		
     </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>