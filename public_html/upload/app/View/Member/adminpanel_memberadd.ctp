<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<script type="text/javascript"> 
(function() {
$('#Memberadd').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();

$(document).ready( function() {
	$(".details-nav ul li a").click(function() {
		$(".details-nav ul li a").removeClass('active');
		$(this).addClass('active');
	});
});
</script>
<?php
if(!$tabmenu || !$ajaxnewtab){?>
<div class="whitetitlebox">Member / List</div>
<div id="memberpageTK">
<div id="memberpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<div class="height10"></div>
<div class="backgroundwhite nopadding">
    
<?php if(isset($memberdata['Member']["member_id"])){ ?>
<?php if(!$ajax) { ?>		
			<div class="tab-blue-box">
				<div id="tab">
					<ul class="nav nav-tabs" id="myTab">
						<li class="active">
							<?php echo $this->Js->link("Details", array('controller'=>'member', "action"=>"memberadd/".$memberdata['Member']["member_id"]), array(
								'update'=>'#memberdetailpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'active'
							));?>
						</li>
						<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_mdposition', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link("Order", array('controller'=>'member', "action"=>"mdposition/".$memberdata['Member']["member_id"]), array(
									'update'=>'#memberdetailpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
						<?php } ?>
						<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_withdrawhistory', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link("Withdrawals", array('controller'=>'member', "action"=>"withdrawhistory/".$memberdata['Member']["member_id"]), array(
									'update'=>'#memberdetailpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
						<?php } ?>
						<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_cash_by_admin', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link("Balance Updates", array('controller'=>'member', "action"=>"cash_by_admin/".$memberdata['Member']["member_id"]), array(
									'update'=>'#memberdetailpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
						<?php } ?>
						<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_mdreferral', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link("Referrals", array('controller'=>'member', "action"=>"mdreferral/".$memberdata['Member']["member_id"]), array(
									'update'=>'#memberdetailpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
						<?php } ?>
						<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_commission', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link("Commissions/Bonus", array('controller'=>'member', "action"=>"commission/".$memberdata['Member']["member_id"]), array(
									'update'=>'#memberdetailpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
						<?php } ?>
						<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_mdstat', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link("Stats", array('controller'=>'member', "action"=>"mdstat/".$memberdata['Member']["member_id"]), array(
									'update'=>'#memberdetailpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
						<?php } ?>
						<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_memberactivity', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link("Activity Log", array('controller'=>'member', "action"=>"memberactivity/".$memberdata['Member']["member_id"]), array(
									'update'=>'#memberdetailpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
						<?php } ?>
						<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_mdemaillog', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link("Sent Emails", array('controller'=>'member', "action"=>"mdemaillog/".$memberdata['Member']["member_id"]), array(
									'update'=>'#memberdetailpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
						<?php } ?>
						<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_mdsendmail', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link("Send Message", array('controller'=>'member', "action"=>"mdsendmail/".$memberdata['Member']["member_id"]), array(
									'update'=>'#memberdetailpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
						<?php } ?>
						<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_mdmakeorder', $SubadminAccessArray)){ ?>
							<li>
								<?php echo $this->Js->link("Add Order", array('controller'=>'member', "action"=>"mdmakeorder/".$memberdata['Member']["member_id"]), array(
									'update'=>'#memberdetailpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>''
								));?>
							</li>
						<?php } ?>
						<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_login', $SubadminAccessArray)){ ?>
							<li style="border-right:none;"><a class="vtip" onclick="return confirm('Take a Note That Member Logs Will Not Be Created When You Log in From Here. Continue?');" title="Login to <?php echo $memberdata['Member']['user_name']."'s"; ?> Member Area" target="_blank" href="<?php echo $ADMINURL;?>/member/login/<?php echo $memberdata['Member']['member_id'];?>">Login</a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
	<?php }?>
	
		<div id="memberdetailpage">
	<?php } ?>
	<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List" target="_blank">Help</a></div>
	<div id="UpdateMessage"></div>
	<div class="height10"></div>
	<?php echo $this->Form->create('Member',array('type' => 'post','id' => 'Memberadd','novalidate' => 'novalidate', 'type' => 'file', 'onsubmit' => 'return true;','url'=>array('controller'=>'member','action'=>'memberaddaction')));?>
	<?php if(isset($memberdata['Member']["member_id"])){
		echo $this->Form->input('member_id', array('type'=>'hidden', 'value'=>$memberdata['Member']["member_id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		echo $this->Form->input('current_banner_credits', array('type'=>'hidden', 'value'=>$memberdata['Member']["banner_credit"], 'label' => false));
		echo $this->Form->input('current_text_credits', array('type'=>'hidden', 'value'=>$memberdata['Member']["txtadd_credit"], 'label' => false));
		echo $this->Form->input('current_solo_credits', array('type'=>'hidden', 'value'=>$memberdata['Member']["soloads_credit"], 'label' => false));
	}?>
	<?php echo $this->Form->input('userlimit', array('type'=>'hidden', 'value'=>$SITECONFIG["usernamelimit"], 'label' => false));?>				
	<div class="adminovimain">
		<div class="frommain">
			<?php if($memberdata['Member']["isdelete"]==1) { ?>
			<div class="whitenoteboxinner"><b><span class="red-color"><?php echo "This Member Deleted";?></span></b></div>
			<?php } ?>
			<div class="ovititle">Basic Details</div>	
			<?php if(isset($memberdata['Member']["member_id"])){?>
				<div class="oviprofilebg">
					  <div>
						<?php
						if($memberdata['Member']['member_photo']==NULL || $memberdata['Member']['member_photo']=='')
						{
							echo $this->html->image("member/dummy.jpg", array('class'=>'profilepicture','alt'=>'','id'=>'memberimg','width'=>'69','height'=>'69'));
						}
						else
						{
							echo $this->html->image("member/".$memberdata['Member']['member_photo'].'?tk='.date('sHi'), array('class'=>'profilepicture','alt'=>'','id'=>'memberimg','width'=>'69','height'=>'69'));
						}
						?>
					  </div>
					  <div class="ovibrose">
					      <div class="btnorange browsebutton">
					
						Browse<?php echo $this->Form->input('member_photo', array('type'=>'file', 'label' => false, 'div' => false,'class'=>'browserbuttonlink','onchange'=>'$("#browservalue").html(this.value);'));?>
					      </div>
					  </div>
				  </div>
			<?php } ?>
			
			<div class="oviprofileright">
				<div class="ovinotetext">Admin Note :</div>
				<div class="admintopfrom">
					<?php echo $this->Form->input('note', array('type'=>'textarea', 'value'=>stripslashes($memberdata['Member']["note"]), 'label' => false, 'class'=>'from-textarea'));?>
				</div>
			</div>
			
			<div class="fromnewtext2">First Name :</div>
			<div class="fromnewtext2">Last Name :</div>
			
			<div class="fromovi">
			     <?php echo $this->Form->input('f_name', array('type'=>'text', 'value'=>stripslashes($memberdata['Member']["f_name"]), 'label' => false, 'div' => false, 'class'=>'fromboxbghalf'));?>
			     <?php echo $this->Form->input('l_name', array('type'=>'text', 'value'=>stripslashes($memberdata['Member']["l_name"]), 'label' => false, 'div' => false, 'class'=>'fromboxbghalf'));?>
			</div>
			
			<div class="fromnewtext2">Status :</div>
			<div class="fromnewtext2">Member Group :</div> 
			<div class="fromovi">
				<div class="select-main2">
					<div class="select-main">
						<label>
						   <?php 
							  echo $this->Form->input('active_status', array(
									  'type' => 'select',
									  'options' => array('1'=>'Active', '0'=>'Inactive'),
									  'selected' => $memberdata['Member']["active_status"],
									  'class'=>'',
									  'label' => false,
									  'style' => ''
								  ));
							?>
						</label>
					</div>
				</div>
					   
				<div class="select-main2 borderpadding">
					<div class="select-main">
						<label>
							<?php
								$membergroup[0]="Select Member Group";
								ksort($membergroup);
								echo $this->Form->input('membergroup', array(
									'type' => 'select',
									'options' => $membergroup,
									'selected' => $memberdata['Member']["membergroup"],
									'class'=>'',
									'label' => false,
									'style' => ''
								));
							?>
						</label>
					</div>
				</div>
			</div>
			
			<div class="differentform">
			<?php if(isset($memberdata['Member']["member_id"])){?>     
			<div class="fromnewtext2">Member Id :</div>
			<?php } ?>
			<div class="fromnewtext2">Username :*</div>
			
			
				<?php if(isset($memberdata['Member']["member_id"]))
				{?>
					<div class="fromovi">
						<div class="fromovitextin"><?php echo $memberdata['Member']["member_id"];?></div>
						<div class="fromovitextin"><?php echo $memberdata['Member']["user_name"];?></div>
					</div>
				<?php
				}
				else
				{?>
					<div class="fromovi2 width100">
					<?php echo $this->Form->input('user_name', array('type'=>'text', 'value'=>$memberdata['Member']["user_name"], 'label' => false, 'div' => false, 'class'=>'fromboxbghalf'));?>
					</div>
					<?php
				}?>
			</div>
			    
			<div class="fromnewtext2">Password :*</div>
			<div class="fromnewtext2">Email :*</div>
			<div class="fromovi">
				<?php if(isset($memberdata['Member']["member_id"]) && $memberdata['Member']["member_id"]==1)
				{?>
					<div class="fromovitextin">Changeable From Settings => Admin Settings</div><?php
				} else
				{ ?>
					<?php echo $this->Form->input('password', array('type'=>'password', 'value'=>$memberdata['Member']["password"], 'label' => false, 'div' => false, 'class'=>'fromboxbghalf'));?><?php
				} ?>
				<?php echo $this->Form->input('email', array('type'=>'text', 'value'=>$memberdata['Member']["email"], 'label' => false, 'div' => false, 'class'=>'fromboxbghalf'));?>
			</div>
			
		</div>
		
	</div>
	<div class="adminovimain">
		<div class="ovititle">Other Details</div>
		<div class="frommain">
		    
		    <div class="fromnewtext2">Security Question :</div>
		    <div class="fromnewtext2">Security Answer :</div>
		    <div class="fromovi">
			<?php echo $this->Form->input('security_question', array('type'=>'text', 'value'=>stripslashes($memberdata['Member']["security_question"]), 'label' => false, 'div' => false, 'class'=>'fromboxbghalf'));?>
			<?php echo $this->Form->input('security_answer', array('type'=>'password', 'value'=>$memberdata['Member']["security_answer"], 'label' => false, 'div' => false, 'class'=>'fromboxbghalf'));?>
		    </div>
		      
		    <div class="fromnewtext2">Country :</div>
		    <div class="fromnewtext2">Contact No :</div>
		    <div class="fromovi">
			<div class="select-main2 formrightborder">
				<div class="select-main">
				     <label>
				       <?php
					   $countries[0]="Select Country";
						ksort($countries);
						echo $this->Form->input('country', array(
							'type' => 'select',
							'options' => $countries,
							'selected' => $memberdata['Member']["country"],
							'class'=>'',
							'label' => false,
							'style' => ''
						));
					?>
				     </label>
				</div>
			</div>
		       <?php echo $this->Form->input('contact_no', array('type'=>'text', 'value'=>$memberdata['Member']["contact_no"], 'label' => false, 'div' => false, 'class'=>'fromboxbghalf'));?>
		    </div>
		    
		    <div class="fromnewtext">Address :</div>
		    <div class="fromovi">
		      <?php echo $this->Form->input('address', array('type'=>'textarea', 'value'=>stripslashes($memberdata['Member']["address"]), 'label' => false, 'class'=>'from-textarea'));?>
		    </div>
		
		    <div class="fromnewtext2">Member Status</div>
		    <div class="fromnewtext2">Unsubscribe from Solo E-mails :</div>
		    <div class="fromovi">
			<div class="select-main2">
			     <div class="select-main">
				  <label>
					<?php 
						echo $this->Form->input('ispaid', array(
							'type' => 'select',
							'options' => array('0'=>'Unpaid', '1'=>'Paid'),
							'selected' => $memberdata['Member']["ispaid"],
							'class'=>'',
							'label' => false,
							'style' => ''
						));
					?>
				  </label>
			       </div>
			</div>
			<div class="select-main2 borderpadding">
			     <div class="select-main">
				  <label>
					<?php 
						echo $this->Form->input('solo_email', array(
							  'type' => 'select',
							  'options' => array('1'=>'Yes', '0'=>'No'),
							  'selected' => $memberdata['Member']["solo_email"],
							  'class'=>'',
							  'label' => false,
							  'style' => ''
						));
					?>
				  </label>
			       </div>
			</div>
		    </div>
		    
		    <div class="fromnewtext2">Member Representative</div>
		    <div class="fromnewtext2"></div>
		    <div class="fromovi">
			<div class="select-main2">
			     <div class="select-main">
				  <label>
					<?php 
						echo $this->Form->input('representative', array(
							'type' => 'select',
							'options' => array('0'=>'No', '1'=>'Yes'),
							'selected' => $memberdata['Member']["representative"],
							'class'=>'',
							'label' => false,
							'style' => ''
						));
					?>
				  </label>
			       </div>
			</div>
		    </div>
		
		    
		    <div class="fromnewtext2">Sponsor ID :</div>
		    <div class="fromnewtext2">Sponsor Name :</div>
		    <div class="fromovi">
		       <?php echo $this->Form->input('referrer', array('type'=>'text', 'value'=>$memberdata['Member']["referrer"], 'label' => false, 'div' => false, 'class'=>'fromboxbghalf', 'onblur'=>'GetSponsorName(this.value, "#usernamehtml", "'.$ADMINURL.'");'));?>
		       <?php echo $this->Form->input('member_referrer', array('type'=>'hidden', 'value'=>$memberdata['Member']["referrer"], 'label' => false)); ?>
		       <div class="fromovitextin fromovitextinwithheight"><div class="" id="usernamehtml"><?php if($memberdata['Member']["referrer"]>0)echo $referrername['Member']['user_name'];?></div></div>
		       
		    </div>
		    
			<?php if(isset($custom_fields) && count($custom_fields)>0)
			{  ?>
				<div class="differentform">
			    <div class="ovititle" style="padding-bottom: 0;padding-top: 15px;;font-size: 18px;">Custom Fields</div>
			    <?php 
				    $setvalue='';
				    foreach ($custom_fields as $custom_field):
					      $member_value_key=array_search($custom_field["Regform_manage"]["field_id"],$member_custom_fields);
					      if($member_value_key!==false) $setvalue=$member_custom_values[$member_value_key]; ?>
					      <div class="differentforminner">
							<div class="fromnewtext2"><?php echo $custom_field["Regform_manage"]["field_title"];?> :</div>
							<div class="fromovi">
								<?php if($custom_field["Regform_manage"]["field_type"]=="textbox"){ ?>
								<input type="textbox" name="<?php echo 'custom['.$custom_field['Regform_manage']['field_id'].']'; ?>" class="fromboxbghalf" value="<?php echo stripslashes($setvalue);?>"  >
				<?php } else { ?>
								<textarea name="<?php echo "custom[".$custom_field["Regform_manage"]["field_id"]."]"; ?>" class="from-textarea"><?php echo stripslashes($setvalue);?></textarea>
								<?php } ?>
							</div>
					      </div>
				    <?php $setvalue='';
				    endforeach;?>
				</div>
				    <?php
			}?>
			
			<div class="differentform"><div class="ovititle" style="padding-bottom: 0;padding-top: 15px;;font-size: 18px;">Ads Credit</div></div>	
			<div class="fromnewtext2">Text Ad :</div>
			<div class="fromnewtext2">Banner Ad :</div>
			
			<div class="fromovi">
				 <?php echo $this->Form->input('txtadd_credit', array('type'=>'text', 'value'=>stripslashes($memberdata['Member']["txtadd_credit"]), 'label' => false, 'div' => false, 'class'=>'fromboxbghalf'));?>
				 <?php echo $this->Form->input('banner_credit', array('type'=>'text', 'value'=>stripslashes($memberdata['Member']["banner_credit"]), 'label' => false, 'div' => false, 'class'=>'fromboxbghalf'));?>
			</div>
			
			<div class="fromnewtext2">Solo Ad :</div>
			<div class="fromnewtext2">Web Credits</div>
			<div class="fromovi">
				 <?php echo $this->Form->input('soloads_credit', array('type'=>'text', 'value'=>stripslashes($memberdata['Member']["soloads_credit"]), 'label' => false, 'div' => false, 'class'=>'fromboxbghalf'));?>
				  <?php echo $this->Form->input('webcredits', array('type'=>'text', 'value'=>stripslashes($memberdata['Member']["webcredits"]), 'label' => false, 'div' => false, 'class'=>'fromboxbghalf'));?>
			</div>
			
		</div>
	</div>
	
	
	<div class="adminovimain">
		<div class="ovititle">Processor Account Details</div>
		<div class="frommain">
			<?php
			if($SITECONFIG['balance_type']==1)
			{?>
				<div class="fromovi">
					<div class="select-main2 formrightborder">
						<div class="select-main">
							<label>
								<?php echo $this->Form->input('payment_processor', array(
									  'type' => 'select',
									  'options' => $paymentprocessors,
									  'selected' => $memberdata['Member']['payment_processor'],
									  'class'=>'',
									  'label' => false,
									  'div' => false
								));?>
							</label>
						</div>
					</div>
				
				<?php echo $this->Form->input('pro_acc_id', array('type'=>'text', 'value'=>$memberdata['Member']["pro_acc_id"], 'label' => false,'div' => false,'style' => 'width:152px', 'class'=>'fromboxbghalf'));?>
				</div>
				<?php
			}
			elseif($SITECONFIG['balance_type']==2)
			{?>
				
					<div class="ovileft">
					    <div class="ovisubtitletext">Name</div>
					</div>
					<div class="ovimiddle">
					  <div class="ovisubtitletext">Account</div>
					</div>
					<div class="oviright">
					  <div class="ovisubtitletext">Preferred</div>
					</div>
			
				<?php
				$checked=TRUE;
				foreach($paymentprocessors as $proid=>$proname)
				{
					?>
					
					<div class="ovileft">
					    <?php echo $proname;?> : 	
					</div>
					<div class="ovimiddle">
					  <input name="<?php echo "processor[".$proid."]"; ?>" title="<?php echo $proname;?>" class="ovifrombox" type="text" value="<?php echo @$memberdata['Member']['processor_'.$proid];?>" />
					</div>
					<div class="oviright">
					  <div class="rediobtn">
					      <div class="retbtn">
						<div class="rediobox">
							<?php 
							echo $this->Form->radio('priority_processor', array($proid=>''), array('legend' => false, 'value' => false, 'label' => true, 'checked'=>$checked));
							if($memberdata['Member']['priority_processor']==$proid)
								$checked=FALSE;
							?>
							
						  
						 </div>
					      </div>
					  </div>
					</div>  
					
					<?php
				}
				?>  
			<?php
			}?>
		</div>
	</div>
	
	<div class="adminovimain">
		<div class="ovititle">Processor Balances</div>
		<div id="tab2">
			<ul class="nav nav-tabs">
			    <li class="active"><a href="#cashbalance" data-toggle="tab">  Cash Balance ($) </a></li>
			    <li><a href="#repurchasebalance" data-toggle="tab">Re-purchase Balance ($)</a></li>
			    <?php if($SITECONFIG["wallet_for_earning"] != 'cash'){ ?>
			    <li><a href="#earningbalance" data-toggle="tab">Earning Balance ($)</a></li>
			    <?php } if($SITECONFIG["wallet_for_commission"] != 'cash'){ ?>
			    <li><a href="#commissionbalnace" data-toggle="tab">Commission Balance ($)</a></li>
				<?php }?>
			</ul>
		</div>
		<div class="tab-content tab-content2">
			<div class="tab-pane active" id="cashbalance">
				
				<?php if($SITECONFIG['balance_type']==1)
				{ ?>
					<div class="ovileft">Cash Balance ($) :</div>
					<div class="ovipayment">
						<?php echo $this->Form->input('cash_balance', array('type'=>'text', 'value'=>$memberdata['Member']["cash_balance"], 'label' => false, 'div' => false, 'class'=>'ovifrombox'));?>
					</div>
				<?php
				}
				elseif($SITECONFIG['balance_type']==2)
				{ ?>
					<div class="ovileft">
						<div class="ovisubtitletext">Processor</div>
					</div>
					<div class="ovimiddle">
						<div class="ovisubtitletext">Balance</div>
					</div>
					
					<?php
					foreach($paymentprocessors as $proid=>$proname)
					{
						?>
						<div class="ovileft"><?php echo $proname;?> :</div>
						<div class="ovipayment">
							<input name="<?php echo "cash[".$proid."]"; ?>" title="<?php echo $proname;?>" class="ovifrombox" type="text"  value="<?php echo @$memberdata['Member']['cash_'.$proid];?>" />
						</div>
						<?php
					}
					?>
				<?php
				}?>
				
			</div>
			
			<div class="tab-pane" id="repurchasebalance">
				<?php if($SITECONFIG['balance_type']==1)
				{ ?>
					<div class="ovileft">Purchase Balance ($) :</div>
					<div class="ovipayment">
						<?php echo $this->Form->input('repurchase_balance', array('type'=>'text', 'value'=>$memberdata['Member']["repurchase_balance"], 'label' => false, 'div' => false, 'class'=>'ovifrombox'));?>
					</div>
				<?php
				}
				elseif($SITECONFIG['balance_type']==2)
				{ ?>
					<div class="ovileft">
						<div class="ovisubtitletext">Processor</div>
					</div>
					<div class="ovimiddle">
						<div class="ovisubtitletext">Balance</div>
					</div>
					
					<?php
					foreach($paymentprocessors as $proid=>$proname)
					{
						?>
						<div class="ovileft"><?php echo $proname;?> :</div>
						<div class="ovipayment">
							<input name="<?php echo "repurchase[".$proid."]"; ?>" title="<?php echo $proname;?>" class="ovifrombox" type="text" value="<?php echo @$memberdata['Member']['repurchase_'.$proid];?>" />
						</div>
						<?php
					}
					?>
				<?php
				}?>
			</div>
			
			<div class="tab-pane" id="earningbalance">
				<?php if($SITECONFIG['balance_type']==1)
				{ ?>
					<div class="ovileft">Earning Balance ($) :</div>
					<div class="ovipayment">
						<?php echo $this->Form->input('earning_balance', array('type'=>'text', 'value'=>$memberdata['Member']["earning_balance"], 'label' => false, 'div' => false, 'class'=>'ovifrombox'));?>
					</div>
				<?php
				}
				elseif($SITECONFIG['balance_type']==2)
				{ ?>
					<div class="ovileft">
						<div class="ovisubtitletext">Processor</div>
					</div>
					<div class="ovimiddle">
						<div class="ovisubtitletext">Balance</div>
					</div>
					
					<?php
					foreach($paymentprocessors as $proid=>$proname)
					{
						?>
						<div class="ovileft"><?php echo $proname;?> :</div>
						<div class="ovipayment">
							<input name="<?php echo "earning[".$proid."]"; ?>" title="<?php echo $proname;?>" class="ovifrombox" type="text" value="<?php echo @$memberdata['Member']['earning_'.$proid];?>" />
						</div>
						<?php
					}
					?>
				<?php
				}?>
			</div>
			<div class="tab-pane" id="commissionbalnace">
				<?php if($SITECONFIG['balance_type']==1)
				{ ?>
					<div class="ovileft">Commission Balance ($) :</div>
					<div class="ovipayment">
						<?php echo $this->Form->input('commission_balance', array('type'=>'text', 'value'=>$memberdata['Member']["commission_balance"], 'label' => false, 'div' => false, 'class'=>'ovifrombox'));?>
						
					</div>
				<?php
				}
				elseif($SITECONFIG['balance_type']==2)
				{ ?>
					<div class="ovileft">
						<div class="ovisubtitletext">Processor</div>
					</div>
					<div class="ovimiddle">
						<div class="ovisubtitletext">Balance</div>
					</div>
					
					<?php
					foreach($paymentprocessors as $proid=>$proname)
					{
						?>
						<div class="ovileft"><?php echo $proname;?> :</div>
						<div class="ovipayment">
							<input name="<?php echo "commission[".$proid."]"; ?>" title="<?php echo $proname;?>" class="ovifrombox" type="text" value="<?php echo @$memberdata['Member']['commission_'.$proid];?>" />
						</div>
						<?php
					}
					?>
				<?php
				}?>
			</div>
	       </div>
	</div>
			  
	
	<div class="adminovimain" style="background-color: transparent;border-top:none;padding: 0px;padding-right: 20px;">		  
	<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_memberupdate', $SubadminAccessArray)){ ?>
	<?php echo $this->Form->submit('Submit', array(
		'class'=>'btnorange',
		'div'=>false
	));?>
	<?php } ?>
	<?php echo $this->Js->link("Back", array('controller'=>$contr, "action"=>$act), array(
		'update'=>'#'.$div,
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'escape'=>false,
		'class'=>'btngray'
	));?>
	</div>
	<?php echo $this->Form->end();?>
	</div>
	
	


<?php if(!$tabmenu || !$ajaxnewtab){?>
</div>
</div><!--#memberpage over-->
</div><!--#memberpageTK over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>