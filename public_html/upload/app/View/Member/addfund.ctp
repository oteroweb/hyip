<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 18-11-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<script type="text/javascript">
eraseCookie('verifycookie');
setCookie('verifycookie',<?php echo $this->Session->read('coockval');?>,1);
</script>
<?php echo $this->Javascript->link('allpage');?>
<div id="memberpage">

<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<script type="text/javascript">
(function() {
$('#MemberAddFundPage').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();
</script>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Add Funds");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php if(isset($IsAddFundLimitOver) && $IsAddFundLimitOver){
		echo __('Admin has placed add fund limits in the site. You cannot invest more than following amount').' : '.$Currency['prefix'].round($membercaninvest*$Currency['rate'],4)." ".$Currency['suffix']; ?><br /><br /><br />
		<div class="floatleft">
			<?php echo $this->html->link(__("Back"), array('controller' => 'member', 'action' => 'addfund'), array('class'=>'button'));?>
		</div>
		<div class="floatright"></div>
		<div class="clear-both"></div>
	<?php }elseif(count($paymentprocessors)==0){
		echo __('Add Fund is disabled by Administrator'); ?><br /><br /><br />
	<?php }elseif(isset($IsJavascriptDisabled) && $IsJavascriptDisabled){?>
		
		<?php // Javascript disabled notice starts here ?>
		<?php 
		echo '<div class="form_header">'.__('Javascript is disabled in your browser. Please enable javascript and allow cookies to access this page.').'</div><br />';
		echo '<strong>'.__('Please follow these instructions to enable javascript').' :</strong><br /><br />';
		echo "<b>".__('Google Chrome')."</b>
			<ul>	
				<li>".__('Click the spanner icon on the browser toolbar.')."</li>
				<li>".__('Select')." <b>".__('Options')."</b>.</li>
				<li>".__('Click the')." <b>".__('Under the Hood')."</b> ".__('tab').".</li>
				<li>".__('Click')." <b>".__('Content Settings')."</b> ".__("in the 'Privacy section.'")."</li>
				<li>".__('Select')." <b>".__('Allow all sites to run JavaScript')."</b> ".__("in the 'JavaScript' section.")."</li>
			</ul>
			<b>".__('Mozilla Firefox')."</b>
			<ul>
				<li>".__('Select')." <b>".__('Tools')."</b> ".__('from the top menu.')."</li>
				<li>".__('Choose')." <b>".__('Options')."</b>.</li>
				<li>".__('Choose')." <b>".__('Content')."</b> ".__('from the top navigation.')."</li>
				<li>".__('Select the checkbox next to')." <b>".__('Enable JavaScript')."</b> ".__('and click')." <b>".__('OK')."</b>.</li>
			</ul>
			<b>".__('Internet Explorer')."</b>
			<ul>
				<li>".__('Select')." <b>".__('Tools')."</b> ".__('from the top menu.')."</li>
				<li>".__('Choose')." <b>".__('Internet Options')."</b>.</li>
				<li>".__('Click on the')." <b>".__('Security')."</b> ".__('tab').".</li>
				<li>".__('Click on')." <b>".__('Custom Level')."</b>.</li>
				<li>".__("Scroll down until you see the section labeled 'Scripting'.")."</li>
				<li>".__("Under 'Active Scripting,' select")." <b>".__('Enable')."</b> ".__('and click')." <b>".__('OK')."</b>.</li>
			</ul>
			<b>".__('Apple Safari')."</b>
			<ul>
				<li>".__('Open the')." <b>".__('Safari')."</b> ".__("menu on your browser's toolbar.")."</li>
				<li>".__('Choose')." <b>".__('Preferences')."</b>.</li>
				<li>".__('Choose')." <b>".__('Security')."</b>.</li>
				<li>".__('Select the checkbox next to')." <b>".__('Enable JavaScript')."</b>.</li></ul>";
		?>
		<br /><br /><br />
		<div class="floatleft">
			<?php echo $this->html->link(__("Back"), array('controller' => 'member', 'action' => 'addfund'), array('class'=>'button'));?>
		</div>
		<div class="floatright"></div>
		<?php // Javascript disabled notice ends here ?>
		
		<div class="clear-both"></div>
	<?php }else{?>
		<div class="form-box">
		
		<script> processer=new Array();
		<?php
		$firstproceser=0;
		foreach($Processorsextrafield as $proid=>$provalue)
		{
			if($firstproceser==0)
				$firstproceser =$proid;
			?>
			processer[<?php echo $proid; ?>]=<?php echo $provalue; ?>;
		<?php //echo '
		}?>
		processorextrafield(<?php echo $firstproceser; ?>,processer,"<?php echo $SITEURL;?>app/processorextrafield","Member",".extrafield");
		</script>
		<?php // Add Fund form starts here ?>
		<div class="height10"></div>
		<?php echo $this->Form->create('Member',array('id' => 'MemberAddFundPage', 'type' => 'post', 'type' => 'file', 'onsubmit' => 'return true;', 'autocomplete'=>'off','url'=>array('controller'=>'member','action'=>'addfundpay')));?>
		<div class="purchaseplanmain">
		<?php if($SITECONFIG['addfundminlimit']>0){ ?>
		<div class="form-row">
			<div class="form-col-1 form-text"><?php echo __("Minimum Add Fund Amount");?> : </div>
			<div class="form-col-2 form-text"><?php echo $Currency['prefix'];?><?php echo round($SITECONFIG['addfundminlimit']*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
		</div>
		<?php } if($SITECONFIG['addfundmaxlimit']>0){ ?>
		<div class="form-row">
			<div class="form-col-1 form-text"><?php echo __("Maximum Add Fund Amount");?> : </div>
			<div class="form-col-2 form-text"><?php echo $Currency['prefix'];?><?php echo round($SITECONFIG['addfundmaxlimit']*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
		</div>
		<?php } ?>
		<div class="form-row">
			<div class="form-col-1"><?php echo __("Amount");?> : <span class="required">*</span></div>
			<?php echo $this->Form->input('add_fund' ,array('type'=>'text', "class"=>"formtextbox",'onkeyup' => '$("#payableamount").html($(this).val());caladdfundamount();', 'label'=>false,'div'=>false));?>
		</div>
		<div class="form-row">
			<div class="form-col-1"><?php echo __("Payment Processor");?> : <span class="required">*</span></div>
			<div class="select-dropdown">
				<label>
					<?php echo $this->Form->input('payment_processor', array(
						  'type' => 'select',
						  'options' => $paymentprocessors,
						  'selected' => '',
						  'class'=>'searchcomboboxwidth paymentprocessor',
						  'label' => false,
						  'div' => false,
						  'onchange' => 'processorextrafield(this.value,processer,"'.$SITEURL.'app/processorextrafield","Member",".extrafield");caladdfundamount();'
					));?>
				</label>
			</div>
			
		</div>
		<div class='extrafield'></div>
		<?php // Amount calculation box starts here ?>
		<div class="purchaseplanamountdetails floatleft" style="width:49.5%">
			<div class="form-box">
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Amount");?> : </div>
					<div class="form-col-2 form-text"><?php echo $Currency['prefix'];?><span id="payableamount">0</span> <?php echo $Currency['suffix'];?></div>
				</div>
				
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Fees");?> : </div>
					<div class="form-col-2 form-text"><span id="procfee">0</span>% + <?php echo $Currency['prefix'];?><span id="procfeeamount">0</span> <?php echo $Currency['suffix'];?></div>
				</div>
				
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Final Amount");?> : </div>
					<div class="form-col-2 form-text"><?php echo $Currency['prefix'];?><span id="finalamount"><?php echo $payamount*$qty;?></span> <?php echo $Currency['suffix'];?></div>
				</div>
			</div>	
		</div>
		<?php
			$procfee_string = "var procfee_array = new Array();var procfeeamount_array = new Array();";
			foreach($processorsfee as $key=>$value)
			{
				$procfee_string.=" procfee_array[".$key."] = ".$value['fee'].";";
				$procfee_string.=" procfeeamount_array[".$key."] = ".$value['feeamount'].";";
			}
		?>
		<script type='text/javascript'><?php echo $procfee_string;?> caladdfundamount();</script>
		<?php // Amount calculation box ends here ?>
		<div class="purchaseplanbuttonbox floatright" style="width:50%;">
			<?php if($MemberAddFundCaptcha){ ?>
			<div class="form-box">
				  <div class="form-row captchrow">
					<div class="form-col-1 forcaptchaonly"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
						<?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formcapthcatextbox",'div'=>false, 'label'=>false));?>
						<span style="margin-left: 15px;" class="fontsemibold"><?php echo __('Code')?> :</span>
						<?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'MemberAddFundCaptcha','vspace'=>2, "style"=>"vertical-align: middle", 'width'=>'118', 'height'=>'44')); ?> 
						<a href="javascript:void(0);" onclick="javascript:document.images.MemberAddFundCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "style"=>"vertical-align: middle"));?></a>
				  </div>
			</div>
			<?php }?>
			<?php if($SITECONFIG["enableagree"]==1){?>
			<div class="form-box">
				<div class="form-row captchrow" style="background-color: #ffffff;">
					<div class="profile-bot-left inlineblock" style="font-size: 12px;"><?php echo $this->Form->input('termsandconditions', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>'')); echo ' <a href="'.$SITEURL.'public/terms" target="_blank">'.__('I agree with the terms and conditions').'</a> '.__('of').' '.$SITECONFIG["sitetitle"]; ?></div>
				</div>
			</div>
			<?php } ?>
			<div class="formbutton">
				<?php echo $this->Form->submit(__('Submit'), array(
					'class'=>'button',
					'div'=>false,
					'onclick'=>'return true;'
				));?>
			</div>
		</div>	
		<div class="clear-both"></div>
		</div>
		<?php echo $this->Form->end();?>
		<?php // Add Fund form ends here ?>
		
	<?php if(count($Pending_deposits)>0){ ?>
	<div class="comisson-bg">
		<div class="text-ads-title-text"><?php echo __("Pending Bitcoin Requests");?></div>
		<div class="clear-both"></div>
	</div>
	<div class="main-box-eran">
		<div class="divtable">
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam"><?php echo __('Blockchain Payment Address');?></div>
					<div class="divth textcenter vam"><?php echo __('Amount');?></div>
				</div>
			</div>
			<div class="divtbody">
				<?php $i=1;
				foreach ($Pending_deposits as $Pending_deposit):
				$Pending_depositdata=explode('@',$Pending_deposit['Pending_deposit']['comment']);
				if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php if(isset($Pending_depositdata[1])){ echo $Pending_depositdata[1]; } ?></div>
						<div class="divtd textcenter vam"><?php if(isset($Pending_depositdata[0])){echo $Pending_depositdata[0]; echo __('BTC');} ?> </div>
					</div>
				<?php $i++;endforeach; ?>
			</div>
		</div>
	</div>
	<?php } ?>
	</div>
	<?php }?>
</div>
<div class="height10"></div>
</div><!--#memberpage over-->
<script type="text/javascript">if($("#MemberPaymentProcessor").val()==1)$(".bankwirefields").show(500);</script>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>