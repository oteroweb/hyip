<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 25-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>

<?php if(!$ajax) { ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="memberpage">
<?php } ?>
<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>

<?php // Top Menu starts here ?>
<div class="comisson-bg">
	<div class="commison-menu">
		<ul>
			<li>
				<?php
					echo $this->Js->link(__('My Referrals'), array('controller'=>'member', "action"=>"referrals"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>__('My Referrals')
					));
				?>
			</li>
			<li>
				<?php
					echo $this->Js->link(__('Referral Tracking'), array('controller'=>'member', "action"=>"referraltracking"), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip act',
						'title'=>__('Referral Tracking')
					));
				?>
			</li>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Top Menu ends here ?>

	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'referraltracking')
	));
	$currentpagenumber=$this->params['paging']['Referraltrack']['page'];
	?>
	
	<div class="main-box-eran">
		
		<div class="notetext"><?php echo __('You are a registered affiliate member.');?> <a href="<?php echo $SITEURL;?>member/commission"><?php echo __('View your commission structure'); ?></a></div>
		
		<div class="height15"></div>
		
		<?php // Referral Tracking table starts here ?>
		<div class="padding-left-serchtabal">
			<?php echo __('Total Referrals'); ?> : <?php echo $totalreferral;?>, <?php echo __('Total Paid Referrals'); ?> : <?php echo $totalpaidreferral;?>, <?php echo __('Paid Ratio');?> : <?php echo round($totalpaidreferral*100/$totalreferral,2);?>%, <?php echo __('Total Clicks');?> : <?php echo $this->params['paging']['Referraltrack']['count'];?>
		</div>
		<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			<div class="divtable">	
				<div class="divthead">	
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam">
							<?php
							if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
							echo $this->Js->link(__('Date'), array('controller'=>'member', "action"=>"referraltracking/0/dt/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Date')
							));?>
						</div>
						<div class="divth textcenter vam"><?php echo __('Site URL');?></div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('IP Address'), array('controller'=>'member', "action"=>"referraltracking/0/ip/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('IP Address')
							));?>
						</div>
					</div>
				</div>
				<div class="divtbody">
					<?php $i=1;
					foreach ($referraldata as $referral):
					if($i%2==0){$class='gray-color';}else{$class='white-color';}?>

						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $referral['Referraltrack']['dt']) ?></div>
							<div class="divtd textcenter vam"><?php echo $referral['Referraltrack']['url'];?></div>
							<div class="divtd textcenter vam"><?php echo $referral['Referraltrack']['ip'];?></div>
						</div>

					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($referraldata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // Referral Tracking table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Referraltrack']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Referraltrack',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member', 'action'=>'referraltracking/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#memberpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'member',
						  'action'=>'referraltracking/rpp',
						  'url'   => array('controller' => 'member', 'action' => 'referraltracking/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
            </div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
	
	</div>
<?php if(!$ajax) { ?>		
</div><!--#memberpage over-->
<?php } ?>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>