<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 10-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Member_List#Positions" target="_blank">Help</a></div>
<div class="tab-innar nomargin">
	<ul>
		<li>
			<?php echo $this->Js->link("Active Positions", array('controller'=>'member', "action"=>"mdposition/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'active'
			));?>
		</li>
		<li>
			<?php echo $this->Js->link("Advertisement Plan", array('controller'=>'member', "action"=>"mdadvertisementplan/".$member_id), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>''
			));?>
		</li>
	</ul>
</div>
<div class="positions-menu-drowp">
	<ul>
		<?php foreach($ActivePositionModules as $key=>$value){ if(ucfirst($PendingPositionModules[$key])==$ModuleType){ $class="act";}else{$class="";}?>
		<li>
			<?php echo $this->Js->link($value, array('controller'=>'member', "action"=>"mdposition/".$member_id."/".$key), array(
				'update'=>'#memberdetailpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>$class
			));?>
		</li>
		<?php }?>
	</ul>
</div>
<div class="clear-both"></div>
<div class="height21"></div>
<div id="UpdateMessage"></div>

	
<div class="serchmainbox">
	<div class="serchgreybox"><?php echo "Search Option";?></div>
	
	  <?php echo $this->Form->create('Pending_position',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'pendingposition')));?>
	  
	  <div class="from-box">
		<div class="fromboxmain">
			<span><?php echo "Search By";?> :</span>
			<span>                     
			    <div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php 
							echo $this->Form->input('searchby', array(
							      'type' => 'select',
							      'options' => array('all'=>'Select Parameter','amount'=>'Amount', 'processorid'=>'Payment Processor', 'fees'=>'Fees', 'planid'=>'Plan Id', 'ip_address'=>'IP Address'),
							      'selected' => $searchby,
							      'class'=>'',
							      'label' => false,
							      'style' => '',
										  'onchange'=>'if($(this).val()=="processorid"){generatecombo("data[Pending_position][proc_name]","Processor","id","proc_name","'.$searchfor.'","#searchforcombo","'.$ADMINURL.'","");$("#searchall").hide();$("#searchforcombo").show(500);} else{$("#searchforcombo").hide();$("#searchall").show(500);}'
							));
						?>
					</label>
				</div>
			    </div>
			 </span>
		</div>
		<div class="fromboxmain">
		    <span>Search For :</span>
		    <span id="searchall" style='display:<?php if($searchby=="processorid"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>''));?></span>
		    <span id="searchforcombo" style='display:<?php if($searchby!="processorid"){ echo "none";} ?>'></span>
		</div>
	</div>
	<div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
		</div>
		<div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker', 'style'=>''));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#memberdetailpage',
					'class'=>'searchbtn',
					'controller'=>'member',
					'action'=>'pendingposition',
					'url'=> array('controller' => 'member', 'action' => 'pendingposition/'.$member_id.'/'.$ModuleType)
				));?>
			</span>
		</div>
	</div>
	  
	  <?php echo $this->Form->end();?>
</div>






<div id="gride-bg">
<?php if($searchby=='processorid'){ ?>
<script>
	generatecombo("data[Pending_position][proc_name]","Processor","id","proc_name","<?php echo $searchfor; ?>","#searchforcombo","<?php echo $ADMINURL;?>","");
</script>
<?php } ?>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberdetailpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'pendingposition/'.$member_id."/".$ModuleType)
	));
	$currentpagenumber=$this->params['paging']['Pending_position']['page'];
	?>
    <div class="margin10">
		<div class="display-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
		<div class="clear-both"></div>
	</div>
    
	<?php echo $this->Form->create('Pending_position',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'pendingposition')));?>
	
	<div class="tablegrid">
		<div class="tablegridheader">
			<div>
				<?php
				if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
				echo $this->Js->link('Pay. Date', array('controller'=>'member', "action"=>"pendingposition/".$member_id."/".$ModuleType."/0/0/payment_date/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Payment Date'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Amount', array('controller'=>'member', "action"=>"pendingposition/".$member_id."/".$ModuleType."/0/0/amount/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Amount'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Paid Amount', array('controller'=>'member', "action"=>"pendingposition/".$member_id."/".$ModuleType."/0/0/paidamount/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Paid Amount'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Fees', array('controller'=>'member', "action"=>"pendingposition/".$member_id."/".$ModuleType."/0/0/fees/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Fees'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Processor', array('controller'=>'member', "action"=>"pendingposition/".$member_id."/".$ModuleType."/0/0/processor/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Processor'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Plan Id', array('controller'=>'member', "action"=>"pendingposition/".$member_id."/".$ModuleType."/0/0/planid/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Plan Id'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('Positions', array('controller'=>'member', "action"=>"pendingposition/".$member_id."/".$ModuleType."/0/0/position/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By Positions'
				));?>
			</div>
			<div>
				<?php echo $this->Js->link('IP Address', array('controller'=>'member', "action"=>"pendingposition/".$member_id."/".$ModuleType."/0/0/ip_address/".$sorttype."/".$currentpagenumber), array(
					'update'=>'#memberdetailpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'vtip',
					'title'=>'Sort By IP Address'
				));?>
			</div>
			<div align="center" valign="middle" width="50px;"><?php echo 'Notes';?></div>
                </div>
		<?php
			$i = 0;
			foreach ($pendingpositions as $pendingposition):
				$class = 'class="blue-color"';if ($i++ % 2 == 0){$class = 'class="white-color"';}
				?>
				<div class="tablegridrow">
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $pendingposition['Pending_position']['payment_date']); ?></div>
					<div>$<?php echo round($pendingposition['Pending_position']['amount'],4); ?></div>
					<div>$<?php echo round($pendingposition['Pending_position']['paidamount'],4); ?></div>
					<div>$<?php echo round($pendingposition['Pending_position']['fees'],4); ?></div>
					<div><?php echo $pendingposition['Pending_position']['processor']; ?></div>
					<div><?php echo $pendingposition['Pending_position']['planid']; ?></div>
					<div><?php echo $pendingposition['Pending_position']['position']; ?></div>
					<div><?php echo $pendingposition['Pending_position']['ip_address']; ?></div>
					<div><?php echo $this->html->image('information.png', array('alt'=>'Notes', 'class'=>'vtip', 'title'=>$pendingposition['Pending_position']['notes']));?></div>
				</div>
			<?php endforeach; ?>
			
        </div>
	
	<?php if(count($pendingpositions)==0){ echo '<div class="norecordfound">No records available</div>';}?>
	
	
	<div class="height5"></div>
	<div class="margin10">
		<?php echo $this->Form->end();
		if($this->params['paging']['Pending_position']['count']>$this->Session->read('pagerecord'))
		{?>
		<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
		<div class="floatleft margintop19">
			<?php echo $this->Form->create('Pending_position',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'pendingposition/rpp')));?>
			
			<div class="resultperpage">
				<label>
					<?php 
					echo $this->Form->input('resultperpage', array(
					  'type' => 'select',
					  'options' => $resultperpage,
					  'selected' => $this->Session->read('pagerecord'),
					  'class'=>'',
					  'label' => false,
					  'div'=>false,
					  'style' => '',
					  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
					));
					?>
				</label>
			</div>
			</div>
			<span id="resultperpageapply" style="display:none;">
				<?php echo $this->Js->submit('Apply', array(
				  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#memberdetailpage',
				  'class'=>'',
				  'div'=>false,
				  'controller'=>'member',
				  'action'=>'pendingposition/rpp',
				  'url'   => array('controller' => 'member', 'action' => 'pendingposition/'.$member_id.'/'.$ModuleType.'/rpp')
				));?>
			</span>
			<?php echo $this->Form->end();?>
		</div>
		<?php }?>
		<div class="floatright">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
		</ul>
		</div>
		<div class="clear-both"></div>
	</div>
	<div class="height7"></div>

</div>
<div class="height10"></div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>