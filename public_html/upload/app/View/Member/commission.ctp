<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 03-12-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(!$ajax) { ?>
<div id="memberpagemain">
<?php } ?>
<div id="memberpage">

<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>

<?php // Top menu code starts here ?>
<div class="earnings-history"><?php echo __('Commission Earnings');?></div>
<div class="comisson-bg mobilecss">
	<div class="commison-menu">
		<ul>
			<?php
            if($SITECONFIG["modules"] != ""){
			$modules=@explode(",",trim($SITECONFIG["modules"],","));
			foreach($modules as $module)
			{
				$modulearray=@explode(":", $module);
				if($modulearray[2]==1 && $modulearray[19]!="")
				{
					$subplanname=explode("-",$modulearray[13]);
					$subplanarray=explode("-",$modulearray[19]);
					$supid=0;
					foreach($subplanarray as $subplan)
					{
						if($subplan !='')
						{
							$class='';
							if($this->params['controller']==$modulearray[1] && $this->params['action']==$subplan)
								$class='act';
							echo '<li>';
							echo $this->Js->link(__($subplanname[$supid].' Earning History'), array('controller'=>$modulearray[1], "action"=>$subplan), array(
								'update'=>'#memberpagemain',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip '.$class,
								'title'=>__($subplanname[$supid].' Earning History')
							));
							echo '</li>';
						}
						$supid++;
					}
				}
			}
            }
			?>
			<li>
				<?php
					echo $this->Js->link(__('Commission Earnings'), array('controller'=>'member', "action"=>"commission"), array(
						'update'=>'#memberpagemain',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip act',
						'title'=>__('Commission Earnings')
					));
				?>
			</li>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Top menu code ends here ?>

<div class="main-box-eran">
<?php // Search box code starts here ?>
<div class="floatleft searchlabel"><?php echo __("Advanced Search");?></div>
	<div class="floatright"><a href="javascript:void(0)" onclick="togglebox('.searchlabel', this,'<?php echo __("[+] Show Search Box");?>','<?php echo __("[-] Hide Search Box");?>');"><?php echo __("[-] Hide Search Box");?></a></div>
	<div class="clear-both"></div>
	<div class="advsearchbox searchlabel">
	<?php echo $this->Form->create('Commission',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'member','action'=>'commission')));?>
		<div class="inlineblock vat">
			<div>
				<span class="searchboxcol1"><?php echo __("Search By");?>  :&nbsp;</span>
				<div class="select-dropdown smallsearch">
					<label>
						<?php echo $this->Form->input('searchby', array(
							'type' => 'select',
							'options' => array('all'=>__('All'), 'from_id'=>__('From').' '.__('Member Id'), 'from_member'=>__('From').' '.__('Member'), 'comm_on_member_fee'=>__('Comm. on Member Fees'), 'comm_on_position'=>__('Comm. on Positions'), 'bonus'=>__('Quickener'), 'matching'=>__('Matching Bonus')),
							'selected' => $searchby,
							'class'=>'',
							'label' => false,
							'div' => false,
							'style' => '',
							'onchange'=>'if($(this).val()=="comm_on_member_fee" || $(this).val()=="comm_on_position" || $(this).val()=="matching" || $(this).val()=="bonus"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);}'
						));?>
					</label>
				</div>
				
			</div>
			<div id="SearchFor" <?php if($searchby=="comm_on_position" || $searchby=="comm_on_member_fee" || $searchby=="matching" || $searchby=="bonus"){ echo 'style="display:none"';} ?>>
				<span class="searchboxcol1"><?php echo __("Search For");?> :&nbsp;</span>
				<?php echo $this->Form->input('searchfor', array('type'=>'text', 'id'=>'searchfor', 'value'=>$searchfor, 'label' => false, 'class'=>'finece-from-box searchfortxt','div' => false));?>	
			</div>
		</div>
		<div  class="inlineblock vat">
			<div class="margintb5">
				<span class="searchboxcol2"><?php echo __("From Date");?> :&nbsp;</span>
				<?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker searchdatetxt'));?>
			</div>
			<div>
				<span class="searchboxcol2"><?php echo __("To Date");?> :&nbsp;</span>
				<?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker searchdatetxt'));?>
			</div>
		</div>
		<div class="textcenter margint5">
			<?php echo $this->Js->submit(__('Update Results'), array(
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#memberpage',
				'class'=>'button floatnone',
				'div'=>false,
				'controller'=>'member',
				'action'=>'commission',
				'url'   => array('controller' => 'member', 'action' => 'commission')
			));?>
		</div>
	<?php echo $this->Form->end();?>
	</div>
	<?php // Search box code ends here ?>
	
	<div class="height10"></div>


<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'commission')
	));
	$currentpagenumber=$this->params['paging']['Commission']['page'];
	?>
		
		<?php // Commission Earnings table starts here ?>
		<div class="padding-left-serchtabal"></div>
		<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			<div class="divtable textcenter">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam">
							<?php 
							if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
							echo $this->Js->link(__('Date'), array('controller'=>'member', "action"=>"commission/0/tran_dt/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Date')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('From').' '.__('Member'), array('controller'=>'member', "action"=>"commission/0/from_id/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('From').' '.__('Member')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Amount'), array('controller'=>'member', "action"=>"commission/0/amount/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Amount')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Description'), array('controller'=>'member', "action"=>"commission/0/description/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Description')
							));?>
						</div>
					</div>
				</div>
				<div class="divtbody">
					<?php
					$i = 0;
					foreach ($commissions as $commission):
						if($i%2==0){$class='gray-color';}else{$class='white-color';}
						if($commission['Commission']['description']=='position') $description='Comm. on Positions';
						elseif($commission['Commission']['description']=='memberfee') $description='Comm. on Member Fees';
						elseif($commission['Commission']['description']=='bonus') $description='Quickener';
						elseif($commission['Commission']['description']=='matching') $description='Matching Bonus';
						else $description=ucwords($commission['Commission']['description']);
						?>
						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $commission['Commission']['tran_dt']); ?></div>
							<div class="divtd textcenter vam"><?php echo $commission['From_Member']['user_name'].'('.$commission['Commission']['from_id'].')'; ?></div>
							<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($commission['Commission']['amount']*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
							<div class="divtd textcenter vam"><?php echo __($description); ?></div>
						</div>
					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($commissions)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // Commission Earnings table ends here ?>
			
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Commission']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Commission',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'commission/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#memberpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'member',
						  'action'=>'commission/rpp',
						  'url'   => array('controller' => 'member', 'action' => 'commission/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
            </div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
	
</div>
</div>
<?php if(!$ajax) { ?>
</div>
<?php } ?>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>