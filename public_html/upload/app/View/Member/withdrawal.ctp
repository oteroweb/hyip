<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 22-10-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>

<?php echo $this->Javascript->link('allpage');?>
<div id="memberpagemain">
<?php if(!$ajax){?><?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>

<?php if(!(isset($IsShowSecurityQuestionAnswer) && $IsShowSecurityQuestionAnswer)) { ?>

<?php // Current Balance code starts here ?>
<div class="textright"><a class="shbutton currentbalancebutton" href="javascript:void(0)" onclick="currentbalancebox('.balancebox', this,'<?php echo __("[+] Show Balance");?>','<?php echo __("[-] Hide Balance");?>');"><?php if(@$_COOKIE['curbal']==1)echo __("[+] Show Balance"); else echo __("[-] Hide Balance");?>
</a></div>
<div class="comisson-bg balancebox <?php if(@$_COOKIE['curbal']==1)echo 'hide';?>">
	<div class="text-ads-title-text"><?php echo __("Current Balance");?></div>
	<div class="text-ads-title-text-right"></div>
	<div class="clear-both"></div>
</div>
<div id="balancebox" class="main-box-eran balancebox <?php if(@$_COOKIE['curbal']==1)echo 'hide';?>">
	<?php if($SITECONFIG["balance_type"]==1){ ?>
		<div class="divtable">
                        <div class="divtr white-color">
                                <div class="divth textcente vam"><?php echo __("Cash Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCash*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
                        <div class="divtr gray-color">
                                <div class="divth textcente vam"><?php echo __("Earning Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberEarning*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php } ?>
                        <div class="divtr white-color">
                                <div class="divth textcente vam"><?php echo __("Re-purchase Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberRepurchaseCash*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
                        <div class="divtr gray-color">
                                <div class="divth textcente vam"><?php echo __("Commission Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCommission*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php } ?>
                </div>
		<?php }else{ 
			?>
			<div class="divtable">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam"><?php echo __("Payment Processor");?></div>
						<div class="divth textcenter vam"><?php echo __("Cash Balance");?></div>
                                                <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
						<div class="divth textcenter vam"><?php echo __("Earning Balance");?></div>
                                                <?php } ?>
						<div class="divth textcenter vam"><?php echo __("Re-purchase Balance");?></div>
                                                <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
						<div class="divth textcenter vam"><?php echo __("Commission Balance");?></div>
                                                <?php } ?>
					</div>
				</div>
				<?php $i=1;
				foreach($MemberCash as $proc_name=>$cash)
				{
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $proc_name;?> :</div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCash[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberEarning[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php } ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberRepurchaseCash[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCommission[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php } ?>
					</div>
					<?php
				$i++; }?>
			</div>
	<?php } ?>
</div>
<?php // Current Balance code ends here ?>

<?php } ?>
<div class="height10"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Request Withdrawal");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
		<?php 
		$checkpoint_pass=0;
		if(isset($FreeMemberCanRequestWithdrawal) && !$FreeMemberCanRequestWithdrawal)
		{
			echo '<div class="purchaseerror">';
			echo __('Free members cannot request for a withdrawal. You must purchase from the site first. You can purchase positions or shares or adv packages among other things.');
			echo '</div>';
		}
		elseif(isset($IsMemberBlock) && $IsMemberBlock)
		{
			echo '<div class="purchaseerror">';
			echo __('You cannot request for a withdrawal as you are blocked now. Please try later.');
			echo '</div>';
		}
		elseif(isset($IsWithdrawDaysAccess) && !$IsWithdrawDaysAccess)
		{
			echo '<div class="purchaseerror">';
			echo __('New withdrawal requests are disabled for today.');
			echo '</div>';
		}
		elseif(isset($IsWithdrowRequestTodayLimitOver) && $IsWithdrowRequestTodayLimitOver)
		{
			echo '<div class="purchaseerror">';
			echo __('Member withdrawal requests limit per day is over today. Please visit tomorrow.');
			echo '</div>';
		}
		elseif(isset($perday_perweek) && !$perday_perweek)
		{
			if($perday_type)
			{
				echo '<div class="purchaseerror">';
				echo __('Maximum request amount allowed per day is over. Please come tomorrow.');
				echo '</div>';
			}
			else
			{
				echo '<div class="purchaseerror">';
				echo __('Maximum request amount allowed per week is over. Please come next week.');
				echo '</div>';
			}
		}
		elseif(isset($IsShowSecurityQuestionAnswer) && $IsShowSecurityQuestionAnswer)
		{
			if($this->Session->read('MemberQuestion')=='' || is_null($this->Session->read('MemberQuestion'))){ ?>
				<div class="note-box j-success-box padding-innar">
					<?php echo '&nbsp; &nbsp;'.__('Note : You have not set your security question yet. Please set it first to proceed with the withdrawal.'); ?> <?php echo $this->html->link(__("Click here"), array('controller' => 'member', 'action' => 'profile/security'));?>
				</div>
				<div class="height10"></div>
			<?php }
			// Security Question form starts here ?>
			<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'member','action'=>'withdrawal_'.$SITECONFIG['balance_type'])));
			if($SITECONFIG['balance_type']==2)
			{
				echo $this->Form->input('payment_processor', array('type'=>'hidden', 'value'=>$payment_processor, 'label' => false));
			}
			echo $this->Form->input('paymentmethod', array('type'=>'hidden', 'value'=>$paymentmethod, 'label' => false));
			?>
			<div class="formgrid">
				
				<div class="formgridrow">
					<div><?php echo __("Security Question");?> : </div>
					<div><?php echo $this->Session->read('MemberQuestion');?></div>
				</div>
				
				<div class="formgridrow contact-gray">
					  <div class="textleft vat"><?php echo __("Security Answer");?> : </div>
					  <div class="textleft vat">
							<?php echo $this->Form->input('security_answer', array('type'=>'password', 'value'=>'', 'label' => false, 'class'=>'login-from-box-1 keyboardInput'));?>
					  </div>
				</div>
			</div>
			<div class="floatright"> 
				<div class="" style="margin-top:10px;">
					<?php echo $this->Js->submit(__('Submit'), array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage',
						  'class'=>'add-new',
						  'div'=>false,
						  'controller'=>'member',
						  'action'=>'withdrawal_'.$SITECONFIG['balance_type'],
						  'url'   => array('controller' => 'member', 'action' => 'withdrawal_'.$SITECONFIG['balance_type'])
						));?>
				</div>
			</div>	
			<div class="clear-both"></div>
			<?php echo $this->Form->end();?>
			<?php // Security Question form ends here ?>
			
	</div>
			
			<?php echo $this->Form->end();?>
			<script>VKI_attach(document.getElementById('MemberSecurityAnswer'));</script>
			<?php
		}
		else
		{
			if(count($ProcessorAllowArray)>0)
			{
				$checkpoint_pass=1;
				if($SITECONFIG['balance_type']==1)
				{
					if(isset($Member_proc_status) && !$Member_proc_status)
					{
						$checkpoint_pass=0;
						echo __('Currently, payments for withdrawals through this payment processor are not supported. So, you are requested to change your current payment processor first.').'<br /><br />';
						if(count($ProcessorAllowArray)>0)
						{
							echo __('As of now, supported processors are');
							echo " : <br /><strong>";
							foreach ($ProcessorAllowArray as $key=>$value)
							{
								$key=$key+1;
								echo '('.$key.') ';
								echo $value . " ";
							}
							echo '</strong>';
						}
					}
					elseif(isset($MaximumWithdrawRequestAmountPerDay) && !$MaximumWithdrawRequestAmountPerDay)
					{
						$checkpoint_pass=0;
						echo $PaymentProcessor.' - '.__('Maximum request amount allowed per day is over. Please come tomorrow.');
					}
				}
				
				if($checkpoint_pass)
				{
					// Withdrawal form starts here
					?><script> processer=new Array();
					<?php
					$firstproceser=0;
					
					foreach($Processorsextrafield as $proid=>$provalue)
					{
						
						if($firstproceser==0)
							$firstproceser =$proid;
						?>
						processer[<?php echo $proid; ?>]=<?php echo $provalue; ?>;
					<?php }?>
						function extrafield(id)
						{
							if (id==processer[id]&& processer[id]==1)
							{
								$.ajax({
									url: 'withdrawalextrafield',
									type: 'POST',
									data: {id: id},
									beforeSend:function (XMLHttpRequest) {$("#pleasewait").fadeIn();},
									success: function(data) {
										$("#pleasewait").fadeOut();
										$('.extrafield').html(data);
									}
								});
							}
							else
							{
								$('.extrafield').html('');
							}
						}
						extrafield(<?php echo $firstproceser; ?>);
					</script>
					<?php
					echo $this->Form->create('Withdraw',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'member','action'=>'withdrawal')));?>
					<div class="withdrawbox">
					<div class="floatleft" style="width:49%;">
						<div class="purchaseplanmain withdrawtable">
							<div class="form-box">
								<div class="form-row">
									<div class="form-col-1"><?php echo __("Amount");?> : <span class="required">*</span></div>
									<?php echo $this->Form->input('amount', array('type'=>'text', 'value'=>'', 'label' => false, 'class'=>'formtextbox', 'id'=>'amount', 'style'=>'width:175px;','div'=>false, 'onkeyup'=>'calwithdrawamount()'));?>
								</div>  
								
								<?php if($SITECONFIG['balance_type']==1){ ?>
									<div class="form-row">
											<div class="form-col-1 form-text"><?php echo __("Payment Processor");?> : </div>
											<?php echo $PaymentProcessor;?>
											<?php echo $this->Form->input('paymentprocessor', array('type'=>'hidden', 'value'=>$Memberprocessorid, 'label' => false, 'class'=>'paymentprocessor', 'div'=>false));?>
									</div>
									<div class="form-row">
											<div class="form-col-1 form-text"><?php echo __("Payment Processor Id");?> : </div>
											<?php echo $PaymentProcessorId;?>
									</div>
								<?php }elseif($SITECONFIG['balance_type']==2){ ?>
									<div class="form-row">
										<div class="form-col-1"><?php echo __("Payment Processor");?> : </div>
										
										<div class="select-dropdown smallsearch" style="max-width: 180px;">
											<label>
												<?php echo $this->Form->input('payment_processor', array(
													  'type' => 'select',
													  'options' => $ProcessorAllowArray,
													  'selected' => '',
													  'class'=>'paymentprocessor',
													  'label' => false,
													  'div' => false,
													  'style' => '',
													  'onchange' => 'extrafield(this.value);calwithdrawamount();'
												));?>
											</label>
										</div>
										
									</div>
								<?php } ?>
								<div class='extrafield'></div>
								<div class="form-row">
									<div class="form-col-1 form-text"><?php echo __("Balance");?> : </div>
									<?php
										$paymentmethod=array();
										if(strpos($SITECONFIG["withdrawbalance"],'cash') !== false)
											$paymentmethod['cash']=__('Cash Balance');
										if(strpos($SITECONFIG["withdrawbalance"],'repurchase') !== false)
											$paymentmethod['repurchase']=__('Re-purchase Balance');
										if(strpos($SITECONFIG["withdrawbalance"],'earning') !== false)
											$paymentmethod['earning']=__('Earning Balance');
										if(strpos($SITECONFIG["withdrawbalance"],'commission') !== false)
											$paymentmethod['commission']=__('Commission Balance'); ?>
											
										<div class="select-dropdown smallsearch" style="max-width: 180px;">
											<label>	
												<?php echo $this->Form->input('paymentmethod', array(
													  'type' => 'select',
													  'options' => $paymentmethod,
													  'selected' => '',
													  'class'=>'paymentprocessor',
													  'label' => false,
													  'div' => false,
													  'style' => ''
												));?>
											</label>
										</div>
								</div>
								<div class="form-row">
									<div class="form-col-1 form-text"><?php echo __("Withdrawal Fees");?> : </div>
									<?php
										$procfee_string = "var procfee_array = new Array();var procfeeamount_array = new Array();";
										foreach($processorsfee as $key=>$value)
										{
											$procfee_string.=" procfee_array[".$key."] = ".$value['fee'].";";
											$procfee_string.=" procfeeamount_array[".$key."] = ".$value['feeamount'].";";
										}
									?>
									<script type='text/javascript'><?php echo $procfee_string;?>calwithdrawamount();</script>
									<span id="procfee">0</span>% + <?php echo $Currency['prefix'];?><span id="procfeeamount">0</span> <?php echo $Currency['suffix'];?>
								</div>
								
								<div class="form-row">
									<div class="form-col-1 form-text"><?php echo __("Receivable (after deducting fees)");?> : </div>
									<?php echo $Currency['prefix'];?><span id="totalamt">0</span> <?php echo $Currency['suffix'];?>
								</div>
							</div>	
						</div>
					</div>
					<div class="floatright" style="width:49%;">
						<div class="purchaseplanmain">
							<div class="form-box">
								<?php if($SITECONFIG["withdrawamountlimitperday"]>0){ ?>
								<div class="form-row">
									<div class="form-col-1 form-text" style="width:45%;"><?php echo __("Today Limit");?> : </div>
									<?php echo $Currency['prefix'];?><?php echo round($this->Session->read('allowed_amount_perday')*$Currency['rate'],2)." ".$Currency['suffix'];?>
								</div>
								<?php } if($SITECONFIG["withdrawamountlimitperweek"]>0){ ?>
								<div class="form-row">
									<div class="form-col-1 form-text" style="width:45%;"><?php echo __("Week Limit");?> : </div>
									<?php echo $Currency['prefix'];?><?php echo round($this->Session->read('allowed_amount_perweek')*$Currency['rate'],2)." ".$Currency['suffix'];?>
								</div>
								<?php } if($SITECONFIG["minsumofwidth"]>0){ ?>
								<div class="form-row">
									<div class="form-col-1 form-text" style="width:45%;"><?php echo __("Minimum Withdrawal Amount");?> : </div>
									<?php echo $Currency['prefix'];?><?php echo round($SITECONFIG["minsumofwidth"]*$Currency['rate'],4)." ".$Currency['suffix'];?>
								</div>
								<?php } if($SITECONFIG["maxsumofwidth"]){ ?>
								<div class="form-row">
									<div class="form-col-1 form-text" style="width:45%;"><?php echo __("Maximum Withdrawal Amount");?> : </div>
									<?php echo $Currency['prefix'];?><?php echo round($SITECONFIG["maxsumofwidth"]*$Currency['rate'],4)." ".$Currency['suffix'];?>
								</div>
								<?php } ?>
							</div>	
						</div>
					</div>	
					<div class="clear-both"></div>
					</div>
					<div class="formbutton"> 
						<?php echo $this->Js->submit(__('Submit'), array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage',
						  'class'=>'button',
						  'div'=>false,
						  'controller'=>'member',
						  'action'=>'withdrawal',
						  'url'   => array('controller' => 'member', 'action' => 'withdrawal')
						));?>
					</div>	
					<div class="clear-both"></div>
					<?php echo $this->Form->end();
					// Withdrawal form starts here
				}
			}
			else
			{
				echo __('Withdrawal is disabled by Administrator');
			}
		} ?>
		
	</div>
<?php } ?>
</div>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>