<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 13-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Member / Mass Mailing History</div>
<div id="memberpage">
<?php } ?>
<?php echo $this->Javascript->link('allpage');?>
<script>
function ViewDescription(atag)
{
	$("a[rel='lightboxtext']").colorbox({width:"80%", height:"80%", inline:true, href:"#"+atag.title});
}
</script>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Mass_Mailing_History" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'massmaillisthistory')
	));
	$currentpagenumber=$this->params['paging']['Massmail_history']['page'];
	?>
<div id="Xgride-bg">
    <div class="padding10 backgroundwhite">	
	<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'massmailhistoryremove')));?>
	<div class="greenbottomborder">
	<div class="height10"></div>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php echo $this->Form->checkbox('selectAllCheckboxes', array(
			'hiddenField' => false,
			'onclick' => 'selectAllCheckboxes("HistoryIds",this.checked);'
		));
		?>
		<label for="MemberSelectAllCheckboxes" ></label>
	</div>
	<div class="addnew-button">
		<?php if(!isset($SubadminAccessArray) || in_array('member',$SubadminAccessArray) || in_array('member/adminpanel_massmailhistoryremove',$SubadminAccessArray)){
			echo $this->Js->submit('Delete Selected Record(s)', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'escape'=>false,
			  'update'=>'#memberpage',
			  'class'=>'btnorange',
			  'div'=>false,
			  'controller'=>'member',
			  'action'=>'massmailhistoryremove',
			  'confirm' => 'Are You Sure?',
			  'url'   => array('controller' => 'member', 'action' => 'massmailhistoryremove')
			));
		}?>
    </div>
	<div class="clear-both"></div>
		<div class="tablegrid">
			<div class="tablegridheader">
				<div>
				    <?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Date', array('controller'=>'member', "action"=>"massmaillisthistory/0/0/sending_time/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Date'
					));?>
				</div>
				<div>
				    <?php 
					echo $this->Js->link('Subject', array('controller'=>'member', "action"=>"massmaillisthistory/0/0/mail_subject/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Subject'
					));?>
				</div>
				<div>
				    <?php echo 'Message';?>
				</div>
				<div>
				 <?php 
					echo $this->Js->link('Total Mails', array('controller'=>'member', "action"=>"massmaillisthistory/0/0/total_sent/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#memberpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Total Mails'
					));?>
				</div>
				<div >
					
                </div>
			</div>
			<?php foreach ($massmail_historys as $massmail_history): ?>
				<div class="tablegridrow">
					<div><?php echo $this->Time->format($SITECONFIG["timeformate"], $massmail_history['Massmail_history']['sending_time']);?></div>
					<div><?php echo $massmail_history['Massmail_history']['mail_subject']; ?></div>
					<div>
						<a href="#" rel="lightboxtext" title="ViewDescription-<?php echo $massmail_history['Massmail_history']['id'];?>" onclick="ViewDescription(this)">
							<?php echo $this->html->image('search.png', array('alt'=>'Notes', 'align'=>'absmiddle'));?>
						</a>
                        <span style="display:none;"><span id="ViewDescription-<?php echo $massmail_history['Massmail_history']['id'];?>"><b>Subject : </b><?php echo $massmail_history['Massmail_history']['mail_subject'];?><hr /><?php echo nl2br(stripslashes($massmail_history['Massmail_history']['mail_message'])); ?><hr /><b>Which Member Ids received this message?:</b><hr /><?php echo str_replace(",", ", ", $massmail_history['Massmail_history']['receiver_list']); ?></span></span>
					</div>
                    <div><?php echo $massmail_history['Massmail_history']['total_sent']; ?></div>
					<div class="checkbox">
						<?php
							echo $this->Form->checkbox('HistoryIds.', array(
                               'value' => $massmail_history['Massmail_history']['id'],
								'class' => 'HistoryIds',
								'id'=>'checkHistoryIds'.$massmail_history['Massmail_history']['id'],
                                'hiddenField' => false
                            ));
							?>
						<label for="<?php echo 'checkHistoryIds'.$massmail_history['Massmail_history']['id'];?>"></label>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<?php if(count($massmail_historys)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	<?php echo $this->Form->end();?>
	<?php 
	if($this->params['paging']['Massmail_history']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'massmaillisthistory/0/rpp')));?>
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#memberpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'Member',
			  'action'=>'massmaillisthistory/0/rpp',
			  'url'   => array('controller' => 'member', 'action' => 'massmaillisthistory/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	</div>
	</div>
	</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#memberpage over-->

<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>