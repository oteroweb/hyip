<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 25-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<div id="memberpage">
<div class="height5"></div>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Current Memberships");?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
		<div class="height10"></div>
				
				<?php // Stop Auto Renewal confirmation starts here ?>
				<?php echo $this->Form->create('Membermembership',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'membermembershipaction')));?>
				<?php
				echo __('Are you sure you want to stop the auto renewal of the following membership(s)?').'<br /><br />';
				$n=1;
				$membershipid='';
				foreach($membermemberships as $membermembership)
				{
					echo '('.$n.') '.$membermembership['Membership']['membership_name'].' ('.__('Purchase date').' : '.$membermembership['Membermembership']['purchasedate'].')<br />';
					$n++;
					$membershipid.=$membermembership['Membership']['id'].',';
				}
				$membershipid=trim($membershipid, ",");
				echo $this->Form->input('membershipid', array('type'=>'hidden', 'value'=>$membershipid, 'label' => false));
				?>
				
				<div class="height10"></div>
				<div class="floatleft">
				<?php echo $this->html->link(__("Back"), array('controller' => 'member', 'action' => 'membership' ), array('class'=>'button'));?>
				</div>
				<div class="floatright"> 
					<?php echo $this->Js->submit(__('Confirm'), array(
					  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#UpdateMessage',
					  'class'=>'button',
					  'div'=>false,
					  'controller'=>'member',
					  'action'=>'membermembershipaction',
					  'url'   => array('controller' => 'member', 'action' => 'membermembershipaction')
					));?>
				</div>
				<div class="clear-both"></div>
				<?php echo $this->Form->end();?>
				<?php // Stop Auto Renewal confirmation starts here ?>

	</div>
<div class="height10"></div>
</div><!--#memberpage over-->
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>