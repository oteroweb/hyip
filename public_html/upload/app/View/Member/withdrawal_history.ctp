<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 20-11-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(!$ajax){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Withdrawal History');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">


<?php // Search box code starts here ?>
<div class="floatleft searchlabel"><?php echo __("Advanced Search");?></div>
	<div class="floatright"><a href="javascript:void(0)" onclick="togglebox('.searchlabel', this,'<?php echo __("[+] Show Search Box");?>','<?php echo __("[-] Hide Search Box");?>');"><?php echo __("[-] Hide Search Box");?></a></div>
	<div class="clear-both"></div>
	<div class="advsearchbox searchlabel">
	<?php echo $this->Form->create('Withdraw',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'member','action'=>'withdrawal_history')));?>
		<div class="inlineblock vat">
			<div>
				<span class="searchboxcol1"><?php echo __("Search By");?>  :&nbsp;</span>
				<div class="select-dropdown smallsearch">
					<label>
						<?php echo $this->Form->input('searchby', array(
								'type' => 'select',
								'options' => array('all'=>__('All'), 'payment_processor'=>__('Payment Processor'), 'pro_acc_id'=>__('Payment Processor Id'), 'status'=>__('Status')),
								'selected' => $searchby,
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
						  ));?>
					</label>
				</div>
			</div>
			<div>
				<span class="searchboxcol1"><?php echo __("Search For");?> :&nbsp;</span>
				<?php echo $this->Form->input('searchfor', array('type'=>'text', 'id'=>'searchfor', 'value'=>$searchfor, 'label' => false, 'class'=>'finece-from-box searchfortxt','div' => false));?>
			</div>
		</div>
		<div  class="inlineblock vat">
			<div class="margintb5">
				<span class="searchboxcol2"><?php echo __("From Date");?> :&nbsp;</span>
				<?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker searchdatetxt'));?>
			</div>
			<div>
				<span class="searchboxcol2"><?php echo __("To Date");?> :&nbsp;</span>
				<?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker searchdatetxt'));?>
			</div>
		</div>
		<div class="textcenter margint5">
			<?php echo $this->Js->submit(__('Update Results'), array(
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#memberpage',
				'class'=>'button floatnone',
				'div'=>false,
				'controller'=>'member',
				'action'=>'withdrawal_history',
				'url'   => array('controller' => 'member', 'action' => 'withdrawal_history')
			  ));?>
		</div>
	<?php echo $this->Form->end();?>
	</div>
	<?php // Search box code ends here ?>
	
	<div class="height10"></div>
	
<?php }?>
<div id="memberpage">
<div class="textright"><?php echo "Total Withdraw Amount: ".$Currency['prefix'];?><?php echo round($toal_withdraw_amount[0]['toal_withdraw_amount']*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#memberpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'member', 'action'=>'withdrawal_history')
	));
	$currentpagenumber=$this->params['paging']['Withdraw']['page'];
	?>
	
		<?php // Withdrawal history table starts here ?>
		<div class="padding-left-serchtabal"></div>
		<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			<div class="divtable textcenter">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam">
							<?php
							if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
							echo $this->Js->link(__('Date'), array('controller'=>'member', "action"=>"withdrawal_history/0/req_dt/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Date')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__("Amount"), array('controller'=>'member', "action"=>"withdrawal_history/0/amount/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Amount')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Fees'), array('controller'=>'member', "action"=>"withdrawal_history/0/fee/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Fees')
							));?>
						</div>
						<div class="divth textcenter vam"><?php echo __('Final Amount');?></div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Payment Processor'), array('controller'=>'member', "action"=>"withdrawal_history/0/payment_processor/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Payment Processor')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Balance'), array('controller'=>'member', "action"=>"withdrawal_history/0/withdrawbalance/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Balance')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Payment Processor Id'), array('controller'=>'member', "action"=>"withdrawal_history/0/pro_acc_id/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Payment Processor Id')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Status'), array('controller'=>'member', "action"=>"withdrawal_history/0/status/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#memberpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Status')
							));?>
						</div>
						<div class="divth textcenter vam"><?php echo __("Action");?></div>
					</div>
				</div>
                
				<div class="divtbody">
					<?php
					$i = 0;
					foreach ($withdraws as $withdraw):
						if($i%2==0){$class='gray-color';}else{$class='white-color';}
						?>
						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $withdraw['Withdraw']['req_dt']); ?></div>
							<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($withdraw['Withdraw']['amount']*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
							<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($withdraw['Withdraw']['fee']*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
							<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round(($withdraw['Withdraw']['amount']+$withdraw['Withdraw']['fee'])*$Currency['rate'],4)." ".$Currency['suffix'];?></div>
							<div class="divtd textcenter vam"><?php echo $withdraw['Withdraw']['payment_processor']; ?></div>
							<div class="divtd textcenter vam">
								<?php
								if($withdraw['Withdraw']['withdrawbalance']=='cash')
									echo __('Cash Balance');
								elseif($withdraw['Withdraw']['withdrawbalance']=='repurchase')
									echo __('Re-purchase Balance');
								elseif($withdraw['Withdraw']['withdrawbalance']=='earning')
									echo __('Earning Balance');
								elseif($withdraw['Withdraw']['withdrawbalance']=='commission')
									__('Commission Balance');
								else
									echo '-';
								?>
							</div>
							<div class="divtd textcenter vam"><?php echo $withdraw['Withdraw']['pro_acc_id']; ?></div>
							<div class="divtd textcenter vam"><?php echo ucwords(__($withdraw['Withdraw']['status'])); ?></div>
							<div class="divtd textcenter vam">
							<?php
								if($withdraw['Withdraw']['status']=='Pending')
								{
									echo $this->Js->link($this->html->image('delete.png', array('alt'=>__('Cancel Request'))), array('controller'=>'member', "action"=>"withdrawremove/".$withdraw['Withdraw']['with_id']."/".$currentpagenumber), array(
                                        'update'=>'#memberpage',
                                        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                                        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                                        'escape'=>false,
                                        'class'=>'vtip',
                                        'title'=>__('Cancel Request'),
                                        'confirm'=>__("Do you really want to cancel this withdrawal request?")
                                    ));
								}
								else if($withdraw['Withdraw']['status']=="Done" || $withdraw['Withdraw']['status']=="Paid Manually")
								{
									echo $this->html->image('blue-icon.png', array('alt'=>__('Withdrawal Completed'), 'title'=>__('Withdrawal Completed'), 'class'=>'vtip'));
								}
								else if($withdraw['Withdraw']['status']=="Cancelled")
								{
									echo $this->html->image('red-icon.png', array('alt'=>__('Cancelled By Admin'), 'title'=>__('Cancelled By Admin'), 'class'=>'vtip'));
								}
							?>
							</div>
						</div>
					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($withdraws)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // Withdrawal history table ends here ?>
			
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Withdraw']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Withdraw',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'member','action'=>'withdrawal_history/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#memberpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'member',
						  'action'=>'withdrawal_history/rpp',
						  'url'   => array('controller' => 'member', 'action' => 'withdrawal_history/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
            </div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		
	</div>
	
</div>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>