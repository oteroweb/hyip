<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="settings-site-settings-box">
            	Settings / Template
            </div>
            <div class="sattings-main-box">
            	<div class="sattings-white-box">
                	<div class="sattings-menu-bg">

					<div class="satting-menu">
						<ul>
							<li><?php echo $this->Html->link('Template Directory', 'index', array('class'=>'', 'title' => 'Template Directory')); ?></li>
							<?php echo $this->html->image("sattings-left-menu.jpg", array("alt"=>""));?>
							<li><?php echo $this->Html->link('Layout Directory', 'layout', array('class'=>'', 'title' => 'Layout Directory')); ?></li>
						</ul>
					</div>
					<div class="clear-both"></div>
					<div class="padding-21">
<?php }?>
<div id="success">
		<table class="girde" cellpadding="0" cellspacing="0" width="100%" border="0">
            <thead>
            <tr class="tabal-title-text" >
            <th>&nbsp;</th>
            <th align="center" valign="middle">Layout Directory</th>
            <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <tr >
            	<td colspan="9" class="padding-tabal"></td>
            </tr>
			
			<?php
			$pag=APP."View/themed";//dirname(dirname(__FILE__));
			$dir = new Folder($pag);
			$viewfolder= $dir->read();
			$counter=0;
			foreach ($viewfolder[0] as $folder) 
			{
				if(!in_array($folder,$dontviewfolder))
				{
					$counter++;
					?>
					<tr class="<?php if($counter%=2){echo 'white-color';}else{echo 'blue-color';}?>">
					  <td>&nbsp;</td>
					<td  class="tabal-left-border" valign="middle">
						&nbsp;<?php echo $this->html->image("folder.gif",  array("alt"=>"Directory", "align"=>"absmiddle", 'title'=>'View Directory'));?>&nbsp;
						<?php echo $this->Js->link($folder, array('controller'=>'template', "action"=>"editlayoutfile/".$folder), array(
							'update'=>'#success',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
						));?>
					</td>
					<td>&nbsp;</td>
					</tr>
					<?php
				}
			} ?>
			
<?php if(!$ajax){?>
            </tbody>
        </table>
</div>
						<div class="clear-both"></div>
					</div>
                    <div class="clear-both"></div>
					
					</div>
               </div>
<?php }?>
</div>		  
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>