<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<table class="girde" cellpadding="0" cellspacing="0" width="100%" border="0">
            <thead>
            <tr class="tabal-title-text" >
            <th>&nbsp;</th>
            <th align="center" valign="middle">
				<?php echo $foldername;?> Layout File
			</th>
            <th>&nbsp;</th>
            </tr>
            </thead>
</table>
<?php echo $this->Form->create('TemplateEdit',array('type' => 'post', 'id'=>'TestfilesForm', 'onsubmit' => 'return false;','url'=>array('controller'=>'template','action'=>'updatefile/'.$foldername)));?>
	<?php echo $this->Form->input('filecontent', array('type'=>'textarea','label' => false,'value'=>$filecontent["content"], 'rows'=>'25', 'style'=>'width:100%;padding:0;border:0;'));?>
	
<?php echo $this->Form->end(
	array(
	'div' => false, 
	'label' => 'Submit',
	'class' => 'large white button', 
	'name' => 'submit', 
	'value' => 'Submit', 
	'before' => '', 
	'after' => '',
	'onclick' => 'SubmitFormAjax("#TestfilesForm", "'.$this->Html->url(array("controller" => "template", "action" => "updatelayoutfile/".$foldername),true).'", "#success", "'.$this->Html->url(array("controller" => "template", "action" => "layout")).'");'
	)
);?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>