<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<table class="girde" cellpadding="0" cellspacing="0" width="100%" border="0">
            <thead>
            <tr class="tabal-title-text" >
            <th>&nbsp;</th>
            <th align="center" valign="middle"><?php echo $foldername;?> Directory</th>
            <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <tr >
            	<td colspan="9" class="padding-tabal"></td>
            </tr>
	<?php
	$pag=APP."View".DS.$foldername;
	$dir = new Folder($pag);
	$viewfile= $dir->read();
	foreach ($viewfile[1] as $filename) 
	{
		$pos = strpos($filename, 'adminpanel_');
		if ($pos === false)
		{
			?>
			<tr class="white-color">
			  <td>&nbsp;</td>
			<td  class="tabal-left-border" valign="middle">
				&nbsp;<?php echo $this->html->image("ctp.gif",  array("alt"=>"Directory", "align"=>"absmiddle", 'title'=>'View File'));?>&nbsp;
				<?php echo $this->Js->link($filename, array('controller'=>'template', "action"=>"editfile/".$foldername."_|_".str_replace(".ctp","",$filename)), array(
					'update'=>'#success',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</td>
			<td>&nbsp;</td>
			</tr>
			<?php
		}
	} ?>
			</tbody>
        </table>

<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>