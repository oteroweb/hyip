<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php echo $this->Javascript->link('tinymce');?>
<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Solo Ad Details');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	
	<?php // Solo ad add/edit form starts here ?>
	<?php echo $this->Form->create('Soload',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'soload','action'=>'addaction')));?>
			<div class="form-box">		
				<?php if(isset($soloaddata['Soload']["soload_id"])){
					$action='addaction/'.$soloaddata['Soload']["soload_id"];
				}else{
					$action='addaction';
				}?>
				<div class="form-row">
					<div class="form-col-1"><?php echo __('Available Credits')?> :</div>
					<div class="form-col-2 form-text"><?php echo $addsolo_soloads_credit;?></div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __('Subject')?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('subject' ,array('type'=>'text', "class"=>"formtextbox", 'label'=>false, 'value'=>$soloaddata['Soload']["subject"],'div'=>false));?>
					
					<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?>"></span>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __('Promo Link')?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('link' ,array('type'=>'text', "class"=>"formtextbox", 'label'=>false, 'value'=>!isset($soloaddata['Soload']["link"]) ? 'http://' : $soloaddata['Soload']["link"]));?></div>
				</div>
				<div class="form-row texteditor">
					<div class="colspan">
					        <div>
							<div style="width:100px;margin: 5px 0;"><?php echo __('Message').' : '; ?><span class="required">*</span></div>
							<?php echo $this->Form->input('message' ,array('type'=>'textarea', "class"=>"advancededitor", 'label'=>false, 'value'=>stripslashes($soloaddata['Soload']["message"]), 'style'=>'height:300px; width:275px;'));?>
							<div class="height5"></div>
						</div>
					</div>
					<div></div>
				</div>
				<?php if($AdvertisementSoloAddCaptcha){ ?>
				  <div class="form-row captchrow">
					<div class="form-col-1"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
						<?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formcapthcatextbox", 'div'=>false, 'label'=>false));?>
						<span><?php echo __('Code')?> :</span>
						<?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'AdvertisementSoloAddCaptcha','vspace'=>2,"align"=>"absmiddle")); ?> 
						<a href="javascript:void(0);" onclick="javascript:document.images.AdvertisementSoloAddCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "align"=>"absmiddle"));?></a>
				  </div>
				<?php }?>
			</div>
		<div class="formbutton">
			<?php echo $this->Js->link(__("Back"), array('controller'=>'soload', "action"=>"index"), array(
				'update'=>'#soloadpage',
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button',
				'style'=>'float:left',
				'controller'=>'soload',
				'action'=>'index',
				'url'=> array('controller' => 'soload', 'action' => 'index')
			));?>
			<?php echo $this->Js->submit(__('Save Draft'), array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#UpdateMessage',
				'class'=>'button tinyMCEtriggerSavetk',
				'onfocus'=>"tinyMCE.execCommand('mceRemoveControl', true, 'advancededitor');tinyMCE.triggerSave();",
				'controller'=>'soload',
				'div'=>false,
				'action'=>'savedaddaction',
				'url'   => array('controller' => 'soload', 'action' => 'savedaddaction/0/solo')
			  ));?>
			<?php echo $this->Js->submit(__('Submit'), array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'class'=>'button tinyMCEtriggerSavetk',
			  'onfocus'=>"tinyMCE.execCommand('mceRemoveControl', true, 'advancededitor');tinyMCE.triggerSave();",
			  'controller'=>'soload',
			  'style'=>'margin-right:5px;',
			  'div'=>false,
			  'action'=>$action,
			  'url'   => array('controller' => 'soload', 'action' => $action)
			));?>
		</div>
		<div class="clear-both"></div>
	<?php echo $this->Form->end();?>
	<?php // Solo ad add/edit form ends here ?>
</div>
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>