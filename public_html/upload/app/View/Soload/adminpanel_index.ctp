<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Solo Ads</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li class="active">
				<?php echo $this->Js->link("Solo Ads", array('controller'=>'soload', "action"=>"index"), array(
					'update'=>'#soloadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("View Credits", array('controller'=>'soload', "action"=>"soloadcredit"), array(
					'update'=>'#soloadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Solo Ad Plans", array('controller'=>'soload', "action"=>"plan"), array(
					'update'=>'#soloadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="soloadpage">
<?php }?>
<?php echo $this->Javascript->link('allpage');?>
<script>
function viewsoload(atag)
{
	$("a[rel='lightboxtext']").colorbox({width:"80%", height:"80%", inline:true, href:"#"+atag.title});
}
</script>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Solo_Ads#Solo_Ads" target="_blank">Help</a></div>
 <div class="tab-innar">
	<ul>
	    <li>
			<?php echo $this->Js->link("Credit Solo Ads", array('controller'=>'soload', "action"=>"index"), array(
				'update'=>'#soloadpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'class'=>'active'
			));?>
	    </li>
	    <li>
			<?php echo $this->Js->link("Plan Solo Ads", array('controller'=>'soload', "action"=>"planmember"), array(
				'update'=>'#soloadpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			));?>
		</li>
	</ul>
</div>

<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Soload',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'soload','action'=>'index')));?>
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
						<?php echo $this->Form->input('searchby', array(
						'type' => 'select',
						'options' => array('all'=>'Select Parameter', 'subject'=>'Subject', 'soload_id'=>'Solo Ad Id', 'member_id'=>'Member Id', 'status'=>'Status'),
						'selected' => $searchby,
						'class'=>'',
						'label' => false,
						'style' => '',
						'onchange'=>'if($(this).val()=="status"){$("#select_all").hide();$("#select_status").show(500);}else{$("#select_status").hide();$("#select_all").show(500);}'
						));?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain">
			<span>Search For :</span>
			<span id='select_all' style='display:<?php if($searchby=="status"){ echo "none";} ?>'><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span id='select_status' style='display:<?php if($searchby!="status"){ echo "none";} ?>'>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
							<?php 
							echo $this->Form->input('status', array(
								'type' => 'select',
								'options' => array('pending'=>'Pending', 'approved'=>'Approved', 'sending'=>'Sending', 'sent'=>'Sent'),
								'selected' => $searchfor,
								'class'=>'',
								'label' => false,
								'style' => '',
							));
							?>
						</label>
					</div>
				</div>
			</span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
                  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#soloadpage',
                  'class'=>'searchbtn',
                  'controller'=>'soload',
                  'action'=>'index',
                  'url'=> array('controller' => 'soload', 'action' => 'index')
                ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->

        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#soloadpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'soload', 'action'=>'index')
        ));
        $currentpagenumber=$this->params['paging']['Soload']['page'];
        ?>

<div id="gride-bg">
    <div class="Xpadding10">
	<?php echo $this->Form->create('Soload',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'soload','action'=>'index')));?>
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		<?php 
		echo $this->Form->checkbox('selectAllCheckboxes', array(
		  'hiddenField' => false,
		  'onclick' => 'selectAllCheckboxes("soloadIds",this.checked)',
		));
		?>
		<label for="SoloadSelectAllCheckboxes"></label>
	</div>
	<div class="addnew-button">
		<?php if(!isset($SubadminAccessArray) || in_array('soload',$SubadminAccessArray) || in_array('soload/adminpanel_soloadstatus',$SubadminAccessArray))
		{
			echo $this->Js->submit('Approve Selected Record(s)', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'escape'=>false,
			  'update'=>'#soloadpage',
			  'class'=>'btngray',
			  'div'=>false,
			  'controller'=>'soload',
			  'action'=>'soloadstatus',
			  'confirm' => 'Are You Sure?',
			  'url'   => array('controller' => 'soload', 'action' => 'soloadstatus')
			));
		}?>
	</div>
	<div class="clear-both"></div>
		
		<div class="tablegrid">
				<div class="tablegridheader">
				    <div>
                        <?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('M. Id', array('controller'=>'soload', "action"=>"index/0/0/member_id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#soloadpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Member Id'
                        ));?>
                    </div>
                    <div>
                        <?php echo $this->Js->link('Date', array('controller'=>'soload', "action"=>"index/0/0/incoming_dt/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#soloadpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Date'
                        ));?>
                    </div>
                    <div><?php echo 'Username';?></div>
                    <div>
                        <?php echo $this->Js->link('Subject', array('controller'=>'soload', "action"=>"index/0/0/subject/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#soloadpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Subject'
                        ));?>
                    </div>
			<div>Details</div>
                    <div>Solo Ad</div>
                    <div>
                        <?php echo $this->Js->link('Status', array('controller'=>'soload', "action"=>"index/0/0/status/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#soloadpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Status'
                        ));?>
                    </div>
					<div><?php echo "Action";?></div>
                    <div></div>
                </div>
                <?php foreach ($soloads as $soload): ?>
                    <div class="tablegridrow">
					    <div>
							<?php 
							echo $this->Js->link($soload['Soload']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$soload['Soload']['member_id']."/top/soload/index~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
						</div>
                        <div><?php echo $this->Time->format($SITECONFIG["timeformate"], $soload['Soload']['incoming_dt']); ?></div>
                        <div>
							<?php 
							echo $this->Js->link($soload['Member']['user_name'], array('controller'=>'member', "action"=>"memberadd/".$soload['Soload']['member_id']."/top/soload/index~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
						</div>
                        <div>
							<a href="#" rel="lightboxtext" title="ViewSoload-<?php echo $soload['Soload']['soload_id'];?>" onclick="viewsoload(this)">
								<?php echo $soload['Soload']['subject']; ?>
							</a>
						</div>
						<div>
								<?php
                                $info="";
                                $info.='<b>Date Created : </b>'.$this->Time->format($SITECONFIG["timeformate"], $soload['Soload']['incoming_dt']).'<br/>';
                                if($soload['Soload']['status']=="approved" || $soload['Soload']['status']=="sending" || $soload['Soload']['status']=="sent")
                                {
                                    $info.='<b>Date Approved : </b>'.$this->Time->format($SITECONFIG["timeformate"], $soload['Soload']['approved_dt']).'<br/>';
                                }
                                if($soload['Soload']['status']=="sending" || $soload['Soload']['status']=="sent")
                                {
                                    $info.='<b>Mails Started on : </b>'.$this->Time->format($SITECONFIG["timeformate"], $soload['Soload']['outgoing_dt']).'<br/>';
                                }
                                if($soload['Soload']['status']=="sent")
                                {
                                    $info.='<b>Mails Completed on : </b>'.$this->Time->format($SITECONFIG["timeformate"], $soload['Soload']['completed_dt']).'<br/>';
                                }
                                if($soload['Soload']['status']=="sending" || $soload['Soload']['status']=="sent")
                                {
                                    $info.='<b>Expiry Date : </b>'.$this->Time->format($SITECONFIG["timeformate"], $soload['Soload']['expire_dt']).'<br/>';
                                    $info.='<b>Sent to No. of Members : </b>'.$soload['Soload']['no_of_sent'].'<br/>';
                                    $info.='<b>No. of Members Who Clicked on Link : </b>'.$soload['Soload']['no_of_click'];
                                }
                                ?>
                                <?php echo $this->html->image('information.png', array('alt'=>'Description', 'class'=>'vtip', 'title'=>$info));?>
						</div>
                        <div>
                            <a href="#" rel="lightboxtext" title="ViewSoload-<?php echo $soload['Soload']['soload_id'];?>" onclick="viewsoload(this)">
								<?php echo $this->html->image('search.png', array('alt'=>'Notes', 'align'=>'absmiddle'));?>
							</a>
                            <div style="display:none;"><div id="ViewSoload-<?php echo $soload['Soload']['soload_id'];?>"><?php echo stripslashes($soload['Soload']['message']).'<br /><br /><strong>Promo Link : </strong>'.$soload['Soload']['link'];?></div></div>
                            
                        </div>
                        <div><?php echo ucfirst($soload['Soload']['status']); ?></div>
						<div>
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
										<?php if(!isset($SubadminAccessArray) || in_array('soload',$SubadminAccessArray) || in_array('soload/adminpanel_soloadremove',$SubadminAccessArray)){?>
										<li>
										<?php
												echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Soload'))." Delete", array('controller'=>'soload', "action"=>"soloadremove/".$soload['Soload']['soload_id']."/".$currentpagenumber), array(
													'update'=>'#soloadpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>'',
													'confirm'=>"Do You Really Want to Delete This Soload?"
												));?>
										</li>
										<?php }?>
									</ul>
								  </div>
								</div>
                        </div>
                        <div>
								<?php
								if($soload['Soload']['status']=='pending')
								{?>
									<div class="checkbox">
									<?php echo $this->Form->checkbox('soloadIds.', array(
									  'value' => $soload['Soload']['soload_id'],
									  'class' => 'soloadIds',
									  'hiddenField' => false,
									  'id' => 'SoloadSoloadIds'.$soload['Soload']['soload_id'],
									));
									?><label for="<?php echo 'SoloadSoloadIds'.$soload['Soload']['soload_id']; ?>"></label> </div><?php 
								}
								?>
                        </div>
                    </div>
                <?php endforeach; ?>
        </div>
		<?php if(count($soloads)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php echo $this->Form->end();
        if($this->params['paging']['Soload']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Soload',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'soload','action'=>'index/0/rpp')));?>
			<div class="resultperpage">
				<label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
				</label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#soloadpage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'soload',
                  'action'=>'index/0/rpp',
                  'url'   => array('controller' => 'soload', 'action' => 'index/0/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
     </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#soloadpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>