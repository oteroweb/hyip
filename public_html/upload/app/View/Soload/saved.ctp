<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 15-10-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(!$ajax){ ?>
<div id="soloadpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>

<?php // Top menu code starts here ?>
<div class="comisson-bg">
	<div class="commison-menu">
		<ul>
			<li>
				<?php
					echo $this->Js->link(__('Solo Ads'), array('controller'=>'soload', "action"=>"index"), array(
						'update'=>'#soloadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false
					));
				?>
			</li>
			<li>
				<?php
					echo $this->Js->link(__('Solo Ad Drafts'), array('controller'=>'soload', "action"=>"saved"), array(
						'update'=>'#soloadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'act'
					));
				?>
			</li>
			<?php if(strpos($SITECONFIG["soloadplansetting"],'isenable|1') !== false){ ?>
			<li>
				<?php
					echo $this->Js->link(__('Solo Ad Plans'), array('controller'=>'soload', "action"=>"plans"), array(
						'update'=>'#soloadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
					));
				?>
			</li>
			<?php } ?>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Top menu code ends here ?>

<?php echo $this->Javascript->link('allpage');?>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#soloadpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'soload', 'action'=>'saved')
	));
	$currentpagenumber=$this->params['paging']['Saved_soload']['page'];
	?>
	<div class="main-box-eran">
		<?php // Solo Ad Drafts table starts here ?>
		<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			<div class="divtable textcenter">
				<div class="divthead">
					<div class="divtr tabal-title-text" >
						<div class="divth textcenter vam" style="width:10%;">
							<?php 
							if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
							echo $this->Js->link(__('Id'), array('controller'=>'soload', "action"=>"saved/0/soload_id/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#soloadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Id')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Subject'), array('controller'=>'soload', "action"=>"saved/0/subject/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#soloadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Subject')
							));?>
						</div>
						<div class="divth textcenter vam" style="width:15%;"><?php echo __("Action");?></div>
					</div>
				</div>
				<div class="divtbody">
					<?php $i=1;
					foreach ($soloads as $soload):
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>

						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam"  style="width:10%;"><?php echo $soload['Saved_soload']['soload_id'];?></div>
							<div class="divtd textcenter vam" >
								<?php echo $this->Js->link($soload['Saved_soload']['subject'], array('controller'=>'soload', "action"=>"savedadd/".$soload['Saved_soload']['soload_id']), array(
									'update'=>'#soloadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>__('Edit Solo Ad')
								));?>
							</div>
							<div class="divtd textcenter vam"  style="width:15%;">
								
								<div class="actionmenu">
								      <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
									        <li>
										    <?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit Solo Ad')))." ".__('Edit Solo Ad'), array('controller'=>'soload', "action"=>"savedadd/".$soload['Saved_soload']['soload_id']), array(
											'update'=>'#soloadpage',
											'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
											'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
											'escape'=>false,
											'class'=>'',
											'title'=>''
										));?>
									      </li>
										<li>
											<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>__('Delete Solo Ad')))." ".__('Delete Solo Ad'), array('controller'=>'soload', "action"=>"savedremove/".$soload['Saved_soload']['soload_id']."/".$currentpagenumber), array(
												'update'=>'#soloadpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>'',
												'confirm'=>__("Do you really want to delete this Solo Ad?")
											));?>
										</li>
									 </ul>
								      </div>
								</div>
								
							</div>
						</div>

					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($soloads)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // Solo Ad Drafts table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Saved_soload']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
	<div class="pag-float-left">
		<div class="ul-bg">
			<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Saved_soload',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'soload','action'=>'saved/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#soloadpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'soload',
						  'action'=>'saved/rpp',
						  'url'   => array('controller' => 'soload', 'action' => 'saved/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
			</ul>
		<div class="clear-both"></div>
		</div>
	</div>
	<?php }?>
	<div class="floatright ul-bg">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
		</ul>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>
	
	</div>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>