<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 15-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($EnableSoload==1) { ?>
<?php if(!$ajax){ ?>
<div id="soloadpage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<?php // Top menu code starts here ?>
<div class="comisson-bg">
	<div class="commison-menu">
		<ul>
			<li>
				<?php
					echo $this->Js->link(__('Solo Ads'), array('controller'=>'soload', "action"=>"index"), array(
						'update'=>'#soloadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
					));
				?>
			</li>
			<li>
				<?php
					echo $this->Js->link(__('Solo Ad Drafts'), array('controller'=>'soload', "action"=>"saved"), array(
						'update'=>'#soloadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false
					));
				?>
			</li>
			<?php if(strpos($SITECONFIG["soloadplansetting"],'isenable|1') !== false){ ?>
			<li>
				<?php
					echo $this->Js->link(__('Solo Ad Plans'), array('controller'=>'soload', "action"=>"plans"), array(
						'update'=>'#soloadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'act'
					));
				?>
			</li>
			<?php } ?>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Top menu code ends here ?>
<div class="main-box-eran">
	<?php // Solo Ad Plans table starts here ?>
	<div class="divtable">
			<div class="divthead">
		       <div class="divtr tabal-title-text" >
			       <div class="divth textcenter vam"><?php echo __('Plan Name');?></div>
			       <div class="divth textcenter vam"><?php echo __('Price');?></div>
				   <div class="divth textcenter vam"><?php echo __('Credits');?></div>
			       <div class="divth textcenter vam"><?php echo __("Action");?></div>
		       </div>
			</div>
			<div class="divtbody">
		       <?php
		       $i = 0;
		       foreach ($soloadplandata as $soloadplan):
			       if($i%2==0){$class='white-color';}else{$class='gray-color';}
			       ?>
			       <div class="divtr <?php echo $class;?>">
				       <div class="divtd textcenter vam"><?php echo $soloadplan['Soloadplan']['plan_name'];?></div>
				       <div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($soloadplan['Soloadplan']['price']*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
					   <div class="divtd textcenter vam"><?php echo $soloadplan['Soloadplan']['credits'];?></div>
				       <div class="divtd textcenter vam">
				       <?php
					       echo $this->Js->link('<span class="purchasebutton">Purchase</span>', array('controller'=>'soload', "action"=>"purchase/".$soloadplan['Soloadplan']['id']), array(
						       'update'=>'#soloadpage',
						       'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						       'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						       'escape'=>false,
						       'class'=>'vtip',
						       'title'=>__('Purchase')
					       ));
				       ?>
				       </div>
			       </div>
		       <?php $i++;endforeach; ?>
	       </div>
	</div>
	<?php if(count($soloadplandata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
</div>
	<?php // Solo Ad Plans table ends here ?>
		
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('My Solo Ad Plans');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#soloadpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'soload', 'action'=>'plans')
	));
	$currentpagenumber=$this->params['paging']['Soloadhistory']['page'];
	?>
		<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
		
		<?php // My Solo Ad Plans table starts here ?>
		<div class="divtable">
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam">
						<?php 
						if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
						echo $this->Js->link(__("Id"), array('controller'=>'soload', "action"=>"plans/0/id/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#soloadpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Id')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Plan"), array('controller'=>'soload', "action"=>"plans/0/plan_id/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#soloadpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Plan')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Paid Amount"), array('controller'=>'soload', "action"=>"plans/0/paid_amount/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#soloadpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Paid Amount')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Credits"), array('controller'=>'soload', "action"=>"plans/0/credits/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#soloadpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Credits')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Purchase Date"), array('controller'=>'soload', "action"=>"plans/0/pruchase_date/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#soloadpage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Purchase Date')
						));?>
					</div>
				</div>
			</div>
			<div class="divtbody">
				<?php $i=1;
				foreach ($soloadhistories as $soloadhistory):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $soloadhistory['Soloadhistory']['id'];?></div>
						<div class="divtd textcenter vam">
							<a href="#" class='vtip' title="<?php echo __('Plan Name'); ?> : <?php echo $soloadhistory['Soloadplan']['plan_name'];?>"><?php echo $soloadhistory['Soloadhistory']['plan_id'];?></a>
						</div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($soloadhistory['Soloadhistory']['paid_amount']*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
						<div class="divtd textcenter vam"><?php echo $soloadhistory['Soloadhistory']['credits'];?></div>
						<div class="divtd textcenter vam"><?php echo $this->Time->format($SITECONFIG["timeformate"], $soloadhistory['Soloadhistory']['pruchase_date']); ?></div>
					</div>
				<?php $i++;endforeach; ?>
			</div>
		</div>
		<?php if(count($soloadhistories)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
		<?php // My Solo Ad Plans table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Soloadhistory']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
	<div class="pag-float-left">
		<div class="ul-bg">
			<ul class="nice_paging">
			<?php 
			foreach($resultperpage as $rpp)
			{
				?>
				<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
					<?php 
					echo $this->Form->create('Soloadplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'soload','action'=>'plans/rpp')));
					echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
				
					echo $this->Js->submit($rpp, array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#soloadpage',
					  'class'=>'resultperpagebutton',
					  'div'=>false,
					  'controller'=>'soload',
					  'action'=>'plans/rpp',
					  'url'   => array('controller' => 'soload', 'action' => 'plans/rpp')
					));
					echo $this->Form->end();
					?>
				</li>
				<?php 
			}?>
			</ul>
		<div class="clear-both"></div>
		</div>
	</div>
	<?php }?>
	<div class="floatright ul-bg">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
		</ul>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>
	
</div>
</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>