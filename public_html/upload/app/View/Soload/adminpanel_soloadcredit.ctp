<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 01-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Solo Ads</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Solo Ads", array('controller'=>'soload', "action"=>"index"), array(
					'update'=>'#soloadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'class'=>'satting-menu-active'
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("View Credits", array('controller'=>'soload', "action"=>"soloadcredit"), array(
					'update'=>'#soloadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Solo Ad Plans", array('controller'=>'soload', "action"=>"plan"), array(
					'update'=>'#soloadpage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="soloadpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Solo_Ads#View_Credits" target="_blank">Help</a></div>
    <div id="UpdateMessage"></div>
	
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'soload','action'=>'soloadcredit')));?>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>Search By :</span>
			<span>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
						<?php 
						echo $this->Form->input('searchby', array(
							  'type' => 'select',
							  'options' => array('all'=>'Select Parameter', 'member_id'=>'Member Id', 'user_name'=>'Username', 'soloads_credit'=>'Solo Ad Credits'),
							  'selected' => $searchby,
							  'class'=>'',
							  'label' => false,
							  'style' => ''
						));
						?>
						</label>
					</div>
				</div>
			</span>
		</div>
		 <div class="fromboxmain">
			<span>Search For :</span>
			<span><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
                  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#soloadpage',
                  'class'=>'searchbtn',
                  'controller'=>'soload',
                  'action'=>'soloadcredit',
                  'url'=> array('controller' => 'soload', 'action' => 'soloadcredit')
                ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->

        <?php
        $this->Paginator->options(array(
            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
            'update' => '#soloadpage',
            'evalScripts' => true,
            'url'=> array('controller'=>'soload', 'action'=>'soloadcredit')
        ));
        $currentpagenumber=$this->params['paging']['Member']['page'];
        ?>

<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
        <div class="clear-both"></div>
        <?php echo $this->Form->create('Soload',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'soload','action'=>'soloadcreditupdate/'.$member['Member']['member_id'].'/'.$currentpagenumber)));?>
		
		<div class="tablegrid">
		   <div class="tablegridheader">
				    <div>
                        <?php 
                        if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
                        echo $this->Js->link('M. Id', array('controller'=>'soload', "action"=>"soloadcredit/0/member_id/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#soloadpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Member Id'
                        ));?>
                    </div>
                    <div>
                        <?php echo $this->Js->link('Username', array('controller'=>'soload', "action"=>"soloadcredit/0/user_name/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#soloadpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Username'
                        ));?>
                    </div>
                    <div>
                        <?php echo $this->Js->link('Solo Ad Credits', array('controller'=>'soload', "action"=>"soloadcredit/0/soloads_credit/".$sorttype."/".$currentpagenumber), array(
                            'update'=>'#soloadpage',
                            'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                            'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                            'escape'=>false,
                            'class'=>'vtip',
                            'title'=>'Sort By Solo Ad Credits'
                        ));?>
                    </div>
                    <div style="width: 150px;"><?php echo "Assign New Credits";?></div>
                    <div style="width: 100px;"><?php echo "Action";?></div>
                </div>
                <?php foreach ($members as $member):
		echo $this->Form->input('currentcredits_'.$member['Member']['member_id'], array('type'=>'hidden', 'value'=>$member['Member']['soloads_credit'], 'label' => false)); ?>
                    <div class="tablegridrow">
						<div>
							<?php 
							echo $this->Js->link($member['Member']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$member['Member']['member_id']."/top/soload/index~top"), array(
								'update'=>'#pagecontent',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>'View Member'
							));?>
						</div>
                        <div><?php echo $member['Member']['user_name'];?></div>
                        <div><?php echo $member['Member']['soloads_credit']==NULL?0:$member['Member']['soloads_credit'];?></div>
                        <div><input type="text" class="satting-from-bg" style="width:85px;" name="txt_<?php echo $member['Member']['member_id'];?>" /></div>
                        <div class="textcenter">
								<?php echo $this->Js->submit('Update', array(
								  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								  'update'=>'#soloadpage',
								  'class'=>'btnorange vtip',
								  'div'=>false,
								  'controller'=>'soload',
								  'title'=>'Update Credit of Member Id : '.$member['Member']['member_id'],
								  'action'=>'soloadcreditupdate/'.$member['Member']['member_id']."/".$currentpagenumber,
								  'url'=> array('controller' => 'soload', 'action' => 'soloadcreditupdate/'.$member['Member']['member_id']."/".$currentpagenumber)
								));?>
                        </div>
                    </div>
                <?php endforeach; ?>
        </div>
		<?php if(count($members)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
        <?php echo $this->Form->end();
        if($this->params['paging']['Member']['count']>$this->Session->read('pagerecord'))
        {?>
        <div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
        <div class="floatleft margintop19">
            <?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'soload','action'=>'soloadcredit/rpp')));?>
			<div class="resultperpage">
				<label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
				</label>
			</div>
            <span id="resultperpageapply" style="display:none;">
                <?php echo $this->Js->submit('Apply', array(
                  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
                  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
                  'update'=>'#soloadpage',
                  'class'=>'',
                  'div'=>false,
                  'controller'=>'soload',
                  'action'=>'soloadcredit/rpp',
                  'url'   => array('controller' => 'soload', 'action' => 'soloadcredit/rpp')
                ));?>
            </span>
            <?php echo $this->Form->end();?>
        </div>
        <?php }?>
        <div class="floatright">
        <ul class="nice_paging">
            <?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
            <?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
            <?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
        </ul>
        </div>
        <div class="clear-both"></div>
		<div class="height10"></div>
	</div>
		
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#soloadpage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>