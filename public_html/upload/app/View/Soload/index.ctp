<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 15-10-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php if(!$ajax){?>
<?php echo $this->Javascript->link('tiny_mce/tiny_mce.js');?>
<div id="soloadpage">
<?php } //if(!$ajax){?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>

<?php // Top menu code starts here ?>
<div class="comisson-bg">
	<div class="commison-menu">
		<ul>
			<li>
				<?php
					echo $this->Js->link(__('Solo Ads'), array('controller'=>'soload', "action"=>"index"), array(
						'update'=>'#soloadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'act'
					));
				?>
			</li>
			<li>
				<?php
					echo $this->Js->link(__('Solo Ad Drafts'), array('controller'=>'soload', "action"=>"saved"), array(
						'update'=>'#soloadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false
					));
				?>
			</li>
			<?php if(strpos($SITECONFIG["soloadplansetting"],'isenable|1') !== false){ ?>
			<li>
				<?php
					echo $this->Js->link(__('Solo Ad Plans'), array('controller'=>'soload', "action"=>"plans"), array(
						'update'=>'#soloadpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false
					));
				?>
			</li>
			<?php } ?>
		</ul>
	</div>
	<div class="clear-both"></div>
</div>
<?php // Top menu code ends here ?>

<div class="main-box-eran">

<?php // Search box code starts here ?>
<div class="floatleft searchlabel"><?php echo __("Advanced Search");?></div>
	<div class="floatright"><a href="javascript:void(0)" onclick="togglebox('.searchlabel', this,'<?php echo __("[+] Show Search Box");?>','<?php echo __("[-] Hide Search Box");?>');"><?php echo __("[-] Hide Search Box");?></a></div>
	<div class="clear-both"></div>
	<div class="advsearchbox searchlabel">
	<?php echo $this->Form->create('Soload',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'soload','action'=>'index')));?>
		<div class="inlineblock vat">
			<div>
				<span class="searchboxcol1"><?php echo __("Search By");?>  :&nbsp;</span>
				<div class="select-dropdown smallsearch">
					<label>
						<?php echo $this->Form->input('searchby', array(
								'type' => 'select',
								'options' => array('all'=>__('All'), 'pending'=>__('Pending'), 'approved'=>__('Approved'), 'sending'=>__('Sending'), 'sent'=>__('Sent')),
								'selected' => $searchbySES,
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
						  ));?>
					</label>
				</div>
				  
			</div>
		</div>
		<div class="inlineblock vat">
			<div class="margintb5">
				<span class="searchboxcol2"><?php echo __("From Date");?> :&nbsp;</span>
				<?php echo $this->Form->input('fromdate', array('type'=>'text', 'id'=>'fromdate', 'value'=>$fromdate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker searchdatetxt'));?>
			</div>
		</div>	
		<div class="inlineblock vat">
			<div class="margintb5">
				<span class="searchboxcol2"><?php echo __("To Date");?> :&nbsp;</span>
				<?php echo $this->Form->input('todate', array('type'=>'text', 'id'=>'todate', 'value'=>$todate, 'div' => false, 'label' => false, 'class'=>'finece-from-box datepicker searchdatetxt'));?>
			</div>
		</div>
		<div class="textcenter margint5">
			<?php echo $this->Js->submit(__('Update Results'), array(
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#soloadpage',
				'class'=>'button floatnone',
				'div'=>false,
				'style'=>'float:none',
				'controller'=>'soload',
				'action'=>'index',
				'url'   => array('controller' => 'soload', 'action' => 'index')
			  ));?>
		</div>
		<div class="clear-both"></div>
	<?php echo $this->Form->end();?>
	</div>
	<?php // Search box code ends here ?>
	
	<div class="height10"></div>
<?php echo $this->Javascript->link('allpage');?>
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#soloadpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'soload', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Soload']['page'];
	?>
		<?php // Solo ads table starts here ?>
		<div class="padding-left-serchtabal">
			<?php echo $this->Js->link(__("Add New"), array('controller'=>'soload', "action"=>"add"), array(
				'update'=>'#soloadpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button'
			));?>
		</div>
		<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			<div class="divtable textcenter">	
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam" style="width: 10%">
							<?php 
							if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
							echo $this->Js->link(__('Id'), array('controller'=>'soload', "action"=>"index/0/soload_id/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#soloadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Id')
							));?>
						</div>
						<div class="divth textcenter vam">
							<?php echo $this->Js->link(__('Subject'), array('controller'=>'soload', "action"=>"index/0/subject/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#soloadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Subject')
							));?>
						</div>
						<div class="divth textcenter vam" style="width: 10%">
							<?php 
							echo $this->Js->link(__('Status'), array('controller'=>'soload', "action"=>"index/0/status/".$sorttype."/".$currentpagenumber), array(
								'update'=>'#soloadpage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'vtip',
								'title'=>__('Sort By').' '.__('Status')
							));?>
						</div>
						<div class="divth textcenter vam" style="width: 15%"><?php echo __("Action");?></div>
					</div>
				</div>
				<div class="divtbody">
					<?php $i=1;
					foreach ($soloads as $soload):
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>

						<div class="divtr <?php echo $class;?>">
							<div class="divtd textcenter vam" style="width: 10%"><?php echo $soload['Soload']['soload_id'];?></div>
							<div class="divtd textcenter vam">
								<?php echo $this->Js->link($soload['Soload']['subject'], array('controller'=>'soload', "action"=>"add/".$soload['Soload']['soload_id']), array(
									'update'=>'#soloadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>__('Edit Solo Ad')
								));?>
							</div>
							<div class="divtd textcenter vam" style="width: 10%"><?php echo $soload['Soload']['status'];?></div>
							<div class="divtd textcenter vam"  style="width: 15%">
								<?php 
								$info="";
								$info.=__('Date created').' : '.$this->Time->format($SITECONFIG["timeformate"], $soload['Soload']['incoming_dt']).'<br/>';
								if($soload['Soload']['status']=="approved" || $soload['Soload']['status']=="sending" || $soload['Soload']['status']=="sent")
									$info.=__('Approved Date').' : '.$this->Time->format($SITECONFIG["timeformate"], $soload['Soload']['approved_dt']).'<br/>';
								if($soload['Soload']['status']=="sending" || $soload['Soload']['status']=="sent")
									$info.=__('Mail Starting Date').' : '.$this->Time->format($SITECONFIG["timeformate"], $soload['Soload']['outgoing_dt']).'<br/>';
								if($soload['Soload']['status']=="sent")
									$info.=__('Completed Date').' : '.$this->Time->format($SITECONFIG["timeformate"], $soload['Soload']['completed_dt']).'<br/>';
								if($soload['Soload']['status']=="sending" || $soload['Soload']['status']=="sent")
									$info.=__('Expiry Date').' : '.$this->Time->format($SITECONFIG["timeformate"], $soload['Soload']['expire_dt']).'<br/>';
								if($soload['Soload']['status']=="sending" || $soload['Soload']['status']=="sent")
								{
									$info.=__('Sent to no of members').' : '.$soload['Soload']['no_of_sent'].'<br/>';
									$info.=__('No of members who clicked on link').' : '.$soload['Soload']['no_of_click'];
								}
								echo $this->html->image('search.png', array('alt'=>$info, 'title'=>$info, 'class'=>'vtip', 'style'=>'vertical-align:top;'));
								?>
								<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit Solo Ad'))), array('controller'=>'soload', "action"=>"add/".$soload['Soload']['soload_id']), array(
									'update'=>'#soloadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>__('Edit Solo Ad')
								));?>
								<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>__('Delete Solo Ad'))), array('controller'=>'soload', "action"=>"remove/".$soload['Soload']['soload_id']."/".$currentpagenumber), array(
									'update'=>'#soloadpage',
									'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
									'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
									'escape'=>false,
									'class'=>'vtip',
									'title'=>__('Delete Solo Ad'),
									'confirm'=>__("Do you really want to delete this Solo Ad?")
								));?>
							</div>
						</div>
					<?php $i++;endforeach; ?>
				</div>
			</div>
			<?php if(count($soloads)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
			<?php // Solo ads table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Soload']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
	<div class="pag-float-left">
		<div class="ul-bg">
			<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Soload',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'soload','action'=>'index/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#soloadpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'soload',
						  'action'=>'index/rpp',
						  'url'   => array('controller' => 'soload', 'action' => 'index/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
			</ul>
		<div class="clear-both"></div>
		</div>
	</div>
	<?php }?>
	<div class="floatright ul-bg">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
		</ul>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>
	
	</div>
<?php if(!$ajax){?>	
</div>
<?php } ?>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>