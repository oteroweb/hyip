<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php if($themesubmenuaccess){?>
<?php echo $this->Javascript->link('tinymce');?>
<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Solo Ad Draft Details');?></div>
	<div class="clear-both"></div>
</div>

<div class="main-box-eran">

	<?php // Solo ad draft add/edit form starts here ?>
	<?php echo $this->Form->create('Soload',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'soload','action'=>'savedaddaction')));?>
		<div class="form-box">
			<?php if(isset($soloaddata['Saved_soload']["soload_id"])){
				$action='savedaddaction/'.$soloaddata['Saved_soload']["soload_id"];
			}else{
				$action='savedaddaction';
			}?>
				<div class="form-row">	
					<div class="form-col-1"><?php echo __('Available Credits')?> :</div>
					<div class="form-col-2 form-text"><?php echo $soloaddata['Member']["soloads_credit"];?></div>
				</div>
				<div class="form-row">	
					<div class="form-col-1"><?php echo __('Subject')?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('subject' ,array('type'=>'text', "class"=>"formtextbox", 'label'=>false, 'value'=>$soloaddata['Saved_soload']["subject"],'div'=>false));?>
					
					<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)'); ?>"></span>
					</div>
				</div>
				<div class="form-row">	
					<div class="form-col-1"><?php echo __('Promo Link')?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('link' ,array('type'=>'text', "class"=>"formtextbox", 'label'=>false, 'value'=>$soloaddata['Saved_soload']["link"]));?></div>
				</div>
				<div class="form-row texteditor">
					<div class="colspan">
					    <div>
							<div style="width:100px;margin: 5px 0;"><?php echo __('Message').' : '; ?><span class="required">*</span></div>
							
							<div class="height5"></div><?php echo $this->Form->input('message' ,array('type'=>'textarea', "class"=>"advancededitor", 'label'=>false, 'value'=>stripslashes($soloaddata['Saved_soload']["message"]), 'style'=>'height:300px;'));?>
							<div class="height5"></div>
						</div>
					</div>
					<div></div>
				</div>
				<?php if($AdvertisementSolosavedAddCaptcha){ ?>
				  <div class="form-row captchrow">
					<div class="form-col-1"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
						<?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formcapthcatextbox", 'div'=>false, 'label'=>false));?>
						<span><?php echo __('Code')?> :</span>
						<?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'AdvertisementSolosavedAddCaptcha','vspace'=>2,"style"=>"vertical-align: middle")); ?> 
						<a href="javascript:void(0);" onclick="javascript:document.images.AdvertisementSolosavedAddCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "style"=>"vertical-align: middle"));?></a>
				  </div>
				<?php }?>
		</div>
	<div class="formbutton">
		<?php echo $this->Js->link(__("Back"), array('controller'=>'soload', "action"=>"saved"), array(
			'update'=>'#soloadpage',
			'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'button',
			'style'=>'float:left',
			'controller'=>'soload',
			'action'=>'saved',
			'url'=> array('controller' => 'soload', 'action' => 'saved')
		));?>
		<?php echo $this->Js->submit(__('Save Draft'), array(
			'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'update'=>'#UpdateMessage',
			'class'=>'button tinyMCEtriggerSavetk',
			'onfocus'=>"tinyMCE.execCommand('mceRemoveControl', true, 'advancededitor');tinyMCE.triggerSave();",
			'controller'=>'soload',
			'div'=>false,
			'action'=>'savedaddaction/'.$soloaddata['Saved_soload']["soload_id"],
			'url'   => array('controller' => 'soload', 'action' => 'savedaddaction/'.$soloaddata['Saved_soload']["soload_id"])
		  ));?>
		<?php echo $this->Js->submit(__('Send'), array(
		  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
		  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		  'update'=>'#UpdateMessage',
		  'class'=>'button tinyMCEtriggerSavetk',
		  'onfocus'=>"tinyMCE.execCommand('mceRemoveControl', true, 'advancededitor');tinyMCE.triggerSave();",
		  'controller'=>'soload',
		  'style'=>'margin-right:5px;',
		  'div'=>false,
		  'action'=>'addaction',
		  'url'   => array('controller' => 'soload', 'action' => 'addaction/0/saved')
		));?>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->end();?>
	<?php // Solo ad draft add/edit form ends here ?>
	
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php }else{echo __('This page is disabled by administrator');}?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>