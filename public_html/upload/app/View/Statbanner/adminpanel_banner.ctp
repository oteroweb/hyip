<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 02-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>		
<?php if(!$ajax){?>
<div class="whitetitlebox">Promo Tools / Dynamic Banners</div>
<div id="banneradpage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Dynamic_Banners" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Statbanner',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'statbanner','action'=>'banner')));?>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>Search By :</span>
			<span>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
							<?php 
							echo $this->Form->input('searchby', array(
							      'type' => 'select',
							      'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'title'=>'Banner Title',  'active'=>'Active Banners', 'inactive'=>'Inactive Banners'),
							      'selected' => $searchby,
							      'class'=>'',
							      'label' => false,
							      'style' => '',
								  'onchange'=>'if($(this).val()=="active" || $(this).val()=="inactive"){$(".SearchFor").hide(500);}else{$(".SearchFor").show(500);}'
							));
							?>
						</label>
					</div>
				</div>
			</span>
		</div>
		 <div class="fromboxmain">
			<span class="SearchFor" <?php if($searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>>Search For :</span>
			<span class="SearchFor" <?php if($searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#banneradpage',
				'class'=>'searchbtn',
				'controller'=>'statbanner',
				'action'=>'banner',
				'url'=> array('controller' => 'statbanner', 'action' => 'banner')
				 ));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->


	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#banneradpage',
		'evalScripts' => true,
		'url'=> array('controller'=>'statbanner', 'action'=>'banner')
	));
	$currentpagenumber=$this->params['paging']['Statbanner']['page'];
	?>
	
<div id="gride-bg" class="noborder">
	<div class="padding10">
		<div class="greenbottomborder">
			<div class="height10"></div>
    <div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('statbanner',$SubadminAccessArray) || in_array('statbanner/adminpanel_add', $SubadminAccessArray)){ ?>
		<?php echo $this->Js->link("+ Add New", array('controller'=>'statbanner', "action"=>"add"), array(
			'update'=>'#banneradpage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btnorange'
		));?>
	<?php } ?>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Statbanner',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'statbanner','action'=>'banner')));?>
	
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'statbanner', "action"=>"banner/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Title', array('controller'=>'statbanner', "action"=>"banner/0/0/title/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#banneradpage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Purchase Date'
					));?>
				</div>
				<div><?php echo 'Banner';?></div>
				<div><?php echo 'Action';?></div>
			</div>
			<?php foreach ($bannerads as $bannerad): ?>
				<div class="tablegridrow">
					<div><?php echo $bannerad['Statbanner']['id']; ?></div>
					<div><?php echo $bannerad['Statbanner']['title']; ?></div>
					<div>
						<a href="#" target="_blank" class="vtip" title="<b>Title : </b><?php echo $bannerad['Statbanner']['title']; ?>">
							<?php
								if($bannerad['Statbanner']['banner_size']=='125x125'){$height="50px";$maxwidth='';}
								elseif($bannerad['Statbanner']['banner_size']=='468x60'){$height="90%";$maxwidth="468px";}
								else{$height="100%";$maxwidth="728px";}
								echo '<img src="'.$SITEURL.'img/statbanner/'.$bannerad['Statbanner']['bannerimage'].'" alt="'.$bannerad['Statbanner']['title'].'" width="'.$height.'" style="max-width:'.$maxwidth.';" />';
							?>
						</a>
					</div>
					<div>
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
												<li>
												<?php $filetype = end(@explode('.', strtolower($bannerad['Statbanner']['bannerimage'])));?>
												<a href="<?php echo str_replace("https://", "http://", $SITEURL)."statbanner/preview/".$bannerad['Statbanner']['id'].'/1.'.$filetype; ?>" target="_blank" class="" title="Preview Banner"><?php echo $this->html->image('search.png', array('alt'=>'Preview'));?> Preview Banner</a>
												</li>
												
												<?php if(!isset($SubadminAccessArray) || in_array('statbanner',$SubadminAccessArray) || in_array('statbanner/adminpanel_add/$', $SubadminAccessArray)){ ?>
												<li>
												<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Banner'))." Edit", array('controller'=>'statbanner', "action"=>"add/".$bannerad['Statbanner']['id']), array(
												'update'=>'#banneradpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>''
												));?>
												</li>
												<?php } ?>
												
												<?php if(!isset($SubadminAccessArray) || in_array('statbanner',$SubadminAccessArray) || in_array('statbanner/adminpanel_status', $SubadminAccessArray)){ ?>
												<li>
												<?php
												if($bannerad['Statbanner']['status']==0){
													$statusaction='1';
													$statusicon='red-icon.png';
													$statustext='Approve';
												}else{
													$statusaction='0';
													$statusicon='blue-icon.png';
													$statustext='Disapprove';}
												echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'statbanner', "action"=>"status/".$statusaction."/".$bannerad['Statbanner']['id']."/".$currentpagenumber), array(
													'update'=>'#banneradpage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>''
												));?>
												</li>
												<?php } ?>
												
												<?php if(!isset($SubadminAccessArray) || in_array('statbanner',$SubadminAccessArray) || in_array('statbanner/adminpanel_remove', $SubadminAccessArray)){ ?>
												<li>
												<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Banner'))." Delete", array('controller'=>'statbanner', "action"=>"remove/".$bannerad['Statbanner']['id']."/".$currentpagenumber), array(
												'update'=>'#banneradpage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'confirm'=>"Do You Really Want to Delete This Banner?"
												));?>
												</li>
												<?php } ?>
									</ul>
								  </div>
								</div>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	
	<?php if(count($bannerads)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
	
    <?php echo $this->Form->end();
	if($this->params['paging']['Statbanner']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<div class="height10"></div>
		<?php echo $this->Form->create('Statbanner',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'statbanner','action'=>'banner/0/rpp')));?>
		<div class="resultperpage">
                        <label>
				<?php 
				echo $this->Form->input('resultperpage', array(
				  'type' => 'select',
				  'options' => $resultperpage,
				  'selected' => $this->Session->read('pagerecord'),
				  'class'=>'',
				  'label' => false,
				  'div'=>false,
				  'style' => '',
				  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
				));
				?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#banneradpage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'statbanner',
			  'action'=>'banner/0/rpp',
			  'url'   => array('controller' => 'statbanner', 'action' => 'banner/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
		<div class="height10"></div>
	</div>
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#Loginadpage over-->
<?php }?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>