<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 17-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<div id="banneradpage">
<?php if($EnableBannerad==1) { ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Dynamic Banners");?></div>
	<div class="clear-both"></div>
</div>
<?php
$this->Paginator->options(array(
	'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
	'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
	'update' => '#banneradpage',
	'evalScripts' => true,
	'url'=> array('controller'=>'statbanner', 'action'=>'index')
));
$currentpagenumber=$this->params['paging']['Statbanner']['page'];
?>
<div class="main-box-eran">
		<div class="padding-left-serchtabal"></div>
		<div class="activ-ad-pack"><div class="height15"></div><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
		
		<?php // Dynamic Banners table starts here ?>
		<div class="divtable">
			<div class="divtbody">
			<?php $i=1;
			foreach ($statbannerdata as $statbanner):
			if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
				<div class="divtr <?php echo $class;?>">
					  <div class="divtd textcenter vam">
						<div class="height7"></div>
						<?php
						$banner_path=$SITEURL."img/statbanner/".$statbanner['Statbanner']['bannerimage'];
						$filetype = end(@explode('.', strtolower($statbanner['Statbanner']['bannerimage'])));
						echo '<img src="'.$banner_path.'" alt="'.$statbanner['Statbanner']['title'].'" align="abdmiddle" style="width:100%;max-width:468px;" />';
						$widthheight=@explode('x', $statbanner['Statbanner']['banner_size']);
						
						$preview_banner_path=str_replace("https://", "http://", $SITEURL)."statbanner/preview/".$statbanner['Statbanner']['id']."/".$UID.".".$filetype;
						?>
						<div class="height7"></div>
						<div><textarea class="formtextarea" style="width:84%;height:35px;;margin:0;background-color:transparent;"><a href="<?php echo $SITEURL;?>ref/<?php echo ($SITECONFIG["reflinkiduser"]==1)?$UNAME:$UID;?>"><img src="<?php echo $preview_banner_path;?>" width="<?php echo $widthheight[0];?>" height="<?php echo $widthheight[1];?>" /></a></textarea> &nbsp;
						<a href="<?php echo str_replace("https://", "http://", $SITEURL)."statbanner/preview/".$statbanner['Statbanner']['id'].'/'.$UID.'.'.$filetype; ?>" target="_blank" class="vtip" title="<?php echo __('Preview Banner'); ?>"><?php echo $this->html->image('search.png', array('alt'=>__('Preview Banner')));?></a>
						</div>
						<div class="height7"></div>
					  </div>
				</div>
			<?php $i++;endforeach; ?>
			</div>
	  	</div>
		<?php if(count($statbannerdata)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
		<?php // Dynamic Banners table ends here ?>
	
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Statbanner']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Statbanner',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'statbanner','action'=>'index/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
						
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#advertisementpage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'statbanner',
						  'action'=>'banners/rpp',
						  'url'   => array('controller' => 'statbanner', 'action' => 'index/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
            </div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		
	</div>


<?php } else { echo __('This page is disabled by administrator'); } ?>
</div>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>