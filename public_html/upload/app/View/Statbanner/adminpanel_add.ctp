<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 20-11-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<script type="text/javascript"> 
(function() {
$('#Statbanneradd').ajaxForm({
	beforeSend: function() {
        $('#pleasewait').fadeIn();
    },
    complete: function(xhr) {
		$('#pleasewait').fadeOut();
		$('#UpdateMessage').html(xhr.responseText);
		$('#UpdateMessage').show();
	}
});
})();       
</script>
<script type="text/javascript">$(function() {$('.colorpicker').wheelColorPicker({ sliders: "whsvp", preview: true, format: "css" });});</script>
<?php if(!$ajax){?>
<div class="whitetitlebox">Promo Tools / Dynamic Banners</div>
<div id="banneradpage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Dynamic_Banners" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<div class="height10"></div>
<div class="backgroundwhite">
		
<?php echo $this->Form->create('Statbanner',array('type' => 'post', 'id'=>'Statbanneradd', 'type' =>'file', 'onsubmit' => 'return true;','novalidate'=>'novalidate','url'=>array('controller'=>'statbanner','action'=>'addaction')));?>
	<?php if(isset($banneraddata['Statbanner']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$banneraddata['Statbanner']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
		echo $this->Form->input('current_style', array('type'=>'hidden', 'value'=>$banneraddata['Statbanner']["banner_size"], 'label' => false));
	}else{echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'add', 'label' => false));}?>
	
	<div class="frommain">
		<div class="height10"></div>
		<div class="fromborder">
			<div id="bannerbox" style="position: relative;width: 468px;height: 60px;margin: 0 auto;">
				<?php 
					if($banneraddata['Statbanner']['bannerimage']!='')
						echo '<img src="'.$SITEURL.'img/statbanner/'.$banneraddata['Statbanner']['bannerimage'].'" alt="" />';
					else
						echo '<img src="'.$SITEURL.'img/statbanner/468x60.gif" alt="" />';
				?>
			</div>
			<script type="text/javascript">
				function previewstat(fieldclass)
				{
					var statstyle='position:absolute;';
					$(fieldclass).each( function(key) {
						
						if (key!=2) {
							intval=parseInt($(this).val());
						}
						
						if (key==0)
							statstyle=statstyle+"left:"+$(this).val()+"px;";
						if (key==1)
						{
							if(fieldclass=='.myphotocl')
								statstyle=statstyle+"bottom:"+(26-intval)+"px;";
							else
								statstyle=statstyle+"bottom:"+(60-intval)+"px;";
						}
						if(fieldclass!='.myphotocl')
						{
							if (key==2)
								statstyle=statstyle+"color:"+$(this).val()+";";
							if (key==3)
								statstyle=statstyle+"font-size:"+$(this).val()+"px;";
						}
						if (key==5)
							statstyle=statstyle+"transform:rotate(-"+$(this).val()+"deg);";
					});
					
					if($("#bannerbox span").hasClass(fieldclass.replace('.','')+"box"))
					{
						$("#bannerbox ."+fieldclass.replace('.','')+"box").attr('style', statstyle);
					}
					else
					{
						if(fieldclass=='.myphotocl')
							$("#bannerbox").append("<span class='"+fieldclass.replace('.','')+"box' style='"+statstyle+"'><img src='<?php echo $SITEURL.'img/statbanner/34-34.jpg';?>' /></span>");
						else
							$("#bannerbox").append("<span class='"+fieldclass.replace('.','')+"box' style='"+statstyle+"'>742</span>");
					}
				}
			</script>
		</div>
	
		<div class="fromnewtext">Title :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('title', array('type'=>'text', 'value'=>$banneraddata['Statbanner']["title"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Select Banner : <span class="red-color">*</span></div>
		<div class="fromborderdropedown3">
		  <div class="btnorange browsebutton">Browse<?php echo $this->Form->input('bannerimage', array('type'=>'file', 'label' => false, 'div' => false,'class'=>'browserbuttonlink'));?></div>
		</div>
		
		
		<div class="fromnewtext">Style :</div>
		<div class="fromborderdropedown3">
			<div class="select-main">
				<label>
					<?php 
					echo $this->Form->input('banner_size', array(
						'type' => 'select',
						'options' => array('468x60'=>'468x60'),
						'selected' => $banneraddata['Statbanner']["banner_size"],
						'class'=>'',
						'label' => false,
						'div' => false,
						'style' => ''
					));
					?>
				</label>
			</div>
		</div>
		
		<div class="fromnewtext">Statistics :</div>
		
	</div>
	
			<div class="tablegrid statbannerinputclass">
				<div class="tablegridheader" style="font-size:12px;">
					<div>-</div>
					<div>X</div>
					<div>Y</div>
					<div>Color</div>
					<div>Size</div>
					<div>Font</div>
					<div>Angle</div>
					<div>-</div>
				</div>
				<div class="tablegridrow">
					<?php $myunamearray=explode("|", $banneraddata['Statbanner']["myuname"]);?>
					<div class="textleft"><span class="checkbox"><?php echo $this->Form->input('ismyuname', array('type'=>'checkbox', 'label' => "My Username", 'checked'=>($banneraddata['Statbanner']["ismyuname"]==1)?"checked":"", 'div' => false, 'class'=>'','style'=>'margin-right:5px;'));?></span></div>
					<div><?php echo $this->Form->input('myuname.', array('type'=>'text', 'value'=>@$myunamearray[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg myunamecl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myuname.', array('type'=>'text', 'value'=>@$myunamearray[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg myunamecl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myuname.', array('type'=>'text', 'value'=>@$myunamearray[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg myunamecl colorpicker', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myuname.', array('type'=>'text', 'value'=>@$myunamearray[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg myunamecl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myuname.', array('type'=>'text', 'value'=>@$myunamearray[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg myunamecl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myuname.', array('type'=>'text', 'value'=>@$myunamearray[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg myunamecl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Html->image("search.png", array("alt" => __("View"), 'onclick'=>"previewstat('.myunamecl')"))?></div>
				</div>
				<div class="tablegridrow">
					<?php $myrefarray=explode("|", $banneraddata['Statbanner']["myref"]);?>
					<div class="textleft"><span class="checkbox"><?php echo $this->Form->input('ismyref', array('type'=>'checkbox', 'label' => "Total My Referrals", 'checked'=>($banneraddata['Statbanner']["ismyref"]==1)?"checked":"", 'div' => false, 'class'=>'','style'=>'margin-right:5px;'));?></span></div>
					<div><?php echo $this->Form->input('myref.', array('type'=>'text', 'value'=>@$myrefarray[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg myrefcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myref.', array('type'=>'text', 'value'=>@$myrefarray[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg myrefcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myref.', array('type'=>'text', 'value'=>@$myrefarray[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg myrefcl colorpicker', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myref.', array('type'=>'text', 'value'=>@$myrefarray[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg myrefcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myref.', array('type'=>'text', 'value'=>@$myrefarray[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg myrefcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myref.', array('type'=>'text', 'value'=>@$myrefarray[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg myrefcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Html->image("search.png", array("alt" => __("View"), 'onclick'=>"previewstat('.myrefcl')"))?></div>
				</div>
				<?php /*<div class="tablegridrow">
					<?php $myearningarray=explode("|", $banneraddata['Statbanner']["myearning"]);?>
					  <div><?php echo $this->Form->input('ismyearning', array('type'=>'checkbox', 'label' => "Total My Earning", 'checked'=>($banneraddata['Statbanner']["ismyearning"]==1)?"checked":"", 'div' => true, 'class'=>'','style'=>'margin-right:5px;'));?></div>
					<div><?php echo $this->Form->input('myearning.', array('type'=>'text', 'value'=>@$myearningarray[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg myearningcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myearning.', array('type'=>'text', 'value'=>@$myearningarray[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg myearningcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myearning.', array('type'=>'text', 'value'=>@$myearningarray[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg myearningcl colorpicker', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myearning.', array('type'=>'text', 'value'=>@$myearningarray[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg myearningcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myearning.', array('type'=>'text', 'value'=>@$myearningarray[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg myearningcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myearning.', array('type'=>'text', 'value'=>@$myearningarray[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg myearningcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Html->image("search.png", array("alt" => __("View"), 'onclick'=>"previewstat('.myearningcl')"))?></div>
				</div>*/ ?>
				<div class="tablegridrow">
					<?php $mycommarray=explode("|", $banneraddata['Statbanner']["mycomm"]);?>
					<div class="textleft"><span class="checkbox"><?php echo $this->Form->input('ismycomm', array('type'=>'checkbox', 'label' => "Total My Commissions", 'checked'=>($banneraddata['Statbanner']["ismycomm"]==1)?"checked":"", 'div' => false, 'class'=>'','style'=>'margin-right:5px;'));?></span></div>
					<div><?php echo $this->Form->input('mycomm.', array('type'=>'text', 'value'=>@$mycommarray[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg mycommcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('mycomm.', array('type'=>'text', 'value'=>@$mycommarray[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg mycommcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('mycomm.', array('type'=>'text', 'value'=>@$mycommarray[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg mycommcl colorpicker', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('mycomm.', array('type'=>'text', 'value'=>@$mycommarray[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg mycommcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('mycomm.', array('type'=>'text', 'value'=>@$mycommarray[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg mycommcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('mycomm.', array('type'=>'text', 'value'=>@$mycommarray[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg mycommcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Html->image("search.png", array("alt" => __("View"), 'onclick'=>"previewstat('.mycommcl')"))?></div>
				</div>
				<div class="tablegridrow">
					<?php $mybonusarray=explode("|", $banneraddata['Statbanner']["mybonus"]);?>
					<div class="textleft"><span class="checkbox"><?php echo $this->Form->input('ismybonus', array('type'=>'checkbox', 'label' => "Total My Miscellaneous Bonus", 'checked'=>($banneraddata['Statbanner']["ismybonus"]==1)?"checked":"", 'div' => false, 'class'=>'','style'=>'margin-right:5px;'));?></span></div>
					<div><?php echo $this->Form->input('mybonus.', array('type'=>'text', 'value'=>@$mybonusarray[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg mybonuscl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('mybonus.', array('type'=>'text', 'value'=>@$mybonusarray[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg mybonuscl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('mybonus.', array('type'=>'text', 'value'=>@$mybonusarray[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg mybonuscl colorpicker', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('mybonus.', array('type'=>'text', 'value'=>@$mybonusarray[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg mybonuscl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('mybonus.', array('type'=>'text', 'value'=>@$mybonusarray[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg mybonuscl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('mybonus.', array('type'=>'text', 'value'=>@$mybonusarray[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg mybonuscl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Html->image("search.png", array("alt" => __("View"), 'onclick'=>"previewstat('.mybonuscl')"))?></div>
				</div>
				<div class="tablegridrow">
					<?php $myphotoarray=explode("|", $banneraddata['Statbanner']["myphoto"]);?>
					<div class="textleft"><span class="checkbox"><?php echo $this->Form->input('ismyphoto', array('type'=>'checkbox', 'label' => "My Profile Picture", 'checked'=>($banneraddata['Statbanner']["ismyphoto"]==1)?"checked":"", 'div' => false, 'class'=>'','style'=>'margin-right:5px;'));?></span></div>
					<div><?php echo $this->Form->input('myphoto.', array('type'=>'text', 'value'=>@$myphotoarray[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg myphotocl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('myphoto.', array('type'=>'text', 'value'=>@$myphotoarray[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg myphotocl', 'style'=>'width:100px;'));?></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div><?php echo $this->Html->image("search.png", array("alt" => __("View"), 'onclick'=>"previewstat('.myphotocl')"))?></div>
				</div>
				<div class="tablegridrow">
					<?php $memberarray=explode("|", $banneraddata['Statbanner']["member"]);?>
					<div class="textleft"><span class="checkbox"><?php echo $this->Form->input('ismember', array('type'=>'checkbox', 'label' => "Total Members On Site", 'checked'=>($banneraddata['Statbanner']["ismember"]==1)?"checked":"", 'div' => false, 'class'=>'','style'=>'margin-right:5px;'));?></span></div>
					<div><?php echo $this->Form->input('member.', array('type'=>'text', 'value'=>@$memberarray[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg membercl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('member.', array('type'=>'text', 'value'=>@$memberarray[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg membercl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('member.', array('type'=>'text', 'value'=>@$memberarray[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg membercl colorpicker', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('member.', array('type'=>'text', 'value'=>@$memberarray[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg membercl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('member.', array('type'=>'text', 'value'=>@$memberarray[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg membercl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('member.', array('type'=>'text', 'value'=>@$memberarray[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg membercl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Html->image("search.png", array("alt" => __("View"), 'onclick'=>"previewstat('.membercl')"))?></div>
				</div>
				<div class="tablegridrow">
					<?php $depoarray=explode("|", $banneraddata['Statbanner']["depo"]);?>
					<div class="textleft"><span class="checkbox"><?php echo $this->Form->input('isdepo', array('type'=>'checkbox', 'label' => "Total Deposits", 'checked'=>($banneraddata['Statbanner']["isdepo"]==1)?"checked":"", 'div' => false, 'class'=>'','style'=>'margin-right:5px;'));?></span></div>
					<div><?php echo $this->Form->input('depo.', array('type'=>'text', 'value'=>@$depoarray[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg depocl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('depo.', array('type'=>'text', 'value'=>@$depoarray[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg depocl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('depo.', array('type'=>'text', 'value'=>@$depoarray[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg depocl colorpicker', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('depo.', array('type'=>'text', 'value'=>@$depoarray[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg depocl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('depo.', array('type'=>'text', 'value'=>@$depoarray[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg depocl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('depo.', array('type'=>'text', 'value'=>@$depoarray[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg depocl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Html->image("search.png", array("alt" => __("View"), 'onclick'=>"previewstat('.depocl')"))?></div>
				</div>
				<div class="tablegridrow">
					<?php $payoutarray=explode("|", $banneraddata['Statbanner']["payout"]);?>
					<div class="textleft"><span class="checkbox"><?php echo $this->Form->input('ispayout', array('type'=>'checkbox', 'label' => "Total Payouts", 'checked'=>($banneraddata['Statbanner']["ispayout"]==1)?"checked":"", 'div' => false, 'class'=>'','style'=>'margin-right:5px;'));?></span></div>
					<div><?php echo $this->Form->input('payout.', array('type'=>'text', 'value'=>@$payoutarray[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg payoutcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('payout.', array('type'=>'text', 'value'=>@$payoutarray[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg payoutcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('payout.', array('type'=>'text', 'value'=>@$payoutarray[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg payoutcl colorpicker', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('payout.', array('type'=>'text', 'value'=>@$payoutarray[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg payoutcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('payout.', array('type'=>'text', 'value'=>@$payoutarray[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg payoutcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('payout.', array('type'=>'text', 'value'=>@$payoutarray[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg payoutcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Html->image("search.png", array("alt" => __("View"), 'onclick'=>"previewstat('.payoutcl')"))?></div>
				</div>
				<div class="tablegridrow">
					<?php $commarray=explode("|", $banneraddata['Statbanner']["comm"]);?>
					<div class="textleft"><span class="checkbox"><?php echo $this->Form->input('iscomm', array('type'=>'checkbox', 'label' => "Total Commissions", 'checked'=>($banneraddata['Statbanner']["iscomm"]==1)?"checked":"", 'div' => false, 'class'=>'','style'=>'margin-right:5px;'));?></span></div>
					<div><?php echo $this->Form->input('comm.', array('type'=>'text', 'value'=>@$commarray[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg commcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('comm.', array('type'=>'text', 'value'=>@$commarray[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg commcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('comm.', array('type'=>'text', 'value'=>@$commarray[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg commcl colorpicker', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('comm.', array('type'=>'text', 'value'=>@$commarray[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg commcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('comm.', array('type'=>'text', 'value'=>@$commarray[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg commcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('comm.', array('type'=>'text', 'value'=>@$commarray[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg commcl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Html->image("search.png", array("alt" => __("View"), 'onclick'=>"previewstat('.commcl')"))?></div>
				</div>
				<div class="tablegridrow">
					<?php $bonusarray=explode("|", $banneraddata['Statbanner']["bonus"]);?>
					<div class="textleft"><span class="checkbox"><?php echo $this->Form->input('isbonus', array('type'=>'checkbox', 'label' => "Total Miscellaneous Bonus", 'checked'=>($banneraddata['Statbanner']["isbonus"]==1)?"checked":"", 'div' => false, 'class'=>'','style'=>'margin-right:5px;'));?></span></div>
					<div><?php echo $this->Form->input('bonus.', array('type'=>'text', 'value'=>@$bonusarray[0], 'label' => false, 'div' => false, 'class'=>'fromboxbg bonuscl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('bonus.', array('type'=>'text', 'value'=>@$bonusarray[1], 'label' => false, 'div' => false, 'class'=>'fromboxbg bonuscl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('bonus.', array('type'=>'text', 'value'=>@$bonusarray[2], 'label' => false, 'div' => false, 'class'=>'fromboxbg bonuscl colorpicker', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('bonus.', array('type'=>'text', 'value'=>@$bonusarray[3], 'label' => false, 'div' => false, 'class'=>'fromboxbg bonuscl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('bonus.', array('type'=>'text', 'value'=>@$bonusarray[4], 'label' => false, 'div' => false, 'class'=>'fromboxbg bonuscl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Form->input('bonus.', array('type'=>'text', 'value'=>@$bonusarray[5], 'label' => false, 'div' => false, 'class'=>'fromboxbg bonuscl', 'style'=>'width:100px;'));?></div>
					<div><?php echo $this->Html->image("search.png", array("alt" => __("View"), 'onclick'=>"previewstat('.bonuscl')"))?></div>
				</div>
			</div>
		
	<div class="frommain">
		<div class="formbutton">
			<?php echo $this->Form->submit('Submit', array(
			  'class'=>'btnorange',
			  'div'=>false
			));?>
			<?php echo $this->Js->link("Back", array('controller'=>'statbanner', "action"=>"banner"), array(
				'update'=>'#banneradpage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'div'=>false,
				'class'=>'btngray'
			));?>
		</div>
		
	</div>
	<?php echo $this->Form->end();?>
		
</div>
<?php if(!$ajax){?>
</div><!--#Loginadpage over-->
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>