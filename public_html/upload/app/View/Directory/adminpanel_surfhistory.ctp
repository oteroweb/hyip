<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 11-12-2014
  * Last Modified: 11-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Biz Directory</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Directory Categories", array('controller'=>'directory', "action"=>"category"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Biz Directory Plans", array('controller'=>'directory', "action"=>"plan"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Plan Member", array('controller'=>'directory', "action"=>"planmember"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Surf Free Plan", array('controller'=>'directory', "action"=>"surffreeplan"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Surf Free Plan Member", array('controller'=>'directory', "action"=>"surffreeplanmember"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
                        <li class="active">
				<?php echo $this->Js->link("Surf History", array('controller'=>'directory', "action"=>"surfhistory"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="directorypage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Biz_Directory" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Websitesurfhistory',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'directory','action'=>'surfhistory')));?>
	
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
					<?php 
					echo $this->Form->input('searchby', array(
						  'type' => 'select',
						  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'member_id'=>'Member Id', 'website_url'=>'Website Url', 'ip_address'=>'IP Address'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => '',
					));
					?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain" id="SearchFor">
			<span>Search For :</span>
			<span class="searchforall"><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#directorypage',
				'class'=>'searchbtn',
				'controller'=>'directory',
				'action'=>'surfhistory/'.$planid,
				'url'=> array('controller' => 'directory', 'action' => 'surfhistory/'.$planid)
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->
<?php
    $this->Paginator->options(array(
        'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
        'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
        'update' => '#directorypage',
        'evalScripts' => true,
        'url'=> array('controller'=>'directory', 'action'=>'surfhistory/'.$planid)
    ));
    $currentpagenumber=$this->params['paging']['Websitesurfhistory']['page'];
?>
<div id="gride-bg">
    <div class="Xpadding10">
	<?php echo $this->Form->create('Websitesurfhistory',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'directory','action'=>'surfhistory')));?>		
    <div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		 <?php 
		echo $this->Form->checkbox('selectAllCheckboxes', array(
		  'hiddenField' => false,
		  'onclick' => 'selectAllCheckboxes("bannerIds",this.checked)'
		));
		?>
		<label for="WebsitesurfhistorySelectAllCheckboxes"></label>
	</div>
	<div class="addnew-button">
       <?php if(!isset($SubadminAccessArray) || in_array('directory',$SubadminAccessArray) || in_array('directory/adminpanel_removesurfhistory',$SubadminAccessArray)){
			echo $this->Js->submit('Delete Selected Record(s)', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'escape'=>false,
			  'update'=>'#directorypage',
			  'class'=>'btngray',
			  'div'=>false,
			  'controller'=>'directory',
			  'action'=>'removesurfhistory',
			  'confirm' => 'Are You Sure?',
			  'url'   => array('controller' => 'directory', 'action' => 'removesurfhistory')
			));
		} ?>
		
	</div>
	<div class="clear-both"></div>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'directory', "action"=>"surfhistory/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('M. Id', array('controller'=>'directory', "action"=>"surfhistory/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Website Url', array('controller'=>'directory', "action"=>"surfhistory/0/website_url/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Website Url'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Surf Date', array('controller'=>'directory', "action"=>"surfhistory/0/surf_date/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Surf Date'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('IP Address', array('controller'=>'directory', "action"=>"surfhistory/0/ip_address/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By IP Address'
					));?>
				</div>
				<div></div>
			</div>
			<?php foreach ($Websitesurfhistory as $surfhistory): ?>
				<div class="tablegridrow">
					<div><?php echo $surfhistory['Websitesurfhistory']['id']; ?></div>
					<div>
						<?php 
						echo $this->Js->link($surfhistory['Websitesurfhistory']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$surfhistory['Websitesurfhistory']['member_id']."/top/directory/plan~top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
                                        <div><?php echo stripslashes($surfhistory['Websitesurfhistory']['website_url']); ?></div>
					<div class="textcenter" style="width: 40%;">
						<p><?php echo $this->Time->format($SITECONFIG["timeformate"], $surfhistory['Websitesurfhistory']['surf_date']); ?></p>
					</div>
					
					<div>
                                            <?php echo stripslashes($surfhistory['Websitesurfhistory']['ip_address']); ?>
                                        </div>
					<div class="checkbox">
                                            <?php
                                                echo $this->Form->checkbox('bannerIds.', array(
                                                  'value' => $surfhistory['Websitesurfhistory']['id'],
                                                  'id'=>'bannerIds'.$surfhistory['Websitesurfhistory']['id'],
                                                  'class' => 'bannerIds',
                                                  'hiddenField' => false
                                                ));
                                                echo '<label for="bannerIds'.$surfhistory['Websitesurfhistory']['id'].'"></label>';
                                            ?>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($Websitesurfhistory)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Websitesurfhistory']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Websitesurfhistory',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'directory','action'=>'surfhistory/'.$planid.'/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#directorypage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'directory',
			  'action'=>'surfhistory/'.$planid.'/rpp',
			  'url'   => array('controller' => 'directory', 'action' => 'surfhistory/'.$planid.'/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	
	</div>
</div>
<?php }else{ ?>
<div class="accessdenied"><?php echo "Access Denied";?></div>
<?php }?>

<?php if(!$ajax){?>
</div><!--#directorypage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>