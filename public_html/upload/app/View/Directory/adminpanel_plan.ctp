<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 10-12-2014
  * Last Modified: 11-12-2014
  *********************************************************************/
?>

<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Biz Directory</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Directory Categories", array('controller'=>'directory', "action"=>"category"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Biz Directory Plans", array('controller'=>'directory', "action"=>"plan"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Plan Member", array('controller'=>'directory', "action"=>"planmember"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Surf Free Plan", array('controller'=>'directory', "action"=>"surffreeplan"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Surf Free Plan Member", array('controller'=>'directory', "action"=>"surffreeplanmember"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			
                        <li>
				<?php echo $this->Js->link("Surf History", array('controller'=>'directory', "action"=>"surfhistory"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="directorypage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Biz_Directory#Biz_Directory_Plans" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Directoryplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'directory','action'=>'plan')));?>
	<div class="from-box">
		<div class="fromboxmain width480">
			<span>Search By :</span>
			<span>
				<div class="searchoptionselect">
					<div class="select-main">
						<label>
						<?php 
						echo $this->Form->input('searchby', array(
							  'type' => 'select',
							  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'plan_name'=>'Plan Name', 'price'=>'Price', 'total_click'=>'Clicks', '125x125'=>'125x125 Banners', '468x60'=>'468x60 Banners', '728x90'=>'728x90 Banners','surfing'=>'Surfing Directory Plan','lifetime'=>'Life Time Directory Plan', 'active'=>'Active Plans', 'inactive'=>'Inactive Plans'),
							  'selected' => $searchby,
							  'class'=>'',
							  'label' => false,
							  'style' => '',
							  'onchange'=>'if($(this).val()=="125x125" || $(this).val()=="468x60" || $(this).val()=="728x90" || $(this).val()=="surfing" || $(this).val()=="lifetime" || $(this).val()=="inactive" || $(this).val()=="active"){$(".SearchFor").hide(500);}else{$(".SearchFor").show(500);}'
						));
						?>
						</label>
					</div>
				</div>
			</span>
		</div>
		 <div class="fromboxmain" >
			<span class="SearchFor" <?php if($searchby=="125x125" || $searchby=="468x60" || $searchby=="728x90" || $searchby=="surfing" || $searchby=="lifetime" || $searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>>Search For :</span>
			<span class="SearchFor" <?php if($searchby=="125x125" || $searchby=="468x60" || $searchby=="728x90" || $searchby=="surfing" || $searchby=="lifetime" || $searchby=="inactive" || $searchby=="active"){ echo 'style="display:none"';} ?>><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#directorypage',
				'class'=>'searchbtn',
				'controller'=>'directory',
				'action'=>'plan',
				'url'=> array('controller' => 'directory', 'action' => 'plan')
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->


	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#directorypage',
		'evalScripts' => true,
		'url'=> array('controller'=>'directory', 'action'=>'plan')
	));
	$currentpagenumber=$this->params['paging']['Directoryplan']['page'];
	?>
	
<div id="gride-bg">
    <div class="Xpadding10">
	<div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button">
	<?php if(!isset($SubadminAccessArray) || in_array('directory',$SubadminAccessArray) || in_array('directory/adminpanel_planadd', $SubadminAccessArray)){ ?>
		<?php echo $this->Js->link("+ Add New", array('controller'=>'directory', "action"=>"planadd"), array(
			'update'=>'#directorypage',
			'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			'escape'=>false,
			'class'=>'btnorange'
		));?>
	<?php } ?>
	</div>
	<div class="clear-both"></div>
	<?php echo $this->Form->create('Directoryplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'directory','action'=>'plan')));?>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'directory', "action"=>"plan/0/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Type', array('controller'=>'directory', "action"=>"plan/0/0/ptype/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Type'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Plan Name', array('controller'=>'directory', "action"=>"plan/0/0/plan_name/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Name'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Price', array('controller'=>'directory', "action"=>"plan/0/0/price/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Price'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Banner Size', array('controller'=>'directory', "action"=>"plan/0/0/banner_size/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Banner Size'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Clicks', array('controller'=>'directory', "action"=>"plan/0/0/total_click/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Clicks'
					));?>
				</div>
                <div><?php echo 'Action';?></div>
			</div>
			<?php foreach ($directoryplans as $directoryplan): ?>
				<div class="tablegridrow">
				    <div><?php echo $directoryplan['Directoryplan']['id']; ?></div>
					<div><?php echo ($directoryplan['Directoryplan']['ptype']==1)?"Life Time Directory":"Surfing Directory"; ?></div>
					<div><?php echo $directoryplan['Directoryplan']['plan_name']; ?></div>
					<div>$<?php echo $directoryplan['Directoryplan']['price']; ?></div>
					<div><?php echo $directoryplan['Directoryplan']['banner_size']; ?></div>
					<div><?php echo ($directoryplan['Directoryplan']['ptype']==1)?"N/A":$directoryplan['Directoryplan']['total_click']; ?></div>
					<div>
								<div class="actionmenu">
								  <div class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
											
											<?php if(!isset($SubadminAccessArray) || in_array('directory',$SubadminAccessArray) || in_array('directory/adminpanel_planadd/$', $SubadminAccessArray)){ ?>
											<li>
												<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Biz Directory Plan')).' Edit Biz Directory Plan', array('controller'=>'directory', "action"=>"planadd/".$directoryplan['Directoryplan']['id']), array(
													'update'=>'#directorypage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
												));?>
											</li>
											<?php } ?>
											
											<?php if(!isset($SubadminAccessArray) || in_array('directory',$SubadminAccessArray) || in_array('directory/adminpanel_status', $SubadminAccessArray)){ ?>
										    <li>
												<?php
												if($directoryplan['Directoryplan']['status']==0){
													$statusaction='1';
													$statusicon='red-icon.png';
													$statustext='Activate Biz Directory Plan';
												}else{
													$statusaction='0';
													$statusicon='blue-icon.png';
													$statustext='Inactivate Biz Directory Plan';}
												echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'directory', "action"=>"status/".$statusaction."/".$directoryplan['Directoryplan']['id']."/".$currentpagenumber), array(
													'update'=>'#directorypage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
												));?>
											</li>
										    <?php } ?>
										    <?php if(!isset($SubadminAccessArray) || in_array('directory',$SubadminAccessArray) || in_array('directory/adminpanel_loginad', $SubadminAccessArray)){ ?>
											<li>
												<?php 
												echo $this->Js->link($this->html->image('view-members.png', array('alt'=>'View Members')).' View Members', array('controller'=>'directory', "action"=>"planmember/".$directoryplan['Directoryplan']['id']), array(
													'update'=>'#directorypage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
												)); ?>
											</li>
											<?php } ?>
											
											<?php if(!isset($SubadminAccessArray) || in_array('directory',$SubadminAccessArray) || in_array('directory/adminpanel_purchasestatus', $SubadminAccessArray)){ ?>
										    <li>
												<?php
												if($directoryplan['Directoryplan']['allow_new_purchase']==0){
													$statusaction='1';
													$statusicon='close.png';
													$statustext='Allow New Purchases';
												}else{
													$statusaction='0';
													$statusicon='open.png';
													$statustext='Deny New Purchases';}
												echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext)).' '.$statustext, array('controller'=>'directory', "action"=>"purchasestatus/".$statusaction."/".$directoryplan['Directoryplan']['id']."/".$currentpagenumber), array(
													'update'=>'#directorypage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
												));?>
											</li>
										    <?php } ?>
											
											<?php if(!isset($SubadminAccessArray) || in_array('directory',$SubadminAccessArray) || in_array('directory/adminpanel_loginadremove', $SubadminAccessArray)){ ?>
											<li>
												<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Biz Directory Plan')).' Delete Biz Directory Plan', array('controller'=>'directory', "action"=>"remove/".$directoryplan['Directoryplan']['id']."/".$currentpagenumber), array(
													'update'=>'#directorypage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'confirm'=>"Do You Really Want to Delete This Plan?"
												));?>
											</li>
											<?php } ?>
									</ul>
								  </div>
								</div>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($directoryplans)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Directoryplan']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Directoryplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'directory','action'=>'plan/0/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#directorypage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'directory',
			  'action'=>'plan/0/rpp',
			  'url'   => array('controller' => 'directory', 'action' => 'plan/0/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
    </div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#directorypage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>