<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 11-12-2014
  * Last Modified: 11-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Biz Directory</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Directory Categories", array('controller'=>'directory', "action"=>"category"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Biz Directory Plans", array('controller'=>'directory', "action"=>"plan"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Plan Member", array('controller'=>'directory', "action"=>"planmember"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Surf Free Plan", array('controller'=>'directory', "action"=>"surffreeplan"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Surf Free Plan Member", array('controller'=>'directory', "action"=>"surffreeplanmember"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
                        <li>
				<?php echo $this->Js->link("Surf History", array('controller'=>'directory', "action"=>"surfhistory"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="directorypage">
<?php }?>
<?php if($IsAdminAccess){?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Biz_Directory#Plan_Member" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>
<script type="text/javascript">
	<?php if($searchby=='category'){ ?>
	generatecombo("data[Directorymember][category]","Directorycategory","id","category","<?php echo $searchfor; ?>",".searchforcategory","<?php echo $ADMINURL;?>","");
	<?php } ?>
</script>
<!-- Search-box-start -->
<div class="serchmainbox">
	<div class="serchgreybox">Search Option</div>
	<?php echo $this->Form->create('Directorymember',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'directory','action'=>'planmember')));?>
	
	<div class="from-box">
		<div class="fromboxmain">
		  <span>Search By :</span>
		  <span>
			<div class="searchoptionselect">
				<div class="select-main">
					<label>
					<?php 
					echo $this->Form->input('searchby', array(
						  'type' => 'select',
						  'options' => array('all'=>'Select Parameter', 'id'=>'Id', 'plan_id'=>'Plan Id', 'member_id'=>'Member Id', 'title'=>'Title',  'display_counter'=>'Displayed', 'click_counter'=>'Clicked', 'category'=>'Category', 'running'=>'Running Biz Directory', 'expire'=>'Expire Biz Directory', 'active'=>'Active Biz Directory', 'inactive'=>'Inactive Biz Directory', 'unpause'=>'Unpause Biz Directory', 'pause'=>'Pause Biz Directory'),
						  'selected' => $searchby,
						  'class'=>'',
						  'label' => false,
						  'style' => '',
						  'onchange'=>'if($(this).val()=="unpause" || $(this).val()=="pause" || $(this).val()=="inactive" || $(this).val()=="active" || $(this).val()=="expire" || $(this).val()=="running"){$("#SearchFor").hide(500);}else{$("#SearchFor").show(500);} if($(this).val()=="category"){ generatecombo("data[Directorymember][category]","Directorycategory","id","category","<?php echo $searchfor; ?>",".searchforcategory","'.$ADMINURL.'","");$(".searchforall").hide(); $(".searchforcategory").show(500);}else{ $(".searchforcategory").hide(); $(".searchforall").show(500);}'
					));
					?>
					</label>
				</div>
			</div>
		  </span>
		</div>
		<div class="fromboxmain" id="SearchFor" <?php if($searchby=="unpause" || $searchby=="pause" || $searchby=="inactive" || $searchby=="active" || $searchby=='expire' || $searchby=='running'){ echo 'style="display:none"';} ?>>
			<span>Search For :</span>
			<span class="searchforall" <?php if($searchby=="category"){ echo "style='display:none'";}?>><?php echo $this->Form->input('searchfor', array('type'=>'text', 'value'=>$searchfor, 'label' => false, 'class'=>'searchfor'));?></span>
			<span class="searchforcategory" <?php if($searchby!="category"){ echo "style='display:none'";}?>></span>
		</div>
	 </div>
	 <div class="from-box">
		<div class="fromboxmain width480">
			<span>From :</span>
			<span><?php echo $this->Form->input('fromdate', array('type'=>'text', 'value'=>$fromdate, 'label' => false, 'class'=>'datepicker'));?></span>
		</div>
		 <div class="fromboxmain">
			<span>To :</span>
			<span><?php echo $this->Form->input('todate', array('type'=>'text', 'value'=>$todate, 'label' => false, 'class'=>'datepicker'));?></span>
			<span class="padding-left">
				<?php echo $this->Js->submit('', array(
				'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'update'=>'#directorypage',
				'class'=>'searchbtn',
				'controller'=>'directory',
				'action'=>'planmember/'.$planid,
				'url'=> array('controller' => 'directory', 'action' => 'planmember/'.$planid)
				));?>
			</span>
		 </div>
	</div>
	 <?php echo $this->Form->end();?>
</div>
<!-- Search-box-over -->


	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#directorypage',
		'evalScripts' => true,
		'url'=> array('controller'=>'directory', 'action'=>'planmember/'.$planid)
	));
	$currentpagenumber=$this->params['paging']['Directorymember']['page'];
	?>

<div id="gride-bg">
    <div class="Xpadding10">
	<?php echo $this->Form->create('Directorymember',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'directory','action'=>'planmember')));?>		
    <div class="paginator-text"><?php echo $this->Paginator->counter(array('format' => 'Display %page% Through %current% Records | Total Records : %count%'));?></div>
	<div class="addnew-button checkbox">
		 <?php 
		echo $this->Form->checkbox('selectAllCheckboxes', array(
		  'hiddenField' => false,
		  'onclick' => 'selectAllCheckboxes("bannerIds",this.checked)'
		));
		?>
		<label for="DirectorymemberSelectAllCheckboxes"></label>
	</div>
	<div class="addnew-button">
       <?php if(!isset($SubadminAccessArray) || in_array('directory',$SubadminAccessArray) || in_array('directory/adminpanel_planmemberstatus',$SubadminAccessArray)){
			echo $this->Js->submit('Approve Selected Record(s)', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'escape'=>false,
			  'update'=>'#directorypage',
			  'class'=>'btngray',
			  'div'=>false,
			  'controller'=>'directory',
			  'action'=>'planmemberstatus',
			  'confirm' => 'Are You Sure?',
			  'url'   => array('controller' => 'directory', 'action' => 'planmemberstatus')
			));
		} ?>
		
	</div>
	<div class="clear-both"></div>
	<div class="tablegrid">
			<div class="tablegridheader">
				<div>
					<?php 
					if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
					echo $this->Js->link('Id', array('controller'=>'directory', "action"=>"planmember/".$planid."/0/id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Id'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('M. Id', array('controller'=>'directory', "action"=>"planmember/".$planid."/0/member_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Member Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Plan Id', array('controller'=>'directory', "action"=>"planmember/".$planid."/0/plan_id/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Plan Id'
					));?>
				</div>
				<div>
					<?php 
					echo $this->Js->link('Category Id', array('controller'=>'directory', "action"=>"planmember/".$planid."/0/category/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Category Id'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Purchase Date', array('controller'=>'directory', "action"=>"planmember/".$planid."/0/create_date/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Purchase Date'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Displayed', array('controller'=>'directory', "action"=>"planmember/".$planid."/0/display_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Displayed'
					));?>
				</div>
                <div>
					<?php 
					echo $this->Js->link('Clicked', array('controller'=>'directory', "action"=>"planmember/".$planid."/0/click_counter/".$sorttype."/".$currentpagenumber), array(
						'update'=>'#directorypage',
						'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						'escape'=>false,
						'class'=>'vtip',
						'title'=>'Sort By Clicked'
					));?>
				</div>
                <div><?php echo 'Action';?></div>
				<div></div>
			</div>
			<?php foreach ($directorymembers as $directorymember): ?>
				<div class="tablegridrow">
					<div><?php echo $directorymember['Directorymember']['id']; ?></div>
					<div>
						<?php 
						echo $this->Js->link($directorymember['Directorymember']['member_id'], array('controller'=>'member', "action"=>"memberadd/".$directorymember['Directorymember']['member_id']."/top/directory/plan~top", 'plugin' => false), array(
							'update'=>'#pagecontent',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>'View Member'
						));?>
					</div>
					<div>
						<a href="#" class="vtip" title="<b>Plan Name : </b><?php echo $directorymember['Directoryplan']['plan_name']; ?>"> <?php echo $directorymember['Directorymember']['plan_id']; ?></a>
				    </div>
					<div><?php echo $directorymember['Directorymember']['category']; ?></div>
					<div class="textcenter" style="width: 40%;">
						<p><b>Purchase Date : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $directorymember['Directorymember']['create_date']); ?></p>
						<a href="<?php echo $directorymember['Directorymember']['site_url'];?>" target="_blank" class="vtip" title="<b>Title : </b><?php echo $directorymember['Directorymember']['title']; ?>">
							<?php
								if($directorymember['Directoryplan']['banner_size']=='125x125'){$width="50px";$maxwidth='125px';}
								elseif($directorymember['Directoryplan']['banner_size']=='468x60'){$width="90%";$maxwidth="468px";}
								else{$width="98%";$maxwidth="728px";}
								echo '<img src="'.$directorymember['Directorymember']['banner_url'].'" alt="'.$directorymember['Directorymember']['title'].'" width="'.$width.'" style="max-width:'.$maxwidth.';" />';
							?>
						</a>
						<p><b>Approved Date : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $directorymember['Directorymember']['approve_date']); ?></p>
					</div>
					<div><?php echo $directorymember['Directorymember']['display_counter']; ?></div>
					<div><?php echo $directorymember['Directorymember']['click_counter']; ?></div>
					<div>
								<span class="actionmenu">
								  <span class="btn-group">
									<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
										Action <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
										
												<?php if(!isset($SubadminAccessArray) || in_array('directory',$SubadminAccessArray) || in_array('directory/adminpanel_add/$', $SubadminAccessArray)){ ?>
											<li>
												<?php echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>'Edit Biz Directory'))." Edit", array('controller'=>'directory', "action"=>"add/".$planid."/".$directorymember['Directorymember']['id']), array(
													'update'=>'#directorypage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>''
												));?>
											</li>
											<?php } ?>
										
										   <?php if(!isset($SubadminAccessArray) || in_array('directory',$SubadminAccessArray) || in_array('directory/adminpanel_planmemberstatus', $SubadminAccessArray)){ ?>
										   
										   <li>
												<?php 
													if($directorymember['Directorymember']['display_status']==0){
														$pauseaction='1';
														$pauseicon='pause.png';
														$pausetext='Unpause';
													}else{
														$pauseaction='0';
														$pauseicon='play.png';
														$pausetext='Pause';}
													echo $this->Js->link($this->html->image($pauseicon, array('alt'=>$pausetext)).' '.$pausetext, array('controller'=>'directory', "action"=>"pause/".$planid."/".$pauseaction."/".$directorymember['Directorymember']['id']."/".$currentpagenumber), array(
														'update'=>'#directorypage',
														'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
														'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
														'escape'=>false
													));
												?>
											</li>
										   
										   <li>
												<?php
												if($directorymember['Directorymember']['status']==0){
													$statusaction='1';
													$statusicon='red-icon.png';
													$statustext='Approve';
												}else{
													$statusaction='0';
													$statusicon='blue-icon.png';
													$statustext='Disapprove';}
												echo $this->Js->link($this->html->image($statusicon, array('alt'=>$statustext))." ".$statustext, array('controller'=>'directory', "action"=>"planmemberstatus/".$planid."/".$statusaction."/".$directorymember['Directorymember']['id']."/".$currentpagenumber), array(
													'update'=>'#directorypage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>''
												));?>
											</li>
										   
										   <?php } ?>
										   
											<?php if(!isset($SubadminAccessArray) || in_array('directory',$SubadminAccessArray) || in_array('directory/adminpanel_planmemberremove', $SubadminAccessArray)){ ?>
											<li>
												<?php echo $this->Js->link($this->html->image('delete.png', array('alt'=>'Delete Biz Directory'))." Delete", array('controller'=>'directory', "action"=>"planmemberremove/".$planid."/".$directorymember['Directorymember']['id']."/".$currentpagenumber), array(
													'update'=>'#directorypage',
													'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
													'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
													'escape'=>false,
													'class'=>'',
													'confirm'=>"Do You Really Want to Delete This Biz Directory?"
												));?>
											</li>
											<?php } ?>
									</ul>
								  </span>
								</span>
					</div>
					<div class="checkbox">
							<?php
							if($directorymember['Directorymember']['status']==0)
							{
								echo $this->Form->checkbox('bannerIds.', array(
								  'value' => $directorymember['Directorymember']['id'],
								  'id'=>'bannerIds'.$directorymember['Directorymember']['id'],
								  'class' => 'bannerIds',
								  'hiddenField' => false
								));
							echo '<label for="bannerIds'.$directorymember['Directorymember']['id'].'"></label>';
							}
							?>
					</div>
				</div>
			<?php endforeach; ?>
	</div>
	<?php if(count($directorymembers)==0){ echo '<div class="norecordfound">No records available</div>';} ?>
    <?php echo $this->Form->end();
	if($this->params['paging']['Directorymember']['count']>$this->Session->read('pagerecord'))
	{?>
	<div class="result-text margintop19"><?php echo "Result Per Page";?> :</div>
	<div class="floatleft margintop19">
		<?php echo $this->Form->create('Directorymember',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'directory','action'=>'planmember/'.$planid.'/rpp')));?>
		<div class="resultperpage">
			<label>
			<?php 
			echo $this->Form->input('resultperpage', array(
			  'type' => 'select',
			  'options' => $resultperpage,
			  'selected' => $this->Session->read('pagerecord'),
			  'class'=>'',
			  'label' => false,
			  'div'=>false,
			  'style' => '',
			  'onchange'=>'document.getElementById("resultperpageapply").style.display=""'
			));
			?>
			</label>
		</div>
		<span id="resultperpageapply" style="display:none;">
			<?php echo $this->Js->submit('Apply', array(
			  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#directorypage',
			  'class'=>'',
			  'div'=>false,
			  'controller'=>'directory',
			  'action'=>'planmember/'.$planid.'/rpp',
			  'url'   => array('controller' => 'directory', 'action' => 'planmember/'.$planid.'/rpp')
			));?>
		</span>
		<?php echo $this->Form->end();?>
	</div>
	<?php }?>
	<div class="floatright">
	<ul class="nice_paging">
		<?php echo $this->Paginator->first($this->html->image('left-arrow.png', array('alt'=>'<< First')), array('tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->prev($this->html->image('left-arrow-1.png', array('alt'=>'< Previous')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
		<?php echo $this->Paginator->next($this->html->image('right-arrow-1.png', array('alt'=>'> Next')), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
		<?php echo $this->Paginator->last($this->html->image('right-arrow.png', array('alt'=>'>> Last')), array('tag'=>'li', 'escape'=>false));?>
	</ul>
	</div>
	<div class="clear-both"></div>
	<div class="height10"></div>
	
	</div>
</div>
<?php }else{
	?><div class="accessdenied"><?php echo "Access Denied";?></div><?php
}?>
<?php if(!$ajax){?>
</div><!--#directorypage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>