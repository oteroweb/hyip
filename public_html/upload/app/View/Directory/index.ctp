<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 12-12-2014
  * Last Modified: 12-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($EnableDirectory==1) { ?>
<?php if(!$ajax){ ?>
<div id="Directorypage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<?php // Code for showing free available plans start
if(isset($pending_free_plans) && count($pending_free_plans)){
?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Free Plans Available");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php //free Plans table starts here ?>
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam"><?php echo __('Plan Name');?></div>
				<div class="divth textcenter vam"><?php echo __("Action");?></div>
			</div>
	        </div>
	        <div class="divtbody">
		        <tr><td colspan="8" class="ovw-padding-tabal"></td></tr>
		        <?php
		        $i = 0;
		        foreach ($pending_free_plans as $pending_free_plan):
			       if($i%2==0){$class='white-color';}else{$class='gray-color';}
			       ?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $pending_free_plan['Directoryplan']['plan_name'];?></div>
					<div class="divtd textcenter vam">
					<?php
						echo $this->Js->link('<span class="purchasebutton">Purchase</span>', array('controller'=>'directory', "action"=>"getfree/".$pending_free_plan['Pending_free_plan']['id']), array(
							'update'=>'#Directorypage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Get it')
						));
					?>
					</div>
				</div>
		        <?php $i++;endforeach; ?>
	       </div>
	</div>
	<?php //free Plans table ends here ?>
</div>
<div class="height5"></div>
<?php } // Code for showing free available plans end ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('Business Directory Plans');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<?php // Popup Login Ads table starts here ?>
	<div class="divtable">
		<div class="divthead">
			<div class="divtr tabal-title-text">
				<div class="divth textcenter vam"><?php echo __('Plan Name');?></div>
				<div class="divth textcenter vam"><?php echo __('Price');?></div>
				<div class="divth textcenter vam"><?php echo __("Action");?></div>
			</div>
		</div>
		<div class="divtbody">
			<?php
			$i = 0;
			foreach ($Directoryplan as $Directoryplandata):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}
				?>
				<div class="divtr <?php echo $class;?>">
					<div class="divtd textcenter vam"><?php echo $Directoryplandata['Directoryplan']['plan_name'];?></div>
					<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo round($Directoryplandata['Directoryplan']['price']*$Currency['rate'],2)." ".$Currency['suffix'];?></div>
					<div class="divtd textcenter vam">
					<?php
						echo $this->Js->link('<span class="purchasebutton">Purchase</span>', array('controller'=>'directory', "action"=>"purchase/".$Directoryplandata['Directoryplan']['id']), array(
							'update'=>'#Directorypage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Purchase')
						));
					?>
					</div>
				</div>
			<?php $i++;endforeach; ?>
		</div>
	</div>
	<?php if(count($Directoryplan)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
</div>
	<?php // Popup Login Ads table ends here ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('My Business Directory');?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
	<?php
	$this->Paginator->options(array(
		'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
		'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
		'update' => '#Directorypage',
		'evalScripts' => true,
		'url'=> array('controller'=>'directory', 'action'=>'index')
	));
	$currentpagenumber=$this->params['paging']['Directorymember']['page'];
	?>
		<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
		<div class="clear-both"></div>
		<div class="height5"></div>
			
		<?php // My Popup Login ads table starts here ?>
		<div class="divtable">
			<div class="divthead">
				<div class="divtr tabal-title-text">
					<div class="divth textcenter vam">
						<?php 
						if($ordertype=="desc")$sorttype="asc";else $sorttype="desc";
						echo $this->Js->link(__("Id"), array('controller'=>'directory', "action"=>"index/0/id/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Directorypage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Id')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Plan"), array('controller'=>'directory', "action"=>"index/0/plan_id/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Directorypage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Plan')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Purchase Date"), array('controller'=>'directory', "action"=>"index/0/create_date/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Directorypage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Purchase Date')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Displayed"), array('controller'=>'directory', "action"=>"index/0/display_counter/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Directorypage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Displayed')
						));?>
					</div>
					<div class="divth textcenter vam">
						<?php 
						echo $this->Js->link(__("Clicked"), array('controller'=>'directory', "action"=>"index/0/click_counter/".$sorttype."/".$currentpagenumber), array(
							'update'=>'#Directorypage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false,
							'class'=>'vtip',
							'title'=>__('Sort By').' '.__('Clicked')
						));?>
					</div>
					<div class="divth textcenter vam"><?php echo __("Status"); ?></div>
					<div class="divth textcenter vam"><?php echo __("Action"); ?></div>
				</div>
			</div>
			<div class="divtbody">
				<?php $i=1;
				foreach ($directorymembers as $directorymember):
				if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $directorymember['Directorymember']['id'];?></div>
						<div class="divtd textcenter vam">
							<a href="#" class="vtip" title="<b><?php echo __("Plan Name"); ?> : </b><?php echo $directorymember['Directoryplan']['plan_name']; ?><br><b><?php echo __("Banner Size"); ?> : </b><?php echo $directorymember['Directorymember']['banner_size']; ?>"> <?php echo $directorymember['Directorymember']['plan_id']; ?></a>
						</div>
						<div class="divtd textcenter vam">
							<p><b><?php echo __("Purchase Date"); ?> : </b><?php echo $this->Time->format($SITECONFIG["timeformate"], $directorymember['Directorymember']['create_date']); ?></p>
								<a href="<?php echo $directorymember['Directorymember']['site_url'];?>" target="_blank" class="vtip" title="<b><?php echo __("Title"); ?> : </b><?php echo $directorymember['Directorymember']['title']; ?>">
									<?php
										if($directorymember['Directorymember']['banner_size']=='125x125'){$height="50px";$maxwidth='';}
										elseif($directorymember['Directorymember']['banner_size']=='468x60'){$height="80%";$maxwidth="468px";}
										else{$height="80%";$maxwidth="728px";}
										echo '<img src="'.$directorymember['Directorymember']['banner_url'].'" alt="'.$directorymember['Directorymember']['title'].'" width="'.$height.'" style="max-width:'.$maxwidth.';" />';
									?>
								</a>
								<div class="noborderonmobile"><b><?php echo __("Approved Date"); ?> : </b><?php if($directorymember['Directorymember']['approve_date'] != '0000-00-00'){ echo $this->Time->format($SITECONFIG["timeformate"], $directorymember['Directorymember']['approve_date']); } else { echo '-';}?></div>
						</div>
						<div class="divtd textcenter vam"><?php echo $directorymember['Directorymember']['display_counter'];?></div>
						<div class="divtd textcenter vam"><?php echo $directorymember['Directorymember']['click_counter'];?></div>
						<div class="divtd textcenter vam">
							<?php 
							if($directorymember['Directorymember']['total_click']<=$directorymember['Directorymember']['click_counter'])
								echo __("Expired");
							elseif($directorymember['Directorymember']['status']==0)
								echo __("Pending");
							elseif($directorymember['Directorymember']['display_status']==0)
								echo __("Paused");
							else
								echo __("Running");
							?>
						</div>
						<div class="divtd textcenter vam">
							
							<div class="actionmenu">
								<div class="btn-group">
								  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
									  Action <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu">
									  <li>
									      <?php 
											if($directorymember['Directorymember']['display_status']==0){
												$pauseaction='1';
												$pauseicon='pause.png';
												$pausetext='Unpause';
											}else{
												$pauseaction='0';
												$pauseicon='play.png';
												$pausetext='Pause';}
											echo $this->Js->link($this->html->image($pauseicon, array('alt'=>__($pausetext)))." ".__($pausetext), array('controller'=>'directory', "action"=>"status/".$pauseaction."/".$directorymember['Directorymember']['id']."/".$currentpagenumber), array(
												'update'=>'#Directorypage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));
										?>
									</li>
									 <li>
										<?php
											echo $this->Js->link($this->html->image('men-icon.png', array('alt'=>__('Edit')))." ".__('Edit'), array('controller'=>'directory', "action"=>"add/".$directorymember['Directorymember']['id']), array(
												'update'=>'#Directorypage',
												'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
												'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
												'escape'=>false,
												'class'=>'',
												'title'=>''
											));
										?>
									 </li>
								   </ul>
								</div>
							</div>
						  
						</div>
					</div>
				<?php $i++;endforeach; ?>
			</div>
		</div>
		<?php if(count($directorymembers)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
		<?php // My Popup Login ads table ends here ?>
	<?php // Paging code starts here ?>
	<?php $pagerecord=$this->Session->read('pagerecord');
	if($this->params['paging']['Directorymember']['count']>$pagerecord)
	{?>
	<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
	<div class="pag-float-left">
		<div class="ul-bg">
			<ul class="nice_paging">
			<?php 
			foreach($resultperpage as $rpp)
			{
				?>
				<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
					<?php 
					echo $this->Form->create('Directoryplan',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'directory','action'=>'index/rpp')));
					echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
				
					echo $this->Js->submit($rpp, array(
					  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					  'update'=>'#Directorypage',
					  'class'=>'resultperpagebutton',
					  'div'=>false,
					  'controller'=>'directory',
					  'action'=>'index/rpp',
					  'url'   => array('controller' => 'directory', 'action' => 'index/rpp')
					));
					echo $this->Form->end();
					?>
				</li>
				<?php 
			}?>
			</ul>
		<div class="clear-both"></div>
		</div>
	</div>
	<?php }?>
	<div class="floatright ul-bg">
		<ul class="nice_paging">
			<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
			<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
			<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
		</ul>
	</div>
	<div class="clear-both"></div>
	<?php // Paging code ends here ?>
	
</div>

<?php if(!$ajax){ ?>
</div>
<?php } ?>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>