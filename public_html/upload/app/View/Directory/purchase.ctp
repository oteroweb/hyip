<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 12-12-2014
  * Last Modified: 12-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('framechecker');?>
<?php if($EnableDirectory==1) { ?>
<?php if(!$ajax){ ?>
<div id="Directorypage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<script> processer=new Array();
	<?php
	$firstproceser=0;
	foreach($Processorsextrafield as $proid=>$provalue)
	{
	?>
	processer[<?php echo $proid; ?>]=<?php echo $provalue; ?>;
	<?php }?>
</script>

<?php //Frame Break Code Start ?>
<input type="hidden" id='framebreakmessage' value='<?php echo __('Destination URL Not Valid'); ?>|<?php echo __('Breaking out of frames'); ?>|<?php echo __('Inspecting Website'); ?>|<?php echo __('Framebreaker test passed'); ?>|<?php echo __('Error').' : '; ?>'/>
<div id="check" class="frambreakmain">
	<div class='textleft'>
		<span id='framebreakimg'></span>
		<span class="framebreaktext"><?php echo __('Inspecting Framebreaker'); ?></span>
	</div>
	<div class='textleft'>
		<span id="check_final"><?php echo __('Inspecting Website'); ?></span>
	</div>
</div>
<?php //Frame Break Code Over ?>

<?php // Current Balance code starts here ?>
<div class="textright"><a class="shbutton currentbalancebutton" href="javascript:void(0)" onclick="currentbalancebox('.balancebox', this,'<?php echo __("[+] Show Balance");?>','<?php echo __("[-] Hide Balance");?>');"><?php if(@$_COOKIE['curbal']==1)echo __("[+] Show Balance"); else echo __("[-] Hide Balance");?>
</a></div>
<div class="comisson-bg balancebox <?php if(@$_COOKIE['curbal']==1)echo 'hide';?>">
	<div class="text-ads-title-text"><?php echo __("Current Balance");?></div>
	<div class="text-ads-title-text-right"></div>
	<div class="clear-both"></div>
</div>
<div id="balancebox" class="main-box-eran balancebox <?php if(@$_COOKIE['curbal']==1)echo 'hide';?>">
	<?php if($SITECONFIG["balance_type"]==1){ ?>
		<div class="divtable">
                        <div class="divtr white-color">
                                <div class="divth textcente vam"><?php echo __("Cash Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCash*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
                        <div class="divtr gray-color">
                                <div class="divth textcente vam"><?php echo __("Earning Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberEarning*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php } ?>
                        <div class="divtr white-color">
                                <div class="divth textcente vam"><?php echo __("Re-purchase Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberRepurchaseCash*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
                        <div class="divtr gray-color">
                                <div class="divth textcente vam"><?php echo __("Commission Balance");?> : </div>
                                <div class="divtd textcente vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCommission*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                        </div>
                    <?php } ?>
                </div>
		<?php }else{ 
			?>
			<div class="divtable">
				<div class="divthead">
					<div class="divtr tabal-title-text">
						<div class="divth textcenter vam"><?php echo __("Payment Processor");?></div>
						<div class="divth textcenter vam"><?php echo __("Cash Balance");?></div>
                                                <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
						<div class="divth textcenter vam"><?php echo __("Earning Balance");?></div>
                                                <?php } ?>
						<div class="divth textcenter vam"><?php echo __("Re-purchase Balance");?></div>
                                                <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
						<div class="divth textcenter vam"><?php echo __("Commission Balance");?></div>
                                                <?php } ?>
					</div>
				</div>
				<?php $i=1;
				foreach($MemberCash as $proc_name=>$cash)
				{
					if($i%2==0){$class='white-color';}else{$class='gray-color';}?>
					<div class="divtr <?php echo $class;?>">
						<div class="divtd textcenter vam"><?php echo $proc_name;?> :</div>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCash[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php if($SITECONFIG["wallet_for_earning"] == 'earning'){ ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberEarning[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php } ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberRepurchaseCash[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php if($SITECONFIG["wallet_for_commission"] == 'commission'){ ?>
						<div class="divtd textcenter vam"><?php echo $Currency['prefix'];?><?php echo AppController::truncate_number($MemberCommission[$proc_name]*$Currency['rate'],3); ?> <?php echo " ".$Currency['suffix'];?></div>
                                                <?php } ?>
					</div>
					<?php
				$i++; }?>
			</div>
	<?php } ?>
</div>
<?php // Current Balance code ends here ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Purchase Business Directory");?></div>
	<div class="clear-both"></div>
</div>
<div class="height10"></div>
	<div class="main-box-eran">
		<?php // Banner ad plan purchase form starts here ?>
		<?php echo $this->Form->create('Directorymember',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'directory','action'=>'purchasepay/'.$Directoryplandata['Directoryplan']['id'])));?>
		<div class="purchaseplanmain">	
		<div class="form-box">
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Plan Name");?> : </div>
				<div class="form-col-2 form-text"><?php echo $Directoryplandata['Directoryplan']['plan_name'];?></div>
			</div>
			<div class="form-row ">
				<div class="form-col-1"><?php echo __("Price");?> : </div>
				<div class="form-col-2 form-text">
					<?php echo $Currency['prefix'];?><?php echo round($Directoryplandata['Directoryplan']['price']*$Currency['rate'],2)." ".$Currency['suffix'];?>
					<span id="couponspan">
					<?php
						$payamount = round($Directoryplandata['Directoryplan']['price']*$Currency['rate'],2);
						if($this->Session->check('coupon'))
						{
							$sessioncoupon=$this->Session->read('coupon');
							if($sessioncoupon['page']=='directory' && $sessioncoupon['planid']==$Directoryplandata['Directoryplan']['id'])
							{
								echo " - <span>".$Currency['prefix'].round($sessioncoupon['amount']*$Currency['rate'],4)." ".$Currency['suffix']."</span> = <span>".$Currency['prefix'].round(($Directoryplandata['Directoryplan']['price']-$sessioncoupon['amount'])*$Currency['rate'],4)." ".$Currency['suffix']."</span>";
							}
						}
					?>
					</span>
					<?php echo $this->Form->input('payamount', array('type'=>'hidden', 'id'=>'payamount', 'value'=>$payamount, 'div'=>false, 'label' => false, 'class'=>'payamount', 'onchange'=>''));?>
				<div class="height7"></div></div>
			</div>
				<?php if($Directoryplandata['Directoryplan']['ptype']==0){ ?>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Clicks");?> : </div>
					<div class="form-col-2 form-text"><?php echo $Directoryplandata['Directoryplan']['total_click'].' '.__('Clicks');?></div>
				</div>
				<?php } ?>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Type");?> : </div>
					<div class="form-col-2 form-text"><?php echo ($Directoryplandata['Directoryplan']['ptype']==0)? __('Surfing Directory') : __('Life Time Directory') ;?></div>
				</div>
				
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Banner Size");?> : </div>
					<div class="form-col-2 form-text"><?php echo $Directoryplandata['Directoryplan']['banner_size'];?></div>
				</div>
				<?php $commissionlevel=explode(",", $Directoryplandata['Directoryplan']['commissionlevel']);if($commissionlevel[0]>0){?>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Referral Commission Structure");?> : </div>
					<div class="form-col-2 form-text">
						<?php $commilev="";for($i=0;$i<count($commissionlevel);$i++){if($commissionlevel[$i]>0){$commilev.="Level ".($i+1)." : ".$commissionlevel[$i]."%, ";}else{break;}}echo trim($commilev,", ")?>
					</div>
				</div>
				<?php }?>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Topic");?> :</div>
						<div class="select-dropdown">
							<label>
								<?php
								echo $this->Form->input('category', array(
									  'type' => 'select',
									  'options' => $Directorycategory,
									  'selected' => '',
									  'class'=>'',
									  'label' => false,
									  'div' => false
								));?>
							</label>
						</div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Title");?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('title', array('type'=>'text', 'label' => false, 'div'=>false, 'class'=>'formtextbox'));?>	
						<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)') ?>"></span>
					</div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Banner URL");?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('banner_url', array('type'=>'text', 'label' => false,'onblur'=>'if(this.value!=""){document.getElementById("img1").src = this.value;$("#img1").show();}else{$("#img1").hide();}', 'class'=>'formtextbox'));?><img src="" id="img1" class="hide" /></div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Destination URL");?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('site_url', array('type'=>'text', 'label' => false, 'class'=>'formtextbox txtframebreaker', 'value' => 'http://'));?></div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Description");?> : <span class="required">*</span></div>
					<div class="form-col-2"><?php echo $this->Form->input('description', array('type'=>'textarea', 'label'=>false,'title'=>__('Description'), 'class'=>'formtextarea '));?></div>
				</div>
			<div class="form-row">
				<div class="form-col-1"><?php echo __("How would you like to pay");?> : </div>
				<div class="form-col-2 form-text balance-width">
					
					<?php
						$paymentmethod=array();
						$allmethod=array('cash','repurchase', 'earning', 'commission', 'processor', 'ca:re', 'ca:ea', 'ca:co', 're:ea', 're:co', 'ea:co', 'ca:re:ea', 're:ea:co', 'ea:co:ca', 'co:ca:re', 'ca:ea:re:co');
						$methodarray=@explode(",",$Directoryplandata['Directoryplan']['paymentmethod']);
						
						$find=array('ca', 'ea', 're', 'co', ':');
						$replace=array('Cash Balance', 'Earning Balance', 'Re-purchase Balance', 'Commission Balance', ' + ');
								
						foreach($methodarray as $methodnm)
						{
							if(in_array($methodnm,$allmethod))
							{
								if(!in_array($methodnm,array('cash','repurchase', 'earning', 'commission', 'processor')))
									$methodnmdisp=str_replace($find, $replace, $methodnm);
								elseif($methodnm=='processor')
									$methodnmdisp=ucfirst($methodnm);
								elseif($methodnm=='repurchase')
									$methodnmdisp="Re-purchase Balance";
								else
									$methodnmdisp=ucfirst($methodnm)." Balance";
								$paymentmethod[$methodnm]=__($methodnmdisp);
							}
						}
						//$paymentmethod=array();
						//if(strpos($Directoryplandata['Directoryplan']['paymentmethod'],'cash') !== false)
						//	$paymentmethod['cash']=__('Cash Balance');
						//if(strpos($Directoryplandata['Directoryplan']['paymentmethod'],'repurchase') !== false)
						//	$paymentmethod['repurchase']=__('Re-purchase Balance');
						//if(strpos($Directoryplandata['Directoryplan']['paymentmethod'],'earning') !== false)
						//	$paymentmethod['earning']=__('Earning Balance');
						//if(strpos($Directoryplandata['Directoryplan']['paymentmethod'],'commission') !== false)
						//	$paymentmethod['commission']=__('Commission Balance');
						//if(strpos($Directoryplandata['Directoryplan']['paymentmethod'],'processor') !== false)
						//	$paymentmethod['processor']=__('Payment Processor');
						if($SITECONFIG['balance_type']==1)
						{
							echo $this->Form->radio('paymentmethod', $paymentmethod, array('value'=>'cash', 'legend' => false, 'separator'=>'&nbsp;', 'onchange'=>'if(this.value=="processor") {$(".paymentprocessorfield").show(500);} else{$(".paymentprocessorfield").hide(500);} if(this.value=="processor"){processorextrafield($("#paymentprocessors").val(),processer,"'.$SITEURL.'app/processorextrafield","Directorymember",".extrafield");showprocfee(1);}else{$(".extrafield").html("");showprocfee(0);}if(this.value.indexOf("re:")>=0 || this.value.indexOf(":re")>=0) {$(".refirstfield").show(500);} else{$(".refirstfield").hide(500);}'));
						}
						elseif($SITECONFIG['balance_type']==2)
						{
							echo $this->Form->radio('paymentmethod', $paymentmethod, array('value'=>'cash', 'legend' => false, 'separator'=>'&nbsp;', 'onchange'=>' if(this.value=="processor"){processorextrafield($("#paymentprocessors").val(),processer,"'.$SITEURL.'app/processorextrafield","Directorymember",".extrafield");showprocfee(1);}else{$(".extrafield").html("");showprocfee(0);}if(this.value.indexOf("re:")>=0 || this.value.indexOf(":re")>=0) {$(".refirstfield").show(500);} else{$(".refirstfield").hide(500);}'));
						}
					?>
					<div class="height7"></div>
					<div class="refirstfield" style="display: none;"><?php echo $this->Form->input('refirst', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>'')); echo __('Use Re-purchase Balance first'); ?></div>
				</div>
			</div>
			
			<div class="form-row paymentprocessorfield" style="display:<?php if($SITECONFIG['balance_type']==1){echo 'none';}?>;">
				<div class="form-col-1"><?php echo __("Please select a payment processor");?> : </div>
					<?php
						$procfee_string = "var procfee_array = new Array();var procfeeamount_array = new Array();";
						foreach($processorsfee as $key=>$value)
						{
							$procfee_string.=" procfee_array[".$key."] = ".$value['fee'].";";
							$procfee_string.=" procfeeamount_array[".$key."] = ".$value['feeamount'].";";
						}
					?>
					<script type='text/javascript'><?php echo $procfee_string;?></script>
					
					<div class="select-dropdown">
							<label>
								<?php 
								echo $this->Form->input('paymentprocessor', array(
									  'type' => 'select',
									  'options' => $paymentprocessors,
									  'selected' => '',
									  'class'=>'paymentprocessor',
									  'label' => false,
									  'div' => false,
									  'id'=>'paymentprocessors',
									  'onchange' => 'if($("#DirectorymemberPaymentmethodProcessor").attr("checked")){processorextrafield(this.value,processer,"'.$SITEURL.'app/processorextrafield","Directorymember",".extrafield");showprocfee(1);}else{showprocfee(0);}'
								));?>
							</label>
					</div>
					
					<span class="helptooltip vtip" title="<?php echo __('Fees is added to the amount when you pay through payment processor.') ?>"></span>
			</div>
			<div class='extrafield'></div>
		</div>
		<div class="form-row">
			<div class="form-col-1 form-text"><?php echo __("Show URL in Zone");?> : </div>
			<?php 
					echo $this->Form->input('show_url', array('type' => 'checkbox', 'div' => false, 'label' =>''));?>
					<span class="helptooltip vtip" title="<?php echo __('Choose whether the site URL will be shown with the Business Directory or not.') ?>"></span>
			
		</div>
		<div class="height10"></div>
		<div>
			<?php // Amount calculation box starts here ?>
			<div class="purchaseplanamountdetails floatleft" style="width:49.5%">
				<div class="form-box">
					<div class="form-row">
						<div class="form-col-1"><?php echo __("Amount");?> : </div>
						<div class="form-col-2 form-text"><?php echo $Currency['prefix'];?><span id="payableamount"><?php echo $payamount;?></span> <?php echo $Currency['suffix'];?></div>
					</div>
					
					<div class="form-row">
						<div class="form-col-1"><?php echo __("Fees");?> : </div>
						<div class="form-col-2 form-text"><span id="procfee">0</span>% + <?php echo $Currency['prefix'];?><span id="procfeeamount">0</span> <?php echo $Currency['suffix'];?></div>
					</div>
					
					<div class="form-row">
						<div class="form-col-1"><?php echo __("Final Amount");?> : </div>
						<div class="form-col-2 form-text"><?php echo $Currency['prefix'];?><span id="finalamount"><?php echo $payamount;?></span> <?php echo $Currency['suffix'];?></div>
					</div>
				</div>	
			</div>
			<?php // Amount calculation box ends here ?>
			
			<div class="purchaseplanbuttonbox floatright" style="width:50%;">
				<div class="form-box">
					<div class="form-row captchrow">
						<div class="profile-bot-left inlineblock" style="padding-left: 0px;padding-top: 7px;">
							<?php if($Directoryplandata['Directoryplan']['iscoupon']==1){?>
							<div class="form-col-1 forcaptchaonly"><?php echo __('Enter Coupon Code'); ?> : &nbsp;</div>
							<?php $couponclass='formtextbox coupontextbox';
							if(isset($sessioncoupon) && $sessioncoupon['page']=='directory' && $sessioncoupon['planid']==$Directoryplandata['Directoryplan']['id']) $couponclass.=' greenbg'; else $sessioncoupon['code']='';
							echo $this->Form->input('code', array('type'=>'text', 'div'=>false, 'label' => false, 'style'=>'width:45%;', 'class'=>$couponclass, 'value'=>$sessioncoupon['code']));?>
							<?php echo $this->Js->submit(__('Apply Coupon'), array(
								'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'update'=>'#UpdateMessage',
								'class'=>'button floatnone',
								'div'=>false,
								'controller'=>'directory',
								'action'=>'coupon/'.$Directoryplandata['Directoryplan']['id'],
								'url'   => array('controller' => 'directory', 'action' => 'coupon/'.$Directoryplandata['Directoryplan']['id'])
							));?>
							<?php }?>
						</div>
						<div class="profile-bot-left inlineblock" style="font-size: 12px;"><?php if($SITECONFIG["enableagree"]==1){echo $this->Form->input('termsandconditions', array('type' => 'checkbox', 'div'=>false, 'label' =>'', 'checked'=>'')); echo ' <a href="'.$SITEURL.'public/terms" target="_blank">'.__('I agree with the terms and conditions').'</a> '.__('of').' '.$SITECONFIG["sitetitle"]; }?></div>
						<div class="clear-both"></div>
					</div>
					
					<div class="formbutton">
							<input type="button" value="<?php echo __('Purchase'); ?>" class="button ml10" onclick="return checkframebreaker(0,'<?php echo $SITEURL; ?>','framebreaker');" />
							<?php echo $this->Js->submit(__('Purchase'), array(
								'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'update'=>'#UpdateMessage',
								'class'=>'button framebreaker',
								'div'=>false,
								'controller'=>'directory',
								'style'=>'display:none',
								'action'=>'purchasepay',
								'url'   => array('controller' => 'directory', 'action' => 'purchasepay/'.$Directoryplandata['Directoryplan']['id'])
							));?>
							<?php echo $this->Js->link(__("Back"), array('controller'=>'directory', "action"=>"index"), array(
								'update'=>'#Directorypage',
								'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
								'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
								'escape'=>false,
								'class'=>'button',
								'style'=>''
							));?>
					</div>
					<div class="clear-both"></div>
				</div>
			</div>
			<div class="clear-both"></div>
		</div>
	<div class="clear-both"></div>
	</div>
	<?php echo $this->Form->end();?>
	<?php // Banner ad plan purchase form ends here ?>
	
<?php if(!$ajax){ ?>
</div>
<?php } ?>	
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>