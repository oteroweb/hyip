<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 14-12-2014
  * Last Modified: 14-12-2014
  *********************************************************************/
?>
<?php if($Enabledirectory==1) { ?>
<?php if(!$ajax){ ?>
<div id="Directorypage">
<?php } ?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('View Business Directory');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran mobilespacing" style="text-align: center;">
	
<?php if($bannerdetail){ ?>	
	<div id="directoryid<?php echo $bannerview['Directorymember']['id'];?>" class="mainptcbox">
	
		
		<div class="ptccontent">
			<div class="directorymaininner">
				<div class="directoryleft">
					<div class="directoryimages">
						<?php
						if($bannerview['Member']['member_photo']==NULL || $bannerview['Member']['member_photo']=='')
						{
							echo $this->html->image("member/dummy.jpg", array('alt'=>''));
						}
						else
						{
							echo $this->html->image("member/".$bannerview['Member']['member_photo'].'?tk='.date('sHi'), array('class'=>'profilepicture','alt'=>'','id'=>'memberimg'));
						}
						?>
					</div>
					<div class="directorytitle"><?php echo $bannerview['Directorymember']['title'];?></div>
				</div>
				<div class="directoryleft">
					<span><?php echo __('Clicks');?> : <?php echo $bannerview['Directorymember']['click_counter'];?></span>
					<span><?php echo __('Views'); ?> :<?php echo $bannerview['Directorymember']['display_counter'];?></span>
				</div>
			</div>
			
			<div class="ptcboxmiddle">
				<?php
				if($bannerview['Directorymember']['banner_url']!=''){
					if($bannerview['Directorymember']['banner_size']=='125x125')
					{ ?><a href="<?php echo $SITEURL;?>directory/viewallcounter/<?php echo $bannerview['Directorymember']['id'];?>" target="_blank"><?php  echo '<div class="onetwofivebox"><img src="'.$bannerview['Directorymember']['banner_url'].'" alt="" width="" height="" style="vertical-align: middle;padding-right:10px;" /></div>'; ?>
					</a>
					<?php }
					elseif($bannerview['Directorymember']['banner_size']=='468x60')
					{
						?><a href="<?php echo $SITEURL;?>directory/viewallcounter/<?php echo $bannerview['Directorymember']['id'];?>" target="_blank"><?php
						echo '<img src="'.$bannerview['Directorymember']['banner_url'].'" alt="" /></a>';
					}
					elseif($bannerview['Directorymember']['banner_size']=='728x90')
					{
						?><a href="<?php echo $SITEURL;?>directory/viewallcounter/<?php echo $bannerview['Directorymember']['id'];?>" target="_blank"><?php
						echo '<img src="'.$bannerview['Directorymember']['banner_url'].'" alt="" /></a>';
					}
				}
				?>
				<p <?php if($bannerview['Directorymember']['banner_size']=='125x125'){echo 'class="onetwofivetext"';}?>><?php echo nl2br(stripslashes($bannerview['Directorymember']['description']));?></p>
				<br>
					<?php if($bannerview['Directorymember']['show_url']==1){ echo $bannerview['Directorymember']['site_url'];} ?>
			</div>
		</div>
	</div>

	<?php if(count($bannerview)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
	
	<?php } else { // Search box code starts here ?>
<div class="floatleft searchlabel"><?php echo __("Advanced Search");?></div>
	<div class="floatright"><a href="javascript:void(0)" onclick="togglebox('.searchlabel', this,'<?php echo __("[+] Show Search Box");?>','<?php echo __("[-] Hide Search Box");?>');"><?php echo __("[-] Hide Search Box");?></a></div>
	<div class="clear-both"></div>
	<div class="advsearchbox searchlabel">
	<?php echo $this->Form->create('Directorymember',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'directory','action'=>'viewall')));?>
	<div class="textcenter">
		<div class="inlineblock vat">
			<div>
				<div class="select-dropdown smallsearch">
					<label>
						<?php echo $this->Form->input('ordertype', array(
								'type' => 'select',
								'options' => array('desc'=>__('Newest'), 'asc'=>__('Oldest')),
								'selected' => $ordertype,
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
						  ));?>
					</label>
				</div>
			</div>
		</div>
		<div class="inlineblock vat">
			<div>	
				<div class="select-dropdown smallsearch">
					<label>
						<?php
						$Directorycategory[0]="All Topics";
						ksort($Directorycategory);
						echo $this->Form->input('searchby', array(
								'type' => 'select',
								'options' => $Directorycategory,
								'selected' => $searchby,
								'class'=>'',
								'label' => false,
								'div' => false,
								'style' => ''
						  ));?>
					</label>
				</div>
			</div>
		</div>	
		<div class="inlineblock vat">
			<div>
				<?php echo $this->Form->input('searchfor', array('type'=>'text', 'id'=>'searchfor', 'value'=>$searchfor, 'label' => false,'placeholder'=>'Search Business Name', 'class'=>'finece-from-box searchfortxt','div' => false));?>
			</div>
		</div>
		
		<div class="textcenter margint5 inlineblock vat">
			<div>
				<?php echo $this->Js->submit(__('Update Results'), array(
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#Directorypage',
					'class'=>'add-new',
					'div'=>false,
					'controller'=>'directory',
					'action'=>'viewall',
					'url'   => array('controller' => 'directory', 'action' => 'viewall')
				));?>
			</div>
		</div>
		<div class="clear-both"></div>
	</div>
		
	<?php echo $this->Form->end();?>
	</div>
	<?php // Search box code ends here ?>
	
<?php
$this->Paginator->options(array(
	'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
	'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
	'update' => '#Directorypage',
	'evalScripts' => true,
	'url'=> array('controller'=>'directory', 'action'=>'viewall')
));
$currentpagenumber=$this->params['paging']['Directorymember']['page'];
?>
	<div class="activ-ad-pack"><?php echo $this->Paginator->counter(array('format' => __('Showing').' %page% '.' - '.' %current% '.__('Records').' | '.__('Total Records').' : %count%'));?></div>
	
	<div class="height10"></div>
<div class="clear-both"></div>
	<div class="maindirectorybox">
		<?php foreach ($Directorybanners as $Directorybanner): ?>
		
		<div id="directoryid<?php echo $Directorybanner['Directorymember']['id'];?>" class="mainptcbox">
			<div class="ptccontent">
				<div class="directorymaininner">
					<div class="directoryleft">
						<div class="directoryimages">
							<?php
							if($Directorybanner['Member']['member_photo']==NULL || $Directorybanner['Member']['member_photo']=='')
							{
								echo $this->html->image("member/dummy.jpg", array('alt'=>''));
							}
							else
							{
								echo $this->html->image("member/".$Directorybanner['Member']['member_photo'].'?tk='.date('sHi'), array('class'=>'profilepicture','alt'=>'','id'=>'memberimg'));
							}
							?>	
						</div>
						<div class="directorytitle"><?php echo $Directorybanner['Directorymember']['title'];?></div>
					</div>
					<div class="directoryleft">
						<span><?php echo __('Clicks');?> : <?php echo $Directorybanner['Directorymember']['click_counter'];?></span>
						<span><?php echo __('Views'); ?> : <?php echo $Directorybanner['Directorymember']['display_counter'];?></span>
					</div>
				</div>
				
				
				<div class="ptcboxmiddle">
					<?php
					if($Directorybanner['Directorymember']['banner_url']!=''){
						if($Directorybanner['Directorymember']['banner_size']=='125x125')
						{ ?><a href="<?php echo $SITEURL;?>directory/viewall/<?php echo $Directorybanner['Directorymember']['id'];?>" target="_blank"><?php  echo '<div class="onetwofivebox"><img src="'.$Directorybanner['Directorymember']['banner_url'].'" alt="" width="" height="" style="vertical-align: middle;" /></div>'; ?>
						</a>
						<?php }
						elseif($Directorybanner['Directorymember']['banner_size']=='468x60')
						{
							?><a href="<?php echo $SITEURL;?>directory/viewall/<?php echo $Directorybanner['Directorymember']['id'];?>" target="_blank"><?php
							echo '<img src="'.$Directorybanner['Directorymember']['banner_url'].'" alt="" /></a>';
						}
						elseif($Directorybanner['Directorymember']['banner_size']=='728x90')
						{
							?><a href="<?php echo $SITEURL;?>directory/viewall/<?php echo $Directorybanner['Directorymember']['id'];?>" target="_blank"><?php
							echo '<img src="'.$Directorybanner['Directorymember']['banner_url'].'" alt="" /></a>';
						}
					}
					?>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
		<?php if(count($Directorybanners)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
		<div class="clear-both"></div>
		<?php // Paging code starts here ?>
		<?php $pagerecord=$this->Session->read('pagerecord');
		if($this->params['paging']['Directorymember']['count']>$pagerecord)
		{?>
		<div class="taxt-pag"><?php echo __("Results Per Page");?> :</div>
		<div class="pag-float-left">
			<div class="ul-bg">
				<ul class="nice_paging">
				<?php 
				foreach($resultperpage as $rpp)
				{
					?>
					<li <?php if($pagerecord==$rpp)echo 'class="current"';?>>
						<?php 
						echo $this->Form->create('Directorymember',array('type' => 'post', 'onsubmit' => 'return false;','url'=>array('controller'=>'directory','action'=>'viewall/0/rpp')));
						echo $this->Form->input('resultperpage', array('type'=>'hidden', 'value'=>$rpp, 'label' => false));
					
						echo $this->Js->submit($rpp, array(
						  'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#Directorypage',
						  'class'=>'resultperpagebutton',
						  'div'=>false,
						  'controller'=>'directory',
						  'action'=>'viewall/0/rpp',
						  'url'   => array('controller' => 'directory', 'action' => 'viewall/0/rpp')
						));
						echo $this->Form->end();
						?>
					</li>
					<?php 
				}?>
				</ul>
			<div class="clear-both"></div>
			</div>
		</div>
		<?php }?>
		<div class="floatright ul-bg">
			<ul class="nice_paging">
				<?php echo $this->Paginator->first(__('First'), array('tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->prev(__('Prev'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->numbers(array('modulus' => 5, 'first' => 1, 'last' => 1, 'separator' => false, 'ellipsis' => '<li>...</li>', 'tag'=>'li'));?>
				<?php echo $this->Paginator->next(__('Next'), array('tag'=>'li', 'escape'=>false), NULL, array('class' => 'disabled', 'tag'=>'li', 'escape'=>false));?>
				<?php echo $this->Paginator->last(__('Last'), array('tag'=>'li', 'escape'=>false));?>
			</ul>
		</div>
		<div class="clear-both"></div>
		<?php // Paging code ends here ?>
		<?php } ?>
	</div>
	
	<div class="clear-both"></div>
</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>	
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>