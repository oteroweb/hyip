<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 07-10-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php echo $this->Javascript->link('framechecker');?>
<?php if($EnableDirectory==1) { ?>
<?php if(!$ajax){ ?>
<div id="Directorypage">
<?php } ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>

<?php //Frame Break Code Start ?>
<input type="hidden" id='framebreakmessage' value='<?php echo __('Destination URL Not Valid'); ?>|<?php echo __('Breaking out of frames'); ?>|<?php echo __('Inspecting Website'); ?>|<?php echo __('Framebreaker test passed'); ?>|<?php echo __('Error').' : '; ?>'/>
<div id="check" class="frambreakmain">
	<div class='textleft'>
		<span id='framebreakimg'></span>
		<span class="framebreaktext"><?php echo __('Inspecting Framebreaker'); ?></span>
	</div>
	<div class='textleft'>
		<span id="check_final"><?php echo __('Inspecting Website'); ?></span>
	</div>
</div>
<?php //Frame Break Code Over ?>

<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Business Directory Details");?></div>
	<div class="clear-both"></div>
</div>
	<div class="main-box-eran">
		
		<div class="height10"></div>	
			
			<?php // Banner Ad Plan Details edit form starts here ?>
			<?php echo $this->Form->create('Directorymember',array('type' => 'post', 'onsubmit' => 'return false;', 'autocomplete'=>'off','url'=>array('controller'=>'directory','action'=>'addaction')));?>
			<div class="form-box">
				<?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$Directorymember['Directorymember']["id"], 'label' => false)); ?>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Topic");?> :</div>
						<div class="select-dropdown">
							<label>
								<?php
								echo $this->Form->input('category', array(
									  'type' => 'select',
									  'options' => $Directorycategory,
									  'selected' => $Directorymember['Directorymember']["category"],
									  'class'=>'',
									  'label' => false,
									  'div' => false,
								));?>
							</label>
						</div>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Title");?> : <span class="required">*</span></div>
					<?php echo $this->Form->input('title', array('type'=>'text', 'label' => false, 'value'=>$Directorymember['Directorymember']["title"], 'class'=>'formtextbox login-from-box-1', 'div'=>false));?>
					
					<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)') ?>"></span>
					
				
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Banner URL");?> : <span class="required">*</span></div>
					<?php echo $this->Form->input('banner_url', array('type'=>'text', 'value'=>$Directorymember['Directorymember']["banner_url"], 'label' => false,'onblur'=>'document.getElementById("img1").src = this.value;', 'div'=>false, 'class'=>'formtextbox'));?>
					
					<div class="textcenter"><div class="form-col-1 forbanneronly"></div><div class="bannerdisplay"><img src="<?php echo $Directorymember['Directorymember']["banner_url"]; ?>" id="img1"/></div></div>
					
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Destination URL");?> : <span class="required">*</span></div>
					<?php echo $this->Form->input('site_url', array('type'=>'text', 'label' => false, 'div'=>false, 'value'=>$Directorymember['Directorymember']["site_url"], 'class'=>'formtextbox txtframebreaker'));?>
				</div>
				<div class="form-row">
					<div class="form-col-1"><?php echo __("Description");?> : <span class="required">*</span></div>
					<?php echo $this->Form->input('description', array('type'=>'textarea', 'label'=>false, 'div'=>false, 'value'=>stripslashes($Directorymember['Directorymember']["description"]),'title'=>__('Description'), 'class'=>'formtextarea '));?>
				</div>
				<div class="form-row">
					<div class="form-col-1 form-text"><?php echo __("Show URL in Zone");?> : </div>
					<?php if($Directorymember['Directorymember']["show_url"]){$checked="checked";}else{$checked="";}
							echo $this->Form->input('show_url', array('type' => 'checkbox', 'div' => false, 'label' =>'', 'checked'=>$checked));?>
							<span class="helptooltip vtip" title="<?php echo __('Choose whether the site URL will be shown with the Business Directory or not.') ?>"></span>
					
				</div>
			</div>
			<div class="height10"></div>
			<div class="formbutton">
				<?php echo $this->Js->link(__("Back"), array('controller'=>'directory', "action"=>"index"), array(
					'update'=>'#Directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'escape'=>false,
					'class'=>'button',
					'style'=>'float:left'
				));?>
				<input type="button" value="<?php echo __('Update'); ?>" class="button" onclick="return checkframebreaker(0,'<?php echo $SITEURL; ?>','framebreaker');" />
				<?php echo $this->Js->submit(__('Update'), array(
					'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
					'update'=>'#UpdateMessage',
					'class'=>'button framebreaker',
					'div'=>false,
					'controller'=>'directory',
					'style'=>'display:none',
					'action'=>'addaction',
					'url'   => array('controller' => 'directory', 'action' => 'addaction')
				));?>
			</div>
			<div class="clear-both"></div>
			<?php echo $this->Form->end();?>
			<?php // Banner Ad Details edit form ends here ?>
			
		</div>
<?php if(!$ajax){ ?>
</div>
<?php } ?>	
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>