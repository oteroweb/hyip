<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 11-12-2014
  * Last Modified: 11-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Biz Directory</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Biz Directory Plans", array('controller'=>'directory', "action"=>"plan"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Plan Member", array('controller'=>'directory', "action"=>"planmember"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Surf Free Plan", array('controller'=>'directory', "action"=>"surffreeplan"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Surf Free Plan Member", array('controller'=>'directory', "action"=>"surffreeplanmember"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Directory Categories", array('controller'=>'directory', "action"=>"category"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="directorypage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Biz_Directory#Directory_Categories" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<?php echo $this->Form->create('Directorycategory',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'directory','action'=>'categoryaddaction')));?>
	<?php if(isset($directorycategory['Directorycategory']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$directorycategory['Directorycategory']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
	
	<div class="frommain">
		
		<div class="fromnewtext">Name :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('category', array('type'=>'text', 'value'=>$directorycategory['Directorycategory']["category"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'class'=>'btnorange',
			  'div'=>false,
			  'controller'=>'directory',
			  'action'=>'categoryaddaction',
			  'url'   => array('controller' => 'directory', 'action' => 'categoryaddaction')
			));?>
			
			<?php echo $this->Js->link("Back", array('controller'=>'directory', "action"=>"category"), array(
				'update'=>'#directorypage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'div'=>false,
				'class'=>'btngray'
			));?>
		</div>
		
	</div>	
<?php echo $this->Form->end();?>

<?php if(!$ajax){?>
</div><!--#directorypage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>