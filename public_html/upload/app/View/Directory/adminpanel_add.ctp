<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 11-12-2014
  * Last Modified: 11-12-2014
  *********************************************************************/
?>
<?php echo $this->Javascript->link('allpage');?>
<?php if(!$ajax){?>
<div class="whitetitlebox">Advertisement / Biz Directory</div>
<div class="height10"></div>
<div class="tab-blue-box">
	<div id="tab">
	  <ul class="nav nav-tabs" id="myTab">
			<li>
				<?php echo $this->Js->link("Biz Directory Plans", array('controller'=>'directory', "action"=>"plan"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li class="active">
				<?php echo $this->Js->link("Plan Member", array('controller'=>'directory', "action"=>"planmember"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Surf Free Plan", array('controller'=>'directory', "action"=>"surffreeplan"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Surf Free Plan Member", array('controller'=>'directory', "action"=>"surffreeplanmember"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
			<li>
				<?php echo $this->Js->link("Directory Categories", array('controller'=>'directory', "action"=>"category"), array(
					'update'=>'#directorypage',
					'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
					'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false))
				));?>
			</li>
	  </ul>
	</div>
</div>
<div class="tab-content">
<div id="directorypage">
<?php }?>
<div class="helpicon"><a href="https://www.proxscripts.com/docs/Biz_Directory#Plan_Member" target="_blank">Help</a></div>
<div id="UpdateMessage"></div>

<?php echo $this->Form->create('Directorymember',array('type' => 'post','onsubmit' => 'return false;','url'=>array('controller'=>'directory','action'=>'addaction')));?>
	<?php if(isset($directorydata['Directorymember']["id"])){
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$directorydata['Directorymember']["id"], 'label' => false));
		echo $this->Form->input('formaction', array('type'=>'hidden', 'value'=>'edit', 'label' => false));
	}?>
	
	<div class="frommain">
		<div class="fromnewtext">Banner Size : </div>
		<div class="fromborderdropedown">
		  <div class="select-main">
			  <label>
				<?php 
				echo $this->Form->input('banner_size', array(
					'type' => 'select',
					'options' => array('125x125'=>'125X125', '468x60'=>'468X60', '728x90'=>'728X90'),
					'selected' => $directorydata['Directorymember']["banner_size"],
					'class'=>'',
					'label' => false,
					'div' => false,
					'style' => ''
				));
				?>
			  </label>
		  </div>
		</div>
		<span data-original-title="If Set to 'Active', The Plan Will Be Displayed on Member Side When Purchasing Banner Ad Plans." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="fromnewtext">Category : </div>
		<div class="fromborderdropedown">
		  <div class="select-main">
			  <label>
				<?php 
				echo $this->Form->input('category', array(
					'type' => 'select',
					'options' => $category,
					'selected' => $directorydata['Directorymember']["category"],
					'class'=>'',
					'label' => false,
					'div' => false,
					'style' => ''
				));
				?>
			  </label>
		  </div>
		</div>
		<span data-original-title="Category." data-toggle="tooltip" class="tooltipmain glyphicon-question-sign"> </span>
		
		<div class="fromnewtext">Title :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('title', array('type'=>'text', 'value'=>$directorydata['Directorymember']["title"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Banner URL :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('banner_url', array('type'=>'text', 'value'=>$directorydata['Directorymember']["banner_url"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Site URL :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('site_url', array('type'=>'text', 'value'=>$directorydata['Directorymember']["site_url"], 'label' => false, 'div' => false, 'class'=>'fromboxbg'));?>
		</div>
		
		<div class="fromnewtext">Description :<span class="red-color">*</span> </div>
		<div class="fromborderdropedown3">
			<?php echo $this->Form->input('description', array('type'=>'textarea', 'value'=>stripslashes($directorydata['Directorymember']["description"]),'label' => false, 'div' => false, 'class'=>'from-textarea'));?>
		</div>
		
		<div class="fromnewtext">Show URL In Zone : </div>
		<div class="fromborderdropedown3 checkboxlist">
			<?php if($directorydata['Directorymember']["show_url"]){$checked="checked";}else{$checked="";}
			echo $this->Form->input('show_url', array('type' => 'checkbox', 'div'=>true, 'label' =>"", 'checked'=>$checked));?>
		</div>
		
		<div class="formbutton">
			<?php echo $this->Js->submit('Submit', array(
			  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
			  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
			  'update'=>'#UpdateMessage',
			  'class'=>'btnorange',
			  'div'=>false,
			  'controller'=>'directory',
			  'action'=>'addaction',
			  'url'   => array('controller' => 'directory', 'action' => 'addaction')
			));?>
			
			<?php echo $this->Js->link("Back", array('controller'=>'directory', "action"=>"planmember/".$planid), array(
				'update'=>'#directorypage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'div'=>false,
				'class'=>'btngray'
			));?>
		</div>
		
	</div>
<?php echo $this->Form->end();?>

<?php if(!$ajax){?>
</div><!--#directorypage over-->
</div>
<?php }?>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>