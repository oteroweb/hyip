<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 12-12-2014
  * Last Modified: 17-12-2014
  *********************************************************************/
?>
<?php if(!$ajax){ ?>
<div id="Directorypage">
<?php } ?>
<?php echo $this->Javascript->link('allpage');?>
<?php if($Enabledirectory==1) { ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div id="UpdateMessage"></div>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __('View Ads (Surfing)');?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran" style="text-align: center;">
<script type="text/javascript">
function checkdirectory_ad(id)
{
	document.getElementById("directoryid"+id).style.opacity = 0.3;
	totalclickcounter();
	$('#viewbanner_overlay').height(0);
	$('#viewbanner_overlay').width(0);
	$("#directoryid"+id).find("a").attr('href',"javascript:void(0)");
	$("#directoryid"+id).find("a").attr('target',"");
	$("#directoryid"+id).find("img").attr('onclick',"");
}
function totalclickcounter()
{
	$.ajax({
		type: "POST",
		url: "todayclick",
		data: {func:"totalclickcounter"},
		cache: false
		}).done(function(msg) {
			$("#totalclickcounter").html(msg);
		});
}
totalclickcounter();
function viewbanner()
{
	$('#viewbanner_overlay').height($('#viewbanner_ad').height());
	$('#viewbanner_overlay').width($('#viewbanner_ad').width());
}
</script>
<div class="activ-ad-pack">
			<?php echo $this->Js->link(__("Refresh"), array('controller'=>'directory', "action"=>"view"), array(
				'update'=>'#Directorypage',
				'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
				'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				'escape'=>false,
				'class'=>'button'
			));?>
		</div>
	<div align="left"><?php echo __("Today's Total Clicks");?> : <span id="totalclickcounter"><?php echo $membertotalclick ?></span>
	<?php if($memberclicklimit>0){echo " | ".__("Clicks allowed per day").' : '.$memberclicklimit;}?></div>
	<div class="height5"></div>
	<div class="clear-both"></div>
	<div id="viewbanner_ad">
		
		<div id="viewbanner_overlay">&nbsp;</div>
		<div class="">
			<?php $divid=0;	
			foreach ($Directorybanners as $Directorybanner): 
			$divid++; ?>
			<div id="directoryid<?php echo $divid;?>" class="mainptcbox">
				
				<div class="directorymaininner">
					<div class="directoryleft">
						<div class="directoryimages">
						<?php
						if($Directorybanner['Member']['member_photo']==NULL || $Directorybanner['Member']['member_photo']=='')
						{
							echo $this->html->image("member/dummy.jpg", array('alt'=>''));
						}
						else
						{
							echo $this->html->image("member/".$Directorybanner['Member']['member_photo'].'?tk='.date('sHi'), array('class'=>'profilepicture','alt'=>'','id'=>'memberimg'));
						}
						?>
						</div>
						<div class="directorytitle"><?php echo $Directorybanner['Directorymember']['title'];?></div>
					</div>
					<div class="directoryleft">
						<span><?php echo __('Clicks');?> : <?php echo $Directorybanner['Directorymember']['click_counter'];?></span>
						<span><?php echo __('Views'); ?> : <?php echo $Directorybanner['Directorymember']['display_counter'];?></span>
					</div>
				</div>
				
				
				<div class="ptccontent">
					<div class="ptcboxmiddle">
						<?php
						if($Directorybanner['Directorymember']['banner_url']!=''){
							if($Directorybanner['Directorymember']['banner_size']=='125x125')
							{ ?><a href="<?php echo str_replace("https://", "http://", $SITEURL);?>directory/counter/<?php echo $Directorybanner['Directorymember']['id'];?>/<?php echo $divid; ?>" target="_blank"><?php  echo '<div class="onetwofivebox"><img src="'.$Directorybanner['Directorymember']['banner_url'].'" alt="" onclick="viewbanner();" width="" height="" style="vertical-align: middle;padding-right:10px;" /></div>'; ?>
							</a>
							<?php }
							elseif($Directorybanner['Directorymember']['banner_size']=='468x60')
							{
								?><a href="<?php echo str_replace("https://", "http://", $SITEURL);?>directory/counter/<?php echo $Directorybanner['Directorymember']['id'];?>/<?php echo $divid; ?>" target="_blank"><?php
								echo '<img src="'.$Directorybanner['Directorymember']['banner_url'].'" alt="" onclick="viewbanner();" /></a>';
							}
							elseif($Directorybanner['Directorymember']['banner_size']=='728x90')
							{
								?><a href="<?php echo str_replace("https://", "http://", $SITEURL);?>directory/counter/<?php echo $Directorybanner['Directorymember']['id'];?>/<?php echo $divid; ?>" target="_blank" onclick="viewbanner();"><?php
								echo '<img src="'.$Directorybanner['Directorymember']['banner_url'].'" alt="" /></a>';
							}
						}
						?>
						<p <?php if($Directorybanner['Directorymember']['banner_size']=='125x125'){echo 'class="onetwofivetext"';}?>><?php echo nl2br(stripslashes($Directorybanner['Directorymember']['description']));?></p>
						<br>
							<?php if($Directorybanner['Directorymember']['show_url']==1){ echo $Directorybanner['Directorymember']['site_url']."<br/><br/>";} ?>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
			<?php if(count($Directorybanners)==0) echo "<div class='tabal-content-white textcenter'>".__('No records available')."</div>"; ?>
		</div>
	</div>
	<div class="clear-both"></div>
</div>
<?php } else { echo __('This page is disabled by administrator'); } ?>
<?php if(!$ajax){ ?>
</div>
<?php } ?>	
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>