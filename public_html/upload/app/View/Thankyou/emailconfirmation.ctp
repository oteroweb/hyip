<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php // This file is used to show note after member has changed his email/processor email from profile page ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Note");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<div class="divtable">
		<div class="divtbody">
			<div class="divtr">
				<div class="divtd"><?php echo __("You have made changes in your Email Address/Processor Id. So, you need to verify the change from your email address again. Please verify the change using the link sent to your email.");?></div>
			</div>
		</div>
	</div>
</div>
<div class="height10"></div>