<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php // This file is used to show note after member has added funds ?>
<?php if(trim($web_page_content)!=''){?><div class="main-box-eran"><?php echo stripslashes($web_page_content);?></div><?php }?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo ($MessageType=='Done') ? __("Thank You") : __("Cancelled");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<div class="divtable">
		<div class="divtbody">
			<div class="divtr">
				<div class="divtd">
					<?php if($MessageType=='Done'){?>
						<?php echo __("Payment successful. Thank you for your payment.");?>
					<?php }elseif($MessageType=='Cancel'){?>
						<?php echo __("Payment process has been cancelled by you.").' '.__("Please click on add funds link again to fund your account.");?>
					<?php }?>
				</div>
			</div>
		</div>
	</div>	
</div>
<div class="height10"></div>