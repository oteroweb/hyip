<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Thank You");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<div class="divtable">
		<div class="divtbody">
			<div class="divtr">
				<div class="divtd">
					<?php echo __('Thank you! Your account is successfully created.'); ?>
				</div>
			</div>
			<?php
			$startupbonus=explode(',',$SITECONFIG["startupbonus"]);
			foreach($startupbonus as $startupbonusvalue)
			{
				if(strpos($startupbonusvalue,'StartUpBonus|') !== false)
				{
					$startposition=strpos($startupbonusvalue,'|')+1;
					$startupbonusamount=substr($startupbonusvalue,$startposition);
				}
				elseif(strpos($startupbonusvalue,'ReferralsType|') !== false)
				{
					$startposition=strpos($startupbonusvalue,'|')+1;	
					$ReferralsType=substr($startupbonusvalue,$startposition);
				}
				elseif(strpos($startupbonusvalue,'Referrals|') !== false)
				{
					$startposition=strpos($startupbonusvalue,'|')+1;	
					$Referrals=substr($startupbonusvalue,$startposition);
				}
				elseif(strpos($startupbonusvalue,'PositionsType|') !== false)
				{
					$startposition=strpos($startupbonusvalue,'|')+1;	
					$PositionsType=substr($startupbonusvalue,$startposition);
				}
				elseif(strpos($startupbonusvalue,'Positions|') !== false)
				{
					$startposition=strpos($startupbonusvalue,'|')+1;	
					$Positions=substr($startupbonusvalue,$startposition);
				}
				
			}
			if($startupbonusamount>0) // If admin is providing signup bonus to new members
			{
				echo '<div class="divtr"><div class="divtd"><div class="note-box">';
				if($Referrals==0 && $ReferralsType==0 && $Positions==0 && $PositionsType==0)
				{
					echo __('You would receive the start up bonus in your account soon').' : '.$Currency['prefix'].round($startupbonusamount*$Currency['rate'],4)." ".$Currency['suffix'];
				}
				else // Member can receive signup bonus only after meeting following criterias
				{
					echo __('You would receive the start up bonus in your account after fulfilling below requirements').' : '.$Currency['prefix'].round($startupbonusamount*$Currency['rate'],4)." ".$Currency['suffix'];
					echo '<br />';
					if($Referrals>0 && $ReferralsType>0)
					{
						$startupbonusreferraltype=($ReferralsType==1)?__('Paid'):'';
						echo $Referrals.' '.$startupbonusreferraltype.' '.__('Referral(s)');
						echo '<br />';
					}
					if($Positions>0 && $PositionsType>0)
					{
						$startupbonuspositiontype=($PositionsType==1)?__('Completed'):'';
						echo $Positions.' '.$startupbonuspositiontype.' '.__('Position(s)');
						echo '<br />';
					}
				}
				if($SITECONFIG['balance_type']==2)
					echo '<br />'.__('Start up bonus gets credited in your account only after you select your preferred payment processor.');
				echo '</div></div></div>';
			}
			?>
		</div>
	</div>
</div>
<div class="height10"></div>