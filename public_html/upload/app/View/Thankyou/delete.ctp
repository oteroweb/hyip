<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Thank You");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<div class="divtable">
		<div class="divtbody">
			<div class="divtr">
				<div class="divtd">
					<?php echo __('Thank you! Your account is successfully Deleted.'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="height10"></div>