<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 26-09-2014
  *********************************************************************/
?>
<?php // This file is used to show note if any error occurs when member tries to signup using facebook or google ?>
<div class="comisson-bg">
	<div class="text-ads-title-text"><?php echo __("Note");?></div>
	<div class="clear-both"></div>
</div>
<div class="main-box-eran">
	<div class="divtable">
		<div class="divtbody">
			<div class="divtr">
				<div class="divtd">
					<?php if($MessageType=='Notallow'){?>
						<?php echo __("Admin has stopped new registration currently. Please try later.");?>
					<?php }elseif($MessageType=='Error'){?>
						<?php echo __("Error occurred. Please try again").'.';?>
					<?php }elseif($MessageType=='Email'){?>
						<?php echo __("Email is already used").'. '.__('Please try with some other email address.');?>
					<?php }elseif($MessageType=='Block'){?>
						<?php echo __("Your Username, IP Address, Domain Name, Email or Country is blocked by admin.");?>
					<?php }elseif($MessageType=='Nosponsor'){?>
						<?php echo __("Sponsor is required to sign up. Please specify a sponsor first.");?>
					<?php }else{?>
						<?php echo __("Error occurred. Please try again").'.';?>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="height10"></div>