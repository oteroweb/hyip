<?php /*****************************************************************
  * ProXCore Script
  * Copyright &copy; 2014 ProXScripts.com All Rights Reserved.
  **********************************************************************
  * IMPORTANT: The copyright notice in the footer must not be removed
  * and is a legal includement for operating this software.
  * Redistribution of this code in any form IS NOT ALLOWED.  You are
  * given permission to run it on a single site only.  Any infringement
  * will result in prosecution.  Full details of the license granted
  * can be found on www.proxcore.com.
  * Created Date: 24-09-2014
  * Last Modified: 19-11-2014
  *********************************************************************/
?>
<?php // Content is fetched from Website Pages ?>
<?php echo $this->Javascript->link('allpage');?>
<div id="UpdateMessage"></div>
<div class="main-box-eran">	
	<?php if($SITECONFIG["allow_member_registration"]==1){ // If admin has allowed new registrations ?>
		<div class="form-box">
		
		<?php // Code to allow user to change sponsor starts here ?>
		<?php echo $this->Form->create('Member',array('type' => 'post', 'onsubmit' => 'return false;')); ?>
			<?php if($SITECONFIG["chkfb"]==1 || $SITECONFIG["chkgoogle"]==1){ ?>
			  <div class="form-row">
				<div class="form-col-1"><?php echo __("Sign up with");?> : </div>
				<div class="form-col-2 imagetag">
					<?php if($SITECONFIG["chkfb"]==1){ // If admin has enabled facebook login ?>
						<?php echo $this->Js->link($this->html->image('facebook.jpg', array('alt'=>'', 'align' =>'absmiddle')), array('controller'=>'login', "action"=>"with/facebook"), array(
							'update'=>'#UpdateMessage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					<?php }?>
					<?php if($SITECONFIG["chkgoogle"]==1){ // If admin has enabled google login ?>
						<?php echo $this->Js->link($this->html->image('google.jpg', array('alt'=>'', 'align' =>'absmiddle')), array('controller'=>'login', "action"=>"with/google"), array(
							'update'=>'#UpdateMessage',
							'before'=>$this->Js->get('#pleasewait')->effect('fadeIn', array('buffer' => false)),
							'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
							'escape'=>false
						));?>
					<?php }?>
				</div>
			  </div>
			<?php }?>
			
			<div class="form-row">
				<div class="form-col-1"><?php echo __("Sponsor");?> : </div>
					<div class="form-text inlineblock">
						<span id="sponsor">
						<?php if($REFFERAL){ ?>
							<?php echo $REFFERAL." - ".$REFFERALNAME;?>
						<?php }else{ ?>
							<?php echo __("N/A");?>
						<?php } ?>
						</span>
						<?php if($SITECONFIG["issponsorchange"]==1){ // If admin has enabled sponsor change ?>
						<a onclick="$('.changesponsor').fadeIn(1000).css('display','inline-block');"><?php echo __('Change'); ?></a>
						<?php } // If admin has enabled sponsor change ?>
					</div>
					<?php if($SITECONFIG["issponsorchange"]==1){ // If admin has enabled sponsor change ?>
					<div class="changesponsor">
						<?php echo $this->Form->input('newreferrer' ,array('type'=>'text',"class"=>"formtextbox",'label'=>false,'div'=>false,'style'=>'width:50px;margin-bottom:9px;;'));?>
						
						&nbsp;<?php echo $this->Js->submit(__('Change'), array(
						  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
						  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
						  'update'=>'#UpdateMessage',
						  'div'=>false,
						  'class'=>'button floatnone',
						  'controller'=>'Member',
						  'action'=>'index',
						  'url'   => array('controller' => 'register', 'action' => 'changereferral')
						));?>
					</div>
					<?php } // If admin has enabled sponsor change ?>
			</div>
			
		<?php echo $this->Form->end();?>
		<?php // Code to allow user to change sponsor ends here ?>
		
		<?php // Registration form starts here ?>
		<?php echo $this->Form->create('Member',array('type' => 'post', 'id' => 'RegistrationForm', 'onsubmit' => 'return false;', 'autocomplete'=>'off')); ?>
			
			<?php // Code for dynamic fields starts here. Admin can decide which options should be shown on the signup form. ?>
			<?php $class='form-row'; ?>
			<?php $i=0;foreach($customfields as $fieldarray){
				if($i%2==0){$class='form-row';}else{$class='form-row';}
			?>
			<div class="<?php echo $class;?>">
				<div class="form-col-1"><?php echo __($customfields[$i]["display"]);?> : <?php if($customfields[$i]["required"]){echo '<span class="required">*</span>';}?></div>
					<?php if($customfields[$i]["required"]==1){$customrequired='customrequired';}else{$customrequired='';}?>
					<?php if($customfields[$i]["display"]=='First Name'){?>
						<?php echo $this->Form->input('f_name' ,array('type'=>'text', 'label'=>false, 'div'=>false, 'title'=>__('First Name'), 'class'=>'formtextbox '.$customrequired));?>
					<?php }elseif($customfields[$i]["display"]=='Last Name'){?>
						<?php echo $this->Form->input('l_name' ,array('type'=>'text', 'label'=>false, 'div'=>false, 'title'=>__('Last Name'), 'class'=>'formtextbox '.$customrequired));?>
					<?php }elseif($customfields[$i]["display"]=='Email'){?>
						<?php echo $this->Form->input('email' ,array('type'=>'text','div'=>false, "class"=>"formtextbox",'label'=>false));?>
					<?php }elseif($customfields[$i]["display"]=='Username'){?>
						<?php echo $this->Form->input('user_name' ,array('type'=>'text', "class"=>"login-from-box-1", 'label'=>false, 'div' => false));?>
						
						<span class="helptooltip vtip" title="<?php echo __('Allowed Characters - Alphanumeric, Dash(-), Underscore(_), Question Mark(?) and Dot(.)').'. '.__('Maximum characters allowed').' : '.$SITECONFIG["usernamelimit"]; ?>"></span>
						
					<?php }elseif($customfields[$i]["display"]=='Password'){?>
						<?php echo $this->Form->input('new_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 confirmpassword keyboardInput",'div'=>false, 'label'=>false, 'onblur'=>'passwordStrength(this.value,".passwordStrength")', 'onkeyup'=>'passwordStrength(this.value,".passwordStrength")'));?>
						</div>
						<div class="<?php echo $class;?> passstrengthrow">
							<div class="form-col-1"></div>
							<div class="floatleft"><div style="height:2px;"></div><div class="passwordStrength"></div></div>
							<div class="clearboth"></div>
					<?php }elseif($customfields[$i]["display"]=='Re-enter Password'){?>
						<?php echo $this->Form->input('confirm_pwd' ,array('type'=>'password', "class"=>"login-from-box-1 keyboardInput", 'div'=>false, 'label'=>false, 'onblur'=>'if(this.value!=$(".confirmpassword").val()){$(this).css({"border-color":"red"});}else{$(this).css({"border-color":"#dddddd"});}', 'onkeyup'=>'if(this.value!=$(".confirmpassword").val()){$(this).css({"border-color":"red"});}else{$(this).css({"border-color":"#dddddd"});}'));?>
					<?php }elseif($customfields[$i]["display"]=='Payment Processor'){?>
					
					<div class="select-dropdown">
						<label>
							<?php echo $this->Form->input('payment_processor', array(
								  'type' => 'select',
								  'options' => $paymentprocessors,
								  'selected' => '',
								  'class'=>'searchcomboboxwidth '.$customrequired,
								  'label' => false,
								  'title'=>__('Payment Processor'),
								  'div' => false
							));?>
						</label>
					</div>
						
					<?php }elseif($customfields[$i]["display"]=='Payment Processor Id'){?>
						<?php echo $this->Form->input('pro_acc_id' ,array('type'=>'text', 'label'=>false, 'div'=>false, 'title'=>__('Payment Processor Id'), 'class'=>'formtextbox '.$customrequired));?>
					<?php }elseif($customfields[$i]["display"]=='Address'){?>
						<?php echo $this->Form->input('address', array('type'=>'textarea', 'label'=>false, 'div'=>false, 'title'=>__('Address'), 'class'=>'formtextarea '.$customrequired));?>
					<?php }elseif($customfields[$i]["display"]=='Contact No'){?>
						<?php echo $this->Form->input('contact_no' ,array('type'=>'text',  'label'=>false, 'div'=>false, 'title'=>__('Contact No'), "class"=>"formtextbox ".$customrequired));?>
					<?php }elseif($customfields[$i]["display"]=='Country'){?>
					
					<div class="select-dropdown">
						<label>
							<?php
								$countries[0]="Select country";
								ksort($countries);
								echo $this->Form->input('country', array(
								  'type' => 'select',
								  'options' => $countries,
								  'selected' => '',
								  'class'=>'searchcomboboxwidth '.$customrequired,
								  'label' => false,
								  'div'=>false,
								  'title'=>__('Country')
							));?>
						</label>
					</div>
						
					<?php }else{?>
						<?php if($customfields[$i]["type"]=='textarea'){?>
							<textarea name="<?php echo "custom[".$customfields[$i]["field_id"]."]"; ?>" title="<?php echo __($customfields[$i]["display"]);?>"  class="formtextarea <?php echo $customrequired;?>"></textarea>
						<?php }elseif($customfields[$i]["type"]=='text'){?>
							<input name="<?php echo "custom[".$customfields[$i]["field_id"]."]"; ?>" title="<?php echo __($customfields[$i]["display"]);?>" class="formtextbox  <?php echo $customrequired;?>" type="<?php echo $customfields[$i]["type"]; ?>" />
						<?php }?>
					<?php }?>
			</div>
			<?php $i++; }?>
			<?php // Code for dynamic fields ends here. ?>
			
			<?php if($MemberSignupCaptcha){ // If captcha is enabled by admin ?>
			  <div class="captchrow <?php if($class=='form-row'){ echo 'form-row';}else{echo 'form-row';} ?>">
				<div class="form-col-1"><?php echo __('Enter Captcha')?> : <span class="required">*</span></div>
					<?php echo $this->Form->input('captchacode' ,array('id'=>'captchacode', "class"=>"formcapthcatextbox", 'label'=>false,'div'=>false));?>
					<span><?php echo __('Code')?> :</span>
					<?php echo $this->html->image($this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true),array('id'=>'MemberSignupCaptcha','vspace'=>2, 'align'=>'absmiddle', 'width'=>'118', 'height'=>'44')); ?> 
					<a href="javascript:void(0);" onclick="javascript:document.images.MemberSignupCaptcha.src='<?php echo $this->html->url(array('controller'=>'login', 'action'=>'captcha_image'), true);?>?' + Math.round(Math.random(0)*1000)+1 + ''"><?php echo $this->html->image("refresh.png", array("alt"=>"", "width"=>"", "height"=>"", 'align'=>'absmiddle'));?></a>
			  </div>
			<?php }?>
			<?php if($SITECONFIG["enableagree"]==1){?>
			<div class="profile-bot-left">
				<?php echo $this->Form->input('agree', array('type' => 'checkbox', 'div'=>false, 'hiddenField'=>false, 'label' =>''));?>&nbsp;&nbsp;
				<?php echo ' <a href="'.$SITEURL.'public/terms" target="_blank">'.__('I agree with the terms and conditions').'</a> '.__('of').' '.$SITECONFIG["sitetitle"]; ?>
			</div>
			<?php } ?>
			<div class="formbutton">
				<?php echo $this->Js->submit(__('Register'), array(
				  'before'=>$this->Js->get('#pleasewait, #UpdateMessage')->effect('fadeIn', array('buffer' => false)),
				  'success'=>$this->Js->get('#pleasewait')->effect('fadeOut', array('buffer' => false)),
				  'update'=>'#UpdateMessage',
				  'div'=>false,
				  'class'=>'button',
				  'controller'=>'Member',
				  'action'=>'index',
				  'onfocus'=>'return ValidateRegisterForm();'
				));?>
			</div>
			<div class="clear-both"></div>
			<?php echo $this->Form->input('ErrorMessage', array('type'=>'hidden', 'id'=>'ErrorMessage', 'label' => false));?>
			<?php echo $this->Form->input('userlimit', array('type'=>'hidden', 'value'=>$SITECONFIG["usernamelimit"], 'label' => false));?>
		<?php echo $this->Form->end();?>
		<script>VKI_attach(document.getElementById('MemberNewPwd'));VKI_attach(document.getElementById('MemberConfirmPwd'));</script>
		<?php // Registration form ends here ?>
		
	</div>
	<?php }else{ // If admin has not allowed new registrations ?>
		<?php echo __("Admin has stopped new registration currently. Please try later.");?>
	<?php } ?>
			
<div class="clear-both"></div>
</div>
<script>$("form input").bind("keypress", function (e){ if (e.keyCode == 13) {return false;} });</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true'));?>