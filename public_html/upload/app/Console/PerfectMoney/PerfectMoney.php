<?php
class PerfectMoney
{
	function getFormAddProcesser($processordata,$SITEURL='')
	{
		$processorreturn=array();
		$processorreturn[0] ='<div class="fromnewtext">Processor Name : Perfect Money</div>
				<div class="fromnewtext">Routine URL :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_url]" id="SitesettingProcUrl" class="fromboxbg" value="'.$processordata["Processor"]["proc_url"].'" />
				</div>
				<div class="fromnewtext">Payee Account :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_accid]" id="SitesettingProcAccid" class="fromboxbg" value="'.$processordata["Processor"]["proc_accid"].'" />
				</div>
				<div class="fromnewtext">Alternate Passphrase :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_password\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_password]" style="display:none;" id="api_password" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Maximum Request Amount Per Day ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][max_req_amt]" id="SitesettingMaxReqAmt" class="fromboxbg" value="'.$processordata["Processor"]["max_req_amt"].'" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Add Fund Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee_amount"].'" name="data[Sitesetting][proc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Add Fund Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee"].'" name="data[Sitesetting][proc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[1]='<div class="fromnewtext">Perfect Money Member Id :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'lr_api\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][lr_api]" style="display:none;" id="lr_api" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Account Password :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'lr_security\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][lr_security]" style="display:none;" id="lr_security" class="fromboxbg" value="" />
				</div>
				<div class="formborderfinance"><div class="fromnewtext">Fixed Withdrawal Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee_amount"].'" name="data[Sitesetting][mproc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Withdrawal Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee"].'" name="data[Sitesetting][mproc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[2]='';
		return $processorreturn;
	}
	function getFormAddfund($processordata,$data)
	{
		$processorreturn=array();
		$processorreturn['message']='';
		
		echo '<form method="post" action="'.$processordata['Processor']['proc_url'].'" id="service">';
		echo '<input type="hidden" name="PAYEE_ACCOUNT" value="'.$processordata['Processor']['proc_accid'].'">';
		echo '<input type="hidden" name="PAYEE_NAME" value="'.$data['sitetitle'].'">';
		echo '<input type="hidden" name="PAYMENT_ID" value="NULL">';	
		echo '<input type="hidden" name="PAYMENT_AMOUNT" value="'.round($data['total'],2).'">';
		echo '<input type="hidden" name="PAYMENT_UNITS" value="USD">';
		echo '<input type="hidden" name="STATUS_URL" value="'.$data['siteurl'].'perfectmoneynotify/index'.'">';
		echo '<input type="hidden" name="PAYMENT_URL" value="'.$data['siteurl'].'thankyou/addfund/done'.'">';
		echo '<input type="hidden" name="PAYMENT_URL_METHOD" value="POST">';
		echo '<input type="hidden" name="NOPAYMENT_URL" value="'.$data['siteurl'].'thankyou/addfund/cancel'.'">';
		echo '<input type="hidden" name="NOPAYMENT_URL_METHOD" value="POST">';
		echo '<input type="hidden" name="SUGGESTED_MEMO" value="Member Id - '.$data['userid'].'">';
		echo '<input type="hidden" name="MEM_ID" value="'.$data['userid'].'">';
		echo '<input type="hidden" name="PM_PRODUCT_NAME" value="'.$data['type'].'">';
		echo '<input type="hidden" name="IP_ADDRESS" value="'.$_SERVER['REMOTE_ADDR'].'">';
		echo '<input type="hidden" name="CONFIRM_ID" value="'.$data['lastid'].'">';
		echo '<input type="hidden" name="CONTROLLERTYPE" value="'.$data['callmethod'].'">';
		echo '<input type="hidden" name="BAGGAGE_FIELDS" value="MEM_ID PM_PRODUCT_NAME IP_ADDRESS CONFIRM_ID CONTROLLERTYPE">';
		echo '<input type="hidden" name="PAYMENT_METHOD" value="Pay Now!">';
		echo '</form>';

		echo '<script language="javascript">document.getElementById("service").submit();</script>';
		
		return $processorreturn;
	}
	
	function getdatawithdraw($processordata='',$withdrawdata='')
	{
		$processorreturn=array();
		
		//$totamt=$totamt+$datam['cash_balance'];
		$account_id =stripslashes($processordata['Processor']['lr_api']);
		$account_pwd = stripslashes($processordata['Processor']['lr_security']);
		$Payer_Account = stripslashes($processordata['Processor']['proc_accid']);
		$totwith=0;
		$totselamt=0;
		if(count($withdrawdata)>0)
		{
			$processorreturn[1]='<div class="Masspay-Message-Title">Perfect Money MassPay Message</div><div class="Masspay-Message-Description">Description : </div><div class="Masspay-Message-Error">';
		}
		foreach($withdrawdata as $withdrow)
		{
			$payee_account=$withdrow['Withdraw']['pro_acc_id'];//send to
			$testmode=0;//set to 1 for enable and 0 for disable
			$amount=$withdrow['Withdraw']['amount'];
			
			// trying to open URL to process PerfectMoney Spend request
			
			$f=fopen('https://perfectmoney.is/acct/confirm.asp?AccountID='.$account_id.'&PassPhrase='.$account_pwd.'&Payer_Account='.$Payer_Account.'&Payee_Account='.$payee_account.'&Amount='.$amount.'&PAY_IN=1&PAYMENT_ID='.$withdrow['Withdraw']['with_id'].'','rb');
			
			if($f===false){
			   echo 'Error in Opening URL for payment.';
			}
			
			// getting data
			$out=array(); $out="";
			while(!feof($f)) $out.=fgets($f);
			
			fclose($f);
			
			// searching for hidden fields
			if(!preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/", $out, $result, PREG_SET_ORDER)){
			   echo 'Invalid output';
			   //exit;
			}
			
			$ar="";$description="";
			foreach($result as $item)
			{
			   $key=$item[1];
			   $ar[$key]=$item[2];
			   $description.=$key."=>".$item[2].",\n";
			}
			
			$processorreturn[3][] = array('id'=>NULL,'memberid'=>$withdrow['Withdraw']['member_id'],'status' => 'Done', 'processorid' => $withdrow['Withdraw']['processorid'], 'transaction_no' => @$ar['PAYMENT_BATCH_NUM'], 'processor' => 'Perfect Money','type'=>'masspay','description' => trim($description), 'ipaddress' => '-', 'pay_dt' => date('Y-m-d H:i:s'));
					
			$status = "FAIL";
			if(!isset($ar['ERROR']) && @$ar['PAYMENT_BATCH_NUM']!="")
			{
				$status = "Done";
				$processorreturn[0][]='Done';
				
				$processorreturn[2][] = array('id' => NULL, 'batch_number' => $ar['PAYMENT_BATCH_NUM'], 'return_code' => 0, 'return_desc' => $status, 'refrence_no' => $ar['PAYMENT_BATCH_NUM'], 'dt' => date('Y-m-d H:i:s'), 'amount' => $withdrow['Withdraw']['amount'], 'sender_email' => 'Admin', 'receiver_email' => $ar['Payee_Account'], 'receiver_id' => $withdrow['Withdraw']['member_id'], 'with_id' => $withdrow['Withdraw']['with_id'], 'processor' => 'Perfect Money', 'processor_id' => $withdrow['Withdraw']['processorid']);
				$totwith++;
				$totselamt=$totselamt+$withdrow['Withdraw']['amount'];
			}
			else
			{
				$processorreturn[0][]='Fail';
				$processorreturn[2][] = array('id' => NULL, 'batch_number' => '-', 'return_code' => 0, 'return_desc' => $ar['ERROR'], 'refrence_no' => '-', 'dt' => date('Y-m-d H:i:s'), 'amount' => $withdrow['Withdraw']['amount'], 'sender_email' => 'Admin', 'receiver_email' => '-', 'receiver_id' => $withdrow['Withdraw']['member_id'], 'with_id' => $withdrow['Withdraw']['with_id'], 'processor' => 'Perfect Money', 'processor_id' => $withdrow['Withdraw']['processorid']);
			}
			
			$processorreturn[1].="<div><span>Payment Status : </span><span>".$status."</span></div>
			<div><span>Account : </span><span>".$ar['Payee_Account']."</span></div>
			<div><span>Amount : </span><span>".$withdrow['Withdraw']['amount']."</span></div>";
		
		}
		if(count($withdrawdata)>0)
		{
			$processorreturn[1].='</div><div class="Masspay-Message-Total">Total Members : '.$totwith.'</div>
			<div class="Masspay-Message-Total">Total Amount : $'.$totselamt.'</div>';
		}
		return $processorreturn;
	}
	function GetBalance($processordata='')
	{
		//$f=fopen('https://perfectmoney.is/acct/balance.asp?AccountID='.$processordata['Processor']['lr_api'].'&PassPhrase='.$processordata['Processor']['lr_security'], 'rb');
		$url='https://perfectmoney.is/acct/balance.asp?AccountID='.$processordata['Processor']['lr_api'].'&PassPhrase='.$processordata['Processor']['lr_security'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, false);
		//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_REFERER, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		$out = curl_exec($ch);
		curl_close($ch);
		//echo $f;exit;
		if($out==""){
		   return 'error openning url';
		}
		
		// getting data
		//$out=array(); $out="";
		//while(!feof($f)) $out.=fgets($f);
		//fclose($f);
		
		// searching for hidden fields
		if(!preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/", $out, $result, PREG_SET_ORDER)){
		   return 'Ivalid output';
		   exit;
		}
		
		// putting data to array
		$ar="";
		foreach($result as $item){
		   $key=$item[1];
		   $ar[$key]=$item[2];
		}
		
		return $ar[$processordata['Processor']['proc_accid']];
		
	}
}
?>