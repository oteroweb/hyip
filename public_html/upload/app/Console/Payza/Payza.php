<?php
include("MassPayClient.php");
include("GetBalanceAPIClient.php");
class Payza
{
	function getFormAddProcesser($processordata,$SITEURL='')
	{
		$processorreturn=array();
		$processorreturn[0] ='<div class="fromnewtext">Processor Name : Payza</div>
				<div class="fromnewtext">Routine URL :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_url]" id="SitesettingProcUrl" class="fromboxbg" value="'.$processordata["Processor"]["proc_url"].'" />
				</div>
				<div class="fromnewtext">Your Payza Email :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_accid]" id="SitesettingProcAccid" class="fromboxbg" value="'.$processordata["Processor"]["proc_accid"].'" />
				</div>
				<div class="fromnewtext">IPN Security Code :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_signature\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_signature]" style="display:none;" id="api_signature" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Maximum Request Amount Per Day ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][max_req_amt]" id="SitesettingMaxReqAmt" class="fromboxbg" value="'.$processordata["Processor"]["max_req_amt"].'" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Add Fund Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee_amount"].'" name="data[Sitesetting][proc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Add Fund Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee"].'" name="data[Sitesetting][proc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[1]='<div class="fromnewtext">Payza Api Password :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_password\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_password]" style="display:none;" id="api_password" class="fromboxbg" value="" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Withdrawal Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee_amount"].'" name="data[Sitesetting][mproc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Withdrawal Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee"].'" name="data[Sitesetting][mproc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[2]='';
		return $processorreturn;
	}
	
	function getFormAddfund($processordata,$data)
	{
		$processorreturn=array();
		$processorreturn['message']='';
		echo '<form method="post" action="'.$processordata['Processor']['proc_url'].'" id="service">'; 
		echo	 '<input type="hidden" value="'.$processordata['Processor']['proc_accid'].'" name="ap_merchant">';
		echo     '<input type="hidden" value="service" name="ap_purchasetype">';
		echo     '<input type="hidden" value="'.$data['type'].'" name="ap_itemname">';
		echo     '<input type="hidden" value="'.round($data['total'],2).'" name="ap_amount">';
		echo     '<input type="hidden" value="USD" name="ap_currency">';
		echo     '<input type="hidden" value="'.$data['description'].'" name="ap_description">';
		echo     '<input type="hidden" value="'.$data['userid'].'" name="ap_itemcode">';
		echo     '<input type="hidden" value="1" name="ap_quantity">';
		echo     '<input type="hidden" value="'.$_SERVER['REMOTE_ADDR'].'" name="apc_1">';
		echo 	 '<input type="hidden" value="'.$data['lastid'].'" name="apc_2">';
		echo 	 '<input type="hidden" value="'.$data['callmethod'].'" name="apc_3">';
		//echo     '<input type="hidden" value="1" name="TESTMODE">';
		echo     '<input type="hidden" value="'.$data['siteurl'].'thankyou/addfund/done'.'" name="ap_returnurl">';
		echo     '<input type="hidden" value="'.$data['siteurl'].'thankyou/addfund/cancel'.'" name="ap_cancelurl">';       
		echo '</form>';

		echo '<script language="javascript">document.getElementById("service").submit();</script>';
		return $processorreturn;
	}
	
	function getdatawithdraw($processordata='',$withdrawdata='')
	{
		$processorreturn=array();
		$processorreturn[0]='';
		$payments=array();
		$totwith=0;
		$totselamt=0;
		foreach($withdrawdata as $withdrow)
		{
			$uniqueID = $withdrow['Withdraw']['member_id']."|".$withdrow['Withdraw']['with_id']."|".$withdrow['Withdraw']['fee'];
			$payments[]=array("receiver"=>$withdrow['Withdraw']['pro_acc_id'],"amount"=>$withdrow['Withdraw']['amount'],"custome"=>$uniqueID);
			$totselamt=$totselamt+$withdrow['Withdraw']['amount'];
			$totwith++;
			
			$processorreturn[0][]='Fail';
			$processorreturn[4][0][] = array("Withdraw.status" => "'Pendingipn'");
			$processorreturn[4][1][] = $withdrow['Withdraw']['with_id'];
		}
		
		if(count($payments)>0)
		{
			//$massIt=new MassPayClient('seller_1_observe_u_11@yahoo.com','05BBEsu5yc5v5C4L');
			$massIt=new MassPayClient(stripslashes($processordata['Processor']['proc_accid']),stripslashes($processordata['Processor']['api_password']));
			//$builtData=$massIt->buildPostVariables($payments,'USD','seller_1_observe_u_11@yahoo.com','1');
			$builtData=$massIt->buildPostVariables($payments,'USD',stripslashes($processordata['Processor']['proc_accid']),'0');
			$response = $massIt->send();//parse the response
			$massIt->parseResponse($response);//get the response into an array
			$resArray =  $massIt->getResponse();//echo '<pre>';print_r($resArray);echo '</pre>';
			
			$processorreturn[1]='<div class="Masspay-Message-Title">Payza MassPay Message</div>
			<div class="Masspay-Message-Description">Description : </div>
			<div class="Masspay-Message-Error">
				<div><span>Description : </span><span>'.$resArray['DESCRIPTION'].'</span></div>
				<div><span>Reference Number : </span><span>'.$resArray['REFERENCENUMBER'].'</span></div>
			</div>
			<div class="Masspay-Message-Total">Total Members : '.$totwith.'</div>
			<div class="Masspay-Message-Total">Total Amount : $'.$totselamt.'</div>';
		}
		return $processorreturn;
	}
	function GetBalance($processordata='')
	{
		//echo $processordata['Processor']['proc_accid'];exit;
		$getBla=new GetBalanceAPIClient(stripslashes($processordata['Processor']['proc_accid']),stripslashes($processordata['Processor']['api_password']));
		$getBla->buildPostVariables('USD');
		$response = $getBla->send();//parse the response
		$getBla->parseResponse($response);//get the response into an array
		$resArray =  $getBla->getResponse();//echo '<pre>';print_r($resArray);echo '</pre>';
		
		return $resArray['AVAILABLEBALANCE_1']." ".$resArray['CURRENCY_1'];
	}

}
?>