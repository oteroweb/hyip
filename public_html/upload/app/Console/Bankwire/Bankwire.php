<?php
class Bankwire
{
	function getFormAddProcesser($processordata,$SITEURL='')
	{
		$processorreturn=array();
		$processorreturn[0] ='<div class="fromnewtext">Processor Name : Bank Wire</div>
				<div class="fromnewtext">Your Bank Account Details :<span class="red-color">*</span></div>
				<div class="frombordermain">
					<textarea name="data[Sitesetting][comment]" id="SitesettingComment" style="" class="from-textarea">'.$processordata["Processor"]["comment"].'</textarea>
				</div>
				<div class="fromnewtext">Maximum Request Amount Per Day ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][max_req_amt]" id="SitesettingMaxReqAmt" class="fromboxbg" value="'.$processordata["Processor"]["max_req_amt"].'" />
				</div>
				
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Add Fund Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee_amount"].'" name="data[Sitesetting][proc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Add Fund Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee"].'" name="data[Sitesetting][proc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[1]='<div class="formborderfinance"><div class="fromnewtext">Fixed Withdrawal Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee_amount"].'" name="data[Sitesetting][mproc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Withdrawal Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee"].'" name="data[Sitesetting][mproc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[2]='';
			return $processorreturn;
	}
	function getdatawithdraw($processordata='',$withdrawdata='')
	{
		$processorreturn=array();
		$totwith=0;
		$totselamt=0;
		//Pass [0]=0 then not Update Withdraw wait for processor responce and [0]=1 then Update Withdraw
		if(count($withdrawdata)>0)
		{
			$processorreturn[1].='<div class="Masspay-Message-Title">Bank Wire Message</div><div class="Masspay-Message-Description">Description : </div><div class="Masspay-Message-Error">';
		}
		foreach($withdrawdata as $withdrow)
		{
			$processorreturn[0][]='Paid Manually';
			$processorreturn[2][]=array('with_id' => $withdrow['Withdraw']['with_id'] , 'batch_number' => '0', 'return_code' => 0, 'return_desc' => 'Manually Paid By Admin', 'refrence_no' => 'N/A', 'dt' => date('Y-m-d H:i:s'), 'amount' => $withdrow['Withdraw']['amount'], 'sender_email' => 'Admin', 'receiver_email' => 'Manually Paid By Admin', 'receiver_id' => $withdrow['Withdraw']['member_id'], 'processor' => 'Bank Wire', 'processor_id' => $withdrow['Withdraw']['processorid']);
			$totwith++;
			$totselamt=$totselamt+$withdrow['Withdraw']['amount'];
			$processorreturn[1].='<div><span>Payment Status : </span><span>Paid Manually</span></div>
			<div><span>Amount : </span><span>'.$withdrow['Withdraw']['amount'].'</span></div>';
		}
		if(count($withdrawdata)>0)
		{
			$processorreturn[1].='</div><div class="Masspay-Message-Total">Total Members : '.$totwith.'</div>
			<div class="Masspay-Message-Total">Total Amount : $'.$totselamt.'</div>';
		}
		return $processorreturn;
	}
	function getFormAddfund($processordata,$data)
	{
		$processorreturn=array();
		$processorreturn['message']='';
		
		if(trim($_POST['data'][$data["controller"]]['comment'])!='')
		{
			$processorreturn['data']= array("comment" => '"'.strip_tags(trim($_POST['data'][$data["controller"]]['comment'])).'"');
			if(isset($_FILES['data']["name"][$data["controller"]]["file1"]) && $_FILES['data']["name"][$data["controller"]]["file1"]!='')
			{
				$filetype = end(@explode('.', strtolower($_FILES["data"]["name"][$data["controller"]]["file1"])));
				$basicfiletype=$_FILES['data']["type"][$data["controller"]]["file1"];
				if(in_array($filetype, array('jpg', 'jpeg', 'png', 'gif')) && in_array($basicfiletype, array('image/jpeg', 'image/gif', 'image/png')))
				{
					if($_FILES["data"]['size'][$data["controller"]]['file1']<=2097152)
					{
						$imagename1=date("dYmiHs").".".$filetype;
						$processorreturn['data']= array("comment" => '"'.strip_tags(trim($_POST['data'][$data["controller"]]['comment'])).'"',"file1" => '"'.$imagename1.'"');
						
					}
					else
					{
						$processorreturn['data']='';
						$processorreturn['message']='<script language="javascript">$("#UpdateMessage").removeClass("formsuccess").addClass("formerror");$("#UpdateMessage").html("'.__("Attachments : Only .jpg, .jpeg, .gif and .png files are allowed to upload. Max file size is 2 MB.").'").show();</script>';
					}
				}
				else
				{
					$processorreturn['data']='';
					$processorreturn['message']='<script language="javascript">$("#UpdateMessage").removeClass("formsuccess").addClass("formerror");$("#UpdateMessage").html("'.__("Attachments : Only .jpg, .jpeg, .gif and .png files are allowed to upload. Max file size is 2 MB.").'").show();</script>';
				}
			}
			if(isset($_FILES['data']["name"][$data["controller"]]["file2"]) && $_FILES['data']["name"][$data["controller"]]["file2"]!='')
			{
				$filetype = end(@explode('.', strtolower($_FILES["data"]["name"][$data["controller"]]["file2"])));
				$basicfiletype=$_FILES['data']["type"][$data["controller"]]["file2"];
				if(in_array($filetype, array('jpg', 'jpeg', 'png', 'gif')) && in_array($basicfiletype, array('image/jpeg', 'image/gif', 'image/png')))
				{
					if($_FILES["data"]['size'][$data["controller"]]['file2']<=2097152)
					{
						$imagename2=(date("dYmiHs")+1).".".$filetype;
						if(isset($imagename1)&& $imagename1!='')
						{
							$processorreturn['data']= array("comment" => '"'.strip_tags(trim($_POST['data'][$data["controller"]]['comment'])).'"',"file1" => '"'.$imagename1.'"',"file2" => '"'.$imagename2.'"');
						}
						else
						{
							$processorreturn['data']= array("comment" => '"'.strip_tags(trim($_POST['data'][$data["controller"]]['comment'])).'"',"file2" => '"'.$imagename2.'"');
						}
					}
					else
					{
						$processorreturn['data']='';
						$processorreturn['message']='<script language="javascript">$("#UpdateMessage").removeClass("formsuccess").addClass("formerror");$("#UpdateMessage").html("'.__("Attachments : Only .jpg, .jpeg, .gif and .png files are allowed to upload. Max file size is 2 MB.").'").show();</script>';
					}
				}
				else
				{
					$processorreturn['data']='';
					$processorreturn['message']='<script language="javascript">$("#UpdateMessage").removeClass("formsuccess").addClass("formerror");$("#UpdateMessage").html("'.__("Attachments : Only .jpg, .jpeg, .gif and .png files are allowed to upload. Max file size is 2 MB.").'").show();</script>';
				}
			}
		}
		else
		{
			$processorreturn['data']='';
			$processorreturn['message']='<script language="javascript">$("#UpdateMessage").removeClass("formsuccess").addClass("formerror");$("#UpdateMessage").html("'.__("Please enter your comments. It generally contains your bank account details.").'").show();</script>';
		}	
		
		if($processorreturn['message']=='')
		{
			if(isset($_FILES["data"]["name"][$data["controller"]]["file1"]) && $_FILES["data"]["name"][$data["controller"]]["file1"]!='' && $imagename1!='')
			{
				$filepath1=WWW_ROOT."img".DS."bankwire".DS.$imagename1;
				@move_uploaded_file($_FILES["data"]["tmp_name"][$data["controller"]]["file1"],$filepath1);
			}
			if(isset($_FILES["data"]["name"][$data["controller"]]["file2"]) && $_FILES["data"]["name"][$data["controller"]]["file2"]!='' && $imagename2!='')
			{
				$filepath2=WWW_ROOT."img".DS."bankwire".DS.$imagename2;
				@move_uploaded_file($_FILES["data"]["tmp_name"][$data["controller"]]["file2"],$filepath2);
			}
			$processorreturn['message']='<script language="javascript">window.location="'.$data['siteurl'].'thankyou/addfund/done";</script>';
		}
		
		return $processorreturn;
	}
	function getextrafield($processordata='')
	{
		$processorreturn='<div class="form-row vat">
			<div class="form-col-1">';
			$processorreturn.=__("Payment Details");
			$processorreturn.=': </div>
			<div class="lh20 form-col-2">';
			$processorreturn.=nl2br(stripslashes($processordata["Processor"]["comment"]));
			$processorreturn.='</div>
		</div>
		<div class="form-row">
			<div class="form-col-1">';
			$processorreturn.= __("Comments");
			$processorreturn.=': </div>
			<textarea id="MemberComment" rows="6" cols="30" class="formtextarea " title="Comment" name="data['.$processordata["controller"].'][comment]"></textarea>
		</div>
		<div class="form-row">
			<div class="form-col-1">&nbsp;</div>
			<div class="form-col-2">
				<div class="height10"></div>
				<div class="inline">';
				$processorreturn.=__("File");
				$processorreturn.=' 1 :
					<div class="browse_wrapper">
						<input id="MemberFile1" class="" type="file" style="display:inline-block" name="data['.$processordata["controller"].'][file1]">
					</div>
				</div>
				<div class="inline">';
				$processorreturn.=__("File");
				$processorreturn.=' 2 :
					<div class="browse_wrapper">
						<input id="MemberFile2" class="" type="file" name="data['.$processordata["controller"].'][file2]">
					</div>
				</div>
				<div class="height10"></div>
			</div>
		</div>
		<div class="form-row">
			<div class="note-box form-text">
				<b><span class="red-color">*';
				$processorreturn.=__("Note");
				$processorreturn.=':</span></b>';
				$processorreturn.=__("Attachments : Only .jpg, .jpeg, .gif and .png files are allowed to upload. Max file size is 2 MB.");
				$processorreturn.='
			</div>
		</div>';
		return $processorreturn;
	}
	function getExtraFieldWithdrawal($processordata='')
	{
		$processorreturn='<div class="form-row earning_interval_typefields">
						<div class="form-col-1">'.__("Comments").': </div>
						<textarea id="WithdrawComment" rows="6" cols="30" style="width:60%;" class="formtextarea" title="Comments" name="data[Withdraw][comment]"></textarea>
						<span class="helptooltip vtip" title="';
		$processorreturn.=__("Specify your bank details and account details here for admin to make payment towards your withdrawal request.");
		$processorreturn.='" style="">
						</div>';
		return $processorreturn;
	}
	function extraFieldWithdrawaldata($processordata='')
	{
		$processorreturn=array();
		$processorreturn['message']='';
			
		if(trim($_POST['data']['Withdraw']['comment'])!='')
		{
			$processorreturn['data'][]= array('field'=>"comment",'value'=>strip_tags(trim($_POST['data']['Withdraw']['comment'])));
		}
		else
		{
			$processorreturn['data']='';
			$processorreturn['message']='<script language="javascript">$("#UpdateMessage").removeClass("formsuccess").addClass("formerror");$("#UpdateMessage").html("'.__("Please enter your comments. It generally contains your bank account details.").'").show();</script>';
		}
		
		return $processorreturn;
	}
	function paymentextrafield($depositedata='')
	{
		
		$processorreturn='<tr class="c1">
			<td class="main-content-from-text" width="30%" valign="top">Comment :</td>
			<td>'.nl2br(stripslashes($depositedata["Member_fee_payment"]["comment"])).'</td>
		  </tr>
		  <tr class="c1">
			<td class="main-content-from-text">File :</td>
				<td><div class="marginleft9">';
				if($depositedata["Member_fee_payment"]["file1"]!="") { 
					$processorreturn.='<a href="'.$depositedata["Member_fee_payment"]["SITEURL"].'img/bankwire/'.$depositedata["Member_fee_payment"]["file1"].'" rel="lightbox" title="'.$depositedata["Member_fee_payment"]["file1"].'">'.$depositedata["Member_fee_payment"]["file1"].'</a>';
					} 
				if($depositedata["Member_fee_payment"]["file2"]!="") {
					$processorreturn.='&nbsp;&nbsp;<a href="'.$depositedata["Member_fee_payment"]["SITEURL"].'img/bankwire/'.$depositedata["Member_fee_payment"]["file2"].'" rel="lightbox" title="'.$depositedata["Member_fee_payment"]["file2"].'">'.$depositedata["Member_fee_payment"]["file2"].'</a>';
					}
					$processorreturn.='</div>
			</td>
		  </tr>';
		return $processorreturn;
	}
	function pendingpaymentdata($processordata='',$depositdata='',$controller='')
	{
		$processorreturn='<div class="fromnewtext c1">Processor : </div>
				<div class="fromborderdropedown4 c1">'.$depositdata[$controller]["processor"].'</div>
				<div class="fromnewtext c1">IP Address : </div>
				<div class="fromborderdropedown4 c1">'.$depositdata[$controller]["ip_address"].'</div>
				<div class="fromnewtext c1">Pay. Date : </div>
				<div class="fromborderdropedown4 c1">'.$depositdata[$controller]["payment_date"].'</div>
				<div class="fromnewtext c1">Amount : </div>
				<div class="fromborderdropedown4 c1">$'.$depositdata[$controller]["amount"].'</div>
				<div class="fromnewtext c1">Fees : </div>
				<div class="fromborderdropedown4 c1">$'.$depositdata[$controller]["fees"].'</div>
				<div class="fromnewtext c1">Paid Amount :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3 c1"><input type="text" id="Pending_depositAddFund" class="fromboxbg" value="'.$depositdata[$controller]["paidamount"].'" name="data['.$controller.'][add_fund]">
				</div>
				<div class="fromnewtext c1">Comment :<span class="red-color">*</span></div>
				  <div class="frombordermain c1"><textarea id="Pending_depositComment" rows="6" cols="30" class="from-textarea" name="data['.$controller.'][comment]">'.stripslashes($depositdata[$controller]["comment"]).'</textarea>
				</div>
				<div class="fromnewtext c1">File : </div>
				<div class="fromborderdropedown4 c1">';
					  if($depositdata[$controller]["file1"]!="") { 
						  $processorreturn.='<a href="'.$depositdata[$controller]["SITEURL"].'img/bankwire/'.$depositdata[$controller]["file1"].'" rel="lightbox" title="'.$depositdata[$controller]["file1"].'">'.$depositdata[$controller]["file1"].'</a>'; }
					  if($depositdata[$controller]["file2"]!="") {
					  $processorreturn.='&nbsp;&nbsp;<a href="'.$depositdata[$controller]["SITEURL"].'img/bankwire/'.$depositdata[$controller]["file2"].'" rel="lightbox" title="'.$depositdata[$controller]["file2"].'">'.$depositdata[$controller]["file2"].'</a>'; }
					  $processorreturn.='</div>
				
				
				<div class="fromnewtext c2" style="display:none">Processor :</div>
				<div class="fromborderdropedown4 c2" style="display:none">'.$depositdata[$controller]["processor"].'</div>
				<div class="fromnewtext c2" style="display:none">Amount :</div>
				<div class="fromborderdropedown4 c2" style="display:none"><span id="amount" ></span></div>
				<div class="fromnewtext c2" style="display:none">Fees : </div>
				<div class="fromborderdropedown4 c2" style="display:none"><span id="fees"></span></div>
				<div class="fromnewtext c2" style="display:none">Paid Amount : </div>
				<div class="fromborderdropedown4 c2" style="display:none"><span id="paidamount"></span></div>';
		return $processorreturn;
	}
	function GetBalance($processordata='')
	{
		return '';
	}
}
?>