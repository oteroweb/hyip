<?php
class SolidTrustPay
{
	function getFormAddProcesser($processordata,$SITEURL='')
	{
		$processorreturn=array();
		$processorreturn[0] ='<div class="fromnewtext">Processor Name : SolidTrustPay</div>
				<div class="fromnewtext">Routine URL :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_url]" id="SitesettingProcUrl" class="fromboxbg" value="'.$processordata["Processor"]["proc_url"].'" />
				</div>
				<div class="fromnewtext">Your STP Username :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_accid]" id="SitesettingProcAccid" class="fromboxbg" value="'.$processordata["Processor"]["proc_accid"].'" />
				</div>
				<div class="fromnewtext">Your Button Name (shopname) :</div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][lr_api]" id="lr_api" class="fromboxbg" value="'.$processordata["Processor"]["lr_api"].'" />
				</div>
				<div class="fromnewtext">Secondary Password/Button Password :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_signature\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_signature]" style="display:none;" id="api_signature" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Maximum Request Amount Per Day ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][max_req_amt]" id="SitesettingMaxReqAmt" class="fromboxbg" value="'.$processordata["Processor"]["max_req_amt"].'" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Add Fund Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee_amount"].'" name="data[Sitesetting][proc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Add Fund Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee"].'" name="data[Sitesetting][proc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="fromnewtext">Test Mode :</div>
				<div class="fromborderdropedown3">
			    <div class="select-main">
				    <label>
					<select id="SitesettingTestmode" class="" style="" name="data[Sitesetting][testmode]">
						<option value="0">No</option>
						<option '; if($processordata["Processor"]["testmode"]==1){ $processorreturn[0].='selected="selected"';
						} $processorreturn[0].='value="1">Yes</option>
					</select>
					</label>
					</div>
				</div>';
		$processorreturn[1]='<div class="fromnewtext">STP Api Name :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_name\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_name]" style="display:none;" id="api_name" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Api Password :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_password\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_password]" style="display:none;" id="api_password" class="fromboxbg" value="" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Withdrawal Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee_amount"].'" name="data[Sitesetting][mproc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Withdrawal Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee"].'" name="data[Sitesetting][mproc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[2]='';
		return $processorreturn;
	}
	
	function getFormAddfund($processordata,$data)
	{
		$processorreturn=array();
		$processorreturn['message']='';
		
		echo '<form action="'.$processordata['Processor']['proc_url'].'" method="post" id="service">';
		echo 	'<input type=hidden name="merchantAccount" value="'.$processordata['Processor']['proc_accid'].'" />';
		echo 	'<input type=hidden name="amount" value="'.round($data['total'],2).'" />';
		echo	'<input type=hidden name="currency" value="USD" />';
		echo 	'<input type=hidden name="item_id" value="'.$data['type'].'" />';
		echo	'<input type=hidden name="notify_url" value="'.$data['siteurl'].'solidtrustnotify/index'.'" />';
		//echo	'<input type=hidden name="confirm_url" value="http://www.yoursite.com/stp_confirm.php" />';
		echo	'<input type=hidden name="return_url" value="'.$data['siteurl'].'thankyou/addfund/done'.'" />';
		echo	'<input type=hidden name="cancel_url" value="'.$data['siteurl'].'thankyou/addfund/cancel'.'" />';
		echo	'<input type=hidden name="user1" value="'.$data['userid'].'" />';
		echo 	'<input type=hidden name="user2" value="'.$_SERVER['REMOTE_ADDR'].'" />';
		echo	'<input type=hidden name="user3" value="Member Id - '.$data['userid'].'" />';
		echo	'<input type=hidden name="user4" value="'.$data['lastid'].'" />';
		echo	'<input type=hidden name="user5" value="'.$data['callmethod'].'" />';
		echo	'<input type=hidden name="handshake" value="0" />';
		echo 	'<input type="hidden" name="sci_name" value="'.$processordata['Processor']['lr_api'].'">';
		if($processordata['Processor']['testmode']==1)
		{
			echo '<input type=hidden name="testmode" value="on" />';
		}
		echo '</form>';
		echo '<script language="javascript">document.getElementById("service").submit();</script>';	
		
		return $processorreturn;
	}
	
	function getdatawithdraw($processordata='',$withdrawdata='')
	{
		$processorreturn=array();
		$processorreturn[0]='';
		$processorreturn[1]='';
		$api_signature =stripslashes($processordata['Processor']['api_signature']);
		$api_signature=strtoupper(md5($api_signature))."K%T%G".strtoupper(md5(date("ymdhis")));
		
		$urladdress = "https://solidtrustpay.com/accapi/process.php";
		$api_id =stripslashes($processordata['Processor']['api_name']);
		$api_pwd = stripslashes($processordata['Processor']['api_password']);
		$api_pwd = md5($api_pwd.'s+E_a*');
		$totwith=0;
		$totselamt=0;
		if(count($withdrawdata)>0)
		{
			$processorreturn[1].='<div class="Masspay-Message-Title">SolidTrustPay MassPay Message</div><div class="Masspay-Message-Description">Description : </div><div class="Masspay-Message-Error">';
		}
		
		foreach($withdrawdata as $withdrow)
		{
			$user=$withdrow['Withdraw']['pro_acc_id'];//send to
			$testmode=0;//set to 1 for enable and 0 for disable
			if($processordata['Processor']['testmode']==1)
				$testmode=1;
			$amount=$withdrow['Withdraw']['amount'];
			$currency='USD';
			$comments="masspay comment";
			$fee=$withdrow['Withdraw']['fee'];
			//$udf1=$datam['member_id'];
			$udf1 = $withdrow['Withdraw']['member_id']."|".$withdrow['Withdraw']['with_id']."|".$withdrow['Withdraw']['fee']."|".$api_signature;
			
			$data = "user=".$user. "&testmode=".$testmode."&api_id=".$api_id. "&api_pwd=".$api_pwd. "&amount=".$amount."&paycurrency=".$currency."&comments=".$comments."&fee=".$fee."&udf1=".$udf1."";
			
			$processorreturn[0][]='Fail';
			$processorreturn[4][0][] = array("Withdraw.status" => "'Pendingipn'");
			$processorreturn[4][1][] = $withdrow['Withdraw']['with_id'];
			
			// Call STP API
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"$urladdress");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0); //use this to suppress output
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);// tell cURL to graciously accept an SSL certificate
			$result = curl_exec ($ch) or die(curl_error($ch));
			$totwith++;
			$totselamt=$totselamt+$amount;
			$processorreturn[1].='<div>'.trim($result).'</div>';
			
			curl_close ($ch);
		}
		if(count($withdrawdata)>0)
		{
			$processorreturn[1].='</div><div class="Masspay-Message-Total">Total Members : '.$totwith.'</div>
			<div class="Masspay-Message-Total">Total Amount : $'.$totselamt.'</div>';
		}
		return $processorreturn;
	}
	function GetBalance($processordata='')
	{
		return "";
	}

}
?>