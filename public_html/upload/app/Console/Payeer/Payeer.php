<?php
include("cpayeer.php");
class Payeer
{
	function getFormAddProcesser($processordata,$SITEURL='')
	{
		$processorreturn=array();
		$processorreturn[0] ='<div class="fromnewtext">Processor Name : Payeer</div>
				<div class="fromnewtext">Routine URL :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_url]" id="SitesettingProcUrl" class="fromboxbg" value="'.$processordata["Processor"]["proc_url"].'" />
				</div>
				<div class="fromnewtext">Payee Account :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_accid]" id="SitesettingProcAccid" class="fromboxbg" value="'.$processordata["Processor"]["proc_accid"].'" />
				</div>
				<div class="fromnewtext">Shop Secret Key :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_password\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_password]" style="display:none;" id="api_password" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Maximum Request Amount Per Day ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][max_req_amt]" id="SitesettingMaxReqAmt" class="fromboxbg" value="'.$processordata["Processor"]["max_req_amt"].'" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Add Fund Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee_amount"].'" name="data[Sitesetting][proc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Add Fund Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee"].'" name="data[Sitesetting][proc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[1]='<div class="fromnewtext">Payee Account Number :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_name\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_name]" style="display:none;" id="api_name" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Payeer API ID :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'lr_api\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][lr_api]" style="display:none;" id="lr_api" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Payeer API Secret Key :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'lr_security\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][lr_security]" style="display:none;" id="lr_security" class="fromboxbg" value="" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Withdrawal Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee_amount"].'" name="data[Sitesetting][mproc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Withdrawal Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee"].'" name="data[Sitesetting][mproc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[2]='';
		return $processorreturn;
	}
	function getFormAddfund($processordata,$data)
	{
		$processorreturn=array();
		$processorreturn['message']='';
		
		$m_shop = $processordata['Processor']['proc_accid'];
		$m_orderid = $data['userid'];
		$m_amount = number_format($data['total'], 2, '.', '');
		$m_curr = 'USD';
		$m_desc = base64_encode($data['type'].",".$data['lastid'].",".$_SERVER['REMOTE_ADDR'].",".$data['callmethod']);
		$m_key = $processordata['Processor']['api_password'];
		
		$arHash = array(
			$m_shop,
			$m_orderid,
			$m_amount,
			$m_curr,
			$m_desc,
			$m_key
		);
		$sign = strtoupper(hash('sha256', implode(":", $arHash)));
		
		echo '<form method="GET" action="'.$processordata['Processor']['proc_url'].'" id="service">';
		echo '<input type="hidden" name="m_shop" value="'.$m_shop.'" />';
		echo '<input type="hidden" name="m_orderid" value="'.$m_orderid.'">';
		echo '<input type="hidden" name="m_amount" value="'.$m_amount.'">';
		echo '<input type="hidden" name="m_curr" value="'.$m_curr.'">';
		echo '<input type="hidden" name="m_desc" value="'.$m_desc.'">';
		echo '<input type="hidden" name="m_sign" value="'.$sign.'">';
		echo '<input type="hidden" name="m_process" value="send">';
		echo '</form>';
		echo '<script language="javascript">document.getElementById("service").submit();</script>';
			
		return $processorreturn;
	}
	
	function getdatawithdraw($processordata='',$withdrawdata='')
	{
		$processorreturn=array();
		
		$accountNumber = $processordata['Processor']['api_name'];
		$apiId = $processordata['Processor']['lr_api'];
		$apiKey = $processordata['Processor']['lr_security'];
		$totwith=0;
		$totselamt=0;
		if(count($withdrawdata)>0)
		{
			$processorreturn[1]='<div class="Masspay-Message-Title">Payeer MassPay Message</div> <div class="Masspay-Message-Description">Description : </div><div class="Masspay-Message-Error">';
		}
		foreach($withdrawdata as $withdrow)
		{
			$comment = "MassPay Payeer";
			$payeer = new CPayeer($accountNumber, $apiId, $apiKey);
			if ($payeer->isAuth())
			{
				$arTransfer = $payeer->transfer(array(
					'curIn' => 'USD', // write-off account 
					'sum' => $withdrow['Withdraw']['amount'], // Amount to receive 
					'curOut' => 'USD', // receiving currency  
					'to' => $withdrow['Withdraw']['pro_acc_id'], // Recipient (email)
					//'to' => '+01112223344',  // Recipient (Phone)
					//'to' => 'P1000000',  // Recipient (Account number)
					'comment' => $comment,
					'anonim' => 'Y' // anonymous transfer 
					//'protect' => 'Y', // protect transaction 
					//'protectPeriod' => '3', // protection period (from 1 to 30 days) 
					//'protectCode' => '12345', // protection code 
				));
				if (!empty($arTransfer["historyId"]))
				{
					$status = "Done";
					$processorreturn[0][]='Done';
				}
				else
				{
					$status =  "FAIL";
					$processorreturn[0][]='Fail';
				}
				
				$processorreturn[2][] = array('id' => NULL, 'batch_number' => $arTransfer["historyId"], 'return_code' => 0, 'return_desc' => $status, 'refrence_no' => $arTransfer["historyId"], 'dt' => date('Y-m-d H:i:s'), 'amount' => $withdrow['Withdraw']['amount'], 'sender_email' => 'Admin', 'receiver_email' => $withdrow['Withdraw']['pro_acc_id'], 'receiver_id' => $withdrow['Withdraw']['member_id'], 'with_id' => $withdrow['Withdraw']['with_id'], 'processor' => 'Payeer', 'processor_id' => $withdrow['Withdraw']['processorid']);
				$totwith++;
				$totselamt=$totselamt+$withdrow['Withdraw']['amount'];
				$processorreturn[1].="<div><span>Payment Status : </span><span>".$status."</span></div>
				<div><span>Account : </span><span>".$withdrow['Withdraw']['pro_acc_id']."</span></div>
				<div><span>Amount : </span><span>".$withdrow['Withdraw']['amount']."</span</div>";
			}
			else
			{
				$status =  "FAIL";
				$processorreturn[0][]='Fail';
				
				$processorreturn[2][] = array('id' => NULL, 'batch_number' => "-", 'return_code' => 0, 'return_desc' => $status, 'refrence_no' => '-', 'dt' => date('Y-m-d H:i:s'), 'amount' => $withdrow['Withdraw']['amount'], 'sender_email' => 'Admin', 'receiver_email' => $withdrow['Withdraw']['pro_acc_id'], 'receiver_id' => $withdrow['Withdraw']['member_id'], 'with_id' => $withdrow['Withdraw']['with_id'], 'processor' => 'Payeer', 'processor_id' => $withdrow['Withdraw']['processorid']);
				
				$processorreturn[1].="<div><span>Payment Status : </span><span>".$status.", </span></div>
				<div><span>Account : </span><span>".$withdrow['Withdraw']['pro_acc_id']."</span></div>
				<div><span>Amount : </span><span>".$withdrow['Withdraw']['amount']."</span></div>";
			}
		}
		if(count($withdrawdata)>0)
		{
			$processorreturn[1].='</div><div class="Masspay-Message-Total">Total Members : '.$totwith.'</div>
			<div class="Masspay-Message-Total">Total Amount : $'.$totselamt.'</div>';
		}
		return $processorreturn;
	}
	function GetBalance($processordata='')
	{
		$payeer = new CPayeer(stripslashes($processordata['Processor']['api_name']), stripslashes($processordata['Processor']['lr_api']), stripslashes($processordata['Processor']['lr_security']));
		if ($payeer->isAuth())
		{
			$arBalance = $payeer->getBalance();
			return $arBalance['balance']['USD']['BUDGET']." USD";//BUDGET,DOSTUPNO,DOSTUPNO_SYST
		}
		else
		{
			return $payeer->getErrors();
		}
	}
}
?>