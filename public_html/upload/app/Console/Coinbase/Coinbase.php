<?php
require __DIR__ . '/autoload.php';
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Enum\CurrencyCode;
use Coinbase\Wallet\Resource\Checkout;
use Coinbase\Wallet\Resource\Order;
use Coinbase\Wallet\Value\Money;
use Coinbase\Wallet\Authentication\Authentication;
use Coinbase\Wallet\Authentication\ApiKeyAuthentication;
class Coinbase
{
	private $client;
	function callbackauth($apiKey,$apiSecret,$testmode)
	{
		$configuration = Configuration::apiKey($apiKey, $apiSecret);
		if($testmode==1)
		{
			$configuration->setApiUrl(Configuration::SANDBOX_API_URL);
		}
		return $configuration;
	}
	function createclient($configuration)
	{
		return $this->client = Client::create($configuration);
	}
	/*function getccountinfo()
	{
		return $this->client->getPrimaryAccount();
	}*/
        function getFormAddProcesser($processordata,$SITEURL='')
	{
		$processorreturn=array();
		$processorreturn[0] ='<div class="fromnewtext">Processor Name : Coinbase</div>
				<div class="fromnewtext">Routine URL :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_url]" id="SitesettingProcUrl" class="fromboxbg" value="'.$processordata["Processor"]["proc_url"].'" />
				</div>
				<div class="fromnewtext">Merchant API Key :<span class="red-color">*</span></div>
                                <div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_name\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_name]" style="display:none;" id="api_name" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Merchant API Secret :</div>
                                <div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_password\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_password]" style="display:none;" id="api_password" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Maximum Request Amount Per Day ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][max_req_amt]" id="SitesettingMaxReqAmt" class="fromboxbg" value="'.$processordata["Processor"]["max_req_amt"].'" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Add Fund Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee_amount"].'" name="data[Sitesetting][proc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Add Fund Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee"].'" name="data[Sitesetting][proc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
                                <div class="fromnewtext">Test Mode :</div>
				<div class="fromborderdropedown3">
                                <div class="select-main">
				    <label>
					<select id="SitesettingTestmode" class="" style="" name="data[Sitesetting][testmode]">
						<option value="0">No</option>
						<option '; if($processordata["Processor"]["testmode"]==1){ $processorreturn[0].='selected="selected"';
						} $processorreturn[0].='value="1">Yes</option>
					</select>
					</label>
					</div>
				</div>';
		$processorreturn[1]='<div class="formborderfinance">
				<div class="fromnewtext">Fixed Withdrawal Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee_amount"].'" name="data[Sitesetting][mproc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Withdrawal Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee"].'" name="data[Sitesetting][mproc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[2]='';
		return $processorreturn;
	}
        function getFormAddfund($processordata,$data)
        {
            $apiKey = $processordata['Processor']['api_name'];
            $apiSecret = $processordata['Processor']['api_password'];
            $configuration = Configuration::apiKey($apiKey, $apiSecret);
            if($processordata['Processor']['testmode']==1)
	    {
                $proaccurl="https://sandbox.coinbase.com/checkouts";
                $configuration->setApiUrl(Configuration::SANDBOX_API_URL); // remove this line to not use the sandbox
            }
            else
            {
                $proaccurl=$processordata['Processor']['proc_url'];
                $configuration->setApiUrl(Configuration::DEFAULT_API_URL);
            }
            $client = Client::create($configuration);
            
            $order = new Checkout([
                'style' => 'buy_now_large',
                'amount' => new Money($data['total'], CurrencyCode::USD),
                'currency' => 'USD', 
                'name' => $data['type'],
                'type' => 'order',
                'description' => $data['userid'].'|'.$data['callmethod'].'|'.$_SERVER['REMOTE_ADDR'].'|'.$data['lastid'],
                'callback_url' => $data['siteurl'].'coinbasenotify/index',
                'success_url' => $data['siteurl'].'thankyou/addfund/done',
                'cancel_url' => $data['siteurl'].'thankyou/addfund/cancel',
                'notifications_url' => $data['siteurl'].'coinbasenotify/index'
            ]);
            $client->createCheckout($order);
            $checkouts = $client->getCheckouts();
            $code = $checkouts[0]->getEmbedCode();
            echo '<a id="service" class="coinbase-button" style="display: none;" data-code="'.$code.'"  href="'.$proaccurl.'/'.$code.'">Pay with Coinbase</a>';
            echo '<script language="javascript">document.getElementById("service").click();</script>';
        }

		function getdatawithdraw($processordata='',$withdrawdata='')
		{
			
			$processorreturn=array();
			$totwith=0;
			$totselamt=0;
			if(count($withdrawdata)>0)
			{
				$processorreturn[1]='<div class="Masspay-Message-Title">Coinbase MassPay Message</div> <div class="Masspay-Message-Description">Description : </div><div class="Masspay-Message-Error">';
			}
			$apiKey = $processordata['Processor']['api_name'];
            $apiSecret = $processordata['Processor']['api_password'];
            $configuration = Configuration::apiKey($apiKey, $apiSecret);
            if($processordata['Processor']['testmode']==1)
	    	{
                $proaccurl="https://sandbox.coinbase.com/checkouts";
                $configuration->setApiUrl(Configuration::SANDBOX_API_URL); // remove this line to not use the sandbox
            }
            else
            {
                $proaccurl=$processordata['Processor']['proc_url'];
                $configuration->setApiUrl(Configuration::DEFAULT_API_URL);
            }
            $client = Client::create($configuration);
            $account = $client->getPrimaryAccount();
			foreach($withdrawdata as $withdrow)
			{
				$transaction = Transaction::send([
			        'toBitcoinAddress' => $withdrow['Withdraw']['pro_acc_id'],
			        'amount'           => new Money($withdrow['Withdraw']['amount'], CurrencyCode::USD),
			        'description'      => 'Bitcoin Transaction from member id -> '.$withdrow['Withdraw']['member_id']
			    ]);
			    $client->createAccountTransaction($account, $transaction);
			    $transactionreturn=$transaction->getStatus();

			    $processorreturn[0][]='Fail';
			    if($transactionreturn=='pending')
			    {
			        $description='';
					$processorreturn[4][0][] = array("Withdraw.status" => "'Pendingipn'");
					$processorreturn[4][1][] = $withdrow['Withdraw']['with_id'];

					$totwith++;	
					$totselamt=$totselamt+$withdrawdata['Withdraw']['amount'];
					$processorreturn[1].="<div><span>Payment Status : </span><span>Done</span></div>
					<div><span>Account : </span><span>".$withdrow['Withdraw']['pro_acc_id']."</span></div>
					<div><span>Amount : </span><span>".$withdrawdata['Withdraw']['amount']."</span></div>";
			    }
			    else
			    {
			       $processorreturn[0][]='Fail';
				
					$processorreturn[2][] = array('id' => NULL, 'batch_number' => 0, 'return_code' => '-', 'return_desc' => $e->getMessage(), 'refrence_no' => '-', 'dt' => date('Y-m-d H:i:s'), 'amount' => $withdrawdata['Withdraw']['amount'], 'sender_email' => 'Admin', 'receiver_email' =>  $withdrawdata['Withdraw']['pro_acc_id'], 'receiver_id' => $withdrawdata['Withdraw']['member_id'], 'with_id' => $withdrawdata['Withdraw']['with_id'], 'processor' => 'Coinbase', 'processor_id' => $withdrawdata['Withdraw']['processorid']);
					
					$processorreturn[1].="<div><span> Email : </span><span>".$withdrawdata['Withdraw']['pro_acc_id']."</span></div>
					<div><span>Error Code : </span><span>FAIL</span></div>";
			    }
				
			}
			
			if(count($withdrawdata)>0)
			{
				$processorreturn[1].='</div><div class="Masspay-Message-Total">Total Members : '.$totwith.'</div>
				<div class="Masspay-Message-Total">Total Amount : $'.$totselamt.'</div>';
			}
			return $processorreturn;
		}
		function getextrafield($processordata='')
		{
			$processorreturn='';
			return $processorreturn;
		}
		function getExtraFieldWithdrawal($processordata='')
		{
			
			$processorreturn='';
			return $processorreturn;
		}
		function extraFieldWithdrawaldata($processordata='')
		{
			
			$processorreturn='';
			return $processorreturn;
		
		}
		function paymentextrafield($depositedata='')
		{
			$processorreturn='';
			return $processorreturn;
		}
		function pendingpaymentdata($processordata='',$depositdata='',$controller='')
		{
			$processorreturn='';
			return $processorreturn;
		}
		function GetBalance($processordata='')
		{
			$processorreturn='';
			return $processorreturn;
		}
}
?>