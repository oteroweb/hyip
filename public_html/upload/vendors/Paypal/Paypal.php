<?php
class Paypal
{
	function getFormAddProcesser($processordata,$SITEURL='')
	{
		$processorreturn=array();
		$processorreturn[0] ='<div class="fromnewtext">Processor Name : PayPal</div>
				<div class="fromnewtext">Routine URL :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_url]" id="SitesettingProcUrl" class="fromboxbg" value="'.$processordata["Processor"]["proc_url"].'" />
				</div>
				<div class="fromnewtext">Your Paypal Email :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_accid]" id="SitesettingProcAccid" class="fromboxbg" value="'.$processordata["Processor"]["proc_accid"].'" />
				</div>
				<div class="fromnewtext">Maximum Request Amount Per Day ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][max_req_amt]" id="SitesettingMaxReqAmt" class="fromboxbg" value="'.$processordata["Processor"]["max_req_amt"].'" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Add Fund Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee_amount"].'" name="data[Sitesetting][proc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Add Fund Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee"].'" name="data[Sitesetting][proc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[1]='<div class="fromnewtext">Paypal Api Name :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_name\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_name]" style="display:none;" id="api_name" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Api Password :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_password\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_password]" style="display:none;" id="api_password" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Api Signature :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_signature\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_signature]" style="display:none;" id="api_signature" class="fromboxbg" value="" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Withdrawal Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee_amount"].'" name="data[Sitesetting][mproc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Withdrawal Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee"].'" name="data[Sitesetting][mproc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[2]='';
		return $processorreturn;
	}
	
	function getFormAddfund($processordata,$data)
	{
		$processorreturn=array();
		$processorreturn['message']='';
		echo '<form method="post" action="'.$processordata['Processor']['proc_url'].'" id="service">';
		echo	 '<input type="hidden" value="_xclick" name="cmd">';
		echo     '<input type="hidden" value="'.$processordata['Processor']['proc_accid'].'" name="business">';
		echo     '<input type="hidden" value="'.round($data['total'],2).'" name="amount">';
		echo     '<input type="hidden" value="'.$data['userid'].'" name="item_number">';
		echo     '<input type="hidden" value="'.$_SERVER['REMOTE_ADDR'].'|'.$data['lastid'].'|'.$data['callmethod'].'" name="custom">';
		echo     '<input type="hidden" value="'.$data['type'].'" name="item_name">';
		echo     '<input type="hidden" value="USD" name="currency_code">';
		echo     '<input type="hidden" value="1" name="quantity">';
		echo     '<input type="hidden" value="1" name="no_shipping">';
		echo     '<input type="hidden" value="2" name="rm">';
		echo     '<input type="hidden" value="'.$data['siteurl'].'paypalnotify/index" name="notify_url">';
		echo     '<input type="hidden" value="'.$data['siteurl'].'thankyou/addfund/done" name="return">';
		echo     '<input type="hidden" value="'.$data['siteurl'].'thankyou/addfund/cancel" name="cancel_return">';       
		echo '</form>';
		
		echo '<script language="javascript">document.getElementById("service").submit();</script>';
		return $processorreturn;
	}
	
	function getdatawithdraw($processordata='',$withdrawdata='')
	{
		define('API_USERNAME',$processordata['api_name']);
		define('API_PASSWORD',$processordata['api_password']);
		define('API_SIGNATURE',$processordata['api_signature']);
		define('API_ENDPOINT', 'https://api-3t.paypal.com/nvp'); //$environment = 'sandbox';	// or 'beta-sandbox' or 'live'
		define('SUBJECT','');
		define('USE_PROXY',FALSE);
		define('PROXY_HOST', '127.0.0.1');
		define('PROXY_PORT', '808');
		//define('PAYPAL_URL', 'https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=');
		define('PAYPAL_URL', 'https://www.paypal.com/webscr&cmd=_express-checkout&token=');
		define('VERSION', '76.0');
		define('ACK_SUCCESS', 'SUCCESS');
		define('ACK_SUCCESS_WITH_WARNING', 'SUCCESSWITHWARNING');
		include("CallerService.php");
		
		$emailSubject =urlencode('You have money!');
		$receiverType = urlencode('EmailAddress');
		$currency=urlencode('USD');
		$processorreturn=array();
		$processorreturn[0]='';
		$payments=array();
		$totwith=0;
		$totselamt=0;
		$nvpstr='';
		foreach($withdrawdata as $withdrow)
		{
			$receiverEmail = urlencode($withdrow['Withdraw']['pro_acc_id']);
			$amount = urlencode($withdrow['Withdraw']['amount']);
			$uniqueID = urlencode($withdrow['Withdraw']['with_id']);
			$note = urlencode('MassPay');
			$nvpstr.="&L_EMAIL$totwith=$receiverEmail&L_Amt$totwith=$amount&L_UNIQUEID$totwith=$uniqueID&L_NOTE$totwith=$note";
			$totselamt=$totselamt+$withdrow['Withdraw']['amount'];
			$totwith++;
			
			$processorreturn[0][]='Fail';
			$processorreturn[4][0][] = array("Withdraw.status" => "'Pendingipn'");
			$processorreturn[4][1][] = $withdrow['Withdraw']['with_id'];
		}
		
		if($nvpstr!="")
		{
			/* Construct the request string that will be sent to PayPal.
			   The variable $nvpstr contains all the variables and is a
			   name value pair string with & as a delimiter */
			 
			 $nvpstr.="&EMAILSUBJECT=$emailSubject&RECEIVERTYPE=$receiverType&CURRENCYCODE=$currency" ;
			
			/* Make the API call to PayPal, using API signature.
			   The API response is stored in an associative array called $resArray */
			$resArray=hash_call("MassPay",$nvpstr);
			if(count($resArray)==0)
			{
				$resArray['Error']="An Error Occured. Please Check Your Paypal Credentials In 'Payment Processors' Page.</br>";
			}
			/* Display the API response back to the browser.
			   If the response from PayPal was a success, display the response parameters'
			   If the response was an error, display the errors received using APIError.php.
			   */
			$ack = strtoupper($resArray["ACK"]);
			$errormessage='';
			foreach($resArray as $key => $value) 
			{
				$errormessage.="<div><span>".$key." : </span><span>".$value."</span></div>";
			}
			$processorreturn[1]='<div class="Masspay-Message-Title">Paypal MassPay Message</div>
			<div class="Masspay-Message-Description">Description : </div>
			<div class="Masspay-Message-Error">'.$errormessage.'</div>
			<div class="Masspay-Message-Total">Total Members : '.$totwith.'</div>
			<div class="Masspay-Message-Total">Total Amount : $'.$totselamt.'</div>';
		}
		return $processorreturn;
	}
	function GetBalance($processordata='')
	{
		$environment = 'live';   // 'sandbox', 'beta-sandbox', or 'live'
		//$environment = 'sandbox';//, 'beta-sandbox', or 'live'
		$config = array(
			'username'  => $processordata['Processor']['api_name'],
			'password'  => $processordata['Processor']['api_password'],
			'signature' => $processordata['Processor']['api_signature'],
			'version'   => '51.0'
		);
		$action = 'GetBalance';
		
		switch ($environment) {
			case 'sandbox':
			case 'beta-sandbox':
				$url = "https://api-3t.$environment.paypal.com/nvp";
				break;
			default:
				$url = "https://api-3t.paypal.com/nvp";
		}
		
		foreach ($config as &$value) {
			$value = urlencode($value);
		}
		
		$request = http_build_query(array(
			"METHOD"    => $action,
			"VERSION"   => $config['version'],
			"USER"      => $config['username'],
			"PWD"       => $config['password'],
			"SIGNATURE" => $config['signature'],
			//"RETURNALLCURRENCIES" => 1,
		));
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);    
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		
		$response = curl_exec($ch);
		
		if (!$response) {
			$resArray =  array(curl_error($ch),curl_errno($ch));
			return $resArray;
			//echo 'Failed to retrieve paypal balance: ' . curl_error($ch) . ' (' . curl_errno($ch) . ')';
			exit;
		}
		
		parse_str($response, $result);
		
		foreach ($result as &$value) {
			$value = urldecode($value);
		}
		
		if (!isset($result['ACK']) || $result['ACK'] != "Success") {
			return $result;
			//echo "{$result['L_SEVERITYCODE0']} {$result['L_ERRORCODE0']}: {$result['L_SHORTMESSAGE0']}\n{$result['L_LONGMESSAGE0']}\n";
			exit;
		}
		return $result['L_AMT0']." ".$result['L_CURRENCYCODE0'];
		//echo "Paypal balance: {$result['L_CURRENCYCODE0']} {$result['L_AMT0']}";
	}

}
?>