<?php
class CountryFromIp
{
	var $_path;
	var $_found			= false;
	var $_start_ip 		= false;
	var $_end_ip 		= false;
	var $_country_code 	= false;
	var $_country_name 	= false;
	var $_data			= array();
	var $_longip		= 0;
	function CountryFromIp($path="")
	{
		$filename = $path."ipdb.csv";
		$this->_data = gzfile($filename);
	}
	private function convert2number($string)
	{
		$pattern= "\"([0-9]+)\"";
		if (ereg($pattern, $string, $regs))
			return (int)$regs[1];
	}
	private function search($beg, $end)
	{
		$mid = ceil(($end + $beg) / 2);

		$arr_beg = array();
		$arr_mid = array();
		$arr_end = array();

		$arr_beg = explode(",", $this->_data[$beg]);
		$arr_mid = explode(",", $this->_data[$mid]);
		$arr_end = explode(",", $this->_data[$end]);


		$arr_beg[2] = str_replace('"', '', $arr_beg[2]);
		$arr_beg[3] = str_replace('"', '', $arr_beg[3]);

		$arr_mid[2] = str_replace('"', '', $arr_mid[2]);
		$arr_mid[3] = str_replace('"', '', $arr_mid[3]);

		$arr_end[2] = str_replace('"', '', $arr_end[2]);
		$arr_end[3] = str_replace('"', '', $arr_end[3]);

		if (($this->_longip >= $arr_beg[2]) && ($this->_longip <= $arr_beg[3]))
		{
			unset($arr_beg); unset($arr_mid); unset($arr_end);
			return $beg;
		}
		elseif (($this->_longip >= $arr_mid[2]) && ($this->_longip <= $arr_mid[3]))
		{
			unset($arr_beg); unset($arr_mid); unset($arr_end);
			return $mid;
		}
		elseif (($this->_longip >= $arr_end[2]) && ($this->_longip <= $arr_end[3]))
		{
			unset($arr_beg); unset($arr_mid); unset($arr_end);
			return $end;

		}
		elseif (($this->_longip > $arr_beg[3]) && ($this->_longip < $arr_mid[2]))
		{
			unset($arr_beg); unset($arr_mid); unset($arr_end);
			return $this->search($beg, $mid);
		}
		elseif (($this->_longip > $arr_mid[3]) && ($this->_longip < $arr_end[2]))
		{
			unset($arr_beg); unset($arr_mid); unset($arr_end);
			return $this->search($mid, $end);
		}
		else
		{
			unset($arr_beg); unset($arr_mid); unset($arr_end);
			return false;
		}
	}
	private function IpAddress2IpNumber($address)
	{
		$pattern = "([0-9]+).([0-9]+).([0-9]+).([0-9]+)";

		if (ereg($pattern, $address, $regs))
			return $number = $regs[1] * 256 * 256 * 256 + $regs[2] * 256 * 256 + $regs[3] * 256 + $regs[4];
		else
			return false;
	}
	public function ip_country($ip)
	{
		// if not localhost ...
		if (($ip != "127.0.0.1") && ($ip != "0.0.0.1"))
		{
			$poz=false;
			$this->_longip = $this->IpAddress2IpNumber($ip);

			if ($this->_longip !== false)
				$poz = $this->search(0, sizeof($this->_data)-1);
			
			if ($poz !== false)
			{
				$info = explode(",", $this->_data[$poz]);

				$this->_found			= true;
				$this->_start_ip 		= str_replace('"', '', $info[0]);
				$this->_end_ip 			= str_replace('"', '', $info[1]);
				$this->_country_code 	= str_replace('"', '', $info[4]);
				$this->_country_name 	= str_replace('"', '', $info[5]);
			}
		}
	}
	public function getStartIp()		{return $this->_start_ip;}
	public function getEndIp()			{return $this->_end_ip;}
	public function getCountryCode()	{return $this->_country_code;}
	public function getCountryName()	{return $this->_country_name;}
	public function found()				{return $this->_found;}
	public function destroy()
	{
		unset($this->_found);
		unset($this->_start_ip);
		unset($this->_end_ip);
		unset($this->_country_code);
		unset($this->_country_name);
		unset($this->_data);
		unset($this);
	}
}
?>
