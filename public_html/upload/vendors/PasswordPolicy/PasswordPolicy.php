<?php
class PasswordPolicy 
{
    private $rules;     // Array of policy rules
    private $errors;    // Array of errors for the last validation
    function __construct ($params=array())
    {
        $this->rules['min_length'] = array(
            'value' => false,
            'type'  => 'integer',
            'test'  => 'return strlen($p)>=$v;',
            'error' => 'Password must be more than #VALUE# characters long');
            
        $this->rules['max_length'] = array(
            'value' => false,
            'type'  => 'integer',
            'test'  => 'return (strlen($p)<=$v);',
            'error' => 'Password must be less than #VALUE# characters long');
            
        $this->rules['min_numeric_chars'] = array(
            'value' => false,
            'type'  => 'integer',
            'test'  => 'return preg_match_all("/[0-9]/",$p,$x)>=$v;',
            'error' => 'Password must contain at least #VALUE# numbers');
            
        $this->rules['max_numeric_chars'] = array(
            'value' => false,
            'type'  => 'integer',
            'test'  => 'return preg_match_all("/[0-9]/",$p,$x)<=$v;',
            'error' => 'Password must contain no more than #VALUE# numbers');
        
        $this->rules['disallow_nonalphanumeric_chars'] = array(
            'value' => false,
            'type'  => 'boolean',
            'test'  => 'return preg_match_all("/[\W]/",$p,$x)==0;',
            'error' => 'Password must not contain non-alphanumeric characters');
            
        $this->rules['min_nonalphanumeric_chars'] = array(
            'value' => false,
            'type'  => 'integer',
            'test'  => 'return preg_match_all("/[\W]/",$p,$x)>=$v;',
            'error' => 'Password must contain at least #VALUE# non-aplhanumeric characters');
        
        // Apply params from constructor array
        foreach( $params as $k=>$v ) { $this->$k = $v; }
        
        // Errors defaults empty
        $this->errors = array();
        
        return 1;
    }
    public function __get($rule)
    {
        if( isset($this->rules[$rule]) ) return $this->rules[$rule]['value'];
                                         return false;   
    }
    public function __set($rule, $value)
    {
        if( isset($this->rules[$rule]) )
        {
            if( 'integer' == $this->rules[$rule]['type'] && is_int($value) )
            return $this->rules[$rule]['value'] = $value;
            
            if( 'boolean' == $this->rules[$rule]['type'] && is_bool($value) )
            return $this->rules[$rule]['value'] = $value;
        }
        return false;
    }
    public function policy()
    {
        $return = array();
        
        // Itterate over policy rules
        foreach( $this->rules as $k => $v )
        {
            // If rule is enabled, add string to array
            $string = $this->get_rule_error($k);
            if( $string ) $return[$k] = $string;
        }
        
        return $return;
    }
    public function validate($password)
    {
    
        foreach( $this->rules as $k=>$rule )
        {
            // Aliases for password and rule value
            $p = $password;
            $v = $rule['value'];
            
            // Apply each configured rule in turn
            if( $rule['value'] && !eval($rule['test']) )
            $this->errors[$k] = $this->get_rule_error($k);
        }
    
        return sizeof($this->errors) == 0;
    }
    public function get_errors()
    {
        return $this->errors;
    }
    private function get_rule_error($rule)
    {
        return ( isset($this->rules[$rule]) && $this->rules[$rule]['value'] ) 
        ? str_replace( '#VALUE#', $this->rules[$rule]['value'], $this->rules[$rule]['error'] )
        : false;
    }
}