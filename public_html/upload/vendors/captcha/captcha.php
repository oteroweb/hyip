<?php class captcha {	
	public function show_captcha($characters, $characterlenth, $textcolor, $bgimage, $fontfile) {
		if (session_id() == "") {
			session_name("CAKEPHP");
			session_start();
		}

		$path= VENDORS.'captcha';
		$imgname = $bgimage;//'default.jpg';
		$imgpath  = $path.'/images/'.$imgname;
		
		$captchatext='';
		$characters=$characters;//'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		$textcolorarray=explode(",",$textcolor);
		
		$length=$characterlenth;//5;
		while( strlen($captchatext) < $length ) {
			$captchatext .= substr($characters, rand() % (strlen($characters)), 1);
		}
		
		$_SESSION['captcha']=$captchatext;

		if (file_exists($imgpath) ){
			$im = imagecreatefromjpeg($imgpath); 
			$grey = imagecolorallocate($im, $textcolorarray[0], $textcolorarray[1], $textcolorarray[2]);
			$font = $path.'/fonts/'.$fontfile;//'AHGBold.ttf';
			
			imagettftext($im, 20, 0, 10, 25, $grey, $font, $captchatext) ;
			
			header('Content-Type: image/jpeg');
			header("Cache-control: private, no-cache");
			header ("Last-Modified: " . gmdate ("D, d M Y H:i:s") . " GMT");
			header("Pragma: no-cache");
			imagejpeg($im);
			
			imagedestroy($im);
			ob_flush();
			flush();
		}
		else{
			echo 'captcha error';
			exit;
		}		 
	}
}?>