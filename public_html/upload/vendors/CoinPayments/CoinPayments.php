<?php
include("coinpayments.inc.php");
class CoinPayments
{
	function getFormAddProcesser($processordata,$SITEURL='')
	{
		$processorreturn=array();
		$processorreturn[0] ='<div class="fromnewtext">Processor Name : CoinPayments</div>
				<div class="fromnewtext">Routine URL :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_url]" id="SitesettingProcUrl" class="fromboxbg" value="'.$processordata["Processor"]["proc_url"].'" />
				</div>
				<div class="fromnewtext">Merchant ID :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_accid]" id="SitesettingProcAccid" class="fromboxbg" value="'.$processordata["Processor"]["proc_accid"].'" />
				</div>
				<div class="fromnewtext">IPN Secret :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_name\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_name]" style="display:none;" id="api_name" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Maximum Request Amount Per Day ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][max_req_amt]" id="SitesettingMaxReqAmt" class="fromboxbg" value="'.$processordata["Processor"]["max_req_amt"].'" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Add Fund Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee_amount"].'" name="data[Sitesetting][proc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Add Fund Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee"].'" name="data[Sitesetting][proc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[1]='<div class="fromnewtext">API Private Key :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_password\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_password]" style="display:none;" id="api_password" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">API Public Key :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_signature\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_signature]" style="display:none;" id="api_signature" class="fromboxbg" value="" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Withdrawal Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee_amount"].'" name="data[Sitesetting][mproc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Withdrawal Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee"].'" name="data[Sitesetting][mproc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[2]='';
		return $processorreturn;
	}
	function getFormAddfund($processordata,$data)
	{
		echo '<form action="'.$processordata['Processor']['proc_url'].'" method="post" id="service">';
		echo '<input type="hidden" name="cmd" value="_pay" />';
		echo '<input type="hidden" name="reset" value="1" />';
		echo '<input type="hidden" name="want_shipping" value="0" />';
		echo '<input type="hidden" name="merchant" value="'.$processordata['Processor']['proc_accid'].'" />';
		echo '<input type="hidden" name="currency" value="USD" />';
		echo '<input type="hidden" name="amountf" value="'.$data['total'].'" />';
		echo '<input type="hidden" name="invoice" value="'.$data['userid'].'" />';
		echo '<input type="hidden" name="success_url" value="'.$data['siteurl'].'thankyou/addfund/done" />';
		echo '<input type="hidden" name="cancel_url" value="'.$data['siteurl'].'thankyou/addfund/cancel" />';
		echo '<input type="hidden" name="ipn_url" value="'.$data['siteurl'].'coinpaymentsnotify/index" />';
		echo '<input type="hidden" name="item_name" value="'.$data['type'].'" />';
		echo '<input type="hidden" name="custom" value="'.$data['callmethod'].'|'.$_SERVER['REMOTE_ADDR'].'|'.$data['lastid'].'" />';
		echo '<input type="hidden" name="item_number" value="'.$data['description'].'" />';
		echo '</form>';
		
		echo '<script language="javascript">document.getElementById("service").submit();</script>';
	}
	
	function getdatawithdraw($processordata='',$withdrawdata='')
	{
		
		$processorreturn=array();
		$totwith=0;
		$totselamt=0;
                $cp_status="Fail";
		if(count($withdrawdata)>0)
		{
			$processorreturn[1]='<div class="Masspay-Message-Title">CoinPayments MassPay Message</div> <div class="Masspay-Message-Description">Description : </div><div class="Masspay-Message-Error">';
		}
		$ipnurl=$processordata['Processor']['siteurl']."coinpaymentsnotify/index";
		
		$cps = new CoinPaymentsAPI();
                
		$cps->Setup($processordata['Processor']['api_password'], $processordata['Processor']['api_signature']);
		//print_r($withdrawdata);exit;
		foreach($withdrawdata as $withdrow)
		{
                        $btcamount=$cps->GetRates();
			$withdrawamount=($withdrow['Withdraw']['amount']*$btcamount['result']['USD']['rate_btc']);
			$result = $cps->CreateWithdrawal($withdrawamount,'BTC',$withdrow['Withdraw']['pro_acc_id'],1);
			$processorreturn[0][]='Fail';
			
			if ($result['error'] == 'ok')
			{
                                $cp_amount = round($withdrow['Withdraw']['amount'],8);
                                $cp_address = $withdrow['Withdraw']['pro_acc_id'];
				$cp_txn_id = addslashes($result['result']['id']);
                                if($result['result']['status']==0)
                                {
                                    $cp_status = "Pendingipn";
                                }
                                elseif($result['result']['status']==1)
                                {
                                    $cp_status = "Done";
                                }
                                else
                                {
                                    $cp_status = "Pendingipn";
                                }
				
				$comment='Amount : '.$cp_amount.', Payment Address : '.$cp_address.', Transaction Id : '.$cp_txn_id.', Added Date : '.date("Y-m-d H:i:s");
				$processorreturn[4][0][] = array("Withdraw.comment" => "'".$comment."'","Withdraw.status" => "'".$cp_status."'");
				$processorreturn[4][1][] = $withdrow['Withdraw']['with_id'];
                                
                                $processorreturn[2][] = array('id' => NULL, 'batch_number' => $result['error'], 'return_code' => 0, 'return_desc' => $cp_status, 'refrence_no' => $cp_txn_id, 'dt' => date('Y-m-d H:i:s'), 'amount' => $withdrow['Withdraw']['amount'], 'sender_email' => 'Admin', 'receiver_email' => $withdrow['Withdraw']['pro_acc_id'], 'receiver_id' => $withdrow['Withdraw']['member_id'], 'with_id' => $withdrow['Withdraw']['with_id'], 'processor' => 'CoinPayments', 'processor_id' => $withdrow['Withdraw']['processorid']);
				
				$totwith++;
				$totselamt=$totselamt+$withdrow['Withdraw']['amount'];
				$processorreturn[1].="<div><span>Payment Address : </span><span>".$cp_address."</span></div>
                                <div><span>Payment Status : </span><span>".$cp_status."</span></div>
				<div><span>Account : </span><span>".$withdrow['Withdraw']['pro_acc_id']."</span></div>
				<div><span>Amount : </span><span>".$withdrow['Withdraw']['amount']."</span></div>";
			}
			else
			{
				$processorreturn[2][] = array('id' => NULL, 'batch_number' => $result['error'], 'return_code' => 0, 'return_desc' => 'Fail', 'refrence_no' => $result['error'], 'dt' => date('Y-m-d H:i:s'), 'amount' => $withdrow['Withdraw']['amount'], 'sender_email' => 'Admin', 'receiver_email' => $withdrow['Withdraw']['pro_acc_id'], 'receiver_id' => $withdrow['Withdraw']['member_id'], 'with_id' => $withdrow['Withdraw']['with_id'], 'processor' => 'CoinPayments', 'processor_id' => $withdrow['Withdraw']['processorid']);
				$totwith++;
				$totselamt=$totselamt+$withdrow['Withdraw']['amount'];
				$processorreturn[1].="<div><span>Payment Status : </span><span>".$cp_status."</span></div>
				<div><span>Account : </span><span>".$withdrow['Withdraw']['pro_acc_id']."</span></div>
				<div><span>Amount : </span><span>".$withdrow['Withdraw']['amount']."</span></div>";
			}
		}
		
		if(count($withdrawdata)>0)
		{
			$processorreturn[1].='</div><div class="Masspay-Message-Total">Total Members : '.$totwith.'</div>
			<div class="Masspay-Message-Total">Total Amount : $'.$totselamt.'</div>';
		}
		return $processorreturn;
	}
	function getextrafield($processordata='')
	{
		$processorreturn='';
		return $processorreturn;
	}
	function getExtraFieldWithdrawal($processordata='')
	{
		
		$processorreturn='';
		return $processorreturn;
	}
	function extraFieldWithdrawaldata($processordata='')
	{
		
		$processorreturn='';
		return $processorreturn;
	
	}
	function paymentextrafield($depositedata='')
	{
		$processorreturn='';
		return $processorreturn;
	}
	function pendingpaymentdata($processordata='',$depositdata='',$controller='')
	{
		$processorreturn='';
		return $processorreturn;
	}
	function GetBalance($processordata='')
	{
		$cps = new CoinPaymentsAPI();
		$cps->Setup($processordata['Processor']['api_password'], $processordata['Processor']['api_signature']);
		
		$result = $cps->GetBalances(1);
		if ($result['error'] == 'ok')
		{
			echo $result['result']['BTC']['balancef']." BTC";
		}
		else
		{
			print_r($result['error']);
		}
	}

}
?>