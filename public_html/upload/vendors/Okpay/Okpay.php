<?php
class Okpay
{
	function getFormAddProcesser($processordata,$SITEURL='')
	{
		$processorreturn=array();
		$processorreturn[0] ='<div class="fromnewtext">Processor Name : Okpay</div>
				<div class="fromnewtext">Routine URL :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_url]" id="SitesettingProcUrl" class="fromboxbg" value="'.$processordata["Processor"]["proc_url"].'" />
				</div>
				<div class="fromnewtext">Wallet ID :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_accid]" id="SitesettingProcAccid" class="fromboxbg" value="'.$processordata["Processor"]["proc_accid"].'" />
				</div>
				<div class="fromnewtext">Maximum Request Amount Per Day ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][max_req_amt]" id="SitesettingMaxReqAmt" class="fromboxbg" value="'.$processordata["Processor"]["max_req_amt"].'" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Add Fund Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee_amount"].'" name="data[Sitesetting][proc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Add Fund Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee"].'" name="data[Sitesetting][proc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[1]='<div class="fromnewtext">API Access Password :</div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_password\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_password]" style="display:none;" id="api_password" class="fromboxbg" value="" /></td>
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Withdrawal Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee_amount"].'" name="data[Sitesetting][mproc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Withdrawal Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["mproc_fee"].'" name="data[Sitesetting][mproc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>';
		$processorreturn[2]='';
		return $processorreturn;
	}
	
	function getFormAddfund($processordata,$data)
	{
		$processorreturn=array();
		$processorreturn['message'] ='';
		
		echo '<form action="'.$processordata['Processor']['proc_url'].'" method="post" id="service">';		
		echo '<input type="hidden" name="ok_receiver" value="'.$processordata['Processor']['proc_accid'].'" />';
		echo '<input type="hidden" name="ok_item_1_price" value="'.round($data['total'],2).'" />';
		echo '<input type="hidden" name="ok_invoice" value="'.$data['userid'].'-'.$data['lastid'].'" />';
		echo '<input type="hidden" name="ok_currency" value="USD" />';
		//echo '<input type="hidden" name="ok_ipn_test" value="1" />';
		echo '<input type="hidden" name="ok_return_success" value="'.$data['siteurl'].'thankyou/addfund/done'.'" />';
		echo '<input type="hidden" name="ok_return_fail" value="'.$data['siteurl'].'thankyou/addfund/cancel'.'" />';
		echo '<input type="hidden" name="ok_ipn" value="'.$data['siteurl'].'okpaynotify/index'.'" />';
		echo '<input type="hidden" name="ok_item_1_custom_1_title" value="'.$data['callmethod'].'" />';
		echo '<input type="hidden" name="ok_item_1_name" value="'.$data['type'].'" />';
		echo '<input type="hidden" name="ok_item_1_custom_1_value" value="'.$_SERVER['REMOTE_ADDR'].'" />';
		echo '<input type="hidden" name="ok_item_1_custom_2_title" value="'.$data['description'].'" />';
		echo '<input type="hidden" name="ok_item_1_custom_2_value" value="'.$data['type'].'" />';
		echo '</form>';
	
		echo '<script language="javascript">document.getElementById("service").submit();</script>';
		
		return $processorreturn;
	}
	
	function getdatawithdraw($processordata='',$withdrawdata='')
	{
		$processorreturn=array();
		
		$okpay_password = $processordata['Processor']['api_password'];
		$okpay_walletid = $processordata['Processor']['proc_accid'];
		$totwith=0;
		$totselamt=0;
		if(count($withdrawdata)>0)
		{
			$processorreturn[1]='<div class="Masspay-Message-Title">Okpay MassPay Message</div><div class="Masspay-Message-Description">Description : </div><div class="Masspay-Message-Error">';
		}
		foreach($withdrawdata as $withdrow)
		{	
			$totamt=$totamt+$withdrow['Withdraw']['amount'];
			$receiver=$withdrow['Withdraw']['pro_acc_id'];//send to
			$amount=$withdrow['Withdraw']['amount'];
			$paymentID  = $withdrow['Withdraw']['with_id']; //set with_id if you want to check duplicate payment for same with_id.
			
			$comments = $withdrow['Withdraw']['member_id']."|".$withdrow['Withdraw']['with_id']."|".$withdrow['Withdraw']['fee'];
			
			try
			{
				$datePart = gmdate("Ymd");
				$timePart = gmdate("H");
				$authString = $okpay_password.":".$datePart.":".$timePart;
				$sha256 = bin2hex(hash('sha256', $authString));
				$secToken = strtoupper($sha256);
				
				$client = new SoapClient("https://api.okpay.com/OkPayAPI?wsdl");
				$obj->WalletID = $okpay_walletid;
				$obj->SecurityToken = $secToken;
				$obj->Currency = "USD";
				$obj->Receiver = $receiver;
				$obj->Amount = round($amount,2);
				$obj->Comment = $comments;
				$obj->IsReceiverPaysFees = FALSE;
				$obj->Invoice = $paymentID;
				$webService1 = $client->Send_Money($obj);
				$wsResult1 = $webService1->Send_MoneyResult;
				//print_r($wsResult1);
				
				if($wsResult1->Status=="Completed")
				{	
					$processorreturn[0][]='Done';
					
					$processorreturn[2][] = array('id' => NULL, 'batch_number' => $wsResult1->ID, 'return_code' => 0, 'return_desc' => 'Completed', 'refrence_no' => $wsResult1->ID, 'dt' => date('Y-m-d H:i:s'), 'amount' => $withdrow['Withdraw']['amount'], 'sender_email' => 'Admin', 'receiver_email' => $wsResult1->Receiver->Email, 'receiver_id' => $withdrow['Withdraw']['member_id'], 'with_id' => $withdrow['Withdraw']['with_id'], 'processor' => 'Okpay', 'processor_id' => $withdrow['Withdraw']['processorid']);
					$totwith++;
					$totselamt=$totselamt+$withdrow['Withdraw']['amount'];
					$processorreturn[1].='<div><span>Payment Status : </span><span>'.$wsResult1->Status.'</span></div>
					<div><span>Account : </span><span>'.$wsResult1->Sender->WalletID.'</span></div>
					<div><span>Amount : </span><span>'.$withdrow['Withdraw']['amount'].'</span></div>';
				
				}
				else
				{
					$processorreturn[0][]='Fail';
					$processorreturn[2][] = array('id' => NULL, 'batch_number' => $wsResult1->ID, 'return_code' => 0, 'return_desc' => $wsResult1->Status, 'refrence_no' => $wsResult1->ID, 'dt' => date('Y-m-d H:i:s'), 'amount' => $withdrow['Withdraw']['amount'], 'sender_email' => 'Admin', 'receiver_email' =>  $wsResult1->Receiver->Email, 'receiver_id' => $withdrow['Withdraw']['member_id'], 'with_id' => $withdrow['Withdraw']['with_id'], 'processor' => 'Okpay', 'processor_id' => $withdrow['Withdraw']['processorid']);
					
					$processorreturn[1].="<div><span>Email : </span><span>".$withdrow['Withdraw']['pro_acc_id']."</span></div>
					<div><span>Error Message : </span><span>".$wsResult1->Status."</span></div>";
				}
			}
			catch (Exception $e)
			{
				$msg = $e->getMessage();
				$processorreturn[0][]='Fail';
				$processorreturn[2][] = array('id' => NULL, 'batch_number' => '-', 'return_code' => 0, 'return_desc' => trim($msg), 'refrence_no' => '-', 'dt' => date('Y-m-d H:i:s'), 'amount' => $withdrow['Withdraw']['amount'], 'sender_email' => 'Admin', 'receiver_email' =>  '-', 'receiver_id' => $withdrow['Withdraw']['member_id'], 'with_id' => $withdrow['Withdraw']['with_id'], 'processor' => 'Okpay', 'processor_id' => $withdrow['Withdraw']['processorid']);
				$processorreturn[1].='<div>'.trim($msg).'</div>';
			}
		}
		if(count($withdrawdata)>0)
		{
			$processorreturn[1].='</div><div class="Masspay-Message-Total">Total Members : '.$totwith.'</div>
			<div class="Masspay-Message-Total">Total Amount : $'.$totselamt.'</div>';
		}
		return $processorreturn;
	}
	function GetBalance($processordata='')
	{
		try
		{
			$secWord  = stripslashes($processordata['Processor']['api_password']); // wallet API password
			$WalletID = stripslashes($processordata['Processor']['proc_accid']); // wallet ID
	   
			$datePart = gmdate("Ymd:H");
			$authString = $secWord.":".$datePart;
		
			$secToken = hash('sha256', $authString);
			$secToken = strtoupper($secToken);
		
			$client = new SoapClient("https://api.okpay.com/OkPayAPI?wsdl");
			
			$obj->WalletID = $WalletID;
			$obj->SecurityToken = $secToken;
			$obj->Currency = "USD";
		
			$webService1 = $client->Wallet_Get_Currency_Balance($obj);
			$wsResult1 = $webService1->Wallet_Get_Currency_BalanceResult;
	   
			return $wsResult1->Amount." ".$wsResult1->Currency;
	   
		}
		catch (Exception $e)
		{
			
			return $e->getMessage();
		}
	}

}
?>