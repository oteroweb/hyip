<?php
class Pay4you
{
	function getFormAddProcesser($processordata,$SITEURL='')
	{
		$processorreturn=array();
		$processorreturn[0] ='<div class="fromnewtext">Processor Name : 2Pay4You</div>
				<div class="fromnewtext">Routine URL :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][proc_url]" id="SitesettingProcUrl" class="fromboxbg" value="'.$processordata["Processor"]["proc_url"].'" />
				</div>
				<div class="fromnewtext">Sci Password :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_name\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_name]" style="display:none;" id="api_name" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Public Key :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'api_password\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][api_password]" style="display:none;" id="api_password" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Payment Key :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<button class="btngray" type="button" style="margin:2px 0 1px 9px;" onclick="show_textbox(\'lr_api\',this)" value="Change" >Change</button>
					<input type="password" name="data[Sitesetting][lr_api]" style="display:none;" id="lr_api" class="fromboxbg" value="" />
				</div>
				<div class="fromnewtext">Maximum Request Amount Per Day ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" name="data[Sitesetting][max_req_amt]" id="SitesettingMaxReqAmt" class="fromboxbg" value="'.$processordata["Processor"]["max_req_amt"].'" />
				</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Fixed Add Fund Fees ($) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee_amount"].'" name="data[Sitesetting][proc_fee_amount]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="plussign">+</div>
				<div class="formborderfinance">
				<div class="fromnewtext">Variable Add Fund Fees (%) :<span class="red-color">*</span></div>
				<div class="fromborderdropedown3">
					<input type="text" value="'.$processordata["Processor"]["proc_fee"].'" name="data[Sitesetting][proc_fee]" id="SitesettingProcFee" class="fromboxbg" />
				</div>
				</div>
				<div class="fromnewtext">Test Mode :</div>
				<div class="fromborderdropedown3">
				<div class="select-main">
				    <label>
					<select id="SitesettingTestmode" class="" style="" name="data[Sitesetting][testmode]">
						<option value="0">No</option>
						<option '; if($processordata["Processor"]["testmode"]==1){ $processorreturn[0].='selected="selected"';
						} $processorreturn[0].='value="1">Yes</option>
					</select>
					</label>
					</div>
				</div>';
		$processorreturn[1]='';
		$processorreturn[2]='';
		return $processorreturn;
	}
	
	function getFormAddfund($processordata,$data)
	{
		$processorreturn=array();
		$processorreturn['message']='';
		echo '<form method="post" action="'.$processordata['Processor']['proc_url'].'" id="service">'; 
		echo	 '<input type="hidden" value="'.$processordata['Processor']['api_password'].'" name="public_key">';
		echo	 '<input type="hidden" value="'.$processordata['Processor']['lr_api'].'" name="payment_key">';
		echo     '<input type="hidden" value="'.round($data['total'],2).'" name="amount">';
		echo     '<input type="hidden" value="USD" name="currency">';
		echo     '<input type="hidden" value="'.$data['type'].'" name="product">';
		echo     '<input type="hidden" value="'.$data['userid'].'" name="user1">';
		echo     '<input type="hidden" value="'.$_SERVER['REMOTE_ADDR'].'" name="user2">';
		echo 	 '<input type="hidden" value="'.$data['lastid'].'" name="user3">';
		echo 	 '<input type="hidden" value="'.$data['callmethod'].'" name="user4">';
		echo     '<input type="hidden" value="'.$data['siteurl'].'pay4younotify/index'.'" name="notify_url">';
		echo     '<input type="hidden" value="'.$data['siteurl'].'thankyou/addfund/done'.'" name="return_url">';
		echo     '<input type="hidden" value="'.$data['siteurl'].'thankyou/addfund/cancel'.'" name="cancel_url">';       	  if($processordata['Processor']['testmode']==1)
		{
			echo '<input type=hidden name="testmode" value="ON" />';
		}	
		echo '</form>';

		echo '<script language="javascript">document.getElementById("service").submit();</script>';
		return $processorreturn;
	}
	
	function getdatawithdraw($processordata='',$withdrawdata='')
	{
		$processorreturn=array();
		$processorreturn[0]='';
		return $processorreturn;
	}
	function GetBalance($processordata='')
	{
		return 0;
	}

}
?>