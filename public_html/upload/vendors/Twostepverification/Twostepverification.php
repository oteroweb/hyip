<?php
include_once("lib/GoogleAuthenticator.php");
class Twostepverification
{
	
	function getQRCodeData($email,$title)
	{
		$time = floor(time() / 30);
		$g = new GoogleAuthenticator();
		$secret = $g->generateSecret();
		
		$temp=explode("@",$email);
		return array('secret'=>$secret,'qr'=>$g->getURL($title.':'.$temp[0],$temp[1],$secret));
	}
	function getSecretKeyData()
	{
		$time = floor(time() / 30);
		$g = new GoogleAuthenticator();
		return $g->generateSecret();
	}
	function checkCode($secret,$code)
	{
		$g = new GoogleAuthenticator();
		if ($g->checkCode($secret,$code))
			return 1;
		else
			return 0;
	}
}
?>