<?php 
@session_start();
require_once( 'Facebook/autoload.php' );
class facebook
{
  function facebooklogin($appid,$secret,$baseurl,$islogin)
  {
  	$fb = new Facebook\Facebook([
	  'app_id' => $appid,
	  'app_secret' => $secret,
	  'default_graph_version' => 'v2.5',
	]);  
	
	$helper = $fb->getRedirectLoginHelper();  
	if($islogin==1)
	{	 

		try {  
		  $accessToken = $helper->getAccessToken();  
		} catch(Facebook\Exceptions\FacebookResponseException $e) {  
		  // When Graph returns an error  
		  
		  echo 'Graph returned an error: ' . $e->getMessage();  
		  exit;  
		} catch(Facebook\Exceptions\FacebookSDKException $e) {  
		  // When validation fails or other local issues  

		  echo 'Facebook SDK returned an error: ' . $e->getMessage();  
		  exit;  
		}  


		try {
		  // Get the Facebook\GraphNodes\GraphUser object for the current user.
		  // If you provided a 'default_access_token', the '{access-token}' is optional.
		  $response = $fb->get('/me?fields=id,name,email,first_name,last_name', $accessToken->getValue());
		//  print_r($response);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'ERROR: Graph ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'ERROR: validation fails ' . $e->getMessage();
		  exit;
		}
			$me = $response->getGraphUser();
			$fname = $me->getProperty('first_name'); // To Get Facebook full name
            $lname = $me->getProperty('last_name'); // To Get Facebook full name
            $email = $me->getProperty('email');    // To Get Facebook email ID
            $return=array('email'=>$email,'fname'=>$fname,'lname'=>$lname);
           	return $return;			
	}
	else
	{
		$permissions = ['email']; 
		$loginUrl = $helper->getLoginUrl($baseurl, $permissions);
		?> <script type="text/javascript">window.location="<?php echo $loginUrl; ?>";</script><?php
	}

    
  }
}
/*
require_once 'autoload.php';
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

class facebook
{
  function facebooklogin($appid,$secret,$baseurl)
  {
    FacebookSession::setDefaultApplication( $appid,$secret);
    // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper($baseurl);
    


        
    try {
      $user = $helper->getSessionFromRedirect();
    } catch( FacebookRequestException $ex ) {
      // When Facebook returns an error
    } catch( Exception $ex ) {
      // When validation fails or other local issues
    }
    
    if ( isset( $user ) )
    {
            // graph api request for user data
            $request = new FacebookRequest( $user, 'GET', '/me' );
            $response = $request->execute();
            // get response
            $graphObject = $response->getGraphObject();
            //$fbid = $graphObject->getProperty('id');              // To Get Facebook ID
            
            $fname = $graphObject->getProperty('first_name'); // To Get Facebook full name
            $lname = $graphObject->getProperty('last_name'); // To Get Facebook full name
            $email = $graphObject->getProperty('email');    // To Get Facebook email ID
            $return=array('email'=>$email,'fname'=>$fname,'lname'=>$lname);
           	print_r($return); exit;
            return $return;
    }
    else
    {
            $params = array('email');
            $loginUrl = $helper->getLoginUrl(array('email','manage_pages'));
            ?> <script type="text/javascript">window.location="<?php echo $loginUrl; ?>";</script><?php
            exit;
    }
  }
}


// init app with app id and secret
/*FacebookSession::setDefaultApplication( '514151995271048','270e409a358762bf971a2e6a75debfa4' );
// login helper with redirect_uri
    $helper = new FacebookRedirectLoginHelper('https://www.proxscripts.com/sdklogin.php?type=fb' );
try {
  $user = $helper->getSessionFromRedirect();
} catch( FacebookRequestException $ex ) {
  // When Facebook returns an error
} catch( Exception $ex ) {
  // When validation fails or other local issues
}*/
// see if we have a session
/*if ( isset( $user ) ) {
  // graph api request for user data
  $request = new FacebookRequest( $user, 'GET', '/me' );
  $response = $request->execute();
  // get response
  $graphObject = $response->getGraphObject();
     	$fbid = $graphObject->getProperty('id');              // To Get Facebook ID
 	    $fbfullname = $graphObject->getProperty('name'); // To Get Facebook full name
	    $femail = $graphObject->getProperty('email');    // To Get Facebook email ID
	/* ---- Session Variables -----*/
	//    $_SESSION['FBID'] = $fbid;           
        //$_SESSION['FULLNAME'] = $fbfullname;
	  //  $_SESSION['EMAIL'] =  $femail;
    /* ---- header location after session ----*/
 /* header("Location: index.php");
} else {
  $loginUrl = $helper->getLoginUrl();
 header("Location: ".$loginUrl);
}*/
?>