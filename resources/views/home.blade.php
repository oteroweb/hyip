@extends('layouts.app')

@section('htmlheader_title')
@section('contentheader_title')
  <div style="font-size: 24px; background-color: green; padding: 10px; color: white;
    font-weight: 400px; border-radius: 3px;" >Dashboard</div>
@endsection

@section('main-content')

<div class="row">
		{{-- my-balance --}}
		<div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>150</h3>

              <p>My Balance</p>
            </div>
            <div class="icon">
              <i class="fa fa-dollar"></i>
            </div>
            <a href="#" class="small-box-footer">Add Balance <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		{{-- my-balance --}}

		{{-- my-invest --}}
		<div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>

              <p>My Invest</p>
            </div>
            <div class="icon">
              <i class="fa fa-line-chart"></i>
            </div>
            <a href="#" class="small-box-footer">Invest Balance <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		{{-- my-invest --}}
		{{-- withdrawals --}}
		<div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>150</h3>

              <p>Withdrawals</p>
            </div>
            <div class="icon">
              <i class="fa fa-bank"></i>
            </div>
            <a href="#" class="small-box-footer">Withdrawals <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		{{-- withdrawals --}}
    {{-- withdrawals --}}
    <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>150</h3>

              <p>Withdrawals</p>
            </div>
            <div class="icon">
              <i class="fa fa-paper-plane"></i>
            </div>
            <a href="#" class="small-box-footer">Withdrawals <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
    {{-- withdrawals --}}
</div>
@endsection