@extends('layouts.app')

@section('htmlheader_title')
	Deposit Funds
@endsection


@section('main-content')
	{!! Form::open(array('url' => $parameters_method['cons_form']['action'], 'method' =>  $parameters_method['cons_form']['method'])) !!}
	<div class="container spark-screen">

				<img class="img-responsive" src="{{ asset('/img/'.$parameters_method['cons_form']['img']) }}" alt="">


@foreach ($parameters_method['var_form'] as $key => $value)

<input type="hidden" name="{{ $key }}" value="{{ $value }}"></input>
@endforeach

  


		<div class="row">
			<div class="col-md-10">

      		</div>
			</div>
			<div class="row">	
			<div class="col-md-10">

				<div class="input-group input-group-lg">
	            <input type="text" name="{{$parameters_method['cons_form']['qtyfield']}}" class="form-control"><span class="input-group-btn">
	            <input value="deposit" type="submit" class="btn btn-info btn-flat"></span>
	        	</div>
			</div>
			</div>

	</div>
	{!! Form::close() !!}

@endsection

{{-- "cmd" => "_pay", "reset" => "1", "want_shipping" => "0","merchant" => "e6d0babd431a64d0277447d2cf00f1c4","first_name" => "first_name","last_name" => "last_name","email" => "email@email.com","currency" => "USD",    "amountf" => "5.00",     "item_name" => "Test Item",             "allow_extra" => "1",     "success_url" => "https://www.coinpayments.net/index.php?cmd=acct_home",    "cancel_url" => "https://www.coinpayments.net/merchant-tools", --}}