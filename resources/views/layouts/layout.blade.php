<!DOCTYPE html>
<html lang="en">
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show
<body class="skin-blue sidebar-mini">
<div class="wrapper">
    @include('layouts.partials.mainheader')
    @include('layouts.partials.sidebar')
    <div class="content-wrapper">
        @include('layouts.partials.contentheader')
        <section class="content">
            @yield('main-content')
        @section('caso1')
        @show
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    @include('layouts.partials.controlsidebar')
    @include('layouts.partials.footer')
</div><!-- ./wrapper -->
@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
