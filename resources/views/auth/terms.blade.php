<!-- Terms and conditions modal -->
<div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="Terms and conditions" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Terms and conditions</h3>
            </div>

            <div class="modal-body">
                <p>
1. Terms of Use

 When registering at GenADShare.com, you confirm that you have read, understood and accepted the following terms and conditions explained below. If you do not agree to any of these terms, you must not register.

 

2. REFUND POLICY

 Since the Advertising Credits that you are purchasing are instantly added to your account, no refund will be provided. Also, referral commissions and profit sharing are instantly and automatically done. This process cannot be reversed. Therefore, NO REFUND will be provided.

 

3. USER ACCOUNT

 3.1. You are allowed to create only 1 account. Any attempt to create more than one account will result to the termination of all of them. 
3.2. We will only modify your user account information if you request so.

 

4. LIABILITY

 4.1. GenADShare will not be liable for any kind of delays or failures that are not directly related to GenADShare and therefore beyond our control. 
4.2. GenADShare reserve the right to alter the Terms of Service at any time, including fees, special offers, rules and also reserves the right to cancel its services any time and without any notice. 
4.3. GenADShare not guarantees a daily earnings for the ad share pack purchases , as you are purchasing advertisement credits, earnings are increased depending on our portfolio products growth and other members purchases.

 

5. REFERRALS

5.1. You may refer as many people as you want. 
5.2. Every one of your referrals, as users, must have a unique email address. 
5.3. A referral will never be able to modify the member who referred him/her.

 

6. PAYMENTS TO USERS

6.1. All payments will be made in 48 hours upon request. 
6.2. You need to have atleast $5 in your main balance to be able to request payment. From the amount paid, a fee can be deducted depending on the payment processor you use.
6.3. You can only request max $100 payment per day. 
6.4. You need to buy atleast 1 adpack to be able to recive payments. 
6.5. We are responsible only for submitting your payment to the payment processor. Any action after that is to be handled by payment processor's support. 
6.6. You must have a correct and existing Payeer, PayZa, SolitTrustPay, Neteller or BTC username or email address. All payments will go directly to that user's email address and cannot be canceled. We ask you to check the email address from your account so no issues regarding payments will happen.
6.7. We Do not guarantee and payments to all members we are revenue sharing company that share revenue so if there no revenue there will be no payments to be made.

 

7. ANTI-CHEAT POLICY

7.1. Each attempt, in any way, to hack into the system will be logged. 
7.2. When you will request payout, our anti-cheat system will analyze your actions and you will be detected if any attempts of hacking happened. In this case your account will be suspended. 
7.3. We will always provide a reason why you have been banned by sending you a email regarding the situation.</p>
                
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>