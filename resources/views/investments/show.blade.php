@extends('layouts.app')

@section('content')
    @include('investments.show_fields')

    <div class="form-group">
           <a href="{!! route('investments.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection
