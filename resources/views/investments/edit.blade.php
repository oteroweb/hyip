@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit Investment</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($investment, ['route' => ['investments.update', $investment->id], 'method' => 'patch']) !!}

            @include('investments.fields')

            {!! Form::close() !!}
        </div>
@endsection
