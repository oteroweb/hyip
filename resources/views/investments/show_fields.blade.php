<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $investment->id !!}</p>
</div>

<!-- Investment Field -->
<div class="form-group">
    {!! Form::label('investment', 'Investment:') !!}
    <p>{!! $investment->investment !!}</p>
</div>

<!-- Final Investment Field -->
<div class="form-group">
    {!! Form::label('final_investment', 'Final Investment:') !!}
    <p>{!! $investment->final_investment !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $investment->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $investment->updated_at !!}</p>
</div>

