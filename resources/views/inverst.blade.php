@extends('layouts.app')

@section('htmlheader_title')
	Inverst Funds
@endsection
@section('contentheader_title')
	Inverst Funds
@endsection
@section('contentheader_description')
	{{-- Add your funds for get  --}}
@endsection

@section('main-content')
{!! Form::open([
    'route	' => 'InverstMake'
]) !!}



{{-- {!! Form::token() !!} --}}
<div class="form-group">
    {!! Form::number('inverst', null, ['step'=>'0.01','class' => 'form-control', 'placeholder'=>'Enter your amount']) !!}
</div>
{!! Form::submit('Inverst', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
<table class="table table-bordered" id="investment-table">
        <thead>
            <tr>
                <th>Invest</th>
                <th>Return</th>
                <th>Date Created</th>
                <th>Finish Date</th>
                <th>Status</th>
            </tr>
        </thead>
    </table>
@endsection
@push('scripts')
<script>
$('#investment-table').DataTable({
	processing: true,
	serverSide: true,
	ajax: '{!! url('datatables/data') !!}',
	// funcion para poner borde a la filas dependiendo del status
 	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            if ( aData[0] == "1" )
            { $('td', nRow).css('background-color', 'LightBlue'); }
            else if ( aData[0] == "0" )
            { $('td', nRow).css('background-color', 'Yellow'); }
            
        },
	// para modificar el valor dependiendo del valor 
  	"columnDefs": [ {
        "targets": 4,
        // "data": 0, // STATUS 1 or 0
		"render": function ( data, type, row ) {
		    if(row[4] == 1){ return '<label class=\' btn btn-primary\'>Active</label>'; }
		    else{ return '<label class=\'btn btn-warning\'>Inactive</label>';}
		},
    },
    {
        "targets": [2,3],
		"render": function ( data, type, row ) {
            var date = new Date(data*1000);
            var day = date.getDate();
            var month = date.getUTCMonth()+1;
            var month = ("0" + (date.getMonth() + 1)).slice(-2);
            var  year = date.getFullYear();
            var hour = date.getHours();
            var minutes = ("0" + (date.getMinutes() + 1)).slice(-2);
            datefull = day +'/'+ month +'/'+ year + ' ' +hour +':'+  minutes;
            return datefull;
		},
    },
     ],
});
</script>
@endpush