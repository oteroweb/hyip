@extends('layouts.app')

@section('htmlheader_title')
@endsection

@section('contentheader_title')
    withdrawals Funds
@endsection

@section('main-content')

{!! Form::open([
    'route	' => 'InverstMake'
]) !!}

{!! Form::token() !!}
<div class="form-group">
    {!! Form::number('withdrawals', null, ['step'=>'0.01','class' => 'form-control', 'placeholder'=>'Enter your amount']) !!}
</div>
{!! Form::submit('withdrawals', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
@endsection
