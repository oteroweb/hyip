@extends('layouts.app')

@section('htmlheader_title')
	Deposit Funds
@endsection


@section('main-content')
	{!! Form::open(array('url' => 'pay')) !!}
	<div class="container spark-screen">


		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
                  <div class="col-md-4 col-sm-4 col-xs-4">
                  	<div class="radio">
                    <label>
                      <input type="radio" name="processor" id="processor1" value="payeer" checked="">
						<img class="img-responsive" src="{{ asset('/img/payeer.png') }}" alt="">
                    </label>
                  </div>
                  </div> {{-- col-md-4 col-sm-4 col-xs-4 --}}
                  <div class="col-md-4 col-sm-4 col-xs-4">
                  	
                  <div class="radio">
                    <label>
                      <input type="radio" name="processor" id="processor2" value="pm"> 
						<img class="img-responsive" src="{{ asset('/img/PM.png') }}" alt="">
                      </label>
                  </div>
                  </div>  {{-- col-md-4 col-sm-4 col-xs-4 --}}
                  <div class="col-md-4 col-sm-4 col-xs-4">
                  	
	                  <div class="radio">
	                    <label>
	                      <input type="radio" name="processor" id="processor3" value="bit"> 
							<img class="img-responsive" src="{{ asset('/img/bitcoin.png') }}" alt="">
	                  </div>
                 	</div> {{-- col-md-4 col-sm-4 col-xs-4 --}}
                 
                </div>

      		</div>
			</div>
			<div class="row">	
			<div class="col-md-10">
				
				<div class="input-group input-group-lg">
	            <input value="deposit" type="submit" class="btn btn-info btn-flat"></span>
	        	</div>
			</div>
			</div>

	</div>
	{!! Form::close() !!}

@endsection
