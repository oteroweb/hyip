<?php

namespace App\Repositories;

use App\investment;
use InfyOm\Generator\Common\BaseRepository;

class investmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'investment',
        'final_investment'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return investment::class;
    }
}
