<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
class incrementearn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'earn:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    // DB::table('users')->where('id', '=', $id)->decrement('balance', 1);
    DB::table('users')->decrement('balance', 1);


    }
}
