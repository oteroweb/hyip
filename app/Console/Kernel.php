<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\incrementearn::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

          $schedule->call(function () {
        // DB::transaction(function () use($id, &$myinverst) {
        DB::transaction(function ()  {
//         // DB::table('users')->where('id', '=', $id)->decrement('balance', $myinverst);

        });
        })->hourly();

// $schedule->command('emails:send --force')->daily();
$schedule->command('earn:generate')->everyMinute();

//         $schedule->command('inspire')
//                  ->hourly();
    }
}
