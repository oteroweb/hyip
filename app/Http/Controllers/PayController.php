<?php

namespace App\Http\Controllers;
	use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\Validator;



class PayController extends Controller
{
	   //  public function __construct()
    // {
    //     // $this->middleware('auth');
    // }


    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function SelectPayment(Request $request)
    {
    	$email = Auth::user()->email;
    	$name = Auth::user()->name;
    $verificationalidator = Validator::make($request->all(), [
    //     'processor' => 'required',
    ]);
    	$method =   $request->processor;
   		if ($method == 'pm') {
        $parameters_method = array (
            'cons_form' => array ( 'action'=>"https://perfectmoney.is/api/step1.asp", 'method'=> "POST", "qtyfield"=>"PAYMENT_AMOUNT", 'img' => "PM.png"),
            'var_form' => 
                array( 'PAYMENT_ID' => time(), "PAYEE_ACCOUNT" => "U12389590", "PAYEE_NAME" => "Test account", "PAYMENT_UNITS" => "USD", "STATUS_URL" => "http://dailybitspro.com/statuspm", "PAYMENT_URL" => "http://dailybitspro.com/completepm", "PAYMENT_URL_METHOD" => "POST", "NOPAYMENT_URL" => "http://dailybitspro.com/failedpm", "NOPAYMENT_URL_METHOD" => "POST", "SUGGESTED_MEMO" => "", "BAGGAGE_FIELDS" => "IDENT",
                    ),
                        );
                    } 
    	elseif($method == 'payeer') {
        $m_shop = '214282263';
        $m_orderid =rand(1, 100000);
        $m_curr = 'USD';
        $m_amount = number_format(6, 2, '.', '');
        $m_desc = base64_encode('Test');
        $m_key = 'XBGAZuq9k6MWcCmV';
        $arHash = array( $m_shop, $m_orderid,
        $m_amount, $m_curr, $m_desc, $m_key );
        $sign = strtoupper(hash('sha256', implode(':', $arHash)));

        // $m_amount = number_format(6, 2, '.', '');
        $parameters_method = array (
            'cons_form' => array ( 'action'=>"https://payeer.com/merchant/", 'method'=> "GET", "qtyfield"=>"m_amount", 'img' => "payeer.png"),
            'var_form' => array ( 'm_shop' => $m_shop, 'm_orderid' => $m_orderid, 'm_curr' => $m_curr, 'm_desc' => $m_desc, 'm_key' => $m_key, 'm_sign' => $sign, ),
        );
    	} 
    	elseif($method == 'bit'){
        $parameters_method = array (
            'cons_form' => array ( 'action'=>"https://www.coinpayments.net/index.php", 'method'=> "post", "qtyfield"=>"amountf", 'img' => "bitcoin.png"),

        'var_form' => 
            array( 
         'public_key'=> '4c1476fb84083fa53a537e2f0839778a027f5a1433dfc896d8b1e2f15b7ccae6', 'private_ket'=>'bc7dC861d45dF90e315Ec3D8C07b9d0A8b33a2d202734947bBdcB4ED895dD812', 'cmd' => '_pay', 'reset' => '1', "want_shipping" => "0", "merchant" => "e6d0babd431a64d0277447d2cf00f1c4", "currency" => "USD",
        "item_name" => "Test Item", "allow_extra" => "1", "success_url" => "https://www.coinpayments.net/index.php?cmd=acct_home" , 
        "cancel_url" => "https://www.coinpayments.net/merchant-tools",
        "first_name" => $name,
        "last_name" => $name,
        "email" => $email,
                    ),
        );
    	}
    return view('pay',['pay'=>$method, 'parameters_method' => $parameters_method]); 
	}
}
