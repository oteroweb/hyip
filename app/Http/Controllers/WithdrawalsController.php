<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use App\User;

use App\investment;

use DB;


class WithdrawalsController extends Controller
{
	    public function __construct()
    {
        $this->middleware('verify.user');
    }
    public function Index(Request $request)
    {
    	return view('withdrawals');
    }
      public function MakeWithdrawals(Request $request)
    { 
    	$withdrawals = $request->all(); 
    	$v = Validator::make($withdrawals, [ 'withdrawals' => 'checkfunds|required|numeric'], [ 'checkfunds' => 'Insufficient funds']    		);
        if ($v->fails()) { return redirect('withdrawals')->withErrors($v)->withInput();}
 		$id = intval(Auth::user()->id);
		$user = User::find($id);
		$mywithdrawals= doubleval($withdrawals['withdrawals']);
		$mybalance= doubleval(Auth::user()->balance);
		$source = "bit";
		// $myinverstbalance= doubleval(Auth::user()->repurchase);

    	DB::transaction(function ($id) use($id, &$mywithdrawals, &$source) {
    		DB::table('users')->where('id', '=', $id)->decrement('balance', $mywithdrawals);
		    DB::insert('insert into withdrawals (`withdrawals`,`id_user`,`source`) values (?,?,?)', [$mywithdrawals,$id,$source]);
		});
    		$message =  ' '.Auth::user()->name.'&nbsp;withdrawals made successfully for '.$mywithdrawals.' $';
    	return redirect::back()->with('status',$message);
    }
}
