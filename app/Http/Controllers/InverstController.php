<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use	Auth;
use App\User;
use App\Role;
use App\investment;
use DB;
use Yajra\Datatables\Datatables;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;


class InverstController extends Controller
{
  public function Index(Request $request)
    {
// $user = User::where('name', '=', 'oterolopez')->first();
// $admin = Role::findOrFail(1);
// role attach alias
// $user->attachRole($admin); // parameter can be an Role object, array, or id

// or eloquent's original technique
// $user->roles()->attach($admin->id); 

                                        //     	date_default_timezone_set("America/New_York"); 

                                        // $dftz021 = date_default_timezone_get(); 

// echo '<br><b>' . $dftz021 . '</b><br><br>'; 

                                        // $dtms021 = date_create(); 
                                        // $dtms022 = date_create(); 
                                        // $now = date_create();
                                        //     	$last = date_create('+24 hours');

// date_timestamp_set($now, 1473525613); 
// date_timestamp_set($last, 1473548400); 

// echo date_format($now, 'B => (U) => T Y-M-d H:i:s'); 
// echo "<br>";
// echo date_format($last, 'B => (U) => T Y-M-d H:i:s'); 
// echo "<br>";
// echo date_format($last, 'Y-m-d H:i:s'); 
                            //  $datetime1 = date_format($now, 'Y-M-d H:i:s'); 
                            //  $datetime2 = date_format($last, 'Y-M-d H:i:s'); 
                            //  $datett =date_create($datetime1);
                            //  $datetn =date_create($datetime2);
                            // echo $datetime1;




// $date1 = new DateTime("now");

// $date2 = new DateTime("tomorrow");

// var_dump($date1 == $date2);
// var_dump($date1 < $date2);
// var_dump($date1 > $date2);

                                 // $interval = date_diff($datett, $datetn);
                                	// 	var_dump($interval);
    	
    	// $fecha = date_create();
		// echo date_timestamp_get($fecha);
		// echo date_timestamp_set($fecha);
		// $dtms021 = date_create(); 

		// $dtms02122 = date_timestamp_set($dtms021, 1473525613); 
		// echo $dtms02122; 

		// $datetime1 = date_timestamp_set($fecha, 1473525613);
// (1473525613);
// $datetime2 = date_create(1473548400);
		// $datetime2 = date_timestamp_set($fecha, 1473525613);

// $interval = date_diff($datetime1, $datetime2);
		// echo $interval;
    	return view('inverst');
    }
     public function MakeInverst(Request $request)
    { 
    	$inverst = $request->all(); 
    	$v = Validator::make($inverst, [ 'inverst' => 'checkfunds|required|numeric'], [ 'checkfunds' => 'Insufficient funds']
    		);
        if ($v->fails()) { return redirect('inverst')->withErrors($v)->withInput();}
 		$id = intval(Auth::user()->id);
		$user = User::find($id);
		$myinverst= doubleval($inverst['inverst']);
		$mybalance= doubleval(Auth::user()->balance);
		$myinverstbalance= doubleval(Auth::user()->repurchase);

    	DB::transaction(function ($id) use($id, &$myinverst) {
    	$now = date_create();
    	$last = date_create('+24 hours');
    	$lastt = date_timestamp_get($last);
        $nowd = date_timestamp_get($now);
        $diference = $lastt - $nowd;  
    	 $status = 1;
        $earning = 1.5;
        $interval = 3600;
        $ciclos = $diference/$interval;
        $earnins_interval = ($myinverst*$earning)/$ciclos;
    		DB::table('users')->where('id', '=', $id)->decrement('balance', $myinverst);
		    DB::insert('insert into investment (`investment`, `user_id`,`final_investment`,`created_att`,`finish_att`,`time_elapsed`,`earnins_interval`,`interval`,`loop`,`status`) values (?, ?,?,?,?,?,?,?,?,?)', [$myinverst, $id, $myinverst*$earning,$nowd,$lastt,$nowd,$earnins_interval,$interval,$ciclos,$status]);

		});
    		$message =  ' '.Auth::user()->name.'&nbsp;investment made successfully for '.$myinverst.' $';
    		// $message =  html_entity_decode(	"&lt;strong&gt;Findx&lt;/strong&gt;");
    	return Redirect::back()->with('status',$message);
    	// return $message;

    }


public function getDateLastInvest()
{
  $invest = investment::where('status', 1)->where('user_id', Auth::user()->id)
               ->orderBy('created_at')
               ->take(1)
               ->first();
               if ($invest==null) {
                 return false;
               }
               else {
return $invest->time_elapsed;
               }
}

public function getIntervalLastInvest($id)
{
return 3600;
}
public function CheckOneHourAgo()
{
// return 1474005206
$now = date_timestamp_get(date_create('now'));
  $LastInvest = $this->getDateLastInvest();
  $interval = $this->getIntervalLastInvest(Auth::user()->id);
  $count = $now - $LastInvest;
  if ($LastInvest == false) { return false;  }
  elseif ($count < $interval) {return false;}
  else {return true;}

}
public function getEarnings()
  {
  $allMyInvest = investment::where('status', 1)->where('user_id', Auth::user()->id)
               ->orderBy('created_at')
               // ->take(1)
               // ->first();
               ->get();
               // ->count();
  foreach ($allMyInvest as $invest => $value) {
 //tomo el tiempo que ha pasado
    $id = $value->id;
    if ($value->loop <= 0) {DB::table('investment')->where('id', $id )->update(['status' => 0]);}
    if ($value->time_elapsed ==  $value->finish_att ) {DB::table('investment')->where('id', $id )->update(['status' => 0]);}
    

    $tiempopasado = $value->time_elapsed;
    $tiempofinal = $value->finish_att;


  //tomo tiempo actual
    $tiempoactual = date_timestamp_get(date_create('now'));
    $resta = $tiempoactual - $tiempopasado;
    $intervalo = $value->interval;
    $ciclos = intval($resta / $intervalo);
    if ($tiempoactual > $tiempofinal ) {
      $ciclos = $value->loop;
    }
    $nuevotiempoactual = ($ciclos*$intervalo) + $tiempopasado;  
    // echo $ciclos;
    $ganancias= $value->earnins_interval * $ciclos;
      $iduser=Auth::user()->id;
    echo $ciclos;

      DB::transaction(function ($id) use($id, &$ganancias, &$iduser,&$ciclos,&$nuevotiempoactual) {
      DB::table('users')->where('id', '=', $iduser)->increment('balance', $ganancias);
      DB::table('investment')->where('id', '=', $id)->decrement('loop', $ciclos);
    DB::table('investment')->where('id', $id )->update(['time_elapsed' => $nuevotiempoactual]);
    // echo $earnins_interval;
    //determinar cuanto tiempo ha pasado
    //convertir ese tiempo en tT    
  //tomar el tiempo en el que se creo el paquete
  // $creadot = $value->time_elapsed;
  //tomamos el tiempo que sobro en anteriores
  // $sobrat = $value->time_left;
  //loop
  // $loop = $value->loop;
  // le añadimos el tiempo que sobro del anterior transaccion a la actual 
  // $time_elapsed = $time_elapsed+$sobrat;
  //restamos y tomamos el tiempo que paso
  // $resta = (($time_elapsed) - ($creadot) );
  //se toma el intervalo
  // $intervalo = $this->getIntervalLastInvest($value->id);
  //se divide para obtener la diferencia
  // $diferencia = $resta / $intervalo; 
  // aqui se añade el valor 
    // agregar ganancias
    //maxreturn tiene la cifra dividido por 100
    // echo $id;
    // echo $creadot;
    // echo "<br>";
    // echo $creadot;
      //   $now = date_create();
      //   $last = date_create('+24 hours');
      //   $lastt = date_timestamp_get($last);
      //   $nowd = date_timestamp_get($now);
      //   $status = 1;
      //     $earning = 1.5;
      // DB::insert('insert into investment (`investment`, `user_id`,`final_investment`,`created_att`,`finish_att`,`time_elapsed`,`status`) values (?, ?,?,?,?,?,?)', [$myinverst, $id, $myinverst*$earning,$nowd,$lastt,$nowd,$status]);
      // DB::insert('insert into investment (`investment`, `user_id`, `created_at`, `finish_at`) values (?, ?)', [$myinverst, $id]);

      });

  //aqui se sabe cuanto se va a restar 
  // $resto =(intval($diferencia)*$intervalo);
  //nuevo sobra
  // $new_left =(($time_elapsed) - ($creadot+$resto) );
}
$message =  ' '.Auth::user()->name.'&nbsp; earnigs generate';
return Redirect::back()->with('status',$message);

}

     /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        return view('datatables.index');
    }


    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
            $investment = investment::select(['investment','final_investment','created_att', 'finish_att','status'])
                          ->where('user_id', Auth::user()->id);
            return Datatables::of($investment)->make();
    }
}
