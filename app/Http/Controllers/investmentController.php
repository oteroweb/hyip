<?php

namespace App\Http\Controllers;

use App\DataTables\investmentDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateinvestmentRequest;
use App\Http\Requests\UpdateinvestmentRequest;
use App\Repositories\investmentRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class investmentController extends AppBaseController
{
    /** @var  investmentRepository */
    private $investmentRepository;

    public function __construct(investmentRepository $investmentRepo)
    {
        $this->investmentRepository = $investmentRepo;
    }

    /**
     * Display a listing of the investment.
     *
     * @param investmentDataTable $investmentDataTable
     * @return Response
     */
    public function index(investmentDataTable $investmentDataTable)
    {
        return $investmentDataTable->render('investments.index');
    }

    /**
     * Show the form for creating a new investment.
     *
     * @return Response
     */
    public function create()
    {
        return view('investments.create');
    }

    /**
     * Store a newly created investment in storage.
     *
     * @param CreateinvestmentRequest $request
     *
     * @return Response
     */
    public function store(CreateinvestmentRequest $request)
    {
        $input = $request->all();

        $investment = $this->investmentRepository->create($input);

        Flash::success('Investment saved successfully.');

        return redirect(route('investments.index'));
    }

    /**
     * Display the specified investment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $investment = $this->investmentRepository->findWithoutFail($id);

        if (empty($investment)) {
            Flash::error('Investment not found');

            return redirect(route('investments.index'));
        }

        return view('investments.show')->with('investment', $investment);
    }

    /**
     * Show the form for editing the specified investment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $investment = $this->investmentRepository->findWithoutFail($id);

        if (empty($investment)) {
            Flash::error('Investment not found');

            return redirect(route('investments.index'));
        }

        return view('investments.edit')->with('investment', $investment);
    }

    /**
     * Update the specified investment in storage.
     *
     * @param  int              $id
     * @param UpdateinvestmentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateinvestmentRequest $request)
    {
        $investment = $this->investmentRepository->findWithoutFail($id);

        if (empty($investment)) {
            Flash::error('Investment not found');

            return redirect(route('investments.index'));
        }

        $investment = $this->investmentRepository->update($request->all(), $id);

        Flash::success('Investment updated successfully.');

        return redirect(route('investments.index'));
    }

    /**
     * Remove the specified investment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $investment = $this->investmentRepository->findWithoutFail($id);

        if (empty($investment)) {
            Flash::error('Investment not found');

            return redirect(route('investments.index'));
        }

        $this->investmentRepository->delete($id);

        Flash::success('Investment deleted successfully.');

        return redirect(route('investments.index'));
    }
}
