<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class UserEmailVerifyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $verify = Auth::user()->verified;
        
        if ($verify == 0) {

return redirect()->route('dashboard')->withErrors([
                "message" => "You need verify his email, to do that"
                ]);
        } else {
        return $next($request);
        }
        
    }
}
