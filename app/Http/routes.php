<?php
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;
use App\User;
use App\investment;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {

    try {
    return view('welcome');
    	
    } catch (Exception $e) {
    // abort(404);
    	
    }
})->name('casa');

Route::get('user/activation/{token}', 'Auth\AuthController@activateUser')->name('user.activate');
Route::get('verification/error', 'Auth\AuthController@getVerificationError');
Route::get('verification/{token}', 'Auth\AuthController@getVerification');

Route::group(['middlewareGroups' => ['web','auth']], function () { 
	Route::get('deposit', function () {return view('deposit');})->name('deposit_');
    Route::post('pay', 'PayController@SelectPayment')->name('pay_');
	Route::get('inverst', 'InverstController@Index')->name('inverst');
	Route::get('pay', function () {return redirect()->route('deposit_'); });
	Route::get('withdrawals', 'WithdrawalsController@Index')->name('withdrawals');
	Route::post('inverst', 'InverstController@MakeInverst')->name('InverstMake');

	Route::post('withdrawals', 'WithdrawalsController@MakeWithdrawals')->name('WithdrawalsStore');
	Route::get('datatables',['uses'=>'InverstController@getIndex', 'as' => 'datatables']);
	Route::get('datatables/{data}',['uses'=>'InverstController@anyData', 'as' => 'Inverst.data']);
    Route::get('getearnings', 'InverstController@getEarnings')->name('generate.earnings');
    // Route::patch('home',['as' r=> 'dashboard','uses' => 'HomeController@index']);

});


/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {
    Route::group(['prefix' => 'v1'], function () {
        require config('infyom.laravel_generator.path.api_routes');
    });
});


Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate');





Route::group(['prefix' => 'administration', 'middleware' => ['role:owner']], function() {
    Route::get('/', 'AdminController@welcome')->name("administration.index");
    // Route::get('/manage', ['middleware' => ['permission:manage-admins'], 'uses' => 'AdminController@manageAdmins']);
});
// return App::abort(404);
