<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Validator;

class checkemail extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('accepted_email', function ($attribute, $value, $parameters) {
    $accepted_emails = [
        '123',
    ];
    $is_accepted = in_array($value, $accepted_emails);
    return $is_accepted;
});
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
       
    }
}
