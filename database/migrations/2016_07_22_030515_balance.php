<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Balance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasColumn('users', 'balance')) {
                Schema::table('users', function (Blueprint $table) {
                $table->float('balance')->default(0);
                $table->float('repurchase')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'balance')) {
       Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('balance');
            $table->dropColumn('repurchase');
        });
        }
    }
}
