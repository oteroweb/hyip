<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Timestampinverst extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('investment', 'created_att')) {
                Schema::table('investment', function (Blueprint $table) {
                $table->integer('created_att');
                $table->integer('finish_att');
                $table->integer('time_elapsed');
                $table->integer('status');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
