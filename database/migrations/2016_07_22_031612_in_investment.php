<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InInvestment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('investment')) {
            Schema::create('investment', function (Blueprint $table) {
                $table->increments('id');
                $table->float('investment', 8, 6)->default(0);
                $table->float('final_investment', 8, 6)->default(0);
                  $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                // $table->timestamp('created_at')->useCurrent();;   
                // $table->timestamp('finish_at');
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasColumn('investment', 'created_att')) {
                Schema::table('investment', function (Blueprint $table) {
                $table->integer('created_att');
                $table->integer('finish_att');
                $table->integer('time_elapsed');
                $table->integer('status');

            });
        }

         if (!Schema::hasColumn('investment', 'time_left')) {
                Schema::table('investment', function (Blueprint $table) {
                $table->integer('time_left');
            });
        }

         if (!Schema::hasColumn('investment', 'earnins_interval')) {
                Schema::table('investment', function (Blueprint $table) {
                $table->float('earnins_interval', 8, 6);    
            });
        }
        if (!Schema::hasColumn('investment', 'interval')) {
                Schema::table('investment', function (Blueprint $table) {
                $table->integer('interval');
            });
        }

        if (!Schema::hasColumn('investment', 'loop')) {
                Schema::table('investment', function (Blueprint $table) {
                $table->integer('loop');
            });
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('investment')) {

            Schema::drop('investment');
        }
    }
}
