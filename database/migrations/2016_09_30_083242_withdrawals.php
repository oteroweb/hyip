<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Withdrawals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           if (!Schema::hasTable('withdrawals')) {
    
        Schema::create('withdrawals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('withdrawals');
            $table->float('balance')->default(0);
            $table->integer('id_user');
            $table->string('source');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
